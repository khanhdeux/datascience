/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basic;

/**
 *
 * @author khanhdeux
 */
public class Applicant {
    //composition has-a relationship
    private Job job;
    
    public Applicant() {
        this.job = new Job();
        job.setSalary(1000L);
    }    
    
    public long getSalary() {
        return job.getSalary();
    }
}
