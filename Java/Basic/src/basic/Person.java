/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basic;

/**
 *
 * @author khanhdeux
 */
public class Person {
    private int age;

    public Person() {
      System.out.println("Person Created");
    }

    public Person(int i) {
      this.age = i;
      System.out.println("Person Created with Age = " + i);
    }    
}
