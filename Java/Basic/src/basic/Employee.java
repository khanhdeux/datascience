/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basic;

/**
 *
 * @author khanhdeux
 */
public class Employee {
    private int id;
    private String name;
  
    public Employee() {
        this("John Doe", 999);
        System.out.println("Default Employee Created");
    }
	
    public Employee(int i) {
        this("John Doe", i);
        System.out.println("Employee Created with Default Name");
    }
    
    public Employee(String s, int i) {
        this.id = i;
        this.name = s;
        System.out.println("Employee Created");
    }  
    
    @Override
    public String toString() {
        return "ID = "+id+", Name = "+name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }    
}
