/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basic;

/**
 *
 * @author khanhdeux
 */
public class Constructor {
    private String name;
    private int id;    
    
    //no-args constructor
    public Constructor() {
        this.name = "Default Name";
    }     
    //one parameter constructor
    public Constructor(String n) {
      this.name = n;
    }  
    //two parameter constructor
    public Constructor(String n, int i) {
      this.name = n;
      this.id = i;
    }    
    
    public String getName() {
      return name;
    }    
    public int getId() {
      return id;
    }    
    
    @Override
    public String toString() {
      return "ID="+id+", Name="+name;
    }    
}
