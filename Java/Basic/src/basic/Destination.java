/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basic;

/**
 *
 * @author khanhdeux
 */
public class Destination {
    static Destination NONE;
    static Destination OUTER_SPACE; 
    private String name;

    Destination(String name) {
        this.name = name;
    }

    Destination(Destination destination) {
        this.name = destination.name;
    }
}
