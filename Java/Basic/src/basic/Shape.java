/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basic;

/**
 *
 * @author khanhdeux
 */

public interface Shape {
    // public, static and final
    public String LABLE= "Shape";

    // interface methods are abstract và public
    void draw();

    double getArea();    
}
