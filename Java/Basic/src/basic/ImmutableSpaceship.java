/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basic;

/**
 *
 * @author khanhdeux
 */
public final class ImmutableSpaceship {
    private final String name;
    private final Destination destination;
    
    public ImmutableSpaceship(String name) {
        this.name = name;
        this.destination = new Destination("NONE");
    }
    
    private ImmutableSpaceship(String name, Destination destination) {
        this.name = name;
        this.destination = new Destination(destination);
    }
    
    public Destination currentDestination() {
        return new Destination(destination);
    }
    
    public ImmutableSpaceship newDestination(Destination newDestination) {
        return new ImmutableSpaceship(this.name, newDestination);
    }      

    @Override
    public String toString() {
        return "Name:" + name + ". Destination:" + destination; //To change body of generated methods, choose Tools | Templates.
    }
    
}
