/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basic;

import java.io.*; 
import java.util.ArrayList;
import java.util.List;

import java.nio.charset.Charset;
import java.util.*;
import java.util.function.Supplier;

import java.io.IOException;
import java.util.stream.Collectors;

/**
 *
 * @author khanhdeux
 */
public class Basic {
    private static Object obj;
    
    private String s_main_class = "default String";
   
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws UnsupportedEncodingException {
        System.out.println("Hello World");
        // Hello World
        
        System.out.println("%-------METHODS--------%");
        
        /** 
        // Error in a class
        public void print(String s) {}
        private static int print(String s) {}
         
        // Non-Error a class
        public void print(String s, int i) {}
        private static void print(int i, String s) {}
         */
        
        MathUtils mu = new MathUtils();
        System.out.println(mu.add(5, 2));
        MathUtils.print("Static Method");
        // 7
        // Static Method
        
        mu.print("Overloaded Method", 2);
        // Overloaded Method
        // Overloaded Method
        
        // Error
        // mu.divide(5, 0);
        // Exception in thread "main" java.lang.IllegalArgumentException: 
        // Can't divide by 0
        
        System.out.println(MathUtils.factorial(3));
        // 6 (3 * 2 * 1)
        
        mu.print("Method calls method", 1);
        // Method calls method
        
        System.out.println("%--------CONSTRUCTOR--------%");
        
        Constructor c = new Constructor();
        System.out.println(c);
        // ID=0, Name=Default Name
        
        c = new Constructor("Java");
        System.out.println(c); 
        // ID=0, Name=Java
        
        c = new Constructor("Pankaj", 25);
        System.out.println(c);
        // ID=25, Name=Pankaj
                
        /**
         * // Private constructor
         * private Data() {
         * 
         * }
         */
        
        Employee emp = new Employee();
        System.out.println(emp);
        // Employee Created
        // Default Employee Created
        // ID = 999, Name = John Doe
        
        Employee emp1 = new Employee(10);
        System.out.println(emp1);
        // Employee Created
        // Employee Created with Default Name
        // ID = 10, Name = John Doe
        
        
        Employee emp2 = new Employee("Pankaj", 20);
        System.out.println(emp2);   
        // Employee Created
        // ID = 20, Name = Pankaj
        
        Student st = new Student();
        System.out.println(st);
        // Person Created
        // Student Created
        // basic.Student@7852e922
        
        st = new Student(34, "Pankaj");
        System.out.println(st);   
        // Person Created with Age = 34
        // Student Created with name = Pankaj
        // basic.Student@4e25154f
        
        List<String> fl = new ArrayList<>();
        fl.add("Mango");
        fl.add("Orange");
        
        Fruits fr = new Fruits(fl); 
        System.out.println(fr.getFruitsList());  
        // [Mango, Orange]
        
        fr.getFruitsList().add("Apple");
        System.out.println(fr.getFruitsList()); 
        // [Mango, Orange, Apple]
               
        fr = new Fruits(fr);
        fr.getFruitsList().add("Banana");
        System.out.println(fr.getFruitsList());
        // [Mango, Orange, Apple, Banana]        

        System.out.println("%--------VARIABLE-------%");
        
        Variable varObj1 = new Variable(); 
        varObj1.studentAge();
        // Local variable age is : 5
        
        Variable varObj2 = new Variable();
        varObj2.instanceVariable = 80;
        System.out.println("Instance Variable : " + varObj2.instanceVariable);
        // Instance Variable : 80
                
        Variable.staticVariable = 100;
        System.out.println("Static Variable : " + Variable.staticVariable);        
        // Static Variable : 100.0
                
        System.out.println("%-------DATA TYPES---------%");
        
        boolean b = true;
        if (b == true)
            System.out.println("true");
        // true
        
        byte a = 126;
        System.out.println(a); 
        // 126
        
        char aChar = 'G';
        System.out.println(aChar);
        // G
        
        short aChar2 = 56;
        System.out.println(aChar2); 
        // 56
        
        int aInt = 56;
        System.out.println(aInt);
        // 56
        
        long aLong = 100000L;
        System.out.println(aLong);
        // 100000
        
        float aFloat = 4.5541132f;
        System.out.println(aFloat);
        // 4.5541134
        
        double aDouble = 1.33526252726;
        System.out.println(aDouble);
        // 1.33526252726                
        
        System.out.println("%--------STRING--------%");
        
        String str = "hello world";  
        String str2 = new String("Hello again");
        
        byte[] b_arr = {81, 101, 101, 107, 115};
        String s_byte = new String(b_arr);
        System.out.println(s_byte);
        // Qeeks
        
        byte[] b_arr2 = {81, 101, 101, 107, 115};
        Charset cs = Charset.defaultCharset();
        String s_byte_char = new String(b_arr2, cs);
        System.out.println(s_byte_char);
        // Qeeks
        
        byte[] b_arr3 = {68, 101, 101, 107, 115};
        String str3 = new String(b_arr3, "US-ASCII");
        System.out.println(str3);
        // Deeks
        
        byte[] b_arr4 = {68, 101, 101, 107, 115};
        String str4 = new String(b_arr4, 1, 3, cs);
        System.out.println(str4);
        // eek
        
        byte[] b_arr5 = {71, 101, 101, 107, 115};
        String str5 = new String(b_arr5, 1, 4, "US-ASCII");
        System.out.println(str5);
        // eeks
        
        char char_arr[] = {'d', 'o', 'g', 'k', 's'};
        String str6 = new String(char_arr);
        System.out.println(str6);
        // dogks
        
        char char_arr2[] = {'w', 'e', 'e', 'k', 's'};
        String str7 = new String(char_arr2 , 1, 3);
        System.out.println(str7);
        // eek
        
        int[] uni_code = {61, 101, 101, 107, 115};
        String str8 = new String(uni_code, 1, 3);
        System.out.println(str8);
        // eek
        
        String s_builder = "dogs";
        String str9 = new String(s_builder);
        System.out.println(str9);
        // dogs - Buffer
        
        System.out.println("Shadow".length());
        // 6
        System.out.println("Shadow".charAt(1));
        // h
        System.out.println("Shadow".substring(3));
        // dow
        System.out.println("Shadow".substring(2, 5));
        // ado
        
        String s1 = "Shadow";
        String s2 = "isfive";
        String output = s1.concat(s2);
        System.out.println(output);
        // Shadowisfive
        
        String s = "Shadow is Five";
        int output2 = s.indexOf("Five");
        System.out.println(output2);
        // 10
        
        Boolean out = "shadow".equalsIgnoreCase("shadow");
        System.out.println(out);
        // true
        out = "shadow".equals("Shadow");
        System.out.println(out);        
        // false
        out = "Shadow".equalsIgnoreCase("shadow");
        System.out.println(out);
        // true
        System.out.println("Hi".toLowerCase());
        // hi
        System.out.println("hi".toUpperCase());
        // HI
        System.out.println(" shadow is five ".trim());
        // shadow is five
        System.out.println("shadowisfive".replace('f', 'g'));
        // shadowisgive        
        
        System.out.println("%-------ACCESS MODIFIERS---------%");        
        
        /** 
         *  Default - Error
         *  package p1;

            class Example
            {
                void display()
                {
                    System.out.println("Hello World!");
                }
            }
            
            package p2;
            import p1.*;

            class ExampleNew
            {
                public static void main(String args[])
                {                 
                    Example obj = new Example();
                    obj.display();
                }
            } 
        * 
        */     
        
        /** 
         *  Private - Error
         *  package p1;

            class Example
            {
                private void display()
                {
                    System.out.println("Hello World!");
                }
            }
            
            package p2;
            import p1.*;

            class ExampleNew
            {
                public static void main(String args[])
                {                 
                    Example obj = new Example();
                    obj.display();
                }
            } 
        * 
        */   
        
        /** 
         *  Protected
         *  package p1;

            class Example
            {
                protected void display()
                {
                    System.out.println("Hello World!");
                }
            }
            
            package p2;
            import p1.*;

            class ExampleNew extends Example
            {
                public static void main(String args[])
                {                 
                    Example obj = new Example();
                    obj.display();
                }
            } 
            
            // Result : Hello World
        * 
        */    
        
        /** 
         *  Public
         *  package p1;

            class Example
            {
                public void display()
                {
                    System.out.println("Hello World!");
                }
            }
            
            package p2;
            import p1.*;

            class ExampleNew
            {
                public static void main(String args[])
                {                 
                    Example obj = new Example();
                    obj.display();
                }
            } 
            
            // Result : Hello World
        * 
        */         

        System.out.println("%--------ABSTRACT CLASS--------%");        
        
        Human student = new Worker("Dove", "Female", 0);
        Human employee = new Worker("Pankaj", "Male", 123);
        
        student.work();
        employee.work();
        // Not working
        // Working as employee!!
        
        employee.changeName("Pankaj Kumar");
        System.out.println(employee.toString());
        // Name=Pankaj Kumar::Gender=Male
        
        System.out.println("%-------INTERFACE---------%");
        
        Shape shape = new Circle(10);
        
        shape.draw();
        System.out.println("Area=" + shape.getArea());
        // Drawing Circle
        // Area=314.1592653589793
        
        // switching from one implementation to another easily
        shape = new Rectangle(10,10);
        shape.draw();
        System.out.println("Area=" + shape.getArea());        
        // Drawing Rectangle
        // Area=100.0
        
        System.out.println("%-------JAR---------%");
        
        // 1. Right-click on the Project name
        // 2. Select Properties
        // 3. Click Packaging
        // 4. Check Build JAR after Compiling
        // 5. Check Compress JAR File
        // 6. Click OK to accept changes
        // 7. Right-click on a Project name
        // 8. Select Build or Clean and Build 
        
        // Create jar
        // jar cf Basic.jar Basic
        
        // Watch jar
        // jar tf Basic.jar
        
        // Extract jar
        // jar xf Basic.jar
        
        // Update jar
        // jar uf Basic.jar
        
        // Run jar
        // java -jar Basic.jar

        System.out.println("%-------NULL---------%");
        
        Object obj1 = null;
        // Error: Object obj = NULL;
        System.out.println("Value of object obj is : " + obj1);
        // Value of object obj is : null
        System.out.println("Value of object obj is : " + obj);
        // Value of object obj is : null
        
        Integer i = null;
        Integer j = 10;            

        System.out.println(i instanceof Integer);
        // false
        System.out.println(j instanceof Integer);
        // true
        
        Basic mockObj = null;
        mockObj.staticMethod();       
        // static method, can be called by null reference
        
        // Error
        // mockObj.nonStaticMethod(); 
        // Exception in thread "main" java.lang.NullPointerException
        
        System.out.println(null==null);
        System.out.println(null!=null);  
        // true
        // false
        
        System.out.println("%-------NUMBER---------%");     
        
        Integer x = 5; // boxes int to an Integer object
        x =  x + 10;   // unboxes the Integer to a int
        System.out.println(x); 
        // 15
        
        // Create a Double Class object with value "6.9685"
        Double d = new Double("6.9685");        
        // Convert Double(Number) object into different types
        byte dByte = d.byteValue();
        short dShort = d.shortValue();
        int dInt = d.intValue();
        long dLong = d.longValue();
        float dFloat = d.floatValue();
        double dDouble = d.doubleValue();
        
        System.out.println("value of d after converting it to byte : " + dByte);
        System.out.println("value of d after converting it to short : " + dShort);
        System.out.println("value of d after converting it to int : " + dInt);
        System.out.println("value of d after converting it to long : " + dLong);
        System.out.println("value of d after converting it to float : " + dFloat);
        System.out.println("value of d after converting it to double : " + dDouble);        
        // value of d after converting it to byte : 6
        // value of d after converting it to short : 6
        // value of d after converting it to int : 6
        // value of d after converting it to long : 6
        // value of d after converting it to float : 6.9685
        // value of d after converting it to double : 6.9685
        
        // Create Integer Class object with value "10"
        Integer iCompare = new Integer("10"); 
        System.out.println(iCompare.compareTo(8));
        System.out.println(iCompare.compareTo(10));
        System.out.println(iCompare.compareTo(11)); 
        // 1
        // 0
        // -1
        
        Short sEqual = new Short("15");
        Short xEqual = 10;
        Integer yEqual = 15;
        Short zEqual = 15;        
        
        System.out.println(sEqual.equals(xEqual));
        System.out.println(sEqual.equals(yEqual));
        System.out.println(sEqual.equals(zEqual));   
        // false
        // false
        // true
        
        int zParse = Integer.parseInt("654",8);
        int aParse = Integer.parseInt("-FF", 16);
        long lParse = Long.parseLong("2158611234",10);
        
        System.out.println(zParse);
        System.out.println(aParse);
        System.out.println(lParse);
        // 428
        // -255
        // 2158611234
        
        int z = Integer.parseInt("655");
        long l = Long.parseLong("2123211234");          
        System.out.println(z);
        System.out.println(l);  
        // 655
        // 2123211234
        
        Integer xToString = 12;
        System.out.println(xToString.toString());
        // 12
        
        System.out.println(Integer.toString(12));
        System.out.println(Integer.toBinaryString(152));
        System.out.println(Integer.toHexString(152));
        System.out.println(Integer.toOctalString(152));
        // 12
        // 10011000
        // 98
        // 230
        
        Integer iValueOf =Integer.valueOf(50);
        Double dValueOf = Double.valueOf(9.36);
        System.out.println(iValueOf);
        System.out.println(dValueOf);
        // 50
        // 9.36
        
        Integer n = Integer.valueOf("333");
        Integer m = Integer.valueOf("-255");
        System.out.println(n);
        System.out.println(m); 
        // 333
        // -255
        
        Integer y = Integer.valueOf("333",8);
        Integer xValueOf = Integer.valueOf("-255",16);
        Long lValueOf = Long.valueOf("51688245",16);
        System.out.println(y);
        System.out.println(xValueOf);
        System.out.println(lValueOf);
        // 219
        // -597
        // 1365803589

        System.out.println("%------AUTOBOXING-UNBOXING----------%");        
        
        Integer iInit = new Integer(10);
        int i1 = iInit;
        System.out.println("Value of i: " + iInit);
        System.out.println("Value of i1: " + i1);
        // Value of i: 10
        // Value of i1: 10
        
        Character gfg = 'a';
        char ch = gfg;
        System.out.println("Value of ch: " + ch);
        System.out.println("Value of gfg: " + gfg); 
        // Value of ch: a
        // Value of gfg: a

        System.out.println("%-------STRING-BUFFER---------%");
        
        StringBuffer sBuffer = new StringBuffer("HELLO");
        int p = sBuffer.length();
        int q = sBuffer.capacity();  
        System.out.println("Length of string HELLO=" + p);
        System.out.println("Capacity of string HELLO=" + q);        
        // Length of string HELLO=5
        // Capacity of string HELLO=21
        
        sBuffer = new StringBuffer("example");
        sBuffer.append("s");
        System.out.println(sBuffer);
        // examples
        sBuffer.append(1);
        System.out.println(sBuffer);
        // examples1
        
        sBuffer = new StringBuffer("DataData");
        sBuffer.insert(4, "for");
        System.out.println(sBuffer);
        // DataforData
        sBuffer.insert(0, 5);
        System.out.println(sBuffer);
        // 5DataforData
        sBuffer.insert(3, true);
        System.out.println(sBuffer);
        // 5DatruetaforData
        sBuffer.insert(5, 41.35d);
        System.out.println(sBuffer);
        // 5Datr41.35uetaforData
        sBuffer.insert(8, 41.35f);
        System.out.println(sBuffer);        
        // 5Datr41.41.3535uetaforData
        char data_arr[] = { 'p', 'a', 'w', 'a', 'n' };
        sBuffer.insert(2, data_arr);
        System.out.println(sBuffer);
        // 5Dpawanatr41.41.3535uetaforData
        
        sBuffer = new StringBuffer("Example");
        sBuffer.reverse();
        System.out.println(sBuffer);
        // elpmaxE
        
        sBuffer = new StringBuffer("DeleteExample");
        sBuffer.delete(0,6);
        System.out.println(sBuffer);
        // Example
        sBuffer.deleteCharAt(5);
        System.out.println(sBuffer);
        // Exampe
        
        sBuffer = new StringBuffer("Flair");
        sBuffer.replace(5,8,"are");
        System.out.println(sBuffer);
        // Flairare

        System.out.println("%-------FOR LOOP---------%");
        
        //print integers 5 to 10
        for (int k=5; k<=10; k++) {
            System.out.println("Java for loop example - " + k);
        }
        // Java for loop example - 5
        // Java for loop example - 6
        // Java for loop example - 7
        // Java for loop example - 8
        // Java for loop example - 9
        // Java for loop example - 10
        
        int[] intArray = { 1, 2, 3, 4, 5 };
        
        for (int o : intArray) 
            System.out.println("Java for each loop with array - " + o);
        // Java for each loop with array - 1
        // Java for each loop with array - 2
        // Java for each loop with array - 3
        // Java for each loop with array - 4
        // Java for each loop with array - 5
        
        List<String> fruits = new ArrayList<>();
        
        fruits.add("Apple");
        fruits.add("Banana");
        fruits.add("Orange");        
        
        for (String f : fruits)
            System.out.println("Java for each loop with collection - " + f);        
        
        // Java for each loop with collection - Apple
        // Java for each loop with collection - Banana
        // Java for each loop with collection - Orange
        
        int[][] intArr = { 
            { 1, -2, 3 }, 
            { 0, 3 }, 
            { 1, 2, 5 }, 
            { 9, 2, 5 } 
        };
        
        for (int v = 0; v < intArr.length; v++) {
            boolean allPositive = true;
            for (int w = 0; n < intArr[v].length; w++) {
                if (intArr[v][w] < 0) {
                    allPositive = false;
                    continue;
                }
            }
            if (allPositive) {
                // process the array
                System.out.println("Processing " + Arrays.toString(intArr[v]));
            }
            
            allPositive = true;
        }   
        // Processing [1, -2, 3]
        // Processing [0, 3]
        // Processing [1, 2, 5]
        // Processing [9, 2, 5]
        
        
        System.out.println("%-------COMPOSITION---------%");
        
        Applicant app = new Applicant();
        long salary = app.getSalary();
        System.out.println(salary);
        // 1000
        // Hint: when Applicant salary changed 
        // => Job salary func not affected

        System.out.println("%-------WHILE---------%");
        
        int t = 5;
        while (t <= 10) {
            System.out.println(t);
            t++;
        }
        // 5
        // 6
        // 7
        // 8
        // 9
        // 10
        
        List<String> veggies = new ArrayList<>();
        veggies.add("Spinach");
        veggies.add("Potato");
        veggies.add("Tomato");    
        Iterator<String> it = veggies.iterator();
 
        while(it.hasNext()) {
            System.out.println(it.next());
        }
        // Spinach
        // Potato
        // Tomato
        
        while(true) {
            System.out.println("Start Processing");
            // look for a file at specific directory
            // if found then process it, say insert rows into database
            System.out.println("End Processing");
			
            //  wait for 1 seconds and look again
            try {
                Thread.sleep(1*1000);
                break; // Added to keep running the next tests
            } catch (InterruptedException e) {
                System.out.println("Thread Interrupted, exit now");
                System.exit(0);
                break;
            }
        }
        // Start Processing
        // End Processing
        // Start Processing
        // End Processing
        // Start Processing
        // End Processing
        
        System.out.println("%--------INHERITANCE--------%");
        
        Cat cat = new Cat(false, "milk", 4, "black");
        System.out.println("Cat is Vegetarian?" + cat.isVegetarian());
        System.out.println("Cat eats " + cat.getEats());
        System.out.println("Cat has " + cat.getNoOfLegs() + " legs.");
        System.out.println("Cat color is " + cat.getColor());
        
        // Cat is Vegetarian?false
        // Cat eats milk
        // Cat has 4 legs.
        // Cat color is black
        
        Cat catInstance = new Cat(); //subclass instance
        Animal ani = catInstance; //upcasting
        System.out.println(ani.getClass());
        // class basic.Cat
        
        Cat c1 = (Cat) ani;
        System.out.println(c1.getClass());
        // class basic.Cat
        
        // Error - ClassCastException
        // Dog d = new Dog();
        // Animal a = d;
        // Cat c1 = (Cat) a;
        
        // Animal a1 = new Animal();
        // Cat c2 = (Cat) a1; // a1 hast "Animal" type
        
        Cat newCat = new Cat();
        Dog newDog = new Dog();
        Animal an = newCat;
        
        boolean flag = newCat instanceof Cat;
        System.out.println(flag);
        // true
        
        flag = newCat instanceof Animal;
        System.out.println(flag);
        // true
        
        flag = an instanceof Cat;
        System.out.println(flag);
        // true
        
        flag = an instanceof Dog;
        System.out.println(flag);
        // false

        System.out.println("%-------ARRAY---------%");        
        
        int iArray[];
        // or 
        int[] iArray2;
        
        iArray = new int[20];
        System.out.println(Arrays.toString(iArray));
        // [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        
        int[] integerArray = new int[]{ 1,2,3,4,5,6,7,8,9,10 };
        System.out.println(Arrays.toString(integerArray));
        // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        
        for (int r = 0; r < integerArray.length; r++) {
          System.out.println("Element at index " + r + " : "+ integerArray[r]);        
        }
        // Element at index 0 : 1
        // Element at index 1 : 2
        // Element at index 2 : 3
        // Element at index 3 : 4
        // Element at index 4 : 5
        // Element at index 5 : 6
        // Element at index 6 : 7
        // Element at index 7 : 8
        // Element at index 8 : 9
        // Element at index 9 : 10
        
        String[] strArr = new String[4];
        System.out.println(Arrays.toString(strArr));
        // [null, null, null, null]
        
        int[][] twoArrInt = new int[4][5];
        System.out.println(Arrays.deepToString(twoArrInt));
        // [[0, 0, 0, 0, 0], 
        //  [0, 0, 0, 0, 0], 
        //  [0, 0, 0, 0, 0], 
        //  [0, 0, 0, 0, 0]]
        
        int[][] twoIntArr = new int[2][];
        System.out.println(Arrays.deepToString(twoIntArr));
        // [null, null]
        twoIntArr[0] = new int[2];
        twoIntArr[1] = new int[3];
        System.out.println(Arrays.deepToString(twoIntArr));
        // [[0, 0], 
        //  [0, 0, 0]]
        
        int[] arrI = {1,2,3};
        int[][] arrI2 = {{1,2}, {1,2,3}};
        System.out.println(Arrays.toString(arrI));
        // [1, 2, 3]
        System.out.println(Arrays.deepToString(arrI2));
        // [[1, 2], [1, 2, 3]]

        System.out.println("%-------INNER CLASS---------%");             
        
        Basic mainObj = new Basic();
        mainObj.print();
        // ABC
        // default String
       
        OuterClass outer = new OuterClass(1,2,3,4);
        
        OuterClass.StaticNestedClass staticNestedClass = new OuterClass.StaticNestedClass();
        OuterClass.StaticNestedClass staticNestedClass1 = new OuterClass.StaticNestedClass();
        
        System.out.println(staticNestedClass.getName());
        // OuterClass
        staticNestedClass.d=10;
        System.out.println(staticNestedClass.d);
        System.out.println(staticNestedClass1.d);
        // 10
        // 0
        
        OuterClass.InnerClass innerClass = outer.new InnerClass();
        System.out.println(innerClass.getName());
        System.out.println(innerClass);
        innerClass.setValues();
        System.out.println(innerClass);
        // OuterClass
        // w=0:x=0:y=0:z=0
        // w=1:x=2:y=3:z=4
        
        outer.print("Outer");
        // Outer: OuterClass
        // Outer: 1
        // Outer: 2
        // Outer: 3
        // Outer: 4
        
        System.out.println(Arrays.toString(outer.getFilesInDir("src/basic", ".java")));
        System.out.println(Arrays.toString(outer.getFilesInDir("src/basic", ".class")));
        // [Basic.java, Applicant.java, Constructor.java, J...
        // []

        System.out.println("%-------HEAP AND STACK---------%");
        System.out.println("%-------MEMORY CLASS LOADED---------%");
        System.out.println("Line 1: All classes loaded into Heap Memory. Java Runtime creates a Stack Memory used by the Thread running the main function");
        System.out.println("Line 2: New Local variable created and saved into Stack Memory");
        System.out.println("Line 3: An object created in Heap Memory and Stack Memory which has its reference");
        System.out.println("Line 4: An object created in Heap Memory and Stack Memory which has its reference");
        System.out.println("Line 5: A block created and used by the method 'foo'");
        System.out.println("Line 6: A new reference to object in stack block");
        System.out.println("Line 7: A string created and saved into Heap space and a reference created into Stack foo()");
        System.out.println("Line 8: Method foo ends. Memory assigned to method 'foo' is released");
        System.out.println("Line 9: Method main ends. Stack Memory assigned to method 'main' is released. Java Runtime also releases all memory and ends the program");
        
        System.out.println("%-------VALUE TRANSFERING - NON-REFERENCE TRANSFERING---------%");
        
        Balloon red = new Balloon("Red"); // Memory reference 50
        Balloon blue = new Balloon("Blue"); // Memory Reference 100
        
        swap(red, blue);
        System.out.println("red color=" + red.getColor());
        System.out.println("blue color=" + blue.getColor());
        // red color=Red
        // blue color=Blue
        
        foo(blue);
        System.out.println("blue color=" + blue.getColor());
        // blue color=Red

        System.out.println("%-------CLOSURES---------%");
        
        int answer = 14;
        
        Thread th = new Thread(
          () -> System.out.println("The answer is: " + answer)
        );       
        
        Thread th2 = new Thread(new Runnable() {
            public void run() {
                System.out.println("The answer is: " + answer);
            }
        });
        
        int myVar = 42;
        Supplier<Integer> lambdaFun = () -> myVar;
        System.out.println(lambdaFun.get());
        // 42
        
        // Error
        // myVar++;
        // System.out.println(lambdaFun.get());

        System.out.println("%-------DEPENDENCY INJECTION---------%");
        
        MyApplication myApp = mainObj.new MyApplication();
        myApp.processMessages("HI", "test@gmail.com");
        // Email sent to test@gmail.com with Message=HI
        // Problem: 
        // Application(mainObj) is responsible for initiating EmailService
        // => Modification in EmailService => Application(mainObj) needs to be changed
        // New Func e.g SMS and Facebook messager
        // => new Service needed
        // => Modification in Application and Client
        // => Application test difficult. No chance to mock obj "email"
        
        // Solution 1:
        // Remove EmailServince instance init + move it to constructor
        // Problem:
        // Client + Test need to initialize EmailSevice
        MyApplication myApp2 = mainObj.new MyApplication(new EmailService());
        myApp2.processMessages("HI2", "test2@gmail.com");
        // Email sent to test2@gmail.com with Message=HI2
        
        // Dependency Injection
        // Requirements:
        // 1. Service components should be created als a class or interface.
        // The best is interface
        // 2. Consumers should be created based on Service interface
        // 3. Services should be created in Injectors and after that Consumer classes
        String msg = "Hi Pankaj";
        String email = "pankaj@abc.com";
        String phone = "4088888888";
        MessageServiceInjector injector = null;
        Consumer appConsumer = null;
        
        // Send email
        injector = new EmailServiceInjector();
        appConsumer = injector.getConsumer();
        appConsumer.processMessages(msg, email);
        // Email sent to pankaj@abc.com with Message=Hi Pankaj
        
        // Send SMS
        injector = new SMSServiceInjector();
        appConsumer = injector.getConsumer();
        appConsumer.processMessages(msg, email);
        // SMS sent to pankaj@abc.com with Message=Hi Pankaj

        System.out.println("%-------COLLECTION---------%");
        
        // Main interfaces: Set, List, and Map
        // Stack, Queue and Deque

        System.out.println("%-------SERIALIZATION---------%");
        
        String fileName = "Workman.ser";
        Workman wrk = new Workman();
        wrk.setId(100);
        wrk.setName("Pankaj");
        wrk.setSalary(5000);
        
        //serialize to file
        try {
          SerializationUtil.serialize(wrk, fileName);
        } catch (IOException e) {
          e.printStackTrace();
          return;
        }

        Workman empNew = null;
        try {
          empNew = (Workman) SerializationUtil.deserialize(fileName);
        } catch (ClassNotFoundException | IOException e) {
          e.printStackTrace();
        }
		
        System.out.println("emp Object::" + wrk);
        System.out.println("empNew Object::" + empNew);
        // emp Object::Employee{name=Pankaj,id=100, salary=5000}
        // empNew Object::Employee{name=Pankaj,id=100, salary=0}
        
        System.out.println("%-------FUNCTIONAL PROGRAMMING---------%");
        
        List<String> names = new ArrayList<>();        
        names.add("Max");
        names.add("Musterman");  
        names.add("Test");
        
        String greeting = "Welcome ";
        for(String name : names) {
            greeting += name + " ";
        }
        greeting += "!";
        
        System.out.println(greeting);
        // Welcome Max Musterman !
        
        // Error because final string is changed
        // final String reducedString = "Welcome " 
        //    + names.get(0) + " " + 
        //      names.get(names.size()-1) + " ";   
        // reducedString += "!";
        // System.out.println(reducedString);
        
        String greeting2 = names
            .stream()
            .map(name -> name + " ")
            .reduce("Welcome ", 
                    (acc, name) -> acc + name);  
        greeting2 += "!";
        System.out.println(greeting2);
        // Welcome Max Musterman !
             
        // Tradition way with List
        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(2);  
        numbers.add(3);
        
        System.out.println(numbers);
        System.out.println(mainObj.addOne(numbers));
        // [1, 2, 3]
        // [2, 3, 4]
        
        // "Map" way
        System.out.println(
           numbers
                .stream()
                .map(number -> number + 1)
                .collect(Collectors.toList())
        );
        // [2, 3, 4]

        System.out.println("%-------OPTIONAL---------%");
        
        // Error - NullPointerException
        // User findUserById(String userId) { ... };
        // User user = findUserById("667290");
        // System.out.println("User's Name = " + user.getName());
        
        // Solution
        // Optional<User> findUserById(String userId) { ... };
        // Optional<User> optional = findUserById("667290");
        // optional.ifPresent(user -> {
        // System.out.println("User's name = " + user.getName());    
        // })
        
        Optional<User> user = Optional.empty();
        System.out.println(user);
        // Optional.empty
        
        User userInstance = new User("667290", "Rajeev Kumar Singh");
        Optional<User> userOptional = Optional.of(userInstance);
        System.out.println(userInstance);
        System.out.println(userOptional);
        // ID=667290, Name=Rajeev Kumar Singh
        // Optional[ID=667290, Name=Rajeev Kumar Singh]
        // NullPointerException: userOptional = Optional.of(null)
        
        Optional<User> userNullOptional = Optional.ofNullable(null);
        // Optional.empty
        
        if(userOptional.isPresent()) {
            System.out.println("Value found - " + userOptional.get());
        } else {
            System.out.println("Optional is empty");
        }
        // Value found - ID=667290, Name=Rajeev Kumar Singh
        
        if(userNullOptional.isPresent()) {
            System.out.println("Value found - " + userOptional.get());
        } else {
            System.out.println("Optional is empty");
        }
        // Optional is empty
        
        userOptional.ifPresent(value -> {
            System.out.println("Value found - " + value);
        });  
        // Value found - ID=667290, Name=Rajeev Kumar Singh
        
        // User finalUser = (user != null) ? user : new User("0", "Unknown User");
        // User finalUser = optionalUser.orElse(new User("0", "Unknown User"));
        
        User finalUser = userNullOptional.orElseGet(() -> {
            return new User("0", "Unknown User");
        });
        System.out.println(finalUser);
        // ID=0, Name=Unknown User
        
        User userA = new User("123", "UserA");
        Optional<User> optionUserA = Optional.of(userA);
        
        if(userA != null && userA.getGender().equalsIgnoreCase("MALE")) {
            System.out.println(userA);
        }  
        // ID=123, Name=UserA       
        
        optionUserA
        .filter(fUser -> fUser.getGender().equalsIgnoreCase("MALE"))    
        .ifPresent(value -> {
            System.out.println(value);
        });
        // ID=123, Name=UserA
        
        Optional<String> genderOptional = optionUserA.map(User::getGender);
        Optional<String> maleGenderOptional = genderOptional
                .filter(gender -> gender.equalsIgnoreCase("MALE"));
        maleGenderOptional.ifPresent(value -> {
            System.out.println(value);
        });
        // MALE
        
        // OR 
        
        optionUserA.map(User::getGender)
        .filter(gender -> gender.equalsIgnoreCase("MALE"))
        .ifPresent(value -> {
            System.out.println(value);
        });
        // MALE
        
        User userB = new User("456", "UserB");
        Optional<String> maleGender = Optional.ofNullable(new String("MALE"));
        userB.setOptionalGender(maleGender);
        Optional<User> optionUserB = Optional.of(userB);        
        
        Optional<Optional<String>> maleOptionalGenderOptional = optionUserB.map(User::getOptionalGender);
        System.out.println(maleOptionalGenderOptional);
        // Optional[Optional[MALE]]
        Optional<String> maleOptionalGenderFinalOptional = optionUserB.flatMap(User::getOptionalGender);
        System.out.println(maleOptionalGenderFinalOptional);
        // Optional[MALE]

        System.out.println("%-------IMMUTABLE---------%");        
        
        Destination desc = new Destination("NONE");
        ImmutableSpaceship spaceShip = new ImmutableSpaceship("Test");
        System.out.println(spaceShip);
        // Name:Test. Destination:basic.Destination@5fd0d5ae
        spaceShip = spaceShip.newDestination(desc);
        System.out.println(spaceShip);
        // Name:Test. Destination:basic.Destination@2d98a335
        
        System.out.println("%--------MEMORY LEAK--------%");
        System.out.println("%--------GARBAGE COLLECTION--------%");        
        System.out.println("%--------REACTIVE STREAM --------%");
        System.out.println("%--------BYTECODE --------%"); 
        
        /**
         * Cmd:
         * javac Test.java (Compile data into .class)
         * // New Test.class
         * 
         * javap -v Test.class - Bytecode Result
         * int a = 1;
         * int b = 2;
         * int c = a + b;
         * 
         *  public static void main(java.lang.String[]); => String is parameter, return a void
            descriptor: ([Ljava/lang/String;)V
            flags: ACC_PUBLIC, ACC_STATIC => Method ist public (ACC_PUBLIC), and static (ACC_STATIC)
            Code:
              stack=2, locals=4, args_size=1
              // How deep ist the stack: 2
              // Local variables: 4 (1 for args and 3 for a, b, c)
              // 
                 0: iconst_1 => const "1" into stack
                 1: istore_1 => take "1" above and save it in local variable a
                 2: iconst_2 => const "2" into stack
                 3: istore_2 => take "2" above and save it in local variable b
                 4: iload_1 => load local variable a and save it into stack
                 5: iload_2 => load local variable b and save it into stack
                 6: iadd => take 2 above values from stack and summerize them, the result weill be saved in stack
                 7: istore_3 => take the value above and save it into local variable c
                 8: return => Return void
              LineNumberTable:
                line 14: 0
                line 15: 2
                line 16: 4
                line 17: 8
         */
        
        /**
         * Cmd:
         * javac Test.java (Compile data into .class)
         * // New Test.class
         * 
         * javap -v Test.class - Bytecode Result
         * int a = 1;
         * int b = 2;
         * int c = calc(a, b);
         * 
         *  public static void main(java.lang.String[]);
            descriptor: ([Ljava/lang/String;)V
            flags: ACC_PUBLIC, ACC_STATIC
            Code:
              stack=2, locals=4, args_size=1
                 0: iconst_1
                 1: istore_1
                 2: iconst_2
                 3: istore_2
                 4: iload_1
                 5: iload_2
                 6: invokestatic  #2                  // Method calc:(II)I
                 9: istore_3
                10: return
              LineNumberTable:
                line 14: 0
                line 15: 2
                line 16: 4
                line 17: 10

          static int calc(int, int);
            descriptor: (II)I
            flags: ACC_STATIC
            Code:
              stack=6, locals=2, args_size=2
                 0: iload_0
                 1: i2d
                 2: ldc2_w        #3                  // double 2.0d
                 5: invokestatic  #5                  // Method java/lang/Math.pow:(DD)D
                 8: iload_1
                 9: i2d
                10: ldc2_w        #3                  // double 2.0d
                13: invokestatic  #5                  // Method java/lang/Math.pow:(DD)D
                16: dadd
                17: invokestatic  #6                  // Method java/lang/Math.sqrt:(D)D
                20: d2i
                21: ireturn
              LineNumberTable:
                line 20: 0
         */        
        
        
        System.out.println("%----------------%");        
    }
    
    private static void staticMethod() {
        System.out.println("static method, can be called by null reference");
    }       
 
    private void nonStaticMethod() {      
         System.out.print(" Non-static method- ");
         System.out.println("cannot be called by null reference");
    }   
     
    public void print() {
        String s_print_method = "ABC";
        // local inner class in a method
        class Logger {
            // Main variables accessable
            String name = s_main_class; 
            // Non-final variables accessable
            String name1 = s_print_method; 

            public void foo() {
                String name1 = s_print_method;
                System.out.println(name1);
                System.out.println(name);
                // Compile Error
                // local s_print_method variable mus be final 
                // s_print_method = ":";
            }
        }
        // Initiate local inner class in method
        Logger logger = new Logger();         
        logger.foo();        
    }   
    
    static {
        class Foo {
            public void print() {
                System.out.println("Foo print");
            }
        }
        
        Foo f = new Foo();
        f.print();
    }
    
    class Memory {
        public void main(String[] args) { // Line 1
            int i=1; // Line 2
            Object obj = new Object(); // Line 3
            Memory mem = new Memory(); // Line 4
            mem.foo(obj); // Line 5
        } // Line 9

        private void foo(Object param) { // Line 6
            String str = param.toString(); //// Line 7
            System.out.println(str);
        } // Line 8
    }    
    
    public static void swap(Object o1, Object o2){
        Object temp = o1;
        o1=o2;
        o2=temp;
    }   
    
    private static void foo(Balloon balloon) { //baloon=100
        balloon.setColor("Red"); //baloon=100
        balloon = new Balloon("Green"); //baloon=200
        balloon.setColor("Blue"); //baloon = 200
    }    
    
    class MyApplication {
        private EmailService email = new EmailService();
        
        private EmailService emailService = null;
        
        public MyApplication() {
            
        }
        
        public MyApplication(EmailService svc) {
            this.emailService = svc;
        }
        
        public void processMessages(String msg, String rec){     
            this.email.sendEmail(msg, rec);
        }        
    } 
    
    public List<Integer> addOne(List<Integer> numbers) {
        List<Integer> plusOne = new LinkedList<>();
        for(Integer number : numbers) {
            plusOne.add(number + 1);
        }
        return plusOne;
    }    
    
    public interface Publisher<T> {
        public void subscribe(Subscriber<? super T> s);
    }  
    
    public interface Subscriber<T> {
        public void onSubscribe(Subscription s);
        public void onNext(T t);
        public void onError(Throwable t);
        public void onComplete();
    } 
    
    public interface Subscription {
        public void request(long n);
        public void cancel();
    }
    
    public interface Processor<T, R> extends Subscriber<T>, Publisher<R> {}
    
    public class PrintSubscriber implements Subscriber<Integer> {
        private Subscription subscription;
        @Override
        public void onSubscribe(Subscription subscription) {
            this.subscription = subscription;
            subscription.request(1);
        }
        @Override
        public void onNext(Integer item) {
            System.out.println("Received item: " + item);
            subscription.request(1);
        }
        @Override
        public void onError(Throwable error) {
            System.out.println("Error occurred: " + error.getMessage());
        }
        @Override
        public void onComplete() {
            System.out.println("PrintSubscriber is complete");
        }
    }    
    
}
