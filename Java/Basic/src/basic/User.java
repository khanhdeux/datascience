/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basic;

import java.util.Optional;

/**
 *
 * @author khanhdeux
 */
public class User {
    private String id;
    private String name;
    private String gender;
    private Optional<String> optionalGender;    
    
    public User(String id, String name) {
        this.id = id;
        this.name = name;
        this.gender = "MALE";
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }        

    public Optional<String> getOptionalGender() {
        return optionalGender;
    }

    public void setOptionalGender(Optional<String> optionalGender) {
        this.optionalGender = optionalGender;
    }
    
    @Override
    public String toString() {
      return "ID="+id+", Name="+name;
    }    
}
