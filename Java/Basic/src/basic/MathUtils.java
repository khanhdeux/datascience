/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basic;

/**
 *
 * @author khanhdeux
 */
public class MathUtils {
    public int add (int x, int y) {
        return x + y;
    }

    public static void print(String s) {
        System.out.println(s);
    }
  
    public void print(String s, int times) {
        for (int i=0; i< times; i++) {
            print(s);
        }
    }  
    
    public int divide(int x, int y) throws IllegalArgumentException {
        if(y==0) throw new IllegalArgumentException("Can't divide by 0");
        return x/y;
    }   
    
    public static long factorial(long n) {
        if (n == 1)
            return 1;
        else
            return (n * factorial(n - 1));
    }    
}
