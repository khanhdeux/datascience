/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basic;

/**
 *
 * @author khanhdeux
 */
public class Spaceship {
    public String name;
    public Destination destination;
    
    public Spaceship(String name) {
        this.name = name;
        this.destination = Destination.NONE;
    }
    
    public Spaceship(String name, Destination destination) {
        this.name = name;
        this.destination = destination;
    }
    
    public Destination currentDestination() {
        return destination;
    }
    
    public Spaceship exploreGalaxy() {
        destination = Destination.OUTER_SPACE;
        return this;
    }
}
