/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basic;

/**
 *
 * @author khanhdeux
 */
public class MyDIApplication implements Consumer { 
    private MessageService service;

    public MyDIApplication(){}
    
    //setter dependency injection
    public void setService(MessageService service) {
      this.service = service;
    }    

    @Override
    public void processMessages(String msg, String rec){
        this.service.sendMessage(msg, rec);
    }
}
