/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basic;

import java.io.Serializable;

/**
 *
 * @author khanhdeux
 */
public class Workman implements Serializable {
    //  private static final long serialVersionUID = -6470090944414208496L;

    private String name;
    private int id;
    transient private int salary;

    @Override
    public String toString(){
        return "Employee{name=" + name + ",id=" + id +", salary=" + salary + "}";
    } 

    // phương thức getter và setter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }	    
}
