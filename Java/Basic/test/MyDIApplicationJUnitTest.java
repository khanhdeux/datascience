/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import basic.Consumer;
import basic.MessageService;
import basic.MessageServiceInjector;
import basic.MyDIApplication;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author khanhdeux
 */
public class MyDIApplicationJUnitTest {
    private MessageServiceInjector injector;
    
    public MyDIApplicationJUnitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        //mock injector using anonymous class
        injector = new MessageServiceInjector() {
            @Override
            public Consumer getConsumer() {
                //mock Messageservice 
                MyDIApplication app = new MyDIApplication();
                app.setService(
                    new MessageService() {
                        @Override
                        public void sendMessage(String msg, String rec) {
                          System.out.println("Mock Message Service implementation");						
                        }
                    }                
                );
                return app;                         
           }
        };        
    }
    
    @After
    public void tearDown() {
        injector = null;
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void test() {
        Consumer consumer = injector.getConsumer();
        consumer.processMessages("Hi Pankaj", "pankaj@abc.com");        
        // Mock Message Service implementation
    }
}
