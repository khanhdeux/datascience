# python3 -m pip install ccxt
import ccxt
import pandas as pd
import numpy as np

# Binance API credentials
#api_key = '8X4wrODh0R9mnMtATwLnS24g2zxswA5ndQ1CIJuI6tN27Z961T5yXYDDO9cMOE8l'
#api_secret = 'Y9fky4vERG2CGsOfL4IZIo0oqojoWGsER59vb1iVZEtHIMT6bqaSMJvzDvYaTydJ'

api_key = 'b5a84bee97f86e77b9897b2ff1c877fe26f68510b084adf62e216898b6f41459'
api_secret = '91545f7f977c20a1f5e30ab220737a65787f4f01c37b9e5341e62b6da27bd901'

# Create Binance exchange object
exchange = ccxt.binance({
	'apiKey': api_key,
	'secret': api_secret,
    'options': {
        'defaultType': 'future'  # ←------------- defaultType inside options
    },    
    'enableRateLimit': True,
})

exchange.set_sandbox_mode(True) # activates testnet mode

# Fetch market data
# markets = exchange.load_markets()
# print(markets)

# Define trading pair and timeframe
symbol = 'ETH/USDT'
timeframe = '1h'

params = {'reduceOnly': 'true'}

# Get the latest market price for BTC/USDT
ticker = exchange.fetch_ticker(symbol)
#print(ticker)

def get_price():
    try:
        # Fetch the ticker
        ticker = exchange.fetch_ticker(symbol)
        
        # Extract the last price
        price = ticker['last']
        return price

    except Exception as e:
        print(f"Error: {e}")
        return None

def moving_average_strategy(symbol, short_window, long_window):
    # Fetch historical data (OHLCV)
    ohlcv = exchange.fetch_ohlcv(symbol, timeframe=timeframe, limit=long_window)
    df = pd.DataFrame(ohlcv, columns=['timestamp', 'open', 'high', 'low', 'close', 'volume'])  
    print(df)  
    
	# Calculate moving averages
    df['SMA_short'] = df['close'].rolling(window=short_window).mean()
    df['SMA_long'] = df['close'].rolling(window=long_window).mean()    
    
    # Determine whether to buy or sell
    if df['SMA_short'].iloc[-1] > df['SMA_long'].iloc[-1]:
        print("Buy Signal")
    else:
        print("Sell Signal")
			
        
# Example usage
moving_average_strategy(symbol=symbol, short_window=50, long_window=200)	

markets = exchange.load_markets()

exchange.verbose = True  # after load_markets()

#print(exchange.fetch_balance())


def execute_trade(symbol, action, amount):
    if action == "buy":
        order = exchange.create_market_buy_order(symbol, amount)
    elif action == "sell":
        order = exchange.create_market_sell_order(symbol, amount)
    print(order)

# Example usage

print(get_price())
amount = 200/get_price()
execute_trade(symbol, action='sell', amount=amount)


