//+------------------------------------------------------------------+
//|                                     ClimberFund_Notification.mq5 |
//|                                  Copyright 2023, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2023, Khanhdeux."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Trade\PositionInfo.mqh>
#include <Trade\Trade.mqh>
#include <Telegram.mqh>
#include <RegularExpressions\Regex.mqh>
#include <Arrays\ArrayLong.mqh>
#include <News.mqh>

//--- importing required dll files
#define MT_WMCMD_EXPERTS   32851
#define WM_COMMAND 0x0111
#define GA_ROOT    2
#include <WinAPI\winapi.mqh>


//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
input group  "==== Telegram Credentials ==="
input string ChatID = ""; // Chat ID
input string BotToken = ""; // Bot Token
input bool TestTelegram = false; // Test Telegram Signal

input group  "==== Stoploss Warning ==="
input double InitLoss = -100; // Initial Warning Loss (in $)
input double LossDistance = 100; // Distance Loss (InitLoss + distance = next warning)

input group  "==== ClimberFund Helper ==="
input bool     Stoploss_Active = false; // Stoploss active
input double   StopLoss_CloseAllPositions      = -2400; // Stoploss to close all positions ($)
input double   StopLoss_DefaultPoints          = 5000; // Default Stoploss points

input group  "==== Indicators ==="
input bool Warning_Rsi = false; // Rsi Warning?
input bool Warning_Cci = false; // CCi Warning?

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

CTrade trade;
CPositionInfo position;
CArrayLong positionMagics;

int CLOSE_KILL   =  1;

enum ENUM_DCA_ORDER_TYPE {DCA_BUY, DCA_SELL};


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string            closePattern = "\\/\\b(k|kill|c|close)\\b ([+-]?[0-9]*[.]?[0-9]+)"; // k 1915.74
string            alertPattern = "\\/\\balert\\b ([+-]?[0-9]*[.]?[0-9]+)"; // alert 1915.74

//+------------------------------------------------------------------+
//|   KhanhdeuxBot                                                   |
//+------------------------------------------------------------------+
class KhanhdeuxBot: public CCustomBot
  {
public:
   double            closePrice;
   int               closeType;
   double            alertPrice;

public:
   bool              checkAlertPattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(alertPattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   void              setAlertPrice(const string text)
     {
      CMatchCollection *matches=CRegex::Matches(text,alertPattern);
      CMatchEnumerator *en=matches.GetEnumerator();

      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         alertPrice = match.Groups()[1].Value();
        }

      delete en;
      delete matches;
     }

   bool              checkClosePattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(closePattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   void              setClosePrice(const string text)
     {
      CMatchCollection *matches=CRegex::Matches(text,closePattern);
      CMatchEnumerator *en=matches.GetEnumerator();
      string closeStr = "";

      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         closeStr = match.Groups()[1].Value();
         bot.closeType = 0;

         if(StringCompare(closeStr, "kill") == 0 || StringCompare(closeStr, "k") == 0)
           {
            bot.closeType = CLOSE_KILL;
           }

         closePrice = match.Groups()[2].Value();
        }

      delete en;
      delete matches;
     }

   void              ProcessMessages(void)
     {
      for(int i=0; i<m_chats.Total(); i++)
        {
         CCustomChat *chat=m_chats.GetNodeAtIndex(i);
         //--- if the message is not processed
         if(!chat.m_new_one.done)
           {
            chat.m_new_one.done=true;
            string text=chat.m_new_one.message_text;
            string actionMessage = StringFormat("%s %d (1:%g)", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN), SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE)) + "\n";

            //--- start
            if(text=="/test")
               SendMessage(chat.m_id, actionMessage + "Test! I am Notification Bot. \xF680");

            //--- help
            if(text=="/help")
              {
               string helpText = "My commands list: ";
               helpText += "\n/test test bot";
               helpText += "\n/info account";
               helpText += "\n/news Pip news";
               helpText += "\n/reset or /r refresh";
               helpText += "\n/on activate algo";
               helpText += "\n/off deactivate algo";
               helpText += "\n/kill or /k kill all";
               helpText += "\n/kill-last or /kl kill the last one";
               helpText += "\n/k [price] Kill at price. E.g /k 1901.02";
               helpText += "\n/kill or /k [price] Kill at price. E.g /k 1901.02";
               helpText += "\n/close or /c [price] Close at price. E.g /c 1901.02";
               helpText += "\n/alert [price] E.g /alert 1901.02";
               helpText += "\n/close-buy or /cb close buy";
               helpText += "\n/close-sell or /cs close sell";

               SendMessage(chat.m_id,helpText);
              }
            if(text=="/info")
              {
               string message = "";
               message       += StringFormat("[BAL]%G",AccountInfoDouble(ACCOUNT_BALANCE)) + "";
               message       += StringFormat("[PROF]%G",AccountInfoDouble(ACCOUNT_PROFIT)) + "";
               message       += StringFormat("[EQU]%G",AccountInfoDouble(ACCOUNT_EQUITY)) + "";
               message       += StringFormat("[ALGO]%G", (bool) TerminalInfoInteger(TERMINAL_TRADE_ALLOWED)) + "";               
               message       += "\n" + getPositionMagicMessage();
               // message       += "\n" + getPositionDetails();

               SendMessage(chat.m_id, actionMessage + message);
              }

            if(text=="/news")
              {
               SendMessage(chat.m_id, analysizeTipsAndDays());
              }

            if(text=="/reset" || text=="/r")
              {
               SendMessage(chat.m_id, actionMessage + "Received RESET order");
               closeAllPositions();
              }

            if(text=="/close-sell" || text=="/cs")
              {
               SendMessage(chat.m_id, actionMessage + "Received CLOSE SELL order");
               closeAllDCAPositions(DCA_SELL);
              }

            if(text=="/close-buy" || text=="/cb")
              {
               SendMessage(chat.m_id, actionMessage + "Received CLOSE BUY order");
               closeAllDCAPositions(DCA_BUY);
              }

            if(text=="/on")
              {
               SendMessage(chat.m_id, actionMessage + "Received ACTIVATE Algo");
               algoTradingToggle(true);
              }

            if(text=="/off")
              {
               SendMessage(chat.m_id, actionMessage + "Received DEACTIVATE Algo");
               algoTradingToggle(false);
              }

            if(text=="/kill" || text=="/k")
              {
               SendMessage(chat.m_id, actionMessage + "Received KILL order");
               closeAllPositions();
               algoTradingToggle(false);
              }

            if(text=="/kill-last" || text=="/kl")
              {
               SendMessage(chat.m_id, actionMessage + "Received KILL LAST order");
               closeLastPosition();
              }

            if(checkClosePattern(text))
              {
               setClosePrice(text);
               SendMessage(chat.m_id, actionMessage + "Received " + (bot.closeType == 1 ? "KILL" : "CLOSE") + " at Price: " + bot.closePrice);
              }

            if(checkAlertPattern(text))
              {
               setAlertPrice(text);
               SendMessage(chat.m_id, actionMessage + "Received ALERT at Price: " + bot.alertPrice);
              }
           }
        }
     }
  };

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
KhanhdeuxBot bot;
int getmeResult;
double loss;
int numOfPositions;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {
   Print("=== INIT === ", TimeCurrent());

   EventSetTimer(10);

   loss = InitLoss;
   numOfPositions = PositionsTotal();

   if(ChatID != "" && BotToken != "")
     {
      if(TestTelegram)
        {
         SendMessage("test", ChatID, BotToken);
        }

      //--- set token
      bot.Token(BotToken);
      //--- check token
      getmeResult=bot.GetMe();
     }

   return(INIT_SUCCEEDED);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   loss = InitLoss;
   positionMagics.Clear();

//--- destroy timer
   EventKillTimer();
  }

//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer()
  {
   if(Stoploss_Active)
     {
      updateDefaultStoploss();
     }
   executeTelegramBot();
  }

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
   if(Stoploss_Active)
     {
      if(getTotalProfit() < StopLoss_CloseAllPositions)
        {
         if(ChatID != "" && BotToken != "")
           {
            string message = StringFormat("Stoploss exeeded = %G", StopLoss_CloseAllPositions);
            printf(message);
            SendMessage(message, ChatID, BotToken);
           }

         closeAllPositions();
         algoTradingToggle(false);
        }
     }

   if(ChatID != "" && BotToken != "")
     {
      SendPositionsChanged();
      SendMessageToStoplossWarning();
      SendMessageToAlert();
     }

   if(bot.closePrice > 0)
     {
      closeAllPositionsAtPrice(bot.closePrice, bot.closeType);
     }

   static datetime bar_time=0;
   datetime this_bar_time=iTime(_Symbol,PERIOD_M1,0);

   if(bar_time!=this_bar_time)
     {
      bar_time=this_bar_time;

      if(ChatID != "" && BotToken != "")
        {
         if(Warning_Rsi)
           {
            SendMessageToRsi();
           }

         if(Warning_Cci)
           {
            SendMessageToCci();
           }
        }
     }
  }



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getTotalProfit()
  {
   double profit = 0;
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      ulong ticket = PositionGetTicket(i);
      string positionSymbol=PositionGetSymbol(i);

      if(ticket > 0 && positionSymbol == _Symbol)
        {
         profit += PositionGetDouble(POSITION_PROFIT);
        }
     }

   return profit;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void updateDefaultStoploss()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(position.SelectByIndex(i))
        {
         if(position.Symbol() == _Symbol)
           {
            double sl = position.StopLoss();
            if(sl == 0.0)
              {
               double tp = position.TakeProfit();

               if(position.PositionType() == POSITION_TYPE_BUY)
                 {
                  sl = NormalizeDouble(SymbolInfoDouble(_Symbol, SYMBOL_BID) - StopLoss_DefaultPoints*_Point,_Digits);
                 }
               if(position.PositionType() == POSITION_TYPE_SELL)
                 {
                  sl = NormalizeDouble(SymbolInfoDouble(_Symbol, SYMBOL_ASK) + StopLoss_DefaultPoints*_Point,_Digits);
                 }

               trade.PositionModify(position.Ticket(),sl,tp);
               Print(StringFormat("Position with lotsize=%g updated Stoploss= %g", PositionGetDouble(POSITION_VOLUME), sl));
              }
           }
        }
     }
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void initPositionMagics()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      ulong ticket = PositionGetTicket(i);
      if(ticket > 0)
        {
         long magic = position.Magic();
         bool found = false;
         for(int j=0; j< positionMagics.Total(); j++)
           {
            if(magic == positionMagics[j])
              {
               found = true;
              }
           }

         if(!found)
           {
            positionMagics.Add(magic);
           }
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SendMessageToStoplossWarning()
  {
   double pL = TotalProfit();

   if(pL <= loss)
     {
      string message = "WARNING!" + "";
      message       += "Loss:" + StringFormat("%g ", pL) + "<" + StringFormat("%g ", loss) + "";
      message       += "%0A" + getPositionMagicMessage();
      message       += "%0A" + StringFormat("%s %d (1:%g)", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN), SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE)) + "%0A";

      printf(message);
      SendMessage(message, ChatID, BotToken);
      loss -= LossDistance;
      positionMagics.Clear();
     }
   else
     {
      if(pL > loss + LossDistance * 1.5 && pL < InitLoss)
        {
         loss = loss + LossDistance;
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SendPositionsChanged()
  {
   int total = PositionsTotal();

   if(numOfPositions != total)
     {
      string message = "NOTICE!" + "";
      message       += "[PO]" + StringFormat("%d", numOfPositions) + "-" + StringFormat("%d", total) + "";
      message       += getPositionMagicMessage();
      message       += StringFormat("[BAL]%G",AccountInfoDouble(ACCOUNT_BALANCE)) + "";
      message       += StringFormat("[PRO]%G",AccountInfoDouble(ACCOUNT_PROFIT)) + "";
      message       += StringFormat("[EQU]%G",AccountInfoDouble(ACCOUNT_EQUITY)) + "";
      message       += "%0A" + getPositionDetails();
      message       += "%0A" + StringFormat("%s %d (1:%g)", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN), SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE)) + "%0A";

      numOfPositions = PositionsTotal();

      printf(message);
      SendMessage(message, ChatID, BotToken);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string getPositionMagicMessage()
  {
   string magicMessage = "";
   initPositionMagics();

   for(int i=0; i<positionMagics.Total(); i++)
     {
      long magic = positionMagics[i];
      int buyCount = 0;
      int sellCount = 0;

      magicMessage += StringFormat("[%d]", magic) +  "";

      for(int i = PositionsTotal() - 1; i >= 0; i--)
        {
         if(PositionGetTicket(i)
            && PositionGetSymbol(i) == _Symbol
            && position.Magic() == magic
            && PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY
           )
           {
            buyCount++;
           }
        }

      if(buyCount > 0)
        {
         magicMessage += StringFormat("BUY:L%d", buyCount);
        }

      for(int i = PositionsTotal() - 1; i >= 0; i--)
        {
         if(PositionGetTicket(i)
            && PositionGetSymbol(i) == _Symbol
            && position.Magic() == magic
            && PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL
           )
           {
            sellCount++;
           }
        }

      if(sellCount > 0)
        {
         magicMessage += StringFormat("SELL:L%d", sellCount);
        }

      magicMessage += "";
     }

   return magicMessage;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string getPositionDetails()
  {
   string detailMessage = "";
   initPositionMagics();

   for(int i=0; i<positionMagics.Total(); i++)
     {
      long magic = positionMagics[i];

      for(int i = PositionsTotal() - 1; i >= 0; i--)
        {
         if(PositionGetTicket(i)
            && PositionGetSymbol(i) == _Symbol
            && position.Magic() == magic
            && PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY
           )
           {
            string positionType = PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY ? "BUY": "SELL";
            detailMessage += StringFormat("%s L:%g, P:%g, T:%s |%I64d:%s",
                                          positionType,
                                          position.Volume(),
                                          position.PriceOpen(),
                                          TimeToString(PositionGetInteger(POSITION_TIME)),
                                          position.Magic(),
                                          position.Comment()) + "%0A";
           }
        }

      for(int i = PositionsTotal() - 1; i >= 0; i--)
        {
         if(PositionGetTicket(i)
            && PositionGetSymbol(i) == _Symbol
            && position.Magic() == magic
            && PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL
           )
           {
            string positionType = PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY ? "BUY": "SELL";
            detailMessage += StringFormat("%s L:%g, P:%g, T:%s |%I64d:%s",
                                          positionType,
                                          position.Volume(),
                                          position.PriceOpen(),
                                          TimeToString(PositionGetInteger(POSITION_TIME)),
                                          position.Magic(),
                                          position.Comment()) + "%0A";
           }
        }
     }

   return detailMessage;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SendMessageToAlert()
  {
   bool isAlert = false;

   if(bot.alertPrice <= 0)
     {
      return;
     }

   MqlRates rate[];
   CopyRates(_Symbol,PERIOD_M1,0,2,rate);

   double currentPrice = rate[1].close;
   double previousPrice = rate[0].close;
   double aPrice = bot.alertPrice;

   if(previousPrice <= aPrice && aPrice <= currentPrice)
     {
      isAlert = true;
     }

   if(currentPrice <= aPrice && aPrice <= previousPrice)
     {
      isAlert = true;
     }

   if(isAlert)
     {
      string message = "ALERT!Price crossed: " + bot.alertPrice;
      printf(message);
      SendMessage(message, ChatID, BotToken);
      bot.alertPrice = 0;
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SendMessageToRsi()
  {
   double rsi[];
   int rsiHandle = iRSI(_Symbol, PERIOD_M1, 14, PRICE_CLOSE);
   int oversold = 30;
   int overbought = 70;
   bool signal = false;

   CopyBuffer(rsiHandle, 0, 0, 3, rsi);

   string message = "SIGNAL!!!" + "%0A";

   if(rsi[0] < oversold && rsi[1] < oversold && rsi[2] >= oversold)
     {
      message += "BUY: RSI crossover " + StringFormat("%d", oversold);
      signal = true;
     }

   if(rsi[0] > overbought && rsi[1] > overbought && rsi[2] <= overbought)
     {
      message += "SELL: RSI crossunder " + StringFormat("%d", overbought);
      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }

   ArrayFree(rsi);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SendMessageToCci()
  {
   double cci[];
   int cciHanle = iCCI(_Symbol, PERIOD_M1, 14, PRICE_TYPICAL);
   int upper = 100;
   int lower = -100;
   bool signal = false;

   CopyBuffer(cciHanle, 0, 0, 2, cci);

   string message = "SIGNAL!!!" + "%0A";

   if(cci[0] < lower && cci[1] >= lower)
     {
      message += "BUY: CCI crossover " + StringFormat("%d", lower);
      signal = true;
     }


   if(cci[0] > upper && cci[1] <= upper)
     {
      message += "BUY: CCI crossunder " + StringFormat("%d", upper);
      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }

   ArrayFree(cci);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void executeTelegramBot()
  {
   if(ChatID != "" && BotToken != "")
     {
      //--- show error message end exit
      if(getmeResult!=0)
        {
         Comment("Error: ",GetErrorDescription(getmeResult));
         return;
        }

      //--- show bot name
      Comment(bot.Name());
      //--- reading messages
      bot.GetUpdates();
      //--- processing messages
      bot.ProcessMessages();
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CNews news;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string analysizeTipsAndDays()
  {
   int days = 30;
   string message = "";

   for(int i=days; i>= 0; i = i - 5)
     {
      MqlRates rate[];
      CopyRates(_Symbol,PERIOD_D1,i,1,rate);
      // message´+= rate[0].time + ": O=" + rate[0].open + " H=" + rate[0].high + " L=" + rate[0].low + " C=" + rate[0].close;
      message += rate[0].time + " ";
      double high = rate[0].high;
      double low = rate[0].low;
      //double maxL = MathRound((high - low)/(DCA_Strategy_Distance / 10));
      // message += "" + DoubleToString(test, 0) + ",";
      // message´+= "Total pips:" + (high - low)/(DCA_Strategy_Distance / 10);
      message += "Pips:" + DoubleToString((high - low),2) + "\n";

      int USD_COUNTRY_ID = 840;
      datetime currentTime = rate[0].time;

      bool hasNews = false;
      int totalnews = ArraySize(news.event);
      for(int i=0; i<totalnews; i++)
        {
         if(news.event[i].country_id == USD_COUNTRY_ID
            && news.event[i].importance >=3
            && news.event[i].time > currentTime && news.event[i].time < currentTime + 24 * 60 * 60
            && !hasNews)
           {
            message += news.event[i].country_id + " " +
                       news.eventname[i] + " " +
                       news.event[i].sector + " " +
                       news.event[i].time + " " +
                       news.event[i].timemode + " " +
                       news.event[i].event_type + " " +
                       news.event[i].importance + "\n";
            hasNews = true;
           }
        }

      message += "========================\n" ;
     }

   return message;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double TotalProfit()
  {
   double profit = 0;
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      ulong ticket = PositionGetTicket(i);
      string positionSymbol=PositionGetSymbol(i);

      if(ticket > 0 && positionSymbol == _Symbol)
        {
         profit += PositionGetDouble(POSITION_PROFIT);
        }
     }

   return profit;
  }

//+------------------------------------------------------------------+
//| Close all positions                                              |
//+------------------------------------------------------------------+
void closeAllPositions()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(position.SelectByIndex(i))
        {
         if(position.Symbol() == _Symbol)
           {
            trade.PositionClose(position.Ticket());
           }
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void closeLastPosition()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
        )
        {
         trade.PositionClose(position.Ticket());
         return;
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void closeAllPositionsAtPrice(double aPrice, int cType)
  {
   bool isClosed = false;

   MqlRates rate[];
   CopyRates(_Symbol,PERIOD_M1,0,2,rate);

   double currentPrice = rate[1].close;
   double previousPrice = rate[0].close;

   if(previousPrice <= aPrice && aPrice <= currentPrice)
     {
      isClosed = true;
     }

   if(currentPrice <= aPrice && aPrice <= previousPrice)
     {
      isClosed = true;
     }

   if(isClosed)
     {
      closeAllPositions();
      if(cType == CLOSE_KILL)
        {
         algoTradingToggle(false);
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void closeAllDCAPositions(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
        )
        {
         trade.PositionClose(position.Ticket());
         Print(StringFormat("Close Position with lotsize=%g", PositionGetDouble(POSITION_VOLUME)));
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getPositionTypeByOrder(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   if(currentOrderType == DCA_BUY)
     {
      return POSITION_TYPE_BUY;
     }
   if(currentOrderType == DCA_SELL)
     {
      return POSITION_TYPE_SELL;
     }

   return NULL;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int SendMessage(string text, string chatID, string botToken)
  {
   string baseUrl = "https://api.telegram.org";
   string headers = "";
   string requestURL = "";
   string requestHeaders = "";
   char resultData[];
   char posData[];
   int timeout = 2000;

   requestURL = StringFormat("%s/bot%s/sendmessage?chat_id=%s&text=%s", baseUrl, botToken, chatID, text);

   int response = WebRequest("POST", requestURL, headers, timeout, posData, resultData, requestHeaders);

   string resultMessage = CharArrayToString(resultData);
   Print(resultMessage);

   return response;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void algoTradingToggle(bool newStatus_True_Or_False)
  {
//--- getting the current status
   bool currentStatus = (bool) TerminalInfoInteger(TERMINAL_TRADE_ALLOWED);

//--- if the current status is equal to input trueFalse then, no need to toggle auto-trading
   if(currentStatus != newStatus_True_Or_False)
     {
      //--- Toggle Auto-Trading
      HANDLE hChart = (HANDLE) ChartGetInteger(ChartID(), CHART_WINDOW_HANDLE);
      PostMessageW(GetAncestor(hChart, GA_ROOT), WM_COMMAND, MT_WMCMD_EXPERTS, 0);
     }
  }
//+------------------------------------------------------------------+
