//+------------------------------------------------------------------+
//|                                            ClimberFundHelper.mq5 |
//|                                  Copyright 2023, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2023, Khanhdeux."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Trade\PositionInfo.mqh>
#include <Trade\Trade.mqh>
#include <Telegram.mqh>

CTrade trade;
CPositionInfo position;

//+------------------------------------------------------------------+
//|   KhanhdeuxBot                                                   |
//+------------------------------------------------------------------+
class KhanhdeuxBot: public CCustomBot
  {
public:
   int               signal;
public:
   void              ProcessMessages(void)
     {
      for(int i=0; i<m_chats.Total(); i++)
        {
         CCustomChat *chat=m_chats.GetNodeAtIndex(i);
         //--- if the message is not processed
         if(!chat.m_new_one.done)
           {
            chat.m_new_one.done=true;
            string text=chat.m_new_one.message_text;

            //--- start
            if(text=="/test")
               SendMessage(chat.m_id,"Test! I am KhanhdeuxBot. \xF680");

            //--- help
            if(text=="/help")
               SendMessage(chat.m_id,"My commands list: \n/start test bot \n/buy buy \n/sell sell \n/close close all\n/auto auto-trading");

            if(text=="/buy")
              {
               signal = 1;
               SendMessage(chat.m_id,"Received BUY order");
              }

            if(text=="/sell")
              {
               signal = -1;
               SendMessage(chat.m_id,"Received SELL order");
              }

            if(text=="/close")
              {
               signal = 2;
               SendMessage(chat.m_id,"Received CLOSE order");
              }

            if(text=="/auto")
              {
               signal = 3;
               SendMessage(chat.m_id,"Received AUTO order");
              }
           }
        }
     }
  };


input group  "==== DCA Trading ==="
input bool    DCA_Stratey_Active = false; // DCA Strategy active
enum ENUM_DCA_ORDER_TYPE {DCA_BUY, DCA_SELL};
input ENUM_DCA_ORDER_TYPE DCA_Strategy_OrderType = DCA_BUY; // DCA Order Type
input double  DCA_Stratey_LotSize = 0.01; // Lot Size
input int     DCA_Strategy_MaxDistance = 10; // Number of L that can be reached
input double  DCA_Stratey_Distance = 25; // Distance between L in points (10 pts=1 pip)
input ulong   DCA_Strategy_MagicNumber = 123456789; // Magic Number

input group  "==== No trade time ==="
input bool    TradingStop_Active = false; // Trading Stop active (GMT+2(winter),+3(summer))
input int     TradingStop_HourStart = 0; // Stop Hour Start
input int     TradingStop_HourStop = 1; // Stop Hour Stop
input bool    TradingStop_CloseAllPositions = true; // Close all positions before Stop Hour

input group  "==== ClimberFund Helper ==="
input bool     Stoploss_Active = false; // Stoploss active
input double   StopLossToCloseAllPositions      = -500; // Stoploss to close all positions
input double   DefaultStoplossPoints            = 2000; // Default Stoploss points

input group  "==== Telegram Credentials ==="
input string ChatID = ""; // Chat ID
input string BotToken = ""; // Bot Token
input bool TestTelegram = false; // Test Telegram Signal

input group  "==== DCA Analysis ==="
input bool   DCA_Matrix = false; // Show DCA Matrix
input double DCA_MaxLossInput = 1200; // Max Loss input per DCA
input double DCA_MaxNumOfDistance = 10; // Number of L that can be reached
input double DCA_Distance = 25; // Distance between L in points (10 pts=1 pip)
input int DCA_Leverage = 100; // Leverage ($ per pip)
input int DCA_LotSize_Multiply = 1; // Lotsize multiply

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
KhanhdeuxBot bot;
int getmeResult;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {
   Print("=== INIT === ", TimeCurrent());

   if(ChatID != "" && BotToken != "")
     {
      if(TestTelegram)
        {
         SendMessage("test", ChatID, BotToken);
        }

      //--- set token
      bot.Token(BotToken);
      //--- check token
      getmeResult=bot.GetMe();
     }

   EventSetTimer(10);

   trade.SetExpertMagicNumber(DCA_Strategy_MagicNumber);

   if(DCA_Matrix)
     {
      showDCAMatrix();
     }

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//--- destroy timer
   EventKillTimer();
   ArrayFree(rsi);
   bot.signal = 0;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
   if(Stoploss_Active)
     {
      if(TotalProfit() < StopLossToCloseAllPositions)
        {
         CloseAllPositions();
         Print(StringFormat("Stoploss exeeded = %G", StopLossToCloseAllPositions));
        }
     }
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer()
  {
// UpdateDefaultStoploss();
   ExecuteDCATrading();
   executeTelegramBot();
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void executeTelegramBot()
  {
   if(ChatID != "" && BotToken != "")
     {
      //--- show error message end exit
      if(getmeResult!=0)
        {
         Comment("Error: ",GetErrorDescription(getmeResult));
         return;
        }

      //--- show bot name
      Comment("Bot:", bot.Name());
      //--- reading messages
      bot.GetUpdates();
      //--- processing messages
      bot.ProcessMessages();
     }

   if(bot.signal == 2)
     {
      closeAllDCAPositions();
      bot.signal = 0;
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double TotalProfit()
  {
   double profit = 0;
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      ulong ticket = PositionGetTicket(i);
      string positionSymbol=PositionGetSymbol(i);

      if(ticket > 0 && positionSymbol == _Symbol)
        {
         profit += PositionGetDouble(POSITION_PROFIT);
        }
     }

   return profit;
  }

//+------------------------------------------------------------------+
//| Close all positions                                              |
//+------------------------------------------------------------------+
void CloseAllPositions()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(position.SelectByIndex(i))
        {
         if(position.Symbol() == _Symbol)
           {
            trade.PositionClose(position.Ticket());
           }
        }
     }
  }

//+------------------------------------------------------------------+
void UpdateDefaultStoploss()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(position.SelectByIndex(i))
        {
         if(position.Symbol() == _Symbol)
           {
            double sl = position.StopLoss();
            if(sl == 0.0)
              {
               double tp = position.TakeProfit();

               if(position.PositionType() == POSITION_TYPE_BUY)
                 {
                  sl = NormalizeDouble(SymbolInfoDouble(_Symbol, SYMBOL_BID) - DefaultStoplossPoints*_Point,_Digits);
                 }
               if(position.PositionType() == POSITION_TYPE_SELL)
                 {
                  sl = NormalizeDouble(SymbolInfoDouble(_Symbol, SYMBOL_ASK) + DefaultStoplossPoints*_Point,_Digits);
                 }

               trade.PositionModify(position.Ticket(),sl,tp);
               Print(StringFormat("Position with lotsize=%g updated Stoploss= %g", PositionGetDouble(POSITION_VOLUME), sl));
              }
           }
        }
     }
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void showDCAMatrix()
  {
   double initLotSize = 0.01 * DCA_LotSize_Multiply;

   for(double k=initLotSize; k<=initLotSize * 10; k=k+initLotSize)
     {
      Print("Lot Size:" + DoubleToString(k, 2));

      for(int i = 1; i <= DCA_MaxNumOfDistance; i++)
        {
         // Calculate TotalVolume
         double totalLotSize = calculateTotalVolume(i, k);

         // Calculate MaxLoss
         double maxLoss = calculateMaxLoss(i, k);

         // Calculate MaxProfit
         double maxProfit = calculateMaxProfit(i, k);

         if(maxLoss >= DCA_MaxLossInput)
           {
            if(i > 1)
              {
               Print(StringFormat("L%d___Total Volume:[%g], Max Loss:[-%g($)], Max Profit:[%g($)]", i - 1, calculateTotalVolume(i - 1, k), calculateMaxLoss(i - 1, k), calculateMaxProfit(i - 1, k)));
              }
            Print(StringFormat("L%d___Total Volume:[%g], Max Loss:[-%g($)], Max Profit:[%g($)]", i, totalLotSize, maxLoss, maxProfit));
            break;
           }
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateTotalVolume(int i, double lotSize)
  {
   double totalLotSize = 0;
   double currentLotSize = lotSize;

   for(int j=1; j<=i; j++)
     {
      totalLotSize += currentLotSize;
      currentLotSize *=2;
     }

   return totalLotSize;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateMaxLoss(int i, double lotSize)
  {
   double totalLoss = 0;
   double currentLotSize = lotSize;
   double pips = DCA_Distance/10;

   for(int j=1; j<=i; j++)
     {
      totalLoss += currentLotSize * pips * DCA_Leverage * (i - j + 1);
      currentLotSize *=2;
     }

   return totalLoss;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateMaxProfit(int i, double lotSize)
  {
   double totalProfit = 0;
   double pips = DCA_Distance/10;
   double currentLotSize = lotSize;

   double takeProfitPrice = 0;
   double priceVolumeSum = 0;

   for(int j=1; j<=i; j++)
     {
      totalProfit += pips * currentLotSize * DCA_Leverage;
      currentLotSize *=2;
     }

   return totalProfit;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double rsi[];

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void ExecuteDCATrading()
  {
   int totalPositions = getTotalDCAPositions();

   if(TradingStop_Active)
     {
      if(checkTradingStopHour())
        {
         if(TradingStop_CloseAllPositions && totalPositions > 0)
           {
            Print(StringFormat("Trading Stop Hour close all %d positions", totalPositions));
            closeAllDCAPositions();
           }
         return;
        }
     }

   if(DCA_Stratey_Active == false && bot.signal == 0 && totalPositions == 0)
     {
      return;
     }

   if(totalPositions == DCA_Strategy_MaxDistance && getNextDCASignal())
     {
      closeAllDCAPositions();
      return;
     }

   if(getTotalDCAOrders() > 0)
     {
      return;
     }


   if(totalPositions == DCA_Strategy_MaxDistance)
     {
      return;
     }

   int signal = getDCASignal();

   if(MathAbs(bot.signal) == 1 && totalPositions == 0)
     {
      signal = bot.signal;
     }

   if(bot.signal == 3 && totalPositions == 0)
     {
      int rsiSignal = getRsiSignal();

      if(rsiSignal)
        {
         signal = bot.signal;
        }
     }

   if(signal)
     {
      int newTotalPositions = totalPositions + 1;

      double lotSize =  calculateDCALotSize(newTotalPositions);
      double price = getCurrentPrice();
      double tp = calculateDCATakeProfit(lotSize, price);

      updateAllDCAPositionTakeProfit(tp);

      if(signal == 1)
        {
         double sl = NormalizeDouble(SymbolInfoDouble(_Symbol, SYMBOL_BID) - DefaultStoplossPoints*_Point,_Digits);
         Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, price, sl, tp, "BUY:" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions));
         trade.Buy(lotSize, _Symbol, price, sl, tp, "BUY:" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions);
        }

      if(signal == -1)
        {
         double sl = NormalizeDouble(SymbolInfoDouble(_Symbol, SYMBOL_ASK) + DefaultStoplossPoints*_Point,_Digits);
         Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, price, sl, tp, "SELL:" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions));
         trade.Sell(lotSize, _Symbol, price, sl, tp, "SELL:" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions);
        }

      signal = 0;
     }

   bot.signal = 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getDCASignal()
  {
   int signal = 0;
   ENUM_DCA_ORDER_TYPE currentOrderType = getCurrentOrderType();

   int nextDCASignal = getNextDCASignal();

   if(currentOrderType == DCA_BUY)
     {
      if(nextDCASignal == 1)
        {
         signal = 1;
        }
     }

   if(currentOrderType == DCA_SELL)
     {
      if(nextDCASignal == -1)
        {
         signal = -1;
        }
     }

   return signal;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getTotalDCAPositions()
  {
   int count = 0;
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i) && PositionGetSymbol(i) == _Symbol && position.Magic() == DCA_Strategy_MagicNumber)
        {
         count++;
        }
     }

   return count;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getTotalDCAOrders()
  {
   int count = 0;
   for(int i = OrdersTotal() - 1; i >= 0; i--)
     {
      if(OrderSelect(OrderGetTicket(i)))
        {
         string symbol = OrderGetString(ORDER_SYMBOL);
         long magicNumber = OrderGetInteger(ORDER_MAGIC);

         if(symbol == _Symbol && magicNumber == DCA_Strategy_MagicNumber)
           {
            count++;
           }
        }
     }

   return count;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getRsiSignal()
  {
   int rsiHandle = iRSI(_Symbol, PERIOD_M1, 14, PRICE_CLOSE);
   int oversold = 30;
   int overbought = 70;
   int signal = 0;
   CopyBuffer(rsiHandle, 0, 0, 3, rsi);
   ENUM_DCA_ORDER_TYPE currentOrderType = getCurrentOrderType();

   if(currentOrderType == DCA_BUY)
     {
      if(rsi[0] < oversold && rsi[1] < oversold && rsi[2] >= oversold)
        {
         signal = 1;
        }
     }

   if(currentOrderType == DCA_SELL)
     {
      if(rsi[0] > overbought && rsi[1] > overbought && rsi[2] <= overbought)
        {
         signal = -1;
        }
     }

   return 1;
//return signal;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int   getNextDCASignal()
  {
   int signal = 0;
   ENUM_DCA_ORDER_TYPE currentOrderType = getCurrentOrderType();
   double lastPrice = getLastDCAPrice();
   double pips = DCA_Stratey_Distance/10;
   double currentPrice = getCurrentPrice();

   if(currentOrderType == DCA_BUY)
     {
      if(lastPrice == 0 || currentPrice <= (lastPrice - pips))
        {
         signal = 1;
        }
     }

   if(currentOrderType == DCA_SELL)
     {
      if(lastPrice == 0 || currentPrice >= (lastPrice + pips))
        {
         signal = -1;
        }
     }

   return signal;
  }

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateDCATakeProfit(double lotSize, double price)
  {
   double priceVolumeSum = 0;
   double totalVolume = 0.0;
   double pips = DCA_Stratey_Distance/10;

   ENUM_DCA_ORDER_TYPE currentOrderType = getCurrentOrderType();

   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i) && PositionGetSymbol(i) == _Symbol && position.Magic() == DCA_Strategy_MagicNumber)
        {
         priceVolumeSum += currentOrderType == DCA_BUY ? (position.PriceOpen() + pips) * position.Volume() : (position.PriceOpen() - pips) * position.Volume();
         totalVolume += position.Volume();
        }
     }

   priceVolumeSum += currentOrderType == DCA_BUY ? (price + pips) * lotSize : (price - pips) * lotSize;
   totalVolume += lotSize;

   return priceVolumeSum / totalVolume;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateDCALotSize(int i)
  {
   if(i == 1)
     {
      return DCA_Stratey_LotSize;
     }
   return calculateDCALotSize(i - 1) * 2;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getCurrentPrice()
  {
   double currentPrice = SymbolInfoDouble(_Symbol, SYMBOL_LAST);
   ENUM_DCA_ORDER_TYPE currentOrderType = getCurrentOrderType();

   if(currentOrderType == DCA_BUY)
     {
      currentPrice = SymbolInfoDouble(_Symbol, SYMBOL_BID);
     }
   else
      if(currentOrderType == DCA_SELL)
        {
         currentPrice = SymbolInfoDouble(_Symbol, SYMBOL_ASK);
        }

   return currentPrice;
  }

//+------------------------------------------------------------------+
ENUM_DCA_ORDER_TYPE getCurrentOrderType()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i) && PositionGetSymbol(i) == _Symbol && position.Magic() == DCA_Strategy_MagicNumber)
        {
         int positionType = PositionGetInteger(POSITION_TYPE);
         if(positionType == POSITION_TYPE_BUY)
            return DCA_BUY;
         if(positionType == POSITION_TYPE_SELL)
            return DCA_SELL;
        }
     }

   return DCA_Strategy_OrderType;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getLastDCAPrice()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i) && PositionGetSymbol(i) == _Symbol && position.Magic() == DCA_Strategy_MagicNumber)
        {
         return PositionGetDouble(POSITION_PRICE_OPEN);
        }
     }

   return 0;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void updateAllDCAPositionTakeProfit(double tpPrice)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i) && PositionGetSymbol(i) == _Symbol && position.Magic() == DCA_Strategy_MagicNumber)
        {
         double sl = position.StopLoss();
         double tp = tpPrice;

         trade.PositionModify(position.Ticket(),sl,tp);
         Print(StringFormat("Position with lotsize=%g updated TakeProfit= %g", PositionGetDouble(POSITION_VOLUME), tpPrice));
        }
     }
  }
//+------------------------------------------------------------------+
void closeAllDCAPositions()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i) && PositionGetSymbol(i) == _Symbol && position.Magic() == DCA_Strategy_MagicNumber)
        {
         trade.PositionClose(position.Ticket());
         Print(StringFormat("Close Position with lotsize=%g", PositionGetDouble(POSITION_VOLUME)));
        }
     }
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
bool checkTradingStopHour()
  {
   MqlDateTime time1;
   TimeToStruct(TimeCurrent(),time1);
   int hour=time1.hour;

   if(TradingStop_HourStart == TradingStop_HourStop)
     {
      if(hour == TradingStop_HourStart)
        {
         return true;
        }
     }

   if(TradingStop_HourStart < TradingStop_HourStop)
     {
      if(TradingStop_HourStart <= hour && hour < TradingStop_HourStop)
        {
         return true;
        }
     }

   if(TradingStop_HourStart > TradingStop_HourStop)
     {
      if(TradingStop_HourStart <= hour || hour < TradingStop_HourStop)
        {
         return true;
        }
     }

   return false;
  }
//+------------------------------------------------------------------+
int SendMessage(string text, string chatID, string botToken)
  {
   string baseUrl = "https://api.telegram.org";
   string headers = "";
   string requestURL = "";
   string requestHeaders = "";
   char resultData[];
   char posData[];
   int timeout = 2000;

   requestURL = StringFormat("%s/bot%s/sendmessage?chat_id=%s&text=%s", baseUrl, botToken, chatID, text);

   int response = WebRequest("POST", requestURL, headers, timeout, posData, resultData, requestHeaders);

   string resultMessage = CharArrayToString(resultData);
   Print(resultMessage);

   return response;
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
