//+------------------------------------------------------------------+
//|                                     ClimberFund_Notification.mq5 |
//|                                  Copyright 2023, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2023, Khanhdeux."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Trade\PositionInfo.mqh>
#include <Trade\Trade.mqh>

#include <Arrays\ArrayLong.mqh>

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
input group  "==== Telegram Credentials ==="
input string ChatID = ""; // Chat ID
input string BotToken = ""; // Bot Token
input bool TestTelegram = false; // Test Telegram Signal

input group  "==== Stoploss Warning ==="
input double InitLoss = -100; // Initial Warning Loss (in $)
input double LossDistance = 100; // Distance Loss (InitLoss + distance = next warning)

input group  "==== Indicators ==="
input bool Warning_Rsi = false; // Rsi Warning?
input bool Warning_Cci = false; // CCi Warning?

input group  "==== DCA Analysis ==="
input bool   DCA_Matrix = false; // Show DCA Matrix
input double DCA_MaxLossInput = 1200; // Max Loss input per DCA ($)
input double DCA_MaxNumOfDistance = 10; // Number of L that can be reached
input double DCA_Distance = 25; // Distance between L in points (10 pts=1 pip)
input int DCA_Leverage = 100; // Leverage ($ per pip)

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

CTrade trade;
CPositionInfo position;
CArrayLong positionMagics;

double loss;
int numOfPositions;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {
   EventSetTimer(60);

   loss = InitLoss;
   numOfPositions = PositionsTotal();

   if(ChatID != "" && BotToken != "" && TestTelegram)
     {
      SendMessage("test", ChatID, BotToken);
     }

   if(DCA_Matrix)
     {
      showDCAMatrix();
     }

   return(INIT_SUCCEEDED);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void showDCAMatrix()
  {
   for(double k=0.01; k <= 0.1; k=k+0.01)
     {
      Print("Lot Size:" + DoubleToString(k, 2));

      for(int i = 1; i <= DCA_MaxNumOfDistance; i++)
        {
         // Calculate TotalVolume
         double totalLotSize = calculateTotalVolume(i, k);

         // Calculate MaxLoss
         double maxLoss = calculateMaxLoss(i, k);

         // Calculate MaxProfit
         double maxProfit = calculateMaxProfit(i, k);

         Print(StringFormat("L%d___ Total Volume:%g,  Max Loss:-%g($), Max Profit:%g($)", i, totalLotSize, maxLoss, maxProfit));

         if(maxLoss >= DCA_MaxLossInput)
           {
            break;
           }
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateTotalVolume(int i, double lotSize)
  {
   double totalLotSize = 0;
   double currentLotSize = lotSize;

   for(int j=1; j<=i; j++)
     {
      totalLotSize += currentLotSize;
      currentLotSize *=2;
     }

   return totalLotSize;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateMaxLoss(int i, double lotSize)
  {
   double totalLoss = 0;
   double currentLotSize = lotSize;
   double pips = DCA_Distance/10;

   for(int j=1; j<=i; j++)
     {
      totalLoss += currentLotSize * pips * DCA_Leverage * (i - j + 1);
      currentLotSize *=2;
     }

   return totalLoss;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateMaxProfit(int i, double lotSize)
  {
   double totalProfit = 0;
   double pips = DCA_Distance/10;
   double currentLotSize = lotSize;

   double takeProfitPrice = 0;
   double priceVolumeSum = 0;

   for(int j=1; j<=i; j++)
     {
      totalProfit += pips * currentLotSize * DCA_Leverage;
      currentLotSize *=2;
     }

   return totalProfit;
  }

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   loss = InitLoss;
   positionMagics.Clear();

//--- destroy timer
   EventKillTimer();
  }

//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer()
  {

  }

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
   if(ChatID != "" && BotToken != "")
     {
      // SendMessageToNewOrder();
      SendMessageToStoplossWarning();
      SendPositionsChanged();

      if(Warning_Rsi)
        {
         SendMessageToRsi();
        }

      if(Warning_Cci)
        {
         SendMessageToCci();
        }
     }
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double TotalProfit()
  {
   double profit = 0;
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      ulong ticket = PositionGetTicket(i);
      string positionSymbol=PositionGetSymbol(i);

      if(ticket > 0 && positionSymbol == _Symbol)
        {
         profit += PositionGetDouble(POSITION_PROFIT);
        }
     }

   return profit;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void initPositionMagics()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      ulong ticket = PositionGetTicket(i);
      if(ticket > 0)
        {
         long magic = position.Magic();
         bool found = false;
         for(int j=0; j< positionMagics.Total(); j++)
           {
            if(magic == positionMagics[j])
              {
               found = true;
              }
           }

         if(!found)
           {
            positionMagics.Add(magic);
           }
        }
     }
  }
  
void SendMessageToStoplossWarning()
  {
   double pL = TotalProfit();

   if(pL <= loss)
     {
      string message = "WARNING!" + "";
      //message       += StringFormat("%s %d (1:%g)", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN), SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE)) + "%0A";
      message       += "Loss:" + StringFormat("%g ", pL) + "<" + StringFormat("%g ", loss) + "";
      message       += getPositionMagicMessage();

      printf(message);
      SendMessage(message, ChatID, BotToken);
      loss -= LossDistance;
      positionMagics.Clear();
     }
   else
     {
      if(pL > loss + LossDistance * 1.5 && pL < InitLoss)
        {
         loss = loss + LossDistance;
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SendPositionsChanged()
  {
   int total = PositionsTotal();

   if(numOfPositions != total)
     {
      string message = "NOTICE!" + "";
      //message       += "%0A" + StringFormat("%s %d (1:%g)", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN), SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE)) + "%0A";
      message       += "[PO]" + StringFormat("%d", numOfPositions) + "-" + StringFormat("%d", total) + "";
      message       += getPositionMagicMessage();
      message       += StringFormat("[BAL]%G",AccountInfoDouble(ACCOUNT_BALANCE)) + "";
      message       += StringFormat("[PRO]%G",AccountInfoDouble(ACCOUNT_PROFIT)) + "";
      message       += StringFormat("[EQU]%G",AccountInfoDouble(ACCOUNT_EQUITY)) + "";

      numOfPositions = PositionsTotal();

      printf(message);
      SendMessage(message, ChatID, BotToken);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string getPositionMagicMessage()
  {
   string magicMessage = "";
   initPositionMagics();

   for(int i=0; i<positionMagics.Total(); i++)
     {
      long magic = positionMagics[i];
      int count = 0;
      for(int j = PositionsTotal() - 1; j >= 0; j--)
        {
         ulong ticket = PositionGetTicket(j);
         long currentMagic = position.Magic();

         if(ticket >0 && magic == currentMagic)
           {
            count +=1;
           }
        }

      magicMessage += StringFormat("[%d]", magic) +  "";
      magicMessage += StringFormat("L%d", count);
      magicMessage += "";
     }

   return magicMessage;
  }  

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SendMessageToRsi()
  {
   double rsi[];
   int rsiHandle = iRSI(_Symbol, PERIOD_M1, 14, PRICE_CLOSE);
   int oversold = 30;
   int overbought = 70;
   bool signal = false;

   CopyBuffer(rsiHandle, 0, 0, 3, rsi);

   string message = "SIGNAL!!!" + "%0A";

   if(rsi[0] < oversold && rsi[1] < oversold && rsi[2] >= oversold)
     {
      message += "BUY: RSI crossover " + StringFormat("%d", oversold);
      signal = true;
     }

   if(rsi[0] > overbought && rsi[1] > overbought && rsi[2] <= overbought)
     {
      message += "SELL: RSI crossunder " + StringFormat("%d", overbought);
      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }

   ArrayFree(rsi);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SendMessageToCci()
  {
   double cci[];
   int cciHanle = iCCI(_Symbol, PERIOD_M1, 14, PRICE_TYPICAL);
   int upper = 100;
   int lower = -100;
   bool signal = false;

   CopyBuffer(cciHanle, 0, 0, 2, cci);

   string message = "SIGNAL!!!" + "%0A";

   if(cci[0] < lower && cci[1] >= lower)
     {
      message += "BUY: CCI crossover " + StringFormat("%d", lower);
      signal = true;
     }


   if(cci[0] > upper && cci[1] <= upper)
     {
      message += "BUY: CCI crossunder " + StringFormat("%d", upper);
      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }

   ArrayFree(cci);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SendMessageToNewOrder()
  {
   for(int i = OrdersTotal() -1; i >= 0; i--)
     {
      ulong    ticket = OrderGetTicket(i);

      if(ticket > 0)
        {
         string message = "Ticket: #" + ticket + "%0A";
         message       += StringFormat("Symbol: %s ", OrderGetString(ORDER_SYMBOL)) + "%0A";
         message       += StringFormat("Type: %s ", EnumToString(ENUM_ORDER_TYPE(OrderGetInteger(ORDER_TYPE)))) + "%0A";
         message       += StringFormat("Volume: %g ", OrderGetDouble(ORDER_VOLUME_INITIAL)) + "%0A";
         message       += StringFormat("Price: %g ", OrderGetDouble(ORDER_PRICE_CURRENT)) + "%0A";
         message       += StringFormat("S/L: %g ", OrderGetDouble(ORDER_SL)) + "%0A";
         message       += StringFormat("T/P: %g ", OrderGetDouble(ORDER_TP)) + "%0A";
         message       += StringFormat("Time: %s ", TimeToString((datetime)OrderGetInteger(ORDER_TIME_SETUP))) + "%0A";

         printf(message);
         SendMessage(message, ChatID, BotToken);
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int SendMessage(string text, string chatID, string botToken)
  {
   string baseUrl = "https://api.telegram.org";
   string headers = "";
   string requestURL = "";
   string requestHeaders = "";
   char resultData[];
   char posData[];
   int timeout = 2000;

   requestURL = StringFormat("%s/bot%s/sendmessage?chat_id=%s&text=%s", baseUrl, botToken, chatID, text);

   int response = WebRequest("POST", requestURL, headers, timeout, posData, resultData, requestHeaders);

   string resultMessage = CharArrayToString(resultData);
   Print(resultMessage);

   return response;
  }
//+------------------------------------------------------------------+
