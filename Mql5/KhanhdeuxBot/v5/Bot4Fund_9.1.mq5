//+------------------------------------------------------------------+
//|                                            ClimberFundHelper.mq5 |
//|                                  Copyright 2023, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2024, Khanhdeux."
#property link      "https://www.mql5.com"
#property version   "5.00"

#include <Trade\PositionInfo.mqh>
#include <Trade\Trade.mqh>
#include <Telegram\Telegram.mqh>
#include <RegularExpressions\Regex.mqh>
#include <Arrays\ArrayLong.mqh>
#include <Daylight.mqh>
#include <News.mqh>

//--- importing required dll files
#define MT_WMCMD_EXPERTS   32851
#define WM_COMMAND 0x0111
#define GA_ROOT    2
#include <WinAPI\winapi.mqh>

int SIGNAL_BUY     =  1;
int SIGNAL_SELL    =  -1;
int SIGNAL_CLOSE   =  2;
int SIGNAL_DCA     =  3;
int SIGNAL_HEDG    =  4;
int SIGNAL_PAUSE   =  5;
int SIGNAL_NONE   =  6;
int SIGNAL_DCA_2 = 7;
int SIGNAL_DCA_3 = 8;

int CLOSE_KILL   =  1;

string COMMAND_MASTER_BUY = "/master-buy";
string COMMAND_MASTER_SELL = "/master-sell";

enum ENUM_DCA_ORDER_TYPE {DCA_BUY, DCA_SELL};
enum ENUM_SESSION_TYPE {SYDNEY, TOKYO, LONDON, NEWYORK};
enum ENUM_STRATEGY_TYPE {DCA, HEDG, DCA_2, DCA_3, NONE};
enum ENUM_RISK_TYPE {RISK_LOW, RISK_HIGH};
enum  ENUM_STRATEGY_ENTRY_TYPE
  {
   ENTRY_TYPE_RSI, // Rsi
   ENTRY_TYPE_FVG, // FVG
   ENTRY_TYPE_RSI_SMOOTHED, // Rsi Smoothed
   ENTRY_TYPE_LAST_DAY_PRICE // Last day price
  };
enum ENUM_NEWS_TYPE
  {
   DEFAULT,
   JOBLESS_CLAIM,
   CPI,
   SPEECH,
   NONFARM,
   GDP,
   PMI,
   GOODS,
   HOME_SALES,
   JOLTS,
   BUILDING_PERMIT,
   RETAIL_SALES,
   PPI,
   FED,
   MCS,
   CCI,
   PCE,
   OTHERS
  };

CTrade trade;
CPositionInfo position;
COrderInfo order;

CArrayLong positionMagics;
double currentBalance;
datetime dailyDate;
double htfRsi;
datetime lastBarTime = 0;

// Global constants

int    RSI_M5_BUY_LEVEL1 = 40; // Rsi M5 Buy level 1
int    RSI_M5_BUY_LEVEL2 = 35; // Rsi M5 Buy level 2
int    RSI_M5_BUY_LEVEL3 = 30; // Rsi M5 Buy level 3
int    RSI_M5_SELL_LEVEL1 = 60; // Rsi M5 Sell level 1
int    RSI_M5_SELL_LEVEL2 = 65; // Rsi M5 Sell level 2
int    RSI_M5_SELL_LEVEL3 = 70; // Rsi M5 Sell level 3
int    RSI_Overbought = 70; // Rsi Overbought
int    RSI_Oversold = 30; // Rsi Oversold

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class KhanhdeuxConfiguration
  {
public:
   double               distance;
   int                  maxDistance;
   ENUM_STRATEGY_TYPE   strategyType;
   ENUM_STRATEGY_ENTRY_TYPE entryType;
   ENUM_RISK_TYPE       riskType;
   int                  rsiOverbought;
   int                  rsiOversold;
   bool                 signalReversed;
public:
   //--- Default constructor
                     KhanhdeuxConfiguration() {};
   //--- Parametric constructor
                     KhanhdeuxConfiguration(double d, int m, ENUM_STRATEGY_TYPE st,ENUM_STRATEGY_ENTRY_TYPE et, ENUM_RISK_TYPE rt, bool sr)
     {
      distance = d;
      maxDistance = m;
      strategyType = st;
      entryType = et;
      riskType = rt;
      rsiOverbought = RSI_Overbought;
      rsiOversold = RSI_Oversold;
      signalReversed = sr;
     }
  };

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class KhanhdeuxSetup
  {
public:
   double            lotsize;
   double            distance;
   int               maxDistance;
public:
   //--- Default constructor
                     KhanhdeuxSetup() {};
   //--- Parametric constructor
                     KhanhdeuxSetup(double l, double d, double m)
     {
      lotsize = l;
      distance = d;
      maxDistance = m;
     }
  };

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class KhanhdeuxEvent
  {
public:
   string            name;
   datetime          time;
   int               importanceLevel;
public:
   //--- Default constructor
                     KhanhdeuxEvent() {};
   //--- Parametric constructor
                     KhanhdeuxEvent(string n, datetime t, int l)
     {
      name = n;
      time = t;
      importanceLevel = l;
     }
  };

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string            signalPattern = "\\/\\b(buy|sell|auto|hedg|update)\\b (\\d+),(\\d+),?(\\d+)?"; // /buy 15,5[,1200]
string            calculatePattern = "\\/\\bcalc\\b (\\d+),(\\d+),(\\d+)"; // /calc 1200,15,5
string            closePattern = "\\/\\b(k|kill|c|close)\\b ([+-]?[0-9]*[.]?[0-9]+)"; // /k 1915.74
string            alertPattern = "\\/\\balert\\b ([+-]?[0-9]*[.]?[0-9]+)"; // /alert 1915.74
string            lotSizePattern = "\\/\\bsize\\b ([+-]?[0-9]*[.]?[0-9]+)"; // /size 0.26
string            algoPattern = "\\/\\b(on|off|kill|reset|buy|sell|cbuy|csell|auto|manual|master|unmaster|slave|unslave)\\b (\\d+)"; // /on xxx
string            updatePattern = "\\/\\b(bsl|ssl|btp|stp)\\b (\\d+) ([+-]?[0-9]*[.]?[0-9]+)"; // /bsl xxx 1915.74
//+------------------------------------------------------------------+
//|   KhanhdeuxBot                                                   |
//+------------------------------------------------------------------+
class KhanhdeuxBot: public CCustomBot
  {
public:
   int               prevSignal;
   int               signal;
   double            lotsize;
   double            distance;
   int               maxDistance;
   double            initCaptital;
   double            closePrice;
   int               closeType;
   double            alertPrice;
   bool              manual;
   bool              isActive;
   bool              isMaster;
   bool              isSlave;

public:
   bool              checkOrderPattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(signalPattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   void              setBotByPattern(const string text)
     {
      CMatchCollection *matches=CRegex::Matches(text,signalPattern);
      CMatchEnumerator *en=matches.GetEnumerator();
      string signalStr = "";

      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         signalStr = match.Groups()[1].Value();

         if(StringCompare(signalStr, "buy") == 0)
           {
            bot.signal = SIGNAL_BUY;
           }

         if(StringCompare(signalStr, "sell") == 0)
           {
            bot.signal = SIGNAL_SELL;
           }

         if(StringCompare(signalStr, "auto") == 0)
           {
            bot.signal = SIGNAL_DCA;
           }

         if(StringCompare(signalStr, "hedg") == 0)
           {
            bot.signal = SIGNAL_HEDG;
           }

         distance = match.Groups()[2].Value();
         maxDistance = match.Groups()[3].Value();
         initCaptital = match.Groups()[4].Value();
        }

      if(bot.signal != 0 || StringCompare(signalStr, "update") == 0)
        {
         if(distance < 10 || maxDistance <= 0)
           {
            resetBot();
           }
         else
           {
            double initCapital = bot.initCaptital ? bot.initCaptital : getDailyStoploss();
            bot.lotsize = getBestLotSize(maxDistance, distance, initCapital);
           }
        }


      delete en;
      delete matches;
     }

   bool              checkCalculatePattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(calculatePattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   double              getCalculatedLotSize(const string text)
     {
      double result = 0;

      CMatchCollection *matches=CRegex::Matches(text,calculatePattern);
      CMatchEnumerator *en=matches.GetEnumerator();
      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         double _distance = match.Groups()[1].Value();
         double _maxDistance = match.Groups()[2].Value();
         double _initCapital = match.Groups()[3].Value();

         if(_initCapital > 0 && _distance > 0 && _maxDistance > 0)
           {
            result = getBestLotSize(_maxDistance, _distance, _initCapital);
           }
        }

      delete en;
      delete matches;
      return result;
     }

   bool              checkAlertPattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(alertPattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   void              setAlertPrice(const string text)
     {
      CMatchCollection *matches=CRegex::Matches(text,alertPattern);
      CMatchEnumerator *en=matches.GetEnumerator();

      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         alertPrice = match.Groups()[1].Value();
        }

      delete en;
      delete matches;
     }

   void              resetBot(void)
     {
      signal = 0;
      lotsize = 0;
      distance = 0;
      maxDistance = 0;
      initCaptital = 0;
      closePrice = 0;
      closeType = 0;
     }

   bool              checkClosePattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(closePattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   void              setClosePrice(const string text)
     {
      CMatchCollection *matches=CRegex::Matches(text,closePattern);
      CMatchEnumerator *en=matches.GetEnumerator();
      string closeStr = "";

      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         closeStr = match.Groups()[1].Value();
         bot.closeType = 0;

         if(StringCompare(closeStr, "kill") == 0 || StringCompare(closeStr, "k") == 0)
           {
            bot.closeType = CLOSE_KILL;
           }

         closePrice = match.Groups()[2].Value();
        }

      delete en;
      delete matches;
     }

   bool              checkLotSizePattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(lotSizePattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   void              setLotSize(const string text)
     {
      CMatchCollection *matches=CRegex::Matches(text,lotSizePattern);
      CMatchEnumerator *en=matches.GetEnumerator();

      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         lotsize = match.Groups()[1].Value();
        }

      delete en;
      delete matches;
     }

   bool              checkAlgoPattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(algoPattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   string             setAlgo(const string text)
     {
      int accountLogin = 0;
      string algoType = "";
      string message = "";

      CMatchCollection *matches=CRegex::Matches(text,algoPattern);
      CMatchEnumerator *en=matches.GetEnumerator();

      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         algoType = match.Groups()[1].Value();
         accountLogin = match.Groups()[2].Value();
        }

      int currentAccount = (int)AccountInfoInteger(ACCOUNT_LOGIN);

      if(currentAccount == accountLogin)
        {
         signal = 0;
         if(algoType == "on")
           {
            isActive = true;
            // algoTradingToggle(true);
           }

         if(algoType == "off")
           {
            isActive = false;
            // algoTradingToggle(false);
           }

         if(algoType == "kill")
           {
            closeAllPositions();
            isActive = false;
            // algoTradingToggle(false);
           }

         if(algoType == "reset")
           {
            closeAllPositions();
           }

         if(algoType == "buy")
           {
            signal = SIGNAL_BUY;
           }

         if(algoType == "sell")
           {
            signal = SIGNAL_SELL;
           }

         if(algoType == "cbuy")
           {
            closeAllDCAPositions(DCA_BUY);
           }

         if(algoType == "csell")
           {
            closeAllDCAPositions(DCA_SELL);
           }

         if(algoType == "auto")
           {
            manual = false;
           }

         if(algoType == "manual")
           {
            manual = true;
           }

         if(algoType == "master")
           {
            isMaster = true;
           }

         if(algoType == "unmaster")
           {
            isMaster = false;
           }

         if(algoType == "slave")
           {
            isSlave = true;
           }

         if(algoType == "unslave")
           {
            isSlave = false;
           }

         message += algoType + "\n";
         message += StringFormat("%s %d (1:%g:%d)", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN), SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE), AccountInfoInteger(ACCOUNT_LEVERAGE));
        }

      delete en;
      delete matches;

      return message;
     }

   bool              checkUpdatePattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(updatePattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   string             updatePosition(const string text)
     {
      int accountLogin = 0;
      string updateType = "";
      double price;
      string message = "";

      CMatchCollection *matches=CRegex::Matches(text,updatePattern);
      CMatchEnumerator *en=matches.GetEnumerator();

      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         updateType = match.Groups()[1].Value();
         accountLogin = match.Groups()[2].Value();
         price = match.Groups()[3].Value();
        }

      int currentAccount = (int)AccountInfoInteger(ACCOUNT_LOGIN);

      if(currentAccount == accountLogin)
        {
         if(updateType == "bsl")
           {
            message += StringFormat("UPDATE BUY!!! SL = %G", price) + "\n";
            updateAllDCAPositionStopLoss(price, DCA_BUY);
           }

         if(updateType == "ssl")
           {
            message += StringFormat("UPDATE SELL!!! SL = %G", price) + "\n";
            updateAllDCAPositionStopLoss(price, DCA_SELL);
           }

         if(updateType == "btp")
           {
            message += StringFormat("UPDATE BUY!!! TP = %G", price) + "\n";
            updateAllDCAPositionTakeProfit(price, DCA_BUY);
           }

         if(updateType == "stp")
           {
            message += StringFormat("UPDATE SELL!!! TP = %G", price) + "\n";
            updateAllDCAPositionTakeProfit(price, DCA_SELL);
           }

         message += StringFormat("%s %d (1:%g:%d)", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN), SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE), AccountInfoInteger(ACCOUNT_LEVERAGE));
        }

      delete en;
      delete matches;

      return message;
     }

   void              ProcessMessages(void)
     {
      for(int i=0; i<m_chats.Total(); i++)
        {
         CCustomChat *chat=m_chats.GetNodeAtIndex(i);
         //--- if the message is not processed
         if(!chat.m_new_one.done)
           {
            chat.m_new_one.done=true;
            string text=chat.m_new_one.message_text;
            string actionMessage = StringFormat("%s %d (1:%g:%d)", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN), SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE), AccountInfoInteger(ACCOUNT_LEVERAGE)) + "\n";

            if(text==NULL || text=="")
              {
               continue;
              }

            if(isMessageCommandType(text))
              {
               Sleep(Notification_Delay);
              }

            //--- start
            if(text=="/test")
               SendMessage(chat.m_id, actionMessage + "Test! I am KhanhdeuxBot. \xF680");

            //--- help
            if(text=="/help")
              {
               string helpText = "My commands list: ";
               helpText += "\n/test test bot";
               helpText += "\n/info account";
               helpText += "\n/news today news";
               helpText += "\n/plan Plan for the week";
               helpText += "\n/config Config";
               helpText += "\n/buy buy. Buy account: /buy xxx: E.g /buy 1234";
               helpText += "\n/sell sell. Buy account: /sell xxx: E.g /sell 1234";
               helpText += "\n/reset or /r refresh. Reset account: /reset xxx: E.g /reset 1234";
               helpText += "\n/on activate algo. Activate account: /on xxx: E.g /on 1234";
               helpText += "\n/off deactivate algo. Deactivate account: /off xxx: E.g /off 1234";
               helpText += "\n/kill kill and off algo. Kill account: /kill xxx: E.g /kill 1234";
               helpText += "\n/cbuy close buy. Close buy account: /cbuy xxx: E.g /cbuy 1234";
               helpText += "\n/csell close sell. Close sell account: /csell xxx: E.g /csell 1234";
               helpText += "\n/auto or manual xxx Trade auto or manual";
               helpText += "\n/master or unmaster xxx Master or un-master";
               helpText += "\n/slave or unslave xxx Slave or un-slave";
               helpText += "\n/bsl|ssl|btp|stp xxx 1234. Buy SL, Sell SL, Buy TP, Sell TP";
               //helpText += "\n/restart restart bot";
               //helpText += "\n/kill or /k kill all. Kill account: /kill xxx. E.g /kill 1234";
               //helpText += "\n/kill-last or /kl kill the last one";
               //helpText += "\n/kill or /k [price] Kill at price. E.g /k 1901.02";
               //helpText += "\n/close or /c [price] Close at price. E.g /c 1901.02";
               //helpText += "\n/alert [price] E.g /alert 1901.02";
               //helpText += "\n/size [size] E.g /size 0.01";
               //helpText += "\n/close-hedg close hedg";
               //helpText += "\n/close-buy or /cb close buy";
               //helpText += "\n/close-sell or /cs close sell";
               //helpText += "\n/pause pause all actions";
               //helpText += "\n/resume resume previous actions";
               //helpText += "\n/auto auto-dca";
               //helpText += "\n/hedg auto-hedg";
               //helpText += "\n/buy|sell|auto|hedg|update dist,max[,initCap] (E.g: /buy 15,5 /buy 15,5,1200)";
               //helpText += "\n/calc dist,max,initCap (E.g: /calc 15,5,1200)";
               //helpText += "\n/best get best setup";

               SendMessage(chat.m_id,helpText);
              }
            if(text=="/info")
              {
               KhanhdeuxConfiguration* config = getConfiguration();

               string message = "";
               message       += StringFormat("[BAL]%G",AccountInfoDouble(ACCOUNT_BALANCE)) + "";
               message       += StringFormat("[PROF]%G",AccountInfoDouble(ACCOUNT_PROFIT)) + "";
               message       += StringFormat("[EQU]%G",AccountInfoDouble(ACCOUNT_EQUITY)) + "";
               message       += StringFormat("[SL]%G", getDailyStoploss()) + "";
               message       += StringFormat("[ALGO]%G", (bool) TerminalInfoInteger(TERMINAL_TRADE_ALLOWED)) + "";
               message       += StringFormat("[TYPE]%s", EnumToString(getDCAStrategyType())) + "";
               message       += StringFormat("[ENTRY_TYPE]%s", EnumToString(getEntryType())) + "";
               message       += StringFormat("[REVERSED]%s", isSignalReversed() ? "true" : "false") + "";
               message       += StringFormat("[LEVEL]%s", EnumToString(Risk_Level)) + "";
               message       += StringFormat("[ACTIVE]%s", bot.isActive ? "true" : "false") + "";
               message       += StringFormat("[MASTER]%s", bot.isMaster ? "true" : "false") + "";
               message       += StringFormat("[SLAVE]%s", bot.isSlave ? "true" : "false") + "";
               message       += StringFormat("\n Lot:%G, Dist:%G, Max:%G, Overbought:%G, Oversold:%G, Risk:%s",
                                             getDCALotSize(),
                                             getDCADistance(),
                                             getDCAMaxDistance(),
                                             getRsiOverbought(),
                                             getRsiOversold(),
                                             EnumToString(config.riskType));
               message       += "\n" + getPositionMagicMessage();
               message       += "\n" + getPositionDetails();

               SendMessage(chat.m_id, actionMessage + message);
              }

            if(text =="/news")
              {
               string message = getNewsMessage();
               SendMessage(chat.m_id, actionMessage + message);
              }

            if(text=="/plan")
              {
               string message = getPlanMessage();
               SendMessage(chat.m_id, actionMessage + message);
              }

            if(text=="/config")
              {
               string message = getConfigMessage();
               SendMessage(chat.m_id, actionMessage + message);
              }

            if(text=="/buy" || (text==COMMAND_MASTER_BUY && bot.isSlave))
              {
               signal = SIGNAL_BUY;
               SendMessage(chat.m_id, actionMessage + "Received BUY order");
              }

            if(text=="/sell" || (text==COMMAND_MASTER_SELL && bot.isSlave))
              {
               signal = SIGNAL_SELL;
               SendMessage(chat.m_id, actionMessage + "Received SELL order");
              }

            //if(text=="/restart")
            //  {
            //   resetBot();
            //   SendMessage(chat.m_id, actionMessage + "Received RESTART BOT");
            //  }

            //if(text=="/close")
            //  {
            //   resetBot();
            //   signal = SIGNAL_CLOSE;
            //   SendMessage(chat.m_id, actionMessage + "Received CLOSE order");
            //  }

            //if(text=="/pause")
            //  {
            //   prevSignal = signal;
            //   signal = SIGNAL_PAUSE;
            //   SendMessage(chat.m_id, actionMessage + "Received PAUSE order");
            //  }

            //if(text=="/resume")
            //  {
            //   signal = prevSignal;
            //   prevSignal = 0;
            //   SendMessage(chat.m_id, actionMessage + "Received RESUME order");
            //  }

            if(text=="/on")
              {
               SendMessage(chat.m_id, actionMessage + "Received ACTIVATE Algo");
               bot.isActive = true;
               // algoTradingToggle(true);
              }

            if(text=="/off")
              {
               SendMessage(chat.m_id, actionMessage + "Received DEACTIVATE Algo");
               bot.isActive = false;
               // algoTradingToggle(false);
              }

            if(text=="/kill" || text=="/k")
              {
               closeAllPositions();
               isActive = false;
               // algoTradingToggle(false);
              }

            //if(text=="/kill-last" || text=="/kl")
            //  {
            //   SendMessage(chat.m_id, actionMessage + "Received KILL LAST order");
            //   closeLastPosition();
            //  }

            //if(text=="/auto")
            //  {
            //   signal = SIGNAL_DCA;
            //   SendMessage(chat.m_id, actionMessage + "Received AUTO order");
            //  }

            //if(text=="/hedg")
            //  {
            //   signal = SIGNAL_HEDG;
            //   SendMessage(chat.m_id, actionMessage + "Received HEDG order");
            //  }

            if(text=="/reset" || text=="/r")
              {
               SendMessage(chat.m_id, actionMessage + "Received RESET order");
               closeAllPositions();
              }

            if(text=="/csell" || text=="/cs")
              {
               SendMessage(chat.m_id, actionMessage + "Received CLOSE SELL order");
               closeAllDCAPositions(DCA_SELL);
              }

            if(text=="/cbuy" || text=="/cb")
              {
               SendMessage(chat.m_id, actionMessage + "Received CLOSE BUY order");
               closeAllDCAPositions(DCA_BUY);
              }

            //if(text=="/close-hedg")
            //  {
            //   closeAllHedgPositions();
            //   SendMessage(chat.m_id, actionMessage + "Received CLOSE HEDG order");
            //  }

            if(checkOrderPattern(text))
              {
               setBotByPattern(text);
               SendMessage(chat.m_id, actionMessage + StringFormat("Received S:%d, Lot:%g, Dist:%g, Max:%g, InitC:%g", bot.signal, bot.lotsize, bot.distance, bot.maxDistance, bot.initCaptital));
              }

            //if(checkClosePattern(text))
            //  {
            //   setClosePrice(text);
            //   SendMessage(chat.m_id, actionMessage + "Received " + (bot.closeType == 1 ? "KILL" : "CLOSE") + " at Price: " + bot.closePrice);
            //  }

            if(checkAlertPattern(text))
              {
               setAlertPrice(text);
               SendMessage(chat.m_id, actionMessage + "Received ALERT at Price: " + bot.alertPrice);
              }

            if(checkLotSizePattern(text))
              {
               setLotSize(text);
               SendMessage(chat.m_id, actionMessage + "Received LotSize: " + bot.lotsize);
              }

            if(checkCalculatePattern(text))
              {
               SendMessage(chat.m_id, getCalculatedLotSize(text));
              }

            if(checkAlgoPattern(text))
              {
               string message = setAlgo(text);
               if(message != "")
                 {
                  SendMessage(chat.m_id, message);
                 }
              }

            if(checkUpdatePattern(text))
              {
               string message = updatePosition(text);
               if(message != "")
                 {
                  SendMessage(chat.m_id, message);
                 }
              }
           }
        }
     }
  };

input group  "==== Telegram Credentials ==="
input string ChatID = ""; // Chat ID
input string BotToken = ""; // Bot Token
input bool TestTelegram = true; // Test Telegram Signal

input group "==== LIVE CONFIG ===="

input double  ACCOUNT_BALANCE_INIT = 100000; // Account balance init

input bool isBotActive = true; // Is Bot Active?
input bool isBotMaster = false; // Is Bot Master?
input bool isBotSlave = false; // is Bot Slave?

input group "= Monday"
input string  SEPARATOR1 = ""; // ----ASIA----
input double              MONDAY_ASIA_Distance = 50; // Distance
input ENUM_STRATEGY_TYPE  MONDAY_ASIA_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  MONDAY_ASIA_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                MONDAY_ASIA_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      MONDAY_ASIA_Risk_Type = RISK_LOW; // Risk type

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
input string  SEPARATOR2 = ""; // ----UK----
input double              MONDAY_UK_Distance = 50; // Distance
input ENUM_STRATEGY_TYPE  MONDAY_UK_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  MONDAY_UK_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                MONDAY_UK_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      MONDAY_UK_Risk_Type = RISK_LOW; // Risk type

input string  SEPARATOR3 = ""; // ----US----
input double              MONDAY_US_Distance = 50; // Distance
input ENUM_STRATEGY_TYPE  MONDAY_US_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  MONDAY_US_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                MONDAY_US_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      MONDAY_US_Risk_Type = RISK_LOW; // Risk type

input group "= Tuesday"
input string  SEPARATOR4 = ""; // ----ASIA----
input double              TUESDAY_ASIA_Distance = 50; // Distance
input ENUM_STRATEGY_TYPE  TUESDAY_ASIA_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  TUESDAY_ASIA_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                TUESDAY_ASIA_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      TUESDAY_ASIA_Risk_Type = RISK_LOW; // Risk type

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
input string  SEPARATOR5 = ""; // ----UK----
input double              TUESDAY_UK_Distance = 50; // Distance
input ENUM_STRATEGY_TYPE  TUESDAY_UK_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  TUESDAY_UK_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                TUESDAY_UK_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      TUESDAY_UK_Risk_Type = RISK_LOW; // Risk type

input string  SEPARATOR6 = ""; // ----US----
input double              TUESDAY_US_Distance = 50; // Distance
input ENUM_STRATEGY_TYPE  TUESDAY_US_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  TUESDAY_US_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                TUESDAY_US_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      TUESDAY_US_Risk_Type = RISK_LOW; // Risk type

input group "= Wednesday"
input string  SEPARATOR7 = ""; // ----ASIA----
input double              WEDNESDAY_ASIA_Distance  = 50; // Distance
input ENUM_STRATEGY_TYPE  WEDNESDAY_ASIA_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  WEDNESDAY_ASIA_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                WEDNESDAY_ASIA_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      WEDNESDAY_ASIA_Risk_Type = RISK_LOW; // Risk type

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
input string  SEPARATOR8 = ""; // ----UK----
input double              WEDNESDAY_UK_Distance  = 50; // Distance
input ENUM_STRATEGY_TYPE  WEDNESDAY_UK_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  WEDNESDAY_UK_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                WEDNESDAY_UK_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      WEDNESDAY_UK_Risk_Type = RISK_LOW; // Risk type

input string  SEPARATOR9 = ""; // ----US----
input double              WEDNESDAY_US_Distance  = 50; // Distance
input ENUM_STRATEGY_TYPE  WEDNESDAY_US_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  WEDNESDAY_US_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                WEDNESDAY_US_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      WEDNESDAY_US_Risk_Type = RISK_LOW; // Risk type

input group "= Thursday"
input string  SEPARATOR10 = ""; // ----ASIA----
input double              THURSDAY_ASIA_Distance   = 50; // Distance
input ENUM_STRATEGY_TYPE  THURSDAY_ASIA_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  THURSDAY_ASIA_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                THURSDAY_ASIA_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      THURSDAY_ASIA_Risk_Type = RISK_LOW; // Risk type

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
input string  SEPARATOR11 = ""; // ----UK----
input double              THURSDAY_UK_Distance   = 50; // Distance
input ENUM_STRATEGY_TYPE  THURSDAY_UK_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  THURSDAY_UK_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                THURSDAY_UK_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      THURSDAY_UK_Risk_Type = RISK_LOW; // Risk type

input string  SEPARATOR12 = ""; // ----US----
input double              THURSDAY_US_Distance   = 50; // Distance
input ENUM_STRATEGY_TYPE  THURSDAY_US_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  THURSDAY_US_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                THURSDAY_US_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      THURSDAY_US_Risk_Type = RISK_LOW; // Risk type

input group "= Friday"
input string  SEPARATOR13 = ""; // ----ASIA----
input double              FRIDAY_ASIA_Distance     = 50; // Distance
input ENUM_STRATEGY_TYPE  FRIDAY_ASIA_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  FRIDAY_ASIA_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                FRIDAY_ASIA_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      FRIDAY_ASIA_Risk_Type = RISK_LOW; // Risk type

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
input string  SEPARATOR14 = ""; // ----UK----
input double              FRIDAY_UK_Distance     = 50; // Distance
input ENUM_STRATEGY_TYPE  FRIDAY_UK_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  FRIDAY_UK_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                FRIDAY_UK_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      FRIDAY_UK_Risk_Type = RISK_LOW; // Risk type

input string  SEPARATOR15 = ""; // ----US----
input double              FRIDAY_US_Distance     = 50; // Distance
input ENUM_STRATEGY_TYPE  FRIDAY_US_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  FRIDAY_US_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                FRIDAY_US_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      FRIDAY_US_Risk_Type = RISK_LOW; // Risk type

input group "= Saturday"
input string  SEPARATOR16 = ""; // ----ASIA----
input double              SATURDAY_ASIA_Distance    = 0; // Distance
input ENUM_STRATEGY_TYPE  SATURDAY_ASIA_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  SATURDAY_ASIA_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                SATURDAY_ASIA_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      SATURDAY_ASIA_Risk_Type = RISK_LOW; // Risk type

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
input string  SEPARATOR17 = ""; // ----UK----
input double              SATURDAY_UK_Distance    = 0; // Distance
input ENUM_STRATEGY_TYPE  SATURDAY_UK_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  SATURDAY_UK_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                SATURDAY_UK_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      SATURDAY_UK_Risk_Type = RISK_LOW; // Risk type

input string  SEPARATOR18 = ""; // ----US----
input double              SATURDAY_US_Distance    = 0; // Distance
input ENUM_STRATEGY_TYPE  SATURDAY_US_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  SATURDAY_US_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                SATURDAY_US_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      SATURDAY_US_Risk_Type = RISK_LOW; // Risk type

input group "= Sunday"
input string  SEPARATOR19 = ""; // ----ASIA----
input double              SUNDAY_ASIA_Distance     = 0; // Distance
input ENUM_STRATEGY_TYPE  SUNDAY_ASIA_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  SUNDAY_ASIA_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                SUNDAY_ASIA_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      SUNDAY_ASIA_Risk_Type = RISK_LOW; // Risk type

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
input string  SEPARATOR20 = ""; // ----UK----
input double              SUNDAY_UK_Distance     = 0; // Distance
input ENUM_STRATEGY_TYPE  SUNDAY_UK_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  SUNDAY_UK_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                SUNDAY_UK_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      SUNDAY_UK_Risk_Type = RISK_LOW; // Risk type

input string  SEPARATOR21 = ""; // ----US----
input double              SUNDAY_US_Distance     = 0; // Distance
input ENUM_STRATEGY_TYPE  SUNDAY_US_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE  SUNDAY_US_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool                SUNDAY_US_Signal_Reversed = false; // Signal reversed?
input ENUM_RISK_TYPE      SUNDAY_US_Risk_Type = RISK_LOW; // Risk type

input group "==== BACKTEST ===="
input bool    DCA_Strategy_Backtest = false; // DCA Strategy Test
input int     DCA_Strategy_MaxDistance = 4; // Number of L that can be reached
input double  DCA_Strategy_Distance = 50; // Distance between L in points (10 pts=1 pip)
input ENUM_STRATEGY_TYPE DCA_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE DCA_Strategy_Entry_Type = ENTRY_TYPE_RSI_SMOOTHED; // Entry type
input bool    DCA_Strategy_Signal_Reversed = false; // Signal Reversed?
input string  SEPARATOR0 = ""; // ----
enum  ENUM_RISK_LEVEL {LEVEL_0, LEVEL_1, LEVEL_2, LEVEL_3};
input ENUM_RISK_LEVEL Risk_Level = LEVEL_1; // Risk level
input double DCA_Strategy_InitLotSize = 0; // Init lotsize
input bool             Trading_DayOfWeek_Active = false;
input ENUM_DAY_OF_WEEK Trading_DayOfWeek = ""; // Trading day of week
enum  ENUM_SESSION
  {
   SESSION_ASIA, // ASIA
   SESSION_UK, // UK
   SESSION_US // US
  };
input bool         TradingSession_Active = false;
input ENUM_SESSION TradingSession_Type = SESSION_ASIA; // Session type

input group "==== TEST Config ===="
input double  TEST_MaxDrawdown = 1.5; // Maxdrawdown in %
enum  ENUM_TEST_CRITERIA {NET_PROFIT, KELLY};
input ENUM_TEST_CRITERIA TEST_Criteria = NET_PROFIT; // Criteria
input bool    TEST_NewReport = false; // New csv file?
input int     TEST_MaxNumberOfResult = 10; // Max number of result
input bool     TEST_ShowAll = false; // Show all result

input group  "==== News Config ==="
input bool    News_Backtest = false;  // News Backtest?
input int     News_Backtest_Year = 2024;  // News Backtest year

input bool    TradingStopNews_Active = true; // Trading Stop on News
input int     TradingStopNews_FreezeInMinutes = 5; // Freeze in minute

input group "==== Entry time ===="
input bool    TradingEntryTime_Active = false; // Trading Entry time
input int     TradingEntryTime_HourStart = 6; // Entry Hour Start
input int     TradingEntryTime_HourStop = 18; // Entry Hour Stop

input group  "==== Trade time ==="
input bool    TradingGo_Active = false; // Trading Go active (GMT+2(w),+3(s))
input int     TradingGo_HourStart = 6; // Go Hour Start
input int     TradingGo_HourStop = 20; // Go Hour Stop
input bool    TradingGo_CloseAllPositions = true; // Close all positions outside Go Hour

input group  "==== No trade time ==="
input bool    TradingStop_Active = false; // Trading Stop active (GMT+2(w),+3(s))
input int     TradingStop_HourStart = 0; // Stop Hour Start
input int     TradingStop_HourStop = 1; // Stop Hour Stop
input bool    TradingStop_CloseAllPositions = true; // Close all positions before Stop Hour

input group  "==== Session Config ==="
input bool    Session_Active = false; // Session active?

input string  SEPARATOR22 = ""; // ----ASIA----
input int    ASIA_HourStart = 0; // Start hour
input int    ASIA_HourStop = 6; // Stop hour

input string  SEPARATOR23 = ""; // ----UK---
input int    UK_HourStart = 6; // Start hour
input int    UK_HourStop = 19; // Stop hour

input string  SEPARATOR24 = ""; // ----US---
input int    US_HourStart = 19; // Start hour
input int    US_HourStop = 24; // Stop hour

input bool   Session_CloseAllPositions = true; // Close all positions after session

input group  "==== Trading Stop Daily ==="
input bool    TradingStopDaily_Active    = true; // Trading Stop Daily
input double  TradingStopDaily  = 4.7;  // Max daily stoploss (E.g 5%)

input group  "==== Trading Profit Daily ==="
input bool    TradingProfitDaily_Active = false; // Trading Profit Daily
input double  TradingProfitDaily  = 3;  // Max daily takeprofit (E.g 5%)
input bool    TradingStopProfit_Active = false; // Trading Stop Profit
input double  TradingStopProfit  = 0.5;  // Max Stop profit (E.g 0.5%)

input group "==== Holding time ===="
input double POSITION_HOLDING_Factor = 0.1; // Holding factor (Holding time in hour = dist * holding factor)
input bool   POSITION_HOLDING_Stoploss = true; // Set stoploss or close all

input group "==== Timeframe ===="
enum CUSTOM_TIMEFRAMES
  {
   TF_M1   = PERIOD_M1,  // M1
   TF_M5   = PERIOD_M5,  // M5
   TF_M15  = PERIOD_M15, // M15
   TF_H1   = PERIOD_H1,  // H1
   TF_H4   = PERIOD_H4,  // H4
   TF_D1   = PERIOD_D1   // D1
  };
  
input CUSTOM_TIMEFRAMES   Timeframe = TF_M1;
input CUSTOM_TIMEFRAMES   Timeframe_High = TF_M5;

input group  "==== Notification ==="
input bool    Notification_Active = true; // Notification active
input int     Notification_Delay = 3000; // Notification delay
input bool    Warning_Rsi = false; // Rsi Warning?
input bool    Warning_Session = false; // Session Warning?
input bool    Warning_News = false; // News Warning?
input bool    Warning_Cci = false; // CCi Warning?
input bool    Warning_FVG = false; // FVG Warning?

input group  "==== RSI ==="
input int    RSI_Length = 14; // RSI Length
input int    RSI_NumOfCandles = 5; // Number of RSI candles
input int    RSI_LastEntry_NumOfCandles = 5; // Rsi LastEntry Candles

input group  "==== Filter ==="
input int    NextOrderMin = 1; // Next order in minute
input bool   DCA_Strategy_DynamicStopLoss_Active = false; // Dynamic Stoploss (L>3)
input bool   DCA_Strategy_TrailingStopLoss_Active = false; // Trailing Stoploss (L=1)
input double DCA_Strategy_TrailingStopLoss_Step = 0.5; // Trailing Stoploss Step (L=1)
input bool   DCA_NoStoploss = false; // Set no stoploss

input group  "==== DCA Config ==="
enum  ENUM_VOLUME_TYPE
  {
   VOL_MULT, // Multiply
   VOL_FIB // Fibonacci
  };

input ENUM_VOLUME_TYPE DCA_Strategy_Volume_Type = VOL_MULT; // Volume type

input double  DCA_Strategy_LotSize = 0.01; // Lot Size
input double  DCA_Strategy_DeviationMultiplier = 1; // Deviation Multiplier
input double  DCA_Strategy_LastDistance = 40; // Last Stoploss distance
input double  DCA_Strategy_AdditionalTP = 0; // Additional TP
input ulong   DCA_Strategy_MagicNumber = 01; // Magic Number

input group "==== DCA2 Config ===="
input double  DCA_2_Factor = 1.5; // DCA_2 Factor (TP = dist * Factor)
input int     DCA_2_Fraction = 1; // DCA_2 Fraction (Initlotsize / Fraction)
input int     DCA_2_StopLevel = 3; // DCA_2 Stop level

input group "==== DCA3 Config ===="
input double  DCA_3_InitLotsize = 0.1; // DCA_3 Init Lotsize
input double  DCA_3_DeadzoneFactor = 0.0; // DCA_3 Deadzone factor

//input group  "==== Trade Session&News time ==="
//bool    TradingSession_Active = false; // Trading Session time

// input group  "==== Server time ==="
int     Server_Offset = 0; // Server offset

input group  "==== General Stoploss ==="
input double   StopLoss_DefaultPoints          = 500; // Default Stoploss points

//input group  "==== Stoploss Warning ==="
double InitLoss = -100; // Initial Warning Loss (in $)
double LossDistance = 100; // Distance Loss (InitLoss + distance = next warning)

// input group  "==== DCA Analysis ==="
bool   DCA_Matrix = false; // Show DCA Matrix
double DCA_MinNumOfDistance = 1; // Number of min Level
double DCA_MaxNumOfDistance = 10; // Number of max Level
int    DCA_LotSize_Multiply = 2; // Lotsize multiply

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
KhanhdeuxBot      bot;
int               getmeResult;
double            loss;
int               numOfPositions;
KhanhdeuxEvent todayEvents[];
datetime todayDate;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               OnInit()
  {
   Print("=== INIT === ", TimeCurrent());

   loss = InitLoss;
   numOfPositions = PositionsTotal();

   if(ChatID != "" && BotToken != "")
     {
      if(TestTelegram && Notification_Active)
        {
         string message = StringFormat("%s %d (1:%g:%d)", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN), SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE), AccountInfoInteger(ACCOUNT_LEVERAGE));
         SendMessage(message, ChatID, BotToken);
        }

      //--- set token
      bot.Token(BotToken);
      //--- check token
      getmeResult=bot.GetMe();
     }

   EventSetTimer(1);

   trade.SetExpertMagicNumber(DCA_Strategy_MagicNumber);

   bot.isActive = isBotActive;
   bot.isMaster = isBotMaster;
   bot.isSlave  = isBotSlave;

   if(DCA_Matrix)
     {
      showDCAMatrix();
     }

   if(News_Backtest)
     {
      if(!MQLInfoInteger(MQL_TESTER))
        {
         deleteFile("news\\newshistory.bin");
        }
      news.date_from = getFirstDayOfNewsYear();
      news.date_to = getLastDayOfNewsYear();
      news.SaveHistory(true);
      news.LoadHistory(true);
     }
//---
   return(INIT_SUCCEEDED);
  }

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void              OnDeinit(const int reason)
  {
//--- destroy timer
   EventKillTimer();
   bot.signal = 0;
   CRegex::ClearCache();
   loss = InitLoss;
   positionMagics.Clear();
   ArrayFree(todayEvents);
   ArrayResize(todayEvents, 0);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void              OnTick()
  {
   executeEvents();
   htfRsi = getRsi((ENUM_TIMEFRAMES) Timeframe_High);
   if(ChatID != "" && BotToken != "" && Notification_Active)
     {
      SendPositionsChanged();
      // SendMessageToStoplossWarning();
      // SendMessageToAlert();
      // SendMessageToBalanceChange();
     }

   if(bot.closePrice > 0)
     {
      closeAllPositionsAtPrice(bot.closePrice, bot.closeType);
     }

   static datetime barTime=0;
   datetime thisBarTime=iTime(_Symbol,(ENUM_TIMEFRAMES) Timeframe,0);

   if(barTime!=thisBarTime)
     {
      barTime=thisBarTime;

      if(ChatID != "" && BotToken != "" && Notification_Active)
        {
         if(Warning_Rsi)
           {
            //SendMessageToRsi();
            SendMessageCloseToRsi();
            SendMessageToSmoothedRsi();
           }

         if(Warning_Cci)
           {
            SendMessageToCci();
           }

         if(Warning_Session)
           {
            SendMessageToSession();
           }

         if(Warning_News)
           {
            SendMessageToNews();
           }
        }

      //showTodayNews();
      showLiveNews();
      // showSessions();
     }

   static datetime fvgBarTime=0;
   datetime thisFvgBarTime=iTime(_Symbol,(ENUM_TIMEFRAMES)Timeframe_High,0);

   if(fvgBarTime!=thisFvgBarTime)
     {
      fvgBarTime=thisFvgBarTime;

      if(ChatID != "" && BotToken != "" && Notification_Active)
        {
         if(Warning_FVG)
           {
            SendMessageToFVG();
           }
        }
     }

   executeDCATrading();
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
KhanhdeuxConfiguration* getConfiguration(ENUM_DAY_OF_WEEK dayOfWeek = -1)
  {
   dayOfWeek = dayOfWeek == -1 ? getDayOfWeek(): dayOfWeek;

   int rsiOverBought = RSI_Overbought;
   int rsiOversold = RSI_Oversold;

   double distance = DCA_Strategy_Distance;
   double maxDistance = DCA_Strategy_MaxDistance;
   ENUM_STRATEGY_TYPE strategyType = DCA_Strategy_Type;
   ENUM_STRATEGY_ENTRY_TYPE entryType = DCA_Strategy_Entry_Type;
   ENUM_RISK_TYPE riskType = RISK_LOW;
   bool signalReversed = DCA_Strategy_Signal_Reversed;

   bool isAsiaTime = checkTradingTime(ASIA_HourStart, ASIA_HourStop);
   bool isUKTime = checkTradingTime(UK_HourStart, UK_HourStop);
   bool isUStime = checkTradingTime(US_HourStart, US_HourStop);

   switch(dayOfWeek)
     {
      case MONDAY:
         if(isAsiaTime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = MONDAY_ASIA_Distance;
            strategyType = MONDAY_ASIA_Strategy_Type;
            entryType = MONDAY_ASIA_Strategy_Entry_Type;
            riskType = MONDAY_ASIA_Risk_Type;
            signalReversed = MONDAY_ASIA_Signal_Reversed;
           }

         if(isUKTime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = MONDAY_UK_Distance;
            strategyType = MONDAY_UK_Strategy_Type;
            entryType = MONDAY_UK_Strategy_Entry_Type;
            riskType = MONDAY_UK_Risk_Type;
            signalReversed = MONDAY_UK_Signal_Reversed;
           }

         if(isUStime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = MONDAY_US_Distance;
            strategyType = MONDAY_US_Strategy_Type;
            entryType = MONDAY_US_Strategy_Entry_Type;
            riskType = MONDAY_US_Risk_Type;
            signalReversed = MONDAY_US_Signal_Reversed;
           }

         break;
      case TUESDAY:
         if(isAsiaTime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = TUESDAY_ASIA_Distance;
            strategyType = TUESDAY_ASIA_Strategy_Type;
            entryType = TUESDAY_ASIA_Strategy_Entry_Type;
            riskType = TUESDAY_ASIA_Risk_Type;
            signalReversed = TUESDAY_ASIA_Signal_Reversed;
           }

         if(isUKTime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = TUESDAY_UK_Distance;
            strategyType = TUESDAY_UK_Strategy_Type;
            entryType = TUESDAY_UK_Strategy_Entry_Type;
            riskType = TUESDAY_UK_Risk_Type;
            signalReversed = TUESDAY_UK_Signal_Reversed;
           }

         if(isUStime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = TUESDAY_US_Distance;
            strategyType = TUESDAY_US_Strategy_Type;
            entryType = TUESDAY_US_Strategy_Entry_Type;
            riskType = TUESDAY_US_Risk_Type;
            signalReversed = TUESDAY_US_Signal_Reversed;
           }

         break;
      case WEDNESDAY:
         if(isAsiaTime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = WEDNESDAY_ASIA_Distance;
            strategyType = WEDNESDAY_ASIA_Strategy_Type;
            entryType = WEDNESDAY_ASIA_Strategy_Entry_Type;
            riskType = WEDNESDAY_ASIA_Risk_Type;
            signalReversed = WEDNESDAY_ASIA_Signal_Reversed;
           }

         if(isUKTime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = WEDNESDAY_UK_Distance;
            strategyType = WEDNESDAY_UK_Strategy_Type;
            entryType = WEDNESDAY_UK_Strategy_Entry_Type;
            riskType = WEDNESDAY_UK_Risk_Type;
            signalReversed = WEDNESDAY_UK_Signal_Reversed;
           }

         if(isUStime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = WEDNESDAY_US_Distance;
            strategyType = WEDNESDAY_US_Strategy_Type;
            entryType = WEDNESDAY_US_Strategy_Entry_Type;
            riskType = WEDNESDAY_US_Risk_Type;
            signalReversed = WEDNESDAY_US_Signal_Reversed;
           }

         break;
      case THURSDAY:
         if(isAsiaTime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = THURSDAY_ASIA_Distance;
            strategyType = THURSDAY_ASIA_Strategy_Type;
            entryType = THURSDAY_ASIA_Strategy_Entry_Type;
            riskType = THURSDAY_ASIA_Risk_Type;
            signalReversed = THURSDAY_ASIA_Signal_Reversed;
           }

         if(isUKTime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = THURSDAY_UK_Distance;
            strategyType = THURSDAY_UK_Strategy_Type;
            entryType = THURSDAY_UK_Strategy_Entry_Type;
            riskType = THURSDAY_UK_Risk_Type;
            signalReversed = THURSDAY_UK_Signal_Reversed;
           }

         if(isUStime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = THURSDAY_US_Distance;
            strategyType = THURSDAY_US_Strategy_Type;
            entryType = THURSDAY_US_Strategy_Entry_Type;
            riskType = THURSDAY_US_Risk_Type;
            signalReversed = THURSDAY_US_Signal_Reversed;
           }
         break;
      case FRIDAY:
         if(isAsiaTime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = FRIDAY_ASIA_Distance;
            strategyType = FRIDAY_ASIA_Strategy_Type;
            entryType = FRIDAY_ASIA_Strategy_Entry_Type;
            riskType = FRIDAY_ASIA_Risk_Type;
            signalReversed = FRIDAY_ASIA_Signal_Reversed;
           }

         if(isUKTime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = FRIDAY_UK_Distance;
            strategyType = FRIDAY_UK_Strategy_Type;
            entryType = FRIDAY_UK_Strategy_Entry_Type;
            riskType = FRIDAY_UK_Risk_Type;
            signalReversed = FRIDAY_UK_Signal_Reversed;
           }

         if(isUStime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = FRIDAY_US_Distance;
            strategyType = FRIDAY_US_Strategy_Type;
            entryType = FRIDAY_US_Strategy_Entry_Type;
            riskType = FRIDAY_US_Risk_Type;
            signalReversed = FRIDAY_US_Signal_Reversed;
           }

         break;
      case SATURDAY:
         if(isAsiaTime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = SATURDAY_ASIA_Distance;
            strategyType = SATURDAY_ASIA_Strategy_Type;
            entryType = SATURDAY_ASIA_Strategy_Entry_Type;
            riskType = SATURDAY_ASIA_Risk_Type;
            signalReversed = SATURDAY_ASIA_Signal_Reversed;
           }

         if(isUKTime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = SATURDAY_UK_Distance;
            strategyType = SATURDAY_UK_Strategy_Type;
            entryType = SATURDAY_UK_Strategy_Entry_Type;
            riskType = SATURDAY_UK_Risk_Type;
            signalReversed = SATURDAY_UK_Signal_Reversed;
           }

         if(isUStime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = SATURDAY_US_Distance;
            strategyType = SATURDAY_US_Strategy_Type;
            entryType = SATURDAY_US_Strategy_Entry_Type;
            riskType = SATURDAY_US_Risk_Type;
            signalReversed = SATURDAY_US_Signal_Reversed;
           }
         break;
      case SUNDAY:
         if(isAsiaTime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = SUNDAY_ASIA_Distance;
            strategyType = SUNDAY_ASIA_Strategy_Type;
            entryType = SUNDAY_ASIA_Strategy_Entry_Type;
            riskType = SUNDAY_ASIA_Risk_Type;
            signalReversed = SUNDAY_ASIA_Signal_Reversed;
           }

         if(isUKTime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = SUNDAY_UK_Distance;
            strategyType = SUNDAY_UK_Strategy_Type;
            entryType = SUNDAY_UK_Strategy_Entry_Type;
            riskType = SUNDAY_UK_Risk_Type;
            signalReversed = SUNDAY_UK_Signal_Reversed;
           }

         if(isUStime)
           {
            maxDistance = DCA_Strategy_MaxDistance;
            distance = SUNDAY_US_Distance;
            strategyType = SUNDAY_US_Strategy_Type;
            entryType = SUNDAY_US_Strategy_Entry_Type;
            riskType = SUNDAY_US_Risk_Type;
            signalReversed = SUNDAY_US_Signal_Reversed;
           }
         break;
     }

   return new KhanhdeuxConfiguration(distance, maxDistance, strategyType, entryType, riskType, signalReversed);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              testPattern()
  {
   string pattern= signalPattern;
   string in="/buy 0.01,25,4";

   CRegex *r=new CRegex(pattern,RegexOptions::IgnoreCase);
   CMatch *m=r.Match(in);

   if(m.Success())
     {
      CMatchCollection *matches=CRegex::Matches(in,pattern);
      CMatchEnumerator *en=matches.GetEnumerator();
      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         Print("1: " + match.Groups()[1].Value());
         Print("2: " + match.Groups()[2].Value());
         Print("3: " + match.Groups()[3].Value());
         Print("4: " + match.Groups()[4].Value());
         Print("5: " + match.Groups()[5].Value());
         Print("\0");
        }

      delete en;
      delete matches;
     }

   delete r;
   delete m;
  }

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
bool isOnTimerLoaded = false;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              OnTimer()
  {
//executeDCATrading();
   executeTelegramBot();
// testPattern();
   isOnTimerLoaded = true;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getDailyRange()
  {
   MqlRates rate[];
   CopyRates(_Symbol,PERIOD_D1,0,1,rate);
   double high = rate[0].high;
   double low = rate[0].low;

   return high - low;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkPauseAction()
  {
   if(bot.signal == SIGNAL_PAUSE)
     {
      return true;
     }

   return false;
  }

// NEWS AREA
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CNews             news;
int importanceLevel = 2;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkStopNews()
  {
   int offset = TradingStopNews_FreezeInMinutes * 60;

   datetime currentTime = TimeCurrent();

   for(int i=0; i<ArraySize(todayEvents); i++)
     {
      if(getDailyNewsType(todayEvents[i].name)
         && currentTime + offset >= todayEvents[i].time
         && currentTime - offset <= todayEvents[i].time
        )
        {
         // Update stoploss which treats the current L as the last L
         // updateAllStopLossWhenStopNews();
         return true;
        }
     }

   return false;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              setTodayEvents(KhanhdeuxEvent& todayEvents[])
  {
   int count = 0;
   int USD_COUNTRY_ID = 840;

   datetime currentTime = TimeCurrent(); //- 24 * 60 * 60;//- 24 * 60 * 60; // + 16 * 60 * 60; // TimeCurrent()
   datetime currentDate = StringToTime(TimeToString(currentTime, TIME_DATE));
   datetime nextDate = StringToTime(TimeToString(currentTime + 24 * 60 * 60, TIME_DATE));

   if(News_Backtest)
     {
      int totalnews = ArraySize(news.event);
      for(int i=0; i<totalnews; i++)
        {
         if(news.event[i].country_id == USD_COUNTRY_ID
            && news.event[i].importance >= importanceLevel
            // && getDailyNewsType(news.eventname[i])
            && currentDate <= news.event[i].time && news.event[i].time < nextDate)
           {
            for(int i=0; i<ArraySize(todayEvents); i++)
              {
               if(StringCompare(todayEvents[i].name, news.eventname[i]) == 0)
                 {
                  continue;
                 }
              }

            ArrayResize(todayEvents, count + 1);
            datetime eventTime = isSummerTime() ? news.event[i].time : news.event[i].time - 1 * 60 * 60;
            todayEvents[count++] = new KhanhdeuxEvent(news.eventname[i], eventTime, news.event[i].importance);
           }
        }
     }
   else
     {
      MqlCalendarEvent event;    //for saving the events importance and country code
      MqlCalendarValue values[];  //for saving the events time and id

      if(CalendarValueHistory(values,currentDate,nextDate))   //get all the events
        {
         for(int i=0; i<ArraySize(values); i++)
           {
            //get the event  info
            if(CalendarEventById(values[i].event_id,event))
              {
               if(event.country_id == USD_COUNTRY_ID
                  && event.importance >= importanceLevel
                  && getDailyNewsType(event.name))
                 {
                  for(int i=0; i<ArraySize(todayEvents); i++)
                    {
                     if(StringCompare(todayEvents[i].name, event.name) == 0)
                       {
                        continue;
                       }
                    }
                  ArrayResize(todayEvents, count + 1);
                  todayEvents[count++] = new KhanhdeuxEvent(event.name, values[i].time, event.importance);
                 }
              }
           }
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ENUM_NEWS_TYPE getDailyNewsType(string eventName)
  {
   if(StringCompare(eventName, "Core CPI m/m") == 0)
     {
      return CPI;
     }

   if(StringCompare(eventName, "Fed Chair Powell Speech") == 0)
     {
      return SPEECH;
     }

   if(StringCompare(eventName, "Nonfarm Payrolls") == 0
      || StringCompare(eventName, "ADP Nonfarm Employment Change") == 0
     )
     {
      return NONFARM;
     }

   if(StringCompare(eventName, "GDP q/q") == 0)
     {
      return GDP;
     }

   if(StringCompare(eventName, "S&P Global Services PMI") == 0
      || StringCompare(eventName, "ISM Non-Manufacturing PMI") == 0
      || StringCompare(eventName, "ISM Manufacturing PMI") == 0)
     {
      return PMI;
     }

   if(StringCompare(eventName, "Core Durable Goods Orders m/m") == 0)
     {
      return GOODS;
     }

   if(StringCompare(eventName, "Existing Home Sales") == 0
      || StringCompare(eventName, "Pending Home Sales m/m") == 0
     )
     {
      return HOME_SALES;
     }

   if(StringCompare(eventName, "JOLTS Job Openings") == 0)
     {
      return JOLTS;
     }

   if(StringCompare(eventName, "Building Permits") == 0)
     {
      return BUILDING_PERMIT;
     }

   if(StringCompare(eventName, "Retail Sales m/m") == 0)
     {
      return RETAIL_SALES;
     }

   if(StringCompare(eventName, "PPI m/m") == 0)
     {
      return PPI;
     }

   if(StringCompare(eventName, "Fed Interest Rate Decision") == 0
      || StringCompare(eventName, "Fed Chair Powell Testimony") == 0
      || StringCompare(eventName, "Fed Chair Powell Speech") == 0
      || StringCompare(eventName, "FOMC Press Conference") == 0)
     {
      return FED;
     }

   if(StringCompare(eventName, "CB Consumer Confidence Index") == 0)
     {
      return CCI;
     }

   if(StringCompare(eventName, "Michigan Consumer Sentiment") == 0)
     {
      return MCS;
     }

   if(StringCompare(eventName, "Initial Jobless Claims") == 0)
     {
      return JOBLESS_CLAIM;
     }

   if(StringCompare(eventName, "Core PCE Price Index m/m") == 0)
     {
      return PCE;
     }

   if(StringCompare(eventName, "Employment Cost Index q/q") == 0)
     {
      return OTHERS;
     }

   return NULL;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void showTodayNews()
  {
   for(int i=0; i<ArraySize(todayEvents); i++)
     {
      Print("===TODAY-NEWS:", todayEvents[i].name,"(", todayEvents[i].importanceLevel, "|", todayEvents[i].time, ")");
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void showLiveNews()
  {
   MqlDateTime STime;
   datetime time_current=TimeCurrent(STime);
   string dayOfWeek = EnumToString((ENUM_DAY_OF_WEEK)STime.day_of_week);

   datetime currentTime = TimeCurrent();

   for(int i=0; i<ArraySize(todayEvents); i++)
     {
      if(getDailyNewsType(todayEvents[i].name)
         && currentTime + 60 >= todayEvents[i].time
         && currentTime <= todayEvents[i].time)
        {
         Print("===LIVE-NEWS:", todayEvents[i].name,"(", dayOfWeek, "|",todayEvents[i].importanceLevel, "|", todayEvents[i].time, ")");
        }
     }
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime getCurrentNewsTime()
  {
   datetime currentTime = isSummerTime() ? TimeCurrent() - 1 * 60 * 60 : TimeCurrent();
   MqlDateTime STime;
   datetime time_current=TimeCurrent(STime);
   return (currentTime - STime.sec);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void closeAllWhenSessionsExpired()
  {
   ENUM_DCA_ORDER_TYPE types[] = {DCA_BUY, DCA_SELL};
   datetime currentTime = TimeCurrent();

   for(int i=0; i<ArraySize(types); i++)
     {
      datetime tradingTime = getFirstDCATime(types[i]);
      if(tradingTime < getTradingSession(LONDON, true) && currentTime > getTradingSession(NEWYORK, true))
        {
         closeAllDCAPositions(types[i]);
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void showSessions()
  {
   datetime currentTime = getCurrentNewsTime();
   bool sessionStart = false;
   ENUM_SESSION_TYPE types[] = {SYDNEY, TOKYO, LONDON, NEWYORK};

   for(int i=0; i<ArraySize(types); i++)
     {
      if(currentTime == getTradingSession(types[i], true))
        {
         Print("===SESSION:", EnumToString(types[i]));
        }

      if(currentTime == getTradingSession(types[i], false))
        {
         Print("===END-SESSION:", EnumToString(types[i]));
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkStopDaily()
  {
   datetime currentTime = TimeCurrent() + Server_Offset * 60 * 60;
   datetime nextDay = StringToTime(TimeToString(currentTime, TIME_DATE)) + 24 * 60 * 60;
   int timeRange = 20 * 60;

// Print("low:", (nextDay - timeRange), "currentTime:", currentTime, ", high:", (nextDay + timeRange));

   if((nextDay - timeRange) < currentTime && currentTime < nextDay + timeRange)
     {
      return true;
     }

   return false;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              executeTelegramBot()
  {
   if(ChatID != "" && BotToken != "")
     {
      //--- show error message end exit
      if(getmeResult!=0)
        {
         Comment("Error: ",GetErrorDescription(getmeResult));
         return;
        }

      //--- show bot name
      Comment(bot.Name(), ":", DCA_Strategy_MagicNumber);
      //--- reading messages
      bot.GetUpdates();
      //--- processing messages
      bot.ProcessMessages();
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getTotalProfit()
  {
   double profit = 0;
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      ulong ticket = PositionGetTicket(i);
      string positionSymbol=PositionGetSymbol(i);

      if(ticket > 0 && positionSymbol == _Symbol)
        {
         profit += PositionGetDouble(POSITION_PROFIT);
        }
     }

   return profit;
  }

//+------------------------------------------------------------------+
//| Close all positions                                              |
//+------------------------------------------------------------------+
void              closeAllPositions()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(position.SelectByIndex(i))
        {
         if(position.Symbol() == _Symbol)
           {
            trade.PositionClose(position.Ticket());
           }
        }
     }
     
    closeAllPendingOrders(); 
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              closeLastPosition()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
        )
        {
         trade.PositionClose(position.Ticket());
         return;
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              closeAllPositionsAtPrice(double aPrice, int cType)
  {
   bool isClosed = false;

   MqlRates rate[];
   CopyRates(_Symbol,(ENUM_TIMEFRAMES)Timeframe,0,2,rate);

   double currentPrice = rate[1].close;
   double previousPrice = rate[0].close;

   if(previousPrice <= aPrice && aPrice <= currentPrice)
     {
      isClosed = true;
     }

   if(currentPrice <= aPrice && aPrice <= previousPrice)
     {
      isClosed = true;
     }

   if(isClosed)
     {
      closeAllPositions();
      if(cType == CLOSE_KILL)
        {
         algoTradingToggle(false);
         bot.isActive = false;
        }
     }
  }

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              showDCAMatrix()
  {
   double initLotSize = 0.01 * DCA_LotSize_Multiply;

   for(double k=initLotSize; k<=initLotSize * 10; k=k+initLotSize)
     {
      Print("Lot Size:" + DoubleToString(k, 2));

      for(int i = 1; i <= DCA_MaxNumOfDistance; i++)
        {
         // Calculate TotalVolume
         double totalLotSize = calculateTotalVolume(i, k);

         // Calculate MaxLoss
         double maxLoss = calculateMaxLoss(i, k, DCA_Strategy_Distance);

         // Calculate MaxProfit
         double maxProfit = calculateMaxProfit(i, k, DCA_Strategy_Distance);

         if(maxLoss >= getDailyStoploss())
           {
            break;
           }

         if(i < DCA_MinNumOfDistance)
           {
            continue;
           }

         Print(StringFormat("L%d___Total Volume:[%g], Max Loss:[-%g($)], Max Profit:[%g($)]", i, totalLotSize, maxLoss, maxProfit));
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getBestLotSize(double level, double distance, double initCapital)
  {
   double initLotSize = 0.01; //  * DCA_LotSize_Multiply;
   double bestLotSize = 0;

   for(double k=initLotSize; k<=initLotSize * 100; k=k+initLotSize)
     {
      for(int i = 1; i <= level; i++)
        {

         // Calculate MaxLoss
         double maxLoss = calculateMaxLoss(i, k, distance);

         // Calculate MaxProfit
         double maxProfit = calculateMaxProfit(i, k, distance);

         if(maxLoss >= initCapital)
           {
            break;
           }

         if(i < level)
           {
            continue;
           }

         bestLotSize = k;
        }
     }

   return bestLotSize ? NormalizeDouble(bestLotSize, 2) : DCA_Strategy_LotSize;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//KhanhdeuxSetup*            getBestSetup(ENUM_DCA_ORDER_TYPE currentOrderType = NULL)
//  {
//   double initLotSize = 0.01;
//   double lotsize = 0.01;
//
//   int maxDistance = getDCAMaxDistanceByType(currentOrderType); // Fix maxDistance from input at the moment
//   double distance = getDCADistance(); // Fix distance from input at the moment
//
//   double maxLoss = getDailyStoploss() / getDCAMaxDistance() * maxDistance + getDayProfit();
//   double maxProfit = getDailyProfit() / getDCAMaxDistance() * maxDistance - getDayProfit();
//
//   double closest = 9999;
//
//   for(double k=initLotSize; k<=initLotSize * 100; k=k+initLotSize)
//     {
//      // Calculate MaxLoss
//      double loss = calculateMaxLoss(maxDistance, k, distance);
//
//      // Calculate MaxProfit
//      double profit = calculateMaxProfit(maxDistance, k, distance);
//
//      // Calculate distance
//      double dist = MathSqrt((maxLoss - loss) * (maxLoss - loss) + (maxProfit - profit) * (maxProfit - profit));
//
//      if(dist < closest)
//        {
//         lotsize = NormalizeDouble(k, 2);
//         closest = dist;
//        }
//
//     }
//
//// Print("dsl=", getDailyStoploss(), "dayProfit=", getDayProfit(), "ml=", maxLoss, "mp=", maxProfit, "ls=", lotsize, "md=", maxDistance,"time=", TimeCurrent());
//
//   return new KhanhdeuxSetup(lotsize, distance, maxDistance);
//  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getRsiOversold()
  {
   KhanhdeuxConfiguration* config = getConfiguration();
   return config.rsiOversold;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getRsiOverbought()
  {
   KhanhdeuxConfiguration* config = getConfiguration();
   return config.rsiOverbought;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
KhanhdeuxSetup*            getBestSetupByTime()
  {
   double distance = DCA_Strategy_Distance;
   int maxDistance = DCA_Strategy_MaxDistance;

   if(DCA_Strategy_Backtest == false && getDCAStrategyType() != NONE)
     {
      KhanhdeuxConfiguration* config = getConfiguration();
      distance = config.distance;
      maxDistance = config.maxDistance;
     }

   if(DCA_Strategy_InitLotSize > 0)
     {
      return new KhanhdeuxSetup(DCA_Strategy_InitLotSize, distance, maxDistance);
     }

// Get best lotsize
   double contractSize = SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE);

   double initLotSize = 0.01;
   double lotSizeMax = 2;
   double step = 0.02;

   if(contractSize == 10)
     {
      initLotSize = 0.1;
      step = 0.1;
     }

   if(contractSize == 1)
     {
      initLotSize = 1;
      step = 1;
      lotSizeMax = 20;
     }

   double lotsize = initLotSize;
   double maxLoss = getDailyStoploss() + getDayProfit();
   double maxProfit = getDailyProfit() - getDayProfit();

   double closest = 9999;

   for(double k=initLotSize; k<=lotSizeMax; k=k+step)
     {
      // Calculate MaxLoss
      double loss = calculateMaxLoss(maxDistance, k, distance);

      // Calculate MaxProfit
      double profit = calculateMaxProfit(maxDistance, k, distance);

      // Calculate distance
      double dist = MathSqrt((maxLoss - loss) * (maxLoss - loss) + (maxProfit - profit) * (maxProfit - profit));

      if(dist < closest)
        {
         lotsize = NormalizeDouble(k, 2);
         closest = dist;
        }

     }

   return new KhanhdeuxSetup(lotsize, distance, maxDistance);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            calculateTotalVolume(int i, double lotSize)
  {
   double totalLotSize = 0;

   for(int j=1; j<=i; j++)
     {
      totalLotSize += calculateDCALotSize(j, lotSize);
     }

   return totalLotSize;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            calculateMaxLoss(int i, double lotSize, double distance)
  {
   double totalLoss = 0;
   double pips = distance * _Point * 10;

   for(int j=1; j<=i; j++)
     {
      double dist = (j == i) ? DCA_Strategy_LastDistance * _Point * 10 : pips;
      totalLoss += calculateDCALotSize(j, lotSize) * dist * SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE) * (i - j + 1);
     }

   return totalLoss;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            calculateMaxProfit(int i, double lotSize, double distance)
  {
   double totalProfit = 0;
   double pips = distance * _Point * 10;

   for(int j=1; j<=i; j++)
     {
      totalProfit += pips * calculateDCALotSize(j, lotSize) * SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE);
     }

   return totalProfit;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getRequestSignal()
  {
   ENUM_STRATEGY_TYPE strategyType = getDCAStrategyType();

   if(strategyType == DCA)
      return SIGNAL_DCA;
   if(strategyType == HEDG)
      return SIGNAL_HEDG;
   if(strategyType == DCA_2)
      return SIGNAL_DCA_2;
   if(strategyType == DCA_3)
      return SIGNAL_DCA_3;
   if(strategyType == NONE)
      return SIGNAL_NONE;

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void executeEvents()
  {
   datetime currentDate = StringToTime(TimeToString(TimeCurrent(), TIME_DATE));

   if(todayDate != currentDate)
     {
      ArrayResize(todayEvents, 0);
      setTodayEvents(todayEvents);
      todayDate = currentDate;
     }
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              executeDCATrading()
  {
   int requestSignal = getRequestSignal();

   datetime currentDate = StringToTime(TimeToString(TimeCurrent(), TIME_DATE));

   if(TradingStopDaily_Active && checkDailyStoploss())
     {
      if(dailyDate != currentDate)
        {
         Print(StringFormat("Stoploss exeeded = %G", getDailyStoploss()));
         dailyDate = currentDate;
        }

      closeAllPositions();
      return;
     }

   if(TradingStopDaily_Active && checkStopDaily() && !checkPauseAction())
     {
      closeAllPositions();
      return;
     }

   if(TradingStop_Active && checkTradingStopHour() && !checkPauseAction())
     {
      if(TradingStop_CloseAllPositions)
        {
         closeAllPositions();
        }
      return;
     }

   if(TradingProfitDaily_Active && checkDailyProfit())
     {
      closeAllPositions();

      if(dailyDate != currentDate)
        {
         Print(StringFormat("Takeprofit exeeded = %G", getDailyProfit()));
         dailyDate = currentDate;
        }

      return;
     }

   if(TradingStopProfit_Active && checkStopProfit())
     {
      closeAllPositions();
      return;
     }

   if(TradingGo_Active && !checkTradingGoHour() && !checkPauseAction())
     {
      if(TradingGo_CloseAllPositions)
        {
         closeAllPositions();
        }
      return;
     }

   if(TradingStopNews_Active && checkStopNews() && !checkPauseAction())
     {
      closeAllPositions();
      return;
     }

   if(Session_Active && !checkSession())
     {
      if(Session_CloseAllPositions)
        {
         closeAllPositions();
        }
      return;
     }

   if(!checkDayOfWeek())
     {
      return;
     }


// TEMP
   if(!checkTradingSession())
     {
      return;
     }

   if(checkPauseAction())
     {
      return;
     }

   if(getDCADistance() == 0 || getDCAMaxDistance() == 0)
     {
      return;
     }

   if(!bot.isActive)
     {
      return;
     }

   if(requestSignal == SIGNAL_DCA_2)
     {
      executeDCA2Trading();
      return;
     }

   if(requestSignal == SIGNAL_DCA_3)
     {
      executeDCA3Trading();
      return;
     }

   if(getTotalDCAOrders() > 0)
     {
      return;
     }

   ENUM_DCA_ORDER_TYPE orderTypes[];
   setOrderTypesByRequestSignal(orderTypes, requestSignal);

   for(int i=0; i < ArraySize(orderTypes); i++)
     {
      ENUM_DCA_ORDER_TYPE orderType = orderTypes[i];
      int totalPositions = getTotalDCAPositions(orderType);

      // Position holding case
      if(POSITION_HOLDING_Factor > 0
         && totalPositions > 0
         && getLastDCATime(orderType) + POSITION_HOLDING_Factor * getDCADistance() * 60 * 60 <= TimeCurrent())
        {
         if(POSITION_HOLDING_Stoploss)
           {
            double price = getLastDCAPrice(orderType);
            double dist = getDCADistance() * _Point * 10;
            double sl = orderType == DCA_BUY ? price - dist : price + dist;

            bool isStoplossUpdated = isAllDCAPositionStopLossUpdated(sl, orderType);
            bool isStoplossValid = isStoplossValid(sl, orderType);

            if(!isStoplossUpdated && isStoplossValid && orderType == DCA_BUY)
              {
               string message = StringFormat("(HOLD) UPDATE BUY!!! SL = %G", sl);
               printf(message);
               SendMessage(message, ChatID, BotToken);

               updateAllDCAPositionStopLoss(sl, orderType);
              }

            if(!isStoplossUpdated && isStoplossValid && orderType == DCA_SELL)
              {
               string message = StringFormat("(HOLD) UPDATE SELL!!! SL = %G", sl);
               printf(message);
               SendMessage(message, ChatID, BotToken);

               updateAllDCAPositionStopLoss(sl, orderType);
              }
           }
         else
           {
            closeAllDCAPositions(orderType);
           }
        }

      // Update SL to last price when current price crosses last second price
      if(totalPositions > 2 && DCA_Strategy_DynamicStopLoss_Active)
        {
         double currentPrice = getCurrentPrice(orderType);
         double lastPrice = getLastNthDCAPrice(orderType, 1);
         double lastSecondPrice = getLastNthDCAPrice(orderType, 2);
         bool isStoplossUpdated = isAllDCAPositionStopLossUpdated(lastPrice, orderType);

         if(!isStoplossUpdated && orderType == DCA_BUY && currentPrice > lastSecondPrice)
           {
            string message = StringFormat("UPDATE BUY!!! SL = %G", lastPrice);
            printf(message);
            SendMessage(message, ChatID, BotToken);

            updateAllDCAPositionStopLoss(lastPrice, orderType);
           }

         if(!isStoplossUpdated && orderType == DCA_SELL && currentPrice < lastSecondPrice)
           {
            string message = StringFormat("UPDATE SELL!!! SL = %G", lastPrice);
            printf(message);
            SendMessage(message, ChatID, BotToken);

            updateAllDCAPositionStopLoss(lastPrice, orderType);
           }
        }

      if(totalPositions == 1 && DCA_Strategy_TrailingStopLoss_Active)
        {
         double lotSize =  getLastDCALotSize(orderType);
         double price = getLastDCAPrice(orderType);
         double sl = getLastDCAStoploss(orderType);

         //double tp = calculateDCATakeProfit(lotSize, price, orderType);
         double currentPrice = getCurrentPrice(orderType);
         double dist = getDCADistance() * _Point * 10;

         int step = dist * DCA_Strategy_TrailingStopLoss_Step;

         if(orderType == DCA_BUY && step > 0)
           {
            bool isSLUpdated = false;

            if(currentPrice >= (price + dist))
              {
               if(sl < price)
                 {
                  sl = price + step;
                  isSLUpdated = true;
                 }
               else
                  if(currentPrice - sl >= dist)
                    {
                     sl = sl + step;
                     isSLUpdated = true;
                    }
              }

            if(isSLUpdated)
              {
               string message = StringFormat("UPDATE BUY!!! SL = %G", sl);
               printf(message);
               SendMessage(message, ChatID, BotToken);

               updateAllDCAPositionStopLoss(sl, orderType);
              }
           }

         if(orderType == DCA_SELL && step > 0)
           {
            bool isSLUpdated = false;

            if(currentPrice <= (price - dist))
              {
               if(sl > price)
                 {
                  sl = price - step;
                  isSLUpdated = true;
                 }
               else
                  if(sl - currentPrice >= dist)
                    {
                     sl = sl - step;
                     isSLUpdated = true;
                    }
              }

            if(isSLUpdated)
              {
               string message = StringFormat("UPDATE SELL!!! SL = %G", sl);
               printf(message);
               SendMessage(message, ChatID, BotToken);

               updateAllDCAPositionStopLoss(sl, orderType);
              }
           }
        }

      if(totalPositions == getDCAMaxDistanceByType(orderType) - 1 && getNextDCASignal(orderType))
        {
         if(!DCA_NoStoploss)
           {
            closeAllDCAPositions(orderType);
           }

         return;
        }


      if(totalPositions == getDCAMaxDistanceByType(orderType))
        {
         return;
        }

      int signal = 0;
      if(totalPositions == 0)
        {
         signal = orderType == DCA_BUY ? SIGNAL_BUY : SIGNAL_SELL;
        }
      else
        {
         signal = getNextDCASignal(orderType);
        }

      if(signal)
        {

         // Close all when the last position time is too short
         if(NextOrderMin > 0 && getLastDCATime(orderType) + NextOrderMin * 60 >= TimeCurrent())
           {
            Print("=== STOPOUT - Next Position in short time ===");
            closeAllDCAPositions(orderType);
            return;
           }

         int newTotalPositions = totalPositions + 1;

         double lotSize =  calculateDCALotSize(newTotalPositions, getDCALotSize());
         double price = getCurrentPrice(orderType);
         double tp = calculateDCATakeProfit(lotSize, price, orderType);
         double sl = calculateDCAStoploss(price, orderType);

         if(newTotalPositions == 1 && DCA_Strategy_TrailingStopLoss_Active)
           {
            tp = 0;
           }

         if(signal == SIGNAL_BUY && checkValidLotSize(lotSize, orderType))
           {
            updateAllDCAPositionTakeProfitAndStopLoss(orderType, tp, sl);
            string comment = BUY_STRING + ":" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions;

            Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, price, sl, tp, comment));
            trade.Buy(lotSize, _Symbol, price, sl, tp, comment);
           }

         if(signal == SIGNAL_SELL && checkValidLotSize(lotSize, orderType))
           {
            updateAllDCAPositionTakeProfitAndStopLoss(orderType, tp, sl);
            string comment = SELL_STRING + ":" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions;

            Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, price, sl, tp, comment));
            trade.Sell(lotSize, _Symbol, price, sl, tp, comment);
           }
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void executeDCA2Trading()
  {
   int signal = 0;
   int totalPositions = getTotalDCAPositions(DCA_BUY) + getTotalDCAPositions(DCA_SELL);

   if(totalPositions == 0)
     {
      signal = getEntrySignal();
     }
   else
     {
      ENUM_DCA_ORDER_TYPE lastOrderType = getLastDCAOrderType();

      if(getNextDCASignal(lastOrderType))
        {
         if(lastOrderType == DCA_BUY)
           {
            signal = SIGNAL_SELL;
           }
         if(lastOrderType == DCA_SELL)
           {
            signal = SIGNAL_BUY;
           }
        }
     }

   if(signal)
     {
      if(totalPositions == DCA_2_StopLevel)
        {
         if(!DCA_NoStoploss)
           {
            closeAllPositions();
           }
         return;
        }

      int newTotalPositions = totalPositions + 1;
      double initLotSize = NormalizeDouble(getDCALotSize() / DCA_2_Fraction,_Digits);
      double lotSize =  calculateDCALotSize(newTotalPositions, initLotSize);

      ENUM_DCA_ORDER_TYPE orderType = signal == SIGNAL_BUY ? DCA_BUY : DCA_SELL;
      double price = getCurrentPrice(orderType);
      double dist = getDCADistance() * _Point * 10 * DCA_2_Factor;

      double tp = 0;
      double sl = 0;

      if(!checkValidLotSize(lotSize, orderType))
        {
         if(!DCA_NoStoploss)
           {
            closeAllPositions();
           }
         return;
        }

      if(signal == SIGNAL_BUY)
        {
         tp = price + dist;
         sl = price - StopLoss_DefaultPoints * _Point * 10;

         updateAllDCAPositionStopLoss(tp, DCA_SELL);
         string comment = BUY_STRING + ":" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions;

         Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, price, sl, tp, comment));
         trade.Buy(lotSize, _Symbol, price, sl, tp, comment);
        }

      if(signal == SIGNAL_SELL)
        {
         tp = price - dist;
         sl = price + StopLoss_DefaultPoints * _Point * 10;

         updateAllDCAPositionStopLoss(tp, DCA_BUY);
         string comment = SELL_STRING + ":" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions;

         Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, price, sl, tp, comment));
         trade.Sell(lotSize, _Symbol, price, sl, tp, comment);
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void executeDCA3Trading()
  {

   ENUM_DCA_ORDER_TYPE types[] = {DCA_BUY, DCA_SELL};

   for(int i=0; i<ArraySize(types); i++)
     {
      ENUM_DCA_ORDER_TYPE orderType = types[i];
      int totalPositions = getTotalDCAPositions(orderType);
      int totalHedgingPositions = getTotalHedgingPositions(orderType);
      
      ENUM_DCA_ORDER_TYPE rOrderType = orderType == DCA_BUY ? DCA_SELL : DCA_BUY;
      int totalOrders = getTotalDCAOrders(rOrderType);

      if(totalOrders > 0)
        {
         if(totalPositions == 0)
           {
            closeAllPendingOrders(rOrderType);
           }
         return;
        }

      if(totalPositions == 0 && totalHedgingPositions > 0)
        {
         closeAllDCAPositions(orderType);
         return;
        }

      if(totalPositions > 0 && totalHedgingPositions == 0)
        {
         double lastProfit = getLastDCATakeProfit(orderType);
         double currentBuyPrice = getCurrentPrice(DCA_BUY);
         double currentSellPrice = getCurrentPrice(DCA_SELL);
         
         bool shouldBeClosed = orderType == DCA_BUY ? MathMax(currentBuyPrice,currentSellPrice) >= lastProfit : MathMin(currentBuyPrice,currentSellPrice) <= lastProfit;

         if(shouldBeClosed)
           {
            closeAllDCAPositions(orderType);
            closeAllPendingOrders(rOrderType);
            return;
           }
        }
     }

   ENUM_DCA_ORDER_TYPE orderTypes[];
   setOrderTypesByRequestSignal(orderTypes, SIGNAL_DCA_3);

   for(int i=0; i < ArraySize(orderTypes); i++)
     {
      ENUM_DCA_ORDER_TYPE orderType = orderTypes[i];
      int totalPositions = getTotalDCAPositions(orderType);
      int totalHedgingPositions = getTotalHedgingPositions(orderType);
      
      ENUM_DCA_ORDER_TYPE rOrderType = orderType == DCA_BUY ? DCA_SELL : DCA_BUY;
      // int totalOrders = getTotalDCAOrders(rOrderType);

      //if(totalOrders > 0)
      //  {
      //   if(totalPositions == 0)
      //     {
      //      closeAllPendingOrders(rOrderType);
      //     }
      //   return;
      //  }

      //if(totalPositions == 0 && totalHedgingPositions > 0)
      //  {
      //   closeAllPositions();
      //   return;
      //  }

//      if(totalPositions > 0 && totalHedgingPositions == 0)
//        {
//         double lastProfit = getLastDCATakeProfit(orderType);
//         double currentBuyPrice = getCurrentPrice(DCA_BUY);
//         double currentSellPrice = getCurrentPrice(DCA_BUY);
//
//         bool shouldBeClosed = orderType == DCA_BUY ? MathMin(currentBuyPrice,currentSellPrice) >= lastProfit : MathMax(currentBuyPrice,currentSellPrice) <= lastProfit;
//
//         if(shouldBeClosed)
//           {
//            closeAllPositions();
//            closeAllPendingOrders(rOrderType);
//            return;
//           }
//        }

      int signal = 0;
      if(totalPositions == 0)
        {
         signal = orderType == DCA_BUY ? SIGNAL_BUY : SIGNAL_SELL;
        }
      else
        {
         signal = getNextDCASignal(orderType);
        }

      if(signal)
        {
         // Close all when the last position time is too short
         if(NextOrderMin > 0 && getLastDCATime(orderType) + NextOrderMin * 60 >= TimeCurrent())
           {
            Print("=== STOPOUT - Next Position in short time ===");
            closeAllDCAPositions(orderType);
            return;
           }

         int newTotalPositions = totalPositions + 1;

         double lotSize =  calculateDCALotSize(newTotalPositions, DCA_3_InitLotsize);
         double price = getCurrentPrice(orderType);
         double tp = calculateDCATakeProfit(lotSize, price, orderType);
         double sl = calculateDCAStoploss(price, orderType);

         if(signal == SIGNAL_BUY && checkValidLotSize(lotSize, orderType))
           {
            updateAllDCAPositionTakeProfitAndStopLoss(orderType, tp, sl);
            string comment = BUY_STRING + ":" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions;

            Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, price, sl, tp, comment));
            trade.Buy(lotSize, _Symbol, price, sl, tp, comment);
           }

         if(signal == SIGNAL_SELL && checkValidLotSize(lotSize, orderType))
           {
            updateAllDCAPositionTakeProfitAndStopLoss(orderType, tp, sl);
            string comment = SELL_STRING + ":" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions;

            Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, price, sl, tp, comment));
            trade.Sell(lotSize, _Symbol, price, sl, tp, comment);
           }

         ENUM_DCA_ORDER_TYPE rOrderType = (signal == SIGNAL_BUY) ? DCA_SELL : DCA_BUY;
         double rPrice = getCurrentPrice(rOrderType);
         double dist = getDCADistance() * _Point * 10;
         double rSl = tp;

         int hedgeOffset = DCA_3_DeadzoneFactor * getDCADistance() * _Point * 10;

         if(signal == SIGNAL_SELL && checkValidLotSize(lotSize, rOrderType))
           {
            closeAllHedgPositions();
            rPrice += hedgeOffset;

            double rTp = rPrice + dist;
            string comment = HEDG_STRING + "-" + SELL_STRING  + ":" + DCA_Strategy_MagicNumber +  "|R" + newTotalPositions;

            Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, rPrice, rSl, rTp, comment));
            // trade.Buy(lotSize, _Symbol, rPrice, rSl, rTp, comment);

            if(hedgeOffset > 0)
              {
               trade.BuyStop(lotSize, rPrice, _Symbol, rSl, rTp, 0, 0, comment);
              }

            if(hedgeOffset < 0)
              {
               trade.BuyLimit(lotSize, rPrice, _Symbol, rSl, rTp, 0, 0, comment);
              }

            if(hedgeOffset == 0)
              {
               trade.Buy(lotSize, _Symbol, rPrice, rSl, rTp, comment);
              }
           }

         if(signal == SIGNAL_BUY && checkValidLotSize(lotSize, rOrderType))
           {
            closeAllHedgPositions();
            rPrice -= hedgeOffset;

            double rTp = rPrice - dist;
            string comment = HEDG_STRING + "-" + BUY_STRING  + ":" + DCA_Strategy_MagicNumber +  "|R" + newTotalPositions;

            Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, rPrice, rSl, rTp, comment));
            // trade.Sell(lotSize, _Symbol, rPrice, rSl, rTp, comment);

            if(hedgeOffset > 0)
              {
               trade.SellStop(lotSize, rPrice, _Symbol, rSl, rTp, 0, 0, comment);
              }

            if(hedgeOffset < 0)
              {
               trade.SellLimit(lotSize, rPrice, _Symbol, rSl, rTp, 0, 0, comment);
              }

            if(hedgeOffset == 0)
              {
               trade.Sell(lotSize, _Symbol, rPrice, rSl, rTp, comment);
              }
           }
        }
     }

   ArrayResize(orderTypes, 0);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool checkValidLotSize(double lotSize, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
// Retrieve account information
   double freeMargin = AccountInfoDouble(ACCOUNT_MARGIN_FREE);

   double price = getCurrentPrice(currentOrderType);
   ENUM_ORDER_TYPE orderType = currentOrderType == DCA_BUY ? ORDER_TYPE_BUY : ORDER_TYPE_SELL;

   double marginRequired;
   OrderCalcMargin(orderType, _Symbol, lotSize, price, marginRequired);

// Check if free margin is sufficient
   return freeMargin >= marginRequired;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getEntrySignal()
  {
   int entrySignal = 0;
   ENUM_STRATEGY_ENTRY_TYPE entryType = getEntryType();

   if(TradingEntryTime_Active && !checkTradingEntryHour())
     {
      return entrySignal;
     }

   if(entryType == ENTRY_TYPE_RSI)
     {
      int rsiSignal = getSmoothedRsiSignal(RSI_NumOfCandles);

      if(checkTradingAreaSignal(rsiSignal))
        {
         entrySignal = rsiSignal;
        }
     }

   if(entryType == ENTRY_TYPE_RSI_SMOOTHED)
     {
      entrySignal = getSmoothedAndHtfRsiSignal(RSI_NumOfCandles);
     }

   if(entryType == ENTRY_TYPE_FVG)
     {
      entrySignal = getFVGSignal();
     }

   if(entryType == ENTRY_TYPE_LAST_DAY_PRICE)
     {
      entrySignal = getLastDayPriceSignal();
     }

   if(isSignalReversed() && entrySignal != 0)
     {
      entrySignal = (entrySignal == SIGNAL_BUY) ? SIGNAL_SELL : SIGNAL_BUY;
     }

   if(entrySignal != 0
      && bot.isMaster && ChatID != "" && BotToken != "")
     {
      datetime currentBarTime = iTime(_Symbol, (ENUM_TIMEFRAMES) Timeframe, 0);

      if(currentBarTime != lastBarTime)
        {
         string message = entrySignal == SIGNAL_BUY ? COMMAND_MASTER_BUY : COMMAND_MASTER_SELL;
         printf(message);
         SendMessage(message, ChatID, BotToken);

         lastBarTime = currentBarTime;
        }
     }

   if(bot.isSlave)
     {
      entrySignal = 0;
     }

   if(bot.signal != 0)
     {
      int signal = bot.signal;
      bot.signal = 0;
      return signal;
     }

   return entrySignal;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              setOrderTypesByRequestSignal(ENUM_DCA_ORDER_TYPE& orderTypes[], int requestSignal)
  {

   if(requestSignal == SIGNAL_HEDG || requestSignal == SIGNAL_DCA || requestSignal == SIGNAL_DCA_3)
     {
      int buyTotal = getTotalDCAPositions(DCA_BUY);
      int sellTotal = getTotalDCAPositions(DCA_SELL);

      int entrySignal = getEntrySignal();
      int count = 0;

      if(requestSignal == SIGNAL_DCA || requestSignal == SIGNAL_DCA_3)
        {
         if((entrySignal == SIGNAL_BUY && buyTotal == 0 && sellTotal == 0) || buyTotal > 0)
           {
            ArrayResize(orderTypes, count + 1);
            orderTypes[count++] = DCA_BUY;
           }
         else
            if((entrySignal == SIGNAL_SELL && sellTotal == 0 && buyTotal == 0) || sellTotal > 0)
              {
               ArrayResize(orderTypes, count + 1);
               orderTypes[count++] = DCA_SELL;
              }
        }

      if(requestSignal == SIGNAL_HEDG)
        {
         if(buyTotal == 0 && sellTotal == 0)
           {
            if(entrySignal == SIGNAL_BUY)
              {
               ArrayResize(orderTypes, count + 1);
               orderTypes[count++] = DCA_BUY;
              }

            if(entrySignal == SIGNAL_SELL)
              {
               ArrayResize(orderTypes, count + 1);
               orderTypes[count++] = DCA_SELL;
              }
           }
         else
            if(buyTotal > 0 && sellTotal > 0)
              {
               ArrayResize(orderTypes, count + 1);
               orderTypes[count++] = DCA_BUY;
               ArrayResize(orderTypes, count + 1);
               orderTypes[count++] = DCA_SELL;
              }
            else
               if(buyTotal > 0)
                 {
                  ArrayResize(orderTypes, count + 1);
                  orderTypes[count++] = DCA_BUY;

                  if(entrySignal == SIGNAL_SELL)
                    {
                     ArrayResize(orderTypes, count + 1);
                     orderTypes[count++] = DCA_SELL;
                    }
                 }
               else
                  if(sellTotal > 0)
                    {
                     ArrayResize(orderTypes, count + 1);
                     orderTypes[count++] = DCA_SELL;

                     if(entrySignal == SIGNAL_BUY)
                       {
                        ArrayResize(orderTypes, count + 1);
                        orderTypes[count++] = DCA_BUY;
                       }
                    }
        }
     }

   if(requestSignal == SIGNAL_BUY || requestSignal == SIGNAL_SELL || requestSignal == SIGNAL_NONE)
     {
      int count = 0;
      int buyTotal = getTotalDCAPositions(DCA_BUY);
      int sellTotal = getTotalDCAPositions(DCA_SELL);

      if(requestSignal == SIGNAL_BUY || buyTotal > 0)
        {
         ArrayResize(orderTypes, count + 1);
         orderTypes[count++] = DCA_BUY;
        }

      if(requestSignal == SIGNAL_SELL || sellTotal > 0)
        {
         ArrayResize(orderTypes, count + 1);
         orderTypes[count++] = DCA_SELL;
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getTotalDCAPositions(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   int count = 0;
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         count++;
        }
     }

   return count;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getTotalHedgingPositions(ENUM_DCA_ORDER_TYPE currentOrderType = NULL)
  {
   int count = 0;
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && isHedgPosition(position.Comment(), currentOrderType)
        )
        {
         count++;
        }
     }

   return count;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getTotalDCAOrders(ENUM_DCA_ORDER_TYPE currentOrderType = NULL)
  {
   int count = 0;
   for(int i = OrdersTotal() - 1; i >= 0; i--)
     {
      if(OrderSelect(OrderGetTicket(i)))
        {
         string symbol = OrderGetString(ORDER_SYMBOL);
         long magicNumber = OrderGetInteger(ORDER_MAGIC);
         ENUM_ORDER_TYPE orderType  = ENUM_ORDER_TYPE(OrderGetInteger(ORDER_TYPE));

         if(symbol == _Symbol && magicNumber == DCA_Strategy_MagicNumber)
           {
            if(currentOrderType)
              {
               if(currentOrderType == DCA_BUY && (orderType == ORDER_TYPE_BUY_LIMIT || orderType == ORDER_TYPE_BUY_STOP))
                 {
                  count++;
                 }

               if(currentOrderType == DCA_SELL && (orderType == ORDER_TYPE_SELL_LIMIT || orderType == ORDER_TYPE_SELL_STOP))
                 {
                  count++;
                 }
              }
            else
              {
               count++;
              }
           }
        }
     }

   return count;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void closeAllPendingOrders(ENUM_DCA_ORDER_TYPE currentOrderType = NULL)
  {
   for(int i = OrdersTotal() - 1; i >= 0; i--)
     {
      if(OrderSelect(OrderGetTicket(i)))
        {
         string symbol = OrderGetString(ORDER_SYMBOL);
         long magicNumber = OrderGetInteger(ORDER_MAGIC);
         ENUM_ORDER_TYPE orderType  = ENUM_ORDER_TYPE(OrderGetInteger(ORDER_TYPE));

         if(symbol == _Symbol && magicNumber == DCA_Strategy_MagicNumber)
           {
            if(currentOrderType == DCA_BUY && (orderType == ORDER_TYPE_BUY_LIMIT || orderType == ORDER_TYPE_BUY_STOP))
              {
               trade.OrderDelete(OrderGetTicket(i));
              }

            if(currentOrderType == DCA_SELL && (orderType == ORDER_TYPE_SELL_LIMIT || orderType == ORDER_TYPE_SELL_STOP))
              {
               trade.OrderDelete(OrderGetTicket(i));
              }
              
            if(currentOrderType == NULL)
              {
               trade.OrderDelete(OrderGetTicket(i));
              }  
           }
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getRsiSignal(int numOfCandles)
  {
   double rsi[];
   int rsiHandle = iRSI(_Symbol, (ENUM_TIMEFRAMES) Timeframe, RSI_Length, PRICE_CLOSE);
   int oversold = getRsiOversold();
   int overbought = getRsiOverbought();
   int signal = 0;

   CopyBuffer(rsiHandle, 0, 1, numOfCandles, rsi);

   int buyCount = 0;
   int sellCount = 0;

   for(int i=0; i< numOfCandles - 1; i++)
     {
      if(rsi[i] < oversold)
        {
         buyCount++;
        }

      if(rsi[i] > overbought)
        {
         sellCount++;
        }
     }

   if(buyCount == numOfCandles - 1 && rsi[numOfCandles - 1] >= oversold)
     {
      signal = SIGNAL_BUY;
     }

   if(sellCount == numOfCandles - 1 && rsi[numOfCandles - 1] <= overbought)
     {
      signal = SIGNAL_SELL;
     }

   ArrayFree(rsi);
   return signal;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getSmoothedRsiSignal(int numOfCandles)
  {
   int signal = 0;

   double rsi[];
   int rsiHandle = iRSI(_Symbol, (ENUM_TIMEFRAMES)Timeframe, RSI_Length, PRICE_CLOSE);
   int oversold = getRsiOversold();
   int overbought = getRsiOverbought();

   int MA_Length = numOfCandles;

   CopyBuffer(rsiHandle, 0, 1, RSI_Length, rsi);

   int maHandle = iMA(_Symbol, (ENUM_TIMEFRAMES) Timeframe, MA_Length, 0, MODE_SMA, rsiHandle);
   double ma[];
   CopyBuffer(maHandle, 0, 1, MA_Length, ma);


   if(ma[MA_Length - 2] < oversold && ma[MA_Length - 1] >= oversold)
     {
      signal = SIGNAL_BUY;
     }

   if(ma[MA_Length - 2] > overbought && ma[MA_Length - 1] <= overbought)
     {
      signal = SIGNAL_SELL;
     }

   return signal;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getHtfRsiSignal()
  {
   int signal = 0;
   int lowBuy = 0;
   int highSell = 0;

   if(Risk_Level == LEVEL_1)
     {
      lowBuy = RSI_M5_BUY_LEVEL1;
      highSell = RSI_M5_SELL_LEVEL1;
     }

   if(Risk_Level == LEVEL_2)
     {
      lowBuy = RSI_M5_BUY_LEVEL2;
      highSell = RSI_M5_SELL_LEVEL2;
     }

   if(Risk_Level == LEVEL_3)
     {
      lowBuy = RSI_M5_BUY_LEVEL3;
      highSell = RSI_M5_SELL_LEVEL3;
     }

   if(htfRsi < lowBuy)
     {
      signal = SIGNAL_BUY;
     }

   if(htfRsi > highSell)
     {
      signal = SIGNAL_SELL;
     }

   return signal;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getSmoothedAndHtfRsiSignal(int numOfCandles)
  {
   int signal = getSmoothedRsiSignal(numOfCandles);
   int htfSignal = getHtfRsiSignal();

   if(Risk_Level == LEVEL_0)
     {
      return signal;
     }

   return signal == htfSignal ? signal : 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getFVGSignal()
  {
   int signal = 0;
   int numOfCandles = 4;

   MqlRates rate[];
   CopyRates(_Symbol,(ENUM_TIMEFRAMES) Timeframe_High, 1, numOfCandles, rate);

// Check all candles have the same color
   int greenCount = 0;
   int redCount = 0;

   for(int i= numOfCandles - 1; i>=0; i--)
     {
      if(rate[i].close >= rate[i].open)
        {
         greenCount++;
        }

      if(rate[i].close <= rate[i].open)
        {
         redCount++;
        }
     }

   bool buyCountCondition = (redCount == numOfCandles);
   bool sellCountCondition = (greenCount == numOfCandles);


// Check FVG
   bool buyFVGCondition = true;
   bool sellFVGCondition = true;

   for(int i= numOfCandles - 1; i>=2; i--)
     {
      int j = i - 2;
      if(rate[i].high > rate[j].low)
        {
         buyFVGCondition = false;
        }

      if(rate[i].low < rate[j].high)
        {
         sellFVGCondition = false;
        }
     }

   if(buyCountCondition && buyFVGCondition)
     {
      signal = SIGNAL_BUY;
     }

   if(sellCountCondition && sellFVGCondition)
     {
      signal = SIGNAL_SELL;
     }

   return signal;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               checkTradingAreaSignal(int signal)
  {
   if(signal == 0)
     {
      return false;
     }

   return true;

   int overlapThreshold = 80; // percent

// Get last day highest/lowest prices

   MqlRates rateD[];
   CopyRates(_Symbol,PERIOD_D1,1,1,rateD);
//Print(rateD[0].time + ": O=" + rateD[0].open + " H=" + rateD[0].high + " L=" + rateD[0].low + " C=" + rateD[0].close);
   double lastHigh = rateD[0].high;
   double lastLow = rateD[0].low;

   double distanceArea = getDCADistance() * getDCAMaxDistance() * _Point * 10;
//Print("DistanceArea:", distanceArea, "_Point:", _Point, ",", getDCADistance(), ", :" , getDCAMaxDistance());

// Calculate the area distance
   double sellUpper = lastHigh;
   double sellLower = lastHigh - distanceArea;

   double buyUpper = lastLow + distanceArea;
   double buyLower = lastLow;

// Check if upper area overlaps lower area
   double overlapPercent = sellLower >= buyUpper ? 0 : ((buyUpper - sellLower) / (sellUpper - buyLower) * 100);
//Print(overlapPercent);
   bool isOverlapValid = overlapPercent <= overlapThreshold;



// Check if price is in area
   double currentPrice = NormalizeDouble(SymbolInfoDouble(_Symbol, SYMBOL_LAST),_Digits);
   bool isInBuy = buyLower <= currentPrice && currentPrice <= buyUpper;
   bool isInSell = sellLower <= currentPrice && currentPrice <= sellUpper;

   if(!isOverlapValid)
     {
      return false;
     }
   else
     {
      return true;
     }

   if(isInBuy && signal == SIGNAL_BUY)
     {
      return true;
     }

   if(isInSell && signal == SIGNAL_SELL)
     {
      return true;
     }

   return false;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getLastDayPriceSignal()
  {
   int signal = 0;

   MqlRates rate[];
   CopyRates(_Symbol,PERIOD_D1,1,1,rate);
   double lastDayPrice = rate[0].close;

   if(getCurrentPrice(DCA_SELL) >= lastDayPrice + getDCADistance() * _Point * 10)
     {
      signal = SIGNAL_SELL;
     }

   if(getCurrentPrice(DCA_BUY) <= lastDayPrice - getDCADistance() * _Point * 10)
     {
      signal = SIGNAL_BUY;
     }

   return signal;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getRsi(ENUM_TIMEFRAMES timeframe)
  {
   double rsi[];
   int rsiHandle = iRSI(_Symbol, timeframe, 14, PRICE_CLOSE);
   CopyBuffer(rsiHandle, 0, 0, 1, rsi);

   double rsiVal = rsi[0];

   ArrayFree(rsi);
   return rsiVal;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getNextDCASignal(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   int signal = 0;
   double lastPrice = getLastDCAPrice(currentOrderType);
   double pips = getDCADistance() * _Point * 10;
   double currentPrice = getCurrentPrice(currentOrderType);
   int totalPositions = getTotalDCAPositions(currentOrderType);
   int maxDistance = getDCAMaxDistanceByType(currentOrderType);
   int lastEnTryNumOfCandles = RSI_LastEntry_NumOfCandles;

   if(totalPositions == maxDistance)
     {
      pips = (DCA_Strategy_LastDistance * _Point * 10);
     }
   else
     {
      pips = pips * DCA_Strategy_DeviationMultiplier;
     }

   if(currentOrderType == DCA_BUY)
     {
      if(currentPrice <= (lastPrice - pips))
        {
         signal = SIGNAL_BUY;
        }
     }

   if(currentOrderType == DCA_SELL)
     {
      if(currentPrice >= (lastPrice + pips))
        {
         signal = SIGNAL_SELL;
        }
     }

   return signal;
  }

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            calculateDCATakeProfit(double lotSize, double price, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   double priceVolumeSum = 0;
   double totalVolume = 0.0;
   double pips = getDCADistance() * _Point * 10;

   if(DCA_Strategy_AdditionalTP > 0)
     {
      return currentOrderType == DCA_BUY ? (price + pips + DCA_Strategy_AdditionalTP * _Point * 10) : (price - pips - DCA_Strategy_AdditionalTP * _Point * 10);
     }

   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
        )
        {
         priceVolumeSum += currentOrderType == DCA_BUY ? (position.PriceOpen() + pips) * position.Volume() : (position.PriceOpen() - pips) * position.Volume();
         totalVolume += position.Volume();
        }
     }

   priceVolumeSum += currentOrderType == DCA_BUY ? (price + pips) * lotSize : (price - pips) * lotSize;
   totalVolume += lotSize;

   return priceVolumeSum / totalVolume;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            calculateDCAStoploss(double price, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   double sl = 0;
   double slDistance = StopLoss_DefaultPoints * _Point * 10;
   int totalPositions = getTotalDCAPositions(currentOrderType);
   int maxDistance = getDCAMaxDistanceByType(currentOrderType);

   if(totalPositions + 1 == maxDistance)
     {
      slDistance = DCA_Strategy_LastDistance * _Point * 10; // (DCA_Strategy_LastDistance/10);
     }

   if(currentOrderType == DCA_BUY)
     {
      sl = NormalizeDouble(price - slDistance,_Digits);
     }

   if(currentOrderType == DCA_SELL)
     {
      sl = NormalizeDouble(price + slDistance,_Digits);
     }

   return sl;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            calculateDCALotSize(int i, double lotSize)
  {
   if(DCA_Strategy_Volume_Type == VOL_FIB)
     {
      if(i <= 1)
        {
         return lotSize;
        }

      return calculateDCALotSize(i - 1, lotSize) + calculateDCALotSize(i - 2, lotSize);
     }

   if(i == 1)
      return lotSize;
   return calculateDCALotSize(i - 1, lotSize) * 2;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getCurrentPrice(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   double currentPrice = NormalizeDouble(SymbolInfoDouble(_Symbol, SYMBOL_LAST),_Digits);

   if(currentOrderType == DCA_BUY)
     {
      currentPrice = NormalizeDouble(SymbolInfoDouble(_Symbol, SYMBOL_ASK),_Digits);
     }
   else
      if(currentOrderType == DCA_SELL)
        {
         currentPrice = NormalizeDouble(SymbolInfoDouble(_Symbol, SYMBOL_BID),_Digits);
        }

   return currentPrice;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ENUM_STRATEGY_TYPE getDCAStrategyType()
  {
   if(bot.manual)
     {
      return NONE;
     }

   if(DCA_Strategy_Backtest)
     {
      return DCA_Strategy_Type;
     }

   KhanhdeuxConfiguration* config = getConfiguration();
   return config.strategyType;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ENUM_STRATEGY_ENTRY_TYPE getEntryType()
  {
   if(DCA_Strategy_Backtest)
     {
      return DCA_Strategy_Entry_Type;
     }

   KhanhdeuxConfiguration* config = getConfiguration();
   return config.entryType;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool isSignalReversed()
  {
   if(DCA_Strategy_Backtest)
     {
      return DCA_Strategy_Signal_Reversed;
     }

   KhanhdeuxConfiguration* config = getConfiguration();
   return config.signalReversed;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getDCALotSize()
  {
   KhanhdeuxSetup* setupByTime = getBestSetupByTime();
   return setupByTime.lotsize;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getDCAMaxDistance()
  {
   KhanhdeuxSetup* setupByTime = getBestSetupByTime();
   return setupByTime.maxDistance;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getDCAMaxDistanceByType(ENUM_DCA_ORDER_TYPE currentOrderType = NULL)
  {
   datetime tradingTime = getFirstDCATime(currentOrderType);
   tradingTime = tradingTime ? tradingTime : TimeCurrent();
   int maxDistance;

   if(getTradingSession(LONDON, true) <= tradingTime && tradingTime < getTradingSession(LONDON, false))
     {
      maxDistance = getDCAMaxDistance();
     }
   else
     {
      maxDistance = getDCAMaxDistance();
     }

   return maxDistance;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime getFirstDCATime(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = 0; i <= PositionsTotal() - 1; i++)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return (datetime) PositionGetInteger(POSITION_TIME);
        }
     }

   return NULL;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getDCADistance()
  {
   KhanhdeuxSetup* setupByTime = getBestSetupByTime();
   return setupByTime.distance;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getPositionTypeByOrder(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   if(currentOrderType == DCA_BUY)
     {
      return POSITION_TYPE_BUY;
     }
   if(currentOrderType == DCA_SELL)
     {
      return POSITION_TYPE_SELL;
     }

   return NULL;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getLastDCAPrice(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return PositionGetDouble(POSITION_PRICE_OPEN);
        }
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ENUM_DCA_ORDER_TYPE getLastDCAOrderType()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && !isHedgPosition(position.Comment())
        )
        {
         return PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(DCA_BUY) ? DCA_BUY : DCA_SELL;
        }
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getLastNthDCAPrice(ENUM_DCA_ORDER_TYPE currentOrderType, int number)
  {
   int count = 1;

   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         if(count == number)
           {
            return PositionGetDouble(POSITION_PRICE_OPEN);
           }
         count++;
        }
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getLastDCALotSize(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return PositionGetDouble(POSITION_VOLUME);
        }
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime getLastDCATime(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return (datetime) PositionGetInteger(POSITION_TIME);
        }
     }

   return NULL;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getLastDCATakeProfit(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return PositionGetDouble(POSITION_TP);
        }
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getLastDCAStoploss(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return PositionGetDouble(POSITION_SL);
        }
     }

   return 0;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              updateAllDCAPositionTakeProfit(double tpPrice, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         double sl = position.StopLoss();
         double tp = tpPrice;

         trade.PositionModify(position.Ticket(),sl,tp);
         Print(StringFormat("Position with lotsize=%g updated TakeProfit= %g", PositionGetDouble(POSITION_VOLUME), tpPrice));
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              updateAllDCAPositionStopLoss(double slPrice, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         double sl = slPrice;
         if(sl == position.StopLoss())
           {
            continue;
           }
         double tp = position.TakeProfit();

         trade.PositionModify(position.Ticket(),sl,tp);
         Print(StringFormat("Position with lotsize=%g updated Stoploss= %g", PositionGetDouble(POSITION_VOLUME), sl));
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              isAllDCAPositionStopLossUpdated(double slPrice, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return slPrice == position.StopLoss() ? true : false;
        }
     }

   return false;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              isStoplossValid(double slPrice, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   double currentPrice = getCurrentPrice(currentOrderType);
   return (currentOrderType == DCA_BUY && slPrice < currentPrice) || (currentOrderType == DCA_SELL && slPrice > currentPrice);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              updateAllDCAPositionTakeProfitAndStopLoss(ENUM_DCA_ORDER_TYPE currentOrderType, double tpPrice, double slPrice)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         double sl = (slPrice > 0 && slPrice != position.StopLoss()) ? slPrice: position.StopLoss();
         double tp = tpPrice;

         trade.PositionModify(position.Ticket(),sl,tp);
         Print(StringFormat("Position with lotsize=%g updated SL= %g TakeProfit= %g", PositionGetDouble(POSITION_VOLUME), sl, tp));
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
void              closeAllDCAPositions(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         trade.PositionClose(position.Ticket());
         Print(StringFormat("Close Position with lotsize=%g", PositionGetDouble(POSITION_VOLUME)));
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              closeAllHedgPositions(ENUM_DCA_ORDER_TYPE currentOrderType = NULL)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && isHedgPosition(position.Comment(), currentOrderType)
        )
        {
         trade.PositionClose(position.Ticket());
         Print(StringFormat("Close Hedg Position with lotsize=%g", PositionGetDouble(POSITION_VOLUME)));
        }
     }
  }

string HEDG_STRING = "HEDG";
string BUY_STRING = "BUY";
string SELL_STRING = "SELL";

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              isHedgPosition(string comment, ENUM_DCA_ORDER_TYPE currentOrderType = NULL)
  {
   string findStr = HEDG_STRING;
   if(currentOrderType)
     {
      findStr += "-" + (currentOrderType == DCA_BUY ? BUY_STRING : SELL_STRING);
     }
   return StringFind(comment, findStr) > -1 ? true : false;
  }

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
bool              checkTradingStopHour()
  {
   MqlDateTime time1;
   TimeToStruct(TimeCurrent(),time1);
   int hour=time1.hour;

   if(TradingStop_HourStart == TradingStop_HourStop)
     {
      if(hour == TradingStop_HourStart)
        {
         return true;
        }
     }

   if(TradingStop_HourStart < TradingStop_HourStop)
     {
      if(TradingStop_HourStart <= hour && hour < TradingStop_HourStop)
        {
         return true;
        }
     }

   if(TradingStop_HourStart > TradingStop_HourStop)
     {
      if(TradingStop_HourStart <= hour || hour < TradingStop_HourStop)
        {
         return true;
        }
     }

   return false;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkTradingGoHour()
  {
   if(DCA_Strategy_Backtest)
     {
      return checkTradingTime(TradingGo_HourStart, TradingGo_HourStop);
     }

   MqlDateTime time1;
   TimeToStruct(TimeCurrent() + 5 * 60, time1);

   if(checkTradingTime(ASIA_HourStart, ASIA_HourStop))
     {
      if(!checkTradingTime(ASIA_HourStart, ASIA_HourStop, time1.hour))
        {
         return false;
        }
      return true;
     }

   if(checkTradingTime(UK_HourStart, UK_HourStop))
     {
      if(!checkTradingTime(UK_HourStart, UK_HourStop, time1.hour))
        {
         return false;
        }
      return true;
     }

   if(checkTradingTime(US_HourStart, US_HourStop))
     {
      if(!checkTradingTime(US_HourStart, US_HourStop, time1.hour))
        {
         return false;
        }
      return true;
     }

   return false;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkTradingTime(int start, int stop, int currentHour = NULL)
  {
   MqlDateTime time1;
   TimeToStruct(TimeCurrent(),time1);
   int hour = currentHour ? currentHour : time1.hour;

   if(start == stop)
     {
      if(hour == start)
        {
         return true;
        }
     }

   if(start < stop)
     {
      if(start <= hour && hour < stop)
        {
         return true;
        }
     }

   if(start > stop)
     {
      if(start <= hour || hour < stop)
        {
         return true;
        }
     }

   return false;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkTradingEntryHour()
  {
   MqlDateTime time1;
   TimeToStruct(TimeCurrent(),time1);
   int hour=time1.hour;

   if(TradingEntryTime_HourStart == TradingEntryTime_HourStop)
     {
      if(hour == TradingEntryTime_HourStart)
        {
         return true;
        }
     }

   if(TradingEntryTime_HourStart < TradingEntryTime_HourStop)
     {
      if(TradingEntryTime_HourStart <= hour && hour < TradingEntryTime_HourStop)
        {
         return true;
        }
     }

   if(TradingEntryTime_HourStart > TradingEntryTime_HourStop)
     {
      if(TradingEntryTime_HourStart <= hour || hour < TradingEntryTime_HourStop)
        {
         return true;
        }
     }

   return false;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkTradingSession()
  {
   datetime currentTime = TimeCurrent();
   int offset = 0 * 60 * 60;
// return true;

   ENUM_DAY_OF_WEEK dayOfWeek = getDayOfWeek();

   switch(dayOfWeek)
     {
      case MONDAY:
         return true;
         break;
      case TUESDAY:
         break;
      case WEDNESDAY:
         break;
      case THURSDAY:
         break;
      case FRIDAY:
         return true;
         break;
     }

   return true;
   return currentTime + offset >= getTradingSession(NEWYORK, true) && currentTime - offset <= getTradingSession(LONDON, false);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkDailyStoploss()
  {
   return getDayProfit() + getTotalProfit() < (getDailyStoploss() * -1);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getDailyStoploss()
  {
   double accountBalance = MathMin(AccountInfoDouble(ACCOUNT_BALANCE), ACCOUNT_BALANCE_INIT);
   return accountBalance * TradingStopDaily / 100;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkDailyProfit()
  {
   return getDayProfit() + getTotalProfit() > getDailyProfit();
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkStopProfit()
  {
   double accountBalance = MathMin(AccountInfoDouble(ACCOUNT_BALANCE), ACCOUNT_BALANCE_INIT);
   return getTotalProfit() > accountBalance * TradingStopProfit / 100;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getDailyProfit()
  {
   double accountBalance = AccountInfoDouble(ACCOUNT_BALANCE);
   return accountBalance * TradingProfitDaily / 100;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkDayOfWeek()
  {
   if(Trading_DayOfWeek_Active == false)
     {
      return true;
     }

   MqlDateTime STime;
   datetime time_current=TimeCurrent(STime);
   return (ENUM_DAY_OF_WEEK) STime.day_of_week == Trading_DayOfWeek;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkSession()
  {
   MqlDateTime time1;
   TimeToStruct(TimeCurrent() + 5 * 60, time1);

   if(TradingSession_Active == true)
     {
      int hourStart;
      int hourStop;

      switch(TradingSession_Type)
        {
         case SESSION_ASIA:
            hourStart = ASIA_HourStart;
            hourStop  = ASIA_HourStop;
            break;
         case SESSION_UK:
            hourStart = UK_HourStart;
            hourStop  = UK_HourStop;
            break;
         case SESSION_US:
            hourStart = US_HourStart;
            hourStop  = US_HourStop;
            break;
        }

      if(checkTradingTime(hourStart, hourStop))
        {
         if(!checkTradingTime(hourStart, hourStop, time1.hour))
           {
            return false;
           }
         return true;
        }

      return false;
     }

   if(checkTradingTime(ASIA_HourStart, ASIA_HourStop))
     {
      if(!checkTradingTime(ASIA_HourStart, ASIA_HourStop, time1.hour))
        {
         return false;
        }
      return true;
     }

   if(checkTradingTime(UK_HourStart, UK_HourStop))
     {
      if(!checkTradingTime(UK_HourStart, UK_HourStop, time1.hour))
        {
         return false;
        }

      return true;
     }

   if(checkTradingTime(US_HourStart, US_HourStop))
     {
      if(!checkTradingTime(US_HourStart, US_HourStop, time1.hour))
        {
         return false;
        }

      return true;
     }

   return false;
  }

//+------------------------------------------------------------------+
//|
// SUNDAY
// MONDAY
// TUESDAY
// WEDNESDAY
// THURSDAY
// FRIDAY
// SATURDAY
//+------------------------------------------------------------------+
ENUM_DAY_OF_WEEK            getDayOfWeek()
  {
   MqlDateTime STime;
   datetime time_current=TimeCurrent(STime);
   return (ENUM_DAY_OF_WEEK)STime.day_of_week;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageToRsi()
  {
   int oversold = getRsiOversold();
   int overbought = getRsiOverbought();
   bool signal = false;

   string message = "RSI SIGNAL!!!" + "%0A";

   int rsiSignal = getRsiSignal(RSI_NumOfCandles);

   if(rsiSignal == SIGNAL_BUY)
     {
      message += "BUY: RSI crossover " + StringFormat("%d", oversold);
      signal = true;
     }

   if(rsiSignal == SIGNAL_SELL)
     {
      message += "SELL: RSI crossunder " + StringFormat("%d", overbought);
      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageCloseToRsi()
  {
   bool signal = false;

   string message = "RSI SIGNAL!!!" + "%0A";

   int rsiSignal = getRsiSignal(3);

   if(rsiSignal == SIGNAL_BUY)
     {
      message += "RSI oversold! IF L3 THEN CLOSE SELL!";
      signal = true;
     }

   if(rsiSignal == SIGNAL_SELL)
     {
      message += "RSI overbought! IF L3 THEN CLOSE BUY!";
      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageToSmoothedRsi()
  {
   bool signal = false;
   string message = "RSI SMOOTHED SIGNAL!!!" + "%0A";

   int rsiSignal = getSmoothedRsiSignal(RSI_NumOfCandles);

   if(rsiSignal == SIGNAL_BUY)
     {
      double currentPrice = getCurrentPrice(DCA_BUY);
      double alertPrice = currentPrice - 10 * _Point * 10;
      bot.alertPrice = alertPrice;

      message += "BUY at price:" + currentPrice;
      message += "%0A" + "Alert at price:" + alertPrice;
      message += "%0A" + "RSI M5:" + htfRsi;
      message += "%0A" + "Level:";

      if(htfRsi < RSI_M5_BUY_LEVEL1)
        {
         message += EnumToString(LEVEL_1) + "|";
        }

      if(htfRsi < RSI_M5_BUY_LEVEL2)
        {
         message += EnumToString(LEVEL_2) + "|";
        }

      if(htfRsi < RSI_M5_BUY_LEVEL3)
        {
         message += EnumToString(LEVEL_3) + "|";
        }


      signal = true;
     }

   if(rsiSignal == SIGNAL_SELL)
     {
      double currentPrice = getCurrentPrice(DCA_SELL);
      double alertPrice = currentPrice + 10 * _Point * 10;
      bot.alertPrice = alertPrice;

      message += "SELL at price:" + currentPrice;
      message += "%0A" + "Alert at price:" + alertPrice;
      message += "%0A" + "RSI M5:" + htfRsi;
      message += "%0A" + "Level:";

      if(htfRsi > RSI_M5_SELL_LEVEL1)
        {
         message += EnumToString(LEVEL_1) + "|";
        }

      if(htfRsi > RSI_M5_SELL_LEVEL2)
        {
         message += EnumToString(LEVEL_2) + "|";
        }

      if(htfRsi > RSI_M5_SELL_LEVEL3)
        {
         message += EnumToString(LEVEL_3) + "|";
        }

      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageToCci()
  {
   double cci[];
   int cciHanle = iCCI(_Symbol, (ENUM_TIMEFRAMES) Timeframe, 14, PRICE_TYPICAL);
   int upper = 100;
   int lower = -100;
   bool signal = false;

   CopyBuffer(cciHanle, 0, 0, 2, cci);

   string message = "SIGNAL!!!" + "%0A";

   if(cci[0] < lower && cci[1] >= lower)
     {
      message += "SELL: CCI crossover " + StringFormat("%d", lower);
      signal = true;
     }


   if(cci[0] > upper && cci[1] <= upper)
     {
      message += "BUY: CCI crossunder " + StringFormat("%d", upper);
      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }

   ArrayFree(cci);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageToSession()
  {
   string message = "";
   bool signal = false;

   datetime currentTime = getCurrentNewsTime();
   bool sessionStart = false;
   ENUM_SESSION_TYPE types[] = {SYDNEY, TOKYO, LONDON, NEWYORK};

   for(int i=0; i<ArraySize(types); i++)
     {
      if(currentTime == getTradingSession(types[i], true))
        {
         message += "%0A" + "===SESSION:" +  EnumToString(types[i]) + "===";
         signal = true;
        }

      if(currentTime == getTradingSession(types[i], false))
        {
         message += "%0A" + "===END-SESSION:" + EnumToString(types[i]) + "===";
         signal = true;
        }
     }

   MqlDateTime time1;
   TimeToStruct(TimeCurrent() - 60, time1);

   string configMessage = "";
   configMessage       += StringFormat("[TYPE]%s", EnumToString(getDCAStrategyType())) + "";
   configMessage       += StringFormat("[ENTRY_TYPE]%s", EnumToString(getEntryType())) + "";
   configMessage       += StringFormat("[REVERSED]%s", isSignalReversed() ? "true" : "false") + "";
   configMessage       += StringFormat("[Lot]%G[Dist]%G",
                                       getDCALotSize(),
                                       getDCADistance());

   if(!checkTradingTime(ASIA_HourStart, ASIA_HourStop, time1.hour)
      && checkTradingTime(ASIA_HourStart, ASIA_HourStop))
     {
      message += "%0A" + "===ASIA-SESSION-CONFIG===";
      message += "%0A" + configMessage;
      signal = true;
     }

   if(!checkTradingTime(UK_HourStart, UK_HourStop, time1.hour)
      && checkTradingTime(UK_HourStart, UK_HourStop))
     {
      message += "%0A" + "===UK-SESSION-CONFIG===";
      message += "%0A" + configMessage;
      signal = true;
     }

   if(!checkTradingTime(US_HourStart, US_HourStop, time1.hour)
      && checkTradingTime(US_HourStart, US_HourStop))
     {
      message += "%0A" + "===US-SESSION-CONFIG===";
      message += "%0A" + configMessage;
      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageToNews()
  {
   MqlDateTime STime;
   datetime time_current=TimeCurrent(STime);
   string dayOfWeek = EnumToString((ENUM_DAY_OF_WEEK)STime.day_of_week);

   int offset = TradingStopNews_FreezeInMinutes * 60;

   datetime currentTime = TimeCurrent();

   for(int i=0; i<ArraySize(todayEvents); i++)
     {
      if(getDailyNewsType(todayEvents[i].name)
         && currentTime + offset + 60 >= todayEvents[i].time
         && currentTime + offset <= todayEvents[i].time)
        {
         string message = "===LIVE-NEWS:" + todayEvents[i].name + "(" + dayOfWeek + "|" + todayEvents[i].importanceLevel +  "|" + todayEvents[i].time + ") in " + TradingStopNews_FreezeInMinutes + " minutes";
         printf(message);
         SendMessage(message, ChatID, BotToken);
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageToFVG()
  {
   bool signal = false;
   string message = "FVG SIGNAL!!!" + "%0A";

   int fvgSignal = getFVGSignal();

   if(fvgSignal == SIGNAL_BUY)
     {
      message += "BUY";
      signal = true;
     }

   if(fvgSignal == SIGNAL_SELL)
     {
      message += "SELL";
      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              initPositionMagics()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      ulong ticket = PositionGetTicket(i);
      if(ticket > 0)
        {
         long magic = position.Magic();
         bool found = false;
         for(int j=0; j< positionMagics.Total(); j++)
           {
            if(magic == positionMagics[j])
              {
               found = true;
              }
           }

         if(!found)
           {
            positionMagics.Add(magic);
           }
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageToStoplossWarning()
  {
   double pL = getTotalProfit();

   if(pL <= loss)
     {
      string message = "WARNING!" + "";
      message       += "Loss:" + StringFormat("%g ", pL) + "<" + StringFormat("%g ", loss) + "";
      message       += getPositionMagicMessage();
      message       += StringFormat("[BAL]%G",AccountInfoDouble(ACCOUNT_BALANCE)) + "";
      message       += StringFormat("[PRO]%G",AccountInfoDouble(ACCOUNT_PROFIT)) + "";
      message       += StringFormat("[EQU]%G",AccountInfoDouble(ACCOUNT_EQUITY)) + "";
      // message       += "%0A" + getPositionDetails();
      message       += "%0A" + StringFormat("%s %d (1:%g:%d)", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN), SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE), AccountInfoInteger(ACCOUNT_LEVERAGE)) + "%0A";

      printf(message);
      SendMessage(message, ChatID, BotToken);
      loss -= LossDistance;
      positionMagics.Clear();
     }
   else
     {
      if(pL > loss + LossDistance * 1.5 && pL < InitLoss)
        {
         loss = loss + LossDistance;
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageToAlert()
  {
   bool isAlert = false;

   if(bot.alertPrice <= 0)
     {
      return;
     }

   MqlRates rate[];
   CopyRates(_Symbol,(ENUM_TIMEFRAMES) Timeframe,0,2,rate);

   double currentPrice = rate[1].close;
   double previousPrice = rate[0].close;
   double aPrice = bot.alertPrice;

   if(previousPrice <= aPrice && aPrice <= currentPrice)
     {
      isAlert = true;
     }

   if(currentPrice <= aPrice && aPrice <= previousPrice)
     {
      isAlert = true;
     }

   if(isAlert)
     {
      string message = "ALERT!Price crossed: " + bot.alertPrice;
      printf(message);
      SendMessage(message, ChatID, BotToken);
      bot.alertPrice = 0;
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageToBalanceChange()
  {
   double newBalance = AccountInfoDouble(ACCOUNT_BALANCE);

   if(currentBalance != newBalance)
     {
      if(currentBalance < newBalance)
        {
         string message        = "";
         message       += StringFormat("BALANCE=%G. TP=%+.2f$", newBalance, (newBalance - currentBalance)) + "";
         message       += "%0A" + StringFormat("%s %d (1:%g:%d)", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN), SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE), AccountInfoInteger(ACCOUNT_LEVERAGE)) + "%0A";
         StringReplace(message, "+", "%2b");

         printf(message);
         if(ChatID != "" && BotToken != "")
           {
            SendMessage(message, ChatID, BotToken);
           }
        }
      currentBalance = newBalance;
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendPositionsChanged()
  {
   int total = PositionsTotal();

   if(numOfPositions != total)
     {
      string message = "NOTICE!" + "";
      message       += "[PO]" + StringFormat("%d", numOfPositions) + "-" + StringFormat("%d", total) + "";
      message       += getPositionMagicMessage();
      message       += StringFormat("[BAL]%G",AccountInfoDouble(ACCOUNT_BALANCE)) + "";
      message       += StringFormat("[PRO]%G",AccountInfoDouble(ACCOUNT_PROFIT)) + "";
      message       += StringFormat("[EQU]%G",AccountInfoDouble(ACCOUNT_EQUITY)) + "";
      message       += "%0A" + getPositionDetails();
      message       += "%0A" + StringFormat("%s %d (1:%g:%d)", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN), SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE), AccountInfoInteger(ACCOUNT_LEVERAGE)) + "%0A";

      numOfPositions = PositionsTotal();

      printf(message);
      SendMessage(message, ChatID, BotToken);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string            getPositionMagicMessage()
  {
   string magicMessage = "";
   initPositionMagics();

   for(int i=0; i<positionMagics.Total(); i++)
     {
      long magic = positionMagics[i];
      int buyCount = 0;
      int sellCount = 0;

      // magicMessage += StringFormat("[%d]", magic) +  "";

      for(int i = PositionsTotal() - 1; i >= 0; i--)
        {
         if(PositionGetTicket(i)
            && PositionGetSymbol(i) == _Symbol
            && position.Magic() == magic
            && PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY
           )
           {
            buyCount++;
           }
        }

      if(buyCount > 0)
        {
         magicMessage += StringFormat("BUY:L%d", buyCount);
        }

      for(int i = PositionsTotal() - 1; i >= 0; i--)
        {
         if(PositionGetTicket(i)
            && PositionGetSymbol(i) == _Symbol
            && position.Magic() == magic
            && PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL
           )
           {
            sellCount++;
           }
        }

      if(sellCount > 0)
        {
         magicMessage += StringFormat("SELL:L%d", sellCount);
        }

      magicMessage += "";
     }

   return magicMessage;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string            getPositionDetails()
  {
   string detailMessage = "";
   initPositionMagics();

   for(int i=0; i<positionMagics.Total(); i++)
     {
      long magic = positionMagics[i];

      for(int i = PositionsTotal() - 1; i >= 0; i--)
        {
         if(PositionGetTicket(i)
            && PositionGetSymbol(i) == _Symbol
            && position.Magic() == magic
            && PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY
           )
           {
            string positionType = PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY ? "BUY": "SELL";
            detailMessage += StringFormat("%s L:%g, P:%g, SL:%g, TP:%g, T:%s",
                                          positionType,
                                          position.Volume(),
                                          position.PriceOpen(),
                                          position.StopLoss(),
                                          position.TakeProfit(),
                                          TimeToString(PositionGetInteger(POSITION_TIME))) + "%0A";
           }
        }

      for(int i = PositionsTotal() - 1; i >= 0; i--)
        {
         if(PositionGetTicket(i)
            && PositionGetSymbol(i) == _Symbol
            && position.Magic() == magic
            && PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL
           )
           {
            string positionType = PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY ? "BUY": "SELL";
            detailMessage += StringFormat("%s L:%g, P:%g, SL:%g, TP:%g, T:%s",
                                          positionType,
                                          position.Volume(),
                                          position.PriceOpen(),
                                          position.StopLoss(),
                                          position.TakeProfit(),
                                          TimeToString(PositionGetInteger(POSITION_TIME))) + "%0A";
           }
        }
     }

   return detailMessage;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string getPlanMessage()
  {
   string message = "";
   ENUM_DAY_OF_WEEK dayOfWeek[] = {MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY};

   MqlDateTime currentDate;
   TimeToStruct(TimeCurrent(), currentDate);

   message += "CURRENT MONTH:" + StringFormat("%d", currentDate.mon);

   for(int i=0; i<ArraySize(dayOfWeek); i++)
     {
      KhanhdeuxConfiguration* config = getConfiguration(dayOfWeek[i]);
      message += "\n" + EnumToString(dayOfWeek[i]);
      message += ": Dist:" + StringFormat("%G", config.distance);
      message += ", Strategy:" + EnumToString(config.strategyType);
      message += ", Entry:" + EnumToString(config.entryType);
      message += ", Risk:" + EnumToString(config.riskType);
     }

   return message;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string getConfigMessage()
  {
   string message = "";

   message += "\n=== MONDAY ===";
   message += "\nASIA:";
   message       += StringFormat("[TYPE]%s", EnumToString(MONDAY_ASIA_Strategy_Type)) + "";
   message       += StringFormat("[ENTRY_TYPE]%s", EnumToString(MONDAY_ASIA_Strategy_Entry_Type)) + "";
   message       += StringFormat("[REVERSED]%s", MONDAY_ASIA_Signal_Reversed ? "true" : "false") + "";
   message       += StringFormat("[DIST]%G", MONDAY_ASIA_Distance);

   message += "\nUK:";
   message       += StringFormat("[TYPE]%s", EnumToString(MONDAY_UK_Strategy_Type)) + "";
   message       += StringFormat("[ENTRY_TYPE]%s", EnumToString(MONDAY_UK_Strategy_Entry_Type)) + "";
   message       += StringFormat("[REVERSED]%s", MONDAY_UK_Signal_Reversed ? "true" : "false") + "";
   message       += StringFormat("[DIST]%G", MONDAY_UK_Distance);

   message += "\nUS:";
   message       += StringFormat("[TYPE]%s", EnumToString(MONDAY_US_Strategy_Type)) + "";
   message       += StringFormat("[ENTRY_TYPE]%s", EnumToString(MONDAY_US_Strategy_Entry_Type)) + "";
   message       += StringFormat("[REVERSED]%s", MONDAY_US_Signal_Reversed ? "true" : "false") + "";
   message       += StringFormat("[DIST]%G", MONDAY_US_Distance);

   message += "\n=== TUESDAY ===";
   message += "\nASIA:";
   message       += StringFormat("[TYPE]%s", EnumToString(TUESDAY_ASIA_Strategy_Type)) + "";
   message       += StringFormat("[ENTRY_TYPE]%s", EnumToString(TUESDAY_ASIA_Strategy_Entry_Type)) + "";
   message       += StringFormat("[REVERSED]%s", TUESDAY_ASIA_Signal_Reversed ? "true" : "false") + "";
   message       += StringFormat("[DIST]%G", TUESDAY_ASIA_Distance);

   message += "\nUK:";
   message       += StringFormat("[TYPE]%s", EnumToString(TUESDAY_UK_Strategy_Type)) + "";
   message       += StringFormat("[ENTRY_TYPE]%s", EnumToString(TUESDAY_UK_Strategy_Entry_Type)) + "";
   message       += StringFormat("[REVERSED]%s", TUESDAY_UK_Signal_Reversed ? "true" : "false") + "";
   message       += StringFormat("[DIST]%G", TUESDAY_UK_Distance);

   message += "\nUS:";
   message       += StringFormat("[TYPE]%s", EnumToString(TUESDAY_US_Strategy_Type)) + "";
   message       += StringFormat("[ENTRY_TYPE]%s", EnumToString(TUESDAY_US_Strategy_Entry_Type)) + "";
   message       += StringFormat("[REVERSED]%s", TUESDAY_US_Signal_Reversed ? "true" : "false") + "";
   message       += StringFormat("[DIST]%G", TUESDAY_US_Distance);


   message += "\n=== WEDNESDAY ===";
   message += "\nASIA:";
   message       += StringFormat("[TYPE]%s", EnumToString(WEDNESDAY_ASIA_Strategy_Type)) + "";
   message       += StringFormat("[ENTRY_TYPE]%s", EnumToString(WEDNESDAY_ASIA_Strategy_Entry_Type)) + "";
   message       += StringFormat("[REVERSED]%s", WEDNESDAY_ASIA_Signal_Reversed ? "true" : "false") + "";
   message       += StringFormat("[DIST]%G", WEDNESDAY_ASIA_Distance);

   message += "\nUK:";
   message       += StringFormat("[TYPE]%s", EnumToString(WEDNESDAY_UK_Strategy_Type)) + "";
   message       += StringFormat("[ENTRY_TYPE]%s", EnumToString(WEDNESDAY_UK_Strategy_Entry_Type)) + "";
   message       += StringFormat("[REVERSED]%s", WEDNESDAY_UK_Signal_Reversed ? "true" : "false") + "";
   message       += StringFormat("[DIST]%G", WEDNESDAY_UK_Distance);

   message += "\nUS:";
   message       += StringFormat("[TYPE]%s", EnumToString(WEDNESDAY_US_Strategy_Type)) + "";
   message       += StringFormat("[ENTRY_TYPE]%s", EnumToString(WEDNESDAY_US_Strategy_Entry_Type)) + "";
   message       += StringFormat("[REVERSED]%s", WEDNESDAY_US_Signal_Reversed ? "true" : "false") + "";
   message       += StringFormat("[DIST]%G", WEDNESDAY_US_Distance);


   message += "\n=== THURSDAY ===";
   message += "\nASIA:";
   message       += StringFormat("[TYPE]%s", EnumToString(THURSDAY_ASIA_Strategy_Type)) + "";
   message       += StringFormat("[ENTRY_TYPE]%s", EnumToString(THURSDAY_ASIA_Strategy_Entry_Type)) + "";
   message       += StringFormat("[REVERSED]%s", THURSDAY_ASIA_Signal_Reversed ? "true" : "false") + "";
   message       += StringFormat("[DIST]%G", THURSDAY_ASIA_Distance);

   message += "\nUK:";
   message       += StringFormat("[TYPE]%s", EnumToString(THURSDAY_UK_Strategy_Type)) + "";
   message       += StringFormat("[ENTRY_TYPE]%s", EnumToString(THURSDAY_UK_Strategy_Entry_Type)) + "";
   message       += StringFormat("[REVERSED]%s", THURSDAY_UK_Signal_Reversed ? "true" : "false") + "";
   message       += StringFormat("[DIST]%G", THURSDAY_UK_Distance);

   message += "\nUS:";
   message       += StringFormat("[TYPE]%s", EnumToString(THURSDAY_US_Strategy_Type)) + "";
   message       += StringFormat("[ENTRY_TYPE]%s", EnumToString(THURSDAY_US_Strategy_Entry_Type)) + "";
   message       += StringFormat("[REVERSED]%s", THURSDAY_US_Signal_Reversed ? "true" : "false") + "";
   message       += StringFormat("[DIST]%G", THURSDAY_US_Distance);

   message += "\n=== FRIDAY ===";
   message += "\nASIA:";
   message       += StringFormat("[TYPE]%s", EnumToString(FRIDAY_ASIA_Strategy_Type)) + "";
   message       += StringFormat("[ENTRY_TYPE]%s", EnumToString(FRIDAY_ASIA_Strategy_Entry_Type)) + "";
   message       += StringFormat("[REVERSED]%s", FRIDAY_ASIA_Signal_Reversed ? "true" : "false") + "";
   message       += StringFormat("[DIST]%G", FRIDAY_ASIA_Distance);

   message += "\nUK:";
   message       += StringFormat("[TYPE]%s", EnumToString(FRIDAY_UK_Strategy_Type)) + "";
   message       += StringFormat("[ENTRY_TYPE]%s", EnumToString(FRIDAY_UK_Strategy_Entry_Type)) + "";
   message       += StringFormat("[REVERSED]%s", FRIDAY_UK_Signal_Reversed ? "true" : "false") + "";
   message       += StringFormat("[DIST]%G", FRIDAY_UK_Distance);

   message += "\nUS:";
   message       += StringFormat("[TYPE]%s", EnumToString(FRIDAY_US_Strategy_Type)) + "";
   message       += StringFormat("[ENTRY_TYPE]%s", EnumToString(FRIDAY_US_Strategy_Entry_Type)) + "";
   message       += StringFormat("[REVERSED]%s", FRIDAY_US_Signal_Reversed ? "true" : "false") + "";
   message       += StringFormat("[DIST]%G", FRIDAY_US_Distance);

   return message;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string getNewsMessage()
  {
   string message = "";
   message += "===CURRENT-TIME:" + TimeCurrent() + "\n";

   for(int i=0; i<ArraySize(todayEvents); i++)
     {
      message += "===TODAY-NEWS:" +  todayEvents[i].name + "(" + todayEvents[i].importanceLevel +  "|" + todayEvents[i].time + ")" + "\n";
     }

   return message;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getDayProfit()
  {
   double dayprof = 0.0;
   datetime end = TimeCurrent();
   string sdate = TimeToString(TimeCurrent(), TIME_DATE);
   datetime start = StringToTime(sdate);

   HistorySelect(start,end);
   int TotalDeals = HistoryDealsTotal();

   for(int i = 0; i < TotalDeals; i++)
     {
      ulong Ticket = HistoryDealGetTicket(i);

      if(HistoryDealGetInteger(Ticket,DEAL_ENTRY) == DEAL_ENTRY_OUT)
        {
         double LatestProfit = HistoryDealGetDouble(Ticket, DEAL_PROFIT);
         dayprof += LatestProfit;
        }
     }

   return dayprof;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool isSummerTime()
  {
   datetime    tm=TimeCurrent();
   MqlDateTime stm;
   TimeToStruct(tm,stm);

   datetime dst_start,dst_end;
   dst_start=dst_end=0;

   DST_USA(stm.year,dst_start,dst_end);
   return dst_start <= tm && tm <= dst_end;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime getTradingSession(ENUM_SESSION_TYPE type, bool isStart = true)
  {
   int hourStart = 0;
   int hourStop = 0;

   switch(type)
     {
      case SYDNEY:
         hourStart = isSummerTime() ? 22 : 23;
         hourStop  = isSummerTime() ? 7 : 8;
         break;
      case TOKYO:
         hourStart = isSummerTime() ? 0 : 1;
         hourStop  = isSummerTime() ? 9 : 10;
         break;
      case LONDON:
         hourStart = isSummerTime() ? 9 : 10;
         hourStop  = isSummerTime() ? 18 : 19;
         break;
      case NEWYORK:
         hourStart = isSummerTime() ? 14 : 15;
         hourStop  = isSummerTime() ? 23 : 0;
         break;
     }

   MqlDateTime structTime;
   TimeCurrent(structTime);
   structTime.sec = 0;
   structTime.hour = hourStart;
   structTime.min = 0;
   datetime timeStart = StructToTime(structTime);
   structTime.hour = hourStop;
   datetime timeEnd = StructToTime(structTime);

   if(hourStart > hourStop)
     {
      timeEnd = timeEnd + 24 * 60 * 60;
     }

   return isStart ? timeStart : timeEnd;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool isMessageCommandType(string text)
  {
   return StringFind(text, "/", 0) == 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime getFirstDayOfNewsYear()
  {
   MqlDateTime firstDayOfYear;

   firstDayOfYear.year = News_Backtest_Year;
   firstDayOfYear.mon = 1;  // January
   firstDayOfYear.day = 1;  // 1st day
   firstDayOfYear.hour = 0;
   firstDayOfYear.min = 0;
   firstDayOfYear.sec = 0;

// Convert back to datetime
   return StructToTime(firstDayOfYear);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime getLastDayOfNewsYear()
  {
   MqlDateTime lastDayOfYear;

// Set the structure to the last day of the current year
   lastDayOfYear.year = News_Backtest_Year;
   lastDayOfYear.mon = 12;  // December
   lastDayOfYear.day = 31;  // 31st day
   lastDayOfYear.hour = 23;
   lastDayOfYear.min = 59;
   lastDayOfYear.sec = 59;

// Convert back to datetime
   return StructToTime(lastDayOfYear);
  }


//+------------------------------------------------------------------+
int               SendMessage(string text, string chatID, string botToken)
  {
   string baseUrl = "https://api.telegram.org";
   string headers = "";
   string requestURL = "";
   string requestHeaders = "";
   char resultData[];
   char posData[];
   int timeout = 2000;

// if not a command
   if(!isMessageCommandType(text))
     {
      Sleep(Notification_Delay);
     }

   requestURL = StringFormat("%s/bot%s/sendmessage?chat_id=%s&text=%s", baseUrl, botToken, chatID, text);

   int response = WebRequest("POST", requestURL, headers, timeout, posData, resultData, requestHeaders);

   string resultMessage = CharArrayToString(resultData);
   Print(resultMessage);

   return response;
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
void              algoTradingToggle(bool newStatus_True_Or_False)
  {
//--- getting the current status
   bool currentStatus = (bool) TerminalInfoInteger(TERMINAL_TRADE_ALLOWED);

//--- if the current status is equal to input trueFalse then, no need to toggle auto-trading
   if(currentStatus != newStatus_True_Or_False)
     {
      //--- Toggle Auto-Trading
      HANDLE hChart = (HANDLE) ChartGetInteger(ChartID(), CHART_WINDOW_HANDLE);
      PostMessageW(GetAncestor(hChart, GA_ROOT), WM_COMMAND, MT_WMCMD_EXPERTS, 0);
     }
  }

// OPTIMIZATION AREA

struct KhanhdeuxResult
  {
   string            pass;
   double            optimization;
   double            forward;
   double            optimizationDD;
   double            forwardDD;
   string            parameters[];
   string            headers[];
  };

KhanhdeuxResult results[];
bool isForwardBacktest = false;

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
void OnTesterInit()
  {
   Print(__FUNCTION__,"(): Start Optimization... \n-----------");
   ArrayResize(results, 0);
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double OnTester()
  {
   string name="";
   long   id=0;
   double data[2];

   data[0]=TesterStatistics(STAT_PROFIT);
   data[1]=TesterStatistics(STAT_EQUITYDD_PERCENT);

   FrameAdd(name, id, 0, data);

   return data[0];
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnTesterPass()
  {
   ulong  pass=0;
   string name="";
   long   id=0;
   double value=0.0;
   double data[];

   while(FrameNext(pass,name,id,value,data))
     {
      bool stop = false;
      for(int i=0;i< ArraySize(results);i++)
        {
         if(results[i].pass == IntegerToString(pass))
           {
            KhanhdeuxResult result = results[i];

            result.forward = data[0];
            result.forwardDD = data[1];

            results[i] = result;
            stop = true;
            isForwardBacktest = true;
            break;
           }
        }

      if(stop)
        {
         continue;
        }

      int index = ArraySize(results);
      ArrayResize(results, index + 1);

      KhanhdeuxResult result;

      result.optimization = data[0];
      result.optimizationDD = data[1];
      result.forward = 0.0;
      result.forwardDD = 0.0;
      result.pass = IntegerToString(pass);

      string     parameters_list[];
      int        parameters_count=0;
      string     parameters[];

      FrameInputs(pass, parameters_list, parameters_count);

      for(int i=0; i<parameters_count; i++)
        {
         if(parameterEnabledForOptimization(parameters_list[i]))
           {
            int newSize = ArraySize(result.parameters) + 1;
            ArrayResize(result.parameters, newSize);
            result.parameters[newSize - 1] = getResultParameter(parameters_list[i]);
           }

         if(parameterForHeader(parameters_list[i]))
           {
            int newSize = ArraySize(result.headers) + 1;
            ArrayResize(result.headers, newSize);
            result.headers[newSize - 1] = getResultParameter(parameters_list[i]);
           }
        }

      results[index] = result;
     }
  }
//+------------------------------------------------------------------+
void OnTesterDeinit()
  {
   KhanhdeuxResult filteredResults[];
   filterResults(results, filteredResults); // Filter the results based on the drawdown conditions
   sortResultsByCustomMetric(filteredResults); // Sort the filtered results

   if(ArraySize(filteredResults) == 0)
     {
      Print("NO BEST FOUND !!!");
     }
   else
     {
      showOptimizationReport(filteredResults);
      writeOptimizationReport(filteredResults);
     }

   ArrayResize(results, 0);
   ArrayResize(filteredResults, 0);
   isForwardBacktest = false;
   Print("-----------\n",__FUNCTION__,"(): End Optimization");
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void filterResults(KhanhdeuxResult &results[], KhanhdeuxResult &filteredResults[])
  {
   ArrayResize(filteredResults, 0);
   for(int i = 0; i < ArraySize(results); i++)
     {
      KhanhdeuxResult result = results[i];

      if(result.forwardDD <= TEST_MaxDrawdown
         && result.optimizationDD <= TEST_MaxDrawdown
         && ((isForwardBacktest && results[i].forward > 0) || !isForwardBacktest)
         && result.optimization >= 0
        )
        {
         int newSize = ArraySize(filteredResults) + 1;
         ArrayResize(filteredResults, newSize);
         filteredResults[newSize - 1] = result;
        }
     }
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateMetric(KhanhdeuxResult &result)
  {
   if(!isForwardBacktest)
     {
      return result.optimization;
     }

   return calculateProfitSum(result);
// return (result.optimization + result.forward);
// return MathPow(result.optimization + result.forward, 2) / ((MathAbs(result.optimization - result.forward) + 1e-10) * (result.optimizationDD + result.forwardDD));

//double epsilon = 1e-10; // Small number to avoid division by zero
//return (result.optimization + result.forward) / (MathAbs(result.optimization - result.forward) + epsilon);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateProfitSum(KhanhdeuxResult &result)
  {
   return (result.optimization + result.forward);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateProfitDifference(KhanhdeuxResult &result)
  {
   return MathAbs(result.optimization - result.forward);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateDrawdownSum(KhanhdeuxResult &result)
  {
   return result.optimizationDD + result.forwardDD;
  }

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void sortResultsByCustomMetric(KhanhdeuxResult &filterResults[])
  {
   int size = ArraySize(filterResults);
   for(int i = 0; i < size - 1; i++)
     {
      for(int j = 0; j < size - i - 1; j++)
        {
         double metricA = calculateMetric(filterResults[j]);
         double metricB = calculateMetric(filterResults[j + 1]);
         if(metricA < metricB)
           {
            // Swap results[j] and results[j + 1]
            KhanhdeuxResult temp = filterResults[j];
            filterResults[j] = filterResults[j + 1];
            filterResults[j + 1] = temp;
           }
        }
     }
  }
//+------------------------------------------------------------------+

//+---------------------------------------------------------------------+
//| Checking whether the external parameter is enabled for optimization |
//+---------------------------------------------------------------------+
bool parameterEnabledForOptimization(string parameter_string)
  {
   bool enable;
   long value,start,step,stop;
//--- Determine the '=' sign position in the string
   int equality_sign_index=StringFind(parameter_string,"=",0);
//--- Get the parameter values
   ParameterGetRange(StringSubstr(parameter_string,0,equality_sign_index),
                     enable,value,start,step,stop);
//--- Return the parameter status
   return(enable);
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool parameterForHeader(string parameter_string)
  {
   string header_list[] =
     {
      "Trading_DayOfWeek",
      "TradingSession_Type"
     };

   for(int i = 0; i < ArraySize(header_list); i++)
     {

      string parts[];
      int parts_count = StringSplit(parameter_string, '=', parts);

      if(parts_count == 2)
        {
         string name = parts[0];
         if(name == header_list[i])
           {
            return true;
           }
        }
     }

   return false;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string joinStringArray(const string &array[], const string delimiter)
  {
   string result = "";
   int size = ArraySize(array);

   for(int i = 0; i < size; i++)
     {
      result += array[i];
      if(i < size - 1)
        {
         result += delimiter;
        }
     }
   return result;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string getResultParameter(string parameterAsList)
  {
   string parts[];
   int parts_count = StringSplit(parameterAsList, '=', parts);

   if(parts_count == 2)
     {
      string name = parts[0];
      string value = parts[1];

      if(name == "DCA_Strategy_Type")
        {
         value = EnumToString((ENUM_STRATEGY_TYPE) StringToInteger(value));
        }

      if(name == "DCA_Strategy_Entry_Type")
        {
         value = EnumToString((ENUM_STRATEGY_ENTRY_TYPE) StringToInteger(value));
        }

      if(name == "Trading_DayOfWeek")
        {
         value = EnumToString((ENUM_DAY_OF_WEEK) StringToInteger(value));
        }

      if(name == "TradingSession_Type")
        {
         value = EnumToString((ENUM_SESSION) StringToInteger(value));
        }
        
      if(name == "Timeframe")
        {
         value = EnumToString((CUSTOM_TIMEFRAMES) StringToInteger(value));
        }  
        
      if(name == "Timeframe_High")
        {
         value = EnumToString((CUSTOM_TIMEFRAMES) StringToInteger(value));
        }               

      return name + "=" + value;
     }
   else
     {
      Print("Warning!! Wrong parameter", parameterAsList);
     }

   return parameterAsList;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void showOptimizationReport(KhanhdeuxResult &filterResults[])
  {
   int size = TEST_ShowAll ? ArraySize(filterResults) : MathMin(ArraySize(filterResults), TEST_MaxNumberOfResult);
   for(int i=0; i<size; i++)
     {
      KhanhdeuxResult result = filterResults[i];
      string message = "No." + (i + 1);
      message += " - " + joinStringArray(result.headers, ", ") + "";
      message += "\n => " + joinStringArray(result.parameters, ", ");
      message += "\n Pass=" + result.pass + ", Metric=" + DoubleToString(calculateMetric(result),2);
      message += ", Optimization(PROFIT=" +  DoubleToString(result.optimization, 2) + "|MAX_DRAWDOWN="+ DoubleToString(result.optimizationDD, 2) + ")";

      if(isForwardBacktest)
        {
         message +=  ", Forward(PROFIT=" + DoubleToString(result.forward, 2) + "|MAX_DRAWDOWN="+ DoubleToString(result.forwardDD, 2) + ")";
        }
      message +=  "\n";
      Print(message);
     }
  }

// Save to csv
string delimiter = ";";
string infoList[] = {"OPTIMIZATION", "OPTIMIZATION_MAXDOWN", "FORWARD", "FORWARD_MAXDOWN", "PROFIT_SUM", "PROFIT_DIFF", "DRAWDOWN_SUM", "METRIC"};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void writeOptimizationReport(KhanhdeuxResult &filterResults[])
  {
// File name and path
   string fileName = "result.csv";
//string filePath = TerminalInfoString(TERMINAL_DATA_PATH) + "\\MQL5\\Files\\" + fileName;
   string filePath = "test\\" + fileName;

   if(TEST_NewReport)
     {
      deleteFile(filePath);
     }

// Check if the file exists
   bool fileExists = FileIsExist(filePath, FILE_COMMON);

// If the file doesn't exist, create it and add headers
   if(!fileExists)
     {
      int fileHandle = FileOpen(filePath, FILE_READ|FILE_WRITE|FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_COMMON|FILE_CSV);
      if(fileHandle != INVALID_HANDLE)
        {
         KhanhdeuxResult result = filterResults[0];

         string headerNameList[];
         getParameterListByIndex(result.headers, 0, headerNameList);

         string parameterNameList[];
         getParameterListByIndex(result.parameters, 0, parameterNameList);

         string header = joinStringArray(headerNameList, ";");
         header += ";" + joinStringArray(parameterNameList, ";");
         header += ";" + joinStringArray(infoList, ";");

         FileWrite(fileHandle, header);
         FileClose(fileHandle);
        }
      else
        {
         Print("Failed to create file: ", filePath);
         return;
        }
     }

// Open file in append mode (FILE_CSV | FILE_WRITE | FILE_SHARE_WRITE | FILE_ANSI)
   int fileHandle = FileOpen(filePath, FILE_READ|FILE_WRITE|FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_COMMON|FILE_CSV);

   if(fileHandle != INVALID_HANDLE)
     {
      // Move file pointer to the end for appending
      FileSeek(fileHandle, 0, SEEK_END);

      int size = TEST_ShowAll ? ArraySize(filterResults) : MathMin(ArraySize(filterResults), TEST_MaxNumberOfResult);
      for(int i = 0; i < size; i++)
        {
         KhanhdeuxResult result = filterResults[i];

         string headerValueList[];
         getParameterListByIndex(result.headers, 1, headerValueList);

         string parameterValueList[];
         getParameterListByIndex(result.parameters, 1, parameterValueList);

         string line = joinStringArray(headerValueList, ";");
         line += ";" + joinStringArray(parameterValueList, ";");

         string infoValueList[];
         for(int i=0;i<ArraySize(infoList);i++)
           {
            string name = infoList[i];
            double value = 0;
            int newSize = ArraySize(infoValueList) + 1;
            ArrayResize(infoValueList, newSize);

            if(name == "OPTIMIZATION")
              {
               value = result.optimization;
              }

            if(name == "OPTIMIZATION_MAXDOWN")
              {
               value = result.optimizationDD;
              }

            if(name == "FORWARD")
              {
               value = result.forward;
              }

            if(name == "FORWARD_MAXDOWN")
              {
               value = result.forwardDD;
              }

            if(name == "PROFIT_SUM")
              {
               value = calculateProfitSum(result);
              }

            if(name == "PROFIT_DIFF")
              {
               value = calculateProfitDifference(result);
              }

            if(name == "DRAWDOWN_SUM")
              {
               value = calculateDrawdownSum(result);
              }

            if(name == "METRIC")
              {
               value = calculateMetric(result);
              }

            infoValueList[newSize - 1] = DoubleToString(value);
           }

         line += ";" + joinStringArray(infoValueList, ";");
         FileWrite(fileHandle, line);
        }

      // Close the file
      FileClose(fileHandle);
     }
   else
     {
      Print("Failed to open file: ", filePath);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void getParameterListByIndex(string &list[], int index, string &parameterList[])
  {
   ArrayResize(parameterList, 0);
   for(int i=0;i<ArraySize(list);i++)
     {
      string item = list[i];
      string parts[];
      int parts_count = StringSplit(item, '=', parts);

      if(parts_count == 2)
        {
         int newSize = ArraySize(parameterList) + 1;
         ArrayResize(parameterList, newSize);
         parameterList[newSize - 1] = parts[index];
        }
      else
        {
         Print("Warning!! Wrong parameter", item);
        }
     }
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void deleteFile(string filePath)
  {
// Close any open file handle before deleting
   int fileHandle = FileOpen(filePath, FILE_READ | FILE_ANSI);
   if(fileHandle != INVALID_HANDLE)
     {
      FileClose(fileHandle);
     }

// Delete the file if it exists
   if(FileIsExist(filePath, FILE_COMMON))
     {
      if(FileDelete(filePath, FILE_COMMON))
        {
         Print("File deleted: ", filePath);
        }
      else
        {
         Print("Failed to delete file: ", filePath);
        }
     }
   else
     {
      Print("File does not exist: ", filePath);
     }
  }
//+------------------------------------------------------------------+
