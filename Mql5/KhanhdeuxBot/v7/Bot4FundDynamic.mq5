//+------------------------------------------------------------------+
//|                                            ClimberFundHelper.mq5 |
//|                                  Copyright 2023, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2024, Khanhdeux."
#property link      "https://www.mql5.com"
#property version   "7.00"

#include <Trade\PositionInfo.mqh>
#include <Trade\Trade.mqh>
#include <Telegram\Telegram.mqh>
#include <RegularExpressions\Regex.mqh>
#include <Arrays\ArrayLong.mqh>
#include <Daylight.mqh>
#include <News.mqh>

//--- importing required dll files
#define MT_WMCMD_EXPERTS   32851
#define WM_COMMAND 0x0111
#define GA_ROOT    2
#include <WinAPI\winapi.mqh>

int SIGNAL_BUY     =  1;
int SIGNAL_SELL    =  -1;
int SIGNAL_CLOSE   =  2;
int SIGNAL_DCA     =  3;
int SIGNAL_HEDG    =  4;
int SIGNAL_PAUSE   =  5;
int SIGNAL_NONE   =  6;
int SIGNAL_DCA_2 = 7;
int SIGNAL_DCA_3 = 8;

int CLOSE_KILL   =  1;

string COMMAND_MASTER_BUY = "/master-buy";
string COMMAND_MASTER_SELL = "/master-sell";

enum ENUM_DCA_ORDER_TYPE {DCA_BUY, DCA_SELL};
enum ENUM_SESSION_TYPE {SYDNEY, TOKYO, LONDON, NEWYORK};
enum ENUM_STRATEGY_TYPE {DCA, HEDG, DCA_2, DCA_3, NONE};
enum ENUM_RISK_TYPE {RISK_LOW, RISK_HIGH};
enum  ENUM_STRATEGY_ENTRY_TYPE
  {
   ENTRY_TYPE_RSI, // Rsi
   ENTRY_TYPE_FVG, // FVG
   ENTRY_TYPE_LAST_DAY_PRICE, // Last day price
   ENTRY_TYPE_MOVING_AVERAGE, // Moving average
   ENTRY_TYPE_SMA, // SMA
   ENTRY_TYPE_HT_CLOSE // Hightime close
  };

enum  ENUM_VOLUME_TYPE
  {
   VOL_MUL, // Multiply
   VOL_FIB,  // Fibonacci
   VOL_UNI, // Unique
  };

enum  ENUM_SIGNAL_FILTER
  {
   ONLY_BUY,      // Only buy
   ONLY_SELL,     // Only sell
   BOTH_BUY_SELL, // Both
  };

enum ENUM_NEWS_TYPE
  {
   DEFAULT,
   JOBLESS_CLAIM,
   CPI,
   SPEECH,
   NONFARM,
   GDP,
   PMI,
   GOODS,
   HOME_SALES,
   JOLTS,
   BUILDING_PERMIT,
   RETAIL_SALES,
   PPI,
   FED,
   MCS,
   CCI,
   PCE,
   OTHERS,
   CUSTOM
  };

CTrade trade;
CPositionInfo position;
COrderInfo order;

double currentBalance;
datetime dailyDate;
double htfRsi;
datetime lastBarTime = 0;

// Global constants

int    RSI_M5_BUY_LEVEL1 = 40; // Rsi M5 Buy level 1
int    RSI_M5_BUY_LEVEL2 = 35; // Rsi M5 Buy level 2
int    RSI_M5_BUY_LEVEL3 = 30; // Rsi M5 Buy level 3
int    RSI_M5_SELL_LEVEL1 = 60; // Rsi M5 Sell level 1
int    RSI_M5_SELL_LEVEL2 = 65; // Rsi M5 Sell level 2
int    RSI_M5_SELL_LEVEL3 = 70; // Rsi M5 Sell level 3
int    RSI_Overbought = 70; // Rsi Overbought
int    RSI_Oversold = 30; // Rsi Oversold

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class KhanhdeuxConfig
  {
public:
   int               index;
   string            symbol;
   ENUM_DCA_ORDER_TYPE orderType;
   double            stoploss[];
   double            lowerPrice;
   double            upperPrice;
   double            takeprofit[];
   bool              active;
   double            points[];

   //--- Default constructor
                     KhanhdeuxConfig()
     {
      active = false;
     };
   //--- Parametric constructor
                     KhanhdeuxConfig(int ind, string sym, ENUM_DCA_ORDER_TYPE ot, double &stop[], double lower, double upper, double &tp[], bool a, double &p[])
     {
      index = ind;
      symbol = sym;
      orderType = ot;
      lowerPrice = lower;
      upperPrice = upper;
      active = a;

      // Resize points and copy elements from the passed array
      ArrayResize(stoploss, ArraySize(stop));
      ArrayCopy(stoploss, stop);  // Copy elements

      ArrayResize(takeprofit, ArraySize(tp));
      ArrayCopy(takeprofit, tp);  // Copy elements

      ArrayResize(points, ArraySize(p));
      ArrayCopy(points, p);  // Copy elements
     }

   KhanhdeuxConfig*   Clone()
     {
      // Create a new object and return a pointer to it
      return new KhanhdeuxConfig(index, symbol, orderType, stoploss, lowerPrice, upperPrice, takeprofit, active, points);
     }
  };


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class KhanhdeuxEvent
  {
public:
   string            symbol;
   string            name;
   datetime          time;
   int               importanceLevel;
   bool              isClosed;
   bool              isShowed;
public:
   //--- Default constructor
                     KhanhdeuxEvent() {};
   //--- Parametric constructor
                     KhanhdeuxEvent(string s, string n, datetime t, int l, bool ic)
     {
      symbol = s;
      name = n;
      time = t;
      importanceLevel = l;
      isClosed = ic;
     }
  };

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class PendingOrder
  {
public:
   ulong             ticket;
   ENUM_ORDER_TYPE   type;
   string            symbol;
   double            lots;
   double            price;
   double            stop;
   double            take;
   string            comment;
public:
   //--- Default constructor
                     PendingOrder() {};
   //--- Parametric constructor
                     PendingOrder(ulong ticket, ENUM_ORDER_TYPE type, string symbol, double lots, double price, double stop, double take, string comment)
     {
      ticket = ticket;
      type = type;
      lots = lots;
      price = price;
      stop = stop;
      take = take;
      comment = comment;
     }
  };

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string            generalPattern = "\\/\\b(dconfig|aconfig|iconfig|dconfigs|daconfigs)\\b(?: ([0-9]+))?";
string            accountPattern = "\\/(\\d+) \\b(on|off|kill|reset|auto|manual|master|unmaster|slave|unslave|pause|unpause|close-news|unclose-news|dconfigs)\\b";
string            symbolPattern  = "\\/([A-Za-z0-9]+(?:[.+][A-Za-z0-9]*)?) \\b(reset|buy|sell|cbuy|csell|pause|unpause|cpending)\\b";
string            eventPattern  = "\\/\\b(event|unevent)\\b ([A-Za-z]{2,3}) (\d{2}:\d{2})(?: (true|false))?";
// string            configPattern  = "\\/([A-Za-z0-9]+(?:[.+][A-Za-z0-9]*)?) \\b(b|s)\\b ([0-9]+(?:[.,][0-9]+)?),([0-9]+(?:[.,][0-9]+)?) ([0-9]+(?:[.,][0-9]+)?),([0-9]+(?:[.,][0-9]+)?)(?: ([+-]?[0-9]*[.]?[0-9]+(?:,[+-]?[0-9]*[.]?[0-9]+)*))?";
string            configPattern  = "\\/([A-Za-z0-9]+(?:[.+][A-Za-z0-9]*)?) \\b(b|s)\\b r([0-9]+(?:[.,][0-9]+)?),([0-9]+(?:[.,][0-9]+)?) s([+-]?[0-9]*[.]?[0-9]+(?:,[+-]?[0-9]*[.]?[0-9]+)*) t([+-]?[0-9]*[.]?[0-9]+(?:,[+-]?[0-9]*[.]?[0-9]+)*)(?: p([+-]?[0-9]*[.]?[0-9]+(?:,[+-]?[0-9]*[.]?[0-9]+)*))?";
// level-1: all in account, level-2 : all by symbol

//+------------------------------------------------------------------+
//|   KhanhdeuxBot                                                   |
//+------------------------------------------------------------------+
class KhanhdeuxBot: public CCustomBot
  {
public:
   int               signal;
   string            signalSymbol;
   double            lotsize;
   double            distance;
   int               maxDistance;
   double            initCaptital;
   double            closePrice;
   int               closeType;
   double            alertPrice;
   bool              manual;
   bool              isActive;
   bool              isMaster;
   bool              isSlave;
   bool              isPaused;
   bool              isSymbolPaused;
   int               volumeType;
   bool              isCloseOnNews;
   KhanhdeuxConfig   config;

public:
   bool              checkGeneralPattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(generalPattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   string              setGeneralPattern(const string text)
     {
      string actionType = "";
      string message = "";

      CMatchCollection *matches=CRegex::Matches(text,generalPattern);
      CMatchEnumerator *en=matches.GetEnumerator();

      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         actionType = match.Groups()[1].Value();

         if(actionType == "dconfig")
           {
            int index = match.Groups()[2].Value();
            removeConfig(index);
           }

         if(actionType == "daconfigs")
           {
            removeActiveConfigs();
           }

         if(actionType == "dconfigs")
           {
            resetConfigs();
           }

         if(actionType == "aconfig")
           {
            int index = match.Groups()[2].Value();
            activateConfig(index);
           }

         if(actionType == "iconfig")
           {
            int index = match.Groups()[2].Value();
            deactivateConfig(index);
           }
        }

      message += "Action:" + actionType + "\n";
      message += "\n\n" + getConfigMessage();

      delete en;
      delete matches;

      return message;
     }
   bool              checkAccountPattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(accountPattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   string              setAccountPattern(const string text)
     {
      int accountLogin = 0;
      string actionType = "";
      string message = "";

      CMatchCollection *matches=CRegex::Matches(text,accountPattern);
      CMatchEnumerator *en=matches.GetEnumerator();

      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         accountLogin = match.Groups()[1].Value();
         actionType =  match.Groups()[2].Value();
        }

      int currentAccount = (int)AccountInfoInteger(ACCOUNT_LOGIN);

      if(currentAccount == accountLogin)
        {
         if(actionType == "on")
           {
            isActive = true;
           }

         if(actionType == "off")
           {
            isActive = false;
           }

         if(actionType == "kill")
           {
            closeAllPositions();
            isActive = false;
           }

         if(actionType == "reset")
           {
            closeAllPositions();
           }

         if(actionType == "auto")
           {
            manual = false;
           }

         if(actionType == "manual")
           {
            manual = true;
           }

         if(actionType == "master")
           {
            isMaster = true;
           }

         if(actionType == "unmaster")
           {
            isMaster = false;
           }

         if(actionType == "slave")
           {
            isSlave = true;
           }

         if(actionType == "unslave")
           {
            isSlave = false;
           }

         if(actionType == "pause")
           {
            isPaused = true;
           }

         if(actionType == "unpause")
           {
            isPaused = false;
           }

         if(actionType == "close-news")
           {
            isCloseOnNews = true;
           }

         if(actionType == "unclose-news")
           {
            isCloseOnNews = false;
           }

         if(actionType == "dconfigs")
           {
            resetConfigs();
           }

         message += actionType + "\n";
        }

      delete en;
      delete matches;

      return message;
     }

   bool              checkSymbolPattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(symbolPattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   string              setSymbolPattern(const string text)
     {
      string symbol = "";
      string actionType = "";
      string message = "";

      CMatchCollection *matches=CRegex::Matches(text,symbolPattern);
      CMatchEnumerator *en=matches.GetEnumerator();

      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         symbol = match.Groups()[1].Value();
         actionType =  match.Groups()[2].Value();
        }

      if(actionType == "reset")
        {
         closeAllPositions(symbol);
        }

      if(actionType == "buy")
        {
         signal = SIGNAL_BUY;
         signalSymbol = symbol;
        }

      if(actionType == "sell")
        {
         signal = SIGNAL_SELL;
         signalSymbol = symbol;
        }

      if(actionType == "cbuy")
        {
         closeAllDCAPositions(symbol, DCA_BUY);
        }

      if(actionType == "csell")
        {
         closeAllDCAPositions(symbol, DCA_SELL);
        }

      if(actionType == "pause")
        {
         isSymbolPaused = true;
         signalSymbol = symbol;
        }

      if(actionType == "unpause")
        {
         isSymbolPaused = false;
         signalSymbol = symbol;
        }

      if(actionType == "cpending")
        {
         closeAllPendingOrders(symbol);
        }

      message += "Symbol:" + symbol + ", Action:" + actionType + "\n";

      delete en;
      delete matches;

      return message;
     }

   bool              checkEventPattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(eventPattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   string              setEventPattern(const string text)
     {
      string actionType = "";
      string symbol = "";
      string time = "";
      bool   isClosed = false;
      string message = "";

      CMatchCollection *matches=CRegex::Matches(text,eventPattern);
      CMatchEnumerator *en=matches.GetEnumerator();

      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         actionType = match.Groups()[1].Value();
         symbol = match.Groups()[2].Value();
         time =  match.Groups()[3].Value();
         isClosed = (actionType == "event" && match.Groups()[4].Value() == "true") ? true : false;

         if(actionType == "event")
           {
            addTodayEvent(new KhanhdeuxEvent(symbol, "CUSTOM", createDateTimeFromString(time), 3, isClosed), todayEvents);
           }

         if(actionType == "unevent")
           {
            removeTodayEvent(symbol, createDateTimeFromString(time), todayEvents);
           }

         message += getNewsMessage();
        }

      message += "ActionType:" + actionType + " - Symbol:" + symbol + ", News at time:" + time + ". IsClosed:" + isClosed +  "\n";

      delete en;
      delete matches;

      return message;
     }

   bool              checkConfigPattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(configPattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   string              setConfigPattern(const string text)
     {
      string message = "";

      config = new KhanhdeuxConfig();
      bool isValid = false;

      CMatchCollection *matches=CRegex::Matches(text,configPattern);
      CMatchEnumerator *en=matches.GetEnumerator();

      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         config.symbol = match.Groups()[1].Value();
         config.orderType = (match.Groups()[2].Value() == "b" ?  DCA_BUY : DCA_SELL);

         config.lowerPrice = match.Groups()[3].Value();
         config.upperPrice = match.Groups()[4].Value();

         double stoploss[];
         stringToDoubleArray(match.Groups()[5].Value(), stoploss);
         ArraySort(stoploss);
         if(config.orderType == DCA_SELL)
           {
            ArrayReverse(stoploss);
           }

         ArrayResize(config.stoploss, ArraySize(stoploss));
         ArrayCopy(config.stoploss, stoploss);

         double takeprofit[];
         stringToDoubleArray(match.Groups()[6].Value(), takeprofit);
         ArraySort(takeprofit);
         if(config.orderType == DCA_SELL)
           {
            ArrayReverse(takeprofit);
           }

         ArrayResize(config.takeprofit, ArraySize(takeprofit));
         ArrayCopy(config.takeprofit, takeprofit);

         double points[];
         stringToDoubleArray(match.Groups()[7].Value(), points);

         ArraySort(points);
         if(config.orderType == DCA_SELL)
           {
            ArrayReverse(points);
           }

         ArrayResize(config.points, ArraySize(points));
         ArrayCopy(config.points, points);
        }

      string error = validateConfig(config);

      if(error == "")
        {
         addConfig(config);
        }
      else
        {
         message += error;
        }

      message += "\n\n" + getConfigMessage();

      delete en;
      delete matches;

      return message;
     }

   void              ProcessMessages(void)
     {
      for(int i=0; i<m_chats.Total(); i++)
        {
         CCustomChat *chat=m_chats.GetNodeAtIndex(i);
         //--- if the message is not processed
         if(!chat.m_new_one.done)
           {
            chat.m_new_one.done=true;
            string text=chat.m_new_one.message_text;

            if(text==NULL || text=="")
              {
               continue;
              }

            if(isMessageCommandType(text))
              {
               Sleep(Notification_Delay);
              }

            //--- start
            if(text=="/test")
               SendMessage(chat.m_id, "Test! I am KhanhdeuxBot. \xF680" + getActionMessage());

            //--- help
            if(text=="/help")
              {
               string helpText = "My commands list: ";
               helpText += "\n/test test bot";
               helpText += "\n/info account";
               helpText += "\n/news today news";
               helpText += "\n/event|unevent cad|us|gbp|eur 16:30 true|false: close on news?";
               helpText += "\n/dconfig|aconfig|iconfig|dconfigs|daconfigs. Remove config by index";
               helpText += "\n/config Config";
               helpText += "\n/on/off/pause/unpause/kill/reset/close-news/unclose-news .General setup";
               helpText += "\n/account on|off|kill|reset|auto|manual|master|unmaster|slave|unslave|pause|unpause|close-news|unclose-news|reset-config";
               helpText += "\n/symbol reset|buy|sell|cbuy|csell|pause|unpause|cpending";
               helpText += "\n/symbol b|s r[lower],[upper] s[sl1,sl2] t[tp1,tp2] p[point1,point2, ...] Eg: /XAUUSD b r0,9999 s2725.25,2725.26 t2746,2747.1 p2733.75,2735.10";

               SendMessage(chat.m_id,helpText);
              }
            if(text=="/info")
              {
               string message = "";
               message       += StringFormat("[BAL]%G",AccountInfoDouble(ACCOUNT_BALANCE)) + "";
               message       += StringFormat("[PROF]%G",AccountInfoDouble(ACCOUNT_PROFIT)) + "";
               message       += StringFormat("[EQU]%G",AccountInfoDouble(ACCOUNT_EQUITY)) + "";
               message       += StringFormat("[SL]%G", getStoploss()) + "";
               message       += StringFormat("[ALGO]%G", (bool) TerminalInfoInteger(TERMINAL_TRADE_ALLOWED)) + "";
               message       += StringFormat("[ACTIVE]%s", bot.isActive ? "true" : "false") + "";
               message       += StringFormat("[PAUSE]%s", bot.isPaused ? "true" : "false") + "";
               message       += StringFormat("[MASTER]%s", bot.isMaster ? "true" : "false") + "";
               message       += StringFormat("[SLAVE]%s", bot.isSlave ? "true" : "false") + "";
               message       += StringFormat("[CLOSEONNEWS]%s", bot.isCloseOnNews ? "true" : "false") + "";

               SendMessage(chat.m_id, message + getActionMessage());
              }

            if(text =="/news")
              {
               string message = getNewsMessage();
               SendMessage(chat.m_id, message + getActionMessage());
              }

            if(text=="/config")
              {
               string message = getConfigMessage();
               SendMessage(chat.m_id, message + getActionMessage());
              }

            if(text=="/buy" || (text==COMMAND_MASTER_BUY && bot.isSlave))
              {
               signal = SIGNAL_BUY;
               SendMessage(chat.m_id, "Received BUY order" + getActionMessage());
              }

            if(text=="/sell" || (text==COMMAND_MASTER_SELL && bot.isSlave))
              {
               signal = SIGNAL_SELL;
               SendMessage(chat.m_id, "Received SELL order" + getActionMessage());
              }

            if(text=="/pause")
              {
               isPaused = true;
               SendMessage(chat.m_id, "Received PAUSE order" + getActionMessage());
              }

            if(text=="/unpause")
              {
               isPaused = false;
               SendMessage(chat.m_id, "Received UNPAUSE order" + getActionMessage());
              }

            if(text=="/on")
              {
               SendMessage(chat.m_id, "Received ACTIVATE Algo" + getActionMessage());
               bot.isActive = true;
              }

            if(text=="/off")
              {
               SendMessage(chat.m_id, "Received DEACTIVATE Algo" + getActionMessage());
               bot.isActive = false;
              }

            if(text=="/kill" || text=="/k")
              {
               closeAllPositions();
               isActive = false;
              }

            if(text=="/reset" || text=="/r")
              {
               SendMessage(chat.m_id, "Received RESET order" + getActionMessage());
               closeAllPositions();
              }

            if(text=="/close-news" || text=="/cn")
              {
               SendMessage(chat.m_id, "Received CLOSE ON NEWS order" + getActionMessage());
               isCloseOnNews = true;
              }

            if(text=="/unclose-news" || text=="/u-n")
              {
               SendMessage(chat.m_id, "Received UNCLOSE ON NEWS order" + getActionMessage());
               isCloseOnNews = false;
              }

            if(text=="/reset-config" || text=="/u-n")
              {
               SendMessage(chat.m_id, "Received RESET-CONFIG" + getActionMessage());
               resetConfigs();
              }

            if(checkEventPattern(text))
              {
               SendMessage(chat.m_id, setEventPattern(text) + getActionMessage());
               continue;
              }

            if(checkGeneralPattern(text))
              {
               SendMessage(chat.m_id, setGeneralPattern(text) + getActionMessage());
               continue;
              }

            if(checkConfigPattern(text))
              {
               SendMessage(chat.m_id, setConfigPattern(text) + getActionMessage());
               continue;
              }

            if(checkAccountPattern(text))
              {
               SendMessage(chat.m_id, setAccountPattern(text) + getActionMessage());
               continue;
              }

            if(checkSymbolPattern(text))
              {
               SendMessage(chat.m_id, setSymbolPattern(text) + getActionMessage());
               continue;
              }
           }
        }
     }
  };

input group  "==== Telegram Credentials ==="
input string ChatID = ""; // Chat ID
input string BotToken = ""; // Bot Token
input bool TestTelegram = true; // Test Telegram Signal

input group "==== LIVE CONFIG ===="

input double  ACCOUNT_BALANCE_INIT = 100000; // Account balance init

input bool isBotActive = true; // Is Bot Active?
input bool isBotMaster = false; // Is Bot Master?
input bool isBotSlave = false; // is Bot Slave?

input group "==== BACKTEST ===="
input ENUM_VOLUME_TYPE DCA_Strategy_Volume_Type = VOL_FIB; // Volume type
input ENUM_STRATEGY_TYPE DCA_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE DCA_Strategy_Entry_Type = ENTRY_TYPE_HT_CLOSE; // Entry type
input bool             Trading_DayOfWeek_Active = false;
input ENUM_DAY_OF_WEEK Trading_DayOfWeek = ""; // Trading day of week
enum  ENUM_SESSION
  {
   SESSION_ASIA, // ASIA
   SESSION_UK, // UK
   SESSION_US // US
  };
input bool         TradingSession_Active = false;
input ENUM_SESSION TradingSession_Type = SESSION_ASIA; // Session type

input group "==== TEST Config ===="
input double  TEST_MaxDrawdown = 1.5; // Maxdrawdown in %
enum  ENUM_TEST_CRITERIA {NET_PROFIT, STOPLOSS_RATE, TAKEPROFIT_RATE, TP_BY_SL};
input ENUM_TEST_CRITERIA TEST_Criteria = STOPLOSS_RATE; // Criteria
input bool    TEST_NewReport = false; // New csv file?
input int     TEST_MaxNumberOfResult = 10; // Max number of result
input bool    TEST_ShowAll = false; // Show all result

input group  "==== News Config ==="
input bool    News_Backtest = false;  // News Backtest?
input int     News_Backtest_Year = 2024;  // News Backtest year

input bool    TradingStopNews_Active = true; // Trading Stop on News
input int     TradingStopNews_FreezeInMinutes = 5; // Freeze in minute
input bool    TradingStopNews_CloseAllPositions = false; // Close all positions

input group "==== Entry time ===="
input bool    TradingEntryTime_Active = false; // Trading Entry time
input int     TradingEntryTime_HourStart = 6; // Entry Hour Start
input int     TradingEntryTime_HourStop = 18; // Entry Hour Stop

input group  "==== Trade time ==="
input bool    TradingGo_Active = false; // Trading Go active (GMT+2(w),+3(s))
input int     TradingGo_HourStart = 6; // Go Hour Start
input int     TradingGo_HourStop = 20; // Go Hour Stop
input bool    TradingGo_CloseAllPositions = true; // Close all positions outside Go Hour

input group  "==== No trade time ==="
input bool    TradingStop_Active = false; // Trading Stop active (GMT+2(w),+3(s))
input int     TradingStop_HourStart = 0; // Stop Hour Start
input int     TradingStop_HourStop = 1; // Stop Hour Stop
input bool    TradingStop_CloseAllPositions = true; // Close all positions before Stop Hour

input group  "==== Session Config ==="
input bool    Session_Active = false; // Session active?

input string  SEPARATOR22 = ""; // ----ASIA----
input int    ASIA_HourStart = 0; // Start hour
input int    ASIA_HourStop = 6; // Stop hour

input string  SEPARATOR23 = ""; // ----UK---
input int    UK_HourStart = 6; // Start hour
input int    UK_HourStop = 19; // Stop hour

input string  SEPARATOR24 = ""; // ----US---
input int    US_HourStart = 19; // Start hour
input int    US_HourStop = 24; // Stop hour

input bool   Session_CloseAllPositions = true; // Close all positions after session

input group  "==== Trading Stop Daily ==="
input bool    TradingStoplossDaily_Active    = true; // Trading Stoploss Daily
input bool    TradingStopDaily_Active        = false; // Trading Stop Daily (23:40)
input double  TradingStopDaily  = 4.8;  // Max daily stoploss (E.g 5%)
input double  TradingStopLoss  = 1;     // Max stoploss (E.g 5%)

input group  "==== Trading Profit Daily ==="
input bool    TradingProfitDaily_Active = false; // Trading Profit Daily
input double  TradingProfitDaily  = 3;  // Max daily takeprofit (E.g 5%)
input bool    TradingStopProfit_Active = false; // Trading Stop Profit
input double  TradingStopProfit  = 0.5;  // Max Stop profit (E.g 0.5%)
input bool    TradingLimitProfit_Active = false; // Trading Limit Profit
input int     TradingLimitProfit_Number = 0; // Number on Limit Profit

input group "==== Holding time ===="
input double POSITION_HOLDING_Factor = 0.1; // Holding factor (Holding time in hour = dist * holding factor)
input bool   POSITION_HOLDING_Stoploss = true; // Set stoploss or close all

input group "==== Timeframe ===="
enum CUSTOM_TIMEFRAMES
  {
   TF_M1   = PERIOD_M1,  // M1
   TF_M5   = PERIOD_M5,  // M5
   TF_M15  = PERIOD_M15, // M15
   TF_H1   = PERIOD_H1,  // H1
   TF_H4   = PERIOD_H4,  // H4
   TF_D1   = PERIOD_D1   // D1
  };

input CUSTOM_TIMEFRAMES   Timeframe = TF_M1;
input CUSTOM_TIMEFRAMES   Timeframe_High = TF_M5;

input group  "==== Next Order Config ==="
input bool              NextOrderWhenCandleClosed = false; // Next order only when candle closed?
input CUSTOM_TIMEFRAMES NextOrder_Timeframe = TF_M1; // Candle timeframe?

input group  "==== Notification ==="
input bool    Notification_Active = true; // Notification active
input int     Notification_Delay = 3000; // Notification delay
input bool    Warning_Signal = false; // Notification signal
input bool    Warning_Rsi = false; // Rsi Warning?
input bool    Warning_Session = false; // Session Warning?
input bool    Warning_News = false; // News Warning?
input bool    Warning_Cci = false; // CCi Warning?
input bool    Warning_FVG = false; // FVG Warning?

input group  "==== RSI ==="
input int    RSI_Length = 14; // RSI Length
input int    RSI_NumOfCandles = 5; // Number of RSI candles
input int    RSI_LastEntry_NumOfCandles = 5; // Rsi LastEntry Candles

input group  "==== Filter ==="
input ENUM_SIGNAL_FILTER DCA_Signal_Filter = BOTH_BUY_SELL; // Signal Filter

input group  "==== DCA Config ==="
input ulong   DCA_Strategy_MagicNumber = 01; // Magic Number

input group "==== FVG Config ===="
input int     FVG_NumOfCandles = 4; // FVG Number of candles

input group  "==== Hightime close Config ==="
input CUSTOM_TIMEFRAMES   HTC_Timeframe = TF_H1;

input group "==== SMA Config ===="
input int SMA_FastPeriod = 15; // Fast period
input int SMA_SlowPeriod = 30; // Slow period
input int SMA_TrendPeriod = 0; // Trend period

// input group  "==== Server time ==="
int     Server_Offset = 0; // Server offset

// input group  "==== DCA Analysis ==="
bool   DCA_Matrix = false; // Show DCA Matrix
double DCA_MinNumOfDistance = 1; // Number of min Level
double DCA_MaxNumOfDistance = 10; // Number of max Level
int    DCA_LotSize_Multiply = 2; // Lotsize multiply

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
KhanhdeuxBot      bot;
int               getmeResult;
int               numOfPositions;
KhanhdeuxEvent todayEvents[];
datetime todayDate;
KhanhdeuxConfig configs[];
string symbols[];
PendingOrder pendingOrders[];

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               OnInit()
  {
   Print("=== INIT === ", TimeCurrent());
   //testConfigs();
//testPattern();

   numOfPositions = PositionsTotal();

   if(ChatID != "" && BotToken != "")
     {
      if(TestTelegram && Notification_Active)
        {
         string message = StringFormat("%s %d", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN)) + "%0A" + getSymbolMessage();
         SendMessage(message, ChatID, BotToken);
        }

      //--- set token
      bot.Token(BotToken);
      //--- check token
      getmeResult=bot.GetMe();
     }

   EventSetTimer(10);

   trade.SetExpertMagicNumber(DCA_Strategy_MagicNumber);

   bot.isActive = isBotActive;
   bot.isMaster = isBotMaster;
   bot.isSlave  = isBotSlave;

   if(News_Backtest)
     {
      if(!MQLInfoInteger(MQL_TESTER))
        {
         deleteFile("news\\newshistory.bin");
        }
      news.date_from = getFirstDayOfNewsYear();
      news.date_to = getLastDayOfNewsYear();
      news.SaveHistory(true);
      news.LoadHistory(true);
     }

//---
   return(INIT_SUCCEEDED);
  }

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void              OnDeinit(const int reason)
  {
//--- destroy timer
   EventKillTimer();
   bot.signal = 0;
   CRegex::ClearCache();
   ArrayFree(todayEvents);
   ArrayResize(todayEvents, 0);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void              OnTick()
  {
   executeEvents();
   executeTelegramBot();

   for(int i = 0; i < ArraySize(symbols); i++)
     {
      executeDCATrading(symbols[i]);
      executeWarning(symbols[i]);
     }

   executeReset();
  }

//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
bool isOnTimerLoaded = false;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              OnTimer()
  {
   if(ChatID != "" && BotToken != "" && Notification_Active)
     {
      SendPositionsChanged();
      SendMessageToNews();
     }

// showLiveNews();
   isOnTimerLoaded = true;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void testConfigs()
  {
// 11.01-11.05
//bot.setConfigPattern("/XAUUSD b 3 2744.00,2750.85,2745.90,2747.55");
//bot.setConfigPattern("/XAUUSD s 5 2757.00,2740.85,2745.90,2747.55");
//bot.setConfigPattern("/XAUUSD b 5 2725.95,2745.35,2726.55,2733.60");
//bot.setConfigPattern("/US2000.cash b 5 2187.00,2198.85,0,9999");
//bot.setConfigPattern("/GBPUSD b 4 1.2888,2,1.2888,2");
//bot.setConfigPattern("/USDJPY b 5 151.575,999,151.575,999");

   Print(bot.setConfigPattern("/XAUUSD b r0,9999 s2725.25,2725.26 t2746,2747.1 p2733.75,2735.10")); // 2024.10.28
   Print(bot.setConfigPattern("/XAUUSD s r0,9999 s2741,2740 t2725 p2739,2729")); // 2024.10.28

// bot.setGeneralPattern("/dconfig 2");
// bot.setGeneralPattern("/reset-config");

// bot.setConfigPattern("/XAUUSD d 5");
// removeConfig(2);

   Print("=== config list ===");
   Print(getConfigMessage());
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void testEvents()
  {
   Print(bot.setEventPattern("/event us 02:15")); // 2024.10.28

   Print("=== event ===");
   Print(getNewsMessage());
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int setListOfUniqueSymbols(string &uniqueSymbols[])
  {
   int uniqueCount = 0;

   for(int i = 0; i < ArraySize(configs); i++)
     {
      string symbol = configs[i].symbol;
      bool isUnique = true;

      // Check if the symbol is already in the uniqueSymbols array
      for(int j = 0; j < uniqueCount; j++)
        {
         if(uniqueSymbols[j] == symbol)
           {
            isUnique = false;
            break;
           }
        }

      // If the symbol is unique, add it to the uniqueSymbols array
      if(isUnique)
        {
         ArrayResize(uniqueSymbols, uniqueCount + 1);
         uniqueSymbols[uniqueCount] = symbol;
         uniqueCount++;
        }
     }

   return uniqueCount; // Return the number of unique symbols
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string getSymbol()
  {
   return _Symbol;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getSymbolIndex(string symbol)
  {
   for(int i = 0; i < ArraySize(symbols); i++)
     {
      if(symbols[i] == symbol)
        {
         return i;
        }
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              testPattern()
  {
// string pattern= eventPattern;
// string in="/event USD 15:30 true";

//string pattern= configPattern;
//string in="/XAUUSD b 3 2744.00,2750.85,2745.90,2747.55";

   string pattern= configPattern;
// string in="/XAUUSD b 0,9999 2720.23,2746.02 2733.213,2728.22";
// /XAUUSD b 0,9999 2720,2746 2733,2728
   string in="/XAUUSD b r0,9999 s2720.23 t2746.02,2746.03 p2733.213,2728.22";
// /XAUUSD b 0,9999 2720 2746 2733,2728

   CRegex *r=new CRegex(pattern,RegexOptions::IgnoreCase);
   CMatch *m=r.Match(in);

   if(m.Success())
     {
      CMatchCollection *matches=CRegex::Matches(in,pattern);
      CMatchEnumerator *en=matches.GetEnumerator();
      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         Print("1: " + match.Groups()[1].Value());
         Print("2: " + match.Groups()[2].Value());
         Print("3: " + match.Groups()[3].Value());
         Print("4: " + match.Groups()[4].Value());
         Print("5: " + match.Groups()[5].Value());
         Print("6: " + match.Groups()[6].Value());
         Print("7: " + match.Groups()[7].Value());

         string numbers[];
         string numbersString = match.Groups()[5].Value();
         StringSplit(numbersString, ',', numbers);
         if(ArraySize(numbers))
           {
            Print("5a: " + numbers[0]);
           }

         numbersString = match.Groups()[7].Value();
         StringSplit(numbersString, ',', numbers);
         if(ArraySize(numbers))
           {
            Print("7a: " + numbers[0]);
           }



         Print("\0");
        }

      delete en;
      delete matches;
     }

   delete r;
   delete m;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getDailyRange(string symbol)
  {
   MqlRates rate[];
   CopyRates(symbol,PERIOD_D1,0,1,rate);
   double high = rate[0].high;
   double low = rate[0].low;

   return high - low;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkPauseAction(string symbol = "")
  {
   if(bot.isPaused)
     {
      return true;
     }

   if(bot.isSymbolPaused && symbol == bot.signalSymbol)
     {
      return true;
     }

   return false;
  }

// NEWS AREA
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CNews             news;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool checkNews(bool checkForClosed, string symbol = "")
  {
   int offset = TradingStopNews_FreezeInMinutes * 60;
   datetime currentTime = TimeCurrent();

   for(int i = 0; i < ArraySize(todayEvents); i++)
     {
      string symbolInLowerCase = symbol;
      string eventSymBolInLowerCase = todayEvents[i].symbol;

      StringToLower(symbolInLowerCase);
      StringToLower(eventSymBolInLowerCase);

      // Check if the symbol matches or if we are checking for all symbols (symbol == "")
      if((StringFind(symbolInLowerCase, eventSymBolInLowerCase) != -1 || symbol == "")
         && currentTime + offset >= todayEvents[i].time  // Check if within upper time bound
         && currentTime - offset <= todayEvents[i].time  // Check if within lower time bound
        )
        {
         // If `checkForClosed` is true, check if the event is marked as closed
         if(checkForClosed)
           {
            if(todayEvents[i].isClosed)
               return true;
           }
         else
           {
            // Otherwise, just return true for stop news
            return true;
           }
        }
     }

   return false;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              addTodayEvent(KhanhdeuxEvent& event, KhanhdeuxEvent& todayEvents[])
  {
   bool eventExists = false;

   for(int i = 0; i < ArraySize(todayEvents); i++)
     {
      // Check if the symbol and name match
      if(todayEvents[i].symbol == event.symbol && todayEvents[i].name == event.name)
        {
         todayEvents[i].time = event.time;
         todayEvents[i].isClosed = event.isClosed;
         todayEvents[i].isShowed = false;
         eventExists = true;
         break;
        }
     }

   if(!eventExists)
     {
      int size = ArraySize(todayEvents);
      ArrayResize(todayEvents, size + 1);
      todayEvents[size] = event; // Add the new event
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void removeTodayEvent(string symbol, datetime eventTime, KhanhdeuxEvent& todayEvents[])
  {
   int i = 0;
   while(i < ArraySize(todayEvents))
     {
      // Check if the symbol and time match
      if(todayEvents[i].symbol == symbol && todayEvents[i].time == eventTime)
        {
         // Shift the array to the left starting from the current index
         for(int j = i; j < ArraySize(todayEvents) - 1; j++)
           {
            todayEvents[j] = todayEvents[j + 1];
           }
         ArrayResize(todayEvents, ArraySize(todayEvents) - 1); // Resize array to remove the last element
        }
      else
        {
         i++; // Only increment if no deletion, as deleting shifts the array
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              addConfig(KhanhdeuxConfig& config)
  {
   resetConfigIndices();

   int size = ArraySize(configs);
   config.index = size + 1;
   ArrayResize(configs, size + 1);
   configs[size] = config;

   resetSymbols();
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void removeConfig(int index)
  {
   int i = 0;
   while(i < ArraySize(configs))
     {
      // Check if the symbol and time match
      if(configs[i].index == index)
        {
         // Shift the array to the left starting from the current index
         for(int j = i; j < ArraySize(configs) - 1; j++)
           {
            configs[j] = configs[j + 1];
           }
         ArrayResize(configs, ArraySize(configs) - 1); // Resize array to remove the last element
        }
      else
        {
         i++; // Only increment if no deletion, as deleting shifts the array
        }
     }

   resetConfigIndices();
   resetSymbols();
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void resetConfigIndices()
  {
   for(int i = 0; i < ArraySize(configs); i++)
     {
      configs[i].index = i + 1; // Reassign index to sequential order
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void resetConfigs()
  {
   ArrayResize(configs, 0);
   resetSymbols();
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void removeActiveConfigs()
  {
   int newSize = 0;

// Iterate through the configs array and keep only inactive configurations
   for(int i = 0; i < ArraySize(configs); i++)
     {
      if(!configs[i].active)
        {
         // Move the inactive configuration to the new position
         configs[newSize] = configs[i];
         newSize++;
        }
     }

// Resize the array to the new size containing only inactive configurations
   ArrayResize(configs, newSize);

   resetConfigIndices();
   resetSymbols();
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void resetSymbols()
  {
   ArrayResize(symbols, 0);
   setListOfUniqueSymbols(symbols);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string              validateConfig(KhanhdeuxConfig& config)
  {
   string error = "";

   if(config.orderType == DCA_BUY)
     {
      for(int i=0;i<ArraySize(config.points);i++)
        {
         double point = config.points[i];
         double sl    = config.stoploss[MathMin(i+1, ArraySize(config.stoploss) - 1)];
         double tp    = config.takeprofit[MathMin(i+1, ArraySize(config.takeprofit) - 1)];

         if(sl >= point)
           {
            error += "\nError BUY: Stoploss:" + sl + " >= Point:" + point;
           }

         if(point >= tp)
           {
            error += "\nError BUY: Point:" + point + " >= Takeprofit:" + tp;
           }

         if(sl >= tp)
           {
            error += "\nError BUY: Stoploss:" + sl + " >= Takeprofit:" + tp;
           }
        }
     }

   if(config.orderType == DCA_SELL)
     {
      for(int i=0;i<ArraySize(config.points);i++)
        {
         double point = config.points[i];
         double sl    = config.stoploss[MathMin(i+1, ArraySize(config.stoploss) - 1)];
         double tp    = config.takeprofit[MathMin(i+1, ArraySize(config.takeprofit) - 1)];

         if(sl <= point)
           {
            error += "\nError SELL: Stoploss:" + sl + " <= Point:" + point;
           }

         if(point <= tp)
           {
            error += "\nError SELL: Point:" + point + " <= Takeprofit:" + tp;
           }

         if(sl <= tp)
           {
            error += "\nError SELL: Stoploss:" + point + " <= Takeprofit:" + tp;
           }
        }
     }

   if(config.lowerPrice >= config.upperPrice)
     {
      error += "\nError: LowerPrice:" + config.lowerPrice + " >= UpperPrice:" + config.upperPrice;
     }

   return error;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int country_ids[] = {840, 999, 826, 124, 392, 36, 554};          // List of country IDs (US, EUR, GBP, CAD, JPY)
string country_codes[] = {"us", "eur", "gbp", "cad", "jpy", "aud", "nzd"}; // Associated country codes

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void setTodayEvents(KhanhdeuxEvent& todayEvents[])
  {
   int count = 0;
   int required_importance = 3; // Set importance level to 3

   datetime currentTime = TimeCurrent();
   datetime currentDate = StringToTime(TimeToString(currentTime, TIME_DATE));
   datetime nextDate = StringToTime(TimeToString(currentTime + 24 * 60 * 60, TIME_DATE));

   if(News_Backtest)
     {
      int totalnews = ArraySize(news.event);
      for(int i = 0; i < totalnews; i++)
        {
         int country_id = news.event[i].country_id;
         int event_importance = news.event[i].importance;

         for(int j = 0; j < ArraySize(country_ids); j++)
           {
            if(country_id == country_ids[j] && event_importance >= required_importance
               && currentDate <= news.event[i].time && news.event[i].time < nextDate)
              {
               bool eventExists = false;
               for(int k = 0; k < ArraySize(todayEvents); k++)
                 {
                  if(StringCompare(todayEvents[k].name, news.eventname[i]) == 0)
                    {
                     eventExists = true;
                     break;
                    }
                 }

               if(!eventExists)
                 {
                  ArrayResize(todayEvents, count + 1);
                  datetime eventTime = isSummerTime() ? news.event[i].time : news.event[i].time - 1 * 60 * 60;
                  todayEvents[count++] = new KhanhdeuxEvent(country_codes[j], news.eventname[i], eventTime, event_importance, false);
                 }
              }
           }
        }
     }
   else
     {
      MqlCalendarEvent event;
      MqlCalendarValue values[];

      if(CalendarValueHistory(values, currentDate, nextDate))
        {
         for(int i = 0; i < ArraySize(values); i++)
           {
            if(CalendarEventById(values[i].event_id, event))
              {
               int country_id = event.country_id;
               int event_importance = event.importance;

               for(int j = 0; j < ArraySize(country_ids); j++)
                 {
                  if(country_id == country_ids[j]
                     && event_importance >= required_importance
                     // && getDailyNewsType(event.name)

                    )
                    {
                     bool eventExists = false;
                     for(int k = 0; k < ArraySize(todayEvents); k++)
                       {
                        if(StringCompare(todayEvents[k].name, event.name) == 0)
                          {
                           eventExists = true;
                           break;
                          }
                       }

                     if(!eventExists)
                       {
                        ArrayResize(todayEvents, count + 1);
                        todayEvents[count++] = new KhanhdeuxEvent(country_codes[j], event.name, values[i].time, event_importance, false);
                       }
                    }
                 }
              }
           }
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ENUM_NEWS_TYPE getDailyNewsType(string eventName)
  {
   if(StringCompare(eventName, "Core CPI m/m") == 0)
     {
      return CPI;
     }

   if(StringCompare(eventName, "Fed Chair Powell Speech") == 0)
     {
      return SPEECH;
     }

   if(StringCompare(eventName, "Nonfarm Payrolls") == 0
// || StringCompare(eventName, "ADP Nonfarm Employment Change") == 0
     )
     {
      return NONFARM;
     }

   if(StringCompare(eventName, "GDP q/q") == 0)
     {
      return GDP;
     }

   if(StringCompare(eventName, "S&P Global Services PMI") == 0
      || StringCompare(eventName, "ISM Non-Manufacturing PMI") == 0
      || StringCompare(eventName, "ISM Manufacturing PMI") == 0)
     {
      return PMI;
     }

   if(StringCompare(eventName, "Core Durable Goods Orders m/m") == 0)
     {
      return GOODS;
     }

   if(StringCompare(eventName, "Existing Home Sales") == 0
      || StringCompare(eventName, "Pending Home Sales m/m") == 0
     )
     {
      return HOME_SALES;
     }

   if(StringCompare(eventName, "JOLTS Job Openings") == 0)
     {
      return JOLTS;
     }

//if(StringCompare(eventName, "Building Permits") == 0)
//  {
//   return BUILDING_PERMIT;
//  }

   if(StringCompare(eventName, "Retail Sales m/m") == 0)
     {
      return RETAIL_SALES;
     }

   if(StringCompare(eventName, "PPI m/m") == 0)
     {
      return PPI;
     }

   if(StringCompare(eventName, "Fed Interest Rate Decision") == 0
      || StringCompare(eventName, "Fed Chair Powell Testimony") == 0
      || StringCompare(eventName, "Fed Chair Powell Speech") == 0
      || StringCompare(eventName, "FOMC Press Conference") == 0)
     {
      return FED;
     }

   if(StringCompare(eventName, "CB Consumer Confidence Index") == 0)
     {
      return CCI;
     }

   if(StringCompare(eventName, "Michigan Consumer Sentiment") == 0)
     {
      return MCS;
     }

//if(StringCompare(eventName, "Initial Jobless Claims") == 0)
//  {
//   return JOBLESS_CLAIM;
//  }

   if(StringCompare(eventName, "Core PCE Price Index m/m") == 0)
     {
      return PCE;
     }

   if(StringCompare(eventName, "Employment Cost Index q/q") == 0)
     {
      return OTHERS;
     }

   if(StringCompare(eventName, "CUSTOM") == 0)
     {
      return CUSTOM;
     }

   return NULL;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void showTodayEvents()
  {
   for(int i=0; i<ArraySize(todayEvents); i++)
     {
      Print("===",todayEvents[i].symbol, "-", todayEvents[i].name,"(", todayEvents[i].importanceLevel, "|", todayEvents[i].time, ")-closed=", todayEvents[i].isClosed);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void showLiveNews()
  {
   MqlDateTime STime;
   datetime time_current=TimeCurrent(STime);
   string dayOfWeek = EnumToString((ENUM_DAY_OF_WEEK)STime.day_of_week);

   datetime currentTime = TimeCurrent();

   for(int i=0; i<ArraySize(todayEvents); i++)
     {
      if(// getDailyNewsType(todayEvents[i].name) &&
         currentTime + 60 >= todayEvents[i].time
         && currentTime <= todayEvents[i].time)
        {
         Print("===",todayEvents[i].symbol, "-", todayEvents[i].name,"(", todayEvents[i].importanceLevel, "|", todayEvents[i].time, ") - closed=", todayEvents[i].isClosed);
        }
     }
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime getCurrentNewsTime()
  {
   datetime currentTime = isSummerTime() ? TimeCurrent() - 1 * 60 * 60 : TimeCurrent();
   MqlDateTime STime;
   datetime time_current=TimeCurrent(STime);
   return (currentTime - STime.sec);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void closeAllWhenSessionsExpired(string symbol)
  {
   ENUM_DCA_ORDER_TYPE types[] = {DCA_BUY, DCA_SELL};
   datetime currentTime = TimeCurrent();

   for(int i=0; i<ArraySize(types); i++)
     {
      datetime tradingTime = getFirstDCATime(symbol, types[i]);
      if(tradingTime < getTradingSession(LONDON, true) && currentTime > getTradingSession(NEWYORK, true))
        {
         closeAllDCAPositions(symbol, types[i]);
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void showSessions()
  {
   datetime currentTime = getCurrentNewsTime();
   bool sessionStart = false;
   ENUM_SESSION_TYPE types[] = {SYDNEY, TOKYO, LONDON, NEWYORK};

   for(int i=0; i<ArraySize(types); i++)
     {
      if(currentTime == getTradingSession(types[i], true))
        {
         Print("===SESSION:", EnumToString(types[i]));
        }

      if(currentTime == getTradingSession(types[i], false))
        {
         Print("===END-SESSION:", EnumToString(types[i]));
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkStopDaily()
  {
   datetime currentTime = TimeCurrent() + Server_Offset * 60 * 60;
   datetime nextDay = StringToTime(TimeToString(currentTime, TIME_DATE)) + 24 * 60 * 60;
   int timeRange = 20 * 60;

   if((nextDay - timeRange) < currentTime && currentTime < nextDay + timeRange)
     {
      return true;
     }

   return false;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              executeTelegramBot()
  {
   if(ChatID != "" && BotToken != "")
     {
      //--- show error message end exit
      if(getmeResult!=0)
        {
         Comment("Error: ",GetErrorDescription(getmeResult));
         return;
        }

      //--- show bot name
      Comment(bot.Name(), ":", DCA_Strategy_MagicNumber);
      //--- reading messages
      bot.GetUpdates();
      //--- processing messages
      bot.ProcessMessages();
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getTotalProfit(string symbol = "")
  {
   double profit = 0;
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      ulong ticket = PositionGetTicket(i);
      string positionSymbol=PositionGetSymbol(i);

      if(ticket > 0 && (positionSymbol == symbol || symbol == ""))
        {
         profit += PositionGetDouble(POSITION_PROFIT);
        }
     }

   return profit;
  }

//+------------------------------------------------------------------+
//| Close all positions                                              |
//+------------------------------------------------------------------+
void              closeAllPositions(string symbol = "")
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && (PositionGetSymbol(i) == symbol || symbol == "")
        )
        {
         trade.PositionClose(position.Ticket());
         Print(StringFormat("Close Position with lotsize=%g", PositionGetDouble(POSITION_VOLUME)));
        }
     }

   closeAllPendingOrders(symbol);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              closeLastPosition(string symbol)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
        )
        {
         trade.PositionClose(position.Ticket());
         return;
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              closeAllPositionsAtPrice(string symbol, double aPrice, int cType)
  {
   bool isClosed = false;

   MqlRates rate[];
   CopyRates(symbol,(ENUM_TIMEFRAMES)Timeframe,0,2,rate);

   double currentPrice = rate[1].close;
   double previousPrice = rate[0].close;

   if(previousPrice <= aPrice && aPrice <= currentPrice)
     {
      isClosed = true;
     }

   if(currentPrice <= aPrice && aPrice <= previousPrice)
     {
      isClosed = true;
     }

   if(isClosed)
     {
      closeAllPositions();
      if(cType == CLOSE_KILL)
        {
         algoTradingToggle(false);
         bot.isActive = false;
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getRsiOversold()
  {
   return RSI_Oversold;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getRsiOverbought()
  {
   return RSI_Overbought;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            calculateTotalVolume(int i, double lotSize, ENUM_VOLUME_TYPE volumeType)
  {
   double totalLotSize = 0;

   for(int j=1; j<=i; j++)
     {
      totalLotSize += calculateDCALotSize(j, lotSize, volumeType);
     }

   return totalLotSize;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            calculateMaxProfit(string symbol, int i, double lotSize, double distance, ENUM_VOLUME_TYPE volumeType)
  {
   double totalProfit = 0;
   double pips = distance * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10;

   for(int j=1; j<=i; j++)
     {
      totalProfit += pips * calculateDCALotSize(j, lotSize, volumeType) * SymbolInfoDouble(symbol, SYMBOL_TRADE_CONTRACT_SIZE);
     }

   return totalProfit;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getRequestSignal(string symbol)
  {
   ENUM_STRATEGY_TYPE strategyType = DCA_Strategy_Type;

   if(strategyType == DCA)
      return SIGNAL_DCA;
   if(strategyType == HEDG)
      return SIGNAL_HEDG;
   if(strategyType == DCA_2)
      return SIGNAL_DCA_2;
   if(strategyType == DCA_3)
      return SIGNAL_DCA_3;
   if(strategyType == NONE)
      return SIGNAL_NONE;

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void executeEvents()
  {
   datetime currentDate = StringToTime(TimeToString(TimeCurrent(), TIME_DATE));

   if(todayDate != currentDate)
     {
      ArrayResize(todayEvents, 0);
      setTodayEvents(todayEvents);
      //testEvents();
      todayDate = currentDate;
     }
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              executeDCATrading(string symbol)
  {
   int requestSignal = getRequestSignal(symbol);

   datetime currentDate = StringToTime(TimeToString(TimeCurrent(), TIME_DATE));

   if(TradingStoplossDaily_Active && checkDailyStoploss())
     {
      if(dailyDate != currentDate)
        {
         Print(StringFormat("Stoploss exeeded = %G", getDailyStoploss()));
         dailyDate = currentDate;
        }

      closeAllPositions();
      return;
     }

   if(checkPauseAction(symbol))
     {
      return;
     }

   if(TradingProfitDaily_Active && checkDailyProfit())
     {
      closeAllPositions();

      if(dailyDate != currentDate)
        {
         Print(StringFormat("Takeprofit exeeded = %G", getDailyProfit()));
         dailyDate = currentDate;
        }

      return;
     }

   if(TradingStopDaily_Active && checkStopDaily())
     {
      closeAllPositions();
      return;
     }

   if(TradingStop_Active && checkTradingStopHour())
     {
      if(TradingStop_CloseAllPositions)
        {
         closeAllPositions();
        }
      return;
     }

   if(TradingStopProfit_Active && checkStopProfit())
     {
      closeAllPositions();
      return;
     }

   if(TradingStopNews_Active && checkNews(false, symbol))
     {
      if(TradingStopNews_CloseAllPositions || bot.isCloseOnNews || checkNews(true, symbol))
        {
         closeAllPositions(symbol);
        }
      else
        {
         // closeAllStopLossWhenStopNews(symbol);
         closeAllPendingOrders(symbol);
        }
      return;
     }

   if(Session_Active && !checkSession())
     {
      if(Session_CloseAllPositions)
        {
         closeAllPositions();
        }
      return;
     }

   if(!checkDayOfWeek())
     {
      return;
     }

   if(!checkTradingSession())
     {
      return;
     }

   if(!bot.isActive)
     {
      return;
     }

//if(getTotalDCAOrders(symbol) > 0)
//  {
//   return;
//  }

   executeTrading(symbol);
   return;

   ENUM_DCA_ORDER_TYPE orderTypes[];
   setOrderTypesByRequestSignal(symbol, orderTypes, requestSignal);

   for(int i=0; i < ArraySize(orderTypes); i++)
     {
      ENUM_DCA_ORDER_TYPE orderType = orderTypes[i];
      int totalPositions = getTotalDCAPositions(symbol, orderType);

      //if(totalPositions > 1)
      //  {
      //   // double tp = calculateDCABreakEvenStoploss(symbol, orderType);
      //   double tp = calculateDCATakeProfit(symbol, orderType);
      //   updateAllDCAPositionTakeProfit(symbol, tp, orderType);
      //  }

      int signal = 0;
      if(totalPositions == 0)
        {
         signal = orderType == DCA_BUY ? SIGNAL_BUY : SIGNAL_SELL;
        }
      else
        {
         signal = getNextDCASignal(symbol, orderType);
        }

      if(signal)
        {
         int newTotalPositions = totalPositions + 1;
         bool valid = false;

         double price = getCurrentPrice(symbol, orderType);
         double initPrice = (totalPositions == 0) ? price : getFirstDCAPrice(symbol, orderType);
         KhanhdeuxConfig* config = findConfig(symbol, initPrice, orderType);

         if(config == NULL)
           {
            return;
           }
         else
           {
            if(totalPositions == 0 && config.active == true)
              {
               return;
              }
           }

         // double lotSize = calculateDCALotSize(newTotalPositions, initPrice, config);
         // double tp = config.takeprofit;
         // double sl = config.stoploss;

         double lotSize = calculateDCALotSize(initPrice, config);
         double sl = config.stoploss[MathMin(totalPositions, ArraySize(config.stoploss) - 1)];
         double tp = config.takeprofit[MathMin(totalPositions, ArraySize(config.takeprofit) - 1)];

         string comment = "";

         if(signal == SIGNAL_BUY && checkValidLotSize(symbol, lotSize, orderType) && checkValidEntry(price, sl, tp, orderType))
           {
            comment = BUY_STRING + ":" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions;
            trade.Buy(lotSize, symbol, price, sl, tp, comment);
            valid = true;
           }

         if(signal == SIGNAL_SELL && checkValidLotSize(symbol, lotSize, orderType) && checkValidEntry(price, sl, tp, orderType))
           {
            comment = SELL_STRING + ":" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions;
            trade.Sell(lotSize, symbol, price, sl, tp, comment);
            valid = true;
           }

         if(valid)
           {
            activateConfig(config.index);
            Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, price, sl, tp, comment));
           }
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void executeTrading(string symbol)
  {
   ENUM_DCA_ORDER_TYPE orderTypes[];

   int buyTotal = getTotalDCAPositions(symbol, DCA_BUY);
   int sellTotal = getTotalDCAPositions(symbol, DCA_SELL);
   int entrySignal = getEntrySignal(symbol);
   int count = 0;

   if(buyTotal == 0)
     {
      //closeAllPendingOrders(symbol, DCA_BUY);

      if(entrySignal == SIGNAL_BUY)
        {
         ArrayResize(orderTypes, count + 1);
         orderTypes[count++] = DCA_BUY;
        }
     }

   if(sellTotal == 0)
     {
      //closeAllPendingOrders(symbol, DCA_SELL);

      if(entrySignal == SIGNAL_SELL)
        {
         ArrayResize(orderTypes, count + 1);
         orderTypes[count++] = DCA_SELL;
        }
     }

   for(int i=0; i < ArraySize(orderTypes); i++)
     {
      ENUM_DCA_ORDER_TYPE orderType = orderTypes[i];

      double price = getCurrentPrice(symbol, orderType);
      KhanhdeuxConfig* config = findConfig(symbol, price, orderType);

      if(config == NULL || config.active == true)
        {
         return;
        }

      double lotSize = calculateDCALotSize(price, config);

      if(checkValidLotSize(symbol, lotSize, orderType) && checkValidEntry(price, config.stoploss[0], config.takeprofit[0], orderType))
        {
         string commentPrefix = (orderType == DCA_BUY ? BUY_STRING : SELL_STRING) + ":" + DCA_Strategy_MagicNumber +  "|L";
         if(orderType == DCA_BUY)
           {
            trade.Buy(lotSize, symbol, price, config.stoploss[0], config.takeprofit[0], commentPrefix + "1");
           }
         else
           {
            trade.Sell(lotSize, symbol, price, config.stoploss[0], config.takeprofit[0], commentPrefix + "1");
           }

         activateConfig(config.index);
         Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, price, config.stoploss[0], config.takeprofit[0], commentPrefix + "1"));

         for(int i = 0; i < ArraySize(config.points); i++)
           {
            double point = config.points[i];
            double sl    = config.stoploss[MathMin(i+1, ArraySize(config.stoploss) - 1)];
            double tp    = config.takeprofit[MathMin(i+1, ArraySize(config.takeprofit) - 1)];
            string comment = commentPrefix + (i+2);

            if(!checkValidEntry(point, sl, tp, orderType))
              {
               continue;
              }

            if(orderType == DCA_BUY)
              {
               if(point > price)
                 {
                  trade.BuyStop(lotSize, point, symbol, sl, tp, 0, 0, comment);
                 }

               if(point < price)
                 {
                  trade.BuyLimit(lotSize, point, symbol, sl, tp, 0, 0, comment);
                 }

               if(point == price)
                 {
                  trade.Buy(lotSize, symbol, point, sl, tp, comment);
                 }
              }
            else
              {
               if(point < price)
                 {
                  trade.SellStop(lotSize, point, symbol, sl, tp, 0, 0, comment);
                 }

               if(point > price)
                 {
                  trade.SellLimit(lotSize, point, symbol, sl, tp, 0, 0, comment);
                 }

               if(point == price)
                 {
                  trade.Sell(lotSize, symbol, point, sl, tp, comment);
                 }
              }

            Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, point, sl, tp, comment));
           }
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              executeWarning(string symbol)
  {
   static datetime lastBarTime = 0;  // Store last processed bar time
   MqlRates rates[]; // Declare an array for MqlRates
// Fetch latest M15 bar
   if(CopyRates(symbol, PERIOD_M15, 0, 1, rates) <= 0)
      return;
   datetime currentBarTime = rates[0].time;  // Get current M15 bar time

// Only execute if a new M15 bar has appeared
   if(currentBarTime == lastBarTime)
      return;
// lastBarTime = currentBarTime;  // Update last processed time

   bool signal = false;
   string message = "WARNING!!! " + symbol + "%0A";
   CUSTOM_TIMEFRAMES timeframes[] = {TF_M15, TF_H1};

   ENUM_DCA_ORDER_TYPE types[] = {DCA_BUY, DCA_SELL};
   bool isTriggered = false;

   for(int j=0; j<ArraySize(types); j++)
     {
      ENUM_DCA_ORDER_TYPE orderType = types[j];

      double entryPrice = getFirstDCAPrice(symbol, orderType);
      datetime entryTime = getFirstDCATime(symbol, orderType);

      if(!entryPrice || entryPrice <= 0)
        {
         continue;
        }

      isTriggered = true;

      for(int i=0; i<ArraySize(timeframes); i++)
        {
         CUSTOM_TIMEFRAMES timeframe = timeframes[i];

         MqlRates rate[];
         CopyRates(symbol,(ENUM_TIMEFRAMES) timeframe, 1, 1, rate);

         double openPrice = rate[0].open;
         double closePrice = rate[0].close;
         datetime time = rate[0].time;

         if(time >= entryTime)
           {
            Print("symbol=", symbol, ", type=", orderType, ", timeframe=", EnumToString(timeframe), ", entryPrice=", entryPrice, ", openPrice=", openPrice, ", closePrice=", closePrice);

            if(orderType == DCA_BUY && openPrice < entryPrice && closePrice < entryPrice)
              {
               message += "BUY at " + EnumToString(timeframe) + "%0A";
               signal = true;
              }

            if(orderType == DCA_SELL && openPrice > entryPrice && closePrice > entryPrice)
              {
               message += "SELL at " + EnumToString(timeframe) + "%0A";
               signal = true;
              }
           }
        }
     }



//   bool isTriggered = false;
//
//   for(int i=0; i<ArraySize(timeframes); i++)
//     {
//      CUSTOM_TIMEFRAMES timeframe = timeframes[i];
//
//      ENUM_DCA_ORDER_TYPE types[] = {DCA_BUY, DCA_SELL};
//      for(int j=0; j<ArraySize(types); j++)
//        {
//         ENUM_DCA_ORDER_TYPE orderType = types[j];
//
//         double entryPrice = getFirstDCAPrice(symbol, orderType);
//         datetime entryTime = getFirstDCATime(symbol, orderType);
//
//         if(!entryPrice || entryPrice <= 0)
//           {
//            continue;
//           }
//
//         isTriggered = true;
//
//         MqlRates rate[];
//         CopyRates(symbol,(ENUM_TIMEFRAMES) timeframe, 1, 1, rate);
//
//         double openPrice = rate[0].open;
//         double closePrice = rate[0].close;
//         datetime time = rate[0].time;
//
//         if(time < entryTime)
//           {
//            continue;
//           }
//
//         Print("symbol=", symbol, ", type=", orderType, ", timeframe=", EnumToString(timeframe), ", entryPrice=", entryPrice, ", openPrice=", openPrice, ", closePrice=", closePrice);
//
//         if(orderType == DCA_BUY && openPrice < entryPrice && closePrice < entryPrice)
//           {
//            message += "BUY at " + EnumToString(timeframe) + "%0A";
//            signal = true;
//           }
//
//         if(orderType == DCA_SELL && openPrice > entryPrice && closePrice > entryPrice)
//           {
//            message += "SELL at " + EnumToString(timeframe) + "%0A";
//            signal = true;
//           }
//
//         // Print(EnumToString(timeframe), ", closePrice=", closePrice , ",openPrice=", openPrice, ",entryPrice=", entryPrice, "time=", time);
//        }
//     }

   if(isTriggered)
     {
      lastBarTime = currentBarTime;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }

  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool checkValidLotSize(string symbol, double lotSize, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   if(lotSize <= 0)
     {
      return false;
     }
// Retrieve account information
   double freeMargin = AccountInfoDouble(ACCOUNT_MARGIN_FREE);

   double price = getCurrentPrice(symbol, currentOrderType);
   ENUM_ORDER_TYPE orderType = currentOrderType == DCA_BUY ? ORDER_TYPE_BUY : ORDER_TYPE_SELL;

   double marginRequired;
   OrderCalcMargin(orderType, symbol, lotSize, price, marginRequired);

// Check if free margin is sufficient
   return freeMargin >= marginRequired;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool checkValidEntry(double price, double sl, double tp, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   if(currentOrderType == DCA_BUY)
     {
      return ((sl > 0 && sl < price) || sl == 0) && ((tp > 0 && price < tp) || tp == 0);
     }

   if(currentOrderType == DCA_SELL)
     {
      return ((sl > 0 && sl > price) || sl == 0) && ((tp > 0 && price > tp) || tp == 0);
     }

   return true;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getEntrySignal(string symbol)
  {
   int entrySignal = 0;
   ENUM_STRATEGY_ENTRY_TYPE entryType = DCA_Strategy_Entry_Type;

   if(TradingEntryTime_Active && !checkTradingEntryHour())
     {
      return entrySignal;
     }

   if(entryType == ENTRY_TYPE_RSI)
     {
      int rsiSignal = getSmoothedRsiSignal(symbol, RSI_NumOfCandles);
     }

   if(entryType == ENTRY_TYPE_FVG)
     {
      entrySignal = getFVGSignal(symbol);
     }

   if(entryType == ENTRY_TYPE_SMA)
     {
      entrySignal = getSMASignal(symbol);
     }

   if(entryType == ENTRY_TYPE_HT_CLOSE)
     {
      entrySignal = getHighTimeCloseSignal(symbol);
     }

   if(entrySignal != 0
      && bot.isMaster && ChatID != "" && BotToken != "")
     {
      datetime currentBarTime = iTime(symbol, (ENUM_TIMEFRAMES) Timeframe, 0);

      if(currentBarTime != lastBarTime)
        {
         string message = entrySignal == SIGNAL_BUY ? COMMAND_MASTER_BUY : COMMAND_MASTER_SELL;
         printf(message);
         SendMessage(message, ChatID, BotToken);

         lastBarTime = currentBarTime;
        }
     }

   if(bot.isSlave)
     {
      entrySignal = 0;
     }

   if(bot.signal != 0 && bot.signalSymbol == symbol)
     {
      int signal = bot.signal;
      bot.signal = 0;
      bot.signalSymbol = "";
      entrySignal = signal;
     }
   else
      if(bot.manual)
        {
         entrySignal = 0;
        }

   if((DCA_Signal_Filter == ONLY_BUY && entrySignal == SIGNAL_SELL) ||
      (DCA_Signal_Filter == ONLY_SELL && entrySignal == SIGNAL_BUY))
     {
      entrySignal = 0;
     }

   return entrySignal;
  }

// Function to remove seconds from a datetime value
datetime removeSeconds(datetime originalTime)
  {
   MqlDateTime timeStruct;

// Convert the datetime to MqlDateTime structure
   TimeToStruct(originalTime, timeStruct);

// Set seconds to zero
   timeStruct.sec = 0;

// Convert the modified MqlDateTime back to datetime
   return StructToTime(timeStruct);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              setOrderTypesByRequestSignal(string symbol, ENUM_DCA_ORDER_TYPE& orderTypes[], int requestSignal)
  {

   if(requestSignal == SIGNAL_HEDG || requestSignal == SIGNAL_DCA || requestSignal == SIGNAL_DCA_3)
     {
      int buyTotal = getTotalDCAPositions(symbol, DCA_BUY);
      int sellTotal = getTotalDCAPositions(symbol, DCA_SELL);

      int entrySignal = getEntrySignal(symbol);
      int count = 0;

      if(requestSignal == SIGNAL_DCA || requestSignal == SIGNAL_DCA_3)
        {
         if((entrySignal == SIGNAL_BUY && buyTotal == 0 && sellTotal == 0) || buyTotal > 0)
           {
            ArrayResize(orderTypes, count + 1);
            orderTypes[count++] = DCA_BUY;
           }
         else
            if((entrySignal == SIGNAL_SELL && sellTotal == 0 && buyTotal == 0) || sellTotal > 0)
              {
               ArrayResize(orderTypes, count + 1);
               orderTypes[count++] = DCA_SELL;
              }
        }

      if(requestSignal == SIGNAL_HEDG)
        {
         if(buyTotal == 0 && sellTotal == 0)
           {
            if(entrySignal == SIGNAL_BUY)
              {
               ArrayResize(orderTypes, count + 1);
               orderTypes[count++] = DCA_BUY;
              }

            if(entrySignal == SIGNAL_SELL)
              {
               ArrayResize(orderTypes, count + 1);
               orderTypes[count++] = DCA_SELL;
              }
           }
         else
            if(buyTotal > 0 && sellTotal > 0)
              {
               ArrayResize(orderTypes, count + 1);
               orderTypes[count++] = DCA_BUY;
               ArrayResize(orderTypes, count + 1);
               orderTypes[count++] = DCA_SELL;
              }
            else
               if(buyTotal > 0)
                 {
                  ArrayResize(orderTypes, count + 1);
                  orderTypes[count++] = DCA_BUY;

                  if(entrySignal == SIGNAL_SELL)
                    {
                     ArrayResize(orderTypes, count + 1);
                     orderTypes[count++] = DCA_SELL;
                    }
                 }
               else
                  if(sellTotal > 0)
                    {
                     ArrayResize(orderTypes, count + 1);
                     orderTypes[count++] = DCA_SELL;

                     if(entrySignal == SIGNAL_BUY)
                       {
                        ArrayResize(orderTypes, count + 1);
                        orderTypes[count++] = DCA_BUY;
                       }
                    }
        }
     }

   if(requestSignal == SIGNAL_BUY || requestSignal == SIGNAL_SELL || requestSignal == SIGNAL_NONE)
     {
      int count = 0;
      int buyTotal = getTotalDCAPositions(symbol, DCA_BUY);
      int sellTotal = getTotalDCAPositions(symbol, DCA_SELL);

      if(requestSignal == SIGNAL_BUY || buyTotal > 0)
        {
         ArrayResize(orderTypes, count + 1);
         orderTypes[count++] = DCA_BUY;
        }

      if(requestSignal == SIGNAL_SELL || sellTotal > 0)
        {
         ArrayResize(orderTypes, count + 1);
         orderTypes[count++] = DCA_SELL;
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getTotalDCAPositions(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   int count = 0;
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         count++;
        }
     }

   return count;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getTotalHedgingPositions(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType = NULL)
  {
   int count = 0;
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && isHedgPosition(position.Comment(), currentOrderType)
        )
        {
         count++;
        }
     }

   return count;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getTotalDCAOrders(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType = NULL)
  {
   int count = 0;
   for(int i = OrdersTotal() - 1; i >= 0; i--)
     {
      if(OrderSelect(OrderGetTicket(i)))
        {
         string symbol = OrderGetString(ORDER_SYMBOL);
         long magicNumber = OrderGetInteger(ORDER_MAGIC);
         ENUM_ORDER_TYPE orderType  = ENUM_ORDER_TYPE(OrderGetInteger(ORDER_TYPE));

         if(symbol == symbol && magicNumber == DCA_Strategy_MagicNumber)
           {
            if(currentOrderType != NULL)
              {
               if(currentOrderType == DCA_BUY && (orderType == ORDER_TYPE_BUY_LIMIT || orderType == ORDER_TYPE_BUY_STOP))
                 {
                  count++;
                 }

               if(currentOrderType == DCA_SELL && (orderType == ORDER_TYPE_SELL_LIMIT || orderType == ORDER_TYPE_SELL_STOP))
                 {
                  count++;
                 }
              }
            else
              {
               count++;
              }
           }
        }
     }

   return count;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void closeAllPendingOrders(string symbol = "", ENUM_DCA_ORDER_TYPE currentOrderType = NULL)
  {
   for(int i = OrdersTotal() - 1; i >= 0; i--)
     {
      if(OrderSelect(OrderGetTicket(i)))
        {
         string sym = OrderGetString(ORDER_SYMBOL);
         long magicNumber = OrderGetInteger(ORDER_MAGIC);
         ENUM_ORDER_TYPE orderType  = ENUM_ORDER_TYPE(OrderGetInteger(ORDER_TYPE));

         if((sym == symbol || symbol == "") && magicNumber == DCA_Strategy_MagicNumber)
           {
            if(currentOrderType == DCA_BUY && (orderType == ORDER_TYPE_BUY_LIMIT || orderType == ORDER_TYPE_BUY_STOP))
              {
               trade.OrderDelete(OrderGetTicket(i));
              }

            if(currentOrderType == DCA_SELL && (orderType == ORDER_TYPE_SELL_LIMIT || orderType == ORDER_TYPE_SELL_STOP))
              {
               trade.OrderDelete(OrderGetTicket(i));
              }

            if(currentOrderType == NULL)
              {
               trade.OrderDelete(OrderGetTicket(i));
              }
           }
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getRsiSignal(string symbol, int numOfCandles)
  {
   double rsi[];
   int rsiHandle = iRSI(symbol, (ENUM_TIMEFRAMES) Timeframe, RSI_Length, PRICE_CLOSE);
   int oversold = getRsiOversold();
   int overbought = getRsiOverbought();
   int signal = 0;

   CopyBuffer(rsiHandle, 0, 1, numOfCandles, rsi);

   int buyCount = 0;
   int sellCount = 0;

   for(int i=0; i< numOfCandles - 1; i++)
     {
      if(rsi[i] < oversold)
        {
         buyCount++;
        }

      if(rsi[i] > overbought)
        {
         sellCount++;
        }
     }

   if(buyCount == numOfCandles - 1 && rsi[numOfCandles - 1] >= oversold)
     {
      signal = SIGNAL_BUY;
     }

   if(sellCount == numOfCandles - 1 && rsi[numOfCandles - 1] <= overbought)
     {
      signal = SIGNAL_SELL;
     }

   ArrayFree(rsi);
   return signal;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getSmoothedRsiSignal(string symbol, int numOfCandles)
  {
   int signal = 0;

   double rsi[];
   int rsiHandle = iRSI(symbol, (ENUM_TIMEFRAMES)Timeframe, RSI_Length, PRICE_CLOSE);
   int oversold = getRsiOversold();
   int overbought = getRsiOverbought();

   int MA_Length = numOfCandles;

   CopyBuffer(rsiHandle, 0, 1, RSI_Length, rsi);

   int maHandle = iMA(symbol, (ENUM_TIMEFRAMES) Timeframe, MA_Length, 0, MODE_SMA, rsiHandle);
   double ma[];
   CopyBuffer(maHandle, 0, 1, MA_Length, ma);


   if(ma[MA_Length - 2] < oversold && ma[MA_Length - 1] >= oversold)
     {
      signal = SIGNAL_BUY;
     }

   if(ma[MA_Length - 2] > overbought && ma[MA_Length - 1] <= overbought)
     {
      signal = SIGNAL_SELL;
     }

   return signal;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
struct FvgResult
  {
   double            price;
   int               signal;
  };

FvgResult fvgResult;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
FvgResult getFVGResult(string symbol)
  {
   FvgResult result;
   result.price = 0;
   result.signal = 0;

   int signal = 0;
   int numOfCandles = FVG_NumOfCandles;

   MqlRates rate[];
   CopyRates(symbol,(ENUM_TIMEFRAMES) Timeframe_High, 1, numOfCandles, rate);

// Check all candles have the same color
   int greenCount = 0;
   int redCount = 0;

   for(int i= numOfCandles - 1; i>=0; i--)
     {
      if(rate[i].close >= rate[i].open)
        {
         greenCount++;
        }

      if(rate[i].close <= rate[i].open)
        {
         redCount++;
        }
     }

   bool buyCountCondition = (redCount == numOfCandles);
   bool sellCountCondition = (greenCount == numOfCandles);


// Check FVG
   bool buyFVGCondition = true;
   bool sellFVGCondition = true;

   for(int i= numOfCandles - 1; i>=2; i--)
     {
      int j = i - 2;
      if(rate[i].high > rate[j].low)
        {
         buyFVGCondition = false;
        }

      if(rate[i].low < rate[j].high)
        {
         sellFVGCondition = false;
        }
     }

   if(buyCountCondition && buyFVGCondition)
     {
      signal = SIGNAL_BUY;

      result.signal = signal;
      result.price = rate[numOfCandles - 1].high;
     }

   if(sellCountCondition && sellFVGCondition)
     {
      signal = SIGNAL_SELL;

      result.signal = signal;
      result.price = rate[numOfCandles - 1].low;
     }

   return result;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getFVGSignal(string symbol)
  {
   if(fvgResult.signal != 0)
     {
      int signal = 0;

      if(fvgResult.signal == SIGNAL_BUY && getCurrentPrice(symbol, DCA_BUY) >= fvgResult.price)
        {
         signal = fvgResult.signal;
        }

      if(fvgResult.signal == SIGNAL_SELL && getCurrentPrice(symbol, DCA_SELL) <= fvgResult.price)
        {
         signal = fvgResult.signal;
        }

      if(signal != 0)
        {
         fvgResult.signal = 0;
         fvgResult.price = 0;

         return signal;
        }
     }

   FvgResult newFvgResult = getFVGResult(symbol);
   if(newFvgResult.signal != 0)
     {
      fvgResult.signal = newFvgResult.signal;
      fvgResult.price = newFvgResult.price;
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void executeReset()
  {
   static int lastDayChecked = 0; // Store the last checked day

// Declare a structure to hold the date and time information
   MqlDateTime timeStruct;

// Get the current date and time
   TimeToStruct(TimeCurrent(), timeStruct);

// Get the current day of the month
   int currentDay = timeStruct.day;

// Check if a new day has started
   if(currentDay != lastDayChecked)
     {
      reset();
      lastDayChecked = currentDay; // Update the last checked day
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void reset()
  {
// ArrayResize(configs, 0);
// resetSymbols();
   if(TradingStopDaily_Active)
     {
      // removeActiveConfigs();
     }
   Print("=== config list ===");
   Print(getConfigMessage());
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double ArraySum(const double &arr[])
  {
   double sum = 0;
   for(int i = 0; i < ArraySize(arr); i++)
     {
      sum += arr[i];
     }
   return sum;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void getPreviousDayRates(string symbol, int day_shift, MqlRates &rates[])
  {
   datetime currentDayStartTime = iTime(symbol, PERIOD_D1, 0);
   datetime startTime, endTime;
   int copied = 0;

   for(int shift = day_shift; shift <= 7; shift++)   // Limit search to within the last 7 days
     {
      startTime = currentDayStartTime - shift * 86400;        // Start of the target day
      endTime = currentDayStartTime - (shift - 1) * 86400 - 1;  // End of the target day (23:59:59)
      copied = CopyRates(symbol, PERIOD_M1, startTime, endTime, rates);

      if(copied > 0)
        {
         break;  // Successfully found a trading day with data
        }
     }

   if(copied <= 0)
     {
      Print("Error: Unable to fetch M1 data for the previous trading day within the last 7 days.");
     }
   else
     {
      Print(currentDayStartTime, "|", startTime, "|", endTime);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getSMASignal(string symbol)
  {
   ENUM_TIMEFRAMES timeframe = (ENUM_TIMEFRAMES) Timeframe;

   int signal = 0;

   double smaFastValues[2];
   CopyBuffer(iMA(symbol, timeframe, SMA_FastPeriod, 0, MODE_SMA, PRICE_CLOSE), 0, 1, 2, smaFastValues);

   double smaFastPrevious = smaFastValues[0];
   double smaFast = smaFastValues[1];

   double smaSlowValues[2];
   CopyBuffer(iMA(symbol, timeframe, SMA_SlowPeriod, 0, MODE_SMA, PRICE_CLOSE), 0, 1, 2, smaSlowValues);

   double smaSlowPrevious = smaSlowValues[0];
   double smaSlow = smaSlowValues[1];

//double dist = MathAbs(smaFast - smaSlow);
//double threshold = 20 * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10;

//Print("previousFast=", smaFastPrevious  , "fast=", smaFast, "slow=", smaSlow , "dist=", dist, "thres=", threshold);

   if(smaFast <= 0 || smaSlow <= 0 || smaFastPrevious <= 0 || smaSlowPrevious <= 0)
     {
      return 0;
     }

   if(smaFast > smaSlow && smaFastPrevious < smaSlowPrevious && smaFast > smaFastPrevious && smaSlow > smaSlowPrevious)
     {
      signal = SIGNAL_BUY;
     }

   if(smaFast < smaSlow && smaFastPrevious > smaSlowPrevious && smaFast < smaFastPrevious && smaSlow < smaSlowPrevious)
     {
      signal = SIGNAL_SELL;
     }

   if(SMA_TrendPeriod > 0)
     {
      int trendSignal = 0;

      double smaTrendValues[1];
      CopyBuffer(iMA(symbol, timeframe, SMA_TrendPeriod, 0, MODE_SMA, PRICE_CLOSE), 0, 0, 1, smaTrendValues);
      double smaTrend = smaTrendValues[0];

      if(smaFast > smaTrend && smaSlow > smaTrend)
        {
         trendSignal = SIGNAL_BUY;
        }

      if(smaFast < smaTrend && smaSlow < smaTrend)
        {
         trendSignal = SIGNAL_SELL;
        }

      return signal == trendSignal ? signal : 0;
     }

   return signal;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getHighTimeCloseSignal(string symbol)
  {
   int signal = 0;

   MqlRates rate[];
   CopyRates(symbol,(ENUM_TIMEFRAMES)HTC_Timeframe,0,2,rate);

   if(ArraySize(rate) == 2)
     {
      double currentPrice = rate[1].close;
      double previousPrice = rate[0].close;

      if(currentPrice < previousPrice)
        {
         signal = SIGNAL_SELL;
        }

      if(currentPrice > previousPrice)
        {
         signal = SIGNAL_BUY;
        }
     }

   return signal;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getSMATakeProfitSignal(string symbol)
  {
   ENUM_TIMEFRAMES timeframe = (ENUM_TIMEFRAMES) Timeframe;

   int signal = 0;

   double smaFastValues[2];
   CopyBuffer(iMA(symbol, timeframe, SMA_FastPeriod, 0, MODE_SMA, PRICE_CLOSE), 0, 0, 2, smaFastValues);

   double smaFastPrevious = smaFastValues[0];
   double smaFast = smaFastValues[1];

   double smaSlowValues[2];
   CopyBuffer(iMA(symbol, timeframe, SMA_SlowPeriod, 0, MODE_SMA, PRICE_CLOSE), 0, 0, 2, smaSlowValues);

   double smaSlowPrevious = smaSlowValues[0];
   double smaSlow = smaSlowValues[1];

   if(smaFast <= 0 || smaSlow <= 0 || smaFastPrevious <= 0 || smaSlowPrevious <= 0)
     {
      return 0;
     }

   if(smaFast > smaSlow && smaFastPrevious < smaSlowPrevious && smaFast > smaFastPrevious)
     {
      signal = SIGNAL_SELL;
     }

   if(smaFast < smaSlow && smaFastPrevious > smaSlowPrevious && smaFast < smaFastPrevious)
     {
      signal = SIGNAL_BUY;
     }

   return signal;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getRsi(string symbol, ENUM_TIMEFRAMES timeframe)
  {
   double rsi[];
   int rsiHandle = iRSI(symbol, timeframe, 14, PRICE_CLOSE);
   CopyBuffer(rsiHandle, 0, 0, 1, rsi);

   double rsiVal = rsi[0];

   ArrayFree(rsi);
   return rsiVal;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getNextDCASignal(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   int signal = 0;
   double lastPrice = getLastDCAPrice(symbol, currentOrderType);
   double initPrice = getFirstDCAPrice(symbol, currentOrderType);
   double stoploss  = getFirstDCAStoploss(symbol, currentOrderType);
   int totalPositions = getTotalDCAPositions(symbol, currentOrderType);

   KhanhdeuxConfig* config = findConfig(symbol, initPrice, currentOrderType);
   if(config == NULL || totalPositions - 1 >= ArraySize(config.points))
     {
      return 0;
     }

   double pointPrice = config.points[totalPositions - 1];
//   double currentPrice = getCurrentPrice(symbol, currentOrderType);
//
//   if(currentOrderType == DCA_BUY)
//     {
//      if(currentPrice <= pointPrice)
//        {
//         signal = SIGNAL_BUY;
//        }
//     }
//
//   if(currentOrderType == DCA_SELL)
//     {
//      if(currentPrice >= pointPrice)
//        {
//         signal = SIGNAL_SELL;
//        }
//     }

   MqlRates rate[];
   CopyRates(symbol,(ENUM_TIMEFRAMES)Timeframe,0,2,rate);

   double currentPrice = rate[1].close;
   double previousPrice = rate[0].close;

   if(
      (previousPrice < pointPrice && pointPrice < currentPrice)
      || (previousPrice > pointPrice && pointPrice > currentPrice))
     {
      Print("previousPrice=", previousPrice, ",pointPrice=",pointPrice,",currentPrice=",currentPrice);
      signal = currentOrderType == DCA_BUY ? SIGNAL_BUY : SIGNAL_SELL;
     }


   return signal;
  }

datetime lastCandleTime = 0;  // Variable to track the last closed candle time

// Function to check if a new candle has closed
bool IsNewCandleClosed(string symbol)
  {
   datetime currentCandleTime = iTime(symbol, (ENUM_TIMEFRAMES) Timeframe, 1); // Get the close time of the last completed candle

   if(currentCandleTime != lastCandleTime)  // Check if the candle time is different from the last known time
     {
      lastCandleTime = currentCandleTime; // Update the last known candle time
      return true; // A new candle has closed
     }

   return false; // No new candle closed
  }

//+------------------------------------------------------------------+
//|              SELL: ((lotsize1 * price1 + lotsize2 * price2 + loss * SymbolInfoDouble(_Symbol, SYMBOL_TRADE_TICK_SIZE) / SymbolInfoDouble(_Symbol, SYMBOL_TRADE_TICK_VALUE) ) / (lotsize1 + lotsize2)                                                    |
//|              BUY:  ((lotsize1 * price1 + lotsize2 * price2 - loss * SymbolInfoDouble(_Symbol, SYMBOL_TRADE_TICK_SIZE) / SymbolInfoDouble(_Symbol, SYMBOL_TRADE_TICK_VALUE) ) / (lotsize1 + lotsize2)                                                    |
//+------------------------------------------------------------------+
double            calculateDCAStoploss(string symbol, double lotSize, double price, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   double priceVolumeSum = 0;
   double totalVolume = 0.0;
   double loss = getStoploss();
   double sl = 0;

   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
        )
        {
         priceVolumeSum += position.PriceOpen() * position.Volume();
         totalVolume += position.Volume();
        }
     }

   priceVolumeSum += lotSize * price;
   totalVolume += lotSize;

   double lossInTick = loss * SymbolInfoDouble(symbol, SYMBOL_TRADE_TICK_SIZE) / SymbolInfoDouble(symbol, SYMBOL_TRADE_TICK_VALUE);

   if(currentOrderType == DCA_BUY)
     {
      sl = NormalizeDouble((priceVolumeSum - lossInTick) / totalVolume,SymbolInfoInteger(symbol, SYMBOL_DIGITS));
     }

   if(currentOrderType == DCA_SELL)
     {
      sl = NormalizeDouble((priceVolumeSum + lossInTick) / totalVolume,SymbolInfoInteger(symbol, SYMBOL_DIGITS));
     }

   return sl;
  }

//+------------------------------------------------------------------+
//|
// BUY
// ((lotsize1 + lotsize2 + lotsize3) * (price - stoploss) + loss * SymbolInfoDouble(_Symbol, SYMBOL_TRADE_TICK_SIZE) / SymbolInfoDouble(_Symbol, SYMBOL_TRADE_TICK_VALUE)) / (lotsize2 + 2 * lotsize3) = dist
// SELL
// ((stoploss - price) * (lotsize1 + lotsize2 + lotsize3) - loss * SymbolInfoDouble(_Symbol, SYMBOL_TRADE_TICK_SIZE) / SymbolInfoDouble(_Symbol, SYMBOL_TRADE_TICK_VALUE)) / (lotsize2 + 2 * lotsize3) = dist                                                        |
//+------------------------------------------------------------------+
//double            calculateDCADistance(double initPrice, KhanhdeuxConfig& config)
//  {
//   double totalDistance  = MathAbs(initPrice - config.stoploss) / (SymbolInfoDouble(config.symbol, SYMBOL_POINT) * 10);
//   return NormalizeDouble(totalDistance / (ArraySize(config.points) + 1), SymbolInfoInteger(config.symbol, SYMBOL_DIGITS));
//  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            calculateDCABreakEvenStoploss(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   double priceVolumeSum = 0;
   double totalVolume = 0.0;
   double loss = 0;
   double sl = 0;

   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
        )
        {
         priceVolumeSum += position.PriceOpen() * position.Volume();
         totalVolume += position.Volume();
        }
     }

   double lossInTick = loss * SymbolInfoDouble(symbol, SYMBOL_TRADE_TICK_SIZE) / SymbolInfoDouble(symbol, SYMBOL_TRADE_TICK_VALUE);

   if(currentOrderType == DCA_BUY)
     {
      sl = NormalizeDouble((priceVolumeSum - lossInTick) / totalVolume,SymbolInfoInteger(symbol, SYMBOL_DIGITS));
     }

   if(currentOrderType == DCA_SELL)
     {
      sl = NormalizeDouble((priceVolumeSum + lossInTick) / totalVolume,SymbolInfoInteger(symbol, SYMBOL_DIGITS));
     }

   return sl;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//double            calculateDCATakeProfit(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
//  {
//   double initPrice = getFirstDCAPrice(symbol, currentOrderType);
//   KhanhdeuxConfig* config = findConfig(symbol, initPrice, currentOrderType);
//   double be = calculateDCABreakEvenStoploss(symbol, currentOrderType);
//   double tp;
//
//   if(config == NULL)
//     {
//      return be;
//     }
//
//   double pips = calculateDCADistance(initPrice, config) * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10;
//
//   if(currentOrderType == DCA_BUY)
//     {
//      tp = NormalizeDouble(be + pips,SymbolInfoInteger(symbol, SYMBOL_DIGITS));
//     }
//
//   if(currentOrderType == DCA_SELL)
//     {
//      tp = NormalizeDouble(be - pips,SymbolInfoInteger(symbol, SYMBOL_DIGITS));
//     }
//
//   return tp;
//  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            calculateDCALotSize(int i, double lotSize, ENUM_VOLUME_TYPE volumeType)
  {
   if(volumeType == VOL_FIB)
     {
      if(i <= 1)
        {
         return lotSize;
        }

      return calculateDCALotSize(i - 1, lotSize, volumeType) + calculateDCALotSize(i - 2, lotSize, volumeType);
     }

   if(volumeType == VOL_UNI)
     {
      return lotSize;
     }

   if(i == 1)
      return lotSize;
   return calculateDCALotSize(i - 1, lotSize, volumeType) * 2;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            calculateDCALotSizeNumber(int i, ENUM_VOLUME_TYPE volumeType)
  {
   if(i <= 1)
      return 1;

   if(volumeType == VOL_FIB)
     {
      return calculateDCALotSizeNumber(i - 1, volumeType) + calculateDCALotSizeNumber(i - 2, volumeType);
     }

   if(volumeType == VOL_UNI)
     {
      return 1;
     }


   return calculateDCALotSizeNumber(i - 1, volumeType) * 2;
  }

// 1.XAUUSD,INACTIVE,BUY,range=[0.0,9999.0],sl=2720.0|tp=2746.0,points=[2733.20,2728.00]
//+------------------------------------------------------------------+
//|              BUY:  lotsize = lossInTick / ((price1 + price2 + ... + price-n) - (stoploss1 + stoploss2 + ... + stoploss-n))
//|              SELL: lotsize = lossInTick / ((stoploss1 + stoploss2 + ... + stoploss-n) - (price1 + price2 + ... + price-n))
//+------------------------------------------------------------------+
double            calculateDCALotSize(double initPrice, KhanhdeuxConfig& config)
  {
   string symbol = config.symbol;
   double loss = getStoploss();
   double lossInTick = loss * SymbolInfoDouble(symbol, SYMBOL_TRADE_TICK_SIZE) / SymbolInfoDouble(symbol, SYMBOL_TRADE_TICK_VALUE);

   double priceSum = initPrice;
   double stoplossSum = config.stoploss[0];
   int total = 1;

   for(int i = 0; i < ArraySize(config.points); i++)
     {
      priceSum += config.points[i];
      stoplossSum += config.stoploss[MathMin(i + 1,ArraySize(config.stoploss) - 1)];
      total++;
     }

   return NormalizeDouble(lossInTick / MathAbs(priceSum - stoplossSum), 2);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getCurrentPrice(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   double currentPrice = NormalizeDouble(SymbolInfoDouble(symbol, SYMBOL_LAST),SymbolInfoInteger(symbol, SYMBOL_DIGITS));

   if(currentOrderType == DCA_BUY)
     {
      currentPrice = NormalizeDouble(SymbolInfoDouble(symbol, SYMBOL_ASK),SymbolInfoInteger(symbol, SYMBOL_DIGITS));
     }
   else
      if(currentOrderType == DCA_SELL)
        {
         currentPrice = NormalizeDouble(SymbolInfoDouble(symbol, SYMBOL_BID),SymbolInfoInteger(symbol, SYMBOL_DIGITS));
        }

   return currentPrice;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
KhanhdeuxConfig* findConfig(string symbol, double initPrice, ENUM_DCA_ORDER_TYPE orderType)
  {
   for(int i = 0; i < ArraySize(configs); i++)
     {
      KhanhdeuxConfig config = configs[i];

      if(config.symbol == symbol
         && config.orderType == orderType
         && config.lowerPrice <= initPrice
         && initPrice <= config.upperPrice
         /// && active == config.active
        )
        {

         //for(int i=0;i<ArraySize(config.points);i++)
         //  {
         //   if(orderType == DCA_BUY && initPrice < config.points[i])
         //     {
         //      return NULL;
         //     }
         //   if(orderType == DCA_SELL && initPrice > config.points[i])
         //     {
         //      return NULL;
         //     }
         //  }

         return config.Clone();
        }
     }

   return NULL;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void activateConfig(int index)
  {
   for(int i = 0; i < ArraySize(configs); i++)
     {
      KhanhdeuxConfig config = configs[i];

      if(configs[i].index == index)
        {
         configs[i].active = true;
         break;
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void deactivateConfig(int index)
  {
   for(int i = 0; i < ArraySize(configs); i++)
     {
      KhanhdeuxConfig config = configs[i];

      if(configs[i].index == index)
        {
         configs[i].active = false;
         break;
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime getFirstDCATime(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = 0; i <= PositionsTotal() - 1; i++)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return (datetime) PositionGetInteger(POSITION_TIME);
        }
     }

   return NULL;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getFirstDCAPrice(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = 0; i <= PositionsTotal() - 1; i++)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return PositionGetDouble(POSITION_PRICE_OPEN);
        }
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getFirstDCAStoploss(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = 0; i <= PositionsTotal() - 1; i++)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return PositionGetDouble(POSITION_SL);
        }
     }

   return 0;
  }



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getPositionTypeByOrder(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   if(currentOrderType == DCA_BUY)
     {
      return POSITION_TYPE_BUY;
     }
   if(currentOrderType == DCA_SELL)
     {
      return POSITION_TYPE_SELL;
     }

   return NULL;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getLastDCAPrice(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return PositionGetDouble(POSITION_PRICE_OPEN);
        }
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ENUM_DCA_ORDER_TYPE getLastDCAOrderType(string symbol)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && !isHedgPosition(position.Comment())
        )
        {
         return PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(DCA_BUY) ? DCA_BUY : DCA_SELL;
        }
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getLastNthDCAPrice(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType, int number)
  {
   int count = 1;

   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         if(count == number)
           {
            return PositionGetDouble(POSITION_PRICE_OPEN);
           }
         count++;
        }
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void            closeLastNthPosition(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType, int number)
  {
   int count = 1;

   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         if(count == number)
           {
            trade.PositionClose(position.Ticket());
            Print(StringFormat("Close Position with lotsize=%g", PositionGetDouble(POSITION_VOLUME)));
            return;
           }
         count++;
        }
     }

   return;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getLastDCALotSize(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return PositionGetDouble(POSITION_VOLUME);
        }
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime getLastDCATime(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return (datetime) PositionGetInteger(POSITION_TIME);
        }
     }

   return NULL;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getLastDCATakeProfit(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return PositionGetDouble(POSITION_TP);
        }
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getLastDCAStoploss(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return PositionGetDouble(POSITION_SL);
        }
     }

   return 0;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              updateAllDCAPositionTakeProfit(string symbol, double tpPrice, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         double sl = position.StopLoss();
         double tp = tpPrice;

         if(tpPrice == position.TakeProfit())
           {
            continue;
           }

         trade.PositionModify(position.Ticket(),sl,tp);
         Print(StringFormat("Position with lotsize=%g updated TakeProfit= %g", PositionGetDouble(POSITION_VOLUME), tpPrice));
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              updateAllDCAPositionStopLoss(string symbol, double slPrice, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         double sl = slPrice;
         if(sl == position.StopLoss())
           {
            continue;
           }
         double tp = position.TakeProfit();

         trade.PositionModify(position.Ticket(),sl,tp);
         Print(StringFormat("Position with lotsize=%g updated Stoploss= %g", PositionGetDouble(POSITION_VOLUME), sl));
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              isAllDCAPositionStopLossUpdated(string symbol, double slPrice, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return slPrice == position.StopLoss() ? true : false;
        }
     }

   return false;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              isStoplossValid(string symbol, double slPrice, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   double currentPrice = getCurrentPrice(symbol, currentOrderType);
   return (currentOrderType == DCA_BUY && slPrice < currentPrice) || (currentOrderType == DCA_SELL && slPrice > currentPrice);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              updateAllDCAPositionTakeProfitAndStopLoss(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType, double tpPrice, double slPrice)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         double sl = (slPrice > 0 && slPrice != position.StopLoss()) ? slPrice: position.StopLoss();
         double tp = tpPrice;

         trade.PositionModify(position.Ticket(),sl,tp);
         Print(StringFormat("Position with lotsize=%g updated SL= %g TakeProfit= %g", PositionGetDouble(POSITION_VOLUME), sl, tp));
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
void              closeAllDCAPositions(string symbol = "", ENUM_DCA_ORDER_TYPE currentOrderType = NULL)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && (PositionGetSymbol(i) == symbol || symbol == "")
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         trade.PositionClose(position.Ticket());
         Print(StringFormat("Close Position with lotsize=%g", PositionGetDouble(POSITION_VOLUME)));
        }
     }

   closeAllPendingOrders(symbol, currentOrderType);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              closeAllHedgPositions(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType = NULL)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && isHedgPosition(position.Comment(), currentOrderType)
        )
        {
         trade.PositionClose(position.Ticket());
         Print(StringFormat("Close Hedg Position with lotsize=%g", PositionGetDouble(POSITION_VOLUME)));
        }
     }
  }

string HEDG_STRING = "HEDG";
string BUY_STRING = "BUY";
string SELL_STRING = "SELL";

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              isHedgPosition(string comment, ENUM_DCA_ORDER_TYPE currentOrderType = -1)
  {
   string findStr = HEDG_STRING;
   if(currentOrderType > -1)
     {
      findStr += "-" + (currentOrderType == DCA_BUY ? BUY_STRING : SELL_STRING);
     }
   return StringFind(comment, findStr) > -1 ? true : false;
  }

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
bool              checkTradingStopHour()
  {
   MqlDateTime time1;
   TimeToStruct(TimeCurrent(),time1);
   int hour=time1.hour;

   if(TradingStop_HourStart == TradingStop_HourStop)
     {
      if(hour == TradingStop_HourStart)
        {
         return true;
        }
     }

   if(TradingStop_HourStart < TradingStop_HourStop)
     {
      if(TradingStop_HourStart <= hour && hour < TradingStop_HourStop)
        {
         return true;
        }
     }

   if(TradingStop_HourStart > TradingStop_HourStop)
     {
      if(TradingStop_HourStart <= hour || hour < TradingStop_HourStop)
        {
         return true;
        }
     }

   return false;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkTradingTime(int start, int stop, int currentHour = NULL)
  {
   MqlDateTime time1;
   TimeToStruct(TimeCurrent(),time1);
   int hour = currentHour ? currentHour : time1.hour;

   if(start == stop)
     {
      if(hour == start)
        {
         return true;
        }
     }

   if(start < stop)
     {
      if(start <= hour && hour < stop)
        {
         return true;
        }
     }

   if(start > stop)
     {
      if(start <= hour || hour < stop)
        {
         return true;
        }
     }

   return false;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkTradingEntryHour()
  {
   MqlDateTime time1;
   TimeToStruct(TimeCurrent(),time1);
   int hour=time1.hour;

   if(TradingEntryTime_HourStart == TradingEntryTime_HourStop)
     {
      if(hour == TradingEntryTime_HourStart)
        {
         return true;
        }
     }

   if(TradingEntryTime_HourStart < TradingEntryTime_HourStop)
     {
      if(TradingEntryTime_HourStart <= hour && hour < TradingEntryTime_HourStop)
        {
         return true;
        }
     }

   if(TradingEntryTime_HourStart > TradingEntryTime_HourStop)
     {
      if(TradingEntryTime_HourStart <= hour || hour < TradingEntryTime_HourStop)
        {
         return true;
        }
     }

   return false;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkTradingSession()
  {
   datetime currentTime = TimeCurrent();
   int offset = 0 * 60 * 60;
// return true;

   ENUM_DAY_OF_WEEK dayOfWeek = getDayOfWeek();

   switch(dayOfWeek)
     {
      case MONDAY:
         return true;
         break;
      case TUESDAY:
         break;
      case WEDNESDAY:
         break;
      case THURSDAY:
         break;
      case FRIDAY:
         return true;
         break;
     }

   return true;
   return currentTime + offset >= getTradingSession(NEWYORK, true) && currentTime - offset <= getTradingSession(LONDON, false);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkDailyStoploss()
  {
   return getDayProfit() + getTotalProfit() < (getDailyStoploss() * -1);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getDailyStoploss()
  {
   double accountBalance = MathMin(AccountInfoDouble(ACCOUNT_BALANCE), ACCOUNT_BALANCE_INIT);
   return accountBalance * TradingStopDaily / 100;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getStoploss()
  {
   double accountBalance = MathMin(AccountInfoDouble(ACCOUNT_BALANCE), ACCOUNT_BALANCE_INIT);
   return accountBalance * TradingStopLoss / 100;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkDailyProfit()
  {
   return getDayProfit() + getTotalProfit() > getDailyProfit();
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkStopProfit()
  {
   double accountBalance = MathMin(AccountInfoDouble(ACCOUNT_BALANCE), ACCOUNT_BALANCE_INIT);
   return getTotalProfit() > accountBalance * TradingStopProfit / 100;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//bool              checkLimitProfit()
//  {
//   int buyTotal = getTotalDCAPositions(DCA_BUY);
//   int sellTotal = getTotalDCAPositions(DCA_SELL);
//   int maxTotal = MathMax(buyTotal, sellTotal);
//
//   return (getTotalProfit() > 0 && maxTotal >= TradingLimitProfit_Number);
//  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getDailyProfit()
  {
   double accountBalance = AccountInfoDouble(ACCOUNT_BALANCE);
   return accountBalance * TradingProfitDaily / 100;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkDayOfWeek()
  {
   if(Trading_DayOfWeek_Active == false)
     {
      return true;
     }

   MqlDateTime STime;
   datetime time_current=TimeCurrent(STime);
   return (ENUM_DAY_OF_WEEK) STime.day_of_week == Trading_DayOfWeek;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkSession()
  {
   MqlDateTime time1;
   TimeToStruct(TimeCurrent() + 5 * 60, time1);

   if(TradingSession_Active == true)
     {
      int hourStart;
      int hourStop;

      switch(TradingSession_Type)
        {
         case SESSION_ASIA:
            hourStart = ASIA_HourStart;
            hourStop  = ASIA_HourStop;
            break;
         case SESSION_UK:
            hourStart = UK_HourStart;
            hourStop  = UK_HourStop;
            break;
         case SESSION_US:
            hourStart = US_HourStart;
            hourStop  = US_HourStop;
            break;
        }

      if(checkTradingTime(hourStart, hourStop))
        {
         if(!checkTradingTime(hourStart, hourStop, time1.hour))
           {
            return false;
           }
         return true;
        }

      return false;
     }

   if(checkTradingTime(ASIA_HourStart, ASIA_HourStop))
     {
      if(!checkTradingTime(ASIA_HourStart, ASIA_HourStop, time1.hour))
        {
         return false;
        }
      return true;
     }

   if(checkTradingTime(UK_HourStart, UK_HourStop))
     {
      if(!checkTradingTime(UK_HourStart, UK_HourStop, time1.hour))
        {
         return false;
        }

      return true;
     }

   if(checkTradingTime(US_HourStart, US_HourStop))
     {
      if(!checkTradingTime(US_HourStart, US_HourStop, time1.hour))
        {
         return false;
        }

      return true;
     }

   return false;
  }

//+------------------------------------------------------------------+
//|
// SUNDAY
// MONDAY
// TUESDAY
// WEDNESDAY
// THURSDAY
// FRIDAY
// SATURDAY
//+------------------------------------------------------------------+
ENUM_DAY_OF_WEEK            getDayOfWeek()
  {
   MqlDateTime STime;
   datetime time_current=TimeCurrent(STime);
   return (ENUM_DAY_OF_WEEK)STime.day_of_week;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageToRsi(string symbol)
  {
   int oversold = getRsiOversold();
   int overbought = getRsiOverbought();
   bool signal = false;

   string message = "RSI SIGNAL!!!" + "%0A";

   int rsiSignal = getRsiSignal(symbol, RSI_NumOfCandles);

   if(rsiSignal == SIGNAL_BUY)
     {
      message += "BUY: RSI crossover " + StringFormat("%d", oversold);
      signal = true;
     }

   if(rsiSignal == SIGNAL_SELL)
     {
      message += "SELL: RSI crossunder " + StringFormat("%d", overbought);
      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageCloseToRsi(string symbol)
  {
   bool signal = false;

   string message = "RSI SIGNAL!!!" + "%0A";

   int rsiSignal = getRsiSignal(symbol, 3);

   if(rsiSignal == SIGNAL_BUY)
     {
      message += "RSI oversold! IF L3 THEN CLOSE SELL!";
      signal = true;
     }

   if(rsiSignal == SIGNAL_SELL)
     {
      message += "RSI overbought! IF L3 THEN CLOSE BUY!";
      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageToCci(string symbol)
  {
   double cci[];
   int cciHanle = iCCI(symbol, (ENUM_TIMEFRAMES) Timeframe, 14, PRICE_TYPICAL);
   int upper = 100;
   int lower = -100;
   bool signal = false;

   CopyBuffer(cciHanle, 0, 0, 2, cci);

   string message = "SIGNAL!!!" + "%0A";

   if(cci[0] < lower && cci[1] >= lower)
     {
      message += "SELL: CCI crossover " + StringFormat("%d", lower);
      signal = true;
     }


   if(cci[0] > upper && cci[1] <= upper)
     {
      message += "BUY: CCI crossunder " + StringFormat("%d", upper);
      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }

   ArrayFree(cci);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageToNews()
  {
   MqlDateTime STime;
   datetime time_current=TimeCurrent(STime);
   string dayOfWeek = EnumToString((ENUM_DAY_OF_WEEK)STime.day_of_week);

   int offset = TradingStopNews_FreezeInMinutes * 60;

   datetime currentTime = TimeCurrent();

   for(int i=0; i<ArraySize(todayEvents); i++)
     {
      if(// getDailyNewsType(todayEvents[i].name) &&
         currentTime + offset + 60 >= todayEvents[i].time
         && currentTime + offset <= todayEvents[i].time
         && todayEvents[i].isShowed == false
      )
        {
         todayEvents[i].isShowed = true;
         string message = "===" + todayEvents[i].symbol + "-" + todayEvents[i].name + "(" + dayOfWeek + "|" + todayEvents[i].importanceLevel +  "|" + todayEvents[i].time + ")-closed=" + todayEvents[i].isClosed + " in " + TradingStopNews_FreezeInMinutes + " minutes.";
         printf(message);
         SendMessage(message, ChatID, BotToken);
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageToAlert(string symbol)
  {
   bool isAlert = false;

   if(bot.alertPrice <= 0)
     {
      return;
     }

   MqlRates rate[];
   CopyRates(symbol,(ENUM_TIMEFRAMES) Timeframe,0,2,rate);

   double currentPrice = rate[1].close;
   double previousPrice = rate[0].close;
   double aPrice = bot.alertPrice;

   if(previousPrice <= aPrice && aPrice <= currentPrice)
     {
      isAlert = true;
     }

   if(currentPrice <= aPrice && aPrice <= previousPrice)
     {
      isAlert = true;
     }

   if(isAlert)
     {
      string message = "ALERT!Price crossed: " + bot.alertPrice;
      printf(message);
      SendMessage(message, ChatID, BotToken);
      bot.alertPrice = 0;
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageToBalanceChange(string symbol)
  {
   double newBalance = AccountInfoDouble(ACCOUNT_BALANCE);

   if(currentBalance != newBalance)
     {
      if(currentBalance < newBalance)
        {
         string message        = "";
         message       += StringFormat("BALANCE=%G. TP=%+.2f$", newBalance, (newBalance - currentBalance)) + "";
         message       += StringFormat("%s %d", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN)) + "%0A" + getSymbolMessage();
         StringReplace(message, "+", "%2b");

         printf(message);
         if(ChatID != "" && BotToken != "")
           {
            SendMessage(message, ChatID, BotToken);
           }
        }
      currentBalance = newBalance;
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendPositionsChanged()
  {
   int total = PositionsTotal();

   if(numOfPositions != total)
     {
      string message = "NOTICE!" + "";
      message       += "[PO]" + StringFormat("%d", numOfPositions) + "-" + StringFormat("%d", total) + "";
      message       += getPositionMessage();
      message       += StringFormat("[BAL]%G",AccountInfoDouble(ACCOUNT_BALANCE)) + "";
      message       += StringFormat("[PRO]%G",AccountInfoDouble(ACCOUNT_PROFIT)) + "";
      message       += StringFormat("[EQU]%G",AccountInfoDouble(ACCOUNT_EQUITY)) + "";
      message       += "%0A" + StringFormat("%s %d", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN)) + "%0A" + getSymbolMessage();

      numOfPositions = PositionsTotal();

      printf(message);
      SendMessage(message, ChatID, BotToken);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string            getSymbolMessage()
  {
   string message = "";
   message += "1:" + AccountInfoInteger(ACCOUNT_LEVERAGE) + "|";

   for(int i = 0; i < ArraySize(symbols); i++)
     {
      string symbol = symbols[i];
      message += symbol + ":" + SymbolInfoDouble(symbol, SYMBOL_TRADE_CONTRACT_SIZE) + "|";
     }

   return message;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string            getPositionMessage()
  {
   string message = "";

   for(int i = 0; i < ArraySize(symbols); i++)
     {
      string symbol = symbols[i];
      message += "%0A";
      message += "[" + symbol + "]";

      int buyCount = 0;
      int sellCount = 0;

      for(int i = PositionsTotal() - 1; i >= 0; i--)
        {
         if(PositionGetTicket(i)
            && PositionGetSymbol(i) == symbol
            && PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY
           )
           {
            buyCount++;
           }
        }

      for(int i = PositionsTotal() - 1; i >= 0; i--)
        {
         if(PositionGetTicket(i)
            && PositionGetSymbol(i) == symbol
            && PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL
           )
           {
            sellCount++;
           }
        }


      if(buyCount > 0)
        {
         message += StringFormat("BUY:L%d", buyCount);
        }

      if(sellCount > 0)
        {
         message += StringFormat("SELL:L%d", sellCount);
        }
     }

   return message;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string getNewsMessage()
  {
   string message = "";
   message += "===CURRENT-TIME:" + TimeCurrent() + "\n";

   for(int i=0; i<ArraySize(todayEvents); i++)
     {
      message += "===" + todayEvents[i].symbol + "-" + todayEvents[i].name + "(" + todayEvents[i].importanceLevel +  "|" + todayEvents[i].time + ")-closed=" + todayEvents[i].isClosed + "\n";
     }

   return message;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string getConfigMessage()
  {
   string message  = "";
   for(int i=0; i<ArraySize(configs); i++)
     {
      KhanhdeuxConfig config = configs[i];
      message += config.index  + ".";
      message += config.symbol;
      message += "," + (config.active == true ? "ACTIVE" : "INACTIVE");
      message += "," + (config.orderType == DCA_BUY ? "BUY": "SELL");
      message += ",range=[" + config.lowerPrice + "," + config.upperPrice + "]";
      message += ",sl=[" + doubleArrayToString(config.stoploss)   + "]";
      message += ",tp=[" + doubleArrayToString(config.takeprofit) + "]";
      message += ",points=[" + doubleArrayToString(config.points) + "]";
      message += "\n";
     }

   return message;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string getActionMessage()
  {
   return "\n" + StringFormat("%s %d", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN)) + "\n" + getSymbolMessage() + "\n";
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getDayProfit()
  {
   double dayprof = 0.0;
   datetime end = TimeCurrent();
   string sdate = TimeToString(TimeCurrent(), TIME_DATE);
   datetime start = StringToTime(sdate);

   HistorySelect(start,end);
   int TotalDeals = HistoryDealsTotal();

   for(int i = 0; i < TotalDeals; i++)
     {
      ulong Ticket = HistoryDealGetTicket(i);

      if(HistoryDealGetInteger(Ticket,DEAL_ENTRY) == DEAL_ENTRY_OUT)
        {
         double LatestProfit = HistoryDealGetDouble(Ticket, DEAL_PROFIT);
         dayprof += LatestProfit;
        }
     }

   return dayprof;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool isSummerTime()
  {
   datetime    tm=TimeCurrent();
   MqlDateTime stm;
   TimeToStruct(tm,stm);

   datetime dst_start,dst_end;
   dst_start=dst_end=0;

   DST_USA(stm.year,dst_start,dst_end);
   return dst_start <= tm && tm <= dst_end;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime getTradingSession(ENUM_SESSION_TYPE type, bool isStart = true)
  {
   int hourStart = 0;
   int hourStop = 0;

   switch(type)
     {
      case SYDNEY:
         hourStart = isSummerTime() ? 22 : 23;
         hourStop  = isSummerTime() ? 7 : 8;
         break;
      case TOKYO:
         hourStart = isSummerTime() ? 0 : 1;
         hourStop  = isSummerTime() ? 9 : 10;
         break;
      case LONDON:
         hourStart = isSummerTime() ? 9 : 10;
         hourStop  = isSummerTime() ? 18 : 19;
         break;
      case NEWYORK:
         hourStart = isSummerTime() ? 14 : 15;
         hourStop  = isSummerTime() ? 23 : 0;
         break;
     }

   MqlDateTime structTime;
   TimeCurrent(structTime);
   structTime.sec = 0;
   structTime.hour = hourStart;
   structTime.min = 0;
   datetime timeStart = StructToTime(structTime);
   structTime.hour = hourStop;
   datetime timeEnd = StructToTime(structTime);

   if(hourStart > hourStop)
     {
      timeEnd = timeEnd + 24 * 60 * 60;
     }

   return isStart ? timeStart : timeEnd;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool isMessageCommandType(string text)
  {
   return StringFind(text, "/", 0) == 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime getFirstDayOfNewsYear()
  {
   MqlDateTime firstDayOfYear;

   firstDayOfYear.year = News_Backtest_Year;
   firstDayOfYear.mon = 1;  // January
   firstDayOfYear.day = 1;  // 1st day
   firstDayOfYear.hour = 0;
   firstDayOfYear.min = 0;
   firstDayOfYear.sec = 0;

// Convert back to datetime
   return StructToTime(firstDayOfYear);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime getLastDayOfNewsYear()
  {
   MqlDateTime lastDayOfYear;

// Set the structure to the last day of the current year
   lastDayOfYear.year = News_Backtest_Year;
   lastDayOfYear.mon = 12;  // December
   lastDayOfYear.day = 31;  // 31st day
   lastDayOfYear.hour = 23;
   lastDayOfYear.min = 59;
   lastDayOfYear.sec = 59;

// Convert back to datetime
   return StructToTime(lastDayOfYear);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime createDateTimeFromString(string timeStr)
  {
// Get the current date
   MqlDateTime currentDate;
   TimeCurrent(currentDate);

// Split the time string "15:30" into hours and minutes
   string timeParts[];
   StringSplit(timeStr, ':', timeParts);

// Convert the hours and minutes from the string to integers
   int hour = StringToInteger(timeParts[0]);
   int minute = StringToInteger(timeParts[1]);

// Construct a datetime for the current day with the given hour and minute, and set seconds to 00
   MqlDateTime customDateTime;
   customDateTime.year   = currentDate.year;
   customDateTime.mon    = currentDate.mon;
   customDateTime.day    = currentDate.day;
   customDateTime.hour   = hour;
   customDateTime.min    = minute;
   customDateTime.sec    = 0;

// Convert MqlDateTime structure back to datetime type
   return StructToTime(customDateTime);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void stringToDoubleArray(const string str, double &points[])
  {
// Split the string by comma
   string parts[];
   int count = StringSplit(str, ',', parts);

// Resize the points array to match the number of elements
   ArrayResize(points, count);

// Convert each element to double and store in points array
   for(int i = 0; i < count; i++)
     {
      points[i] = StringToDouble(parts[i]);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string doubleArrayToString(const double &points[])
  {
   string result = "";

   for(int i = 0; i < ArraySize(points); i++)
     {
      // Append the double value to the result string
      result += DoubleToString(points[i]); // Convert to string with 2 decimal places

      // Add a comma if it's not the last element
      if(i < ArraySize(points) - 1)
         result += ",";
     }

   return result;
  }


//+------------------------------------------------------------------+
int               SendMessage(string text, string chatID, string botToken)
  {
   string baseUrl = "https://api.telegram.org";
   string headers = "";
   string requestURL = "";
   string requestHeaders = "";
   char resultData[];
   char posData[];
   int timeout = 2000;

// if not a command
   if(!isMessageCommandType(text))
     {
      Sleep(Notification_Delay);
     }

   requestURL = StringFormat("%s/bot%s/sendmessage?chat_id=%s&text=%s", baseUrl, botToken, chatID, text);

   int response = WebRequest("POST", requestURL, headers, timeout, posData, resultData, requestHeaders);

   string resultMessage = CharArrayToString(resultData);
   Print(resultMessage);

   return response;
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
void              algoTradingToggle(bool newStatus_True_Or_False)
  {
//--- getting the current status
   bool currentStatus = (bool) TerminalInfoInteger(TERMINAL_TRADE_ALLOWED);

//--- if the current status is equal to input trueFalse then, no need to toggle auto-trading
   if(currentStatus != newStatus_True_Or_False)
     {
      //--- Toggle Auto-Trading
      HANDLE hChart = (HANDLE) ChartGetInteger(ChartID(), CHART_WINDOW_HANDLE);
      PostMessageW(GetAncestor(hChart, GA_ROOT), WM_COMMAND, MT_WMCMD_EXPERTS, 0);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void deleteFile(string filePath)
  {
// Close any open file handle before deleting
   int fileHandle = FileOpen(filePath, FILE_READ | FILE_ANSI);
   if(fileHandle != INVALID_HANDLE)
     {
      FileClose(fileHandle);
     }

// Delete the file if it exists
   if(FileIsExist(filePath, FILE_COMMON))
     {
      if(FileDelete(filePath, FILE_COMMON))
        {
         Print("File deleted: ", filePath);
        }
      else
        {
         Print("Failed to delete file: ", filePath);
        }
     }
   else
     {
      Print("File does not exist: ", filePath);
     }
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
