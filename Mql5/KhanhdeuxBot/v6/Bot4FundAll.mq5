//+------------------------------------------------------------------+
//|                                            ClimberFundHelper.mq5 |
//|                                  Copyright 2023, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2024, Khanhdeux."
#property link      "https://www.mql5.com"
#property version   "6.00"

#include <Trade\PositionInfo.mqh>
#include <Trade\Trade.mqh>
#include <Telegram\Telegram.mqh>
#include <RegularExpressions\Regex.mqh>
#include <Arrays\ArrayLong.mqh>
#include <Daylight.mqh>
#include <News.mqh>

//--- importing required dll files
#define MT_WMCMD_EXPERTS   32851
#define WM_COMMAND 0x0111
#define GA_ROOT    2
#include <WinAPI\winapi.mqh>

int SIGNAL_BUY     =  1;
int SIGNAL_SELL    =  -1;
int SIGNAL_CLOSE   =  2;
int SIGNAL_DCA     =  3;
int SIGNAL_HEDG    =  4;
int SIGNAL_PAUSE   =  5;
int SIGNAL_NONE   =  6;
int SIGNAL_DCA_2 = 7;
int SIGNAL_DCA_3 = 8;

int CLOSE_KILL   =  1;

string COMMAND_MASTER_BUY = "/master-buy";
string COMMAND_MASTER_SELL = "/master-sell";

enum ENUM_DCA_ORDER_TYPE {DCA_BUY, DCA_SELL};
enum ENUM_SESSION_TYPE {SYDNEY, TOKYO, LONDON, NEWYORK};
enum ENUM_STRATEGY_TYPE {DCA, HEDG, DCA_2, DCA_3, NONE};
enum ENUM_RISK_TYPE {RISK_LOW, RISK_HIGH};
enum  ENUM_STRATEGY_ENTRY_TYPE
  {
   ENTRY_TYPE_RSI, // Rsi
   ENTRY_TYPE_FVG, // FVG
   ENTRY_TYPE_LAST_DAY_PRICE, // Last day price
   ENTRY_TYPE_MOVING_AVERAGE, // Moving average
   ENTRY_TYPE_SMA, // SMA
   ENTRY_TYPE_MARKET_PROFILE, // Market profile
  };

enum  ENUM_VOLUME_TYPE
  {
   VOL_MUL, // Multiply
   VOL_FIB,  // Fibonacci
   VOL_UNI, // Unique
  };

enum  ENUM_SIGNAL_FILTER
  {
   ONLY_BUY,      // Only buy
   ONLY_SELL,     // Only sell
   BOTH_BUY_SELL, // Both
  };

enum ENUM_NEWS_TYPE
  {
   DEFAULT,
   JOBLESS_CLAIM,
   CPI,
   SPEECH,
   NONFARM,
   GDP,
   PMI,
   GOODS,
   HOME_SALES,
   JOLTS,
   BUILDING_PERMIT,
   RETAIL_SALES,
   PPI,
   FED,
   MCS,
   CCI,
   PCE,
   OTHERS,
   CUSTOM
  };

CTrade trade;
CPositionInfo position;
COrderInfo order;

double currentBalance;
datetime dailyDate;
double htfRsi;
datetime lastBarTime = 0;

// Global constants

int    RSI_M5_BUY_LEVEL1 = 40; // Rsi M5 Buy level 1
int    RSI_M5_BUY_LEVEL2 = 35; // Rsi M5 Buy level 2
int    RSI_M5_BUY_LEVEL3 = 30; // Rsi M5 Buy level 3
int    RSI_M5_SELL_LEVEL1 = 60; // Rsi M5 Sell level 1
int    RSI_M5_SELL_LEVEL2 = 65; // Rsi M5 Sell level 2
int    RSI_M5_SELL_LEVEL3 = 70; // Rsi M5 Sell level 3
int    RSI_Overbought = 70; // Rsi Overbought
int    RSI_Oversold = 30; // Rsi Oversold

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class KhanhdeuxConfiguration
  {
public:
   double               distance;
   int                  maxDistance;
   ENUM_STRATEGY_TYPE   strategyType;
   ENUM_STRATEGY_ENTRY_TYPE entryType;
   bool                 signalReversed;
   ENUM_VOLUME_TYPE     volumeType;
public:
   //--- Default constructor
                     KhanhdeuxConfiguration() {};
   //--- Parametric constructor
                     KhanhdeuxConfiguration(double d, int m, ENUM_STRATEGY_TYPE st,ENUM_STRATEGY_ENTRY_TYPE et, bool sr, ENUM_VOLUME_TYPE vt)
     {
      distance = d;
      maxDistance = m;
      strategyType = st;
      entryType = et;
      signalReversed = sr;
      volumeType = vt;
     }
  };

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class KhanhdeuxSetup
  {
public:
   string            symbol;
   double            lotsize;
   double            distance;
   int               maxDistance;
   ENUM_VOLUME_TYPE     volumeType;
public:
   //--- Default constructor
                     KhanhdeuxSetup() {};
   //--- Parametric constructor
                     KhanhdeuxSetup(string s, double l, double d, double m, ENUM_VOLUME_TYPE vt)
     {
      symbol = s;
      lotsize = l;
      distance = d;
      maxDistance = m;
      volumeType = vt;
     }

   // Clone method
   KhanhdeuxSetup*   Clone()
     {
      // Create a new object and return a pointer to it
      return new KhanhdeuxSetup(symbol, lotsize, distance, maxDistance, volumeType);
     }
  };

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class KhanhdeuxEvent
  {
public:
   string            symbol;
   string            name;
   datetime          time;
   int               importanceLevel;
   bool              isClosed;
   bool              isShowed;
public:
   //--- Default constructor
                     KhanhdeuxEvent() {};
   //--- Parametric constructor
                     KhanhdeuxEvent(string s, string n, datetime t, int l, bool ic)
     {
      symbol = s;
      name = n;
      time = t;
      importanceLevel = l;
      isClosed = ic;
     }
  };

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string            accountPattern = "\\/(\\d+) \\b(on|off|kill|reset|auto|manual|master|unmaster|slave|unslave|pause|unpause|close-news|unclose-news)\\b";
string            symbolPattern  = "\\/([A-Za-z0-9]+(?:[.+][A-Za-z0-9]*)?) \\b(reset|buy|sell|cbuy|csell|pause|unpause)\\b";
string            eventPattern  = "\\/\\b(event|unevent)\\b ([A-Za-z]{2,3}) (\d{2}:\d{2})(?: (true|false))?";

// level-1: all in account, level-2 : all by symbol

//+------------------------------------------------------------------+
//|   KhanhdeuxBot                                                   |
//+------------------------------------------------------------------+
class KhanhdeuxBot: public CCustomBot
  {
public:
   int               signal;
   string            signalSymbol;
   double            lotsize;
   double            distance;
   int               maxDistance;
   double            initCaptital;
   double            closePrice;
   int               closeType;
   double            alertPrice;
   bool              manual;
   bool              isActive;
   bool              isMaster;
   bool              isSlave;
   bool              isPaused;
   bool              isSymbolPaused;
   int               volumeType;
   bool              isCloseOnNews;

public:
   bool              checkAccountPattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(accountPattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   string              setAccountPattern(const string text)
     {
      int accountLogin = 0;
      string actionType = "";
      string message = "";

      CMatchCollection *matches=CRegex::Matches(text,accountPattern);
      CMatchEnumerator *en=matches.GetEnumerator();

      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         accountLogin = match.Groups()[1].Value();
         actionType =  match.Groups()[2].Value();
        }

      int currentAccount = (int)AccountInfoInteger(ACCOUNT_LOGIN);

      if(currentAccount == accountLogin)
        {
         if(actionType == "on")
           {
            isActive = true;
           }

         if(actionType == "off")
           {
            isActive = false;
           }

         if(actionType == "kill")
           {
            closeAllPositions();
            isActive = false;
           }

         if(actionType == "reset")
           {
            closeAllPositions();
           }

         if(actionType == "auto")
           {
            manual = false;
           }

         if(actionType == "manual")
           {
            manual = true;
           }

         if(actionType == "master")
           {
            isMaster = true;
           }

         if(actionType == "unmaster")
           {
            isMaster = false;
           }

         if(actionType == "slave")
           {
            isSlave = true;
           }

         if(actionType == "unslave")
           {
            isSlave = false;
           }

         if(actionType == "pause")
           {
            isPaused = true;
           }

         if(actionType == "unpause")
           {
            isPaused = false;
           }

         if(actionType == "close-news")
           {
            isCloseOnNews = true;
           }

         if(actionType == "unclose-news")
           {
            isCloseOnNews = false;
           }

         message += actionType + "\n";
        }

      delete en;
      delete matches;

      return message;
     }

   bool              checkSymbolPattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(symbolPattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   string              setSymbolPattern(const string text)
     {
      string symbol = "";
      string actionType = "";
      string message = "";

      CMatchCollection *matches=CRegex::Matches(text,symbolPattern);
      CMatchEnumerator *en=matches.GetEnumerator();

      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         symbol = match.Groups()[1].Value();
         actionType =  match.Groups()[2].Value();
        }

      if(actionType == "reset")
        {
         closeAllPositions(symbol);
        }

      if(actionType == "buy")
        {
         signal = SIGNAL_BUY;
         signalSymbol = symbol;
        }

      if(actionType == "sell")
        {
         signal = SIGNAL_SELL;
         signalSymbol = symbol;
        }

      if(actionType == "cbuy")
        {
         closeAllDCAPositions(symbol, DCA_BUY);
        }

      if(actionType == "csell")
        {
         closeAllDCAPositions(symbol, DCA_SELL);
        }

      if(actionType == "pause")
        {
         isSymbolPaused = true;
         signalSymbol = symbol;
        }

      if(actionType == "unpause")
        {
         isSymbolPaused = false;
         signalSymbol = symbol;
        }

      message += "Symbol:" + symbol + ", Action:" + actionType + "\n";

      delete en;
      delete matches;

      return message;
     }

   bool              checkEventPattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(eventPattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   string              setEventPattern(const string text)
     {
      string actionType = "";
      string symbol = "";
      string time = "";
      bool   isClosed = false;
      string message = "";

      CMatchCollection *matches=CRegex::Matches(text,eventPattern);
      CMatchEnumerator *en=matches.GetEnumerator();

      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         actionType = match.Groups()[1].Value();
         symbol = match.Groups()[2].Value();
         time =  match.Groups()[3].Value();
         isClosed = (actionType == "event" && match.Groups()[4].Value() == "true") ? true : false;

         if(actionType == "event")
           {
            addTodayEvent(new KhanhdeuxEvent(symbol, "CUSTOM", createDateTimeFromString(time), 3, isClosed), todayEvents);
           }

         if(actionType == "unevent")
           {
            removeTodayEvent(symbol, createDateTimeFromString(time), todayEvents);
           }

         message += getNewsMessage();
        }

      message += "ActionType:" + actionType + " - Symbol:" + symbol + ", News at time:" + time + ". IsClosed:" + isClosed +  "\n";

      delete en;
      delete matches;

      return message;
     }

   void              ProcessMessages(void)
     {
      for(int i=0; i<m_chats.Total(); i++)
        {
         CCustomChat *chat=m_chats.GetNodeAtIndex(i);
         //--- if the message is not processed
         if(!chat.m_new_one.done)
           {
            chat.m_new_one.done=true;
            string text=chat.m_new_one.message_text;
            string actionMessage = StringFormat("%s %d", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN)) + "\n" + getSymbolMessage() + "\n";

            if(text==NULL || text=="")
              {
               continue;
              }

            if(isMessageCommandType(text))
              {
               Sleep(Notification_Delay);
              }

            //--- start
            if(text=="/test")
               SendMessage(chat.m_id, actionMessage + "Test! I am KhanhdeuxBot. \xF680");

            //--- help
            if(text=="/help")
              {
               string helpText = "My commands list: ";
               helpText += "\n/test test bot";
               helpText += "\n/info account";
               helpText += "\n/news today news";
               helpText += "\n/event|unevent cad|us|gbp|eur 16:30 true|false: close on news?";
               //helpText += "\n/plan Plan for the week";
               //helpText += "\n/config Config";
               helpText += "\n/on/off/pause/unpause/kill/reset/close-news/unclose-news .General setup";
               helpText += "\n/account on|off|kill|reset|auto|manual|master|unmaster|slave|unslave|pause|unpause|close-news|unclose-news";
               helpText += "\n/symbol reset|buy|sell|cbuy|csell|pause|unpause";

               SendMessage(chat.m_id,helpText);
              }
            if(text=="/info")
              {
               KhanhdeuxConfiguration* config = getConfiguration(getSymbol());

               string message = "";
               message       += StringFormat("[BAL]%G",AccountInfoDouble(ACCOUNT_BALANCE)) + "";
               message       += StringFormat("[PROF]%G",AccountInfoDouble(ACCOUNT_PROFIT)) + "";
               message       += StringFormat("[EQU]%G",AccountInfoDouble(ACCOUNT_EQUITY)) + "";
               message       += StringFormat("[SL]%G", getStoploss()) + "";
               message       += StringFormat("[ALGO]%G", (bool) TerminalInfoInteger(TERMINAL_TRADE_ALLOWED)) + "";
               message       += StringFormat("[ACTIVE]%s", bot.isActive ? "true" : "false") + "";
               message       += StringFormat("[PAUSE]%s", bot.isPaused ? "true" : "false") + "";
               message       += StringFormat("[MASTER]%s", bot.isMaster ? "true" : "false") + "";
               message       += StringFormat("[SLAVE]%s", bot.isSlave ? "true" : "false") + "";
               message       += StringFormat("[CLOSEONNEWS]%s", bot.isCloseOnNews ? "true" : "false") + "";

               message       += StringFormat("[VOLUME_TYPE]%s", getDCAVolumeTypeMessage()) + "";
               message       += StringFormat("[TYPE]%s", getDCAStrategyTypeMessage()) + "";
               message       += StringFormat("[ENTRY_TYPE]%s", getEntryTypeMessage()) + "";
               message       += StringFormat("[REVERSED]%s", getIsSignalReversedMessage()) + "";
               message       += StringFormat("[LOT]%s", getDCALotSizeMessage()) + "";
               message       += StringFormat("[DIST]%s", getDCADistanceMessage()) + "";
               message       += StringFormat("[MAX]%s", getDCAMaxDistanceMessage()) + "";

               SendMessage(chat.m_id, actionMessage + message);
              }

            if(text =="/news")
              {
               string message = getNewsMessage();
               SendMessage(chat.m_id, actionMessage + message);
              }

            if(text=="/plan")
              {
               string message = getPlanMessage();
               SendMessage(chat.m_id, actionMessage + message);
              }

            if(text=="/buy" || (text==COMMAND_MASTER_BUY && bot.isSlave))
              {
               signal = SIGNAL_BUY;
               SendMessage(chat.m_id, actionMessage + "Received BUY order");
              }

            if(text=="/sell" || (text==COMMAND_MASTER_SELL && bot.isSlave))
              {
               signal = SIGNAL_SELL;
               SendMessage(chat.m_id, actionMessage + "Received SELL order");
              }

            if(text=="/pause")
              {
               isPaused = true;
               SendMessage(chat.m_id, actionMessage + "Received PAUSE order");
              }

            if(text=="/unpause")
              {
               isPaused = false;
               SendMessage(chat.m_id, actionMessage + "Received UNPAUSE order");
              }

            if(text=="/on")
              {
               SendMessage(chat.m_id, actionMessage + "Received ACTIVATE Algo");
               bot.isActive = true;
              }

            if(text=="/off")
              {
               SendMessage(chat.m_id, actionMessage + "Received DEACTIVATE Algo");
               bot.isActive = false;
              }

            if(text=="/kill" || text=="/k")
              {
               closeAllPositions();
               isActive = false;
              }

            if(text=="/reset" || text=="/r")
              {
               SendMessage(chat.m_id, actionMessage + "Received RESET order");
               closeAllPositions();
              }

            if(text=="/close-news" || text=="/cn")
              {
               SendMessage(chat.m_id, actionMessage + "Received CLOSE ON NEWS order");
               isCloseOnNews = true;
              }

            if(text=="/unclose-news" || text=="/u-n")
              {
               SendMessage(chat.m_id, actionMessage + "Received UNCLOSE ON NEWS order");
               isCloseOnNews = false;
              }

            if(checkEventPattern(text))
              {
               SendMessage(chat.m_id, actionMessage + setEventPattern(text));
               continue;
              }

            if(checkAccountPattern(text))
              {
               SendMessage(chat.m_id, actionMessage + setAccountPattern(text));
               continue;
              }

            if(checkSymbolPattern(text))
              {
               SendMessage(chat.m_id, actionMessage + setSymbolPattern(text));
               continue;
              }
           }
        }
     }
  };

input group  "==== Telegram Credentials ==="
input string ChatID = ""; // Chat ID
input string BotToken = ""; // Bot Token
input bool TestTelegram = true; // Test Telegram Signal

input group "==== LIVE CONFIG ===="

input double  ACCOUNT_BALANCE_INIT = 100000; // Account balance init

input bool isBotActive = true; // Is Bot Active?
input bool isBotMaster = false; // Is Bot Master?
input bool isBotSlave = false; // is Bot Slave?

input group "=="
input string  SEPARATOR1 = ""; // ----Monday----
input string              MONDAY_Distances  = "50";            // Distances (";" separated)
input string              MONDAY_VolumeTypes = "1";            // Volume type (";" separated). 0:MUL, 1:FIB, 2:UNI
input string  SEPARATOR2 = ""; // ----Tuesday----
input string              TUESDAY_Distances = "50";            // Distances
input string              TUESDAY_VolumeTypes = "1";           // Volume type
input string  SEPARATOR3 = ""; // ----Wednesday----
input string              WEDNESDAY_Distances = "50";          // Distances
input string              WEDNESDAY_VolumeTypes = "1";         // Volume type
input string  SEPARATOR4 = ""; // ----Thursday----
input string              THURSDAY_Distances = "50";           // Distances
input string              THURSDAY_VolumeTypes = "1";          // Volume type
input string  SEPARATOR5 = ""; // ----Friday----
input string              FRIDAY_Distances = "50";             // Distances
input string              FRIDAY_VolumeTypes = "1";            // Volume type
input string  SEPARATOR6 = ""; // ----Saturday----
input string              SATURDAY_Distances = "50";           // Distances
input string              SATURDAY_VolumeTypes = "1";          // Volume type
input string  SEPARATOR7 = ""; // ----Sunday----
input string              SUNDAY_Distances = "50";             // Distances
input string              SUNDAY_VolumeTypes = "1";            // Volume type

input group "==== BACKTEST ===="
input bool    DCA_Strategy_Backtest = false; // DCA Strategy Test
input double  DCA_Strategy_Distance = 50; // Distance between L in points (10 pts=1 pip)
input ENUM_VOLUME_TYPE DCA_Strategy_Volume_Type = VOL_FIB; // Volume type

input string  SEPARATOR0 = ""; // ----
input string  SYMBOLS = "XAUUSD;US2000.cash"; // Symbols (";" separated)
input int     DCA_Strategy_MaxDistance = 4; // Number of L that can be reached
input ENUM_STRATEGY_TYPE DCA_Strategy_Type = HEDG; // Strategy type
input ENUM_STRATEGY_ENTRY_TYPE DCA_Strategy_Entry_Type = ENTRY_TYPE_SMA; // Entry type
input bool    DCA_Strategy_Signal_Reversed = false; // Signal Reversed?
input double DCA_Strategy_InitLotSize = 0; // Init lotsize
input bool             Trading_DayOfWeek_Active = false;
input ENUM_DAY_OF_WEEK Trading_DayOfWeek = ""; // Trading day of week
enum  ENUM_SESSION
  {
   SESSION_ASIA, // ASIA
   SESSION_UK, // UK
   SESSION_US // US
  };
input bool         TradingSession_Active = false;
input ENUM_SESSION TradingSession_Type = SESSION_ASIA; // Session type

input group "==== TEST Config ===="
input double  TEST_MaxDrawdown = 1.5; // Maxdrawdown in %
enum  ENUM_TEST_CRITERIA {NET_PROFIT, STOPLOSS_RATE, TAKEPROFIT_RATE, TP_BY_SL};
input ENUM_TEST_CRITERIA TEST_Criteria = STOPLOSS_RATE; // Criteria
input bool    TEST_NewReport = false; // New csv file?
input int     TEST_MaxNumberOfResult = 10; // Max number of result
input bool    TEST_ShowAll = false; // Show all result

input group  "==== News Config ==="
input bool    News_Backtest = false;  // News Backtest?
input int     News_Backtest_Year = 2024;  // News Backtest year

input bool    TradingStopNews_Active = true; // Trading Stop on News
input int     TradingStopNews_FreezeInMinutes = 5; // Freeze in minute
input bool    TradingStopNews_CloseAllPositions = false; // Close all positions

input group "==== Entry time ===="
input bool    TradingEntryTime_Active = false; // Trading Entry time
input int     TradingEntryTime_HourStart = 6; // Entry Hour Start
input int     TradingEntryTime_HourStop = 18; // Entry Hour Stop

input group  "==== Trade time ==="
input bool    TradingGo_Active = false; // Trading Go active (GMT+2(w),+3(s))
input int     TradingGo_HourStart = 6; // Go Hour Start
input int     TradingGo_HourStop = 20; // Go Hour Stop
input bool    TradingGo_CloseAllPositions = true; // Close all positions outside Go Hour

input group  "==== No trade time ==="
input bool    TradingStop_Active = false; // Trading Stop active (GMT+2(w),+3(s))
input int     TradingStop_HourStart = 0; // Stop Hour Start
input int     TradingStop_HourStop = 1; // Stop Hour Stop
input bool    TradingStop_CloseAllPositions = true; // Close all positions before Stop Hour

input group  "==== Session Config ==="
input bool    Session_Active = false; // Session active?

input string  SEPARATOR22 = ""; // ----ASIA----
input int    ASIA_HourStart = 0; // Start hour
input int    ASIA_HourStop = 6; // Stop hour

input string  SEPARATOR23 = ""; // ----UK---
input int    UK_HourStart = 6; // Start hour
input int    UK_HourStop = 19; // Stop hour

input string  SEPARATOR24 = ""; // ----US---
input int    US_HourStart = 19; // Start hour
input int    US_HourStop = 24; // Stop hour

input bool   Session_CloseAllPositions = true; // Close all positions after session

input group  "==== Trading Stop Daily ==="
input bool    TradingStoplossDaily_Active    = true; // Trading Stoploss Daily
input bool    TradingStopDaily_Active        = true; // Trading Stop Daily (23:40)
input double  TradingStopDaily  = 4.7;  // Max daily stoploss (E.g 5%)
input double  TradingStopLoss  = 3;     // Max stoploss (E.g 5%)

input group  "==== Trading Profit Daily ==="
input bool    TradingProfitDaily_Active = false; // Trading Profit Daily
input double  TradingProfitDaily  = 3;  // Max daily takeprofit (E.g 5%)
input bool    TradingStopProfit_Active = false; // Trading Stop Profit
input double  TradingStopProfit  = 0.5;  // Max Stop profit (E.g 0.5%)
input bool    TradingLimitProfit_Active = false; // Trading Limit Profit
input int     TradingLimitProfit_Number = 0; // Number on Limit Profit

input group "==== Holding time ===="
input double POSITION_HOLDING_Factor = 0.1; // Holding factor (Holding time in hour = dist * holding factor)
input bool   POSITION_HOLDING_Stoploss = true; // Set stoploss or close all

input group "==== Timeframe ===="
enum CUSTOM_TIMEFRAMES
  {
   TF_M1   = PERIOD_M1,  // M1
   TF_M5   = PERIOD_M5,  // M5
   TF_M15  = PERIOD_M15, // M15
   TF_H1   = PERIOD_H1,  // H1
   TF_H4   = PERIOD_H4,  // H4
   TF_D1   = PERIOD_D1   // D1
  };

input CUSTOM_TIMEFRAMES   Timeframe = TF_M1;
input CUSTOM_TIMEFRAMES   Timeframe_High = TF_M5;

input group  "==== Next Order Config ==="
input bool              NextOrderWhenCandleClosed = false; // Next order only when candle closed?
input CUSTOM_TIMEFRAMES NextOrder_Timeframe = TF_M1; // Candle timeframe?

input group  "==== Notification ==="
input bool    Notification_Active = true; // Notification active
input int     Notification_Delay = 3000; // Notification delay
input bool    Warning_Signal = false; // Notification signal
input bool    Warning_Rsi = false; // Rsi Warning?
input bool    Warning_Session = false; // Session Warning?
input bool    Warning_News = false; // News Warning?
input bool    Warning_Cci = false; // CCi Warning?
input bool    Warning_FVG = false; // FVG Warning?

input group  "==== RSI ==="
input int    RSI_Length = 14; // RSI Length
input int    RSI_NumOfCandles = 5; // Number of RSI candles
input int    RSI_LastEntry_NumOfCandles = 5; // Rsi LastEntry Candles

input group  "==== Filter ==="
input ENUM_SIGNAL_FILTER DCA_Signal_Filter = BOTH_BUY_SELL; // Signal Filter
input int    NextOrderMin = 1; // Next order in minute
input bool   DCA_Strategy_DynamicStopLoss_Active = false; // Dynamic Stoploss (L>3)
input bool   DCA_Strategy_TrailingStopLoss_Active = false; // Trailing Stoploss (L=1)
input double DCA_Strategy_TrailingStopLoss_Step = 0.5; // Trailing Stoploss Step (L=1)
input bool   DCA_NoStoploss = false; // Set no stoploss
input bool   DCA_RefreshLevel = false; // Refresh level
input int    DCA_LastSignalEntry = 0; // Last signal entry
input bool   DCA_DynamicTakeProfit = false; // Dynamic take profit

input group  "==== DCA Config ==="
input double  DCA_Strategy_LotSize = 0.01; // Lot Size
input double  DCA_Strategy_DeviationMultiplier = 1; // Deviation Multiplier
input double  DCA_Strategy_LastDistance = 0; // Last Stoploss distance
input double  DCA_Strategy_AdditionalTP = 0; // Additional TP
input ulong   DCA_Strategy_MagicNumber = 01; // Magic Number

input group "==== DCA2 Config ===="
input double  DCA_2_Factor = 1.5; // DCA_2 Factor (TP = dist * Factor)
input int     DCA_2_Fraction = 1; // DCA_2 Fraction (Initlotsize / Fraction)
input int     DCA_2_StopLevel = 3; // DCA_2 Stop level

input group "==== DCA3 Config ===="
input double  DCA_3_InitLotsize = 0.1; // DCA_3 Init Lotsize
input double  DCA_3_DeadzoneFactor = 0.0; // DCA_3 Deadzone factor

input group "==== FVG Config ===="
input int     FVG_NumOfCandles = 4; // FVG Number of candles

input group "==== SMA Config ===="
input int SMA_FastPeriod = 15; // Fast period
input int SMA_SlowPeriod = 30; // Slow period
input int SMA_TrendPeriod = 0; // Trend period

input group "==== MarketProfile Config ===="
input int    MP_NumOfRows = 24; // MarketProfile Number of rows
input double MP_ValueArea = 70; // Value Area Volume %
enum  ENUM_MP_TYPE
  {
   MP_TREND,   // Trend
   MP_SIDEWAY, // Side way
   MP_POC,     // Poc
  };
input ENUM_MP_TYPE MP_MarketType = MP_TREND; // Market profile type

// input group  "==== Server time ==="
int     Server_Offset = 0; // Server offset

// input group  "==== DCA Analysis ==="
bool   DCA_Matrix = false; // Show DCA Matrix
double DCA_MinNumOfDistance = 1; // Number of min Level
double DCA_MaxNumOfDistance = 10; // Number of max Level
int    DCA_LotSize_Multiply = 2; // Lotsize multiply

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
KhanhdeuxBot      bot;
int               getmeResult;
int               numOfPositions;
KhanhdeuxEvent todayEvents[];
datetime todayDate;
KhanhdeuxSetup setups[];
string symbols[];

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               OnInit()
  {
   Print("=== INIT === ", TimeCurrent());
   initSymbols();
   initSetups();
// testPattern();

   numOfPositions = PositionsTotal();

   if(ChatID != "" && BotToken != "")
     {
      if(TestTelegram && Notification_Active)
        {
         string message = StringFormat("%s %d", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN)) + "%0A" + getSymbolMessage();
         SendMessage(message, ChatID, BotToken);
        }

      //--- set token
      bot.Token(BotToken);
      //--- check token
      getmeResult=bot.GetMe();
     }

   EventSetTimer(10);

   trade.SetExpertMagicNumber(DCA_Strategy_MagicNumber);

   bot.isActive = isBotActive;
   bot.isMaster = isBotMaster;
   bot.isSlave  = isBotSlave;

   if(DCA_Matrix)
     {
      showDCAMatrix();
     }

   if(News_Backtest)
     {
      if(!MQLInfoInteger(MQL_TESTER))
        {
         deleteFile("news\\newshistory.bin");
        }
      news.date_from = getFirstDayOfNewsYear();
      news.date_to = getLastDayOfNewsYear();
      news.SaveHistory(true);
      news.LoadHistory(true);
     }

//---
   return(INIT_SUCCEEDED);
  }

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void              OnDeinit(const int reason)
  {
//--- destroy timer
   EventKillTimer();
   bot.signal = 0;
   CRegex::ClearCache();
   ArrayFree(todayEvents);
   ArrayResize(todayEvents, 0);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void              OnTick()
  {
   executeEvents();
   executeTelegramBot();

   for(int i = 0; i < ArraySize(symbols); i++)
     {
      executeDCATrading(symbols[i]);
     }

   executeReset();
  }

//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
bool isOnTimerLoaded = false;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              OnTimer()
  {
   if(ChatID != "" && BotToken != "" && Notification_Active)
     {
      SendPositionsChanged();
      SendMessageToNews();
     }

// showLiveNews();
   isOnTimerLoaded = true;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void initSymbols()
  {
   StringSplit(SYMBOLS, ';', symbols);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void initSetups()
  {
   int count = 0;
   ArrayResize(setups, 0);

   for(int i = 0; i < ArraySize(symbols); i++)
     {
      string symbol = symbols[i];
      KhanhdeuxSetup setup = getBestSetupByTime(symbol);
      ArrayResize(setups, count + 1);
      setups[count++] = setup;
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string getSymbol()
  {
   return _Symbol;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
KhanhdeuxConfiguration* getConfiguration(string symbol, ENUM_DAY_OF_WEEK dayOfWeek = -1)
  {
   dayOfWeek = dayOfWeek == -1 ? getDayOfWeek(): dayOfWeek;

   double distance = DCA_Strategy_Distance;
   double maxDistance = DCA_Strategy_MaxDistance;
   ENUM_STRATEGY_TYPE strategyType = DCA_Strategy_Type;
   ENUM_STRATEGY_ENTRY_TYPE entryType = DCA_Strategy_Entry_Type;
   bool signalReversed = DCA_Strategy_Signal_Reversed;
   int symbolIndex = getSymbolIndex(symbol);
   ENUM_VOLUME_TYPE volumeType = DCA_Strategy_Volume_Type;

   string distances[];
   string volumeTypes[];

   switch(dayOfWeek)
     {
      case MONDAY:
         StringSplit(MONDAY_Distances, ';', distances);
         StringSplit(MONDAY_VolumeTypes, ';', volumeTypes);
         break;
      case TUESDAY:
         StringSplit(TUESDAY_Distances, ';', distances);
         StringSplit(TUESDAY_VolumeTypes, ';', volumeTypes);
         break;
      case WEDNESDAY:
         StringSplit(WEDNESDAY_Distances,';', distances);
         StringSplit(WEDNESDAY_VolumeTypes, ';', volumeTypes);
         break;
      case THURSDAY:
         StringSplit(THURSDAY_Distances, ';', distances);
         StringSplit(THURSDAY_VolumeTypes, ';', volumeTypes);
         break;
      case FRIDAY:
         StringSplit(FRIDAY_Distances, ';', distances);
         StringSplit(FRIDAY_VolumeTypes, ';', volumeTypes);
         break;
      case SATURDAY:
         StringSplit(SATURDAY_Distances, ';', distances);
         StringSplit(SATURDAY_VolumeTypes, ';', volumeTypes);
         break;
      case SUNDAY:
         StringSplit(SUNDAY_Distances, ';', distances);
         StringSplit(SUNDAY_VolumeTypes, ';', volumeTypes);
         break;
     }


   distance = StringToInteger(distances[symbolIndex > ArraySize(distances) - 1 ? 0 : symbolIndex]);
   volumeType = (ENUM_VOLUME_TYPE) StringToInteger(volumeTypes[symbolIndex > ArraySize(volumeTypes) - 1 ? 0 : symbolIndex]);

   return new KhanhdeuxConfiguration(distance, maxDistance, strategyType, entryType, signalReversed, volumeType);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getSymbolIndex(string symbol)
  {
   for(int i = 0; i < ArraySize(symbols); i++)
     {
      if(symbols[i] == symbol)
        {
         return i;
        }
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              testPattern()
  {
   string pattern= eventPattern;
   string in="/event USD 15:30 true";

   CRegex *r=new CRegex(pattern,RegexOptions::IgnoreCase);
   CMatch *m=r.Match(in);

   if(m.Success())
     {
      CMatchCollection *matches=CRegex::Matches(in,pattern);
      CMatchEnumerator *en=matches.GetEnumerator();
      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         Print("1: " + match.Groups()[1].Value());
         Print("2: " + match.Groups()[2].Value());
         Print("3: " + match.Groups()[3].Value());
         Print("4: " + match.Groups()[4].Value());
         Print("5: " + match.Groups()[5].Value());
         Print("\0");
        }

      delete en;
      delete matches;
     }

   delete r;
   delete m;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getDailyRange(string symbol)
  {
   MqlRates rate[];
   CopyRates(symbol,PERIOD_D1,0,1,rate);
   double high = rate[0].high;
   double low = rate[0].low;

   return high - low;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkPauseAction(string symbol = "")
  {
   if(bot.isPaused)
     {
      return true;
     }

   if(bot.isSymbolPaused && symbol == bot.signalSymbol)
     {
      return true;
     }

   return false;
  }

// NEWS AREA
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CNews             news;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool checkNews(bool checkForClosed, string symbol = "")
  {
   int offset = TradingStopNews_FreezeInMinutes * 60;
   datetime currentTime = TimeCurrent();

   for(int i = 0; i < ArraySize(todayEvents); i++)
     {
      string symbolInLowerCase = symbol;
      string eventSymBolInLowerCase = todayEvents[i].symbol;

      StringToLower(symbolInLowerCase);
      StringToLower(eventSymBolInLowerCase);

      // Check if the symbol matches or if we are checking for all symbols (symbol == "")
      if((StringFind(symbolInLowerCase, eventSymBolInLowerCase) != -1 || symbol == "")
         && currentTime + offset >= todayEvents[i].time  // Check if within upper time bound
         && currentTime - offset <= todayEvents[i].time  // Check if within lower time bound
        )
        {
         // If `checkForClosed` is true, check if the event is marked as closed
         if(checkForClosed)
           {
            if(todayEvents[i].isClosed)
               return true;
           }
         else
           {
            // Otherwise, just return true for stop news
            return true;
           }
        }
     }

   return false;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              addTodayEvent(KhanhdeuxEvent& event, KhanhdeuxEvent& todayEvents[])
  {
   bool eventExists = false;

   for(int i = 0; i < ArraySize(todayEvents); i++)
     {
      // Check if the symbol and name match
      if(todayEvents[i].symbol == event.symbol && todayEvents[i].name == event.name)
        {
         todayEvents[i].time = event.time;
         todayEvents[i].isClosed = event.isClosed;
         todayEvents[i].isShowed = false;
         eventExists = true;
         break;
        }
     }

   if(!eventExists)
     {
      int size = ArraySize(todayEvents);
      ArrayResize(todayEvents, size + 1);
      todayEvents[size] = event; // Add the new event
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void removeTodayEvent(string symbol, datetime eventTime, KhanhdeuxEvent& todayEvents[])
  {
   int i = 0;
   while(i < ArraySize(todayEvents))
     {
      // Check if the symbol and time match
      if(todayEvents[i].symbol == symbol && todayEvents[i].time == eventTime)
        {
         // Shift the array to the left starting from the current index
         for(int j = i; j < ArraySize(todayEvents) - 1; j++)
           {
            todayEvents[j] = todayEvents[j + 1];
           }
         ArrayResize(todayEvents, ArraySize(todayEvents) - 1); // Resize array to remove the last element
        }
      else
        {
         i++; // Only increment if no deletion, as deleting shifts the array
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int country_ids[] = {840, 999, 826, 124, 392, 36, 554};          // List of country IDs (US, EUR, GBP, CAD, JPY)
string country_codes[] = {"us", "eur", "gbp", "cad", "jpy", "aud", "nzd"}; // Associated country codes

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void setTodayEvents(KhanhdeuxEvent& todayEvents[])
  {
   int count = 0;
   int required_importance = 3; // Set importance level to 3

   datetime currentTime = TimeCurrent();
   datetime currentDate = StringToTime(TimeToString(currentTime, TIME_DATE));
   datetime nextDate = StringToTime(TimeToString(currentTime + 24 * 60 * 60, TIME_DATE));

   if(News_Backtest)
     {
      int totalnews = ArraySize(news.event);
      for(int i = 0; i < totalnews; i++)
        {
         int country_id = news.event[i].country_id;
         int event_importance = news.event[i].importance;

         for(int j = 0; j < ArraySize(country_ids); j++)
           {
            if(country_id == country_ids[j] && event_importance >= required_importance
               && currentDate <= news.event[i].time && news.event[i].time < nextDate)
              {
               bool eventExists = false;
               for(int k = 0; k < ArraySize(todayEvents); k++)
                 {
                  if(StringCompare(todayEvents[k].name, news.eventname[i]) == 0)
                    {
                     eventExists = true;
                     break;
                    }
                 }

               if(!eventExists)
                 {
                  ArrayResize(todayEvents, count + 1);
                  datetime eventTime = isSummerTime() ? news.event[i].time : news.event[i].time - 1 * 60 * 60;
                  todayEvents[count++] = new KhanhdeuxEvent(country_codes[j], news.eventname[i], eventTime, event_importance, false);
                 }
              }
           }
        }
     }
   else
     {
      MqlCalendarEvent event;
      MqlCalendarValue values[];

      if(CalendarValueHistory(values, currentDate, nextDate))
        {
         for(int i = 0; i < ArraySize(values); i++)
           {
            if(CalendarEventById(values[i].event_id, event))
              {
               int country_id = event.country_id;
               int event_importance = event.importance;

               for(int j = 0; j < ArraySize(country_ids); j++)
                 {
                  if(country_id == country_ids[j]
                     && event_importance >= required_importance
                     // && getDailyNewsType(event.name)

                    )
                    {
                     bool eventExists = false;
                     for(int k = 0; k < ArraySize(todayEvents); k++)
                       {
                        if(StringCompare(todayEvents[k].name, event.name) == 0)
                          {
                           eventExists = true;
                           break;
                          }
                       }

                     if(!eventExists)
                       {
                        ArrayResize(todayEvents, count + 1);
                        todayEvents[count++] = new KhanhdeuxEvent(country_codes[j], event.name, values[i].time, event_importance, false);
                       }
                    }
                 }
              }
           }
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ENUM_NEWS_TYPE getDailyNewsType(string eventName)
  {
   if(StringCompare(eventName, "Core CPI m/m") == 0)
     {
      return CPI;
     }

   if(StringCompare(eventName, "Fed Chair Powell Speech") == 0)
     {
      return SPEECH;
     }

   if(StringCompare(eventName, "Nonfarm Payrolls") == 0
// || StringCompare(eventName, "ADP Nonfarm Employment Change") == 0
     )
     {
      return NONFARM;
     }

   if(StringCompare(eventName, "GDP q/q") == 0)
     {
      return GDP;
     }

   if(StringCompare(eventName, "S&P Global Services PMI") == 0
      || StringCompare(eventName, "ISM Non-Manufacturing PMI") == 0
      || StringCompare(eventName, "ISM Manufacturing PMI") == 0)
     {
      return PMI;
     }

   if(StringCompare(eventName, "Core Durable Goods Orders m/m") == 0)
     {
      return GOODS;
     }

   if(StringCompare(eventName, "Existing Home Sales") == 0
      || StringCompare(eventName, "Pending Home Sales m/m") == 0
     )
     {
      return HOME_SALES;
     }

   if(StringCompare(eventName, "JOLTS Job Openings") == 0)
     {
      return JOLTS;
     }

//if(StringCompare(eventName, "Building Permits") == 0)
//  {
//   return BUILDING_PERMIT;
//  }

   if(StringCompare(eventName, "Retail Sales m/m") == 0)
     {
      return RETAIL_SALES;
     }

   if(StringCompare(eventName, "PPI m/m") == 0)
     {
      return PPI;
     }

   if(StringCompare(eventName, "Fed Interest Rate Decision") == 0
      || StringCompare(eventName, "Fed Chair Powell Testimony") == 0
      || StringCompare(eventName, "Fed Chair Powell Speech") == 0
      || StringCompare(eventName, "FOMC Press Conference") == 0)
     {
      return FED;
     }

   if(StringCompare(eventName, "CB Consumer Confidence Index") == 0)
     {
      return CCI;
     }

   if(StringCompare(eventName, "Michigan Consumer Sentiment") == 0)
     {
      return MCS;
     }

//if(StringCompare(eventName, "Initial Jobless Claims") == 0)
//  {
//   return JOBLESS_CLAIM;
//  }

   if(StringCompare(eventName, "Core PCE Price Index m/m") == 0)
     {
      return PCE;
     }

   if(StringCompare(eventName, "Employment Cost Index q/q") == 0)
     {
      return OTHERS;
     }

   if(StringCompare(eventName, "CUSTOM") == 0)
     {
      return CUSTOM;
     }

   return NULL;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void showTodayEvents()
  {
   for(int i=0; i<ArraySize(todayEvents); i++)
     {
      Print("===",todayEvents[i].symbol, "-", todayEvents[i].name,"(", todayEvents[i].importanceLevel, "|", todayEvents[i].time, ")-closed=", todayEvents[i].isClosed);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void showLiveNews()
  {
   MqlDateTime STime;
   datetime time_current=TimeCurrent(STime);
   string dayOfWeek = EnumToString((ENUM_DAY_OF_WEEK)STime.day_of_week);

   datetime currentTime = TimeCurrent();

   for(int i=0; i<ArraySize(todayEvents); i++)
     {
      if(// getDailyNewsType(todayEvents[i].name) &&
         currentTime + 60 >= todayEvents[i].time
         && currentTime <= todayEvents[i].time)
        {
         Print("===",todayEvents[i].symbol, "-", todayEvents[i].name,"(", todayEvents[i].importanceLevel, "|", todayEvents[i].time, ") - closed=", todayEvents[i].isClosed);
        }
     }
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime getCurrentNewsTime()
  {
   datetime currentTime = isSummerTime() ? TimeCurrent() - 1 * 60 * 60 : TimeCurrent();
   MqlDateTime STime;
   datetime time_current=TimeCurrent(STime);
   return (currentTime - STime.sec);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void closeAllWhenSessionsExpired(string symbol)
  {
   ENUM_DCA_ORDER_TYPE types[] = {DCA_BUY, DCA_SELL};
   datetime currentTime = TimeCurrent();

   for(int i=0; i<ArraySize(types); i++)
     {
      datetime tradingTime = getFirstDCATime(symbol, types[i]);
      if(tradingTime < getTradingSession(LONDON, true) && currentTime > getTradingSession(NEWYORK, true))
        {
         closeAllDCAPositions(symbol, types[i]);
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void showSessions()
  {
   datetime currentTime = getCurrentNewsTime();
   bool sessionStart = false;
   ENUM_SESSION_TYPE types[] = {SYDNEY, TOKYO, LONDON, NEWYORK};

   for(int i=0; i<ArraySize(types); i++)
     {
      if(currentTime == getTradingSession(types[i], true))
        {
         Print("===SESSION:", EnumToString(types[i]));
        }

      if(currentTime == getTradingSession(types[i], false))
        {
         Print("===END-SESSION:", EnumToString(types[i]));
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkStopDaily()
  {
   datetime currentTime = TimeCurrent() + Server_Offset * 60 * 60;
   datetime nextDay = StringToTime(TimeToString(currentTime, TIME_DATE)) + 24 * 60 * 60;
   int timeRange = 20 * 60;

   if((nextDay - timeRange) < currentTime && currentTime < nextDay + timeRange)
     {
      return true;
     }

   return false;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              executeTelegramBot()
  {
   if(ChatID != "" && BotToken != "")
     {
      //--- show error message end exit
      if(getmeResult!=0)
        {
         Comment("Error: ",GetErrorDescription(getmeResult));
         return;
        }

      //--- show bot name
      Comment(bot.Name(), ":", DCA_Strategy_MagicNumber);
      //--- reading messages
      bot.GetUpdates();
      //--- processing messages
      bot.ProcessMessages();
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getTotalProfit(string symbol = "")
  {
   double profit = 0;
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      ulong ticket = PositionGetTicket(i);
      string positionSymbol=PositionGetSymbol(i);

      if(ticket > 0 && (positionSymbol == symbol || symbol == ""))
        {
         profit += PositionGetDouble(POSITION_PROFIT);
        }
     }

   return profit;
  }

//+------------------------------------------------------------------+
//| Close all positions                                              |
//+------------------------------------------------------------------+
void              closeAllPositions(string symbol = "")
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && (PositionGetSymbol(i) == symbol || symbol == "")
        )
        {
         trade.PositionClose(position.Ticket());
         Print(StringFormat("Close Position with lotsize=%g", PositionGetDouble(POSITION_VOLUME)));
        }
     }

   closeAllPendingOrders(symbol);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              closeLastPosition(string symbol)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
        )
        {
         trade.PositionClose(position.Ticket());
         return;
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              closeAllPositionsAtPrice(string symbol, double aPrice, int cType)
  {
   bool isClosed = false;

   MqlRates rate[];
   CopyRates(symbol,(ENUM_TIMEFRAMES)Timeframe,0,2,rate);

   double currentPrice = rate[1].close;
   double previousPrice = rate[0].close;

   if(previousPrice <= aPrice && aPrice <= currentPrice)
     {
      isClosed = true;
     }

   if(currentPrice <= aPrice && aPrice <= previousPrice)
     {
      isClosed = true;
     }

   if(isClosed)
     {
      closeAllPositions();
      if(cType == CLOSE_KILL)
        {
         algoTradingToggle(false);
         bot.isActive = false;
        }
     }
  }

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              showDCAMatrix()
  {
   double initLotSize = 0.01 * DCA_LotSize_Multiply;

   for(double k=initLotSize; k<=initLotSize * 10; k=k+initLotSize)
     {
      Print("Lot Size:" + DoubleToString(k, 2));

      for(int i = 1; i <= DCA_MaxNumOfDistance; i++)
        {
         // Calculate TotalVolume
         double totalLotSize = calculateTotalVolume(i, k, DCA_Strategy_Volume_Type);

         // Calculate MaxLoss
         double maxLoss = calculateMaxLoss(getSymbol(), i, k, DCA_Strategy_Distance, DCA_Strategy_Volume_Type);

         // Calculate MaxProfit
         double maxProfit = calculateMaxProfit(getSymbol(), i, k, DCA_Strategy_Distance, DCA_Strategy_Volume_Type);

         if(maxLoss >= getStoploss())
           {
            break;
           }

         if(i < DCA_MinNumOfDistance)
           {
            continue;
           }

         Print(StringFormat("L%d___Total Volume:[%g], Max Loss:[-%g($)], Max Profit:[%g($)]", i, totalLotSize, maxLoss, maxProfit));
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//double            getBestLotSize(string symbol, double distance, double level, double initCapital)
//  {
//   double initLotSize = 0.01; //  * DCA_LotSize_Multiply;
//   double bestLotSize = 0;
//
//   for(double k=initLotSize; k<=initLotSize * 100; k=k+initLotSize)
//     {
//      for(int i = 1; i <= level; i++)
//        {
//
//         // Calculate MaxLoss
//         double maxLoss = calculateMaxLoss(symbol, i, k, distance);
//
//         // Calculate MaxProfit
//         double maxProfit = calculateMaxProfit(symbol, i, k, distance);
//
//         if(maxLoss >= initCapital)
//           {
//            break;
//           }
//
//         if(i < level)
//           {
//            continue;
//           }
//
//         bestLotSize = k;
//        }
//     }
//
//   return bestLotSize ? NormalizeDouble(bestLotSize, 2) : DCA_Strategy_LotSize;
//  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getRsiOversold()
  {
   return RSI_Oversold;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getRsiOverbought()
  {
   return RSI_Overbought;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
KhanhdeuxSetup*            getBestSetupByTime(string symbol)
  {
   double distance = DCA_Strategy_Distance;
   int maxDistance = DCA_Strategy_MaxDistance;
   ENUM_VOLUME_TYPE volumeType = DCA_Strategy_Volume_Type;

   if(DCA_Strategy_Backtest == false && getDCAStrategyType(symbol) != NONE)
     {
      KhanhdeuxConfiguration* config = getConfiguration(symbol);
      distance = config.distance;
      maxDistance = config.maxDistance;
      volumeType = config.volumeType;
     }

   if(DCA_Strategy_InitLotSize > 0)
     {
      return new KhanhdeuxSetup(symbol, DCA_Strategy_InitLotSize, distance, maxDistance, volumeType);
     }

   double lotSize = getBestLotSize(symbol, distance, maxDistance, volumeType);

// Print("symbol=", symbol, "distance=", distance, ", maxDistance=", maxDistance, " point=", SymbolInfoDouble(symbol, SYMBOL_POINT));

   return new KhanhdeuxSetup(symbol, lotSize, distance, maxDistance, volumeType);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getBestLotSize(string symbol, double distance, int maxDistance, ENUM_VOLUME_TYPE volumeType)
  {
// Get best lotsize
   double contractSize = SymbolInfoDouble(symbol, SYMBOL_TRADE_CONTRACT_SIZE);

   double initLotSize = 0.01;
   double lotSizeMax = 5;
   double step = 0.01;

   if(contractSize == 10)
     {
      initLotSize = 0.1;
      step = 0.1;
     }

   if(contractSize == 1)
     {
      initLotSize = 1;
      step = 1;
      lotSizeMax = 20;
     }

   double lotsize = initLotSize;
   double maxLoss = getStoploss();//+ getDayProfit();
   double maxProfit = getDailyProfit() - getDayProfit();

   double closest = 9999;

   for(double k=initLotSize; k<=lotSizeMax; k=k+step)
     {
      // Calculate MaxLoss
      double loss = calculateMaxLoss(symbol, maxDistance, k, distance, volumeType);

      //if(symbol == "USDJPY")
      //  {
      //   loss = loss / 100;
      //  }

      // Calculate MaxProfit
      // double profit = calculateMaxProfit(maxDistance, k, distance);

      // Calculate distance
      // double dist = MathSqrt((maxLoss - loss) * (maxLoss - loss) + (maxProfit - profit) * (maxProfit - profit));
      double dist = MathAbs(maxLoss - loss);

      if(dist < closest && loss < maxLoss)
        {
         lotsize = NormalizeDouble(k, 2);
         closest = dist;
        }
     }

   return lotsize;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            calculateTotalVolume(int i, double lotSize, ENUM_VOLUME_TYPE volumeType)
  {
   double totalLotSize = 0;

   for(int j=1; j<=i; j++)
     {
      totalLotSize += calculateDCALotSize(j, lotSize, volumeType);
     }

   return totalLotSize;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            calculateMaxLoss(string symbol, int i, double lotSize, double distance, ENUM_VOLUME_TYPE volumeType)
  {
   double totalLoss = 0;
   double pips = distance * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10;
   double lastDistance = getLastDistance(symbol) > 0 ? getLastDistance(symbol) : distance;

   for(int j=1; j<=i; j++)
     {
      double dist = (j == i) ? lastDistance * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10 : (pips * (i - j) + lastDistance * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10);
      // totalLoss += calculateDCALotSize(j, lotSize) * dist * SymbolInfoDouble(symbol, SYMBOL_TRADE_CONTRACT_SIZE);
      totalLoss += calculateDCALotSize(j, lotSize, volumeType) * dist * SymbolInfoDouble(symbol, SYMBOL_TRADE_TICK_VALUE) / SymbolInfoDouble(symbol, SYMBOL_TRADE_TICK_SIZE);
     }

   return totalLoss;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double           getDefaultStoploss(string symbol)
  {
   double lastDistance = getLastDistance(symbol) > 0 ? getLastDistance(symbol) : getDCADistance(symbol);
   return getDCADistance(symbol) * getDCAMaxDistance(symbol) + lastDistance;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            calculateMaxProfit(string symbol, int i, double lotSize, double distance, ENUM_VOLUME_TYPE volumeType)
  {
   double totalProfit = 0;
   double pips = distance * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10;

   for(int j=1; j<=i; j++)
     {
      totalProfit += pips * calculateDCALotSize(j, lotSize, volumeType) * SymbolInfoDouble(symbol, SYMBOL_TRADE_CONTRACT_SIZE);
     }

   return totalProfit;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getRequestSignal(string symbol)
  {
   ENUM_STRATEGY_TYPE strategyType = getDCAStrategyType(symbol);

   if(strategyType == DCA)
      return SIGNAL_DCA;
   if(strategyType == HEDG)
      return SIGNAL_HEDG;
   if(strategyType == DCA_2)
      return SIGNAL_DCA_2;
   if(strategyType == DCA_3)
      return SIGNAL_DCA_3;
   if(strategyType == NONE)
      return SIGNAL_NONE;

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void executeEvents()
  {
   datetime currentDate = StringToTime(TimeToString(TimeCurrent(), TIME_DATE));

   if(todayDate != currentDate)
     {
      ArrayResize(todayEvents, 0);
      setTodayEvents(todayEvents);
      todayDate = currentDate;
     }
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              executeDCATrading(string symbol)
  {
   int requestSignal = getRequestSignal(symbol);

   datetime currentDate = StringToTime(TimeToString(TimeCurrent(), TIME_DATE));

   if(TradingStoplossDaily_Active && checkDailyStoploss())
     {
      if(dailyDate != currentDate)
        {
         Print(StringFormat("Stoploss exeeded = %G", getDailyStoploss()));
         dailyDate = currentDate;
        }

      closeAllPositions();
      return;
     }

   if(checkPauseAction(symbol))
     {
      return;
     }

   if(TradingProfitDaily_Active && checkDailyProfit())
     {
      closeAllPositions();

      if(dailyDate != currentDate)
        {
         Print(StringFormat("Takeprofit exeeded = %G", getDailyProfit()));
         dailyDate = currentDate;
        }

      return;
     }

   if(TradingStopDaily_Active && checkStopDaily())
     {
      closeAllPositions();
      return;
     }

   if(TradingStop_Active && checkTradingStopHour())
     {
      if(TradingStop_CloseAllPositions)
        {
         closeAllPositions();
        }
      return;
     }

   if(TradingStopProfit_Active && checkStopProfit())
     {
      closeAllPositions();
      return;
     }

//if(TradingLimitProfit_Active && checkLimitProfit())
//  {
//   closeAllPositions();
//   return;
//  }

   if(TradingGo_Active && !checkTradingGoHour())
     {
      if(TradingGo_CloseAllPositions)
        {
         closeAllPositions();
        }
      return;
     }

   if(TradingStopNews_Active && checkNews(false, symbol))
     {
      if(TradingStopNews_CloseAllPositions || bot.isCloseOnNews || checkNews(true, symbol))
        {
         closeAllPositions(symbol);
        }
      else
        {
         closeAllStopLossWhenStopNews(symbol);
        }  
      return;
     }

   if(Session_Active && !checkSession())
     {
      if(Session_CloseAllPositions)
        {
         closeAllPositions();
        }
      return;
     }

   if(!checkDayOfWeek())
     {
      return;
     }

   if(!checkTradingSession())
     {
      return;
     }

   if(getDCADistance(symbol) == 0 || getDCAMaxDistance(symbol) == 0)
     {
      return;
     }

   if(!bot.isActive)
     {
      return;
     }

   if(requestSignal == SIGNAL_DCA_2)
     {
      executeDCA2Trading(symbol);
      return;
     }

   if(requestSignal == SIGNAL_DCA_3)
     {
      executeDCA3Trading(symbol);
      return;
     }

   if(getTotalDCAOrders(symbol) > 0)
     {
      return;
     }

   ENUM_DCA_ORDER_TYPE orderTypes[];
   setOrderTypesByRequestSignal(symbol, orderTypes, requestSignal);

   for(int i=0; i < ArraySize(orderTypes); i++)
     {
      ENUM_DCA_ORDER_TYPE orderType = orderTypes[i];
      int totalPositions = getTotalDCAPositions(symbol, orderType);

      // Position holding case
      if(POSITION_HOLDING_Factor > 0
         && totalPositions > 0
         && getLastDCATime(symbol, orderType) + POSITION_HOLDING_Factor * getDCADistance(symbol) * 60 * 60 <= TimeCurrent())
        {
         if(POSITION_HOLDING_Stoploss)
           {
            double price = getLastDCAPrice(symbol, orderType);
            double dist = getDCADistance(symbol) * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10;
            double sl = orderType == DCA_BUY ? price - dist : price + dist;

            bool isStoplossUpdated = isAllDCAPositionStopLossUpdated(symbol, sl, orderType);
            bool isStoplossValid = isStoplossValid(symbol, sl, orderType);

            if(!isStoplossUpdated && isStoplossValid && orderType == DCA_BUY)
              {
               string message = StringFormat("(HOLD) UPDATE BUY!!! SL = %G", sl);
               printf(message);
               SendMessage(message, ChatID, BotToken);

               updateAllDCAPositionStopLoss(symbol, sl, orderType);
              }

            if(!isStoplossUpdated && isStoplossValid && orderType == DCA_SELL)
              {
               string message = StringFormat("(HOLD) UPDATE SELL!!! SL = %G", sl);
               printf(message);
               SendMessage(message, ChatID, BotToken);

               updateAllDCAPositionStopLoss(symbol, sl, orderType);
              }
           }
         else
           {
            closeAllDCAPositions(symbol, orderType);
           }
        }

      // Update SL to last price when current price crosses last second price
      if(totalPositions > 2 && DCA_Strategy_DynamicStopLoss_Active)
        {
         double currentPrice = getCurrentPrice(symbol, orderType);
         double lastPrice = getLastNthDCAPrice(symbol, orderType, 1);
         double lastSecondPrice = getLastNthDCAPrice(symbol, orderType, 2);
         bool isStoplossUpdated = isAllDCAPositionStopLossUpdated(symbol, lastPrice, orderType);

         if(!isStoplossUpdated && orderType == DCA_BUY && currentPrice > lastSecondPrice)
           {
            string message = StringFormat("UPDATE BUY!!! SL = %G", lastPrice);
            printf(message);
            SendMessage(message, ChatID, BotToken);

            updateAllDCAPositionStopLoss(symbol, lastPrice, orderType);
           }

         if(!isStoplossUpdated && orderType == DCA_SELL && currentPrice < lastSecondPrice)
           {
            string message = StringFormat("UPDATE SELL!!! SL = %G", lastPrice);
            printf(message);
            SendMessage(message, ChatID, BotToken);

            updateAllDCAPositionStopLoss(symbol, lastPrice, orderType);
           }
        }

      if(totalPositions == 1 && DCA_Strategy_TrailingStopLoss_Active)
        {
         double lotSize =  getLastDCALotSize(symbol, orderType);
         double price = getLastDCAPrice(symbol, orderType);
         double sl = getLastDCAStoploss(symbol, orderType);

         //double tp = calculateDCATakeProfit(lotSize, price, orderType);
         double currentPrice = getCurrentPrice(symbol, orderType);
         double dist = getDCADistance(symbol) * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10;

         int step = dist * DCA_Strategy_TrailingStopLoss_Step;

         if(orderType == DCA_BUY && step > 0)
           {
            bool isSLUpdated = false;

            if(currentPrice >= (price + dist))
              {
               if(sl < price)
                 {
                  sl = price + step;
                  isSLUpdated = true;
                 }
               else
                  if(currentPrice - sl >= dist)
                    {
                     sl = sl + step;
                     isSLUpdated = true;
                    }
              }

            if(isSLUpdated)
              {
               string message = StringFormat("UPDATE BUY!!! SL = %G", sl);
               printf(message);
               SendMessage(message, ChatID, BotToken);

               updateAllDCAPositionStopLoss(symbol, sl, orderType);
              }
           }

         if(orderType == DCA_SELL && step > 0)
           {
            bool isSLUpdated = false;

            if(currentPrice <= (price - dist))
              {
               if(sl > price)
                 {
                  sl = price - step;
                  isSLUpdated = true;
                 }
               else
                  if(sl - currentPrice >= dist)
                    {
                     sl = sl - step;
                     isSLUpdated = true;
                    }
              }

            if(isSLUpdated)
              {
               string message = StringFormat("UPDATE SELL!!! SL = %G", sl);
               printf(message);
               SendMessage(message, ChatID, BotToken);

               updateAllDCAPositionStopLoss(symbol, sl, orderType);
              }
           }
        }

      if(totalPositions == 1 && DCA_DynamicTakeProfit)
        {
         double sl = getLastDCAStoploss(symbol, orderType);
         double tp = getLastDCATakeProfit(symbol, orderType);
         double price = getLastDCAPrice(symbol, orderType);

         double currentPrice = getCurrentPrice(symbol, orderType);
         double dist = getDCADistance(symbol) * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10;

         if((orderType == DCA_BUY && currentPrice >= price + dist)
            || (orderType == DCA_SELL && currentPrice <= price - dist))
           {
            // double breakevenSL = calculateDCABreakEvenStoploss(symbol, orderType);
            int tpSignal = getSMATakeProfitSignal(symbol);

            //if(sl != price)
            //  {
            //   updateAllDCAPositionStopLoss(symbol, price, orderType);
            //  }

            if(orderType == DCA_BUY && tpSignal == SIGNAL_BUY && tp != currentPrice)
              {
               // closeAllDCAPositions(symbol, DCA_BUY);
               updateAllDCAPositionTakeProfit(symbol, currentPrice, orderType);
               return;
              }

            if(orderType == DCA_SELL && tpSignal == SIGNAL_SELL && tp != currentPrice)
              {
               // closeAllDCAPositions(symbol, DCA_SELL);
               updateAllDCAPositionTakeProfit(symbol, currentPrice, orderType);
               return;
              }
           }
        }

      if(totalPositions >= 2 && DCA_RefreshLevel)
        {
         double currentPrice = getCurrentPrice(symbol, orderType);
         double lastSecondPrice = getLastNthDCAPrice(symbol, orderType, 2);

         if((orderType == DCA_BUY && currentPrice > lastSecondPrice) ||
            (orderType == DCA_SELL && currentPrice < lastSecondPrice)
           )
           {
            closeLastNthPosition(symbol, orderType, 1);
            double lotSize =  calculateDCALotSize(totalPositions - 1, getDCALotSize(symbol), getDCAVolumeType(symbol));
            double tp = calculateDCATakeProfit(symbol, lotSize, lastSecondPrice, orderType);
            updateAllDCAPositionTakeProfit(symbol, tp, orderType);
           }

        }

      //      if(totalPositions == getDCAMaxDistance(symbol) && getNextDCASignal(symbol, orderType))
      //        {
      //         if(!DCA_NoStoploss)
      //           {
      //            closeAllDCAPositions(symbol, orderType);
      //           }
      //
      //         return;
      //        }


      if(totalPositions == getDCAMaxDistance(symbol))
        {
         return;
        }

      int signal = 0;
      if(totalPositions == 0)
        {
         signal = orderType == DCA_BUY ? SIGNAL_BUY : SIGNAL_SELL;
        }
      else
        {
         signal = getNextDCASignal(symbol, orderType);
        }

      if(signal)
        {

         // Close all when the last position time is too short
         if(NextOrderMin > 0 && getLastDCATime(symbol, orderType) + NextOrderMin * 60 >= TimeCurrent())
           {
            Print("=== STOPOUT - Next Position in short time ===");
            closeAllDCAPositions(symbol, orderType);
            return;
           }

         int newTotalPositions = totalPositions + 1;

         double lotSize =  calculateDCALotSize(newTotalPositions, getDCALotSize(symbol), getDCAVolumeType(symbol));
         double price = getCurrentPrice(symbol, orderType);
         double tp = calculateDCATakeProfit(symbol, lotSize, price, orderType);
         double sl = calculateDCAStoploss(symbol, lotSize, price, orderType);

         if(newTotalPositions == 1 && DCA_Strategy_TrailingStopLoss_Active)
           {
            tp = 0;
           }

         if(newTotalPositions == 1 && DCA_DynamicTakeProfit)
           {
            tp = 0;
           }

         if(signal == SIGNAL_BUY && checkValidLotSize(symbol, lotSize, orderType))
           {
            updateAllDCAPositionTakeProfitAndStopLoss(symbol, orderType, tp, sl);
            string comment = BUY_STRING + ":" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions;

            Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, price, sl, tp, comment));
            trade.Buy(lotSize, symbol, price, sl, tp, comment);
           }

         if(signal == SIGNAL_SELL && checkValidLotSize(symbol, lotSize, orderType))
           {
            updateAllDCAPositionTakeProfitAndStopLoss(symbol, orderType, tp, sl);
            string comment = SELL_STRING + ":" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions;

            Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, price, sl, tp, comment));
            trade.Sell(lotSize, symbol, price, sl, tp, comment);
           }
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void executeDCA2Trading(string symbol)
  {
   int signal = 0;
   int totalPositions = getTotalDCAPositions(symbol, DCA_BUY) + getTotalDCAPositions(symbol, DCA_SELL);

   if(totalPositions == 0)
     {
      signal = getEntrySignal(symbol);
     }
   else
     {
      ENUM_DCA_ORDER_TYPE lastOrderType = getLastDCAOrderType(symbol);

      if(getNextDCASignal(symbol, lastOrderType))
        {
         if(lastOrderType == DCA_BUY)
           {
            signal = SIGNAL_SELL;
           }
         if(lastOrderType == DCA_SELL)
           {
            signal = SIGNAL_BUY;
           }
        }
     }

   if(signal)
     {
      if(totalPositions == DCA_2_StopLevel)
        {
         if(!DCA_NoStoploss)
           {
            closeAllPositions();
           }
         return;
        }

      int newTotalPositions = totalPositions + 1;
      double initLotSize = NormalizeDouble(getDCALotSize(symbol) / DCA_2_Fraction,SymbolInfoInteger(symbol, SYMBOL_DIGITS));
      double lotSize =  calculateDCALotSize(newTotalPositions, initLotSize, getDCAVolumeType(symbol));

      ENUM_DCA_ORDER_TYPE orderType = signal == SIGNAL_BUY ? DCA_BUY : DCA_SELL;
      double price = getCurrentPrice(symbol, orderType);
      double dist = getDCADistance(symbol) * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10 * DCA_2_Factor;

      double tp = 0;
      double sl = 0;

      if(!checkValidLotSize(symbol, lotSize, orderType))
        {
         if(!DCA_NoStoploss)
           {
            closeAllPositions();
           }
         return;
        }

      if(signal == SIGNAL_BUY)
        {
         tp = price + dist;
         sl = price - getDefaultStoploss(symbol) * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10;

         updateAllDCAPositionStopLoss(symbol, tp, DCA_SELL);
         string comment = BUY_STRING + ":" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions;

         Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, price, sl, tp, comment));
         trade.Buy(lotSize, symbol, price, sl, tp, comment);
        }

      if(signal == SIGNAL_SELL)
        {
         tp = price - dist;
         sl = price + getDefaultStoploss(symbol) * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10;

         updateAllDCAPositionStopLoss(symbol, tp, DCA_BUY);
         string comment = SELL_STRING + ":" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions;

         Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, price, sl, tp, comment));
         trade.Sell(lotSize, symbol, price, sl, tp, comment);
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void executeDCA3Trading(string symbol)
  {

   ENUM_DCA_ORDER_TYPE types[] = {DCA_BUY, DCA_SELL};

   for(int i=0; i<ArraySize(types); i++)
     {
      ENUM_DCA_ORDER_TYPE orderType = types[i];
      int totalPositions = getTotalDCAPositions(symbol, orderType);
      int totalHedgingPositions = getTotalHedgingPositions(symbol, orderType);

      ENUM_DCA_ORDER_TYPE rOrderType = orderType == DCA_BUY ? DCA_SELL : DCA_BUY;
      int totalOrders = getTotalDCAOrders(rOrderType);

      if(totalOrders > 0)
        {
         if(totalPositions == 0)
           {
            closeAllPendingOrders(symbol, rOrderType);
           }
         return;
        }

      if(totalPositions == 0 && totalHedgingPositions > 0)
        {
         closeAllHedgPositions(symbol, orderType);
         return;
        }

      if(totalPositions > 0 && totalHedgingPositions == 0)
        {
         double lastProfit = getLastDCATakeProfit(symbol, orderType);
         double currentBuyPrice = getCurrentPrice(symbol, DCA_BUY);
         double currentSellPrice = getCurrentPrice(symbol, DCA_SELL);

         bool shouldBeClosed = orderType == DCA_BUY ? MathMax(currentBuyPrice,currentSellPrice) >= lastProfit : MathMin(currentBuyPrice,currentSellPrice) <= lastProfit;

         if(shouldBeClosed)
           {
            closeAllDCAPositions(symbol, orderType);
            closeAllPendingOrders(symbol, rOrderType);
            return;
           }
        }
     }

   ENUM_DCA_ORDER_TYPE orderTypes[];
   setOrderTypesByRequestSignal(symbol, orderTypes, SIGNAL_DCA_3);

   for(int i=0; i < ArraySize(orderTypes); i++)
     {
      ENUM_DCA_ORDER_TYPE orderType = orderTypes[i];
      int totalPositions = getTotalDCAPositions(symbol, orderType);
      int totalHedgingPositions = getTotalHedgingPositions(symbol, orderType);

      ENUM_DCA_ORDER_TYPE rOrderType = orderType == DCA_BUY ? DCA_SELL : DCA_BUY;

      int signal = 0;
      if(totalPositions == 0)
        {
         signal = orderType == DCA_BUY ? SIGNAL_BUY : SIGNAL_SELL;
        }
      else
        {
         signal = getNextDCASignal(symbol, orderType);
        }

      if(signal)
        {
         // Close all when the last position time is too short
         if(NextOrderMin > 0 && getLastDCATime(symbol, orderType) + NextOrderMin * 60 >= TimeCurrent())
           {
            Print("=== STOPOUT - Next Position in short time ===");
            closeAllDCAPositions(symbol, orderType);
            return;
           }

         int newTotalPositions = totalPositions + 1;

         double lotSize =  calculateDCALotSize(newTotalPositions, DCA_3_InitLotsize, getDCAVolumeType(symbol));
         double price = getCurrentPrice(symbol, orderType);
         double tp = calculateDCATakeProfit(symbol, lotSize, price, orderType);
         double sl = calculateDCAStoploss(symbol, lotSize, price, orderType);

         if(signal == SIGNAL_BUY && checkValidLotSize(symbol, lotSize, orderType))
           {
            updateAllDCAPositionTakeProfitAndStopLoss(symbol, orderType, tp, sl);
            string comment = BUY_STRING + ":" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions;

            Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, price, sl, tp, comment));
            trade.Buy(lotSize, symbol, price, sl, tp, comment);
           }

         if(signal == SIGNAL_SELL && checkValidLotSize(symbol, lotSize, orderType))
           {
            updateAllDCAPositionTakeProfitAndStopLoss(symbol, orderType, tp, sl);
            string comment = SELL_STRING + ":" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions;

            Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, price, sl, tp, comment));
            trade.Sell(lotSize, symbol, price, sl, tp, comment);
           }

         ENUM_DCA_ORDER_TYPE rOrderType = (signal == SIGNAL_BUY) ? DCA_SELL : DCA_BUY;
         double rPrice = getCurrentPrice(symbol, rOrderType);
         double dist = getDCADistance(symbol) * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10;
         double rSl = tp;

         int hedgeOffset = DCA_3_DeadzoneFactor * getDCADistance(symbol) * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10;

         if(signal == SIGNAL_SELL && checkValidLotSize(symbol, lotSize, rOrderType))
           {
            closeAllHedgPositions(symbol);
            rPrice += hedgeOffset;

            double rTp = rPrice + dist;
            string comment = HEDG_STRING + "-" + SELL_STRING  + ":" + DCA_Strategy_MagicNumber +  "|R" + newTotalPositions;

            Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, rPrice, rSl, rTp, comment));
            // trade.Buy(lotSize, getSymbol(), rPrice, rSl, rTp, comment);

            if(hedgeOffset > 0)
              {
               trade.BuyStop(lotSize, rPrice, symbol, rSl, rTp, 0, 0, comment);
              }

            if(hedgeOffset < 0)
              {
               trade.BuyLimit(lotSize, rPrice, symbol, rSl, rTp, 0, 0, comment);
              }

            if(hedgeOffset == 0)
              {
               trade.Buy(lotSize, symbol, rPrice, rSl, rTp, comment);
              }
           }

         if(signal == SIGNAL_BUY && checkValidLotSize(symbol, lotSize, rOrderType))
           {
            closeAllHedgPositions(symbol);
            rPrice -= hedgeOffset;

            double rTp = rPrice - dist;
            string comment = HEDG_STRING + "-" + BUY_STRING  + ":" + DCA_Strategy_MagicNumber +  "|R" + newTotalPositions;

            Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, rPrice, rSl, rTp, comment));
            // trade.Sell(lotSize, getSymbol(), rPrice, rSl, rTp, comment);

            if(hedgeOffset > 0)
              {
               trade.SellStop(lotSize, rPrice, symbol, rSl, rTp, 0, 0, comment);
              }

            if(hedgeOffset < 0)
              {
               trade.SellLimit(lotSize, rPrice, symbol, rSl, rTp, 0, 0, comment);
              }

            if(hedgeOffset == 0)
              {
               trade.Sell(lotSize, symbol, rPrice, rSl, rTp, comment);
              }
           }
        }
     }

   ArrayResize(orderTypes, 0);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool checkValidLotSize(string symbol, double lotSize, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
// Retrieve account information
   double freeMargin = AccountInfoDouble(ACCOUNT_MARGIN_FREE);

   double price = getCurrentPrice(symbol, currentOrderType);
   ENUM_ORDER_TYPE orderType = currentOrderType == DCA_BUY ? ORDER_TYPE_BUY : ORDER_TYPE_SELL;

   double marginRequired;
   OrderCalcMargin(orderType, symbol, lotSize, price, marginRequired);

// Check if free margin is sufficient
   return freeMargin >= marginRequired;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getEntrySignal(string symbol)
  {
   int entrySignal = 0;
   ENUM_STRATEGY_ENTRY_TYPE entryType = getEntryType(symbol);

   if(TradingEntryTime_Active && !checkTradingEntryHour())
     {
      return entrySignal;
     }

   if(entryType == ENTRY_TYPE_RSI)
     {
      int rsiSignal = getSmoothedRsiSignal(symbol, RSI_NumOfCandles);

      if(checkTradingAreaSignal(symbol, rsiSignal))
        {
         entrySignal = rsiSignal;
        }
     }

   if(entryType == ENTRY_TYPE_FVG)
     {
      entrySignal = getFVGSignal(symbol);
     }

   if(entryType == ENTRY_TYPE_LAST_DAY_PRICE)
     {
      entrySignal = getLastDayPriceSignal(symbol);
     }

   if(entryType == ENTRY_TYPE_MOVING_AVERAGE)
     {
      entrySignal = getMovingAverageSignal(symbol);
     }

   if(entryType == ENTRY_TYPE_SMA)
     {
      entrySignal = getSMASignal(symbol);
     }

   if(entryType == ENTRY_TYPE_MARKET_PROFILE)
     {
      entrySignal = getMarketProfileSignal(symbol);
     }

   if(isSignalReversed(symbol) && entrySignal != 0)
     {
      entrySignal = (entrySignal == SIGNAL_BUY) ? SIGNAL_SELL : SIGNAL_BUY;
     }

   if(entrySignal != 0
      && bot.isMaster && ChatID != "" && BotToken != "")
     {
      datetime currentBarTime = iTime(symbol, (ENUM_TIMEFRAMES) Timeframe, 0);

      if(currentBarTime != lastBarTime)
        {
         string message = entrySignal == SIGNAL_BUY ? COMMAND_MASTER_BUY : COMMAND_MASTER_SELL;
         printf(message);
         SendMessage(message, ChatID, BotToken);

         lastBarTime = currentBarTime;
        }
     }

   if(bot.isSlave)
     {
      entrySignal = 0;
     }

   if(bot.signal != 0 && bot.signalSymbol == symbol)
     {
      int signal = bot.signal;
      bot.signal = 0;
      bot.signalSymbol = "";
      entrySignal = signal;
     }
   else
      if(bot.manual)
        {
         entrySignal = 0;
        }

   if((DCA_Signal_Filter == ONLY_BUY && entrySignal == SIGNAL_SELL) ||
      (DCA_Signal_Filter == ONLY_SELL && entrySignal == SIGNAL_BUY))
     {
      entrySignal = 0;
     }

   return entrySignal;
  }

// Function to remove seconds from a datetime value
datetime removeSeconds(datetime originalTime)
  {
   MqlDateTime timeStruct;

// Convert the datetime to MqlDateTime structure
   TimeToStruct(originalTime, timeStruct);

// Set seconds to zero
   timeStruct.sec = 0;

// Convert the modified MqlDateTime back to datetime
   return StructToTime(timeStruct);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              setOrderTypesByRequestSignal(string symbol, ENUM_DCA_ORDER_TYPE& orderTypes[], int requestSignal)
  {

   if(requestSignal == SIGNAL_HEDG || requestSignal == SIGNAL_DCA || requestSignal == SIGNAL_DCA_3)
     {
      int buyTotal = getTotalDCAPositions(symbol, DCA_BUY);
      int sellTotal = getTotalDCAPositions(symbol, DCA_SELL);

      int entrySignal = getEntrySignal(symbol);
      int count = 0;

      if(requestSignal == SIGNAL_DCA || requestSignal == SIGNAL_DCA_3)
        {
         if((entrySignal == SIGNAL_BUY && buyTotal == 0 && sellTotal == 0) || buyTotal > 0)
           {
            ArrayResize(orderTypes, count + 1);
            orderTypes[count++] = DCA_BUY;
           }
         else
            if((entrySignal == SIGNAL_SELL && sellTotal == 0 && buyTotal == 0) || sellTotal > 0)
              {
               ArrayResize(orderTypes, count + 1);
               orderTypes[count++] = DCA_SELL;
              }
        }

      if(requestSignal == SIGNAL_HEDG)
        {
         if(buyTotal == 0 && sellTotal == 0)
           {
            if(entrySignal == SIGNAL_BUY)
              {
               ArrayResize(orderTypes, count + 1);
               orderTypes[count++] = DCA_BUY;
              }

            if(entrySignal == SIGNAL_SELL)
              {
               ArrayResize(orderTypes, count + 1);
               orderTypes[count++] = DCA_SELL;
              }
           }
         else
            if(buyTotal > 0 && sellTotal > 0)
              {
               ArrayResize(orderTypes, count + 1);
               orderTypes[count++] = DCA_BUY;
               ArrayResize(orderTypes, count + 1);
               orderTypes[count++] = DCA_SELL;
              }
            else
               if(buyTotal > 0)
                 {
                  ArrayResize(orderTypes, count + 1);
                  orderTypes[count++] = DCA_BUY;

                  if(entrySignal == SIGNAL_SELL)
                    {
                     ArrayResize(orderTypes, count + 1);
                     orderTypes[count++] = DCA_SELL;
                    }
                 }
               else
                  if(sellTotal > 0)
                    {
                     ArrayResize(orderTypes, count + 1);
                     orderTypes[count++] = DCA_SELL;

                     if(entrySignal == SIGNAL_BUY)
                       {
                        ArrayResize(orderTypes, count + 1);
                        orderTypes[count++] = DCA_BUY;
                       }
                    }
        }
     }

   if(requestSignal == SIGNAL_BUY || requestSignal == SIGNAL_SELL || requestSignal == SIGNAL_NONE)
     {
      int count = 0;
      int buyTotal = getTotalDCAPositions(symbol, DCA_BUY);
      int sellTotal = getTotalDCAPositions(symbol, DCA_SELL);

      if(requestSignal == SIGNAL_BUY || buyTotal > 0)
        {
         ArrayResize(orderTypes, count + 1);
         orderTypes[count++] = DCA_BUY;
        }

      if(requestSignal == SIGNAL_SELL || sellTotal > 0)
        {
         ArrayResize(orderTypes, count + 1);
         orderTypes[count++] = DCA_SELL;
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getTotalDCAPositions(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   int count = 0;
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         count++;
        }
     }

   return count;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getTotalHedgingPositions(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType = NULL)
  {
   int count = 0;
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && isHedgPosition(position.Comment(), currentOrderType)
        )
        {
         count++;
        }
     }

   return count;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getTotalDCAOrders(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType = NULL)
  {
   int count = 0;
   for(int i = OrdersTotal() - 1; i >= 0; i--)
     {
      if(OrderSelect(OrderGetTicket(i)))
        {
         string symbol = OrderGetString(ORDER_SYMBOL);
         long magicNumber = OrderGetInteger(ORDER_MAGIC);
         ENUM_ORDER_TYPE orderType  = ENUM_ORDER_TYPE(OrderGetInteger(ORDER_TYPE));

         if(symbol == symbol && magicNumber == DCA_Strategy_MagicNumber)
           {
            if(currentOrderType)
              {
               if(currentOrderType == DCA_BUY && (orderType == ORDER_TYPE_BUY_LIMIT || orderType == ORDER_TYPE_BUY_STOP))
                 {
                  count++;
                 }

               if(currentOrderType == DCA_SELL && (orderType == ORDER_TYPE_SELL_LIMIT || orderType == ORDER_TYPE_SELL_STOP))
                 {
                  count++;
                 }
              }
            else
              {
               count++;
              }
           }
        }
     }

   return count;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void closeAllPendingOrders(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType = NULL)
  {
   for(int i = OrdersTotal() - 1; i >= 0; i--)
     {
      if(OrderSelect(OrderGetTicket(i)))
        {
         string symbol = OrderGetString(ORDER_SYMBOL);
         long magicNumber = OrderGetInteger(ORDER_MAGIC);
         ENUM_ORDER_TYPE orderType  = ENUM_ORDER_TYPE(OrderGetInteger(ORDER_TYPE));

         if(symbol == symbol && magicNumber == DCA_Strategy_MagicNumber)
           {
            if(currentOrderType == DCA_BUY && (orderType == ORDER_TYPE_BUY_LIMIT || orderType == ORDER_TYPE_BUY_STOP))
              {
               trade.OrderDelete(OrderGetTicket(i));
              }

            if(currentOrderType == DCA_SELL && (orderType == ORDER_TYPE_SELL_LIMIT || orderType == ORDER_TYPE_SELL_STOP))
              {
               trade.OrderDelete(OrderGetTicket(i));
              }

            if(currentOrderType == NULL)
              {
               trade.OrderDelete(OrderGetTicket(i));
              }
           }
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getRsiSignal(string symbol, int numOfCandles)
  {
   double rsi[];
   int rsiHandle = iRSI(symbol, (ENUM_TIMEFRAMES) Timeframe, RSI_Length, PRICE_CLOSE);
   int oversold = getRsiOversold();
   int overbought = getRsiOverbought();
   int signal = 0;

   CopyBuffer(rsiHandle, 0, 1, numOfCandles, rsi);

   int buyCount = 0;
   int sellCount = 0;

   for(int i=0; i< numOfCandles - 1; i++)
     {
      if(rsi[i] < oversold)
        {
         buyCount++;
        }

      if(rsi[i] > overbought)
        {
         sellCount++;
        }
     }

   if(buyCount == numOfCandles - 1 && rsi[numOfCandles - 1] >= oversold)
     {
      signal = SIGNAL_BUY;
     }

   if(sellCount == numOfCandles - 1 && rsi[numOfCandles - 1] <= overbought)
     {
      signal = SIGNAL_SELL;
     }

   ArrayFree(rsi);
   return signal;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getSmoothedRsiSignal(string symbol, int numOfCandles)
  {
   int signal = 0;

   double rsi[];
   int rsiHandle = iRSI(symbol, (ENUM_TIMEFRAMES)Timeframe, RSI_Length, PRICE_CLOSE);
   int oversold = getRsiOversold();
   int overbought = getRsiOverbought();

   int MA_Length = numOfCandles;

   CopyBuffer(rsiHandle, 0, 1, RSI_Length, rsi);

   int maHandle = iMA(symbol, (ENUM_TIMEFRAMES) Timeframe, MA_Length, 0, MODE_SMA, rsiHandle);
   double ma[];
   CopyBuffer(maHandle, 0, 1, MA_Length, ma);


   if(ma[MA_Length - 2] < oversold && ma[MA_Length - 1] >= oversold)
     {
      signal = SIGNAL_BUY;
     }

   if(ma[MA_Length - 2] > overbought && ma[MA_Length - 1] <= overbought)
     {
      signal = SIGNAL_SELL;
     }

   return signal;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
struct FvgResult
  {
   double            price;
   int               signal;
  };

FvgResult fvgResult;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
FvgResult getFVGResult(string symbol)
  {
   FvgResult result;
   result.price = 0;
   result.signal = 0;

   int signal = 0;
   int numOfCandles = FVG_NumOfCandles;

   MqlRates rate[];
   CopyRates(symbol,(ENUM_TIMEFRAMES) Timeframe_High, 1, numOfCandles, rate);

// Check all candles have the same color
   int greenCount = 0;
   int redCount = 0;

   for(int i= numOfCandles - 1; i>=0; i--)
     {
      if(rate[i].close >= rate[i].open)
        {
         greenCount++;
        }

      if(rate[i].close <= rate[i].open)
        {
         redCount++;
        }
     }

   bool buyCountCondition = (redCount == numOfCandles);
   bool sellCountCondition = (greenCount == numOfCandles);


// Check FVG
   bool buyFVGCondition = true;
   bool sellFVGCondition = true;

   for(int i= numOfCandles - 1; i>=2; i--)
     {
      int j = i - 2;
      if(rate[i].high > rate[j].low)
        {
         buyFVGCondition = false;
        }

      if(rate[i].low < rate[j].high)
        {
         sellFVGCondition = false;
        }
     }

   if(buyCountCondition && buyFVGCondition)
     {
      signal = SIGNAL_BUY;

      result.signal = signal;
      result.price = rate[numOfCandles - 1].high;
     }

   if(sellCountCondition && sellFVGCondition)
     {
      signal = SIGNAL_SELL;

      result.signal = signal;
      result.price = rate[numOfCandles - 1].low;
     }

   return result;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getFVGSignal(string symbol)
  {
   if(fvgResult.signal != 0)
     {
      int signal = 0;

      if(fvgResult.signal == SIGNAL_BUY && getCurrentPrice(symbol, DCA_BUY) >= fvgResult.price)
        {
         signal = fvgResult.signal;
        }

      if(fvgResult.signal == SIGNAL_SELL && getCurrentPrice(symbol, DCA_SELL) <= fvgResult.price)
        {
         signal = fvgResult.signal;
        }

      if(signal != 0)
        {
         fvgResult.signal = 0;
         fvgResult.price = 0;

         return signal;
        }
     }

   FvgResult newFvgResult = getFVGResult(symbol);
   if(newFvgResult.signal != 0)
     {
      fvgResult.signal = newFvgResult.signal;
      fvgResult.price = newFvgResult.price;
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void executeReset()
  {
   static int lastDayChecked = 0; // Store the last checked day

// Declare a structure to hold the date and time information
   MqlDateTime timeStruct;

// Get the current date and time
   TimeToStruct(TimeCurrent(), timeStruct);

// Get the current day of the month
   int currentDay = timeStruct.day;

// Check if a new day has started
   if(currentDay != lastDayChecked)
     {
      reset();
      lastDayChecked = currentDay; // Update the last checked day
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void reset()
  {
   initSetups();
  }



struct KhanhdeuxMarketProfile
  {
   double            lowest;
   double            val;
   double            poc;
   double            vah;
   double            highest;
  };

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
KhanhdeuxMarketProfile profile;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//void executeMarketProfile()
//  {
//   static int lastDayChecked = 0; // Store the last checked day
//
//// Declare a structure to hold the date and time information
//   MqlDateTime timeStruct;
//
//// Get the current date and time
//   TimeToStruct(TimeCurrent(), timeStruct);
//
//// Get the current day of the month
//   int currentDay = timeStruct.day;
//
//// Check if a new day has started
//   if(currentDay != lastDayChecked)
//     {
//      profile = calculateMarketProfileForDay(1);
//
//      Print("lowest=", DoubleToString(profile.lowest,2)," < val=", DoubleToString(profile.val,2), " < poc=", DoubleToString(profile.poc,2), " < vah=", DoubleToString(profile.vah, 2), " < highest=", DoubleToString(profile.highest,2));
//      lastDayChecked = currentDay; // Update the last checked day
//     }
//  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double ArraySum(const double &arr[])
  {
   double sum = 0;
   for(int i = 0; i < ArraySize(arr); i++)
     {
      sum += arr[i];
     }
   return sum;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void getPreviousDayRates(string symbol, int day_shift, MqlRates &rates[])
  {
   datetime currentDayStartTime = iTime(symbol, PERIOD_D1, 0);
   datetime startTime, endTime;
   int copied = 0;

   for(int shift = day_shift; shift <= 7; shift++)   // Limit search to within the last 7 days
     {
      startTime = currentDayStartTime - shift * 86400;        // Start of the target day
      endTime = currentDayStartTime - (shift - 1) * 86400 - 1;  // End of the target day (23:59:59)
      copied = CopyRates(symbol, PERIOD_M1, startTime, endTime, rates);

      if(copied > 0)
        {
         break;  // Successfully found a trading day with data
        }
     }

   if(copied <= 0)
     {
      Print("Error: Unable to fetch M1 data for the previous trading day within the last 7 days.");
     }
   else
     {
      Print(currentDayStartTime, "|", startTime, "|", endTime);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
KhanhdeuxMarketProfile calculateMarketProfileForDay(string symbol, int day_shift)
  {
   double poc_price, vah_price, val_price;
   KhanhdeuxMarketProfile profile;

   int profileLevels = MP_NumOfRows;

   MqlRates rates[];
   getPreviousDayRates(symbol, day_shift, rates);

   int ratesCount = ArraySize(rates);
   if(ratesCount == 0)
     {
      Print("No data available for the previous day.");
      return profile;
     }

// Determine price range (high to low)
   double priceHighest = rates[0].high;
   double priceLowest = rates[0].low;
   for(int i = 1; i < ratesCount; i++)
     {
      if(rates[i].high > priceHighest)
         priceHighest = rates[i].high;
      if(rates[i].low < priceLowest)
         priceLowest = rates[i].low;
     }

   double priceStep = (priceHighest - priceLowest) / profileLevels;
   double volumeStorageT[];
   ArrayResize(volumeStorageT, profileLevels);
   ArrayInitialize(volumeStorageT, 0);

   for(int barIndex = 0; barIndex < ratesCount; barIndex++)
     {
      double candleSize = rates[barIndex].high - rates[barIndex].low;
      for(int level = 0; level < profileLevels; level++)
        {
         double priceLevel = priceLowest + level * priceStep;
         if(rates[barIndex].high >= priceLevel && rates[barIndex].low < priceLevel + priceStep)
           {
            if(rates[barIndex].high <= priceLevel + priceStep && rates[barIndex].low >= priceLevel)
              {
               volumeStorageT[level] += rates[barIndex].tick_volume;
              }
            else
               if(rates[barIndex].high >= priceLevel + priceStep && rates[barIndex].low <= priceLevel)
                 {
                  volumeStorageT[level] += rates[barIndex].tick_volume * (priceStep / candleSize);
                 }
               else
                  if(rates[barIndex].high >= priceLevel + priceStep)
                    {
                     volumeStorageT[level] += rates[barIndex].tick_volume * ((priceLevel + priceStep - rates[barIndex].low) / candleSize);
                    }
                  else
                     if(rates[barIndex].low <= priceLevel)
                       {
                        volumeStorageT[level] += rates[barIndex].tick_volume * ((rates[barIndex].high - priceLevel) / candleSize);
                       }
           }
        }
     }

   int pocLevel = ArrayMaximum(volumeStorageT);
   double totalVolumeTraded = ArraySum(volumeStorageT);
   double valueArea = volumeStorageT[pocLevel];
   int levelAbovePoc = pocLevel, levelBelowPoc = pocLevel;

   while(valueArea / totalVolumeTraded < (MP_ValueArea/ 100))  // Assuming 70% value area
     {
      double upperVolume = (levelAbovePoc < profileLevels - 1) ? volumeStorageT[levelAbovePoc + 1] : 0;
      double lowerVolume = (levelBelowPoc > 0) ? volumeStorageT[levelBelowPoc - 1] : 0;

      if(upperVolume > lowerVolume)
        {
         valueArea += upperVolume;
         levelAbovePoc++;
        }
      else
        {
         valueArea += lowerVolume;
         levelBelowPoc--;
        }

      if(levelAbovePoc >= profileLevels - 1 && levelBelowPoc <= 0)
        {
         break;
        }
     }

   poc_price = priceLowest + pocLevel * priceStep;
   vah_price = priceLowest + levelAbovePoc * priceStep;
   val_price = priceLowest + levelBelowPoc * priceStep;

   profile.lowest = priceLowest;
   profile.val = val_price;
   profile.poc = poc_price;
   profile.vah = vah_price;
   profile.highest = priceHighest;

   return profile;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getMarketProfileSignal(string symbol)
  {
   ENUM_TIMEFRAMES timeframe = (ENUM_TIMEFRAMES) Timeframe;

   double closeCurrent = iClose(symbol, timeframe, 0);
   double closePrevious = iClose(symbol, timeframe, 1);

   if(MP_MarketType == MP_TREND)
     {
      if(closePrevious < profile.vah && profile.vah < closeCurrent)
        {
         return SIGNAL_BUY;
        }

      if(closePrevious > profile.val && profile.val > closeCurrent)
        {
         return SIGNAL_SELL;
        }
     }

   if(MP_MarketType == MP_SIDEWAY)
     {
      if(closePrevious > profile.vah && profile.vah > closeCurrent)
        {
         return SIGNAL_SELL;
        }

      if(closePrevious < profile.val && profile.val < closeCurrent)
        {
         return SIGNAL_BUY;
        }
     }

   if(MP_MarketType == MP_POC)
     {
      closeCurrent = iClose(symbol, (ENUM_TIMEFRAMES) Timeframe_High, 0);
      closePrevious = iClose(symbol, (ENUM_TIMEFRAMES) Timeframe_High, 1);

      if(closePrevious > profile.poc && profile.poc > closeCurrent)
        {
         return SIGNAL_SELL;
        }

      if(closePrevious < profile.poc && profile.poc < closeCurrent)
        {
         return SIGNAL_BUY;
        }
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getMovingAverageSignal(string symbol)
  {
   ENUM_TIMEFRAMES timeframe = (ENUM_TIMEFRAMES) MT_Timeframe;

   double close_current = iClose(symbol, timeframe, 0);
   double close_previous = iClose(symbol, timeframe, 1);

   double ma_values[1];
   CopyBuffer(iMA(symbol, timeframe, MT_MA_Period, 0, MODE_SMA, PRICE_CLOSE), 0, 0, 1, ma_values);
   double ma_current = ma_values[0];

   if(close_previous < ma_current && ma_current < close_current)
     {
      return SIGNAL_BUY;
     }

   if(close_previous > ma_current && ma_current > close_current)
     {
      return SIGNAL_SELL;
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getSMASignal(string symbol)
  {
   ENUM_TIMEFRAMES timeframe = (ENUM_TIMEFRAMES) Timeframe;

   int signal = 0;

   double smaFastValues[2];
   CopyBuffer(iMA(symbol, timeframe, SMA_FastPeriod, 0, MODE_SMA, PRICE_CLOSE), 0, 1, 2, smaFastValues);

   double smaFastPrevious = smaFastValues[0];
   double smaFast = smaFastValues[1];

   double smaSlowValues[2];
   CopyBuffer(iMA(symbol, timeframe, SMA_SlowPeriod, 0, MODE_SMA, PRICE_CLOSE), 0, 1, 2, smaSlowValues);

   double smaSlowPrevious = smaSlowValues[0];
   double smaSlow = smaSlowValues[1];

//double dist = MathAbs(smaFast - smaSlow);
//double threshold = 20 * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10;

//Print("previousFast=", smaFastPrevious  , "fast=", smaFast, "slow=", smaSlow , "dist=", dist, "thres=", threshold);

   if(smaFast <= 0 || smaSlow <= 0 || smaFastPrevious <= 0 || smaSlowPrevious <= 0)
     {
      return 0;
     }

   if(smaFast > smaSlow && smaFastPrevious < smaSlowPrevious && smaFast > smaFastPrevious && smaSlow > smaSlowPrevious)
     {
      signal = SIGNAL_BUY;
     }

   if(smaFast < smaSlow && smaFastPrevious > smaSlowPrevious && smaFast < smaFastPrevious && smaSlow < smaSlowPrevious)
     {
      signal = SIGNAL_SELL;
     }

   if(SMA_TrendPeriod > 0)
     {
      int trendSignal = 0;

      double smaTrendValues[1];
      CopyBuffer(iMA(symbol, timeframe, SMA_TrendPeriod, 0, MODE_SMA, PRICE_CLOSE), 0, 0, 1, smaTrendValues);
      double smaTrend = smaTrendValues[0];

      if(smaFast > smaTrend && smaSlow > smaTrend)
        {
         trendSignal = SIGNAL_BUY;
        }

      if(smaFast < smaTrend && smaSlow < smaTrend)
        {
         trendSignal = SIGNAL_SELL;
        }

      return signal == trendSignal ? signal : 0;
     }

   return signal;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getSMATakeProfitSignal(string symbol)
  {
   ENUM_TIMEFRAMES timeframe = (ENUM_TIMEFRAMES) Timeframe;

   int signal = 0;

   double smaFastValues[2];
   CopyBuffer(iMA(symbol, timeframe, SMA_FastPeriod, 0, MODE_SMA, PRICE_CLOSE), 0, 0, 2, smaFastValues);

   double smaFastPrevious = smaFastValues[0];
   double smaFast = smaFastValues[1];

   double smaSlowValues[2];
   CopyBuffer(iMA(symbol, timeframe, SMA_SlowPeriod, 0, MODE_SMA, PRICE_CLOSE), 0, 0, 2, smaSlowValues);

   double smaSlowPrevious = smaSlowValues[0];
   double smaSlow = smaSlowValues[1];

   if(smaFast <= 0 || smaSlow <= 0 || smaFastPrevious <= 0 || smaSlowPrevious <= 0)
     {
      return 0;
     }

   if(smaFast > smaSlow && smaFastPrevious < smaSlowPrevious && smaFast > smaFastPrevious)
     {
      signal = SIGNAL_SELL;
     }

   if(smaFast < smaSlow && smaFastPrevious > smaSlowPrevious && smaFast < smaFastPrevious)
     {
      signal = SIGNAL_BUY;
     }

   return signal;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               checkTradingAreaSignal(string symbol, int signal)
  {
   if(signal == 0)
     {
      return false;
     }

   return true;

   int overlapThreshold = 80; // percent

// Get last day highest/lowest prices

   MqlRates rateD[];
   CopyRates(symbol,PERIOD_D1,1,1,rateD);
//Print(rateD[0].time + ": O=" + rateD[0].open + " H=" + rateD[0].high + " L=" + rateD[0].low + " C=" + rateD[0].close);
   double lastHigh = rateD[0].high;
   double lastLow = rateD[0].low;

   double distanceArea = getDCADistance(symbol) * getDCAMaxDistance(symbol) * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10;
//Print("DistanceArea:", distanceArea, "SymbolInfoDouble(symbol, SYMBOL_POINT):", SymbolInfoDouble(symbol, SYMBOL_POINT), ",", getDCADistance(symbol), ", :" , getDCAMaxDistance());

// Calculate the area distance
   double sellUpper = lastHigh;
   double sellLower = lastHigh - distanceArea;

   double buyUpper = lastLow + distanceArea;
   double buyLower = lastLow;

// Check if upper area overlaps lower area
   double overlapPercent = sellLower >= buyUpper ? 0 : ((buyUpper - sellLower) / (sellUpper - buyLower) * 100);
//Print(overlapPercent);
   bool isOverlapValid = overlapPercent <= overlapThreshold;



// Check if price is in area
   double currentPrice = NormalizeDouble(SymbolInfoDouble(symbol, SYMBOL_LAST),SymbolInfoInteger(symbol, SYMBOL_DIGITS));
   bool isInBuy = buyLower <= currentPrice && currentPrice <= buyUpper;
   bool isInSell = sellLower <= currentPrice && currentPrice <= sellUpper;

   if(!isOverlapValid)
     {
      return false;
     }
   else
     {
      return true;
     }

   if(isInBuy && signal == SIGNAL_BUY)
     {
      return true;
     }

   if(isInSell && signal == SIGNAL_SELL)
     {
      return true;
     }

   return false;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getLastDayPriceSignal(string symbol)
  {
   int signal = 0;

   MqlRates rate[];
   CopyRates(symbol,PERIOD_D1,1,1,rate);
   double lastDayPrice = rate[0].close;

   if(getCurrentPrice(symbol, DCA_SELL) >= lastDayPrice + getDCADistance(symbol) * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10)
     {
      signal = SIGNAL_SELL;
     }

   if(getCurrentPrice(symbol, DCA_BUY) <= lastDayPrice - getDCADistance(symbol) * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10)
     {
      signal = SIGNAL_BUY;
     }

   return signal;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getRsi(string symbol, ENUM_TIMEFRAMES timeframe)
  {
   double rsi[];
   int rsiHandle = iRSI(symbol, timeframe, 14, PRICE_CLOSE);
   CopyBuffer(rsiHandle, 0, 0, 1, rsi);

   double rsiVal = rsi[0];

   ArrayFree(rsi);
   return rsiVal;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getNextDCASignal(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   int signal = 0;
   double lastPrice = getLastDCAPrice(symbol, currentOrderType);
   double pips = getDCADistance(symbol) * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10;
   double currentPrice = getCurrentPrice(symbol, currentOrderType);
   int totalPositions = getTotalDCAPositions(symbol, currentOrderType);
   int maxDistance = getDCAMaxDistance(symbol);
   int lastEnTryNumOfCandles = RSI_LastEntry_NumOfCandles;

   if(NextOrderWhenCandleClosed == true)
     {
      if(IsNewCandleClosed(symbol))
        {
         currentPrice = iClose(symbol, (ENUM_TIMEFRAMES) NextOrder_Timeframe, 1);
        }
      else
         return signal;
     }

   if(DCA_LastSignalEntry > 0
      && (totalPositions >= maxDistance - DCA_LastSignalEntry)
      && totalPositions != maxDistance
      && getEntrySignal(symbol) == 0)
     {
      return signal;
     }

   if(totalPositions == maxDistance)
     {
      pips = (getLastDistance(symbol) * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10);
     }
   else
     {
      pips = pips * DCA_Strategy_DeviationMultiplier;
     }

   if(currentOrderType == DCA_BUY)
     {
      if(currentPrice <= (lastPrice - pips))
        {
         signal = SIGNAL_BUY;
        }
     }

   if(currentOrderType == DCA_SELL)
     {
      if(currentPrice >= (lastPrice + pips))
        {
         signal = SIGNAL_SELL;
        }
     }

   return signal;
  }

datetime lastCandleTime = 0;  // Variable to track the last closed candle time

// Function to check if a new candle has closed
bool IsNewCandleClosed(string symbol)
  {
   datetime currentCandleTime = iTime(symbol, (ENUM_TIMEFRAMES) Timeframe, 1); // Get the close time of the last completed candle

   if(currentCandleTime != lastCandleTime)  // Check if the candle time is different from the last known time
     {
      lastCandleTime = currentCandleTime; // Update the last known candle time
      return true; // A new candle has closed
     }

   return false; // No new candle closed
  }

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            calculateDCATakeProfit(string symbol, double lotSize, double price, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   double priceVolumeSum = 0;
   double totalVolume = 0.0;
   double pips = getDCADistance(symbol) * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10;

   if(DCA_Strategy_AdditionalTP > 0)
     {
      return currentOrderType == DCA_BUY ? (price + pips + DCA_Strategy_AdditionalTP * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10) : (price - pips - DCA_Strategy_AdditionalTP * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10);
     }

   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
        )
        {
         priceVolumeSum += currentOrderType == DCA_BUY ? (position.PriceOpen() + pips) * position.Volume() : (position.PriceOpen() - pips) * position.Volume();
         totalVolume += position.Volume();
        }
     }

   priceVolumeSum += currentOrderType == DCA_BUY ? (price + pips) * lotSize : (price - pips) * lotSize;
   totalVolume += lotSize;

   return priceVolumeSum / totalVolume;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//double            calculateDCAStoploss(string symbol, double price, ENUM_DCA_ORDER_TYPE currentOrderType)
//  {
//   double sl = 0;
//   double slDistance = getDefaultStoploss(symbol) * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10;
//   int totalPositions = getTotalDCAPositions(symbol, currentOrderType);
//   int maxDistance = getDCAMaxDistance(symbol);
//
//   if(totalPositions + 1 == maxDistance)
//     {
//      slDistance = getLastDistance(symbol) * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10; // (DCA_Strategy_LastDistance/10);
//     }
//
//   if(currentOrderType == DCA_BUY)
//     {
//      sl = NormalizeDouble(price - slDistance,SymbolInfoInteger(symbol, SYMBOL_DIGITS));
//     }
//
//   if(currentOrderType == DCA_SELL)
//     {
//      sl = NormalizeDouble(price + slDistance,SymbolInfoInteger(symbol, SYMBOL_DIGITS));
//     }
//
//   return sl;
//  }

//+------------------------------------------------------------------+
//|              SELL: ((lotsize1 * price1 + lotsize2 * price2 + loss * SymbolInfoDouble(_Symbol, SYMBOL_TRADE_TICK_SIZE) / SymbolInfoDouble(_Symbol, SYMBOL_TRADE_TICK_VALUE) ) / (lotsize1 + lotsize2)                                                    |
//|              BUY:  ((lotsize1 * price1 + lotsize2 * price2 - loss * SymbolInfoDouble(_Symbol, SYMBOL_TRADE_TICK_SIZE) / SymbolInfoDouble(_Symbol, SYMBOL_TRADE_TICK_VALUE) ) / (lotsize1 + lotsize2)                                                    |
//+------------------------------------------------------------------+
double            calculateDCAStoploss(string symbol, double lotSize, double price, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   double priceVolumeSum = 0;
   double totalVolume = 0.0;
   double loss = getStoploss();
   double sl = 0;

   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
        )
        {
         priceVolumeSum += position.PriceOpen() * position.Volume();
         totalVolume += position.Volume();
        }
     }

   priceVolumeSum += lotSize * price;
   totalVolume += lotSize;

   double lossInTick = loss * SymbolInfoDouble(symbol, SYMBOL_TRADE_TICK_SIZE) / SymbolInfoDouble(symbol, SYMBOL_TRADE_TICK_VALUE);

   if(currentOrderType == DCA_BUY)
     {
      sl = NormalizeDouble((priceVolumeSum - lossInTick) / totalVolume,SymbolInfoInteger(symbol, SYMBOL_DIGITS));
     }

   if(currentOrderType == DCA_SELL)
     {
      sl = NormalizeDouble((priceVolumeSum + lossInTick) / totalVolume,SymbolInfoInteger(symbol, SYMBOL_DIGITS));
     }

   return sl;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            calculateDCABreakEvenStoploss(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   double priceVolumeSum = 0;
   double totalVolume = 0.0;
   double loss = 0;
   double sl = 0;

   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
        )
        {
         priceVolumeSum += position.PriceOpen() * position.Volume();
         totalVolume += position.Volume();
        }
     }

   double lossInTick = loss * SymbolInfoDouble(symbol, SYMBOL_TRADE_TICK_SIZE) / SymbolInfoDouble(symbol, SYMBOL_TRADE_TICK_VALUE);

   if(currentOrderType == DCA_BUY)
     {
      sl = NormalizeDouble((priceVolumeSum - lossInTick) / totalVolume,SymbolInfoInteger(symbol, SYMBOL_DIGITS));
     }

   if(currentOrderType == DCA_SELL)
     {
      sl = NormalizeDouble((priceVolumeSum + lossInTick) / totalVolume,SymbolInfoInteger(symbol, SYMBOL_DIGITS));
     }

   return sl;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            calculateDCALotSize(int i, double lotSize, ENUM_VOLUME_TYPE volumeType)
  {
   if(volumeType == VOL_FIB)
     {
      if(i <= 1)
        {
         return lotSize;
        }

      return calculateDCALotSize(i - 1, lotSize, volumeType) + calculateDCALotSize(i - 2, lotSize, volumeType);
     }

   if(volumeType == VOL_UNI)
     {
      return lotSize;
     }

   if(i == 1)
      return lotSize;
   return calculateDCALotSize(i - 1, lotSize, volumeType) * 2;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getCurrentPrice(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   double currentPrice = NormalizeDouble(SymbolInfoDouble(symbol, SYMBOL_LAST),SymbolInfoInteger(symbol, SYMBOL_DIGITS));

   if(currentOrderType == DCA_BUY)
     {
      currentPrice = NormalizeDouble(SymbolInfoDouble(symbol, SYMBOL_ASK),SymbolInfoInteger(symbol, SYMBOL_DIGITS));
     }
   else
      if(currentOrderType == DCA_SELL)
        {
         currentPrice = NormalizeDouble(SymbolInfoDouble(symbol, SYMBOL_BID),SymbolInfoInteger(symbol, SYMBOL_DIGITS));
        }

   return currentPrice;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ENUM_STRATEGY_TYPE getDCAStrategyType(string symbol)
  {
   if(DCA_Strategy_Backtest)
     {
      return DCA_Strategy_Type;
     }

   KhanhdeuxConfiguration* config = getConfiguration(symbol);
   return config.strategyType;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ENUM_STRATEGY_ENTRY_TYPE getEntryType(string symbol)
  {
   if(DCA_Strategy_Backtest)
     {
      return DCA_Strategy_Entry_Type;
     }

   KhanhdeuxConfiguration* config = getConfiguration(symbol);
   return config.entryType;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool isSignalReversed(string symbol)
  {
   if(DCA_Strategy_Backtest)
     {
      return DCA_Strategy_Signal_Reversed;
     }

   KhanhdeuxConfiguration* config = getConfiguration(symbol);
   return config.signalReversed;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getDCALotSize(string symbol)
  {
   if(bot.lotsize > 0)
     {
      return bot.lotsize;
     }

   KhanhdeuxSetup* setup = getSetup(symbol);
   return setup != NULL ? setup.lotsize : 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
KhanhdeuxSetup* getSetup(string symbol)
  {
   for(int i = 0; i < ArraySize(setups); i++)
     {
      KhanhdeuxSetup setup = setups[i];

      if(setup.symbol == symbol)
        {
         return setup.Clone();
        }
     }

   return NULL;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getDCAMaxDistance(string symbol)
  {
   if(bot.maxDistance > 0)
     {
      return bot.maxDistance;
     }

   KhanhdeuxSetup* setup = getSetup(symbol);
   return setup != NULL ? setup.maxDistance : 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getDCADistance(string symbol)
  {
   if(bot.distance > 0)
     {
      return bot.distance;
     }

   KhanhdeuxSetup* setup = getSetup(symbol);
   return setup != NULL ? setup.distance : 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ENUM_VOLUME_TYPE            getDCAVolumeType(string symbol)
  {
   if(bot.volumeType)
     {
      if(bot.volumeType == 1)
        {
         return VOL_MUL;
        }

      if(bot.volumeType == 2)
        {
         return VOL_FIB;
        }

      if(bot.volumeType == 3)
        {
         return VOL_UNI;
        }
     }

   KhanhdeuxSetup* setup = getSetup(symbol);
   return setup != NULL ? setup.volumeType : VOL_FIB;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//double            getDCAMaxDistanceByType(ENUM_DCA_ORDER_TYPE currentOrderType = NULL)
//  {
//   datetime tradingTime = getFirstDCATime(currentOrderType);
//   tradingTime = tradingTime ? tradingTime : TimeCurrent();
//   int maxDistance;
//
//   if(getTradingSession(LONDON, true) <= tradingTime && tradingTime < getTradingSession(LONDON, false))
//     {
//      maxDistance = getDCAMaxDistance();
//     }
//   else
//     {
//      maxDistance = getDCAMaxDistance();
//     }
//
//   return maxDistance;
//  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime getFirstDCATime(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = 0; i <= PositionsTotal() - 1; i++)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return (datetime) PositionGetInteger(POSITION_TIME);
        }
     }

   return NULL;
  }



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int               getPositionTypeByOrder(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   if(currentOrderType == DCA_BUY)
     {
      return POSITION_TYPE_BUY;
     }
   if(currentOrderType == DCA_SELL)
     {
      return POSITION_TYPE_SELL;
     }

   return NULL;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getLastDCAPrice(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return PositionGetDouble(POSITION_PRICE_OPEN);
        }
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ENUM_DCA_ORDER_TYPE getLastDCAOrderType(string symbol)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && !isHedgPosition(position.Comment())
        )
        {
         return PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(DCA_BUY) ? DCA_BUY : DCA_SELL;
        }
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getLastNthDCAPrice(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType, int number)
  {
   int count = 1;

   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         if(count == number)
           {
            return PositionGetDouble(POSITION_PRICE_OPEN);
           }
         count++;
        }
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void            closeLastNthPosition(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType, int number)
  {
   int count = 1;

   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         if(count == number)
           {
            trade.PositionClose(position.Ticket());
            Print(StringFormat("Close Position with lotsize=%g", PositionGetDouble(POSITION_VOLUME)));
            return;
           }
         count++;
        }
     }

   return;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getLastDCALotSize(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return PositionGetDouble(POSITION_VOLUME);
        }
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime getLastDCATime(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return (datetime) PositionGetInteger(POSITION_TIME);
        }
     }

   return NULL;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getLastDCATakeProfit(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return PositionGetDouble(POSITION_TP);
        }
     }

   return 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getLastDCAStoploss(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return PositionGetDouble(POSITION_SL);
        }
     }

   return 0;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              updateAllDCAPositionTakeProfit(string symbol, double tpPrice, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         double sl = position.StopLoss();
         double tp = tpPrice;
         
         if(tpPrice == position.TakeProfit())
           {
            continue;
           }

         trade.PositionModify(position.Ticket(),sl,tp);
         Print(StringFormat("Position with lotsize=%g updated TakeProfit= %g", PositionGetDouble(POSITION_VOLUME), tpPrice));
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              updateAllDCAPositionStopLoss(string symbol, double slPrice, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         double sl = slPrice;
         if(sl == position.StopLoss())
           {
            continue;
           }
         double tp = position.TakeProfit();

         trade.PositionModify(position.Ticket(),sl,tp);
         Print(StringFormat("Position with lotsize=%g updated Stoploss= %g", PositionGetDouble(POSITION_VOLUME), sl));
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              isAllDCAPositionStopLossUpdated(string symbol, double slPrice, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         return slPrice == position.StopLoss() ? true : false;
        }
     }

   return false;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              isStoplossValid(string symbol, double slPrice, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   double currentPrice = getCurrentPrice(symbol, currentOrderType);
   return (currentOrderType == DCA_BUY && slPrice < currentPrice) || (currentOrderType == DCA_SELL && slPrice > currentPrice);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              updateAllDCAPositionTakeProfitAndStopLoss(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType, double tpPrice, double slPrice)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         double sl = (slPrice > 0 && slPrice != position.StopLoss()) ? slPrice: position.StopLoss();
         double tp = tpPrice;

         trade.PositionModify(position.Ticket(),sl,tp);
         Print(StringFormat("Position with lotsize=%g updated SL= %g TakeProfit= %g", PositionGetDouble(POSITION_VOLUME), sl, tp));
        }
     }
  }
  
void closeAllStopLossWhenStopNews(string symbol)
  {
   ENUM_DCA_ORDER_TYPE types[] = {DCA_BUY, DCA_SELL};

   for(int i=0; i<ArraySize(types); i++)
     {
      ENUM_DCA_ORDER_TYPE orderType = types[i];
      
      int totalPosition = getTotalDCAPositions(symbol, orderType);
      if(totalPosition == 0)
        {
         continue;
        }
      
      double price = getLastDCAPrice(symbol, orderType);
      double currentPrice = getCurrentPrice(symbol, orderType);

      if(price == 0)
        {
         continue;
        }

      double dist = getDCADistance(symbol) * SymbolInfoDouble(symbol, SYMBOL_POINT) * 10;

      if((types[i] == DCA_BUY && currentPrice <= price - dist)
      || (types[i] == DCA_SELL && currentPrice >= price + dist)
      )
        {
         Print("=== Stoploss news ===");
         closeAllDCAPositions(symbol, orderType);
        }
     }
  }  

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
void              closeAllDCAPositions(string symbol = "", ENUM_DCA_ORDER_TYPE currentOrderType = NULL)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && (PositionGetSymbol(i) == symbol || symbol == "")
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
         && !isHedgPosition(position.Comment())
        )
        {
         trade.PositionClose(position.Ticket());
         Print(StringFormat("Close Position with lotsize=%g", PositionGetDouble(POSITION_VOLUME)));
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              closeAllHedgPositions(string symbol, ENUM_DCA_ORDER_TYPE currentOrderType = NULL)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && isHedgPosition(position.Comment(), currentOrderType)
        )
        {
         trade.PositionClose(position.Ticket());
         Print(StringFormat("Close Hedg Position with lotsize=%g", PositionGetDouble(POSITION_VOLUME)));
        }
     }
  }

string HEDG_STRING = "HEDG";
string BUY_STRING = "BUY";
string SELL_STRING = "SELL";

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              isHedgPosition(string comment, ENUM_DCA_ORDER_TYPE currentOrderType = -1)
  {
   string findStr = HEDG_STRING;
   if(currentOrderType > -1)
     {
      findStr += "-" + (currentOrderType == DCA_BUY ? BUY_STRING : SELL_STRING);
     }
   return StringFind(comment, findStr) > -1 ? true : false;
  }

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
bool              checkTradingStopHour()
  {
   MqlDateTime time1;
   TimeToStruct(TimeCurrent(),time1);
   int hour=time1.hour;

   if(TradingStop_HourStart == TradingStop_HourStop)
     {
      if(hour == TradingStop_HourStart)
        {
         return true;
        }
     }

   if(TradingStop_HourStart < TradingStop_HourStop)
     {
      if(TradingStop_HourStart <= hour && hour < TradingStop_HourStop)
        {
         return true;
        }
     }

   if(TradingStop_HourStart > TradingStop_HourStop)
     {
      if(TradingStop_HourStart <= hour || hour < TradingStop_HourStop)
        {
         return true;
        }
     }

   return false;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkTradingGoHour()
  {
   if(DCA_Strategy_Backtest)
     {
      return checkTradingTime(TradingGo_HourStart, TradingGo_HourStop);
     }

   MqlDateTime time1;
   TimeToStruct(TimeCurrent() + 5 * 60, time1);

   if(checkTradingTime(ASIA_HourStart, ASIA_HourStop))
     {
      if(!checkTradingTime(ASIA_HourStart, ASIA_HourStop, time1.hour))
        {
         return false;
        }
      return true;
     }

   if(checkTradingTime(UK_HourStart, UK_HourStop))
     {
      if(!checkTradingTime(UK_HourStart, UK_HourStop, time1.hour))
        {
         return false;
        }
      return true;
     }

   if(checkTradingTime(US_HourStart, US_HourStop))
     {
      if(!checkTradingTime(US_HourStart, US_HourStop, time1.hour))
        {
         return false;
        }
      return true;
     }

   return false;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkTradingTime(int start, int stop, int currentHour = NULL)
  {
   MqlDateTime time1;
   TimeToStruct(TimeCurrent(),time1);
   int hour = currentHour ? currentHour : time1.hour;

   if(start == stop)
     {
      if(hour == start)
        {
         return true;
        }
     }

   if(start < stop)
     {
      if(start <= hour && hour < stop)
        {
         return true;
        }
     }

   if(start > stop)
     {
      if(start <= hour || hour < stop)
        {
         return true;
        }
     }

   return false;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkTradingEntryHour()
  {
   MqlDateTime time1;
   TimeToStruct(TimeCurrent(),time1);
   int hour=time1.hour;

   if(TradingEntryTime_HourStart == TradingEntryTime_HourStop)
     {
      if(hour == TradingEntryTime_HourStart)
        {
         return true;
        }
     }

   if(TradingEntryTime_HourStart < TradingEntryTime_HourStop)
     {
      if(TradingEntryTime_HourStart <= hour && hour < TradingEntryTime_HourStop)
        {
         return true;
        }
     }

   if(TradingEntryTime_HourStart > TradingEntryTime_HourStop)
     {
      if(TradingEntryTime_HourStart <= hour || hour < TradingEntryTime_HourStop)
        {
         return true;
        }
     }

   return false;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkTradingSession()
  {
   datetime currentTime = TimeCurrent();
   int offset = 0 * 60 * 60;
// return true;

   ENUM_DAY_OF_WEEK dayOfWeek = getDayOfWeek();

   switch(dayOfWeek)
     {
      case MONDAY:
         return true;
         break;
      case TUESDAY:
         break;
      case WEDNESDAY:
         break;
      case THURSDAY:
         break;
      case FRIDAY:
         return true;
         break;
     }

   return true;
   return currentTime + offset >= getTradingSession(NEWYORK, true) && currentTime - offset <= getTradingSession(LONDON, false);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkDailyStoploss()
  {
   return getDayProfit() + getTotalProfit() < (getDailyStoploss() * -1);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getDailyStoploss()
  {
   double accountBalance = MathMin(AccountInfoDouble(ACCOUNT_BALANCE), ACCOUNT_BALANCE_INIT);
   return accountBalance * TradingStopDaily / 100;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getStoploss()
  {
   double accountBalance = MathMin(AccountInfoDouble(ACCOUNT_BALANCE), ACCOUNT_BALANCE_INIT);
   return accountBalance * TradingStopLoss / 100;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkDailyProfit()
  {
   return getDayProfit() + getTotalProfit() > getDailyProfit();
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkStopProfit()
  {
   double accountBalance = MathMin(AccountInfoDouble(ACCOUNT_BALANCE), ACCOUNT_BALANCE_INIT);
   return getTotalProfit() > accountBalance * TradingStopProfit / 100;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//bool              checkLimitProfit()
//  {
//   int buyTotal = getTotalDCAPositions(DCA_BUY);
//   int sellTotal = getTotalDCAPositions(DCA_SELL);
//   int maxTotal = MathMax(buyTotal, sellTotal);
//
//   return (getTotalProfit() > 0 && maxTotal >= TradingLimitProfit_Number);
//  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getDailyProfit()
  {
   double accountBalance = AccountInfoDouble(ACCOUNT_BALANCE);
   return accountBalance * TradingProfitDaily / 100;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkDayOfWeek()
  {
   if(Trading_DayOfWeek_Active == false)
     {
      return true;
     }

   MqlDateTime STime;
   datetime time_current=TimeCurrent(STime);
   return (ENUM_DAY_OF_WEEK) STime.day_of_week == Trading_DayOfWeek;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              checkSession()
  {
   MqlDateTime time1;
   TimeToStruct(TimeCurrent() + 5 * 60, time1);

   if(TradingSession_Active == true)
     {
      int hourStart;
      int hourStop;

      switch(TradingSession_Type)
        {
         case SESSION_ASIA:
            hourStart = ASIA_HourStart;
            hourStop  = ASIA_HourStop;
            break;
         case SESSION_UK:
            hourStart = UK_HourStart;
            hourStop  = UK_HourStop;
            break;
         case SESSION_US:
            hourStart = US_HourStart;
            hourStop  = US_HourStop;
            break;
        }

      if(checkTradingTime(hourStart, hourStop))
        {
         if(!checkTradingTime(hourStart, hourStop, time1.hour))
           {
            return false;
           }
         return true;
        }

      return false;
     }

   if(checkTradingTime(ASIA_HourStart, ASIA_HourStop))
     {
      if(!checkTradingTime(ASIA_HourStart, ASIA_HourStop, time1.hour))
        {
         return false;
        }
      return true;
     }

   if(checkTradingTime(UK_HourStart, UK_HourStop))
     {
      if(!checkTradingTime(UK_HourStart, UK_HourStop, time1.hour))
        {
         return false;
        }

      return true;
     }

   if(checkTradingTime(US_HourStart, US_HourStop))
     {
      if(!checkTradingTime(US_HourStart, US_HourStop, time1.hour))
        {
         return false;
        }

      return true;
     }

   return false;
  }

//+------------------------------------------------------------------+
//|
// SUNDAY
// MONDAY
// TUESDAY
// WEDNESDAY
// THURSDAY
// FRIDAY
// SATURDAY
//+------------------------------------------------------------------+
ENUM_DAY_OF_WEEK            getDayOfWeek()
  {
   MqlDateTime STime;
   datetime time_current=TimeCurrent(STime);
   return (ENUM_DAY_OF_WEEK)STime.day_of_week;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getLastDistance(string symbol)
  {
   return DCA_Strategy_LastDistance > 0 ? DCA_Strategy_LastDistance : getDCADistance(symbol);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageToRsi(string symbol)
  {
   int oversold = getRsiOversold();
   int overbought = getRsiOverbought();
   bool signal = false;

   string message = "RSI SIGNAL!!!" + "%0A";

   int rsiSignal = getRsiSignal(symbol, RSI_NumOfCandles);

   if(rsiSignal == SIGNAL_BUY)
     {
      message += "BUY: RSI crossover " + StringFormat("%d", oversold);
      signal = true;
     }

   if(rsiSignal == SIGNAL_SELL)
     {
      message += "SELL: RSI crossunder " + StringFormat("%d", overbought);
      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageCloseToRsi(string symbol)
  {
   bool signal = false;

   string message = "RSI SIGNAL!!!" + "%0A";

   int rsiSignal = getRsiSignal(symbol, 3);

   if(rsiSignal == SIGNAL_BUY)
     {
      message += "RSI oversold! IF L3 THEN CLOSE SELL!";
      signal = true;
     }

   if(rsiSignal == SIGNAL_SELL)
     {
      message += "RSI overbought! IF L3 THEN CLOSE BUY!";
      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageToCci(string symbol)
  {
   double cci[];
   int cciHanle = iCCI(symbol, (ENUM_TIMEFRAMES) Timeframe, 14, PRICE_TYPICAL);
   int upper = 100;
   int lower = -100;
   bool signal = false;

   CopyBuffer(cciHanle, 0, 0, 2, cci);

   string message = "SIGNAL!!!" + "%0A";

   if(cci[0] < lower && cci[1] >= lower)
     {
      message += "SELL: CCI crossover " + StringFormat("%d", lower);
      signal = true;
     }


   if(cci[0] > upper && cci[1] <= upper)
     {
      message += "BUY: CCI crossunder " + StringFormat("%d", upper);
      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }

   ArrayFree(cci);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageToSession()
  {
   string message = "";
   bool signal = false;

   datetime currentTime = getCurrentNewsTime();
   bool sessionStart = false;
   ENUM_SESSION_TYPE types[] = {SYDNEY, TOKYO, LONDON, NEWYORK};

   for(int i=0; i<ArraySize(types); i++)
     {
      if(currentTime == getTradingSession(types[i], true))
        {
         message += "%0A" + "===SESSION:" +  EnumToString(types[i]) + "===";
         signal = true;
        }

      if(currentTime == getTradingSession(types[i], false))
        {
         message += "%0A" + "===END-SESSION:" + EnumToString(types[i]) + "===";
         signal = true;
        }
     }

   MqlDateTime time1;
   TimeToStruct(TimeCurrent() - 60, time1);

   string configMessage = "";
   configMessage       += StringFormat("[TYPE]%s", EnumToString(getDCAStrategyType(getSymbol()))) + "";
   configMessage       += StringFormat("[ENTRY_TYPE]%s", EnumToString(getEntryType(getSymbol()))) + "";
   configMessage       += StringFormat("[REVERSED]%s", isSignalReversed(getSymbol()) ? "true" : "false") + "";
   configMessage       += StringFormat("[Lot]%G[Dist]%G",
                                       getDCALotSize(getSymbol()),
                                       getDCADistance(getSymbol()));

   if(!checkTradingTime(ASIA_HourStart, ASIA_HourStop, time1.hour)
      && checkTradingTime(ASIA_HourStart, ASIA_HourStop))
     {
      message += "%0A" + "===ASIA-SESSION-CONFIG===";
      message += "%0A" + configMessage;
      signal = true;
     }

   if(!checkTradingTime(UK_HourStart, UK_HourStop, time1.hour)
      && checkTradingTime(UK_HourStart, UK_HourStop))
     {
      message += "%0A" + "===UK-SESSION-CONFIG===";
      message += "%0A" + configMessage;
      signal = true;
     }

   if(!checkTradingTime(US_HourStart, US_HourStop, time1.hour)
      && checkTradingTime(US_HourStart, US_HourStop))
     {
      message += "%0A" + "===US-SESSION-CONFIG===";
      message += "%0A" + configMessage;
      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageToNews()
  {
   MqlDateTime STime;
   datetime time_current=TimeCurrent(STime);
   string dayOfWeek = EnumToString((ENUM_DAY_OF_WEEK)STime.day_of_week);

   int offset = TradingStopNews_FreezeInMinutes * 60;

   datetime currentTime = TimeCurrent();

   for(int i=0; i<ArraySize(todayEvents); i++)
     {
      if(// getDailyNewsType(todayEvents[i].name) &&
         currentTime + offset + 60 >= todayEvents[i].time
         && currentTime + offset <= todayEvents[i].time
         && todayEvents[i].isShowed == false
      )
        {
         todayEvents[i].isShowed = true;
         string message = "===" + todayEvents[i].symbol + "-" + todayEvents[i].name + "(" + dayOfWeek + "|" + todayEvents[i].importanceLevel +  "|" + todayEvents[i].time + ")-closed=" + todayEvents[i].isClosed + " in " + TradingStopNews_FreezeInMinutes + " minutes.";
         printf(message);
         SendMessage(message, ChatID, BotToken);
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageToAlert(string symbol)
  {
   bool isAlert = false;

   if(bot.alertPrice <= 0)
     {
      return;
     }

   MqlRates rate[];
   CopyRates(symbol,(ENUM_TIMEFRAMES) Timeframe,0,2,rate);

   double currentPrice = rate[1].close;
   double previousPrice = rate[0].close;
   double aPrice = bot.alertPrice;

   if(previousPrice <= aPrice && aPrice <= currentPrice)
     {
      isAlert = true;
     }

   if(currentPrice <= aPrice && aPrice <= previousPrice)
     {
      isAlert = true;
     }

   if(isAlert)
     {
      string message = "ALERT!Price crossed: " + bot.alertPrice;
      printf(message);
      SendMessage(message, ChatID, BotToken);
      bot.alertPrice = 0;
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendMessageToBalanceChange(string symbol)
  {
   double newBalance = AccountInfoDouble(ACCOUNT_BALANCE);

   if(currentBalance != newBalance)
     {
      if(currentBalance < newBalance)
        {
         string message        = "";
         message       += StringFormat("BALANCE=%G. TP=%+.2f$", newBalance, (newBalance - currentBalance)) + "";
         message       += StringFormat("%s %d", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN)) + "%0A" + getSymbolMessage();
         StringReplace(message, "+", "%2b");

         printf(message);
         if(ChatID != "" && BotToken != "")
           {
            SendMessage(message, ChatID, BotToken);
           }
        }
      currentBalance = newBalance;
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              SendPositionsChanged()
  {
   int total = PositionsTotal();

   if(numOfPositions != total)
     {
      string message = "NOTICE!" + "";
      message       += "[PO]" + StringFormat("%d", numOfPositions) + "-" + StringFormat("%d", total) + "";
      message       += getPositionMessage();
      message       += StringFormat("[BAL]%G",AccountInfoDouble(ACCOUNT_BALANCE)) + "";
      message       += StringFormat("[PRO]%G",AccountInfoDouble(ACCOUNT_PROFIT)) + "";
      message       += StringFormat("[EQU]%G",AccountInfoDouble(ACCOUNT_EQUITY)) + "";
      message       += "%0A" + StringFormat("%s %d", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN)) + "%0A" + getSymbolMessage();

      numOfPositions = PositionsTotal();

      printf(message);
      SendMessage(message, ChatID, BotToken);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string            getSymbolMessage()
  {
   string message = "";
   message += "1:" + AccountInfoInteger(ACCOUNT_LEVERAGE) + "|";

   for(int i = 0; i < ArraySize(symbols); i++)
     {
      string symbol = symbols[i];
      message += symbol + ":" + SymbolInfoDouble(symbol, SYMBOL_TRADE_CONTRACT_SIZE) + "|";
     }

   return message;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string            getDCAStrategyTypeMessage()
  {
   string message = "";

   for(int i = 0; i < ArraySize(symbols); i++)
     {
      string symbol = symbols[i];
      message += symbol + ":" + EnumToString(getDCAStrategyType(symbol)) + "|";
     }

   return message;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string            getDCAVolumeTypeMessage()
  {
   string message = "";

   for(int i = 0; i < ArraySize(symbols); i++)
     {
      string symbol = symbols[i];
      message += symbol + ":" + EnumToString(getDCAVolumeType(symbol)) + "|";
     }

   return message;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string            getEntryTypeMessage()
  {
   string message = "";

   for(int i = 0; i < ArraySize(symbols); i++)
     {
      string symbol = symbols[i];
      message += symbol + ":" + EnumToString(getEntryType(symbol)) + "|";
     }

   return message;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string            getIsSignalReversedMessage()
  {
   string message = "";

   for(int i = 0; i < ArraySize(symbols); i++)
     {
      string symbol = symbols[i];
      message += symbol + ":" + (isSignalReversed(symbol) ? "true" : "false") + "|";
     }

   return message;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string            getDCALotSizeMessage()
  {
   string message = "";

   for(int i = 0; i < ArraySize(symbols); i++)
     {
      string symbol = symbols[i];
      message += symbol + ":" + getDCALotSize(symbol) + "|";
     }

   return message;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string            getDCADistanceMessage()
  {
   string message = "";

   for(int i = 0; i < ArraySize(symbols); i++)
     {
      string symbol = symbols[i];
      message += symbol + ":" + getDCADistance(symbol) + "|";
     }

   return message;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string            getDCAMaxDistanceMessage()
  {
   string message = "";

   for(int i = 0; i < ArraySize(symbols); i++)
     {
      string symbol = symbols[i];
      message += symbol + ":" + getDCAMaxDistance(symbol) + "|";
     }

   return message;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string            getPositionMessage()
  {
   string message = "";

   for(int i = 0; i < ArraySize(symbols); i++)
     {
      string symbol = symbols[i];
      message += "%0A";
      message += "[" + symbol + "]";

      int buyCount = 0;
      int sellCount = 0;

      for(int i = PositionsTotal() - 1; i >= 0; i--)
        {
         if(PositionGetTicket(i)
            && PositionGetSymbol(i) == symbol
            && PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY
           )
           {
            buyCount++;
           }
        }

      for(int i = PositionsTotal() - 1; i >= 0; i--)
        {
         if(PositionGetTicket(i)
            && PositionGetSymbol(i) == symbol
            && PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL
           )
           {
            sellCount++;
           }
        }


      if(buyCount > 0)
        {
         message += StringFormat("BUY:L%d", buyCount);
        }

      if(sellCount > 0)
        {
         message += StringFormat("SELL:L%d", sellCount);
        }
     }

   return message;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string getPlanMessage()
  {
   string message = "";
   ENUM_DAY_OF_WEEK dayOfWeek[] = {MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY};

   MqlDateTime currentDate;
   TimeToStruct(TimeCurrent(), currentDate);

   message += "CURRENT MONTH:" + StringFormat("%d", currentDate.mon);

   for(int i=0; i<ArraySize(dayOfWeek); i++)
     {
      KhanhdeuxConfiguration* config = getConfiguration(dayOfWeek[i]);
      message += "\n" + EnumToString(dayOfWeek[i]);
      message += ": Dist:" + StringFormat("%G", config.distance);
      message += ", Strategy:" + EnumToString(config.strategyType);
      message += ", Entry:" + EnumToString(config.entryType);
     }

   return message;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string getNewsMessage()
  {
   string message = "";
   message += "===CURRENT-TIME:" + TimeCurrent() + "\n";

   for(int i=0; i<ArraySize(todayEvents); i++)
     {
      message += "===" + todayEvents[i].symbol + "-" + todayEvents[i].name + "(" + todayEvents[i].importanceLevel +  "|" + todayEvents[i].time + ")-closed=" + todayEvents[i].isClosed + "\n";
     }

   return message;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double            getDayProfit()
  {
   double dayprof = 0.0;
   datetime end = TimeCurrent();
   string sdate = TimeToString(TimeCurrent(), TIME_DATE);
   datetime start = StringToTime(sdate);

   HistorySelect(start,end);
   int TotalDeals = HistoryDealsTotal();

   for(int i = 0; i < TotalDeals; i++)
     {
      ulong Ticket = HistoryDealGetTicket(i);

      if(HistoryDealGetInteger(Ticket,DEAL_ENTRY) == DEAL_ENTRY_OUT)
        {
         double LatestProfit = HistoryDealGetDouble(Ticket, DEAL_PROFIT);
         dayprof += LatestProfit;
        }
     }

   return dayprof;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool isSummerTime()
  {
   datetime    tm=TimeCurrent();
   MqlDateTime stm;
   TimeToStruct(tm,stm);

   datetime dst_start,dst_end;
   dst_start=dst_end=0;

   DST_USA(stm.year,dst_start,dst_end);
   return dst_start <= tm && tm <= dst_end;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime getTradingSession(ENUM_SESSION_TYPE type, bool isStart = true)
  {
   int hourStart = 0;
   int hourStop = 0;

   switch(type)
     {
      case SYDNEY:
         hourStart = isSummerTime() ? 22 : 23;
         hourStop  = isSummerTime() ? 7 : 8;
         break;
      case TOKYO:
         hourStart = isSummerTime() ? 0 : 1;
         hourStop  = isSummerTime() ? 9 : 10;
         break;
      case LONDON:
         hourStart = isSummerTime() ? 9 : 10;
         hourStop  = isSummerTime() ? 18 : 19;
         break;
      case NEWYORK:
         hourStart = isSummerTime() ? 14 : 15;
         hourStop  = isSummerTime() ? 23 : 0;
         break;
     }

   MqlDateTime structTime;
   TimeCurrent(structTime);
   structTime.sec = 0;
   structTime.hour = hourStart;
   structTime.min = 0;
   datetime timeStart = StructToTime(structTime);
   structTime.hour = hourStop;
   datetime timeEnd = StructToTime(structTime);

   if(hourStart > hourStop)
     {
      timeEnd = timeEnd + 24 * 60 * 60;
     }

   return isStart ? timeStart : timeEnd;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool isMessageCommandType(string text)
  {
   return StringFind(text, "/", 0) == 0;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime getFirstDayOfNewsYear()
  {
   MqlDateTime firstDayOfYear;

   firstDayOfYear.year = News_Backtest_Year;
   firstDayOfYear.mon = 1;  // January
   firstDayOfYear.day = 1;  // 1st day
   firstDayOfYear.hour = 0;
   firstDayOfYear.min = 0;
   firstDayOfYear.sec = 0;

// Convert back to datetime
   return StructToTime(firstDayOfYear);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime getLastDayOfNewsYear()
  {
   MqlDateTime lastDayOfYear;

// Set the structure to the last day of the current year
   lastDayOfYear.year = News_Backtest_Year;
   lastDayOfYear.mon = 12;  // December
   lastDayOfYear.day = 31;  // 31st day
   lastDayOfYear.hour = 23;
   lastDayOfYear.min = 59;
   lastDayOfYear.sec = 59;

// Convert back to datetime
   return StructToTime(lastDayOfYear);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime createDateTimeFromString(string timeStr)
  {
// Get the current date
   MqlDateTime currentDate;
   TimeCurrent(currentDate);

// Split the time string "15:30" into hours and minutes
   string timeParts[];
   StringSplit(timeStr, ':', timeParts);

// Convert the hours and minutes from the string to integers
   int hour = StringToInteger(timeParts[0]);
   int minute = StringToInteger(timeParts[1]);

// Construct a datetime for the current day with the given hour and minute, and set seconds to 00
   MqlDateTime customDateTime;
   customDateTime.year   = currentDate.year;
   customDateTime.mon    = currentDate.mon;
   customDateTime.day    = currentDate.day;
   customDateTime.hour   = hour;
   customDateTime.min    = minute;
   customDateTime.sec    = 0;

// Convert MqlDateTime structure back to datetime type
   return StructToTime(customDateTime);
  }



//+------------------------------------------------------------------+
int               SendMessage(string text, string chatID, string botToken)
  {
   string baseUrl = "https://api.telegram.org";
   string headers = "";
   string requestURL = "";
   string requestHeaders = "";
   char resultData[];
   char posData[];
   int timeout = 2000;

// if not a command
   if(!isMessageCommandType(text))
     {
      Sleep(Notification_Delay);
     }

   requestURL = StringFormat("%s/bot%s/sendmessage?chat_id=%s&text=%s", baseUrl, botToken, chatID, text);

   int response = WebRequest("POST", requestURL, headers, timeout, posData, resultData, requestHeaders);

   string resultMessage = CharArrayToString(resultData);
   Print(resultMessage);

   return response;
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
void              algoTradingToggle(bool newStatus_True_Or_False)
  {
//--- getting the current status
   bool currentStatus = (bool) TerminalInfoInteger(TERMINAL_TRADE_ALLOWED);

//--- if the current status is equal to input trueFalse then, no need to toggle auto-trading
   if(currentStatus != newStatus_True_Or_False)
     {
      //--- Toggle Auto-Trading
      HANDLE hChart = (HANDLE) ChartGetInteger(ChartID(), CHART_WINDOW_HANDLE);
      PostMessageW(GetAncestor(hChart, GA_ROOT), WM_COMMAND, MT_WMCMD_EXPERTS, 0);
     }
  }


//+------------------------------------------------------------------+
//| Script to identify market types (trending, ranging, volatile)    |
//| Applied to XAUUSD (Gold).                                        |
//+------------------------------------------------------------------+
input group "==== Market type Config ===="

input bool     MT_Active = false; // Market type active
input CUSTOM_TIMEFRAMES MT_Timeframe = TF_H1; // Timeframe
input int      MT_MA_Period = 50;          // Moving Average period for trend detection
input int      MT_ATR_Period = 14;         // ATR period for volatility detection
input double   MT_ATR_Threshold = 2.0;     // ATR threshold for volatile market
input int      MT_RSI_Period = 14;         // RSI period for ranging detection
input int      MT_RSI_Low = 40;            // RSI low threshold for ranging market
input int      MT_RSI_High = 60;           // RSI high threshold for ranging market

enum MarketType { TRENDING_UP, TRENDING_DOWN, RANGING, VOLATILE, UNKNOWN };
MarketType market_type = UNKNOWN;

//+------------------------------------------------------------------+
//| Custom function to determine the market type                     |
//+------------------------------------------------------------------+
MarketType identifyMarketType(string symbol)
  {
   ENUM_TIMEFRAMES timeframe = (ENUM_TIMEFRAMES) MT_Timeframe;
   double ATR_Value;
   double RSI_Value;

// Calculate ATR
   ATR_Value = iATR(symbol, timeframe, MT_ATR_Period);

// Calculate RSI
   RSI_Value = iRSI(symbol, timeframe, MT_RSI_Period, PRICE_CLOSE);

// Check for Volatile Market
   if(ATR_Value > MT_ATR_Threshold)
     {
      return VOLATILE;
     }

// Check for Ranging Market
   if(RSI_Value > MT_RSI_Low && RSI_Value < MT_RSI_High)
     {
      return RANGING;
     }

// Check for Trending Market
   double close_current = iClose(symbol, timeframe, 0);
   double close_previous = iClose(symbol, timeframe, 1);

   double ma_values[1];
   CopyBuffer(iMA(symbol, timeframe, MT_MA_Period, 0, MODE_SMA, PRICE_CLOSE), 0, 0, 1, ma_values);
   double ma_current = ma_values[0];

   Print(close_current,",", close_previous, ",", ma_current);

   if(close_previous < ma_current && ma_current < close_current)
     {
      return TRENDING_UP;
     }

   if(close_previous > ma_current && ma_current > close_current)
     {
      return TRENDING_DOWN;
     }

   return UNKNOWN;  // If none of the conditions are met
  }




// OPTIMIZATION AREA

struct KhanhdeuxResult
  {
   string            pass;
   double            optimization;
   double            forward;
   double            optimizationDD;
   double            forwardDD;
   string            parameters[];
   string            headers[];
  };

KhanhdeuxResult results[];
bool isForwardBacktest = false;

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
void OnTesterInit()
  {
   Print(__FUNCTION__,"(): Start Optimization... \n-----------");
   ArrayResize(results, 0);
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double OnTester()
  {
   string name="";
   long   id=0;
   double data[2];

   data[0]=TesterStatistics(STAT_PROFIT);
   data[1]=TesterStatistics(STAT_EQUITYDD_PERCENT);

   FrameAdd(name, id, 0, data);

   if(TEST_Criteria == NET_PROFIT)
     {
      return data[0];
     }

   double minGroupSize = DCA_Strategy_MaxDistance - 1;

   if(TEST_Criteria == STOPLOSS_RATE)
     {
      if(countTotalGroups(minGroupSize) == 0)
        {
         return 0;
        }
      return 1 - countHitsPerDCAGroup(DEAL_REASON_SL, minGroupSize) / countTotalGroups(minGroupSize);
     }

   if(TEST_Criteria == TAKEPROFIT_RATE)
     {
      if(countTotalGroups(0) == 0)
        {
         return 0;
        }
      return countHitsPerDCAGroup(DEAL_REASON_TP, 1) / countTotalGroups(0);
     }

   if(TEST_Criteria == TP_BY_SL)
     {
      if(countHitsPerDCAGroup(DEAL_REASON_SL, minGroupSize) == 0)
        {
         return 0;
        }
      return countHitsPerDCAGroup(DEAL_REASON_TP, 1) / countHitsPerDCAGroup(DEAL_REASON_SL, minGroupSize);
     }

   return data[0];
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int countTotalTrades()
  {
//--- Request the complete trading history
   if(!HistorySelect(0, TimeCurrent()))
      return 0;

   int total_deals = HistoryDealsTotal();
   int total_trades = 0; // Counter for total trades

//--- Go through all deals
   for(int i = 0; i < total_deals; i++)
     {
      ulong deal_ticket = HistoryDealGetTicket(i);

      if(deal_ticket > 0)
        {
         //--- Get deal details
         long deal_magic = HistoryDealGetInteger(deal_ticket, DEAL_MAGIC);

         //--- We're only interested in trades from our EA
         if(deal_magic == DCA_Strategy_MagicNumber) // Replace with your EA's magic number
           {
            //--- Get deal entry type (entry/exit)
            int deal_entry = HistoryDealGetInteger(deal_ticket, DEAL_ENTRY);

            //--- Count only opened trades (ENTRY_IN), excluding exits
            if(deal_entry == DEAL_ENTRY_IN)
              {
               total_trades++;
              }
           }
        }
     }

//--- Return the total number of trades
   return total_trades;
  }




//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double countHitsPerDCAGroup(ENUM_DEAL_REASON dealReason, int minGroupSize)
  {
//--- Request the complete trading history
   if(!HistorySelect(0, TimeCurrent()))
      return 0;

   int total_deals = HistoryDealsTotal();
   int hitGroups = 0; // Counter for SL/TP hit groups

//--- A map to track SL/TP and close time for trades already counted
   double previous_sl_tp = 0.0;
   datetime previous_close_time = 0;

//--- Go through all deals
   for(int i = 0; i < total_deals; i++)
     {
      ulong deal_ticket = HistoryDealGetTicket(i);

      if(deal_ticket > 0)
        {
         //--- Get deal details
         long deal_magic = HistoryDealGetInteger(deal_ticket, DEAL_MAGIC);
         double deal_sl_tp = (dealReason == DEAL_REASON_SL) ? HistoryDealGetDouble(deal_ticket, DEAL_SL) : HistoryDealGetDouble(deal_ticket, DEAL_TP);
         datetime close_time = HistoryDealGetInteger(deal_ticket, DEAL_TIME);

         //--- We're only interested in trades from our EA
         if(deal_magic == DCA_Strategy_MagicNumber)
           {
            int hits = 0; // Counter for SL/TP hits in this group
            int groupSize = 0; // Counter for the number of trades in this group

            //--- We only check for new SL/TP-close time combinations
            if(MathAbs(deal_sl_tp - previous_sl_tp) > 0.00001 || close_time != previous_close_time)
              {
               //--- Reset previous data
               previous_sl_tp = deal_sl_tp;
               previous_close_time = close_time;

               //--- Search for other trades sharing the same SL/TP and close time
               for(int j = i; j < total_deals; j++)  // Start from the current index
                 {
                  ulong next_ticket = HistoryDealGetTicket(j);
                  long next_magic = HistoryDealGetInteger(next_ticket, DEAL_MAGIC);
                  double next_sl_tp = (dealReason == DEAL_REASON_SL) ? HistoryDealGetDouble(next_ticket, DEAL_SL) : HistoryDealGetDouble(next_ticket, DEAL_TP);
                  datetime next_close_time = HistoryDealGetInteger(next_ticket, DEAL_TIME);

                  //--- Check if the next trade has the same SL/TP and close time
                  if(next_magic == DCA_Strategy_MagicNumber && MathAbs(next_sl_tp - deal_sl_tp) < 0.00001 && next_close_time == close_time)
                    {
                     groupSize++; // Increment group size for every trade in the group

                     //--- Check if this trade hit the reason (SL/TP)
                     ENUM_DEAL_REASON reason = (ENUM_DEAL_REASON)HistoryDealGetInteger(next_ticket, DEAL_REASON);
                     if(reason == dealReason)
                       {
                        hits++; // Increment hit counter if trade was closed due to SL/TP
                       }
                    }
                 }

               //--- If any SL/TP hits were in the group and groupSize is >= minGroupSize
               if(hits > 0 && groupSize >= minGroupSize)
                 {
                  hitGroups++;
                 }
              }
           }
        }
     }

//--- Return the number of groups where SL/TP was hit and group size is >= minGroupSize
   return hitGroups;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int countTotalGroups(int minGroupSize)
  {
//--- Request the complete trading history
   if(!HistorySelect(0, TimeCurrent()))
      return 0;

   int total_deals = HistoryDealsTotal();
   int totalGroups = 0; // Counter for total groups

//--- Declare a dynamic array to keep track of processed deals
   bool processedDeals[];

//--- Resize the array to match the total number of deals
   ArrayResize(processedDeals, total_deals);

//--- Initialize the array with 'false'
   for(int k = 0; k < total_deals; k++)
     {
      processedDeals[k] = false;
     }

//--- Go through all deals
   for(int i = 0; i < total_deals; i++)
     {
      if(processedDeals[i])  // Skip if this deal has already been processed
         continue;

      ulong deal_ticket = HistoryDealGetTicket(i);

      if(deal_ticket > 0)
        {
         //--- Get deal details
         long deal_magic = HistoryDealGetInteger(deal_ticket, DEAL_MAGIC);
         double deal_sl_tp = HistoryDealGetDouble(deal_ticket, DEAL_SL);
         datetime close_time = HistoryDealGetInteger(deal_ticket, DEAL_TIME);

         //--- We're only interested in trades from our EA
         if(deal_magic == DCA_Strategy_MagicNumber)
           {
            int groupSize = 0; // Counter for the number of trades in this group

            //--- Mark the current deal as processed
            processedDeals[i] = true;
            groupSize++;

            //--- Search for other trades sharing the same SL/TP and close time
            for(int j = i + 1; j < total_deals; j++)  // Start from the next index
              {
               if(processedDeals[j])  // Skip if already processed
                  continue;

               ulong next_ticket = HistoryDealGetTicket(j);
               long next_magic = HistoryDealGetInteger(next_ticket, DEAL_MAGIC);
               double next_sl_tp = HistoryDealGetDouble(next_ticket, DEAL_SL);
               datetime next_close_time = HistoryDealGetInteger(next_ticket, DEAL_TIME);

               //--- Check if the next trade has the same SL/TP and close time
               if(next_magic == DCA_Strategy_MagicNumber && MathAbs(next_sl_tp - deal_sl_tp) < 0.00001 && next_close_time == close_time)
                 {
                  processedDeals[j] = true; // Mark this deal as processed
                  groupSize++; // Increment group size for every trade in the group
                 }
              }

            //--- If the group size is >= minGroupSize, count it as a valid group
            if(groupSize >= minGroupSize)
              {
               totalGroups++;
              }
           }
        }
     }

   return totalGroups;
  }




//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnTesterPass()
  {
   ulong  pass=0;
   string name="";
   long   id=0;
   double value=0.0;
   double data[];

   while(FrameNext(pass,name,id,value,data))
     {
      bool stop = false;
      for(int i=0;i< ArraySize(results);i++)
        {
         if(results[i].pass == IntegerToString(pass))
           {
            KhanhdeuxResult result = results[i];

            result.forward = data[0];
            result.forwardDD = data[1];

            results[i] = result;
            stop = true;
            isForwardBacktest = true;
            break;
           }
        }

      if(stop)
        {
         continue;
        }

      int index = ArraySize(results);
      ArrayResize(results, index + 1);

      KhanhdeuxResult result;

      result.optimization = data[0];
      result.optimizationDD = data[1];
      result.forward = 0.0;
      result.forwardDD = 0.0;
      result.pass = IntegerToString(pass);

      string     parameters_list[];
      int        parameters_count=0;
      string     parameters[];

      FrameInputs(pass, parameters_list, parameters_count);

      for(int i=0; i<parameters_count; i++)
        {
         if(parameterEnabledForOptimization(parameters_list[i]))
           {
            int newSize = ArraySize(result.parameters) + 1;
            ArrayResize(result.parameters, newSize);
            result.parameters[newSize - 1] = getResultParameter(parameters_list[i]);
           }

         if(parameterForHeader(parameters_list[i]))
           {
            int newSize = ArraySize(result.headers) + 1;
            ArrayResize(result.headers, newSize);
            result.headers[newSize - 1] = getResultParameter(parameters_list[i]);
           }
        }

      results[index] = result;
     }
  }
//+------------------------------------------------------------------+
void OnTesterDeinit()
  {
   KhanhdeuxResult filteredResults[];
   filterResults(results, filteredResults); // Filter the results based on the drawdown conditions
   sortResultsByCustomMetric(filteredResults); // Sort the filtered results

   if(ArraySize(filteredResults) == 0)
     {
      Print("NO BEST FOUND !!!");
     }
   else
     {
      showOptimizationReport(filteredResults);
      writeOptimizationReport(filteredResults);
     }

   ArrayResize(results, 0);
   ArrayResize(filteredResults, 0);
   isForwardBacktest = false;
   Print("-----------\n",__FUNCTION__,"(): End Optimization");
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void filterResults(KhanhdeuxResult &results[], KhanhdeuxResult &filteredResults[])
  {
   ArrayResize(filteredResults, 0);
   for(int i = 0; i < ArraySize(results); i++)
     {
      KhanhdeuxResult result = results[i];

      if(((isForwardBacktest && result.forwardDD <= TEST_MaxDrawdown) || !isForwardBacktest)
         && result.optimizationDD <= TEST_MaxDrawdown
         && ((isForwardBacktest && results[i].forward > 0) || !isForwardBacktest)
         && result.optimization >= 0
        )
        {
         int newSize = ArraySize(filteredResults) + 1;
         ArrayResize(filteredResults, newSize);
         filteredResults[newSize - 1] = result;
        }
     }
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateMetric(KhanhdeuxResult &result)
  {
   if(!isForwardBacktest)
     {
      return result.optimization;
     }

   return calculateProfitSum(result);
// return (result.optimization + result.forward);
// return MathPow(result.optimization + result.forward, 2) / ((MathAbs(result.optimization - result.forward) + 1e-10) * (result.optimizationDD + result.forwardDD));

//double epsilon = 1e-10; // Small number to avoid division by zero
//return (result.optimization + result.forward) / (MathAbs(result.optimization - result.forward) + epsilon);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateProfitSum(KhanhdeuxResult &result)
  {
   return (result.optimization + result.forward);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateProfitDifference(KhanhdeuxResult &result)
  {
   return MathAbs(result.optimization - result.forward);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateDrawdownSum(KhanhdeuxResult &result)
  {
   return result.optimizationDD + result.forwardDD;
  }

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void sortResultsByCustomMetric(KhanhdeuxResult &filterResults[])
  {
   int size = ArraySize(filterResults);
   for(int i = 0; i < size - 1; i++)
     {
      for(int j = 0; j < size - i - 1; j++)
        {
         double metricA = calculateMetric(filterResults[j]);
         double metricB = calculateMetric(filterResults[j + 1]);
         if(metricA < metricB)
           {
            // Swap results[j] and results[j + 1]
            KhanhdeuxResult temp = filterResults[j];
            filterResults[j] = filterResults[j + 1];
            filterResults[j + 1] = temp;
           }
        }
     }
  }
//+------------------------------------------------------------------+

//+---------------------------------------------------------------------+
//| Checking whether the external parameter is enabled for optimization |
//+---------------------------------------------------------------------+
bool parameterEnabledForOptimization(string parameter_string)
  {
   bool enable;
   long value,start,step,stop;
//--- Determine the '=' sign position in the string
   int equality_sign_index=StringFind(parameter_string,"=",0);
//--- Get the parameter values
   ParameterGetRange(StringSubstr(parameter_string,0,equality_sign_index),
                     enable,value,start,step,stop);
//--- Return the parameter status
   return(enable);
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool parameterForHeader(string parameter_string)
  {
   string header_list[] =
     {
      "SYMBOLS",
      "Trading_DayOfWeek",
      // "TradingSession_Type"
     };

   for(int i = 0; i < ArraySize(header_list); i++)
     {

      string parts[];
      int parts_count = StringSplit(parameter_string, '=', parts);

      if(parts_count == 2)
        {
         string name = parts[0];
         if(name == header_list[i])
           {
            return true;
           }
        }
     }

   return false;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string joinStringArray(const string &array[], const string delimiter)
  {
   string result = "";
   int size = ArraySize(array);

   for(int i = 0; i < size; i++)
     {
      result += array[i];
      if(i < size - 1)
        {
         result += delimiter;
        }
     }
   return result;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string getResultParameter(string parameterAsList)
  {
   string parts[];
   int parts_count = StringSplit(parameterAsList, '=', parts);

   if(parts_count == 2)
     {
      string name = parts[0];
      string value = parts[1];

      if(name == "DCA_Strategy_Type")
        {
         value = EnumToString((ENUM_STRATEGY_TYPE) StringToInteger(value));
        }

      if(name == "DCA_Strategy_Entry_Type")
        {
         value = EnumToString((ENUM_STRATEGY_ENTRY_TYPE) StringToInteger(value));
        }

      if(name == "Trading_DayOfWeek")
        {
         value = EnumToString((ENUM_DAY_OF_WEEK) StringToInteger(value));
        }

      if(name == "TradingSession_Type")
        {
         value = EnumToString((ENUM_SESSION) StringToInteger(value));
        }

      if(name == "Timeframe"
         || name == "Timeframe_High"
         || name == "MT_Timeframe"
        )
        {
         value = EnumToString((CUSTOM_TIMEFRAMES) StringToInteger(value));
        }

      return name + "=" + value;
     }
   else
     {
      Print("Warning!! Wrong parameter", parameterAsList);
     }

   return parameterAsList;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void showOptimizationReport(KhanhdeuxResult &filterResults[])
  {
   int size = TEST_ShowAll ? ArraySize(filterResults) : MathMin(ArraySize(filterResults), TEST_MaxNumberOfResult);
   for(int i=0; i<size; i++)
     {
      KhanhdeuxResult result = filterResults[i];
      string message = "No." + (i + 1);
      message += " - " + joinStringArray(result.headers, ", ") + "";
      message += "\n => " + joinStringArray(result.parameters, ", ");
      message += "\n Pass=" + result.pass + ", Metric=" + DoubleToString(calculateMetric(result),2);
      message += ", Optimization(PROFIT=" +  DoubleToString(result.optimization, 2) + "|MAX_DRAWDOWN="+ DoubleToString(result.optimizationDD, 2) + ")";

      if(isForwardBacktest)
        {
         message +=  ", Forward(PROFIT=" + DoubleToString(result.forward, 2) + "|MAX_DRAWDOWN="+ DoubleToString(result.forwardDD, 2) + ")";
        }
      message +=  "\n";
      Print(message);
     }
  }

// Save to csv
string delimiter = ";";
string infoList[] = {"OPTIMIZATION", "OPTIMIZATION_MAXDOWN", "FORWARD", "FORWARD_MAXDOWN", "PROFIT_SUM", "PROFIT_DIFF", "DRAWDOWN_SUM", "METRIC"};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void writeOptimizationReport(KhanhdeuxResult &filterResults[])
  {
// File name and path
   string fileName = "result.csv";
//string filePath = TerminalInfoString(TERMINAL_DATA_PATH) + "\\MQL5\\Files\\" + fileName;
   string filePath = "test\\" + fileName;

   if(TEST_NewReport)
     {
      deleteFile(filePath);
     }

// Check if the file exists
   bool fileExists = FileIsExist(filePath, FILE_COMMON);

// If the file doesn't exist, create it and add headers
   if(!fileExists)
     {
      int fileHandle = FileOpen(filePath, FILE_READ|FILE_WRITE|FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_COMMON|FILE_CSV);
      if(fileHandle != INVALID_HANDLE)
        {
         KhanhdeuxResult result = filterResults[0];

         string headerNameList[];
         getParameterListByIndex(result.headers, 0, headerNameList);

         string parameterNameList[];
         getParameterListByIndex(result.parameters, 0, parameterNameList);

         string header = joinStringArray(headerNameList, ";");
         header += ";" + joinStringArray(parameterNameList, ";");
         header += ";" + joinStringArray(infoList, ";");

         FileWrite(fileHandle, header);
         FileClose(fileHandle);
        }
      else
        {
         Print("Failed to create file: ", filePath);
         return;
        }
     }

// Open file in append mode (FILE_CSV | FILE_WRITE | FILE_SHARE_WRITE | FILE_ANSI)
   int fileHandle = FileOpen(filePath, FILE_READ|FILE_WRITE|FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_COMMON|FILE_CSV);

   if(fileHandle != INVALID_HANDLE)
     {
      // Move file pointer to the end for appending
      FileSeek(fileHandle, 0, SEEK_END);

      int size = TEST_ShowAll ? ArraySize(filterResults) : MathMin(ArraySize(filterResults), TEST_MaxNumberOfResult);
      for(int i = 0; i < size; i++)
        {
         KhanhdeuxResult result = filterResults[i];

         string headerValueList[];
         getParameterListByIndex(result.headers, 1, headerValueList);

         string parameterValueList[];
         getParameterListByIndex(result.parameters, 1, parameterValueList);

         string line = joinStringArray(headerValueList, ";");
         line += ";" + joinStringArray(parameterValueList, ";");

         string infoValueList[];
         for(int i=0;i<ArraySize(infoList);i++)
           {
            string name = infoList[i];
            double value = 0;
            int newSize = ArraySize(infoValueList) + 1;
            ArrayResize(infoValueList, newSize);

            if(name == "OPTIMIZATION")
              {
               value = result.optimization;
              }

            if(name == "OPTIMIZATION_MAXDOWN")
              {
               value = result.optimizationDD;
              }

            if(name == "FORWARD")
              {
               value = result.forward;
              }

            if(name == "FORWARD_MAXDOWN")
              {
               value = result.forwardDD;
              }

            if(name == "PROFIT_SUM")
              {
               value = calculateProfitSum(result);
              }

            if(name == "PROFIT_DIFF")
              {
               value = calculateProfitDifference(result);
              }

            if(name == "DRAWDOWN_SUM")
              {
               value = calculateDrawdownSum(result);
              }

            if(name == "METRIC")
              {
               value = calculateMetric(result);
              }

            infoValueList[newSize - 1] = DoubleToString(value);
           }

         line += ";" + joinStringArray(infoValueList, ";");
         FileWrite(fileHandle, line);
        }

      // Close the file
      FileClose(fileHandle);
     }
   else
     {
      Print("Failed to open file: ", filePath);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void getParameterListByIndex(string &list[], int index, string &parameterList[])
  {
   ArrayResize(parameterList, 0);
   for(int i=0;i<ArraySize(list);i++)
     {
      string item = list[i];
      string parts[];
      int parts_count = StringSplit(item, '=', parts);

      if(parts_count == 2)
        {
         int newSize = ArraySize(parameterList) + 1;
         ArrayResize(parameterList, newSize);
         parameterList[newSize - 1] = parts[index];
        }
      else
        {
         Print("Warning!! Wrong parameter", item);
        }
     }
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void deleteFile(string filePath)
  {
// Close any open file handle before deleting
   int fileHandle = FileOpen(filePath, FILE_READ | FILE_ANSI);
   if(fileHandle != INVALID_HANDLE)
     {
      FileClose(fileHandle);
     }

// Delete the file if it exists
   if(FileIsExist(filePath, FILE_COMMON))
     {
      if(FileDelete(filePath, FILE_COMMON))
        {
         Print("File deleted: ", filePath);
        }
      else
        {
         Print("Failed to delete file: ", filePath);
        }
     }
   else
     {
      Print("File does not exist: ", filePath);
     }
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
