//+------------------------------------------------------------------+
//|                                            ClimberFundHelper.mq5 |
//|                                  Copyright 2023, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2023, Khanhdeux."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Trade\PositionInfo.mqh>
#include <Trade\Trade.mqh>
#include <Telegram.mqh>
#include <RegularExpressions\Regex.mqh>
#include <Arrays\ArrayLong.mqh>
#include <News.mqh>

int SIGNAL_BUY     =  1;
int SIGNAL_SELL    =  -1;
int SIGNAL_CLOSE   =  2;
int SIGNAL_AUTO    =  3;
int SIGNAL_HEDG    =  4;

enum ENUM_DCA_ORDER_TYPE {DCA_BUY, DCA_SELL};


CTrade trade;
CPositionInfo position;
CArrayLong positionMagics;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string            signalPattern = "\\/\\b(buy|sell|auto|hedg|update)\\b (\\d+),(\\d+),?(\\d+)?"; // /buy 15,5[,1200]
string            calculatePattern = "\\/\\bcalc\\b (\\d+),(\\d+),(\\d+)"; // /calc 1200,15,5
//+------------------------------------------------------------------+
//|   KhanhdeuxBot                                                   |
//+------------------------------------------------------------------+
class KhanhdeuxBot: public CCustomBot
  {
public:
   int               signal;
   double            lotsize;
   double            distance;
   int               maxDistance;
   double            initCaptital;


public:
   bool              checkOrderPattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(signalPattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   void              setBotByPattern(const string text)
     {
      CMatchCollection *matches=CRegex::Matches(text,signalPattern);
      CMatchEnumerator *en=matches.GetEnumerator();
      string signalStr = "";

      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         signalStr = match.Groups()[1].Value();

         if(StringCompare(signalStr, "buy") == 0)
           {
            bot.signal = SIGNAL_BUY;
           }

         if(StringCompare(signalStr, "sell") == 0)
           {
            bot.signal = SIGNAL_SELL;
           }

         if(StringCompare(signalStr, "auto") == 0)
           {
            bot.signal = SIGNAL_AUTO;
           }

         if(StringCompare(signalStr, "hedg") == 0)
           {
            bot.signal = SIGNAL_HEDG;
           }

         distance = match.Groups()[2].Value();
         maxDistance = match.Groups()[3].Value();
         initCaptital = match.Groups()[4].Value();
        }

      if(bot.signal != 0 || StringCompare(signalStr, "update") == 0)
        {
         if(distance < 10 || maxDistance <= 0)
           {
            resetBot();
           }
         else
           {
            double initCapital = bot.initCaptital ? bot.initCaptital : MathAbs(StopLoss_CloseAllPositions);
            bot.lotsize = getBestLotSize(maxDistance, distance, initCapital);
           }
        }


      delete en;
      delete matches;
     }

   bool              checkCalculatePattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(calculatePattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   double              getCalculatedLotSize(const string text)
     {
      double result = 0;

      CMatchCollection *matches=CRegex::Matches(text,calculatePattern);
      CMatchEnumerator *en=matches.GetEnumerator();
      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         double _distance = match.Groups()[1].Value();
         double _maxDistance = match.Groups()[2].Value();
         double _initCapital = match.Groups()[3].Value();

         if(_initCapital > 0 && _distance > 0 && _maxDistance > 0)
           {
            result = getBestLotSize(_maxDistance, _distance, _initCapital);
           }
        }

      delete en;
      delete matches;
      return result;
     }

   void              resetBot(void)
     {
      signal = 0;
      lotsize = 0;
      distance = 0;
      maxDistance = 0;
      initCaptital = 0;
     }

   void              ProcessMessages(void)
     {
      for(int i=0; i<m_chats.Total(); i++)
        {
         CCustomChat *chat=m_chats.GetNodeAtIndex(i);
         //--- if the message is not processed
         if(!chat.m_new_one.done)
           {
            chat.m_new_one.done=true;
            string text=chat.m_new_one.message_text;
            string actionMessage = StringFormat("%s %d (1:%g)", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN), SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE)) + "\n";

            //--- start
            if(text=="/test")
               SendMessage(chat.m_id, actionMessage + "Test! I am KhanhdeuxBot. \xF680");

            //--- help
            if(text=="/help")
              {
               string helpText = "My commands list: ";
               helpText += "\n/test test bot";
               helpText += "\n/info account";
               helpText += "\n/news Pip news";
               helpText += "\n/buy buy";
               helpText += "\n/sell sell";
               helpText += "\n/close close all";
               helpText += "\n/auto auto-dca";
               helpText += "\n/hedg auto-hedg";
               helpText += "\n/reset reset signal";
               helpText += "\n/buy|sell|auto|hedg|update dist,max[,initCap] (E.g: /buy 15,5 /buy 15,5,1200)";
               helpText += "\n/calc dist,max,initCap (E.g: /calc 15,5,1200)";

               SendMessage(chat.m_id,helpText);
              }
            if(text=="/info")
              {
               string message = "";
               message       += StringFormat("[BAL]%G",AccountInfoDouble(ACCOUNT_BALANCE)) + "";
               message       += StringFormat("[PROF]%G",AccountInfoDouble(ACCOUNT_PROFIT)) + "";
               message       += StringFormat("[EQU]%G",AccountInfoDouble(ACCOUNT_EQUITY)) + "";
               message       += StringFormat("\n S:%d, Lot:%G, Dist:%G, Max:%G, InitC:%G", bot.signal, getDCALotSize(), getDCADistance(), getDCAMaxDistance(), bot.initCaptital);
               message       += getPositionMagicMessage();

               SendMessage(chat.m_id, actionMessage + message);
              }

            if(text=="/news")
              {
               SendMessage(chat.m_id, analysizeTipsAndDays());
              }

            if(text=="/buy")
              {
               signal = SIGNAL_BUY;
               SendMessage(chat.m_id, actionMessage + "Received BUY order");
              }

            if(text=="/sell")
              {
               signal = SIGNAL_SELL;
               SendMessage(chat.m_id, actionMessage + "Received SELL order");
              }

            if(text=="/close")
              {
               resetBot();
               signal = SIGNAL_CLOSE;
               SendMessage(chat.m_id, actionMessage + "Received CLOSE order");
              }

            if(text=="/auto")
              {
               signal = SIGNAL_AUTO;
               SendMessage(chat.m_id, actionMessage + "Received AUTO order");
              }

            if(text=="/hedg")
              {
               signal = SIGNAL_HEDG;
               SendMessage(chat.m_id, actionMessage + "Received HEDG order");
              }

            if(text=="/reset")
              {
               resetBot();
               SendMessage(chat.m_id, actionMessage + "Received RESET order");
              }

            if(checkOrderPattern(text))
              {
               setBotByPattern(text);
               SendMessage(chat.m_id, actionMessage + StringFormat("Received S:%d, Lot:%g, Dist:%g, Max:%g, InitC:%g", bot.signal, bot.lotsize, bot.distance, bot.maxDistance, bot.initCaptital));
              }

            if(checkCalculatePattern(text))
              {
               SendMessage(chat.m_id, getCalculatedLotSize(text));
              }
           }
        }
     }
  };
  
input group  "==== Notification ==="  
input bool    Notification_Active = false; // Notification active

input group  "==== DCA Trading ==="
input bool    DCA_Strategy_Backtest = false; // DCA Strategy Test
enum  ENUM_STRATEGY_TYPE {DCA, HEDG, NONE};
input ENUM_STRATEGY_TYPE DCA_Strategy_Type = NONE; // Strategy type
input bool    DCA_Strategy_Distance_Rsi = false; // Calculate distance by Rsi
input double  DCA_Strategy_LotSize = 0.01; // Lot Size
input int     DCA_Strategy_MaxDistance = 10; // Number of L that can be reached
input double  DCA_Strategy_Distance = 25; // Distance between L in points (10 pts=1 pip)
input double  DCA_Strategy_LastDistance = 25; // Last Stoploss distance
input ulong   DCA_Strategy_MagicNumber = 01; // Magic Number

input group  "==== No trade time ==="
input bool    TradingStop_Active = false; // Trading Stop active (GMT+2(w),+3(s))
input int     TradingStop_HourStart = 0; // Stop Hour Start
input int     TradingStop_HourStop = 1; // Stop Hour Stop
input bool    TradingStop_CloseAllPositions = true; // Close all positions before Stop Hour

input group  "==== No trade on News ==="
input bool    TradingStopNews_Active = true; // Trading Stop on News
input int     TradingStopNews_CloseResumeMinutes = 30; // How long in min trades will be deactivated and activated

input group  "==== Trading Stop Daily ==="
input bool    TradingStopDaily_Active = true; // Trading Stop Daily

input group  "==== General Stoploss ==="
input bool     Stoploss_Active = true; // Stoploss active
input double   StopLoss_CloseAllPositions      = -1200; // Stoploss to close all positions ($)
input double   StopLoss_DefaultPoints          = 5000; // Default Stoploss points

input group  "==== Telegram Credentials ==="
input string ChatID = ""; // Chat ID
input string BotToken = ""; // Bot Token
input bool TestTelegram = false; // Test Telegram Signal

input group  "==== Stoploss Warning ==="
input double InitLoss = -100; // Initial Warning Loss (in $)
input double LossDistance = 100; // Distance Loss (InitLoss + distance = next warning)

input group  "==== Indicators Warning ==="
input bool Warning_Rsi = false; // Rsi Warning?
input bool Warning_Cci = false; // CCi Warning?

input group  "==== DCA Analysis ==="
input bool   DCA_Matrix = false; // Show DCA Matrix
input double DCA_MinNumOfDistance = 1; // Number of min Level
input double DCA_MaxNumOfDistance = 10; // Number of max Level
input int    DCA_LotSize_Multiply = 2; // Lotsize multiply

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
KhanhdeuxBot bot;
int getmeResult;
double loss;
int numOfPositions;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {
   Print("=== INIT === ", TimeCurrent());

   loss = InitLoss;
   numOfPositions = PositionsTotal();

   if(ChatID != "" && BotToken != "")
     {
      if(TestTelegram && Notification_Active)
        {
         SendMessage("test", ChatID, BotToken);
        }

      //--- set token
      bot.Token(BotToken);
      //--- check token
      getmeResult=bot.GetMe();
     }

   EventSetTimer(10);

   trade.SetExpertMagicNumber(DCA_Strategy_MagicNumber);

   if(DCA_Matrix)
     {
      showDCAMatrix();
     }

   if(DCA_Strategy_Backtest)
     {
      news.SaveHistory(true);
      news.LoadHistory(true);
     }
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//--- destroy timer
   EventKillTimer();
   bot.signal = 0;
   CRegex::ClearCache();
   loss = InitLoss;
   positionMagics.Clear();
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
   if(Stoploss_Active)
     {
      if(TotalProfit() < StopLoss_CloseAllPositions)
        {
         CloseAllPositions();
         Print(StringFormat("Stoploss exeeded = %G", StopLoss_CloseAllPositions));
        }
     }

   if(ChatID != "" && BotToken != "" && Notification_Active)
     {
      SendPositionsChanged();
     }

   static datetime bar_time=0;
   datetime this_bar_time=iTime(_Symbol,PERIOD_M1,0);

   if(bar_time!=this_bar_time)
     {
      bar_time=this_bar_time;

      if(ChatID != "" && BotToken != "" && Notification_Active)
        {
         SendMessageToStoplossWarning();

         if(Warning_Rsi)
           {
            SendMessageToRsi();
           }

         if(Warning_Cci)
           {
            SendMessageToCci();
           }
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void testPattern()
  {
   string pattern= signalPattern;
   string in="/buy 0.01,25,4";

   CRegex *r=new CRegex(pattern,RegexOptions::IgnoreCase);
   CMatch *m=r.Match(in);

   if(m.Success())
     {
      CMatchCollection *matches=CRegex::Matches(in,pattern);
      CMatchEnumerator *en=matches.GetEnumerator();
      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         Print("1: " + match.Groups()[1].Value());
         Print("2: " + match.Groups()[2].Value());
         Print("3: " + match.Groups()[3].Value());
         Print("4: " + match.Groups()[4].Value());
         Print("5: " + match.Groups()[5].Value());
         Print("\0");
        }

      delete en;
      delete matches;
     }

   delete r;
   delete m;
  }

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
bool isOnTimerLoaded = false;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnTimer()
  {
   ExecuteDCATrading();
   executeTelegramBot();
// testPattern();
   isOnTimerLoaded = true;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string analysizeTipsAndDays()
  {
   int days = 30;
   string message = "";

   for(int i=days; i>= 0; i = i - 5)
     {
      MqlRates rate[];
      CopyRates(_Symbol,PERIOD_D1,i,1,rate);
      // message´+= rate[0].time + ": O=" + rate[0].open + " H=" + rate[0].high + " L=" + rate[0].low + " C=" + rate[0].close;
      message += rate[0].time + " ";
      double high = rate[0].high;
      double low = rate[0].low;
      double maxL = MathRound((high - low)/(DCA_Strategy_Distance / 10));
      // message += "" + DoubleToString(test, 0) + ",";
      // message´+= "Total pips:" + (high - low)/(DCA_Strategy_Distance / 10);
      message += "Pips:" + DoubleToString((high - low),2) + "\n";

      int USD_COUNTRY_ID = 840;
      datetime currentTime = rate[0].time;

      bool hasNews = false;
      int totalnews = ArraySize(news.event);
      for(int i=0; i<totalnews; i++)
        {
         if(news.event[i].country_id == USD_COUNTRY_ID
            && news.event[i].importance >=3
            && news.event[i].time > currentTime && news.event[i].time < currentTime + 24 * 60 * 60
            && !hasNews)
           {
            message += news.event[i].country_id + " " +
                       news.eventname[i] + " " +
                       news.event[i].sector + " " +
                       news.event[i].time + " " +
                       news.event[i].timemode + " " +
                       news.event[i].event_type + " " +
                       news.event[i].importance + "\n";
            hasNews = true;
           }
        }

      message += "========================\n" ;
     }

   return message;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CNews news;
bool checkStopNews()
  {
   int USD_COUNTRY_ID = 840;
   datetime currentTime = TimeCurrent(); // + 16 * 60 * 60; // TimeCurrent()
   int timeToStopResumeTrading = TradingStopNews_CloseResumeMinutes * 60;

   if(DCA_Strategy_Backtest)
     {
      int totalnews = ArraySize(news.event);
      for(int i=0; i<totalnews; i++)
        {
         if(news.event[i].country_id == USD_COUNTRY_ID
            && news.event[i].importance >=3
            && (news.event[i].time - timeToStopResumeTrading) < currentTime && currentTime < (news.event[i].time + timeToStopResumeTrading))
           {
            Print(news.event[i].country_id," ",
                  news.eventname[i]," ",
                  news.event[i].sector," ",
                  news.event[i].time," ",
                  news.event[i].timemode," ",
                  news.event[i].event_type," ",
                  news.event[i].importance
                 );
            return true;
           }
        }
     }
   else
     {
      MqlCalendarEvent event;    //for saving the events importance and country code
      MqlCalendarValue values[];  //for saving the events time and id

      datetime dateFrom=TimeCurrent();  // take all events from
      datetime dateTo=TimeCurrent() + timeToStopResumeTrading + 2 * 60 * 60;     // take all events to
      datetime currentDate = StringToTime(TimeToString(TimeCurrent(), TIME_DATE));

      if(CalendarValueHistory(values,currentDate,0))   //get all the events
        {
         for(int i=0; i<ArraySize(values); i++)
           {
            //get the event  info
            if(CalendarEventById(values[i].event_id,event))
              {
               // if is important enought and time based
               if(event.country_id == USD_COUNTRY_ID
                  && event.importance>=3
                  && (values[i].time - timeToStopResumeTrading) < currentTime && currentTime < (values[i].time + timeToStopResumeTrading))
                 {
                  Print(event.country_id," ",
                        event.name," ",
                        event.importance," ",
                        values[i].time,"");
                  return true;
                 }
              }
           }
        }

     }
   return false;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool checkStopDaily()
  {
   datetime currentTime = TimeCurrent();
   datetime nextDay = StringToTime(TimeToString(TimeCurrent(), TIME_DATE)) + 24 * 60 * 60;
   int timeRange = 20 * 60;

   if((nextDay - timeRange) < currentTime && currentTime < nextDay + timeRange)
     {
      return true;
     }

   return false;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void executeTelegramBot()
  {
   if(ChatID != "" && BotToken != "")
     {
      //--- show error message end exit
      if(getmeResult!=0)
        {
         Comment("Error: ",GetErrorDescription(getmeResult));
         return;
        }

      //--- show bot name
      Comment(bot.Name(), ":", DCA_Strategy_MagicNumber);
      //--- reading messages
      bot.GetUpdates();
      //--- processing messages
      bot.ProcessMessages();
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double TotalProfit()
  {
   double profit = 0;
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      ulong ticket = PositionGetTicket(i);
      string positionSymbol=PositionGetSymbol(i);

      if(ticket > 0 && positionSymbol == _Symbol)
        {
         profit += PositionGetDouble(POSITION_PROFIT);
        }
     }

   return profit;
  }

//+------------------------------------------------------------------+
//| Close all positions                                              |
//+------------------------------------------------------------------+
void CloseAllPositions()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(position.SelectByIndex(i))
        {
         if(position.Symbol() == _Symbol)
           {
            trade.PositionClose(position.Ticket());
           }
        }
     }
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void showDCAMatrix()
  {
   double initLotSize = 0.01 * DCA_LotSize_Multiply;

   for(double k=initLotSize; k<=initLotSize * 10; k=k+initLotSize)
     {
      Print("Lot Size:" + DoubleToString(k, 2));

      for(int i = 1; i <= DCA_MaxNumOfDistance; i++)
        {
         // Calculate TotalVolume
         double totalLotSize = calculateTotalVolume(i, k);

         // Calculate MaxLoss
         double maxLoss = calculateMaxLoss(i, k, DCA_Strategy_Distance);

         // Calculate MaxProfit
         double maxProfit = calculateMaxProfit(i, k, DCA_Strategy_Distance);

         if(maxLoss >= MathAbs(StopLoss_CloseAllPositions))
           {
            break;
           }

         if(i < DCA_MinNumOfDistance)
           {
            continue;
           }

         Print(StringFormat("L%d___Total Volume:[%g], Max Loss:[-%g($)], Max Profit:[%g($)]", i, totalLotSize, maxLoss, maxProfit));
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getBestLotSize(double level, double distance, double initCapital)
  {
   double initLotSize = 0.01 * DCA_LotSize_Multiply;
   double bestLotSize = 0;

   for(double k=initLotSize; k<=initLotSize * 10; k=k+initLotSize)
     {
      for(int i = 1; i <= level; i++)
        {

         // Calculate MaxLoss
         double maxLoss = calculateMaxLoss(i, k, distance);

         // Calculate MaxProfit
         double maxProfit = calculateMaxProfit(i, k, distance);

         if(maxLoss >= initCapital)
           {
            break;
           }

         if(i < level)
           {
            continue;
           }

         bestLotSize = k;
        }
     }

   return bestLotSize ? NormalizeDouble(bestLotSize, 2) : DCA_Strategy_LotSize;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateTotalVolume(int i, double lotSize)
  {
   double totalLotSize = 0;
   double currentLotSize = lotSize;

   for(int j=1; j<=i; j++)
     {
      totalLotSize += currentLotSize;
      currentLotSize *=2;
     }

   return totalLotSize;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateMaxLoss(int i, double lotSize, double distance)
  {
   double totalLoss = 0;
   double currentLotSize = lotSize;
   double pips = distance/10;

   for(int j=1; j<=i; j++)
     {
      totalLoss += currentLotSize * pips * SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE) * (i - j + 1);
      currentLotSize *=2;
     }

   return totalLoss;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateMaxProfit(int i, double lotSize, double distance)
  {
   double totalProfit = 0;
   double pips = distance/10;
   double currentLotSize = lotSize;

   double takeProfitPrice = 0;
   double priceVolumeSum = 0;

   for(int j=1; j<=i; j++)
     {
      totalProfit += pips * currentLotSize * SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE);
      currentLotSize *=2;
     }

   return totalProfit;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getRequestSignal()
  {
   if(bot.signal)
      return bot.signal;
   if(DCA_Strategy_Type == DCA)
      return SIGNAL_AUTO;
   if(DCA_Strategy_Type == HEDG)
      return SIGNAL_HEDG;

   return 0;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void ExecuteDCATrading()
  {

   int requestSignal = getRequestSignal();

   if(TradingStopNews_Active && checkStopNews())
     {
      CloseAllPositions();
      return;
     }

   if(TradingStopDaily_Active && checkStopDaily())
     {
      CloseAllPositions();
      return;
     }

   if(TradingStop_Active && checkTradingStopHour())
     {
      if(TradingStop_CloseAllPositions)
        {
         CloseAllPositions();
        }
      return;
     }

   if(requestSignal == SIGNAL_CLOSE)
     {
      CloseAllPositions();
      return;
     }

   if(getTotalDCAOrders() > 0)
     {
      return;
     }

   ENUM_DCA_ORDER_TYPE orderTypes[];
   setOrderTypesByRequestSignal(orderTypes, requestSignal);

   for(int i=0; i < ArraySize(orderTypes); i++)
     {
      ENUM_DCA_ORDER_TYPE orderType = orderTypes[i];
      int totalPositions = getTotalDCAPositions(orderType);

      if(totalPositions == getDCAMaxDistance() && getNextDCASignal(orderType))
        {
         closeAllDCAPositions(orderType);
         return;
        }


      if(totalPositions == getDCAMaxDistance())
        {
         return;
        }

      int signal = 0;
      if(totalPositions == 0)
        {
         signal = orderType == DCA_BUY ? SIGNAL_BUY : SIGNAL_SELL;
        }
      else
        {
         signal = getNextDCASignal(orderType);
        }

      if(signal)
        {
         int newTotalPositions = totalPositions + 1;

         double lotSize =  calculateDCALotSize(newTotalPositions);
         double price = getCurrentPrice(orderType);
         double tp = calculateDCATakeProfit(lotSize, price, orderType);

         updateAllDCAPositionTakeProfit(tp, orderType);

         if(signal == SIGNAL_BUY)
           {
            double sl = NormalizeDouble(SymbolInfoDouble(_Symbol, SYMBOL_BID) - StopLoss_DefaultPoints*_Point,_Digits);
            Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, price, sl, tp, "BUY:" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions));
            trade.Buy(lotSize, _Symbol, price, sl, tp, "BUY:" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions);
           }

         if(signal == SIGNAL_SELL)
           {
            double sl = NormalizeDouble(SymbolInfoDouble(_Symbol, SYMBOL_ASK) + StopLoss_DefaultPoints*_Point,_Digits);
            Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, price, sl, tp, "SELL:" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions));
            trade.Sell(lotSize, _Symbol, price, sl, tp, "SELL:" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions);
           }
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void setOrderTypesByRequestSignal(ENUM_DCA_ORDER_TYPE& orderTypes[], int requestSignal)
  {

   if(requestSignal == SIGNAL_HEDG || requestSignal == SIGNAL_AUTO)
     {
      int buyTotal = getTotalDCAPositions(DCA_BUY);
      int sellTotal = getTotalDCAPositions(DCA_SELL);
      int rsiSignal = getRsiSignal();
      int count = 0;

      if(requestSignal == SIGNAL_AUTO)
        {
         if((rsiSignal == SIGNAL_BUY && buyTotal == 0 && sellTotal == 0) || buyTotal > 0)
           {
            ArrayResize(orderTypes, count + 1);
            orderTypes[count++] = DCA_BUY;
           }
         else
            if((rsiSignal == SIGNAL_SELL && sellTotal == 0 && buyTotal == 0) || sellTotal > 0)
              {
               ArrayResize(orderTypes, count + 1);
               orderTypes[count++] = DCA_SELL;
              }
        }

      if(requestSignal == SIGNAL_HEDG)
        {
         if((buyTotal == 0) || buyTotal > 0)
           {
            ArrayResize(orderTypes, count + 1);
            orderTypes[count++] = DCA_BUY;
           }

         if((sellTotal == 0) || sellTotal > 0)
           {
            ArrayResize(orderTypes, count + 1);
            orderTypes[count++] = DCA_SELL;
           }
        }
     }

   if(requestSignal == SIGNAL_BUY)
     {
      ArrayResize(orderTypes, 1);
      orderTypes[0] = DCA_BUY;
     }

   if(requestSignal == SIGNAL_SELL)
     {
      ArrayResize(orderTypes, 1);
      orderTypes[0] = DCA_SELL;
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getTotalDCAPositions(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   int count = 0;
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
        )
        {
         count++;
        }
     }

   return count;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getTotalDCAOrders()
  {
   int count = 0;
   for(int i = OrdersTotal() - 1; i >= 0; i--)
     {
      if(OrderSelect(OrderGetTicket(i)))
        {
         string symbol = OrderGetString(ORDER_SYMBOL);
         long magicNumber = OrderGetInteger(ORDER_MAGIC);

         if(symbol == _Symbol && magicNumber == DCA_Strategy_MagicNumber)
           {
            count++;
           }
        }
     }

   return count;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getRsiSignal()
  {
   double rsi[];
   int rsiHandle = iRSI(_Symbol, PERIOD_M1, 14, PRICE_CLOSE);
   int oversold = 30;
   int overbought = 70;
   int signal = 0;
   CopyBuffer(rsiHandle, 0, 0, 3, rsi);

   if(rsi[0] < oversold && rsi[1] < oversold && rsi[2] >= oversold)
     {
      signal = SIGNAL_BUY;
     }

   if(rsi[0] > overbought && rsi[1] > overbought && rsi[2] <= overbought)
     {
      signal = SIGNAL_SELL;
     }

   ArrayFree(rsi);
   return signal;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int   getNextDCASignal(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   int signal = 0;
   double lastPrice = getLastDCAPrice(currentOrderType);
   double pips = getDCADistance()/10;
   double currentPrice = getCurrentPrice(currentOrderType);
   int totalPositions = getTotalDCAPositions(currentOrderType);

   if(totalPositions == getDCAMaxDistance())
     {
      pips = (DCA_Strategy_LastDistance/10);
     }

   if(currentOrderType == DCA_BUY)
     {
      if(currentPrice <= (lastPrice - pips))
        {
         if(DCA_Strategy_Distance_Rsi)
           {
            if(totalPositions == getDCAMaxDistance())
              {
               signal = SIGNAL_BUY;
              }
            else
               if(getRsiSignal() == SIGNAL_BUY)
                 {
                  signal = SIGNAL_BUY;
                 }
           }
         else
           {
            signal = SIGNAL_BUY;
           }

        }
     }

   if(currentOrderType == DCA_SELL)
     {
      if(currentPrice >= (lastPrice + pips))
        {
         if(DCA_Strategy_Distance_Rsi)
           {
            if(totalPositions == getDCAMaxDistance())
              {
               signal = SIGNAL_SELL;
              }
            else
               if(getRsiSignal() == SIGNAL_SELL)
                 {
                  signal = SIGNAL_SELL;
                 }
           }
         else
           {
            signal = SIGNAL_SELL;
           }

        }
     }

   return signal;
  }

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateDCATakeProfit(double lotSize, double price, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   double priceVolumeSum = 0;
   double totalVolume = 0.0;
   double pips = getDCADistance()/10;

   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
        )
        {
         priceVolumeSum += currentOrderType == DCA_BUY ? (position.PriceOpen() + pips) * position.Volume() : (position.PriceOpen() - pips) * position.Volume();
         totalVolume += position.Volume();
        }
     }

   priceVolumeSum += currentOrderType == DCA_BUY ? (price + pips) * lotSize : (price - pips) * lotSize;
   totalVolume += lotSize;

   return priceVolumeSum / totalVolume;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateDCALotSize(int i)
  {
   if(i == 1)
      return getDCALotSize();
   return calculateDCALotSize(i - 1) * 2;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getCurrentPrice(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   double currentPrice = SymbolInfoDouble(_Symbol, SYMBOL_LAST);

   if(currentOrderType == DCA_BUY)
     {
      currentPrice = SymbolInfoDouble(_Symbol, SYMBOL_BID);
     }
   else
      if(currentOrderType == DCA_SELL)
        {
         currentPrice = SymbolInfoDouble(_Symbol, SYMBOL_ASK);
        }

   return currentPrice;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getDCALotSize()
  {
   if(bot.lotsize)
      return bot.lotsize;
   return DCA_Strategy_LotSize;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getDCAMaxDistance()
  {
   if(bot.maxDistance)
      return bot.maxDistance;
   return DCA_Strategy_MaxDistance;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getDCADistance()
  {
   if(bot.distance)
      return bot.distance;
   return DCA_Strategy_Distance;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getPositionTypeByOrder(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   if(currentOrderType == DCA_BUY)
     {
      return POSITION_TYPE_BUY;
     }
   if(currentOrderType == DCA_SELL)
     {
      return POSITION_TYPE_SELL;
     }

   return NULL;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getLastDCAPrice(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
        )
        {
         return PositionGetDouble(POSITION_PRICE_OPEN);
        }
     }

   return 0;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void updateAllDCAPositionTakeProfit(double tpPrice, ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
        )
        {
         double sl = position.StopLoss();
         double tp = tpPrice;

         trade.PositionModify(position.Ticket(),sl,tp);
         Print(StringFormat("Position with lotsize=%g updated TakeProfit= %g", PositionGetDouble(POSITION_VOLUME), tpPrice));
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
void closeAllDCAPositions(ENUM_DCA_ORDER_TYPE currentOrderType)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i)
         && PositionGetSymbol(i) == _Symbol
         && position.Magic() == DCA_Strategy_MagicNumber
         && PositionGetInteger(POSITION_TYPE) == getPositionTypeByOrder(currentOrderType)
        )
        {
         trade.PositionClose(position.Ticket());
         Print(StringFormat("Close Position with lotsize=%g", PositionGetDouble(POSITION_VOLUME)));
        }
     }
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
bool checkTradingStopHour()
  {
   MqlDateTime time1;
   TimeToStruct(TimeCurrent(),time1);
   int hour=time1.hour;

   if(TradingStop_HourStart == TradingStop_HourStop)
     {
      if(hour == TradingStop_HourStart)
        {
         return true;
        }
     }

   if(TradingStop_HourStart < TradingStop_HourStop)
     {
      if(TradingStop_HourStart <= hour && hour < TradingStop_HourStop)
        {
         return true;
        }
     }

   if(TradingStop_HourStart > TradingStop_HourStop)
     {
      if(TradingStop_HourStart <= hour || hour < TradingStop_HourStop)
        {
         return true;
        }
     }

   return false;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SendMessageToRsi()
  {
   double rsi[];
   int rsiHandle = iRSI(_Symbol, PERIOD_M1, 14, PRICE_CLOSE);
   int oversold = 30;
   int overbought = 70;
   bool signal = false;

   CopyBuffer(rsiHandle, 0, 0, 3, rsi);

   string message = "SIGNAL!!!" + "%0A";

   if(rsi[0] < oversold && rsi[1] < oversold && rsi[2] >= oversold)
     {
      message += "BUY: RSI crossover " + StringFormat("%d", oversold);
      signal = true;
     }

   if(rsi[0] > overbought && rsi[1] > overbought && rsi[2] <= overbought)
     {
      message += "SELL: RSI crossunder " + StringFormat("%d", overbought);
      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }

   ArrayFree(rsi);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SendMessageToCci()
  {
   double cci[];
   int cciHanle = iCCI(_Symbol, PERIOD_M1, 14, PRICE_TYPICAL);
   int upper = 100;
   int lower = -100;
   bool signal = false;

   CopyBuffer(cciHanle, 0, 0, 2, cci);

   string message = "SIGNAL!!!" + "%0A";

   if(cci[0] < lower && cci[1] >= lower)
     {
      message += "BUY: CCI crossover " + StringFormat("%d", lower);
      signal = true;
     }


   if(cci[0] > upper && cci[1] <= upper)
     {
      message += "BUY: CCI crossunder " + StringFormat("%d", upper);
      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }

   ArrayFree(cci);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void initPositionMagics()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      ulong ticket = PositionGetTicket(i);
      if(ticket > 0)
        {
         long magic = position.Magic();
         bool found = false;
         for(int j=0; j< positionMagics.Total(); j++)
           {
            if(magic == positionMagics[j])
              {
               found = true;
              }
           }

         if(!found)
           {
            positionMagics.Add(magic);
           }
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SendMessageToStoplossWarning()
  {
   double pL = TotalProfit();

   if(pL <= loss)
     {
      string message = "WARNING!" + "";
      message       += StringFormat("%s %d (1:%g)", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN), SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE)) + "%0A";
      message       += "Loss:" + StringFormat("%g ", pL) + "<" + StringFormat("%g ", loss) + "";
      message       += getPositionMagicMessage();

      printf(message);
      SendMessage(message, ChatID, BotToken);
      loss -= LossDistance;
      positionMagics.Clear();
     }
   else
     {
      if(pL > loss + LossDistance * 1.5 && pL < InitLoss)
        {
         loss = loss + LossDistance;
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SendPositionsChanged()
  {
   int total = PositionsTotal();

   if(numOfPositions != total)
     {
      string message = "NOTICE!" + "";
      message       += "%0A" + StringFormat("%s %d (1:%g)", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN), SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE)) + "%0A";
      message       += "[PO]" + StringFormat("%d", numOfPositions) + "-" + StringFormat("%d", total) + "";
      message       += getPositionMagicMessage();
      message       += StringFormat("[BAL]%G",AccountInfoDouble(ACCOUNT_BALANCE)) + "";
      message       += StringFormat("[PRO]%G",AccountInfoDouble(ACCOUNT_PROFIT)) + "";
      message       += StringFormat("[EQU]%G",AccountInfoDouble(ACCOUNT_EQUITY)) + "";

      numOfPositions = PositionsTotal();

      printf(message);
      SendMessage(message, ChatID, BotToken);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string getPositionMagicMessage()
  {
   string magicMessage = "";
   initPositionMagics();

   for(int i=0; i<positionMagics.Total(); i++)
     {
      long magic = positionMagics[i];
      int count = 0;
      for(int j = PositionsTotal() - 1; j >= 0; j--)
        {
         ulong ticket = PositionGetTicket(j);
         long currentMagic = position.Magic();

         if(ticket >0 && magic == currentMagic)
           {
            count +=1;
           }
        }

      magicMessage += StringFormat("[%d]", magic) +  "";
      magicMessage += StringFormat("L%d", count);
      magicMessage += "";
     }

   return magicMessage;
  }
//+------------------------------------------------------------------+
int SendMessage(string text, string chatID, string botToken)
  {
   string baseUrl = "https://api.telegram.org";
   string headers = "";
   string requestURL = "";
   string requestHeaders = "";
   char resultData[];
   char posData[];
   int timeout = 2000;

   requestURL = StringFormat("%s/bot%s/sendmessage?chat_id=%s&text=%s", baseUrl, botToken, chatID, text);

   int response = WebRequest("POST", requestURL, headers, timeout, posData, resultData, requestHeaders);

   string resultMessage = CharArrayToString(resultData);
   Print(resultMessage);

   return response;
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
