//+------------------------------------------------------------------+
//|                                                     daylight.mqh |
//|                                         Copyright © 2018,Amr Ali |
//|                             https://www.mql5.com/en/users/amrali |
//+------------------------------------------------------------------+
#property copyright "Copyright © 2018,Amr Ali"
#property link      "https://www.mql5.com/en/users/amrali"
#property version   "1.000"
//+------------------------------------------------------------------+
//| Compute the daylight saving time changes in London, UK           |
//| Validated to https://www.timeanddate.com/time/change/uk/london   |
//+------------------------------------------------------------------+
void DST_Europe(int iYear, datetime &dst_start, datetime &dst_end)
  {
   datetime dt1,dt2;
   MqlDateTime st1,st2;
   /* UK DST begins at 01:00 local time on the last Sunday of March
      and ends at 02:00 local time on the last Sunday of October */
   dt1=StringToTime((string)iYear+".03.31 01:00");
   dt2=StringToTime((string)iYear+".10.31 02:00");
   TimeToStruct(dt1,st1);
   TimeToStruct(dt2,st2);
   dst_start=dt1-(st1.day_of_week*86400);
   dst_end  =dt2-(st2.day_of_week*86400);
  }
//+------------------------------------------------------------------+
//| Compute the daylight saving time changes in New York, USA        |
//| Validated to https://www.timeanddate.com/time/change/usa/new-york|
//+------------------------------------------------------------------+
void DST_USA(int iYear, datetime &dst_start, datetime &dst_end)
  {
   datetime dt1,dt2;
   MqlDateTime st1,st2;
   /* US DST begins at 02:00 local time on the second Sunday of March
      and ends at 02:00 local time on the first Sunday of November */
   dt1=StringToTime((string)iYear+".03.14 02:00");
   dt2=StringToTime((string)iYear+".11.07 02:00");
   TimeToStruct(dt1,st1);
   TimeToStruct(dt2,st2);
   dst_start=dt1-(st1.day_of_week*86400);
   dst_end  =dt2-(st2.day_of_week*86400);
  }
//+------------------------------------------------------------------+
