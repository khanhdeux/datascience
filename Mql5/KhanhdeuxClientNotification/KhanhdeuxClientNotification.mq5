//+------------------------------------------------------------------+
//|                                  KhanhdeuxClientNotification.mq5 |
//|                                       Copyright 2023, Khanhdeux. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2023, Khanhdeux."
#property link      "https://www.mql5.com"
#property version   "1.00"
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+

input group  "==== Telegram Credentials ==="
input string ChatID = ""; // Chat ID
input string BotToken = ""; // Bot Token
input bool TestTelegram = false; // Test Telegram Signal

input group  "==== Account Info ==="
input bool   Info_Activate = true;  // Activate Info signal
input double Info_Interval = 60;     // Info Interval in min

double currentBalance;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {
   Print("=== INIT === ", TimeCurrent());

   EventSetTimer(Info_Interval * 60);

   if(ChatID != "" && BotToken != "")
     {
      if(TestTelegram)
        {
         SendMessage("test", ChatID, BotToken);
        }
     }

   currentBalance = AccountInfoDouble(ACCOUNT_BALANCE);
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

  }


//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
   SendMessageToBalanceChange();
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer()
  {
   if(Info_Activate)
     {
      SendMessageToInfo();
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void       SendMessageToBalanceChange()
  {
   double newBalance = AccountInfoDouble(ACCOUNT_BALANCE);

   if(currentBalance != newBalance)
     {
      if(currentBalance < newBalance)
        {
         string message        = "";
         message       += StringFormat("BALANCE=%G. TP=%+.2f$", newBalance, (newBalance - currentBalance)) + "";
         message       += "%0A" + StringFormat("%s %d (1:%g)", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN), SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE)) + "%0A";
         StringReplace(message, "+", "%2b");

         printf(message);
         if(ChatID != "" && BotToken != "")
           {
            SendMessage(message, ChatID, BotToken);
           }
        }
      currentBalance = newBalance;
     }
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SendMessageToInfo()
  {
   string message        = "";
   message       += StringFormat("BALANCE=%G",AccountInfoDouble(ACCOUNT_BALANCE)) + "";
   message       += "%0A" + StringFormat("%s %d (1:%g)", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN), SymbolInfoDouble(_Symbol, SYMBOL_TRADE_CONTRACT_SIZE)) + "%0A";

   printf(message);
   if(ChatID != "" && BotToken != "")
     {
      SendMessage(message, ChatID, BotToken);
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int SendMessage(string text, string chatID, string botToken)
  {
   string baseUrl = "https://api.telegram.org";
   string headers = "";
   string requestURL = "";
   string requestHeaders = "";
   char resultData[];
   char posData[];
   int timeout = 2000;

   requestURL = StringFormat("%s/bot%s/sendmessage?chat_id=%s&text=%s", baseUrl, botToken, chatID, text);

   int response = WebRequest("POST", requestURL, headers, timeout, posData, resultData, requestHeaders);

   string resultMessage = CharArrayToString(resultData);
   Print(resultMessage);

   return response;
  }
//+------------------------------------------------------------------+
