//+------------------------------------------------------------------+
//|                                            ClimberFundHelper.mq5 |
//|                                  Copyright 2023, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2023, Khanhdeux."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Trade\PositionInfo.mqh>
#include <Trade\Trade.mqh>
#include <Telegram.mqh>
#include <RegularExpressions\Regex.mqh>
#include <Arrays\ArrayLong.mqh>
#include <News.mqh>

int SIGNAL_BUY   =  1;
int SIGNAL_SELL  =  -1;
int SIGNAL_CLOSE =  2;
int SIGNAL_AUTO  =  3;


CTrade trade;
CPositionInfo position;
CArrayLong positionMagics;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string            signalPattern = "\\/\\b(buy|sell|auto)\\b ([+-]?[0-9]*[.]?[0-9]+),(\\d+),(\\d+),?(\\d+)?"; // /buy 0.01,15,5,1200
string            calculatePattern = "\\/\\bcalc\\b (\\d+),(\\d+),(\\d+)"; // /calc 1200,15,5
//+------------------------------------------------------------------+
//|   KhanhdeuxBot                                                   |
//+------------------------------------------------------------------+
class KhanhdeuxBot: public CCustomBot
  {
public:
   int               signal;
   double            lotsize;
   double            distance;
   int               maxDistance;
   double            initCaptital;


public:
   bool              checkOrderPattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(signalPattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   void              setBotByPattern(const string text)
     {
      CMatchCollection *matches=CRegex::Matches(text,signalPattern);
      CMatchEnumerator *en=matches.GetEnumerator();
      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         string signalStr = match.Groups()[1].Value();

         if(StringCompare(signalStr, "buy") == 0)
           {
            bot.signal = SIGNAL_BUY;
           }
         else
            if(StringCompare(signalStr, "sell") == 0)
              {
               bot.signal = SIGNAL_SELL;
              }
            else
               if(StringCompare(signalStr, "auto") == 0)
                 {
                  bot.signal = SIGNAL_AUTO;
                 }
               else
                 {
                  bot.signal = 0;
                 }

         lotsize = match.Groups()[2].Value();
         distance = match.Groups()[3].Value();
         maxDistance = match.Groups()[4].Value();
         initCaptital = match.Groups()[5].Value();
        }

      if(bot.signal != 0)
        {
         if(distance <= 0 || maxDistance <= 0)
           {
            resetBot();
           }
         else
            if(lotsize == 0)
              {
               double initCapital = bot.initCaptital ? bot.initCaptital : MathAbs(StopLoss_CloseAllPositions);
               bot.lotsize = getBestLotSize(maxDistance, distance, initCapital);
              }
        }


      delete en;
      delete matches;
     }

   bool              checkCalculatePattern(const string text)
     {
      bool valid = false;

      CRegex *r=new CRegex(calculatePattern,RegexOptions::IgnoreCase);
      CMatch *m=r.Match(text);

      if(m.Success())
        {
         valid = true;
        }

      delete r;
      delete m;
      return valid;
     }

   double              getCalculatedLotSize(const string text)
     {
      double result = 0;

      CMatchCollection *matches=CRegex::Matches(text,calculatePattern);
      CMatchEnumerator *en=matches.GetEnumerator();
      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         double _initCapital = match.Groups()[1].Value();
         double _distance = match.Groups()[2].Value();
         double _maxDistance = match.Groups()[3].Value();

         if(_initCapital > 0 && _distance > 0 && _maxDistance > 0)
           {
            result = getBestLotSize(_maxDistance, _distance, _initCapital);
           }
        }

      delete en;
      delete matches;
      return result;
     }

   void              resetBot(void)
     {
      signal = 0;
      lotsize = 0;
      distance = 0;
      maxDistance = 0;
      initCaptital = 0;
     }

   void              ProcessMessages(void)
     {
      for(int i=0; i<m_chats.Total(); i++)
        {
         CCustomChat *chat=m_chats.GetNodeAtIndex(i);
         //--- if the message is not processed
         if(!chat.m_new_one.done)
           {
            chat.m_new_one.done=true;
            string text=chat.m_new_one.message_text;
            string actionMessage = StringFormat("%s %d (1:%I64d)", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN), AccountInfoInteger(ACCOUNT_LEVERAGE)) + "\n";

            //--- start
            if(text=="/test")
               SendMessage(chat.m_id, actionMessage + "Test! I am KhanhdeuxBot. \xF680");

            //--- help
            if(text=="/help")
               SendMessage(chat.m_id,"My commands list: \n/test test bot \n/info account \n/buy buy \n/sell sell \n/close close all\n/auto auto-trading \n/reset reset signal \n/calc initCap,dist,max (E.g: /calc 1200,15,5) \n/buy|sell|auto lot,dist,max[,initCap] (E.g: /buy 0.01,15,5, /buy 0,15,5,1200");

            if(text=="/info")
              {
               string message = "";
               message       += StringFormat("[BAL]%G",AccountInfoDouble(ACCOUNT_BALANCE)) + "";
               message       += StringFormat("[PROF]%G",AccountInfoDouble(ACCOUNT_PROFIT)) + "";
               message       += StringFormat("[EQU]%G",AccountInfoDouble(ACCOUNT_EQUITY)) + "";
               message       += StringFormat("\n S:%d, Lot:%G, Dist:%G, Max:%G, InitC:%G", bot.signal, getDCALotSize(), getDCADistance(), getDCAMaxDistance(), bot.initCaptital);

               SendMessage(chat.m_id, actionMessage + message);
              }

            if(text=="/buy")
              {
               resetBot();
               signal = SIGNAL_BUY;
               SendMessage(chat.m_id, actionMessage + "Received BUY order");
              }

            if(text=="/sell")
              {
               resetBot();
               signal = SIGNAL_SELL;
               SendMessage(chat.m_id, actionMessage + "Received SELL order");
              }

            if(text=="/close")
              {
               signal = SIGNAL_CLOSE;
               SendMessage(chat.m_id, actionMessage + "Received CLOSE order");
              }

            if(text=="/auto")
              {
               resetBot();
               signal = SIGNAL_AUTO;
               SendMessage(chat.m_id, actionMessage + "Received AUTO order");
              }

            if(text=="/reset")
              {
               resetBot();
               SendMessage(chat.m_id, actionMessage + "Received RESET order");
              }

            if(checkOrderPattern(text))
              {
               resetBot();
               setBotByPattern(text);
               SendMessage(chat.m_id, actionMessage + StringFormat("Received S:%d, Lot:%g, Dist:%g, Max:%g, InitC:%g", bot.signal, bot.lotsize, bot.distance, bot.maxDistance, bot.initCaptital));
               // signal = 0;
              }
            if(checkCalculatePattern(text))
              {
               SendMessage(chat.m_id, getCalculatedLotSize(text));
              }
           }
        }
     }
  };


input group  "==== DCA Trading ==="
input bool    DCA_Strategy_Backtest = false; // DCA Strategy Test
enum ENUM_DCA_ORDER_TYPE {DCA_BUY, DCA_SELL};
input ENUM_DCA_ORDER_TYPE DCA_Strategy_OrderType = DCA_BUY; // DCA Order Type
input double  DCA_Strategy_LotSize = 0.01; // Lot Size
input int     DCA_Strategy_MaxDistance = 10; // Number of L that can be reached
input double  DCA_Strategy_Distance = 25; // Distance between L in points (10 pts=1 pip)
input ulong   DCA_Strategy_MagicNumber = 01; // Magic Number

input group  "==== No trade time ==="
input bool    TradingStop_Active = false; // Trading Stop active (GMT+2(w),+3(s))
input int     TradingStop_HourStart = 0; // Stop Hour Start
input int     TradingStop_HourStop = 1; // Stop Hour Stop
input bool    TradingStop_CloseAllPositions = true; // Close all positions before Stop Hour

input group  "==== No trade on News ==="
input bool    TradingStopNews_Active = true; // Trading Stop on News
input int     TradingStopNews_CloseResumeMinutes = 30; // How long in min trades will be deactivated and activated

input group  "==== Trading Stop Daily ==="
input bool    TradingStopDaily_Active = true; // Trading Stop Daily

input group  "==== General Stoploss ==="
input bool     Stoploss_Active = true; // Stoploss active
input double   StopLoss_CloseAllPositions      = -1200; // Stoploss to close all positions ($)
input double   StopLoss_DefaultPoints          = 5000; // Default Stoploss points

input group  "==== Telegram Credentials ==="
input string ChatID = ""; // Chat ID
input string BotToken = ""; // Bot Token
input bool TestTelegram = false; // Test Telegram Signal

input group  "==== Stoploss Warning ==="
input double InitLoss = -100; // Initial Warning Loss (in $)
input double LossDistance = 100; // Distance Loss (InitLoss + distance = next warning)

input group  "==== Indicators Warning ==="
input bool Warning_Rsi = false; // Rsi Warning?
input bool Warning_Cci = false; // CCi Warning?

input group  "==== DCA Analysis ==="
input bool   DCA_Matrix = false; // Show DCA Matrix
input double DCA_MinNumOfDistance = 1; // Number of min Level
input double DCA_MaxNumOfDistance = 10; // Number of max Level
input int    DCA_LotSize_Multiply = 2; // Lotsize multiply

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
KhanhdeuxBot bot;
int getmeResult;
double loss;
int numOfPositions;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {
   Print("=== INIT === ", TimeCurrent());

   loss = InitLoss;
   numOfPositions = PositionsTotal();

   if(ChatID != "" && BotToken != "")
     {
      if(TestTelegram)
        {
         SendMessage("test", ChatID, BotToken);
        }

      //--- set token
      bot.Token(BotToken);
      //--- check token
      getmeResult=bot.GetMe();
     }

   EventSetTimer(10);

   trade.SetExpertMagicNumber(DCA_Strategy_MagicNumber);

   if(DCA_Matrix)
     {
      showDCAMatrix();
     }

   if(DCA_Strategy_Backtest)
     {
      news.SaveHistory(true);
      news.LoadHistory(true);
     }
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//--- destroy timer
   EventKillTimer();
   bot.signal = 0;
   CRegex::ClearCache();
   loss = InitLoss;
   positionMagics.Clear();
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
   if(Stoploss_Active)
     {
      if(TotalProfit() < StopLoss_CloseAllPositions)
        {
         CloseAllPositions();
         Print(StringFormat("Stoploss exeeded = %G", StopLoss_CloseAllPositions));
        }
     }

   if(ChatID != "" && BotToken != "")
     {
      SendPositionsChanged();
     }

   static datetime bar_time=0;
   datetime this_bar_time=iTime(_Symbol,PERIOD_M1,0);

   if(bar_time!=this_bar_time)
     {
      bar_time=this_bar_time;

      if(ChatID != "" && BotToken != "")
        {
         SendMessageToStoplossWarning();

         if(Warning_Rsi)
           {
            SendMessageToRsi();
           }

         if(Warning_Cci)
           {
            SendMessageToCci();
           }
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void testPattern()
  {
   string pattern= signalPattern;
   string in="/buy 0.01,25,4";

   CRegex *r=new CRegex(pattern,RegexOptions::IgnoreCase);
   CMatch *m=r.Match(in);

   if(m.Success())
     {
      CMatchCollection *matches=CRegex::Matches(in,pattern);
      CMatchEnumerator *en=matches.GetEnumerator();
      while(en.MoveNext())
        {
         CMatch *match=en.Current();
         Print("1: " + match.Groups()[1].Value());
         Print("2: " + match.Groups()[2].Value());
         Print("3: " + match.Groups()[3].Value());
         Print("4: " + match.Groups()[4].Value());
         Print("5: " + match.Groups()[5].Value());
         Print("\0");
        }

      delete en;
      delete matches;
     }

   delete r;
   delete m;
  }

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer()
  {
   ExecuteDCATrading();
   executeTelegramBot();
// testPattern();
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CNews news;
bool checkStopNews()
  {
   int USD_COUNTRY_ID = 840;
   datetime currentTime = TimeCurrent(); // + 16 * 60 * 60; // TimeCurrent()
   int timeToStopResumeTrading = TradingStopNews_CloseResumeMinutes * 60;

   if(DCA_Strategy_Backtest)
     {
      int totalnews = ArraySize(news.event);
      for(int i=0; i<totalnews; i++)
        {
         if(news.event[i].country_id == USD_COUNTRY_ID
            && news.event[i].importance >=3
            && (news.event[i].time - timeToStopResumeTrading) < currentTime && currentTime < (news.event[i].time + timeToStopResumeTrading))
           {
            Print(news.event[i].country_id," ",
                  news.eventname[i]," ",
                  news.event[i].sector," ",
                  news.event[i].time," ",
                  news.event[i].timemode," ",
                  news.event[i].event_type," ",
                  news.event[i].importance
                 );
            return true;
           }
        }
     }
   else
     {
      MqlCalendarEvent event;    //for saving the events importance and country code
      MqlCalendarValue values[];  //for saving the events time and id

      datetime dateFrom=TimeCurrent();  // take all events from
      datetime dateTo=TimeCurrent() + timeToStopResumeTrading + 2 * 60 * 60;     // take all events to
      datetime currentDate = StringToTime(TimeToString(TimeCurrent(), TIME_DATE));

      if(CalendarValueHistory(values,currentDate,0))   //get all the events
        {
         for(int i=0; i<ArraySize(values); i++)
           {
            //get the event  info
            if(CalendarEventById(values[i].event_id,event))
              {
               // if is important enought and time based
               if(event.country_id == USD_COUNTRY_ID
                  && event.importance>=3
                  && (values[i].time - timeToStopResumeTrading) < currentTime && currentTime < (values[i].time + timeToStopResumeTrading))
                 {
                  Print(event.country_id," ",
                        event.name," ",
                        event.importance," ",
                        values[i].time,"");
                  return true;
                 }
              }
           }
        }

     }
   return false;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool checkStopDaily()
  {
   datetime currentTime = TimeCurrent();
   datetime nextDay = StringToTime(TimeToString(TimeCurrent(), TIME_DATE)) + 24 * 60 * 60;
   int timeRange = 5 * 60;

   if((nextDay - timeRange) < currentTime && currentTime < nextDay + timeRange)
     {
      return true;
     }

   return false;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void executeTelegramBot()
  {
   if(ChatID != "" && BotToken != "")
     {
      //--- show error message end exit
      if(getmeResult!=0)
        {
         Comment("Error: ",GetErrorDescription(getmeResult));
         return;
        }

      //--- show bot name
      Comment("Bot:", bot.Name());
      //--- reading messages
      bot.GetUpdates();
      //--- processing messages
      bot.ProcessMessages();
     }

   if(bot.signal == SIGNAL_CLOSE)
     {
      closeAllDCAPositions();
      bot.signal = 0;
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double TotalProfit()
  {
   double profit = 0;
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      ulong ticket = PositionGetTicket(i);
      string positionSymbol=PositionGetSymbol(i);

      if(ticket > 0 && positionSymbol == _Symbol)
        {
         profit += PositionGetDouble(POSITION_PROFIT);
        }
     }

   return profit;
  }

//+------------------------------------------------------------------+
//| Close all positions                                              |
//+------------------------------------------------------------------+
void CloseAllPositions()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(position.SelectByIndex(i))
        {
         if(position.Symbol() == _Symbol)
           {
            trade.PositionClose(position.Ticket());
           }
        }
     }
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void showDCAMatrix()
  {
   double initLotSize = 0.01 * DCA_LotSize_Multiply;

   for(double k=initLotSize; k<=initLotSize * 10; k=k+initLotSize)
     {
      Print("Lot Size:" + DoubleToString(k, 2));

      for(int i = 1; i <= DCA_MaxNumOfDistance; i++)
        {
         // Calculate TotalVolume
         double totalLotSize = calculateTotalVolume(i, k);

         // Calculate MaxLoss
         double maxLoss = calculateMaxLoss(i, k, DCA_Strategy_Distance);

         // Calculate MaxProfit
         double maxProfit = calculateMaxProfit(i, k, DCA_Strategy_Distance);

         if(maxLoss >= MathAbs(StopLoss_CloseAllPositions))
           {
            break;
           }

         if(i < DCA_MinNumOfDistance)
           {
            continue;
           }

         Print(StringFormat("L%d___Total Volume:[%g], Max Loss:[-%g($)], Max Profit:[%g($)]", i, totalLotSize, maxLoss, maxProfit));
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getBestLotSize(double level, double distance, double initCapital)
  {
   double initLotSize = 0.01 * DCA_LotSize_Multiply;
   double bestLotSize = 0;

   for(double k=initLotSize; k<=initLotSize * 10; k=k+initLotSize)
     {
      for(int i = 1; i <= level; i++)
        {

         // Calculate MaxLoss
         double maxLoss = calculateMaxLoss(i, k, distance);

         // Calculate MaxProfit
         double maxProfit = calculateMaxProfit(i, k, distance);

         if(maxLoss >= initCapital)
           {
            break;
           }

         if(i < level)
           {
            continue;
           }

         bestLotSize = k;
        }
     }

   return bestLotSize ? bestLotSize : DCA_Strategy_LotSize;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateTotalVolume(int i, double lotSize)
  {
   double totalLotSize = 0;
   double currentLotSize = lotSize;

   for(int j=1; j<=i; j++)
     {
      totalLotSize += currentLotSize;
      currentLotSize *=2;
     }

   return totalLotSize;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateMaxLoss(int i, double lotSize, double distance)
  {
   double totalLoss = 0;
   double currentLotSize = lotSize;
   double pips = distance/10;

   for(int j=1; j<=i; j++)
     {
      totalLoss += currentLotSize * pips * AccountInfoInteger(ACCOUNT_LEVERAGE) * (i - j + 1);
      currentLotSize *=2;
     }

   return totalLoss;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateMaxProfit(int i, double lotSize, double distance)
  {
   double totalProfit = 0;
   double pips = distance/10;
   double currentLotSize = lotSize;

   double takeProfitPrice = 0;
   double priceVolumeSum = 0;

   for(int j=1; j<=i; j++)
     {
      totalProfit += pips * currentLotSize * AccountInfoInteger(ACCOUNT_LEVERAGE);
      currentLotSize *=2;
     }

   return totalProfit;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void ExecuteDCATrading()
  {
   int totalPositions = getTotalDCAPositions();

   if(TradingStopNews_Active)
     {
      if(checkStopNews())
        {
         CloseAllPositions();
         return;
        }
     }

   if(TradingStopDaily_Active)
     {
      if(checkStopDaily())
        {
         CloseAllPositions();
         return;
        }
     }

   if(TradingStop_Active)
     {
      if(checkTradingStopHour())
        {
         if(TradingStop_CloseAllPositions && totalPositions > 0)
           {
            Print(StringFormat("Trading Stop Hour close all %d positions", totalPositions));
            closeAllDCAPositions();
           }
         return;
        }
     }

   if(DCA_Strategy_Backtest == false && bot.signal == 0 && totalPositions == 0)
     {
      return;
     }

   if(totalPositions == getDCAMaxDistance() && getNextDCASignal())
     {
      closeAllDCAPositions();
      return;
     }

   if(getTotalDCAOrders() > 0)
     {
      return;
     }


   if(totalPositions == getDCAMaxDistance())
     {
      return;
     }

   int signal = 0;
   if(totalPositions == 0)
     {
      int botSignal = getBotSignal();
      if(MathAbs(botSignal) == 1)
        {
         signal = botSignal;
        }
     }
   else
     {
      signal = getNextDCASignal();
     }

   if(signal)
     {
      int newTotalPositions = totalPositions + 1;

      double lotSize =  calculateDCALotSize(newTotalPositions);
      double price = getCurrentPrice();
      double tp = calculateDCATakeProfit(lotSize, price);

      updateAllDCAPositionTakeProfit(tp);

      if(signal == SIGNAL_BUY)
        {
         double sl = NormalizeDouble(SymbolInfoDouble(_Symbol, SYMBOL_BID) - StopLoss_DefaultPoints*_Point,_Digits);
         Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, price, sl, tp, "BUY:" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions));
         trade.Buy(lotSize, _Symbol, price, sl, tp, "BUY:" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions);
        }

      if(signal == SIGNAL_SELL)
        {
         double sl = NormalizeDouble(SymbolInfoDouble(_Symbol, SYMBOL_ASK) + StopLoss_DefaultPoints*_Point,_Digits);
         Print(StringFormat("%g, P:%g, SL:%g, TP:%g, %s", lotSize, price, sl, tp, "SELL:" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions));
         trade.Sell(lotSize, _Symbol, price, sl, tp, "SELL:" + DCA_Strategy_MagicNumber +  "|L" + newTotalPositions);
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getTotalDCAPositions()
  {
   int count = 0;
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i) && PositionGetSymbol(i) == _Symbol && position.Magic() == DCA_Strategy_MagicNumber)
        {
         count++;
        }
     }

   return count;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getTotalDCAOrders()
  {
   int count = 0;
   for(int i = OrdersTotal() - 1; i >= 0; i--)
     {
      if(OrderSelect(OrderGetTicket(i)))
        {
         string symbol = OrderGetString(ORDER_SYMBOL);
         long magicNumber = OrderGetInteger(ORDER_MAGIC);

         if(symbol == _Symbol && magicNumber == DCA_Strategy_MagicNumber)
           {
            count++;
           }
        }
     }

   return count;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getRsiSignal()
  {
   double rsi[];
   int rsiHandle = iRSI(_Symbol, PERIOD_M1, 14, PRICE_CLOSE);
   int oversold = 30;
   int overbought = 70;
   int signal = 0;
   CopyBuffer(rsiHandle, 0, 0, 3, rsi);

   if(rsi[0] < oversold && rsi[1] < oversold && rsi[2] >= oversold)
     {
      signal = SIGNAL_BUY;
     }

   if(rsi[0] > overbought && rsi[1] > overbought && rsi[2] <= overbought)
     {
      signal = SIGNAL_SELL;
     }

   ArrayFree(rsi);
   return signal;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int   getNextDCASignal()
  {
   int signal = 0;
   ENUM_DCA_ORDER_TYPE currentOrderType = getCurrentOrderType();
   double lastPrice = getLastDCAPrice();
   double pips = getDCADistance()/10;
   double currentPrice = getCurrentPrice();

   if(currentOrderType == DCA_BUY)
     {
      if(currentPrice <= (lastPrice - pips))
        {
         signal = SIGNAL_BUY;
        }
     }

   if(currentOrderType == DCA_SELL)
     {
      if(currentPrice >= (lastPrice + pips))
        {
         signal = SIGNAL_SELL;
        }
     }

   return signal;
  }

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateDCATakeProfit(double lotSize, double price)
  {
   double priceVolumeSum = 0;
   double totalVolume = 0.0;
   double pips = getDCADistance()/10;

   ENUM_DCA_ORDER_TYPE currentOrderType = getCurrentOrderType();

   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i) && PositionGetSymbol(i) == _Symbol && position.Magic() == DCA_Strategy_MagicNumber)
        {
         priceVolumeSum += currentOrderType == DCA_BUY ? (position.PriceOpen() + pips) * position.Volume() : (position.PriceOpen() - pips) * position.Volume();
         totalVolume += position.Volume();
        }
     }

   priceVolumeSum += currentOrderType == DCA_BUY ? (price + pips) * lotSize : (price - pips) * lotSize;
   totalVolume += lotSize;

   return priceVolumeSum / totalVolume;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateDCALotSize(int i)
  {
   if(i == 1)
     {
      return getDCALotSize();
     }
   return calculateDCALotSize(i - 1) * 2;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getCurrentPrice()
  {
   double currentPrice = SymbolInfoDouble(_Symbol, SYMBOL_LAST);
   ENUM_DCA_ORDER_TYPE currentOrderType = getCurrentOrderType();

   if(currentOrderType == DCA_BUY)
     {
      currentPrice = SymbolInfoDouble(_Symbol, SYMBOL_BID);
     }
   else
      if(currentOrderType == DCA_SELL)
        {
         currentPrice = SymbolInfoDouble(_Symbol, SYMBOL_ASK);
        }

   return currentPrice;
  }

//+------------------------------------------------------------------+
ENUM_DCA_ORDER_TYPE getCurrentOrderType()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i) && PositionGetSymbol(i) == _Symbol && position.Magic() == DCA_Strategy_MagicNumber)
        {
         int positionType = PositionGetInteger(POSITION_TYPE);
         if(positionType == POSITION_TYPE_BUY)
            return DCA_BUY;
         if(positionType == POSITION_TYPE_SELL)
            return DCA_SELL;
        }
     }

   int botSignal = getBotSignal();
   if(MathAbs(botSignal) == 1)
     {
      return botSignal == SIGNAL_BUY ? DCA_BUY : DCA_SELL;
     }

   return NULL;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getDCALotSize()
  {
   if(bot.lotsize)
     {
      return bot.lotsize;
     }
   return DCA_Strategy_LotSize;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getDCAMaxDistance()
  {
   if(bot.maxDistance)
     {
      return bot.maxDistance;
     }
   return DCA_Strategy_MaxDistance;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getDCADistance()
  {
   if(bot.distance)
     {
      return bot.distance;
     }
   return DCA_Strategy_Distance;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getBotSignal()
  {
   if(bot.signal == SIGNAL_AUTO || DCA_Strategy_Backtest)
     {
      return getRsiSignal();
     }

   return bot.signal;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getLastDCAPrice()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i) && PositionGetSymbol(i) == _Symbol && position.Magic() == DCA_Strategy_MagicNumber)
        {
         return PositionGetDouble(POSITION_PRICE_OPEN);
        }
     }

   return 0;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void updateAllDCAPositionTakeProfit(double tpPrice)
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i) && PositionGetSymbol(i) == _Symbol && position.Magic() == DCA_Strategy_MagicNumber)
        {
         double sl = position.StopLoss();
         double tp = tpPrice;

         trade.PositionModify(position.Ticket(),sl,tp);
         Print(StringFormat("Position with lotsize=%g updated TakeProfit= %g", PositionGetDouble(POSITION_VOLUME), tpPrice));
        }
     }
  }
//+------------------------------------------------------------------+
void closeAllDCAPositions()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      if(PositionGetTicket(i) && PositionGetSymbol(i) == _Symbol && position.Magic() == DCA_Strategy_MagicNumber)
        {
         trade.PositionClose(position.Ticket());
         Print(StringFormat("Close Position with lotsize=%g", PositionGetDouble(POSITION_VOLUME)));
        }
     }
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
bool checkTradingStopHour()
  {
   MqlDateTime time1;
   TimeToStruct(TimeCurrent(),time1);
   int hour=time1.hour;

   if(TradingStop_HourStart == TradingStop_HourStop)
     {
      if(hour == TradingStop_HourStart)
        {
         return true;
        }
     }

   if(TradingStop_HourStart < TradingStop_HourStop)
     {
      if(TradingStop_HourStart <= hour && hour < TradingStop_HourStop)
        {
         return true;
        }
     }

   if(TradingStop_HourStart > TradingStop_HourStop)
     {
      if(TradingStop_HourStart <= hour || hour < TradingStop_HourStop)
        {
         return true;
        }
     }

   return false;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SendMessageToRsi()
  {
   double rsi[];
   int rsiHandle = iRSI(_Symbol, PERIOD_M1, 14, PRICE_CLOSE);
   int oversold = 30;
   int overbought = 70;
   bool signal = false;

   CopyBuffer(rsiHandle, 0, 0, 3, rsi);

   string message = "SIGNAL!!!" + "%0A";

   if(rsi[0] < oversold && rsi[1] < oversold && rsi[2] >= oversold)
     {
      message += "BUY: RSI crossover " + StringFormat("%d", oversold);
      signal = true;
     }

   if(rsi[0] > overbought && rsi[1] > overbought && rsi[2] <= overbought)
     {
      message += "SELL: RSI crossunder " + StringFormat("%d", overbought);
      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }

   ArrayFree(rsi);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SendMessageToCci()
  {
   double cci[];
   int cciHanle = iCCI(_Symbol, PERIOD_M1, 14, PRICE_TYPICAL);
   int upper = 100;
   int lower = -100;
   bool signal = false;

   CopyBuffer(cciHanle, 0, 0, 2, cci);

   string message = "SIGNAL!!!" + "%0A";

   if(cci[0] < lower && cci[1] >= lower)
     {
      message += "BUY: CCI crossover " + StringFormat("%d", lower);
      signal = true;
     }


   if(cci[0] > upper && cci[1] <= upper)
     {
      message += "BUY: CCI crossunder " + StringFormat("%d", upper);
      signal = true;
     }

   if(signal)
     {
      printf(message);
      SendMessage(message, ChatID, BotToken);
     }

   ArrayFree(cci);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void initPositionMagics()
  {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
     {
      ulong ticket = PositionGetTicket(i);
      if(ticket > 0)
        {
         long magic = position.Magic();
         bool found = false;
         for(int j=0; j< positionMagics.Total(); j++)
           {
            if(magic == positionMagics[j])
              {
               found = true;
              }
           }

         if(!found)
           {
            positionMagics.Add(magic);
           }
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SendMessageToStoplossWarning()
  {
   double pL = TotalProfit();

   if(pL <= loss)
     {
      string message = "WARNING!!!" + "%0A";
      message       += StringFormat("%s %d (1:%I64d)", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN), AccountInfoInteger(ACCOUNT_LEVERAGE)) + "%0A";
      message       += "Current Loss: " + StringFormat("%g ", pL) + " below " + StringFormat("%g ", loss) + "%0A%0A";

      initPositionMagics();

      for(int i=0; i<positionMagics.Total(); i++)
        {
         long magic = positionMagics[i];
         string magicMessage = "========================= %0A";
         magicMessage += StringFormat("Magic number: %d", magic) +  "%0A";
         int count = 0;
         for(int j = PositionsTotal() - 1; j >= 0; j--)
           {
            ulong ticket = PositionGetTicket(j);
            long currentMagic = position.Magic();

            if(ticket >0 && magic == currentMagic)
              {
               count +=1;
              }
           }

         magicMessage += StringFormat("L%d ", count);
         magicMessage += "%0A ========================= %0A";
         message += magicMessage;
        }


      printf(message);
      SendMessage(message, ChatID, BotToken);
      loss -= LossDistance;
      positionMagics.Clear();
     }
   else
     {
      if(pL > loss + LossDistance * 1.5 && pL < InitLoss)
        {
         loss = loss + LossDistance;
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SendPositionsChanged()
  {
   int total = PositionsTotal();

   if(numOfPositions != total)
     {
      string message = "NOTICE!" + "";
      message       += "%0A" + StringFormat("%s %d (1:%I64d)", AccountInfoString(ACCOUNT_SERVER), (int)AccountInfoInteger(ACCOUNT_LOGIN), AccountInfoInteger(ACCOUNT_LEVERAGE)) + "%0A";
      message       += "[PO]" + StringFormat("%d", numOfPositions) + "-" + StringFormat("%d ", total) + "";
      message       += StringFormat("[BAL]%G",AccountInfoDouble(ACCOUNT_BALANCE)) + "";
      message       += StringFormat("[PROF]%G",AccountInfoDouble(ACCOUNT_PROFIT)) + "";
      message       += StringFormat("[EQU]%G",AccountInfoDouble(ACCOUNT_EQUITY)) + "";

      numOfPositions = PositionsTotal();

      printf(message);
      SendMessage(message, ChatID, BotToken);
     }
  }
//+------------------------------------------------------------------+
int SendMessage(string text, string chatID, string botToken)
  {
   string baseUrl = "https://api.telegram.org";
   string headers = "";
   string requestURL = "";
   string requestHeaders = "";
   char resultData[];
   char posData[];
   int timeout = 2000;

   requestURL = StringFormat("%s/bot%s/sendmessage?chat_id=%s&text=%s", baseUrl, botToken, chatID, text);

   int response = WebRequest("POST", requestURL, headers, timeout, posData, resultData, requestHeaders);

   string resultMessage = CharArrayToString(resultData);
   Print(resultMessage);

   return response;
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
