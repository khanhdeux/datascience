//+------------------------------------------------------------------+
//|                                                daylight_test.mq5 |
//|                                         Copyright © 2018,Amr Ali |
//|                             https://www.mql5.com/en/users/amrali |
//+------------------------------------------------------------------+
#property copyright "Copyright © 2018,Amr Ali"
#property link      "https://www.mql5.com/en/users/amrali"
#property version   "1.000"

#include "daylight.mqh"
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnStart()
  {
   datetime dst_start,dst_end;
   dst_start=dst_end=0;

//--- In the European Union, Summer Time begins on the last Sunday in
//--- March and ends on the last Sunday in October.                    |

   Print("========= European Summer Time (DST) =========");
   for(int year=2010; year<=2029; year++)
     {
      DST_Europe(year,dst_start,dst_end);
      Print("DST starts on ",dst_start," and ends on ",dst_end);
     }

//--- In the United States, Summer Time begins on the second Sunday in |
//--- March and ends on the first Sunday in November.                  |

   Print("========= American Summer Time (DST) =========");
   for(int year=2010; year<=2029; year++)
     {
      DST_USA(year,dst_start,dst_end);
      Print("DST starts on ",dst_start," and ends on ",dst_end);
     }
  }
//+------------------------------------------------------------------+

/*
output:
	========= European Summer Time (DST) =========
	DST starts on 2010.03.28 01:00:00 and ends on 2010.10.31 02:00:00
	DST starts on 2011.03.27 01:00:00 and ends on 2011.10.30 02:00:00
	DST starts on 2012.03.25 01:00:00 and ends on 2012.10.28 02:00:00
	DST starts on 2013.03.31 01:00:00 and ends on 2013.10.27 02:00:00
	DST starts on 2014.03.30 01:00:00 and ends on 2014.10.26 02:00:00
	DST starts on 2015.03.29 01:00:00 and ends on 2015.10.25 02:00:00
	DST starts on 2016.03.27 01:00:00 and ends on 2016.10.30 02:00:00
	DST starts on 2017.03.26 01:00:00 and ends on 2017.10.29 02:00:00
	DST starts on 2018.03.25 01:00:00 and ends on 2018.10.28 02:00:00
	DST starts on 2019.03.31 01:00:00 and ends on 2019.10.27 02:00:00
	DST starts on 2020.03.29 01:00:00 and ends on 2020.10.25 02:00:00
	DST starts on 2021.03.28 01:00:00 and ends on 2021.10.31 02:00:00
	DST starts on 2022.03.27 01:00:00 and ends on 2022.10.30 02:00:00
	DST starts on 2023.03.26 01:00:00 and ends on 2023.10.29 02:00:00
	DST starts on 2024.03.31 01:00:00 and ends on 2024.10.27 02:00:00
	DST starts on 2025.03.30 01:00:00 and ends on 2025.10.26 02:00:00
	DST starts on 2026.03.29 01:00:00 and ends on 2026.10.25 02:00:00
	DST starts on 2027.03.28 01:00:00 and ends on 2027.10.31 02:00:00
	DST starts on 2028.03.26 01:00:00 and ends on 2028.10.29 02:00:00
	DST starts on 2029.03.25 01:00:00 and ends on 2029.10.28 02:00:00
	========= American Summer Time (DST) =========
	DST starts on 2010.03.14 02:00:00 and ends on 2010.11.07 02:00:00
	DST starts on 2011.03.13 02:00:00 and ends on 2011.11.06 02:00:00
	DST starts on 2012.03.11 02:00:00 and ends on 2012.11.04 02:00:00
	DST starts on 2013.03.10 02:00:00 and ends on 2013.11.03 02:00:00
	DST starts on 2014.03.09 02:00:00 and ends on 2014.11.02 02:00:00
	DST starts on 2015.03.08 02:00:00 and ends on 2015.11.01 02:00:00
	DST starts on 2016.03.13 02:00:00 and ends on 2016.11.06 02:00:00
	DST starts on 2017.03.12 02:00:00 and ends on 2017.11.05 02:00:00
	DST starts on 2018.03.11 02:00:00 and ends on 2018.11.04 02:00:00
	DST starts on 2019.03.10 02:00:00 and ends on 2019.11.03 02:00:00
	DST starts on 2020.03.08 02:00:00 and ends on 2020.11.01 02:00:00
	DST starts on 2021.03.14 02:00:00 and ends on 2021.11.07 02:00:00
	DST starts on 2022.03.13 02:00:00 and ends on 2022.11.06 02:00:00
	DST starts on 2023.03.12 02:00:00 and ends on 2023.11.05 02:00:00
	DST starts on 2024.03.10 02:00:00 and ends on 2024.11.03 02:00:00
	DST starts on 2025.03.09 02:00:00 and ends on 2025.11.02 02:00:00
	DST starts on 2026.03.08 02:00:00 and ends on 2026.11.01 02:00:00
	DST starts on 2027.03.14 02:00:00 and ends on 2027.11.07 02:00:00
	DST starts on 2028.03.12 02:00:00 and ends on 2028.11.05 02:00:00
	DST starts on 2029.03.11 02:00:00 and ends on 2029.11.04 02:00:00
*/
