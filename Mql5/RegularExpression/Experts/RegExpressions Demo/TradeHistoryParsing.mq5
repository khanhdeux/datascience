//+------------------------------------------------------------------+
//|                                          TradeHistoryParsing.mq5 |
//|                   Copyright 2009-2013, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property strict                           
input string   file_name;
#include "TableListView.mqh"
#include <Arrays\Array.mqh>
#include <Controls\Dialog.mqh>
#include <Controls\RadioGroup.mqh>
#include <Controls\ComboBox.mqh>
#include <Controls\Button.mqh>
#include <RegularExpressions\Regex.mqh>
#property version "1.1"
//+------------------------------------------------------------------+
//| Base class from oreder record and dael record.                   |
//+------------------------------------------------------------------+
class Record : public IComparable
  {
private:
   static string     s_value;
   static int        s_index;
protected:
   string            m_data[];
public:
   //--- Methods:
   //+------------------------------------------------------------------+
   //| Operator ([]).                                                   |
   //+------------------------------------------------------------------+
   string operator[](const int i)
     {
      //--- return value;
      return (i>=0 && i<ArraySize(m_data) ? m_data[i] : NULL);
     }
   //+------------------------------------------------------------------+
   //| Sets the value to be found in current column.                    |
   //+------------------------------------------------------------------+   
   static void SetValue(const string value)
     {
      s_value=value;
     }
   //+------------------------------------------------------------------+
   //| Sets the index of current column.                                |
   //+------------------------------------------------------------------+      
   static void SetIndex(const int index)
     {
      s_index=index;
     }
   //+------------------------------------------------------------------+
   //| Compares the value with the current value.                       |
   //+------------------------------------------------------------------+     
   static bool FindRecord(IComparable *value)
     {
      Record *record=dynamic_cast<Record*>(value);
      if(s_index>=ArraySize(record.m_data) || s_index<0)
        {
         Print("Iindex out of range.");
         //--- return false
         return(false);
        }
      //--- return result
      return (record.m_data[s_index] == s_value);
     }
   //+------------------------------------------------------------------+
   //| Copies this data into array.                                     |
   //+------------------------------------------------------------------+
   void CopyTo(string &array[])
     {
      ArrayCopy(array,m_data,0,0,WHOLE_ARRAY);
     }
   //+------------------------------------------------------------------+
   //| Returns a string that represents the current record.             |
   //+------------------------------------------------------------------+
   string ToString()
     {
      string result;
      for(int i=0; i<ArraySize(m_data); i++)
        {
         result+=m_data[i]+"\t";
        }
      //--- return string
      return (result);
     }
  };
static string Record::s_value = NULL;
static int    Record::s_index = NULL;
//+------------------------------------------------------------------+
//| Represent one record from orders table.                          |
//+------------------------------------------------------------------+
class OrderRecord : public Record
  {
public:
   //--- Constrictor:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+   
                     OrderRecord(const string &data[])
     {
      if(ArraySize(data)==11)
        {
         ArrayCopy(m_data,data);
        }
      else
        {
         ZeroMemory(m_data);
        }
     }
   //--- Methods:
   //+------------------------------------------------------------------+
   //| Gets the value of column "Open Time".                            |
   //+------------------------------------------------------------------+   
   datetime OpenTime() { return StringToTime(m_data[0]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "Order".                                |
   //+------------------------------------------------------------------+   
   long Order() { return StringToInteger(m_data[1]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "Symbol".                               |
   //+------------------------------------------------------------------+   
   string Symbol() { return(m_data[2]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "Type".                                 |
   //+------------------------------------------------------------------+   
   string Type() { return(m_data[3]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "Type".                                 |
   //+------------------------------------------------------------------+   
   string Volume() { return(m_data[4]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "Price".                                |
   //+------------------------------------------------------------------+   
   string Price() { return(m_data[5]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "S / L".                                |
   //+------------------------------------------------------------------+   
   string SL() { return(m_data[6]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "T / P".                                |
   //+------------------------------------------------------------------+   
   string TP() { return(m_data[7]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "Time".                                 |
   //+------------------------------------------------------------------+   
   datetime Time() { return StringToTime(m_data[8]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "State".                                |
   //+------------------------------------------------------------------+   
   string State() { return(m_data[9]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "Comment".                              |
   //+------------------------------------------------------------------+   
   string Comment() { return(m_data[10]); }
  };
//+------------------------------------------------------------------+
//| Represent one record from deals table.                           |
//+------------------------------------------------------------------+
class DealRecord : public Record
  {
public:
   //--- Constrictor:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+  
                     DealRecord(const string &data[])
     {
      if(ArraySize(data)==13)
        {
         ArrayCopy(m_data,data);
        }
      else
        {
         ZeroMemory(m_data);
        }
     }
   //--- Methods:
   //+------------------------------------------------------------------+
   //| Gets the value of column "Open Time".                            |
   //+------------------------------------------------------------------+   
   datetime Time() { return StringToTime(m_data[0]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "Deal".                                 |
   //+------------------------------------------------------------------+   
   long Deal() { return StringToInteger(m_data[1]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "Symbol".                               |
   //+------------------------------------------------------------------+   
   string Symbol() { return(m_data[2]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "Type".                                 |
   //+------------------------------------------------------------------+   
   string Type() { return(m_data[3]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "Direction".                            |
   //+------------------------------------------------------------------+   
   string Direction() { return(m_data[4]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "Volume".                               |
   //+------------------------------------------------------------------+   
   string Volume() { return(m_data[5]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "Price".                                |
   //+------------------------------------------------------------------+   
   double Price() { StringReplace(m_data[6]," ",""); return StringToDouble(m_data[6]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "Order".                                |
   //+------------------------------------------------------------------+   
   long Order() { return StringToInteger(m_data[7]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "Commission".                           |
   //+------------------------------------------------------------------+   
   double Commissions() { StringReplace(m_data[8]," ",""); return StringToDouble(m_data[8]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "Swap".                                 |
   //+------------------------------------------------------------------+   
   double Swap() { StringReplace(m_data[9]," ",""); return StringToDouble(m_data[9]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "Profit".                               |
   //+------------------------------------------------------------------+   
   double Profit() { StringReplace(m_data[10]," ",""); return StringToDouble(m_data[10]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "Balance".                              |
   //+------------------------------------------------------------------+   
   double Balance() { StringReplace(m_data[11]," ",""); return StringToDouble(m_data[11]); }
   //+------------------------------------------------------------------+
   //| Gets the value of column "Comment".                              |
   //+------------------------------------------------------------------+   
   string Comment() { return(m_data[12]); }
  };
//+------------------------------------------------------------------+
//| Container for deals table and orders table.                      |
//+------------------------------------------------------------------+
class TradeHistory
  {
private:
   string            m_file_name;
   List<OrderRecord*>*m_list1;
   List<DealRecord*>*m_list2;
   int               m_handel;
public:
   //--- Constructor:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+  
                     TradeHistory(const string path)
     {
      m_file_name=path;
      m_handel= FileOpen(path,FILE_READ|FILE_TXT);
      m_list1 = new List<OrderRecord*>();
      m_list2 = new List<DealRecord*>();
      CRegex *rgx=new CRegex("(>)([^<>]*)(<)");
      while(!FileIsEnding(m_handel))
        {
         string str=FileReadString(m_handel);
         CMatchCollection *matches=rgx.Matches(str);
         if(matches.Count()==23)
           {
            string in[11];
            for(int i=0,j=1; i<11; i++,j+=2)
              {
               in[i]=StringSubstr(matches[j].Value(),1,StringLen(matches[j].Value())-2);
              }
            m_list1.Add(new OrderRecord(in));
           }
         else if(matches.Count()==27)
           {
            string in[13];
            for(int i=0,j=1; i<13; i++,j+=2)
              {
               in[i]=StringSubstr(matches[j].Value(),1,StringLen(matches[j].Value())-2);
              }
            m_list2.Add(new DealRecord(in));
           }
         delete matches;
        }
      FileClose(m_handel);
      delete rgx;
      CRegex::ClearCache();
     }
   //--- Destructor:
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+  
                    ~TradeHistory()
     {
      for(int i=0; i<m_list1.Count(); i++)
        {
         delete m_list1[i];
        }
      for(int i=0; i<m_list2.Count(); i++)
        {
         delete m_list2[i];
        }
      delete m_list1;
      delete m_list2;
     }
   //--- Methods:
   //+------------------------------------------------------------------+
   //| Gets the file name.                                              |
   //+------------------------------------------------------------------+
   string FileName()
     {
      //--- return file name
      return (m_file_name);
     }
   //+------------------------------------------------------------------+
   //| Gets the list of order records.                                  |
   //+------------------------------------------------------------------+
   List<OrderRecord*>*GetOrderRecordList() const
     {
      //--- return list
      return(m_list1);
     }
   //+------------------------------------------------------------------+
   //| Gets the list of deal records.                                   |
   //+------------------------------------------------------------------+
   List<DealRecord*>*GetDealRecordList() const
     {
      //--- return list
      return(m_list2);
     }
   //+------------------------------------------------------------------+
   //| Retrieves all the oredrs that match the conditions defined       |
   //| parameters.                                                      |
   //+------------------------------------------------------------------+   
   template<typename T>
   List<OrderRecord*>*FindAllOrders(const int columnIndex,const T value)
     {
      Record::SetValue((string)value);
      Record::SetIndex(columnIndex);
      //--- return result
      return (m_list1.FindAll(Record::FindRecord));
     }
   //+------------------------------------------------------------------+
   //| Retrieves all the deals that match the conditions defined        |
   //| parameters.                                                      |
   //+------------------------------------------------------------------+
   template<typename T>
   List<DealRecord*>*FindAllDeals(const int columnIndex,const T value)
     {
      Record::SetValue((string)value);
      Record::SetIndex(columnIndex);
      //--- return result
      return (m_list2.FindAll(Record::FindRecord));
     }
  };
//+------------------------------------------------------------------+
//| Orders result.                                                   |
//+------------------------------------------------------------------+
struct OrdersResult
  {
public:
   int               m_prices;
   double            m_max_price;
   double            m_min_price;
   double            m_expected_payoff_price;
   double            m_standard_deviation_price;
   //--- Constructors:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     OrdersResult(const int prices,const double max_price,const double min_price,
                                                    const double expected_payoff_price,const double standard_deviation_price)
     {
      m_prices=prices;
      m_max_price = max_price;
      m_min_price = min_price;
      m_expected_payoff_price=expected_payoff_price;
      m_standard_deviation_price=standard_deviation_price;
     }
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     OrdersResult(const OrdersResult &result)
     {
      m_prices=result.m_prices;
      m_max_price= result.m_max_price;
      m_min_price= result.m_min_price;
      m_expected_payoff_price=result.m_expected_payoff_price;
      m_standard_deviation_price=result.m_standard_deviation_price;
     }
  };
//+------------------------------------------------------------------+
//| Deals result.                                                    |
//+------------------------------------------------------------------+
struct DealsResult
  {
public:
   int               m_trades;
   double            m_gross_profit;
   double            m_gross_loss;
   double            m_expected_payoff;
   double            m_standard_deviation;
   int               m_profit_trades;
   int               m_loss_trades;
   double            m_best_trade;
   double            m_worst_trade;
   double            m_total_net_profit;
   double            m_average_profit;
   double            m_average_loss;
   //--- Constructors:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     DealsResult(const int trades,const int profit_trades,const int loss_trades,
                                                   const double best_trade,const double worst_trade,
                                                   const double gross_profit,const double gross_loss,const double total_net_profit,
                                                   const double average_profit,const double average_loss,
                                                   const double expected_payoff,const double standard_deviation)
     {
      m_trades=trades;
      m_gross_profit=gross_profit;
      m_gross_loss=gross_loss;
      m_expected_payoff=expected_payoff;
      m_standard_deviation=standard_deviation;
      m_profit_trades=profit_trades;
      m_loss_trades=loss_trades;
      m_best_trade=best_trade;
      m_worst_trade=worst_trade;
      m_total_net_profit=total_net_profit;
      m_average_profit=average_profit;
      m_average_loss=average_loss;
     }
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     DealsResult(const DealsResult &result)
     {
      m_trades=result.m_trades;
      m_gross_profit=result.m_gross_profit;
      m_gross_loss=result.m_gross_loss;
      m_expected_payoff=result.m_expected_payoff;
      m_standard_deviation=result.m_standard_deviation;
      m_profit_trades=result.m_profit_trades;
      m_loss_trades=result.m_loss_trades;
      m_best_trade=result.m_best_trade;
      m_worst_trade=result.m_worst_trade;
      m_total_net_profit=result.m_total_net_profit;
      m_average_profit=result.m_average_profit;
      m_average_loss=result.m_average_loss;
     }
  };
//+------------------------------------------------------------------+
//| TradeHistoryView.                                                |
//+------------------------------------------------------------------+
class TradeHistoryView : public CAppDialog
  {
private:
   int               m_column_index;
   string            m_column_value;
   CEdit             m_file_name;
   TradeHistory     *m_history;
   CComboBox         m_combo_box_column;
   CComboBox         m_combo_box_value;
   CEdit             m_text1;
   CEdit             m_text2;
   CEdit             m_text3;
   CRadioGroup       m_radio_group;
   CEdit             m_orders_headers[11];
   CEdit             m_deals_headers[13];
   CTableListView    m_orders_table;
   CTableListView    m_deals_table;
   CButton           m_button_find;
   CButton           m_button_save;
   List<OrderRecord*>*m_orders;
   List<DealRecord*>*m_deals;
   CEdit             m_text4;
   //--- Deadls table result
   CEdit             m_deals_results[12];
   CEdit             m_deals_results_value[12];
   //--- Orders table result
   CEdit             m_orders_results[5];
   CEdit             m_orders_results_value[5];
protected:
   //--- handlers of the dependent controls events
   //+------------------------------------------------------------------+
   //| Event handler                                                    |
   //+------------------------------------------------------------------+
   void OnChangeComboBoxValue()
     {
      m_column_value=m_combo_box_value.Select();
     }
   //+------------------------------------------------------------------+
   //| Event handler                                                    |
   //+------------------------------------------------------------------+
   void OnChangeComboBoxColumn()
     {
      m_combo_box_value.ItemsClear();
      int j=(int)m_combo_box_column.Value();
      m_column_index=j;
      if(CheckPointer(m_history)!=POINTER_INVALID)
        {
         if(m_deals_table.IsVisible())
           {
            List<DealRecord*>*list=m_history.GetDealRecordList();
            if(CheckPointer(list)!=POINTER_INVALID)
              {
               for(int i=0; i<list.Count(); i++)
                 {
                  string value=list[i][j];
                  if(!m_combo_box_value.SelectByText(value))
                    {
                     m_combo_box_value.AddItem(value,i);
                    }
                 }
              }
           }
         if(m_orders_table.IsVisible())
           {
            List<OrderRecord*>*list=m_history.GetOrderRecordList();
            if(CheckPointer(list)!=POINTER_INVALID)
              {
               for(int i=0; i<list.Count(); i++)
                 {
                  string value=list[i][j];
                  if(!m_combo_box_value.SelectByText(value))
                    {
                     m_combo_box_value.AddItem(value,i);
                    }
                 }
              }
           }
        }
     }
   //+------------------------------------------------------------------+
   //| Event handler                                                    |
   //+------------------------------------------------------------------+
   void OnChangeRadioGroup()
     {
      m_deals_table.Visible(false);
      m_orders_table.Visible(false);
      while(m_deals_table.ItemDelete(0));
      while(m_orders_table.ItemDelete(0));
      for(int i=0; i<12; i++)
        {
         m_deals_results[i].Visible(false);
         m_deals_results_value[i].Visible(false);
        }
      for(int i=0; i<5; i++)
        {
         m_orders_results[i].Visible(false);
         m_orders_results_value[i].Visible(false);
        }
      if(m_radio_group.Value()==1)
        {
         m_radio_group.Value(1);
         ShowOredersTable();
         m_orders_table.VScrolled(true);
        }
      if(m_radio_group.Value()==2)
        {
         ShowDealsTable();
         m_deals_table.VScrolled(true);
        }
      FillComboBoxes();
      m_combo_box_column.Select(0);
      OnChangeComboBoxColumn();
      m_combo_box_value.Select(0);
     }
   //+------------------------------------------------------------------+
   //| Event handler                                                    |
   //+------------------------------------------------------------------+
   void OnClickButtonFind()
     {
      int j=(int)m_combo_box_column.Value();
      m_column_index=j;
      m_column_value=m_combo_box_value.Select();
      if(m_column_value==" \0")
        {
         m_column_value="";
        }
      while(m_orders_table.ItemDelete(0));
      while(m_deals_table.ItemDelete(0));
      if(m_orders_table.IsVisible())
        {
         long values[11];
         if(CheckPointer(m_orders)==POINTER_DYNAMIC)
           {
            delete m_orders;
           }
         m_orders=m_history.FindAllOrders(m_column_index,m_column_value);
         if(CheckPointer(m_orders)!=POINTER_INVALID)
           {
            string items[];
            for(int i=0; i<m_orders.Count(); i++)
              {
               m_orders[i].CopyTo(items);
               m_orders_table.ItemAdd(items,values);
              }
            for(int i=0; i<11; i++)
              {
               m_orders_table.TextAlign(i,ALIGN_RIGHT);
              }
           }
         m_orders_table.VScrolled(true);
         for(int i=0; i<12; i++)
           {
            m_deals_results[i].Visible(false);
            m_deals_results_value[i].Visible(false);
           }
         for(int i=0; i<5; i++)
           {
            m_orders_results[i].Visible(true);
            m_orders_results_value[i].Visible(true);
           }
         RefreshOrdersResult(m_orders);
        }
      if(m_deals_table.IsVisible())
        {
         long values[13];
         if(CheckPointer(m_deals)==POINTER_DYNAMIC)
           {
            delete m_deals;
           }
         m_deals=m_history.FindAllDeals(m_column_index,m_column_value);
         if(CheckPointer(m_deals)!=POINTER_INVALID)
           {
            string items[];
            for(int i=0; i<m_deals.Count(); i++)
              {
               m_deals[i].CopyTo(items);
               m_deals_table.AddItem(items,values);
              }
            for(int i=0; i<13; i++)
              {
               m_deals_table.TextAlign(i,ALIGN_RIGHT);
              }
           }
         m_deals_table.VScrolled(true);
         for(int i=0; i<5; i++)
           {
            m_orders_results[i].Visible(false);
            m_orders_results_value[i].Visible(false);
           }
         for(int i=0; i<12; i++)
           {
            m_deals_results[i].Visible(true);
            m_deals_results_value[i].Visible(true);
           }
         RefreshDealsResult(m_deals);
        }
      ZeroMemory(m_column_value);
      Print("Search completed.");
     }
   //+------------------------------------------------------------------+
   //| Event handler                                                    |
   //+------------------------------------------------------------------+ 
   void OnClickButtonSave()
     {
      int handel=FileOpen("Result.csv",FILE_WRITE|FILE_TXT);
      string header;
      string record;
      if(m_deals_table.IsVisible())
        {
         for(int i=0; i<ArraySize(m_deals_headers); i++)
           {
            header+=m_deals_headers[i].Text()+"\t";
           }
         FileWrite(handel,header);
         for(int i=0; i<m_deals.Count(); i++)
           {
            record=m_deals[i].ToString();
            FileWrite(handel,record);
           }
        }
      if(m_orders_table.IsVisible())
        {
         for(int i=0; i<ArraySize(m_orders_headers); i++)
           {
            header+=m_orders_headers[i].Text()+"\t";
           }
         FileWrite(handel,header);
         for(int i=0; i<m_orders.Count(); i++)
           {
            record=m_orders[i].ToString();
            FileWrite(handel,record);
           }
        }
      FileClose(handel);
      Print("Saving completed.");
     }
   //+------------------------------------------------------------------+
   //| Event Handling                                                   |
   //+------------------------------------------------------------------+
   EVENT_MAP_BEGIN(TradeHistoryView)
   ON_EVENT(ON_CLICK,m_button_find,OnClickButtonFind)
   ON_EVENT(ON_CLICK,m_button_save,OnClickButtonSave)
   ON_EVENT(ON_CHANGE,m_radio_group,OnChangeRadioGroup)
   ON_EVENT(ON_CHANGE,m_combo_box_column,OnChangeComboBoxColumn)
   ON_EVENT(ON_CHANGE,m_combo_box_value,OnChangeComboBoxValue)
   EVENT_MAP_END(CAppDialog)
public:
   //--- Constructors:
   //+------------------------------------------------------------------+
   //| Constructor without parameters.                                  |
   //+------------------------------------------------------------------+
                     TradeHistoryView()
     {
     }
   //--- Destructors:
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~TradeHistoryView()
     {
      if(CheckPointer(m_orders)==POINTER_DYNAMIC)
        {
         delete m_orders;
        }
      if(CheckPointer(m_deals)==POINTER_DYNAMIC)
        {
         delete m_deals;
        }
     }
   //--- Methods:
   //+------------------------------------------------------------------+
   //| Sets the trade history.                                          |
   //+------------------------------------------------------------------+
   void SetHistory(TradeHistory *value)
     {
      m_history=value;
     }
   //+------------------------------------------------------------------+
   //| Gets the trade history.                                          |
   //+------------------------------------------------------------------+
   TradeHistory *GetHistory()
     {
      //--- retirn trade history
      return (m_history);
     }
   //+------------------------------------------------------------------+
   //| Creat a control.                                                 |
   //+------------------------------------------------------------------+
   bool Create()
     {
      CAppDialog::Create(0,"Trade History",0,10,10,1360,600);
      CreateFileName();
      CreateTables();
      CreateComboBoxes();
      CreateRadioGroup();
      CreateButtons();
      CreateHeaderResult();
      CreateDealsResult();
      CreateOrdersResult();
      DefaultView();
      return (true);
     }
   //+------------------------------------------------------------------+
   //| Create file name.                                                |
   //+------------------------------------------------------------------+
   bool CreateFileName()
     {
      //--- coordinates
      int x1 = 10;
      int y1 = 10;
      int x2 = 300;
      int y2 = 30;
      //--- create file name
      m_file_name.Create(0,"File_Name",0,x1,y1,x2,y2);
      m_file_name.Text("File name: "+m_history.FileName());
      m_file_name.ReadOnly(true);
      m_file_name.ColorBackground(CONTROLS_DIALOG_COLOR_CLIENT_BG);
      m_file_name.ColorBorder(CONTROLS_DIALOG_COLOR_CLIENT_BG);
      m_file_name.FontSize(11);
      Add(m_file_name);
      //--- return result
      return (true);
     }
   //+------------------------------------------------------------------+
   //| Create a tables.                                                 |
   //+------------------------------------------------------------------+
   bool CreateTables()
     {
      string text_headers1[11]=
        {
         "Open Time",
         "Order",
         "Symbol",
         "Type",
         "Volume",
         "Price",
         "S / L",
         "T / P",
         "Time",
         "State",
         "Comment",
        };
      string text_headers2[13]=
        {
         "Time",
         "Deal",
         "Symbol",
         "Type",
         "Direction",
         "Volume",
         "Price",
         "Order",
         "Commission",
         "Swap",
         "Profit",
         "Balance",
         "Comment",
        };
      ushort sizes1[11]=
        {
         130,  // Open Time 
         100,  // Order
         100,  // Symbol
         100,  // Type
         130,  // Volume
         130,  // Price
         85,   // S / L
         85,   // T / P
         130,  // Time
         120,  // State
         180,  // Commemt 
        };
      ushort sizes2[13]=
        {
         130,  // Time 
         100,  // Deal
         100,  // Symbol
         100,  // Type
         65,   // Direction
         65,   // Volume
         130,  // Price
         85,   // Order
         85,   // Commission
         65,   // Swap
         65,   // Profit
         120,  // Balance
         180,  // Commemt
        };
      //--- coordinates
      int x1 = 10;
      int y1 = 150;
      int x2 = 1330;
      int y2 = 400;
      //--- create table 1
      m_orders_table.Create(0,"Table1",0,x1,y1,x2,y2,11,sizes1);
      m_orders_table.VScrolled(true);
      m_orders_table.Visible(false);
      Add(m_orders_table);
      //---  create table 2
      m_deals_table.Create(0,"Table2",0,x1,y1,x2,y2,13,sizes2);
      m_deals_table.VScrolled(true);
      m_deals_table.Visible(false);
      Add(m_deals_table);
      //--- coordinates
      x1=10;
      y1=130;
      y2=150;
      //--- create headers 1  
      for(int i=0; i<11; i++)
        {
         x2=(i!=10) ? x1+sizes1[i]: x1+sizes1[i]+10;
         m_orders_headers[i].Create(0,"Headers1"+IntegerToString(i),0,x1,y1,x2,y2);
         m_orders_headers[i].ReadOnly(true);
         m_orders_headers[i].Text(text_headers1[i]);
         m_orders_headers[i].ColorBackground(clrDarkGray);
         m_orders_headers[i].TextAlign(ALIGN_CENTER);
         m_orders_headers[i].Visible(false);
         Add(m_orders_headers[i]);
         x1=x2;
        }
      //--- coordinates
      x1=10;
      //--- create headers 2
      for(int i=0; i<13; i++)
        {
         x2=(i!=12) ? x1+sizes2[i]: x1+sizes2[i]+10;
         m_deals_headers[i].Create(0,"Headers2"+IntegerToString(i),0,x1,y1,x2,y2);
         m_deals_headers[i].ReadOnly(true);
         m_deals_headers[i].Text(text_headers2[i]);
         m_deals_headers[i].ColorBackground(clrDarkGray);
         m_deals_headers[i].TextAlign(ALIGN_CENTER);
         m_deals_headers[i].Visible(false);
         Add(m_deals_headers[i]);
         x1=x2;
        }
      //--- return result  
      return (true);
     }
   //+------------------------------------------------------------------+
   //| Cereate a combo box controls.                                    |
   //+------------------------------------------------------------------+
   bool CreateComboBoxes()
     {
      //--- coordinates
      int x1;
      int x2;
      int y1=80;
      int y2=120;
      //--- create text 2
      x1=300;
      x2=550;
      m_text2.Create(0,"Text2",0,x1,y1-40,x2,y1-10);
      m_text2.Text("Select the column:");
      m_text2.ColorBorder(CONTROLS_DIALOG_COLOR_CLIENT_BG);
      m_text2.ColorBackground(CONTROLS_DIALOG_COLOR_CLIENT_BG);
      m_text2.FontSize(10);
      m_text2.ReadOnly(true);
      Add(m_text2);
      //--- create combo box 1
      m_combo_box_column.Create(0,"Column",0,x1,y1+10,x2,y2-10);
      Add(m_combo_box_column);
      //--- create text 3
      x1=600;
      x2=850;
      m_text3.Create(0,"Text3",0,x1,y1-40,x2,y1-10);
      m_text3.Text("Select the value:");
      m_text3.ColorBorder(CONTROLS_DIALOG_COLOR_CLIENT_BG);
      m_text3.ColorBackground(CONTROLS_DIALOG_COLOR_CLIENT_BG);
      m_text3.FontSize(10);
      m_text3.ReadOnly(true);
      Add(m_text3);
      //--- create combo box 2
      m_combo_box_value.Create(0,"Value",0,x1,y1+10,x2,y2-10);
      Add(m_combo_box_value);
      //--- return result
      return (true);
     }
   //+------------------------------------------------------------------+
   //| Create a radio group control.                                    |
   //+------------------------------------------------------------------+     
   bool CreateRadioGroup()
     {
      //--- coordinates
      int x1=10;
      int y1=80;
      int x2=150;
      int y2=120;
      //--- create text 1
      m_text1.Create(0,"Text1",0,x1,y1-40,200,y1-10);
      m_text1.Text("Select the table:");
      m_text1.ColorBorder(CONTROLS_DIALOG_COLOR_CLIENT_BG);
      m_text1.ColorBackground(CONTROLS_DIALOG_COLOR_CLIENT_BG);
      m_text1.FontSize(10);
      m_text1.ReadOnly(true);
      Add(m_text1);
      //--- create
      if(!m_radio_group.Create(0,m_name+"RadioGroup",0,x1,y1,x2,y2))
         return(false);
      if(!Add(m_radio_group))
         return(false);
      //--- fill out with strings
      if(!m_radio_group.AddItem("Orders tabe",1))
         return(false);
      if(!m_radio_group.AddItem("Deals table",2))
         return(false);
      m_radio_group.ColorBorder(clrDarkGray);
      //--- return result
      return(true);
     }
   //+------------------------------------------------------------------+
   //| Create a button controls.                                        |
   //+------------------------------------------------------------------+
   bool CreateButtons()
     {
      //--- coordinates
      int x1;
      int y1=60;
      int x2;
      int y2=80;
      //--- create button 1
      x1=1000;
      x2=1100;
      m_button_find.Create(0,"Button_Find",0,x1,y1,x2,y2);
      m_button_find.Text("Find");
      Add(m_button_find);
      //--- create button 2
      x1=1200;
      x2=1300;
      m_button_save.Create(0,"Button_Save",0,x1,y1,x2,y2);
      m_button_save.Text("Save");
      Add(m_button_save);
      //--- return result
      return (true);
     }
   //+------------------------------------------------------------------+
   //| Create header result.                                            |
   //+------------------------------------------------------------------+
   bool CreateHeaderResult()
     {
      //--- coordinates
      int x1= this.Width()/2-200;
      int y1=400;
      int x2= this.Width()/2;
      int y2=420;
      //--- create text 4
      m_text4.Create(0,"Text4",0,x1,y1,x2,y2);
      m_text4.ReadOnly(true);
      m_text4.ColorBorder(CONTROLS_DIALOG_COLOR_CLIENT_BG);
      m_text4.ColorBackground(CONTROLS_DIALOG_COLOR_CLIENT_BG);
      m_text4.TextAlign(ALIGN_CENTER);
      m_text4.Text("Statistics");
      m_text4.FontSize(13);
      Add(m_text4);
      //--- return result
      return (true);
     }
   //+------------------------------------------------------------------+
   //| Create deals table result.                                       |
   //+------------------------------------------------------------------+
   bool CreateDealsResult()
     {
      //--- coordinates
      int x1= 70;
      int y1= 430;
      int x2=370;
      int y2=450;
      //--- create results
      for(int i=0; i<ArraySize(m_deals_results);i++)
        {
         m_deals_results_value[i].Create(0,"Result_Deals_Value"+IntegerToString(i),0,x2+10,y1,x2+200,y2);
         m_deals_results_value[i].ColorBorder(CONTROLS_DIALOG_COLOR_CLIENT_BG);
         m_deals_results_value[i].ColorBackground(CONTROLS_DIALOG_COLOR_CLIENT_BG);
         m_deals_results_value[i].ReadOnly(true);
         m_deals_results_value[i].TextAlign(ALIGN_LEFT);
         m_deals_results_value[i].Text("");
         m_deals_results_value[i].FontSize(11);
         m_deals_results[i].Create(0,"Result_Deals"+IntegerToString(i),0,x1,y1,x2,y2);
         m_deals_results[i].ColorBorder(CONTROLS_DIALOG_COLOR_CLIENT_BG);
         m_deals_results[i].ColorBackground(CONTROLS_DIALOG_COLOR_CLIENT_BG);
         m_deals_results[i].ReadOnly(true);
         m_deals_results[i].TextAlign(ALIGN_RIGHT);
         m_deals_results[i].Text("Value:");
         m_deals_results[i].FontSize(11);
         Add(m_deals_results[i]);
         Add(m_deals_results_value[i]);
         y1 = y2;
         y2 = y2 + 20;
         if(i==5)
           {
            x1=this.Width()/2;
            y1=430;
            x2=x1+300;
            y2=450;
           }
        }
      //--- return result
      return (true);
     }
   //+------------------------------------------------------------------+
   //| Create orders table result.                                      |
   //+------------------------------------------------------------------+
   bool CreateOrdersResult()
     {
      //--- coordinates
      int x1= 70;
      int y1= 430;
      int x2=370;
      int y2=450;
      //--- create results
      for(int i=0; i<ArraySize(m_orders_results);i++)
        {
         m_orders_results_value[i].Create(0,"Result_Orders_Value"+IntegerToString(i),0,x2+10,y1,x2+200,y2);
         m_orders_results_value[i].ColorBorder(CONTROLS_DIALOG_COLOR_CLIENT_BG);
         m_orders_results_value[i].ColorBackground(CONTROLS_DIALOG_COLOR_CLIENT_BG);
         m_orders_results_value[i].ReadOnly(true);
         m_orders_results_value[i].TextAlign(ALIGN_LEFT);
         m_orders_results_value[i].Text("");
         m_orders_results_value[i].FontSize(11);
         m_orders_results_value[i].Visible(false);
         m_orders_results[i].Create(0,"Result_Orders"+IntegerToString(i),0,x1,y1,x2,y2);
         m_orders_results[i].ColorBorder(CONTROLS_DIALOG_COLOR_CLIENT_BG);
         m_orders_results[i].ColorBackground(CONTROLS_DIALOG_COLOR_CLIENT_BG);
         m_orders_results[i].ReadOnly(true);
         m_orders_results[i].TextAlign(ALIGN_RIGHT);
         m_orders_results[i].Text("");
         m_orders_results[i].FontSize(11);
         m_orders_results[i].Visible(false);
         Add(m_orders_results[i]);
         Add(m_orders_results_value[i]);
         y1 = y2;
         y2 = y2 + 20;
         if(i==2)
           {
            x1=this.Width()/2;
            y1=450;
            x2=x1+300;
            y2=470;
           }
        }
      //--- return result
      return (true);
     }
   //+------------------------------------------------------------------+
   //| Show order table.                                                |
   //+------------------------------------------------------------------+
   void ShowOredersTable()
     {
      m_deals_table.Visible(false);
      m_orders_table.VScrolled(true);
      m_orders_table.Visible(true);
      for(int i=0; i<11; i++)
        {
         m_orders_headers[i].Visible(true);
        }
      for(int i=0; i<13; i++)
        {
         m_deals_headers[i].Visible(false);
        }
     }
   //+------------------------------------------------------------------+
   //| Show deals table.                                                |
   //+------------------------------------------------------------------+
   void ShowDealsTable()
     {
      m_orders_table.Visible(false);
      m_deals_table.VScrolled(true);
      m_deals_table.Visible(true);
      for(int i=0; i<13; i++)
        {
         m_deals_headers[i].Visible(true);
        }
      for(int i=0; i<11; i++)
        {
         m_orders_headers[i].Visible(false);
        }
     }
   //+------------------------------------------------------------------+
   //| Fill the combo box constrols.                                    |
   //+------------------------------------------------------------------+
   void FillComboBoxes()
     {
      if(m_deals_table.IsVisible())
        {
         m_combo_box_column.ItemsClear();
         m_combo_box_value.ItemsClear();
         for(int i=0; i<ArraySize(m_deals_headers); i++)
           {
            m_combo_box_column.AddItem(m_deals_headers[i].Text(),i);
           }
        }
      if(m_orders_table.IsVisible())
        {
         m_combo_box_column.ItemsClear();
         m_combo_box_value.ItemsClear();
         for(int i=0; i<ArraySize(m_orders_headers); i++)
           {
            m_combo_box_column.AddItem(m_orders_headers[i].Text(),i);
           }
        }
     }
   //+------------------------------------------------------------------+
   //| Sets deals result.                                               |
   //+------------------------------------------------------------------+
   void SetOrdersResult(OrdersResult &result)
     {
      string names[5]=
        {
         "Total Prices:",
         "Maximum Price:",
         "Minimum Price:",
         "Expected Value Of Price:",
         "Standard Deviation Of Price:"
        };
      for(int i=0; i<5; i++)
        {
         m_orders_results[i].Text(names[i]);
        }
      m_orders_results_value[0].Text(IntegerToString(result.m_prices));
      m_orders_results_value[1].Text(DoubleToString(result.m_max_price,3));
      m_orders_results_value[2].Text(DoubleToString(result.m_min_price,3));
      m_orders_results_value[3].Text(DoubleToString(result.m_expected_payoff_price,3));
      m_orders_results_value[4].Text(DoubleToString(result.m_standard_deviation_price,3));
     }
   //+------------------------------------------------------------------+
   //| Sets deals result.                                               |
   //+------------------------------------------------------------------+
   void SetDealsResult(DealsResult &result)
     {
      string names[12]=
        {
         "Total Trades:",
         "Total Net Profit:",
         "Gross Profit:",
         "Gross Loss:",
         "Profit Traders:",
         "Loss Traders:",
         //--- column 2         
         "Best Trade:",
         "Worst Trade:",
         "Average Profit:",
         "Average Loss:",
         "Expected Payoff:",
         "Standard Deviation:",
        };
      for(int i=0; i<12; i++)
        {
         m_deals_results[i].Text(names[i]);
        }
      m_deals_results_value[0].Text(IntegerToString(result.m_trades));
      m_deals_results_value[1].Text(DoubleToString(result.m_total_net_profit,3));
      m_deals_results_value[2].Text(DoubleToString(result.m_gross_profit,3));
      m_deals_results_value[3].Text(DoubleToString(result.m_gross_loss,3));
      m_deals_results_value[4].Text(IntegerToString(result.m_profit_trades));
      m_deals_results_value[5].Text(IntegerToString(result.m_loss_trades));
      m_deals_results_value[6].Text(DoubleToString(result.m_best_trade,3));
      m_deals_results_value[7].Text(DoubleToString(result.m_worst_trade,3));
      m_deals_results_value[8].Text(DoubleToString(result.m_average_profit,3));
      m_deals_results_value[9].Text(DoubleToString(result.m_average_loss,3));
      m_deals_results_value[10].Text(DoubleToString(result.m_expected_payoff,3));
      m_deals_results_value[11].Text(DoubleToString(result.m_standard_deviation,3));
     }
   //+------------------------------------------------------------------+
   //| Default view of table.                                           |
   //+------------------------------------------------------------------+
   void DefaultView()
     {
      m_column_index=2;
      m_column_value="";
      int iv=0;
      List<DealRecord*>*list=m_history.GetDealRecordList();
      if(CheckPointer(list)!=POINTER_INVALID)
        {
         for(int i=0; i<list.Count(); i++)
           {
            string value=list[i][2];
            if(!m_combo_box_value.SelectByText(value))
              {
               m_combo_box_value.AddItem(value,i);
              }
            if(value!="")
              {
               m_column_value=value;
               iv=i;
              }
           }
        }
      if(m_column_value==" \0")
        {
         m_column_value="";
        }
      m_radio_group.Value(2);
      ShowDealsTable();
      long values[13];
      if(CheckPointer(m_deals)==POINTER_DYNAMIC)
        {
         delete m_deals;
        }
      m_deals=m_history.FindAllDeals(m_column_index,m_column_value);
      if(CheckPointer(m_deals)!=POINTER_INVALID)
        {
         string items[];
         for(int i=0; i<m_deals.Count(); i++)
           {
            m_deals[i].CopyTo(items);
            m_deals_table.AddItem(items,values);
           }
         for(int i=0; i<13; i++)
           {
            m_deals_table.TextAlign(i,ALIGN_RIGHT);
           }
        }
      m_deals_table.VScrolled(true);
      for(int i=0; i<5; i++)
        {
         m_orders_results[i].Visible(false);
        }
      for(int i=0; i<12; i++)
        {
         m_deals_results[i].Visible(true);
        }
      RefreshDealsResult(m_deals);
      ZeroMemory(m_column_value);
      for(int i=0; i<ArraySize(m_orders_headers); i++)
        {
         m_combo_box_column.AddItem(m_orders_headers[i].Text(),i);
        }
      m_combo_box_column.Select(2);
      m_combo_box_value.Select(iv);
     }
public:
   //+------------------------------------------------------------------+
   //| Create result from orders.                                       |
   //+------------------------------------------------------------------+
   void RefreshOrdersResult(List<OrderRecord*>*orders)
     {
      //--- create list prices
      List<double>prices();
      for(int i=0; i<orders.Count(); i++)
        {
         string price=orders[i].Price();
         StringReplace(price," ","");
         double value_price=StringToDouble(price);
         prices.Add(value_price);
        }
      if(prices.Count()==0)
        {
         OrdersResult result(0,0,0,0,0);
         SetOrdersResult(result);
         //--- return result
         return;
        }
      double max_price = prices[0];
      double min_price = prices[0];
      double expected_payoff=0;
      for(int i=0; i<prices.Count(); i++)
        {
         expected_payoff+=prices[i];
         if(prices[i]>max_price)
           {
            max_price=prices[i];
           }
         if(prices[i]<min_price)
           {
            min_price=prices[i];
           }
        }
      expected_payoff=expected_payoff/prices.Count();
      double standard_deviation=0;
      for(int i=0; i<prices.Count(); i++)
        {
         standard_deviation+=MathPow(prices[i]-expected_payoff,2);
        }
      standard_deviation=(prices.Count()==1) ? 0 : standard_deviation/(prices.Count()-1);
      OrdersResult result(prices.Count(),max_price,min_price,expected_payoff,standard_deviation);
      SetOrdersResult(result);
     }
   //+------------------------------------------------------------------+
   //| Create result from deals.                                        |
   //+------------------------------------------------------------------+
   void RefreshDealsResult(List<DealRecord*>*deals)
     {
      //--- create list trades
      List<double>trades();
      for(int i=0; i<deals.Count(); i++)
        {
         string type=deals[i].Type();
         double profit=deals[i].Profit()+deals[i].Commissions()+deals[i].Swap();
         if((type=="sell" || type=="buy") && profit!=0)
           {
            trades.Add(profit);
           }
        }
      //--- calculate results
      int profit_trades=0;
      int loss_trades=0;
      double best_trade=0.0;
      double worst_trade=0.0;
      double gross_profit=0.0;
      double gross_loss=0.0;
      for(int i=0; i<trades.Count(); i++)
        {
         double value=trades[i];
         if(value>0)
           {
            profit_trades++;
            gross_profit+=value;
            if(best_trade<value)
              {
               best_trade=value;
              }
           }
         if(value<0)
           {
            gross_loss+=value;
            loss_trades++;
            if(worst_trade>value)
              {
               worst_trade=value;
              }
           }
        }
      //--- calculate Total Net Profit
      double total_net_profit=gross_profit+gross_loss;
      //--- calculate Expected Payoff
      double expected_payoff=(trades.Count()==0) ? 0 : total_net_profit/trades.Count();
      //--- calculate Standard Deviation
      double standard_deviation=0;
      for(int i=0; i<trades.Count(); i++)
        {
         standard_deviation+=MathPow(trades[i]-expected_payoff,2);
        }
      standard_deviation=(trades.Count()==1) ? 0 : standard_deviation/(trades.Count()-1);
      standard_deviation= MathSqrt(standard_deviation);
      //--- calculare Average Profit and Average Loss
      double average_profit=(trades.Count()==0 || profit_trades==0) ? 0 : gross_profit/profit_trades;
      double average_loss=(trades.Count()==0 || loss_trades==0) ? 0 : gross_loss/loss_trades;
      DealsResult result(trades.Count(),profit_trades,loss_trades,
                         best_trade,worst_trade,gross_profit,gross_loss,
                         total_net_profit,average_profit,average_loss,expected_payoff,standard_deviation);
      SetDealsResult(result);
     }
  };
//+------------------------------------------------------------------+
//| Global Variables                                                 |
//+------------------------------------------------------------------+
TradeHistoryView TradeView;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {  
   TradeHistory *history=new TradeHistory(file_name);
//--- Set trade history in trade history view
   Print("Reading a trading history is finished.");
   TradeView.SetHistory(history);
//--- create application dialog
   if(!TradeView.Create())
      return(INIT_FAILED);
//--- run application
   TradeView.Run();
//--- succeed
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//--- delete trade history
   delete TradeView.GetHistory();
//--- destroy dialog
   TradeView.Destroy(reason);
  }
//+------------------------------------------------------------------+
//| Expert chart event function                                      |
//+------------------------------------------------------------------+
void OnChartEvent(const int id,         // event ID  
                  const long& lparam,   // event parameter of the long type
                  const double& dparam, // event parameter of the double type
                  const string& sparam) // event parameter of the string type
  {
   TradeView.ChartEvent(id,lparam,dparam,sparam);
  }
//+------------------------------------------------------------------+
