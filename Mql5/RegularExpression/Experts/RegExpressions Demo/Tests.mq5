//+------------------------------------------------------------------+
//|                                                        Tests.mq5 |
//|                   Copyright 2009-2013, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property strict    
#include <Controls\Dialog.mqh>
#include <Controls\Button.mqh>
#include <Controls\Edit.mqh>
#include <Controls\ListView.mqh>
#include <Controls\Label.mqh>
#include <RegularExpressions\Regex.mqh>
#property version "1.1"
//+------------------------------------------------------------------+
//| Example 1.                                                       |
//+------------------------------------------------------------------+
void Example0_1()
  {
   string in="int values[] = { 1, 2, 3 };\n"+
             "for (int ctr = 0; ctr <= ArraySize(values); ctr++)\n"+
             "{\n"+
             "   Print(values[ctr]);\n"+
             "   if (ctr < ArraySize(values)-1)\n"+
             "      Print(\", \");\n"+
             "}\n"+
             "Print(\"\");\n";
   string pattern="Print(Line)?";
   CMatchCollection *matches=CRegex::Matches(in,pattern);
   CMatchEnumerator *en=matches.GetEnumerator();
   while(en.MoveNext())
     {
      CMatch *match=en.Current();
      PrintFormat("'%s' found in the source code at position %i.",
                  match.Value(),match.Index());
     }
   delete en;
   delete matches;
  }
//+------------------------------------------------------------------+
//| Example 2.                                                       |
//+------------------------------------------------------------------+
void Example0_2()
  {
   string in="int values[] = { 1, 2, 3 };\n"+
             "for (int ctr = 0; ctr <= ArraySize(values); ctr++)\n"+
             "{\n"+
             "   Print(values[ctr]);\n"+
             "   if (ctr < ArraySize(values)-1)\n"+
             "      Print(\", \");\n"+
             "}\n"+
             "Print(\"\");\n";
   string pattern="Print(Line)?";
   CMatch *match=CRegex::Match(in,pattern);
   while(match.Success())
     {
      PrintFormat("'%s' found in the source code at position %i.",
                  match.Value(),match.Index());
      CMatch *oldMatch=match;
      match=match.NextMatch();
      delete oldMatch;
     }
  }
//+------------------------------------------------------------------+
//| Example 1.                                                       |
//+------------------------------------------------------------------+
void Example1_1()
  {
   string pattern="(\\d{3})-(\\d{3}-\\d{4})";
   string in="212-555-6666 906-932-1111 415-222-3333 425-888-9999";
   CMatchCollection *matches=CRegex::Matches(in,pattern);
   CMatchEnumerator *en=matches.GetEnumerator();
   while(en.MoveNext())
     {
      CMatch *match=en.Current();
      Print("Area Code:        " + match.Groups()[1].Value());
      Print("Telephone number: " + match.Groups()[2].Value());
      Print("\0");
     }
   delete en;
   delete matches;
  }
//+------------------------------------------------------------------+
//| Example 2.                                                       |
//+------------------------------------------------------------------+
void Example1_2()
  {
   string text="One car red car blue car";
   string pat="(\\w+)\\s+(car)";
//--- Instantiate the regular expression object.
   CRegex *r=new CRegex(pat,RegexOptions::IgnoreCase);
//--- CMatch the regular expression pattern against a text string.
   CMatch *m=r.Match(text);
   int matchCount=0;
   while(m.Success())
     {
      Print("CMatch"+(IntegerToString(++matchCount)));
      for(int i=1; i<=2; i++)
        {
         CGroup *g=m.Groups()[i];
         Print("CGroup"+IntegerToString(i)+"='"+g.Value()+"'");
         CCaptureCollection *cc=g.Captures();
         for(int j=0; j<cc.Count(); j++)
           {
            CCapture *c=cc[j];
            Print("CCapture"+IntegerToString(j)+"='"+c.Value()+"', Position="+IntegerToString(c.Index()));
           }
        }
      CMatch *oldMatch=m;
      m=m.NextMatch();
      delete oldMatch;
     }
   delete r;
  }
//+------------------------------------------------------------------+
//| Example 1.                                                       |
//+------------------------------------------------------------------+
void Example2_1()
  {
   string pattern="--(.+?)--";
   string replacement="($1)";
   string in="He said--decisively--that the time--whatever time it was--had come.";
   CMatchCollection *matches=CRegex::Matches(in,pattern);
   CMatchEnumerator *en=matches.GetEnumerator();
   while(en.MoveNext())
     {
      CMatch *match=en.Current();
      string result=match.Result(replacement);
      Print(result);
     }
   delete en;
   delete matches;
//--- The example displays the following output:
//---       (decisively)
//---       (whatever time it was)
  }
//+------------------------------------------------------------------+
//| Example 1.                                                       |
//+------------------------------------------------------------------+
void Example3_1()
  {
   string pattern="\\b[at]\\w+";
   string text="The threaded application ate up the thread pool as it executed.";
   CMatchCollection *matches;
   CRegex *defaultRegex=new CRegex(pattern);
//--- Get matches of pattern in text
   matches=defaultRegex.Matches(text);
   PrintFormat("Parsing '%s'",text);
//--- Iterate matches
   for(int ctr=0; ctr<matches.Count(); ctr++)
     {
      PrintFormat("%i. %s",ctr,matches[ctr].Value());
     }
   delete defaultRegex;
   delete matches;
  }
//+------------------------------------------------------------------+
//| Example 2.                                                       |
//+------------------------------------------------------------------+
void Example3_2()
  {
   string pattern="\\b[at]\\w+";
   RegexOptions options=RegexOptions::IgnoreCase;
   string text="The threaded application ate up the thread pool as it executed.";
   CMatchCollection *matches;
   CRegex *optionRegex=new CRegex(pattern,options);
   PrintFormat("Parsing '%s'\n with options %i:",text,options);
//--- Get matches of pattern in text
   matches=optionRegex.Matches(text);
//--- Iterate matches
   for(int ctr=0; ctr<matches.Count(); ctr++)
     {
      PrintFormat("%i. %s",ctr,matches[ctr].Value());
     }
   delete optionRegex;
   delete matches;
  }
//+------------------------------------------------------------------+
//| Example 3.                                                       |
//+------------------------------------------------------------------+
void Example3_3()
  {
   string pattern="(a+)+$";    // DO NOT REUSE THIS PATTERN.
   CRegex *rgx=new CRegex(pattern,RegexOptions::IgnoreCase,TimeSpan::FromSeconds(1));
   string inputs[]=
     {
      "aa","aaaa>",
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
     };
   for(int i=0; i<ArraySize(inputs); i++)
     {
      string inputValue=inputs[i];
      PrintFormat("Processing %s",inputValue);
      bool timedOut=false;
      do
        {
         ulong start=GetMicrosecondCount();
         //--- Display the result.
         if(rgx.IsMatch(inputValue))
           {
            ulong end=GetMicrosecondCount();
            PrintFormat("Valid: '"+inputValue+"' ("+DoubleToString((end-start)/1000.0)+" ms)");
           }
         else
           {
            ulong end=GetMicrosecondCount();
            Print("'"+inputValue+"' is not a valid string. ("+DoubleToString((end-start)/1000.0)+" ms)");
           }
        }
      while(timedOut);
      Print("\0");
     }
   delete rgx;
  }
//+------------------------------------------------------------------+
//| Example 1.                                                       |
//+------------------------------------------------------------------+
void Example4_1()
  {
   string pattern="[(.*?)]";
   string in="The animal [what kind?] was visible [by whom?] from the window.";
   CMatchCollection *matches=CRegex::Matches(in,pattern);
   int commentNumber=0;
   PrintFormat("%s produces the following matches:",pattern);
   CMatchEnumerator *en=matches.GetEnumerator();
   while(en.MoveNext())
     {
      PrintFormat("   %i: %s",++commentNumber,en.Current().Value());
     }
   delete en;
   delete matches;
  }
//+------------------------------------------------------------------+
//| Example 2.                                                       |
//+------------------------------------------------------------------+
void Example4_2()
  {
   string pattern=CRegex::Escape("[")+"(.*?)]";
   string in="The animal [what kind?] was visible [by whom?] from the window.";
   CMatchCollection *matches=CRegex::Matches(in,pattern);
   int commentNumber=0;
   PrintFormat("%s produces the following matches:",pattern);
   CMatchEnumerator *en=matches.GetEnumerator();
   while(en.MoveNext())
     {
      PrintFormat("   %i: %s",++commentNumber,en.Current().Value());
     }
   delete en;
   delete matches;
  }
//+------------------------------------------------------------------+
//| Example 3.                                                       |
//+------------------------------------------------------------------+
void Example4_3()
  {
   char beginComment,endComment;
   Print("Begin comment symbol: "+CharToString(beginComment='['));
   Print("End comment symbol: "+CharToString(endComment=']'));
   string in="Text [comment comment comment] more text [comment]";
   string pattern;
   pattern=CRegex::Escape(CharToString(beginComment))+"(.*?)";
   string endPattern=CRegex::Escape(CharToString(endComment));
   if(endComment==']' || endComment=='}')
     {
      endPattern="\\"+endPattern;
     }
   pattern+=endPattern;
   CMatchCollection *matches=CRegex::Matches(in,pattern);
   Print(pattern);
   int commentNumber=0;
   CMatchEnumerator *en=matches.GetEnumerator();
   while(en.MoveNext())
     {
      CMatch *match=en.Current();
      PrintFormat("%i: %s",++commentNumber,match.Groups()[1].Value());
     }
   delete en;
   delete matches;
  }
//+------------------------------------------------------------------+
//| Example 1.                                                       |
//+------------------------------------------------------------------+
void Example5_1()
  {
//--- Define a regular expression for repeated words.
   CRegex *rx=new CRegex("\\b(?<word>\\w+)\\s+(\\k<word>)\\b",RegexOptions::IgnoreCase);
//--- Define a test string.      
   string text="The the quick brown fox  fox jumped over the lazy dog dog.";
//--- Find matches.
   CMatchCollection *matches=rx.Matches(text);
//--- Report the number of matches found.
   Print(IntegerToString(matches.Count())+" matches found in:\n   "+text);
   CMatchEnumerator *en=matches.GetEnumerator();
//--- Report on each match.
   while(en.MoveNext())
     {
      CMatch *match=en.Current();
      CGroupCollection *groups=match.Groups();
      string count=groups["word"].Value();
      int pos1 = groups[0].Index();
      int pos2 = groups[1].Index();
      Print(count+" repeated at positions "+IntegerToString(pos1)+" and "+IntegerToString(pos2));
     }
   delete rx;
   delete en;
   delete matches;
  }
//+------------------------------------------------------------------+
//| Example 2.                                                       |
//+------------------------------------------------------------------+
void Example5_2()
  {
//--- Define the regular expression pattern.
   string pattern;
   pattern="^\\s*[";
//--- Get the positive and negative sign symbols.
   pattern+=CRegex::Escape("+"+"-")+"]?\\s?";
//--- Get the currency symbol.
   pattern+=CRegex::Escape("p\\.")+"?\\s?";
//--- Add integral digits to the pattern.
   pattern+="(\\d*";
//--- Add the decimal separator.
   pattern+=CRegex::Escape(",")+"?";
//--- Add the fractional digits.
   pattern+="\\d{";
//--- Determine the number of fractional digits in currency values.
   pattern+=IntegerToString(2)+"}?){1}$";
   CRegex *rgx=new CRegex(pattern);
//--- Define some test strings.
   string tests[]=
     {
      "-42","19.99","0.001","100 USD",
      ".34","0.34","1,052.21","$10.62",
      "+1.43","-$0.23"
     };
//--- Check each test string against the regular expression.
   for(int i=0; i<ArraySize(tests); i++)
     {
      string test=tests[i];
      if(rgx.IsMatch(test))
        {
         Print(test+" is a currency value.");
        }
      else
        {
         Print(test+" is not a currency value.");
        }
     }
   delete rgx;
  }
//+------------------------------------------------------------------+
//| Show matches.                                                    |
//+------------------------------------------------------------------+ 
void ShowMatches(CRegex *r,CMatch *m)
  {
   string names[];
   r.GetGroupNames(names);
   Print("Named Groups:");
   for(int i=0; i<ArraySize(names); i++)
     {
      string name=names[i];
      CGroup *grp=m.Groups()[name];
      PrintFormat("   %s: '%s'",name,grp.Value());
     }
  }
//+------------------------------------------------------------------+
//| Example 1.                                                       |
//+------------------------------------------------------------------+
void Example6_1()
  {
   string pattern="\\b(?<FirstWord>\\w+)\\s?((\\w+)\\s)*(?<LastWord>\\w+)?(?<Punctuation>\\p{Po})";
   string in="The cow jumped over the moon.";
   CRegex *rgx=new CRegex(pattern);
   CMatch *match=rgx.Match(in);
   if(match.Success())
     {
      ShowMatches(rgx,match);
     }
   delete match;
   delete rgx;
  }
//+------------------------------------------------------------------+
//| Example 1.                                                       |
//+------------------------------------------------------------------+
void Example7_1()
  {
   string pattern="\\b((?<word>\\w+)\\s*)+(?<end>[.?!])";
   string in="This is a sentence. This is a second sentence.";
   CRegex *rgx=new CRegex(pattern);
   int groupNumbers[];
   rgx.GetGroupNumbers(groupNumbers);
   CMatch *m=rgx.Match(in);
   if(m.Success())
     {
      //PrintFormat("CMatch: %s",m.Value());
      for(int i=0; i<ArraySize(groupNumbers); i++)
        {
         int groupNumber=groupNumbers[i];
         string name=rgx.GroupNameFromNumber(groupNumber);
         PrintFormat("   CGroup %i%s: '%s'",
                     groupNumber,
                     " ("+name+")",
                     m.Groups()[groupNumber].Value());
        }
     }
   delete m;
   delete rgx;
  }
//+------------------------------------------------------------------+
//| Example 1.                                                       |
//+------------------------------------------------------------------+
void Example8_1()
  {
   string pattern="(?<city>[A-Za-z\\s]+), (?<state>[A-Za-z]{2}) (?<zip>\\d{5}(-\\d{4})?)";
   string cityLines[]=
     {
      "New York, NY 10003","Brooklyn, NY 11238","Detroit, MI 48204",
      "San Francisco, CA 94109","Seattle, WA 98109"
     };
   CRegex *rgx=new CRegex(pattern);
   List<string>names();
   int ctr=1;
   bool exitFlag=false;
//--- Get group names.
   do
     {
      string name=rgx.GroupNameFromNumber(ctr);
      if(name!="")
        {
         ctr++;
         names.Add(name);
        }
      else
        {
         exitFlag=true;
        }
     }
   while(!exitFlag);
   for(int i=0; i<ArraySize(cityLines); i++)
     {
      string cityLine=cityLines[i];
      CMatch *match=rgx.Match(cityLine);
      if(match.Success())
        {
         PrintFormat("Zip code %s is in %s, %s.",
                     match.Groups()[names[3]].Value(),
                     match.Groups()[names[1]].Value(),
                     match.Groups()[names[2]].Value());
        }
      delete match;
     }
   delete rgx;
  }
//+------------------------------------------------------------------+
//| Example 1.                                                       |
//+------------------------------------------------------------------+
void Example9_1()
  {
   string partNumbers[]=
     {
      "1298-673-4192","A08Z-931-468A",
      "_A90-123-129X","12345-KKA-1230",
      "0919-2893-1256"
     };
   CRegex *rgx=new CRegex("^[a-zA-Z0-9]\\d{2}[a-zA-Z0-9](-\\d{3}){2}[A-Za-z0-9]$");
   for(int i=0; i<ArraySize(partNumbers); i++)
     {
      string partNumber=partNumbers[i];
      PrintFormat("%s %s a valid part number.",
                  partNumber,
                  rgx.IsMatch(partNumber) ? "is" : "is not");
     }
   delete rgx;
  }
//+------------------------------------------------------------------+
//| Example 2.                                                       |
//+------------------------------------------------------------------+
void Example9_2()
  {
   string partNumbers[]=
     {
      "Part Number: 1298-673-4192","Part No: A08Z-931-468A",
      "_A90-123-129X","123K-000-1230",
      "SKU: 0919-2893-1256"
     };
   CRegex *rgx=new CRegex("[a-zA-Z0-9]\\d{2}[a-zA-Z0-9](-\\d{3}){2}[A-Za-z0-9]$");
   for(int i=0; i<ArraySize(partNumbers); i++)
     {
      string partNumber=partNumbers[i];
      int start = StringFind(partNumber,":");
      if(start >= 0)
        {
         PrintFormat("%s %s a valid part number.",
                     partNumber,
                     rgx.IsMatch(partNumber,start) ? "is" : "is not");
        }
      else
        {
         PrintFormat("Cannot find starting position in %s.",partNumber);
        }
     }
   delete rgx;
  }
//+------------------------------------------------------------------+
//| Example 3.                                                       |
//+------------------------------------------------------------------+
void Example9_3()
  {
   string partNumbers[]=
     {
      "1298-673-4192","A08Z-931-468A",
      "_A90-123-129X","12345-KKA-1230",
      "0919-2893-1256"
     };
   string pattern="^[a-zA-Z0-9]\\d{2}[a-zA-Z0-9](-\\d{3}){2}[A-Za-z0-9]$";
   for(int i=0; i<ArraySize(partNumbers); i++)
     {
      string partNumber=partNumbers[i];
      PrintFormat("%s %s a valid part number.",
                  partNumber,
                  CRegex::IsMatch(partNumber,pattern) ? "is" : "is not");
     }
  }
//+------------------------------------------------------------------+
//| Example 4.                                                       |
//+------------------------------------------------------------------+
void Example9_4()
  {
   string partNumbers[]=
     {
      "1298-673-4192","A08Z-931-468a",
      "_A90-123-129X","12345-KKA-1230",
      "0919-2893-1256"
     };
   string pattern="^[A-Z0-9]\\d{2}[A-Z0-9](-\\d{3}){2}[A-Z0-9]$";
   for(int i=0; i<ArraySize(partNumbers); i++)
     {
      string partNumber=partNumbers[i];
      PrintFormat("%s %s a valid part number.",
                  partNumber,
                  CRegex::IsMatch(partNumber,pattern,RegexOptions::IgnoreCase)
                  ? "is" : "is not");
     }
  }
//+------------------------------------------------------------------+
//| Example 5.                                                       |
//+------------------------------------------------------------------+
void Example9_5()
  {
   string partNumbers[]=
     {
      "1298-673-4192","A08Z-931-468a",
      "_A90-123-129X","12345-KKA-1230",
      "0919-2893-1256"
     };
   string pattern="^[A-Z0-9]\\d{2}[A-Z0-9](-\\d{3}){2}[A-Z0-9]$";
   for(int i=0; i<ArraySize(partNumbers); i++)
     {
      TimeSpan matchTimeout=TimeSpan::FromMilliseconds(500);
      string partNumber=partNumbers[i];
      PrintFormat("%s %s a valid part number.",
                  partNumber,
                  CRegex::IsMatch(partNumber,pattern,RegexOptions::IgnoreCase,matchTimeout)
                  ? "is" : "is not");
     }
  }
//+------------------------------------------------------------------+
//| Example 1.                                                       |
//+------------------------------------------------------------------+
void Example10_1()
  {
   string in="This is   text with   far  too   much   whitespace.";
   string pattern="\\s+";
   string replacement=" ";
   CRegex *rgx=new CRegex(pattern);
   string result=rgx.Replace(in,replacement);
   PrintFormat("Original String: %s",in);
   PrintFormat("Replacement String: %s",result);
   delete rgx;
  }
//+------------------------------------------------------------------+
//| Example 2.                                                       |
//+------------------------------------------------------------------+
void Example10_2()
  {
   string pattern="(\\p{Sc}\\s?)?(\\d+\\.?((?<=\\.)\\d+)?)(?(1)|\\s?\\p{Sc})?";
   string in="$17.43  €2 16.33  £0.98  0.43   £43   12€  17";
   string replacement="$2";
   CRegex *rgx=new CRegex(pattern);
   string result=rgx.Replace(in,replacement);
   PrintFormat("Original String:    '%s'",in);
   PrintFormat("Replacement String: '%s'",result);
   delete rgx;
  }
//+------------------------------------------------------------------+
//| MatchEvaluator 1.                                                |
//+------------------------------------------------------------------+
string CapText(CMatch *m)
  {
//--- Get the matched string.
   string x=m.ToString();
   string X= x;
   StringToUpper(X);
//--- If the first char is lower case...
   if(StringGetCharacter(x,0)>='a' && StringGetCharacter(x,0)<='z')
     {
      //--- Capitalize it.
      //--- return result
      return (ShortToString(StringGetCharacter(X,0)) + StringSubstr(x,1, StringLen(x) - 1));
     }
//--- return result
   return (x);
  }
//+------------------------------------------------------------------+
//| Example 3.                                                       |
//+------------------------------------------------------------------+
void Example10_3()
  {
   string text="four score and seven years ago";
   Print("text=["+text+"]");
   CRegex *rx=new CRegex("\\w+");
   MatchEvaluator match=CapText;
   string result=rx.Replace(text,match);
   Print("result=["+result+"]");
   delete rx;
  }
//+------------------------------------------------------------------+
//| Example 1.                                                       |
//+------------------------------------------------------------------+
void Example11_1()
  {
   CRegex *regex=new CRegex("-");       // Split on hyphens.
   string substrings[];
   regex.Split(substrings,"plum--pear");
   for(int i=0; i<ArraySize(substrings); i++)
     {
      string match=substrings[i];
      PrintFormat("'%s'",match);
     }
   delete regex;
  }
//+------------------------------------------------------------------+
//| Example 2.                                                       |
//+------------------------------------------------------------------+
void Example11_2()
  {
   string pattern="\\d+";
   CRegex *rgx=new CRegex(pattern);
   string in="123ABCDE456FGHIJKL789MNOPQ012";
   string result[];
   rgx.Split(result,in);
   string text;
   for(int ctr=0; ctr<ArraySize(result); ctr++)
     {
      text+=StringFormat("'%s'",result[ctr]);
      if(ctr<ArraySize(result)-1)
        {
         text+=(", ");
        }
     }
   Print(text);
   delete rgx;
  }
//+------------------------------------------------------------------+
//| Example 3.                                                       |
//+------------------------------------------------------------------+
void Example11_3()
  {
   CRegex *regex=new CRegex("(-)");     // Split on hyphens.
   string substrings[];
   regex.Split(substrings,"plum-pear");
   for(int i=0; i<ArraySize(substrings); i++)
     {
      string match=substrings[i];
      PrintFormat("'%s'",match);
     }
   delete regex;
  }
//+------------------------------------------------------------------+
//| Example 4.                                                       |
//+------------------------------------------------------------------+
void Example11_4()
  {
   string in="07/14/2007";
   string pattern="(-)|(/)";
   CRegex *regex=new CRegex(pattern);
   string substrings[];
   regex.Split(substrings,in);
   for(int i=0; i<ArraySize(substrings); i++)
     {
      string result=substrings[i];
      PrintFormat("'%s'",result);
     }
   delete regex;
  }
//+------------------------------------------------------------------+
//| Example 5.                                                       |
//+------------------------------------------------------------------+
void Example11_5()
  {
   string in="characters";
   CRegex *regex=new CRegex("");
   string substrings[];
   regex.Split(substrings,in);
   string result="{";
   for(int ctr=0; ctr<ArraySize(substrings); ctr++)
     {
      result+=(substrings[ctr]);
      if(ctr<ArraySize(substrings)-1)
        {
         result+=(", ");
        }
     }
   Print(result+"}");
   delete regex;
  }
//+------------------------------------------------------------------+
//| Example 1.                                                       |
//+------------------------------------------------------------------+
void Example12_1()
  {
   string in="Yes. This dog is very friendly.";
   string pattern="((\\w+)[\\s.])+";
   CMatchCollection*matches=CRegex::Matches(in,pattern);
   CMatchEnumerator *en=matches.GetEnumerator();
   while(en.MoveNext())
     {
      CMatch *match=en.Current();
      PrintFormat("CMatch: %s",match.Value());
      for(int groupCtr=0; groupCtr<match.Groups().Count(); groupCtr++)
        {
         CGroup *group=match.Groups()[groupCtr];
         PrintFormat("   CGroup %i: %s",groupCtr,group.Value());
         for(int captureCtr=0; captureCtr<group.Captures().Count(); captureCtr++)
           {
            PrintFormat("      CCapture %i: %s",captureCtr,
                        group.Captures()[captureCtr].Value());
           }
        }
      delete match;
     }
   delete en;
   delete matches;
  }
//+------------------------------------------------------------------+
//| Example 1.                                                       |
//+------------------------------------------------------------------+
void Example13_1()
  {
   string pattern;
   string in="The young, hairy, and tall dog slowly walked across the yard.";
   CMatch *match;
//--- CMatch a word with a pattern that has no capturing groups.
   pattern="\\b\\w+\\W{1,2}";
   match=CRegex::Match(in,pattern);
   Print("Pattern: "+pattern);
   Print("CMatch: "+match.Value());
   PrintFormat("  CMatch.Captures: %i",match.Captures().Count());
   for(int ctr=0; ctr<match.Captures().Count(); ctr++)
     {
      PrintFormat("    %i: '%s'",ctr,match.Captures()[ctr].Value());
     }
   PrintFormat("  CMatch.Groups: %i",match.Groups().Count());
   for(int groupCtr=0; groupCtr<match.Groups().Count(); groupCtr++)
     {
      PrintFormat("    CGroup %i: '%s'",
                  groupCtr,match.Groups()[groupCtr].Value());
      PrintFormat("    CGroup(%i).Captures: %i",
                  groupCtr,match.Groups()[groupCtr].Captures().Count());
      for(int captureCtr=0; captureCtr<match.Groups()[groupCtr].Captures().Count(); captureCtr++)
        {
         PrintFormat("      CCapture %i: '%s'",
                     captureCtr,
                     match.Groups()[groupCtr].Captures()[captureCtr].Value());
        }
     }
   Print("-----\n");
   delete match;
//--- CMatch a sentence with a pattern that has a quantifier that applies to the entire group.
   pattern="(\\b\\w+\\W{1,2})+";
   match=CRegex::Match(in,pattern);
   Print("Pattern: "+pattern);
   Print("CMatch: "+match.Value());
   PrintFormat("  CMatch.Captures: %i",match.Captures().Count());
   for(int ctr=0; ctr<match.Captures().Count(); ctr++)
     {
      PrintFormat("    %i: '%s'",ctr,match.Captures()[ctr].Value());
     }
   PrintFormat("  CMatch.Groups: %i",match.Groups().Count());
   for(int groupCtr=0; groupCtr<match.Groups().Count(); groupCtr++)
     {
      PrintFormat("    CGroup %i: '%s'",groupCtr,match.Groups()[groupCtr].Value());
      PrintFormat("    CGroup(%i).Captures: %i",
                  groupCtr,match.Groups()[groupCtr].Captures().Count());
      for(int captureCtr=0; captureCtr<match.Groups()[groupCtr].Captures().Count(); captureCtr++)
        {
         PrintFormat("      CCapture %i: '%s'",captureCtr,match.Groups()[groupCtr].Captures()[captureCtr].Value());
        }
     }
   delete match;
  }
//+------------------------------------------------------------------+
//| Example 1.                                                       |
//+------------------------------------------------------------------+
void Example14_1()
  {
   string pattern="(\\b(\\w+?)[,:;]?\\s?)+[?.!]";
   string in="This is one sentence. This is a second sentence.";
   CMatch *match=CRegex::Match(in,pattern);
   Print("CMatch: "+match.Value());
   int groupCtr=0;
   CGroupCollection *groups=match.Groups();
   GroupEnumerator *enGroup=groups.GetEnumerator();
   while(enGroup.MoveNext())
     {
      CGroup *group=enGroup.Current();
      groupCtr++;
      PrintFormat("   CGroup %i: '%s'",groupCtr,group.Value());
      int captureCtr=0;
      CCaptureCollection *captures = group.Captures();
      CCaptureEnumerator *enCapture=captures.GetEnumerator();
      while(enCapture.MoveNext())
        {
         CCapture *capture=enCapture.Current();
         captureCtr++;
         PrintFormat("      CCapture %i: '%s'",captureCtr,capture.Value());
        }
      delete enCapture;
      delete captures;
     }
   delete enGroup;
   delete groups;
   delete match;
  }
//+------------------------------------------------------------------+
//| Example 1.                                                       |
//+------------------------------------------------------------------+
void Example15_1()
  {
   string pattern="\\b(\\w+?)([\\u00AE\\u2122])";
   string in="Microsoft® Office Professional Edition combines several office "+
             "productivity products, including Word, Excel®, Access®, Outlook®, "+
             "PowerPoint®, and several others. Some guidelines for creating "+
             "corporate documents using these productivity tools are available "+
             "from the documents created using Silverlight™ on the corporate "+
             "intranet site.";
   CMatchCollection *matches=CRegex::Matches(in,pattern);
   CMatchEnumerator *en=matches.GetEnumerator();
   while(en.MoveNext())
     {
      CMatch *match=en.Current();
      CGroupCollection *groups=match.Groups();
      PrintFormat("%s: %s",groups[2].Value(),groups[1].Value());
     }
   delete en;
   Print("\0");
   PrintFormat("Found %i trademarks or registered trademarks.",matches.Count());
   delete matches;
  }
//+------------------------------------------------------------------+
//| Example 1.                                                       |
//+------------------------------------------------------------------+
void Example16_1()
  {
   string sentence="Half-way down a by-street of one of our New England towns, stands a rusty wooden "+
                   "house, with seven acutely peaked gables, facing towards various points of the compass, "+
                   "and a huge, clustered chimney in the midst.";
   string pattern="\\b[hH]\\w*\\b";
   CMatchCollection *matches=CRegex::Matches(sentence,pattern);
   for(int ctr=0; ctr<matches.Count(); ctr++)
     {
      Print(matches[ctr].Value());
     }
   delete matches;
  }
//+------------------------------------------------------------------+
//| My class.                                                        |
//+------------------------------------------------------------------+
class MyClass
  {
   static int        s_i;
public:
   //--- Methods:   
   //+------------------------------------------------------------------+
   //| Replace each CRegex cc match with the number of the occurrence.   |
   //+------------------------------------------------------------------+
   static string ReplaceCC(CMatch *m)
     {
      s_i++;
      //--- return result
      return (IntegerToString(s_i) + IntegerToString(s_i));
     }
  };
int MyClass::s_i=0;
//+------------------------------------------------------------------+
//| Example 1.                                                       |
//+------------------------------------------------------------------+
void Example17_1()
  {
   string sInput,sRegex;
//--- The string to search.
   sInput="aabbccddeeffcccgghhcccciijjcccckkcc";
//--- A very simple regular expression.
   sRegex="cc";
   CRegex *r=new CRegex(sRegex);
//--- Assign the replace method to the MatchEvaluator delegate.
   MatchEvaluator myEvaluator=MyClass::ReplaceCC;
//--- Write out the original string.
   Print(sInput);
//--- Replace matched characters using the delegate method.
   sInput=r.Replace(sInput,myEvaluator);
//--- Write out the modified string.
   Print(sInput);
   delete r;
//--- The example displays the following output:
//---       aabbccddeeffcccgghhcccciijjcccckkcc
//---       aabb11ddeeff22cgghh3344iijj5566kk77
  }
//+------------------------------------------------------------------+
//| Example 1.                                                       |
//+------------------------------------------------------------------+
void Example18_1()
  {
   string pattern="\\d+";
   string inputs[]=
     {
      "This sentence contains no numbers.",
      "123 What do I see?",
      "2468 369 48 5"
     };
   for(int i=0; i<ArraySize(inputs); i++)
     {
      string in=inputs[i];
      CMatchCollection *matches=CRegex::Matches(in,pattern);
      PrintFormat("Input: %s",in);
      if(matches.Count()==0)
        {
         Print("   No matches");
        }
      else
        {
         CMatchEnumerator *en=matches.GetEnumerator();
         while(en.MoveNext())
           {
            CMatch *m=en.Current();
            PrintFormat("   %s at index %i",m.Value(),m.Index());
           }
         delete en;
        }
      Print("\0");
      delete matches;
     }
  }
//+------------------------------------------------------------------+
//| Example 1.                                                       |
//+------------------------------------------------------------------+
void Example19_1()
  {
//--- Define a case-insensitive regular expression for repeated words.
   CRegex *rxInsensitive=new CRegex("\\b(?<word>\\w+)\\s+(\\k<word>)\\b",RegexOptions::IgnoreCase);
//--- Define a case-sensitive regular expression for repeated words.
   CRegex *rxSensitive=new CRegex("\\b(?<word>\\w+)\\s+(\\k<word>)\\b");
//--- Define a test string.        
   string text="The the quick brown fox  fox jumped over the lazy dog dog.";
//--- Find matches using case-insensitive regular expression.
   CMatchCollection *matches=rxInsensitive.Matches(text);
//--- Report the number of matches found.
   PrintFormat("%i matches found in:\n   %s",
               matches.Count(),
               text);
//--- Report on each match.
   CMatchEnumerator *en=matches.GetEnumerator();
   while(en.MoveNext())
     {
      CGroupCollection *groups=en.Current().Groups();
      PrintFormat("'%s' repeated at positions %i and %i",
                  groups["word"].Value(),
                  groups[0].Index(),
                  groups[1].Index());
     }
   delete en;
   Print("\0");
//--- Find matches using case-sensitive regular expression.
   delete matches;
   matches=rxSensitive.Matches(text);
//--- Report the number of matches found.
   PrintFormat("%i matches found in:\n   %s",
               matches.Count(),
               text);
//--- Report on each match.
   en=matches.GetEnumerator();
   while(en.MoveNext())
     {
      CGroupCollection *groups=en.Current().Groups();
      PrintFormat("'%s' repeated at positions %i and %i",
                  groups["word"].Value(),
                  groups[0].Index(),
                  groups[1].Index());
     }
   delete en;
   delete matches;
   delete rxInsensitive;
   delete rxSensitive;
  }
//+------------------------------------------------------------------+
//| Class TestView.                                                  |
//+------------------------------------------------------------------+
class TestView : public CAppDialog
  {
private:
   CLabel            m_lable1;                        // the lable object
   CLabel            m_lable2;                        // the lable object
   CEdit             m_names[20];                     // the display field object
   CButton           m_buttons[20];                   // the button object
   CListView         m_list_view;                     // the list object
public:
   //--- Methods:
   //+------------------------------------------------------------------+
   //| Create.                                                          |
   //+------------------------------------------------------------------+
   virtual bool      Create()
     {
      const long chart=0;
      const string name= "Set of samples for testing the Regular Expressions library";
      const int subwin = 0;
      const int x1 = 30;
      const int y1 = 30;
      const int x2 = 700;
      const int y2 = 500;
      if(!CAppDialog::Create(chart,name,subwin,x1,y1,x2,y2))
         return(false);
      //--- create dependent controls
      if(!CreateNames())
         return(false);
      if(!CreateButtons())
         return(false);
      if(!CreateListView())
         return(false);
      m_lable1.Create(chart,"Text1",subwin,10,270,70,280);
      m_lable1.Text("Expected result (see actual output in the Experts tab):");
      Add(m_lable1);
      //--- succeed
      return(true);
     }
   //--- chart event handler
   virtual bool      OnEvent(const int id,const long &lparam,const double &dparam,const string &sparam);
protected:
   //--- create dependent controls:
   //+------------------------------------------------------------------+
   //| Create scripts names.                                            |
   //+------------------------------------------------------------------+
   bool CreateNames()
     {
      int x1=10;
      int y1=20;
      int x2=200;
      int y2=40;
      for(int i=0; i<20; i++)
        {
         m_names[i].Create(0,"Name"+IntegerToString(i),0,x1,y1,x2,y2);
         m_names[i].ReadOnly(true);
         m_names[i].Text("Name"+IntegerToString(i));
         Add(m_names[i]);
         y1=y2+5;
         y2+=25;
         if(i==9)
           {
            x1=350;
            x2=560;
            y1=20;
            y2=40;
           }
        }
      m_names[0].Text("MatchExamples");
      m_names[1].Text("MatchGroups");
      m_names[2].Text("MatchResult");
      m_names[3].Text("RegexConstructor");
      m_names[4].Text("RegexEscape");
      m_names[5].Text("RegexExample");
      m_names[6].Text("RegexGetGroupNames");
      m_names[7].Text("RegexGetGroupNumbers");
      m_names[8].Text("RegexGroupNameFromNumber");
      m_names[9].Text("RegexIsMatch");
      m_names[10].Text("RegexReplace");
      m_names[11].Text("RegexSplit");
      m_names[12].Text("Capture");
      m_names[13].Text("CaptureCollection");
      m_names[14].Text("Group");
      m_names[15].Text("GroupCollection");
      m_names[16].Text("MatchCollectionItem");
      m_names[17].Text("MatchEvaluator");
      m_names[18].Text("RegexMatchCollectionCount");
      m_names[19].Text("RegexOptions");
      //--- return result
      return (true);
     }
   //+------------------------------------------------------------------+
   //| Create the "Button" button.                                      |
   //+------------------------------------------------------------------+ 
   bool              CreateButtons(void)
     {
      //--- coordinates
      int x1=220;
      int y1=20;
      int x2=300;
      int y2=40;
      //--- create
      for(int i=0; i<20; i++)
        {
         m_buttons[i].Create(0,"Button"+IntegerToString(i),0,x1,y1,x2,y2);
         m_buttons[i].Text("Run");
         Add(m_buttons[i]);
         y1=y2+5;
         y2+=25;
         if(i==9)
           {
            x1=570;
            x2=650;
            y1=20;
            y2=40;
           }
        }
      //--- succeed
      return(true);
     }
   //+------------------------------------------------------------------+
   //| Create the "ListView" element                                    |
   //+------------------------------------------------------------------+
   bool              CreateListView(void)
     {
      //--- coordinates
      int x1=10;
      int y1=290;
      int x2=650;
      int y2=430;
      //--- create
      if(!m_list_view.Create(m_chart_id,m_name+"ListView",m_subwin,x1,y1,x2,y2))
         return(false);
      if(!Add(m_list_view))
         return(false);
      //--- succeed
      return(true);
     }
   //+------------------------------------------------------------------+
   //| ListView clear.                                                  |
   //+------------------------------------------------------------------+
   void ListViewClear()
     {
      m_list_view.VScrolled(false);
      m_list_view.HScrolled(false);
      m_list_view.Destroy();
      CreateListView();
     }
   //--- handlers of the dependent controls events
   //+------------------------------------------------------------------+
   //| Button 0 click.                                                  |
   //+------------------------------------------------------------------+
   void              OnClickButton0(void)
     {
      ListViewClear();
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("   'Print' found in the source code at position 159.");
      m_list_view.AddItem("   'Print' found in the source code at position 144.");
      m_list_view.AddItem("   'Print' found in the source code at position 84.");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("   'Print' found in the source code at position 159.");
      m_list_view.AddItem("    'Print' found in the source code at position 144.");
      m_list_view.AddItem("    'Print' found in the source code at position 84.");
      Example0_1();
      Print("");
      Example0_2();
     }
   //+------------------------------------------------------------------+
   //| Button 1 click.                                                  |
   //+------------------------------------------------------------------+
   void              OnClickButton1(void)
     {
      ListViewClear();
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("      Area Code:        212");
      m_list_view.AddItem("      Telephone number: 555-6666");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("      Area Code:        906");
      m_list_view.AddItem("      Telephone number: 932-1111");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("      Area Code:        415");
      m_list_view.AddItem("      Telephone number: 222-3333");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("      Area Code:        425");
      m_list_view.AddItem("      Telephone number: 888-9999");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("This example displays the following output:");
      m_list_view.AddItem("      Match1");
      m_list_view.AddItem("      Group1='One'");
      m_list_view.AddItem("      Capture0='One', Position=0");
      m_list_view.AddItem("      Group2='car'");
      m_list_view.AddItem("      Capture0='car', Position=4");
      m_list_view.AddItem("      Match2");
      m_list_view.AddItem("      Group1='red'");
      m_list_view.AddItem("      Capture0='red', Position=8");
      m_list_view.AddItem("      Group2='car'");
      m_list_view.AddItem("      Capture0='car', Position=12");
      m_list_view.AddItem("      Match3");
      m_list_view.AddItem("      Group1='blue'");
      m_list_view.AddItem("      Capture0='blue', Position=16");
      m_list_view.AddItem("      Group2='car'");
      m_list_view.AddItem("      Capture0='car', Position=21");
      Example1_1();
      Print("");
      Example1_2();
     }
   //+------------------------------------------------------------------+
   //| Button 2 click.                                                  |
   //+------------------------------------------------------------------+
   void              OnClickButton2(void)
     {
      ListViewClear();
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("      (decisively)");
      m_list_view.AddItem("      (whatever time it was)");
      Example2_1();
     }
   //+------------------------------------------------------------------+
   //| Button 3 click.                                                  |
   //+------------------------------------------------------------------+
   void              OnClickButton3(void)
     {
      ListViewClear();
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("      Parsing 'The threaded application ate up the thread pool as it executed.'");
      m_list_view.AddItem("      0. threaded");
      m_list_view.AddItem("      1. application");
      m_list_view.AddItem("      2. ate");
      m_list_view.AddItem("      3. the");
      m_list_view.AddItem("      4. thread");
      m_list_view.AddItem("      5. as  ");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("   Parsing 'The threaded application ate up the thread pool as it executed.' with options IgnoreCase:");
      m_list_view.AddItem("   0. The");
      m_list_view.AddItem("   1. threaded");
      m_list_view.AddItem("   2. application");
      m_list_view.AddItem("   3. ate");
      m_list_view.AddItem("   4. the");
      m_list_view.AddItem("   5. thread");
      m_list_view.AddItem("   6. as ");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("The example displays output like the following :");
      m_list_view.AddItem("   Processing aa");
      m_list_view.AddItem("   Valid: 'aa' (0.01700000 ms)");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("   Processing aaaa>");
      m_list_view.AddItem("   'aaaa>' is not a valid string. (0.09800000 ms)");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("   Processing aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
      m_list_view.AddItem("   Valid: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa' (0.01200000 ms)");
      Example3_1();
      Print("");
      Example3_2();
      Print("");
      Example3_3();
     }
   //+------------------------------------------------------------------+
   //| Button 4 click.                                                  |
   //+------------------------------------------------------------------+
   void              OnClickButton4(void)
     {
      ListViewClear();
      m_list_view.AddItem("This example displays the following output:");
      m_list_view.AddItem("      [(.*?)] produces the following matches:");
      m_list_view.AddItem("         1: ?");
      m_list_view.AddItem("         2: ?");
      m_list_view.AddItem("         3: .");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("This example displays the following output:");
      m_list_view.AddItem("      [(.*?)] produces the following matches:");
      m_list_view.AddItem("         1: [what kind?]");
      m_list_view.AddItem("         2: [by whom?]");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("The example shows possible output from the example:");
      m_list_view.AddItem("      Begin comment symbol: [");
      m_list_view.AddItem("      End comment symbol: ]");
      m_list_view.AddItem("      [(.*?)]");
      m_list_view.AddItem("      1: comment comment comment");
      m_list_view.AddItem("      2: comment");
      Example4_1();
      Print("");
      Example4_2();
      Print("");
      Example4_3();
     }
   //+------------------------------------------------------------------+
   //| Button 5 click.                                                  |
   //+------------------------------------------------------------------+
   void              OnClickButton5(void)
     {
      ListViewClear();
      m_list_view.AddItem("The example produces the following output to the terminal:");
      m_list_view.AddItem("      3 matches found in:");
      m_list_view.AddItem("         The the quick brown fox  fox jumped over the lazy dog dog.");
      m_list_view.AddItem("      'The' repeated at positions 0 and 4");
      m_list_view.AddItem("      'fox' repeated at positions 20 and 25");
      m_list_view.AddItem("      'dog' repeated at positions 50 and 54");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("      -42 is a not currency value.");
      m_list_view.AddItem("      19.99 is not a currency value.");
      m_list_view.AddItem("      0.001 is not a currency value.");
      m_list_view.AddItem("      100 USD is not a currency value.");
      m_list_view.AddItem("      .34 is not a currency value.");
      m_list_view.AddItem("      0.34 is not a currency value.");
      m_list_view.AddItem("      1,052.21 is not a currency value.");
      m_list_view.AddItem("      $10.62 is not a currency value.");
      m_list_view.AddItem("      +1.43 is not a currency value.");
      m_list_view.AddItem("      -$0.23 is not a currency value.");
      Example5_1();
      Print("");
      Example5_2();
     }
   //+------------------------------------------------------------------+
   //| Button 6 click.                                                  |
   //+------------------------------------------------------------------+
   void              OnClickButton6(void)
     {
      ListViewClear();
      m_list_view.AddItem("The example produces the following output to the terminal:");
      m_list_view.AddItem("      Named Groups:");
      m_list_view.AddItem("         0: 'The cow jumped over the moon.'");
      m_list_view.AddItem("         1: 'the '");
      m_list_view.AddItem("         2: 'the'");
      m_list_view.AddItem("         FirstWord: 'The'");
      m_list_view.AddItem("         LastWord: 'moon'");
      m_list_view.AddItem("         Punctuation: '.'");
      Example6_1();
     }
   //+------------------------------------------------------------------+
   //| Button 7 click.                                                  |
   //+------------------------------------------------------------------+
   void              OnClickButton7(void)
     {
      ListViewClear();
      m_list_view.AddItem("The example produces the following output to the terminal:");
      m_list_view.AddItem("      CMatch: This is a sentence.");
      m_list_view.AddItem("         CGroup 0: 'This is a sentence.'");
      m_list_view.AddItem("         CGroup 1: 'sentence'");
      m_list_view.AddItem("         CGroup 2 (word): 'sentence'");
      m_list_view.AddItem("         CGroup 3 (end): '.'");
      Example7_1();
     }
   //+------------------------------------------------------------------+
   //| Button 8 click.                                                  |
   //+------------------------------------------------------------------+
   void              OnClickButton8(void)
     {
      ListViewClear();
      m_list_view.AddItem("The example produces the following output to the terminal:");
      m_list_view.AddItem("      Zip code 10003 is in New York, NY.");
      m_list_view.AddItem("      Zip code 11238 is in Brooklyn, NY.");
      m_list_view.AddItem("      Zip code 48204 is in Detroit, MI.");
      m_list_view.AddItem("      Zip code 94109 is in San Francisco, CA.");
      m_list_view.AddItem("      Zip code 98109 is in Seattle, WA.");
      Example8_1();
     }
   //+------------------------------------------------------------------+
   //| Button 9 click.                                                  |
   //+------------------------------------------------------------------+
   void              OnClickButton9(void)
     {
      ListViewClear();
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("      1298-673-4192 is a valid part number.");
      m_list_view.AddItem("      A08Z-931-468A is a valid part number.");
      m_list_view.AddItem("      _A90-123-129X is not a valid part number.");
      m_list_view.AddItem("      12345-KKA-1230 is not a valid part number.");
      m_list_view.AddItem("      0919-2893-1256 is not a valid part number.");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("      Part Number: 1298-673-4192 is a valid part number.");
      m_list_view.AddItem("      Part No: A08Z-931-468A is a valid part number.");
      m_list_view.AddItem("      Cannot find starting position in _A90-123-129X.");
      m_list_view.AddItem("      Cannot find starting position in 123K-000-1230.");
      m_list_view.AddItem("      SKU: 0919-2893-1256 is not a valid part number.");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("      1298-673-4192 is a valid part number.");
      m_list_view.AddItem("      A08Z-931-468A is a valid part number.");
      m_list_view.AddItem("      _A90-123-129X is not a valid part number.");
      m_list_view.AddItem("      12345-KKA-1230 is not a valid part number.");
      m_list_view.AddItem("      0919-2893-1256 is not a valid part number. ");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("      1298-673-4192 is a valid part number.");
      m_list_view.AddItem("      A08Z-931-468a is a valid part number.");
      m_list_view.AddItem("      _A90-123-129X is not a valid part number.");
      m_list_view.AddItem("      12345-KKA-1230 is not a valid part number.");
      m_list_view.AddItem("      0919-2893-1256 is not a valid part number.");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("      1298-673-4192 is a valid part number.");
      m_list_view.AddItem("      A08Z-931-468a is a valid part number.");
      m_list_view.AddItem("      _A90-123-129X is not a valid part number.");
      m_list_view.AddItem("      12345-KKA-1230 is not a valid part number.");
      m_list_view.AddItem("      0919-2893-1256 is not a valid part number.");
      Example9_1();
      Print("");
      Example9_2();
      Print("");
      Example9_3();
      Print("");
      Example9_4();
      Print("");
      Example9_5();
     }
   //+------------------------------------------------------------------+
   //| Button 10 click.                                                 |
   //+------------------------------------------------------------------+
   void              OnClickButton10(void)
     {
      ListViewClear();
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("      Original String: This is   text with   far  too   much   whitespace.");
      m_list_view.AddItem("      Replacement String: This is text with far too much whitespace.");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("      Original String:    '$17.43  €2 16.33  £0.98  0.43   £43   12€  17'");
      m_list_view.AddItem("      Replacement String: '17.43  2 16.33  0.98  0.43   43   12  17'");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("      text=[four score and seven years ago]");
      m_list_view.AddItem("      result=[Four Score And Seven Years Ago]");
      Example10_1();
      Print("");
      Example10_2();
      Print("");
      Example10_3();
     }
   //+------------------------------------------------------------------+
   //| Button 11 click.                                                 |
   //+------------------------------------------------------------------+
   void              OnClickButton11(void)
     {
      ListViewClear();
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("   'plum'");
      m_list_view.AddItem("   ''");
      m_list_view.AddItem("   'pear'");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("      '', 'ABCDE', 'FGHIJKL', 'MNOPQ', ''");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("   'plum'");
      m_list_view.AddItem("   '-'");
      m_list_view.AddItem("   'pear'");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("The method displays an array of 3 elements, as follows:");
      m_list_view.AddItem("   '07'");
      m_list_view.AddItem("   '/'");
      m_list_view.AddItem("   '14'");
      m_list_view.AddItem("   '/'");
      m_list_view.AddItem("   '2007'");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("   {, c, h, a, r, a, c, t, e, r, s, }");
      Example11_1();
      Print("");
      Example11_2();
      Print("");
      Example11_3();
      Print("");
      Example11_4();
      Print("");
      Example11_5();
     }
   //+------------------------------------------------------------------+
   //| Button 12 click.                                                 |
   //+------------------------------------------------------------------+
   void              OnClickButton12(void)
     {
      ListViewClear();
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("      CMatch: Yes.");
      m_list_view.AddItem("         CGroup 0: Yes.");
      m_list_view.AddItem("            CCapture 0: Yes.");
      m_list_view.AddItem("         CGroup 1: Yes.");
      m_list_view.AddItem("            CCapture 0: Yes.");
      m_list_view.AddItem("         CGroup 2: Yes");
      m_list_view.AddItem("            CCapture 0: Yes");
      m_list_view.AddItem("      CMatch: This dog is very friendly.");
      m_list_view.AddItem("         CGroup 0: This dog is very friendly.");
      m_list_view.AddItem("            CCapture 0: This dog is very friendly.");
      m_list_view.AddItem("         CGroup 1: friendly.");
      m_list_view.AddItem("            CCapture 0: This");
      m_list_view.AddItem("            CCapture 1: dog");
      m_list_view.AddItem("            CCapture 2: is");
      m_list_view.AddItem("            CCapture 3: very");
      m_list_view.AddItem("            CCapture 4: friendly.");
      m_list_view.AddItem("         CGroup 2: friendly");
      m_list_view.AddItem("            CCapture 0: This");
      m_list_view.AddItem("            CCapture 1: dog");
      m_list_view.AddItem("            CCapture 2: is");
      m_list_view.AddItem("            CCapture 3: very");
      m_list_view.AddItem("            CCapture 4: friendly");
      Example12_1();
     }
   //+------------------------------------------------------------------+
   //| Button 13 click.                                                 |
   //+------------------------------------------------------------------+
   void              OnClickButton13(void)
     {
      ListViewClear();
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("   Pattern: \\b\\w+\\W{1,2}");
      m_list_view.AddItem("   CMatch: The");
      m_list_view.AddItem("     CMatch.Captures: 1");
      m_list_view.AddItem("       0: 'The '");
      m_list_view.AddItem("     CMatch.Groups: 1");
      m_list_view.AddItem("       CGroup 0: 'The '");
      m_list_view.AddItem("       CGroup(0).Captures: 1");
      m_list_view.AddItem("         CCapture 0: 'The '");
      m_list_view.AddItem("   -----");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("   Pattern: (\\b\\w+\\W{1,2})+");
      m_list_view.AddItem("   CMatch: The young, hairy, and tall dog slowly walked across the yard.");
      m_list_view.AddItem("     CMatch.Captures: 1");
      m_list_view.AddItem("       0: 'The young, hairy, and tall dog slowly walked across the yard.'");
      m_list_view.AddItem("     CMatch.Groups: 2");
      m_list_view.AddItem("       CGroup 0: 'The young, hairy, and tall dog slowly walked across the yard.'");
      m_list_view.AddItem("       CGroup(0).Captures: 1");
      m_list_view.AddItem("         CCapture 0: 'The young, hairy, and tall dog slowly walked across the yard.'");
      m_list_view.AddItem("       CGroup 1: 'yard.'");
      m_list_view.AddItem("       CGroup(1).Captures: 11");
      m_list_view.AddItem("         CCapture 0: 'The '");
      m_list_view.AddItem("         CCapture 1: 'young, '");
      m_list_view.AddItem("         CCapture 2: 'hairy, '");
      m_list_view.AddItem("         CCapture 3: 'and '");
      m_list_view.AddItem("         CCapture 4: 'tall '");
      m_list_view.AddItem("         CCapture 5: 'dog '");
      m_list_view.AddItem("         CCapture 6: 'slowly '");
      m_list_view.AddItem("         CCapture 7: 'walked '");
      m_list_view.AddItem("         CCapture 8: 'across '");
      m_list_view.AddItem("         CCapture 9: 'the '");
      m_list_view.AddItem("         CCapture 10: 'yard.'");
      Example13_1();
     }
   //+------------------------------------------------------------------+
   //| Button 14 click.                                                 |
   //+------------------------------------------------------------------+
   void              OnClickButton14(void)
     {
      ListViewClear();
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("      CMatch: This is one sentence.");
      m_list_view.AddItem("         CGroup 1: 'This is one sentence.'");
      m_list_view.AddItem("            CCapture 1: 'This is one sentence.'");
      m_list_view.AddItem("         CGroup 2: 'sentence'");
      m_list_view.AddItem("            CCapture 1: 'This '");
      m_list_view.AddItem("            CCapture 2: 'is '");
      m_list_view.AddItem("            CCapture 3: 'one '");
      m_list_view.AddItem("            CCapture 4: 'sentence'");
      m_list_view.AddItem("         CGroup 3: 'sentence'");
      m_list_view.AddItem("            Capture 1: 'This'");
      m_list_view.AddItem("            CCapture 2: 'is'");
      m_list_view.AddItem("            CCapture 3: 'one'");
      m_list_view.AddItem("            CCapture 4: 'sentence'");
      Example14_1();
     }
   //+------------------------------------------------------------------+
   //| Button 15 click.                                                 |
   //+------------------------------------------------------------------+
   void              OnClickButton15(void)
     {
      ListViewClear();
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("      R: Microsoft");
      m_list_view.AddItem("      R: Excel");
      m_list_view.AddItem("      R: Access");
      m_list_view.AddItem("      R: Outlook");
      m_list_view.AddItem("      R: PowerPoint");
      m_list_view.AddItem("      T: Silverlight");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("      Found 6 trademarks or registered trademarks.");
      Example15_1();
     }
   //+------------------------------------------------------------------+
   //| Button 16 click.                                                 |
   //+------------------------------------------------------------------+
   void              OnClickButton16(void)
     {
      ListViewClear();
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("      Half");
      m_list_view.AddItem("      house");
      m_list_view.AddItem("      huge");
      Example16_1();
     }
   //+------------------------------------------------------------------+
   //| Button 17 click.                                                 |
   //+------------------------------------------------------------------+
   void              OnClickButton17(void)
     {
      ListViewClear();
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("      aabbccddeeffcccgghhcccciijjcccckkcc");
      m_list_view.AddItem("      aabb11ddeeff22cgghh3344iijj5566kk77");
      Example17_1();
     }
   //+------------------------------------------------------------------+
   //| Button 18 click.                                                 |
   //+------------------------------------------------------------------+
   void              OnClickButton18(void)
     {
      ListViewClear();
      m_list_view.AddItem("The example displays the following output:");
      m_list_view.AddItem("      Input: This sentence contains no numbers.");
      m_list_view.AddItem("         No matches");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("      Input: 123 What do I see?");
      m_list_view.AddItem("         123 at index 0");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("      Input: 2468 369 48 5");
      m_list_view.AddItem("         2468 at index 0");
      m_list_view.AddItem("         369 at index 5");
      m_list_view.AddItem("         48 at index 9");
      m_list_view.AddItem("         5 at index 12");
      Example18_1();
     }
   //+------------------------------------------------------------------+
   //| Button 19 click.                                                 |
   //+------------------------------------------------------------------+
   void              OnClickButton19(void)
     {
      ListViewClear();
      m_list_view.AddItem("The example produces the following output to the console:");
      m_list_view.AddItem("      3 matches found in:");
      m_list_view.AddItem("         The the quick brown fox  fox jumped over the lazy dog dog.");
      m_list_view.AddItem("      'The' repeated at positions 0 and 4");
      m_list_view.AddItem("      'fox' repeated at positions 20 and 25");
      m_list_view.AddItem("      'dog' repeated at positions 50 and 54");
      m_list_view.AddItem("      ");
      m_list_view.AddItem("      2 matches found in:");
      m_list_view.AddItem("         The the quick brown fox  fox jumped over the lazy dog dog.");
      m_list_view.AddItem("      'fox' repeated at positions 20 and 25");
      m_list_view.AddItem("      'dog' repeated at positions 50 and 54");
      Example19_1();
     }
  };
//+------------------------------------------------------------------+
//| Event Handling                                                   |
//+------------------------------------------------------------------+
EVENT_MAP_BEGIN(TestView)
ON_EVENT(ON_CLICK,m_buttons[0],OnClickButton0)
ON_EVENT(ON_CLICK,m_buttons[1],OnClickButton1)
ON_EVENT(ON_CLICK,m_buttons[2],OnClickButton2)
ON_EVENT(ON_CLICK,m_buttons[3],OnClickButton3)
ON_EVENT(ON_CLICK,m_buttons[4],OnClickButton4)
ON_EVENT(ON_CLICK,m_buttons[5],OnClickButton5)
ON_EVENT(ON_CLICK,m_buttons[6],OnClickButton6)
ON_EVENT(ON_CLICK,m_buttons[7],OnClickButton7)
ON_EVENT(ON_CLICK,m_buttons[8],OnClickButton8)
ON_EVENT(ON_CLICK,m_buttons[9],OnClickButton9)
ON_EVENT(ON_CLICK,m_buttons[10],OnClickButton10)
ON_EVENT(ON_CLICK,m_buttons[11],OnClickButton11)
ON_EVENT(ON_CLICK,m_buttons[12],OnClickButton12)
ON_EVENT(ON_CLICK,m_buttons[13],OnClickButton13)
ON_EVENT(ON_CLICK,m_buttons[14],OnClickButton14)
ON_EVENT(ON_CLICK,m_buttons[15],OnClickButton15)
ON_EVENT(ON_CLICK,m_buttons[16],OnClickButton16)
ON_EVENT(ON_CLICK,m_buttons[17],OnClickButton17)
ON_EVENT(ON_CLICK,m_buttons[18],OnClickButton18)
ON_EVENT(ON_CLICK,m_buttons[19],OnClickButton19)
EVENT_MAP_END(CAppDialog)
//+------------------------------------------------------------------+
//| Global Variables                                                 |
//+------------------------------------------------------------------+
TestView View;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- create application dialog
   if(!View.Create())
      return(INIT_FAILED);
//--- run application
   View.Run();
//--- succeed
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
   CRegex::ClearCache();
//--- destroy dialog
   View.Destroy(reason);
  }
//+------------------------------------------------------------------+
//| Expert chart event function                                      |
//+------------------------------------------------------------------+
void OnChartEvent(const int id,         // event ID  
                  const long& lparam,   // event parameter of the long type
                  const double& dparam, // event parameter of the double type
                  const string& sparam) // event parameter of the string type
  {
   View.ChartEvent(id,lparam,dparam,sparam);
  }
//+------------------------------------------------------------------+
