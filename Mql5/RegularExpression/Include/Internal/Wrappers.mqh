//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
//+------------------------------------------------------------------+
//| Purpose: Set wrapper struct for the primitive type long.         |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| BitInterpretator.                                                |
//+------------------------------------------------------------------+
union BitInterpretator
  {
   float             f;
   double            d;
   long              l;
   int               i;
  };
//+------------------------------------------------------------------+
//| Purpose: Set wrapper struct for the primitive type long.         |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Represents a 64-bit signed integer.                              |
//+------------------------------------------------------------------+
struct Int64
  {
public:
   long              l;
   static const long MaxValue;
   static const long MinValue;
  };
//+------------------------------------------------------------------+
//| Represents a 32-bit signed integer.                              |
//+------------------------------------------------------------------+
struct Int32
  {
public:
   int               i;
   static const int  MaxValue;
   static const int  MinValue;
  };
//+------------------------------------------------------------------+
//| Represents a single-precision floating-point number.             |
//+------------------------------------------------------------------+
struct Single
  {
public:
   float             f;
   static const float NaN;
   static const float NegativeInfinity;
   static const float PositiveInfinity;
   static const float Epsilon;
   static const float MaxValue;
   static const float MinValue;
   //+------------------------------------------------------------------+
   //| Returns a value that indicates whether the specified value is not|
   //| a number (NaN).                                                  |
   //+------------------------------------------------------------------+     
   static bool IsNaN(const float value)
     {
      if(value==0xFFFF000000000000)
        {
         return (true);
        }
      else
        {
         return (false);
        }
     }
   //+------------------------------------------------------------------+
   //| Returns a value indicating whether the specified number evaluates|
   //| to positive infinity.                                            |
   //+------------------------------------------------------------------+      
   static bool IsPositiveInfinity(const float value)
     {
      if(value==0x7FF0000000000000)
        {
         return (true);
        }
      else
        {
         return (false);
        }
     }
   //+------------------------------------------------------------------+
   //| Returns a value indicating whether the specified number evaluates|
   //| to negative infinity.                                            |
   //+------------------------------------------------------------------+       
   static bool IsNegativeInfinity(const float value)
     {
      if(value==0xFFF0000000000000)
        {
         return (true);
        }
      else
        {
         return (false);
        }
     }
   //+------------------------------------------------------------------+
   //| Returns a value indicating whether the specified number evaluates|
   //| to negative or positive infinity.                                |
   //+------------------------------------------------------------------+     
   static bool IsInfinity(const float value)
     {
      if(Single::IsNegativeInfinity(value) || Single::IsPositiveInfinity(value))
        {
         return (true);
        }
      else
        {
         return (false);
        }
     }
  };
//+------------------------------------------------------------------+
//| Represents a double-precision floating-point number.             |
//+------------------------------------------------------------------+
struct Double
  {
public:
   double            d;
   static const double NaN;
   static const double NegativeInfinity;
   static const double PositiveInfinity;
   static const double Epsilon;
   static const double MaxValue;
   static const double MinValue;
   //+------------------------------------------------------------------+
   //| Returns a value indicating whether the specified number evaluates|
   //| to positive infinity.                                            |
   //+------------------------------------------------------------------+      
   static bool IsPositiveInfinity(const double value)
     {

      if(value==(double)"inf")
        {
         return (true);
        }
      else
        {
         return (false);
        }
     }
   //+------------------------------------------------------------------+
   //| Returns a value indicating whether the specified number evaluates|
   //| to negative infinity.                                            |
   //+------------------------------------------------------------------+      
   static bool IsNegativeInfinity(const double value)
     {
      if(value==(double)"-inf")
        {
         return (true);
        }
      else
        {
         return (false);
        }
     }
   //+------------------------------------------------------------------+
   //| Returns a value indicating whether the specified number evaluates|
   //| to negative or positive infinity.                                |
   //+------------------------------------------------------------------+      
   static bool IsInfinity(const double value)
     {
      if(Double::IsNegativeInfinity(value) || Double::IsPositiveInfinity(value))
        {
         return (true);
        }
      else
        {
         return (false);
        }
     }
   //+------------------------------------------------------------------+
   //| Returns a value that indicates whether the specified value is not|
   //| a number (NaN).                                                  |
   //+------------------------------------------------------------------+ 
   static bool IsNaN(const double value)
     {
      if(!MathIsValidNumber(value) && !IsInfinity(value))
        {
         return (true);
        }
      else
        {
         return (false);
        }
     }
  };
//+------------------------------------------------------------------+
//| Get the hash code of the float.                                  |
//+------------------------------------------------------------------+
int GetHashCode(const float value)
  {
   if(value==0.0f)
     {
      //--- Ensure that 0 and -0 have the same hash code
      //--- return hash
      return (0);
     }
   if(value==-0.0f)
     {
      //--- Ensure that 0 and -0 have the same hash code
      //--- return hash
      return (0);
     }
   BitInterpretator converter;
   converter.f=value;
//--- return hash
   return (converter.i);
  }
//+------------------------------------------------------------------+
//| Get the hash code of the double.                                 |
//+------------------------------------------------------------------+
int GetHashCode(const double value)
  {
   if(value==0.0)
     {
      //--- Ensure that 0 and -0 have the same hash code
      //--- return hash
      return (0);
     }
   if(value==-0.0)
     {
      //--- Ensure that 0 and -0 have the same hash code
      //--- return hash
      return (0);
     }
   BitInterpretator converter;
   converter.d=value;
//--- return hash
   return (((int)converter.l) ^ ((int)(converter.l >> 32)));
  }
//+------------------------------------------------------------------+
//| Get the hash code of the integer.                                |
//+------------------------------------------------------------------+
int GetHashCode(const int value)
  {
   if(value==0.0)
     {
      //--- Ensure that 0 and -0 have the same hash code
      //--- return hash
      return (0);
     }
   if(value==-0.0)
     {
      //--- Ensure that 0 and -0 have the same hash code
      //--- return hash
      return (0);
     }
//--- return hash
   return (value);
  }
//+------------------------------------------------------------------+
//| Get the hash code of the long.                                   |
//+------------------------------------------------------------------+
int GetHashCode(const long value)
  {
   if(value==0.0)
     {
      //--- Ensure that 0 and -0 have the same hash code
      //--- return hash
      return (0);
     }
   if(value==-0.0)
     {
      //--- Ensure that 0 and -0 have the same hash code
      //--- return hash
      return (0);
     }
//--- return hash
   return (((int)((long)value)) ^ (int)(value >> 32));
  }
//+------------------------------------------------------------------+
//| Get the hash code of the string.                                 |
//+------------------------------------------------------------------+  
int GetHashCode(const string value)
  {
   int hash1=(5381<<16)+5381;
   int hash2=hash1;
   ushort arr[];
   int len=StringLen(value);
   if(len%2==0)
     {
      ArrayResize(arr,len);
     }
   else
     {
      ArrayResize(arr,len+1);
     }
   for(int i=0; i<ArraySize(arr); i++)
     {
      arr[i]=StringGetCharacter(value,i);
     }
   int i=0;
   ushort v0,v1;
   string hex;
   while(len>2)
     {
      v0=arr[i];
      v1=arr[i+1];
      hex=IntegerToHex(v1)+"00"+IntegerToHex(v0);
      int p0=HexToInteger(hex);
      i+=2;
      v0=arr[i];
      v1=arr[i+1];
      hex=IntegerToHex(v1)+"00"+IntegerToHex(v0);
      int p1= HexToInteger(hex);
      hash1 = ((hash1 << 5) + hash1 + (hash1 >> 27)) ^ p0;
      hash2 = ((hash2 << 5) + hash2 + (hash2 >> 27)) ^ p1;
      i+=2;
      len-=4;
     }
   if(len>0)
     {
      v0=arr[i];
      v1=arr[i+1];
      hex=IntegerToHex(v1)+"00"+IntegerToHex(v0);
      int p=HexToInteger(hex);
      hash1=((hash1<<5)+hash1+(hash1>>27))^(int)p;
     }
//--- return hash
   return (hash1 + (hash2 * 1566083941));
  };
//+------------------------------------------------------------------+
//| Initialization constants.                                        |
//+------------------------------------------------------------------+
const long Int64::MaxValue=LONG_MAX;
const long Int64::MinValue=LONG_MIN;
const int Int32::MaxValue=INT_MAX;
const int Int32::MinValue=INT_MIN;
const float Single::NegativeInfinity=(float)-MathExp(DBL_MAX);
const float Single::PositiveInfinity=(float)MathExp(DBL_MAX);
const float Single::NaN=(float)Single::PositiveInfinity/Single::NegativeInfinity;
const float Single::Epsilon=FLT_EPSILON;
const float Single::MaxValue=FLT_MAX;
const float Single::MinValue=FLT_MIN;
const double Double::NegativeInfinity=-MathExp(DBL_MAX);
const double Double::PositiveInfinity=MathExp(DBL_MAX);
const double Double::NaN=Double::PositiveInfinity/Double::NegativeInfinity;
const double Double::Epsilon=DBL_EPSILON;
const double Double::MaxValue=DBL_MAX;
const double Double::MinValue=DBL_MIN;
//+------------------------------------------------------------------+
//| Consvert integer number to binary (base 2)                       |
//+------------------------------------------------------------------+
string IntegerToBinary(int i)
  {
   if(i==0)
     {
      return ("0");
     }
   if(i<0)
     {
      return ("-" + IntegerToBinary(-i));
     }
   string out="";
   for(; i!=0; i/=2)
     {
      out=string(i%2)+out;
     }
   return (out);
  }
//+------------------------------------------------------------------+
//| Consvert integer number to hex (base 16)                         |
//+------------------------------------------------------------------+
string IntegerToHex(ushort value)
  {
   string zero=(value>9) ? "" : "0";
   string hex="";
   while(value>=16)
     {
      ushort div=value/16;
      int mod=(int)MathMod(value,16.0);
      switch(mod)
        {
         case 10: { hex="a"+hex; break; }
         case 11: { hex="b"+hex; break; }
         case 12: { hex="c"+hex; break; }
         case 13: { hex="d"+hex; break; }
         case 14: { hex="e"+hex; break; }
         case 15: { hex="f"+hex; break; }
         default: hex=IntegerToString(mod)+hex;
        }
      value=div;
     }
   return (zero+IntegerToString(value)+hex);
  }
//+------------------------------------------------------------------+
//| Convert string representation of hex number (base 16) to integer.|
//+------------------------------------------------------------------+
int HexToInteger(string hex)
  {
   int p=StringLen(hex)-1;
   int res=0;
   for(int i=0; i<StringLen(hex); i++)
     {
      char symbol=(char)StringGetCharacter(hex,i);
      int value;
      switch(symbol)
        {
         case 'a': value = 10; break;
         case 'b': value = 11; break;
         case 'c': value = 12; break;
         case 'd': value = 13; break;
         case 'e': value = 14; break;
         case 'f': value = 15; break;
         default: value=(int)CharToString(symbol);
        }
      res+=value*(int)MathPow(16,p);
      p--;
     }
   return (res);
  }
//+------------------------------------------------------------------+
