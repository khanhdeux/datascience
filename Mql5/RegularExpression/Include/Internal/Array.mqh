//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
#include <Internal\Generic\IComparer.mqh>
#include <Internal\IComparable.mqh>
#include <Internal\Generic\IEnumerator.mqh> 
//+------------------------------------------------------------------+
//| Purpose:  Base class for all arrays.                             |
//+------------------------------------------------------------------+
class Array : public IComparable
  {
protected:
   int               m_version;
public:
   //--- Methods:
   //+------------------------------------------------------------------+
   //| Gets the rank (number of dimensions) of the Array.For example, a |
   //| one-dimensional array returns 1, a two-dimensional array returns |
   //| 2, and so on.                                                    |
   //+------------------------------------------------------------------+
   virtual int       Rank()
     {
      return (-1);
     }
   //+------------------------------------------------------------------+
   //| Gets the index of the last element of the specified dimension in |
   //| the array.                                                       |
   //+------------------------------------------------------------------+
   virtual int       GetUpperBound(const int dimension)
     {
      return (-1);
     }
   //+------------------------------------------------------------------+
   //| Gets version of array.                                           |
   //+------------------------------------------------------------------+
   virtual int Version()
     {
      return (m_version);
     }
   //+------------------------------------------------------------------+
   //| Sets a range of elements in an array to the default value of each|
   //| element type.                                                    |
   //+------------------------------------------------------------------+
   template<typename T>
   static void Clear(T &array[],const int start,const int count)
     {
      for(int i=start; i<count; i++)
        {
         array[i]=NULL;
        }
     }
   //+------------------------------------------------------------------+
   //| The function fills an array with the specified value.            |
   //+------------------------------------------------------------------+
   template<typename T>
   static void Fill(T &array[],const int start,const int count,T value)
     {
      for(int i=start; i<count; i++)
        {
         array[i]=T;
        }
     }
   //+------------------------------------------------------------------+
   //| Sorts the elements of two CRow<T> based on the keys in the first | 
   //| CRow<T>. Elements in the keys CRow<T> specify the sort keys for  |
   //| corresponding elements in the items CRow<T>. The sort compares   |
   //| the keys to each other using the IComparable interface, which    |
   //| must be implemented by all elements of the keys CRow<T>.         |
   //+------------------------------------------------------------------+
   template<typename TKey,typename TItem>
   static void Sort(TKey &keys[],TItem &items[],IComparer<TKey> *comparer)
     {
      Array::Sort(keys,items,0,ArraySize(keys),comparer);
     }
   //+------------------------------------------------------------------+
   //| Sorts the elements of two arrays based on the keys in the first  | 
   //| array. Elements in the keys array specify the sort keys for      |
   //| corresponding elements in the items array. The sort compares the |
   //| keys to each other using the IComparable interface, which must be|
   //| implemented by all elements of the keys array.                   |
   //+------------------------------------------------------------------+
   template<typename TKey,typename TItem>
   static void Sort(TKey &keys[],TItem &items[])
     {
      IComparer<TKey>*comparer=NULL;
      Array::Sort(keys,items,0,ArraySize(keys),comparer);
     }
   //+------------------------------------------------------------------+
   //| Sorts the elements in an entire one-dimensional Array using the  |
   //| IComparable implementation of each element of the Array.         |
   //+------------------------------------------------------------------+
   template<typename TKey,typename TItem>
   static void Sort(TKey &keys[],TItem &items[],int index,int length,IComparer<TKey> *comparer)
     {
      IComparer<TKey>*c=comparer;
      SorterGenericArray<TKey,TItem>sorter(keys,items,c);
      sorter.Sort(index,length);
      ArrayCopy(keys,sorter.keys);
      ArrayCopy(items,sorter.items);
     }
   //+------------------------------------------------------------------+
   //| Purpose: Searches an entire one-dimensional sorted array for a   |
   //| specific element, using the IComparable<T> generic interface     |
   //| implemented by each element of the Array and by the specified    |
   //| object.                                                          |
   //+------------------------------------------------------------------+
   template<typename T,typename TComparer>
   static int BinarySearch(T *&array[],const int index,const int length,T *value,TComparer *comparer)
     {
      IComparer<T>*c=comparer;
      int lo=index;
      int hi=index+length-1;
      while(lo<=hi)
        {
         int i=lo+((hi-lo)>>1);
         int order=c.Compare(array[i],value);
         if(order==0)
           {
            return (i);
           }
         if(order<0)
           {
            lo=i+1;
           }
         else
           {
            hi=i-1;
           }
        }
      return (~lo);
     }
   //+------------------------------------------------------------------+
   //| Returns an IEnumerator for the Array.                            |
   //+------------------------------------------------------------------+
   template<typename T>
   static IEnumerator<T>*GetEnumerator(T &array[])
     {
      IEnumerator<T>*enumerator=new IEnumerator<T>();
      for(int i=0; i<ArraySize(array); i++)
        {
         enumerator.Add(array[i]);
        }
      return (enumerator);
     }
   //+------------------------------------------------------------------+
   //| Searches for the specified object and returns the index of its   |
   //| first occurrence in a one-dimensional array.                     |
   //+------------------------------------------------------------------+
   template<typename T>
   static int IndexOf(T &array[],T value,const int startIndex,const int count)
     {
      int lb=0;
      if(startIndex<lb || startIndex>ArraySize(array)+lb)
        {
         Print("Argument 'startIndex' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (-1);
        }
      if(count<0 || count>ArraySize(array)-startIndex+lb)
        {
         Print("Argument 'count' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (-1);
        }
      int endIndex=startIndex+count;
      if(value==NULL)
        {
         for(int i=startIndex; i<endIndex; i++)
           {
            if(array[i]==NULL)
              {
               return (i);
              }
           }
        }
      else
        {
         for(int i=startIndex; i<endIndex; i++)
           {
            if(array[i]!=NULL && array[i]==value)
              {
               return (i);
              }
           }
        }
      //--- Return one less than the lower bound of the array.  This way, for
      //--- arrays with a lower bound of -1 we will not return -1 when the   
      //--- item was not found.  And for SZArrays (the vast majority), -1    
      //--- still works for them.                                            
      return (lb-1);
     }
   //+------------------------------------------------------------------+
   //| Returns the index of the last occurrence of a value in a         |
   //| one-dimensional Array or in a portion of the Array.              |
   //+------------------------------------------------------------------+
   template<typename T>
   static int LastIndexOf(T &array[],T value,const int startIndex,const int count)
     {
      int lb=0;
      if(ArraySize(array)==0)
        {
         return (lb-1);
        }
      if(startIndex<lb || startIndex>=ArraySize(array)+lb)
        {
         Print("Argument 'startIndex' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (-1);
        }
      if(count<0)
        {
         Print("Argument 'count' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (-1);
        }
      if(count>startIndex-lb+1)
        {
         Print("endIndex' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (-1);
        }
      int endIndex=startIndex-count+1;
      if(value==NULL)
        {
         for(int i=startIndex; i>=endIndex; i--)
           {
            if(array[i]==NULL)
              {
               return (i);
              }
           }
        }
      else
        {
         for(int i=startIndex; i>=endIndex; i--)
           {
            if(array[i]!=NULL && array[i]==value)
              {
               return (i);
              }
           }
        }
      //--- return lb-1 for arrays with negative lower bounds.
      return (lb-1);
     }
   //+------------------------------------------------------------------+
   //| Reverses the elements in a range of this list. Following a call  |
   //| to this method, an element in the range given by index and count |
   //| which was previously located at index i will now be located at   |
   //| index index + (index + count - i - 1).                           |
   //+------------------------------------------------------------------+
   template<typename T>
   static void Reverse(T &array[],const int index,const int length)
     {
      if(length<0)
        {
         Print("Invalid argument."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      if(ArraySize(array) -(index-ArrayRange(array,0))<length)
        {
         Print("Invalid argument."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      if(ArrayRange(array,1)!=0)
        {
         Print("Invalid argument."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      int i = index;
      int j = index + length - 1;
      while(i<j)
        {
         T temp=array[i];
         array[i] = array[j];
         array[j] = temp;
         i++;
         j--;
        }
     }
  };
//+------------------------------------------------------------------+
//| Struct used by the Sort methods for instances of Array.          |
//+------------------------------------------------------------------+
template<typename TKey,typename TItem>
struct SorterGenericArray
  {
private:
   IComparer<TKey>*comparer;
   bool              inPlace;
public:
   TKey              keys[];
   TItem             items[];
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     SorterGenericArray(TKey &k[],TItem &i[],IComparer<TKey>*c)
     {
      ArrayCopy(keys,k);
      ArrayCopy(items,i);
      if(c!=NULL)
        {
         inPlace=true;
         this.comparer=c;
        }
      else
        {
         inPlace=false;
         this.comparer=new DefaultComparer<TKey>();
        }
     }
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~SorterGenericArray()
     {
      if(!inPlace)
        {
         delete comparer;
        }
     }   
   //+------------------------------------------------------------------+
   //| SwapIfGreaterWithItem.                                           |
   //+------------------------------------------------------------------+
   void SwapIfGreaterWithItems(const int a,const int b)
     {
      if(a!=b)
        {
         if(comparer.Compare(keys[a],keys[b])>0)
           {
            TKey key=keys[a];
            keys[a]=keys[b];
            keys[b]=key;
            if(ArraySize(items)!=NULL)
              {
               TItem item=items[a];
               items[a]=items[b];
               items[b]=item;
              }
           }
        }
     }
   //+------------------------------------------------------------------+
   //| Swap.                                                            |
   //+------------------------------------------------------------------+
   void Swap(const int i,const int j)
     {
      TKey key=keys[i];
      keys[i]=keys[j];
      keys[j]=key;
      if(ArraySize(items)!=NULL)
        {
         TItem item=items[i];
         items[i]=items[j];
         items[j]=item;
        }
     }
   //+------------------------------------------------------------------+
   //| Sort.                                                            |
   //+------------------------------------------------------------------+
   void Sort(const int left,const int length)
     {
      IntrospectiveSort(left,length);
     }
private:
   //+------------------------------------------------------------------+
   //| FloorLog2.                                                       |
   //+------------------------------------------------------------------+
   static int FloorLog2(int n)
     {
      int result=0;
      while(n>=1)
        {
         result++;
         n=n/2;
        }
      return (result);
     }
   //+------------------------------------------------------------------+
   //| IntrospectiveSort.                                               |
   //+------------------------------------------------------------------+     
   void IntrospectiveSort(const int left,const int length)
     {
      if(length<2)
        {
         return;
        }
      IntroSort(left,length+left-1,2*FloorLog2(ArraySize(keys)));
     }
   //+------------------------------------------------------------------+
   //| IntroSort.                                                       |
   //+------------------------------------------------------------------+
   void IntroSort(int lo,int hi,int depthLimit)
     {
      const int IntrosortSizeThreshold=16;
      while(hi>lo)
        {
         int partitionSize = hi - lo + 1;
         if(partitionSize <= IntrosortSizeThreshold)
           {
            if(partitionSize==1)
              {
               return;
              }
            if(partitionSize==2)
              {
               SwapIfGreaterWithItems(lo,hi);
               return;
              }
            if(partitionSize==3)
              {
               SwapIfGreaterWithItems(lo,hi-1);
               SwapIfGreaterWithItems(lo,hi);
               SwapIfGreaterWithItems(hi-1,hi);
               return;
              }
            InsertionSort(lo,hi);
            return;
           }
         if(depthLimit==0)
           {
            Heapsort(lo,hi);
            return;
           }
         depthLimit--;
         int p=PickPivotAndPartition(lo,hi);
         IntroSort(p+1,hi,depthLimit);
         hi=p-1;
        }
     }
   //+------------------------------------------------------------------+
   //| PickPivotAndPartition.                                           |
   //+------------------------------------------------------------------+
   int PickPivotAndPartition(const int lo,const int hi)
     {
      //--- Compute median-of-three.  But also partition them, since we've done the comparison.
      int mid=lo+(hi-lo)/2;
      SwapIfGreaterWithItems(lo,mid);
      SwapIfGreaterWithItems(lo,hi);
      SwapIfGreaterWithItems(mid,hi);
      TKey pivot=keys[mid];
      Swap(mid,hi-1);
      int left=lo,right=hi-1;  // We already partitioned lo and hi and put the pivot in hi - 1.  And we pre-increment & decrement below.
      while(left<right)
        {
         while(comparer.Compare(keys[++left], pivot) < 0);
         while(comparer.Compare(pivot, keys[--right]) < 0);
         if(left>=right)
           {
            break;
           }
         Swap(left,right);
        }
      //--- Put pivot in the right location.
      Swap(left,(hi-1));
      return (left);
     }
   //+------------------------------------------------------------------+
   //| Heapsort.                                                        |
   //+------------------------------------------------------------------+
   void Heapsort(const int lo,const int hi)
     {
      int n=hi-lo+1;
      for(int i=n/2; i>=1; i=i-1)
        {
         DownHeap(i,n,lo);
        }
      for(int i=n; i>1; i=i-1)
        {
         Swap(lo,lo+i-1);
         DownHeap(1,i-1,lo);
        }
     }
   //+------------------------------------------------------------------+
   //| DownHeap.                                                        |
   //+------------------------------------------------------------------+
   void DownHeap(int i,const int n,int lo)
     {
      TKey d=keys[lo+i-1];
      TItem dt=items[lo+i-1];
      int child;
      while(i<=n/2)
        {
         child=2*i;
         if(child<n && comparer.Compare(keys[lo+child-1],keys[lo+child])<0)
           {
            child++;
           }
         if(!(comparer.Compare(d,keys[lo+child-1])<0))
           {
            break;
           }
         keys[lo+i-1]=keys[lo+child-1];
         if(true)//(items!=NULL)
           {
            items[lo+i-1]=items[lo+child-1];
           }
         i=child;
        }
      keys[lo+i-1]=d;
      if(ArraySize(items)!=NULL)
        {
         items[lo+i-1]=dt;
        }
     }
   //+------------------------------------------------------------------+
   //| InsertionSort.                                                   |
   //+------------------------------------------------------------------+
   void InsertionSort(const int lo,const int hi)
     {
      int i,j;
      TKey t;
      TItem dt;
      for(i=lo; i<hi; i++)
        {
         j = i;
         t = keys[i + 1];
         dt=(ArraySize(items)!=NULL) ? (TItem)items[i+1] : (TItem)NULL;
         while(j>=lo && comparer.Compare(t,keys[j])<0)
           {
            keys[j+1]=keys[j];
            if(ArraySize(items)!=NULL)
              {
               items[j+1]=items[j];
              }
            j--;
           }
         keys[j+1]=t;
         if(ArraySize(items)!=NULL)
           {
            items[j+1]=dt;
           }
        }
     }
  };
//+------------------------------------------------------------------+
