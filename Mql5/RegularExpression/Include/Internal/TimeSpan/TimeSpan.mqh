//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
#include <Internal\Wrappers.mqh>
#include "TimeSpanFormat.mqh"
//+------------------------------------------------------------------+
//| Purpose: TimeSpan represents a duration of time.  A TimeSpan can |
//| be negative or positive.                                         |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| TimeSpan is internally represented as a number of milliseconds.  |
//| While this maps well into units of time such as hours and days,  |
//| any periods longer than that aren't representable in a nice      |
//| fashion. For instance, a month can be between 28 and 31 days,    |
//| while a year can contain 365 or 364 days.  A decade can have     |
//| between 1 and 3 leapyears, depending on when you map the TimeSpan|
//| into the calendar.  This is why we do not provide Years() or     |
//| Months().                                                        |
//+------------------------------------------------------------------+
struct TimeSpan
  {
private:
   long              m_ticks;
   static const double MillisecondsPerTick;
   static const double SecondsPerTick;// 0.0001
   static const double MinutesPerTick;// 1.6666666666667e-9
   static const double HoursPerTick;  // 2.77777777777777778e-11
   static const double DaysPerTick;   // 1.1574074074074074074e-12
   static const int  MillisPerSecond;
   static const int  MillisPerMinute; // 60,000
   static const int  MillisPerHour;   //  3,600,000
   static const int  MillisPerDay;    // 86,400,000
public:
   static const long TicksPerMillisecond;
   static const long TicksPerSecond;  // 10,000,000
   static const long TicksPerMinute;  // 600,000,000
   static const long TicksPerHour;    // 36,000,000,000
   static const long TicksPerDay;     // 864,000,000,000
   static const long MaxSeconds;
   static const long MinSeconds;
   static const long MaxMilliSeconds;
   static const long MinMilliSeconds;
   static const long TicksPerTenthSecond;
   //+------------------------------------------------------------------+
   //| Constructor without parameters.                                  |
   //+------------------------------------------------------------------+
                     TimeSpan()
     {
      m_ticks=0;
     }
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+     
                     TimeSpan(const long ticks)
     {
      this.m_ticks=ticks;
     }
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+  
                     TimeSpan(const int hours,const int minutes,const int seconds)
     {
      m_ticks=TimeToTicks(hours,minutes,seconds);
     }
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+  
                     TimeSpan(const int days,const int hours,const int minutes,const int seconds)
     {
      int milliseconds=0;
      long totalMilliSeconds=((long)days*3600*24+(long)hours*3600+(long)minutes*60+seconds)*1000+milliseconds;
      if(totalMilliSeconds>MaxMilliSeconds || totalMilliSeconds<MinMilliSeconds)
        {
         Print("Argument 'totalMilliSeconds' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
        }
      m_ticks=(long)totalMilliSeconds*TicksPerMillisecond;
     }
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+  
                     TimeSpan(const int days,const int hours,const int minutes,const int seconds,const int milliseconds)
     {
      long totalMilliSeconds=((long)days*3600*24+(long)hours*3600+(long)minutes*60+seconds)*1000+milliseconds;
      if(totalMilliSeconds>MaxMilliSeconds || totalMilliSeconds<MinMilliSeconds)
        {
         Print("Argument 'totalMilliSeconds' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
        }
      m_ticks=(long)totalMilliSeconds*TicksPerMillisecond;
     }
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+  
                     TimeSpan(TimeSpan &value)
     {
      m_ticks=value.Ticks();
     }
   //+------------------------------------------------------------------+
   //| Get the zero time span.                                          |
   //+------------------------------------------------------------------+
   static TimeSpan Zero()
     {
      TimeSpan r(0);
      return (r);
     }
   //+------------------------------------------------------------------+
   //| Gets the max time span.                                          |
   //+------------------------------------------------------------------+
   static TimeSpan MaxValue()
     {
      TimeSpan r(Int64::MaxValue);
      return (r);
     }
   //+------------------------------------------------------------------+
   //| Gets the min time span.                                          |
   //+------------------------------------------------------------------+
   static TimeSpan MinValue()
     {
      TimeSpan r(Int64::MinValue);
      return (r);
     }
   //+------------------------------------------------------------------+
   //| Gets tiks.                                                       |
   //+------------------------------------------------------------------+    
   long Ticks()const
     {
      return (m_ticks);
     }
   //+------------------------------------------------------------------+
   //| Gets days.                                                       |
   //+------------------------------------------------------------------+        
   int Days()
     {
      return(int)(m_ticks/TicksPerDay);
     }
   //+------------------------------------------------------------------+
   //| Gets hours.                                                      |
   //+------------------------------------------------------------------+     
   int Hours()
     {
      return ((int)((m_ticks/TicksPerHour)%24));
     }
   //+------------------------------------------------------------------+
   //| Gets milliseconds.                                               |
   //+------------------------------------------------------------------+   
   int Milliseconds()
     {
      return ((int)((m_ticks/TicksPerMillisecond)%1000));
     }
   //+------------------------------------------------------------------+
   //| Gets minutes.                                                    |
   //+------------------------------------------------------------------+   
   int Minutes()
     {
      return ((int)((m_ticks/TicksPerMinute)%60));
     }
   //+------------------------------------------------------------------+
   //| Gets seconds.                                                    |
   //+------------------------------------------------------------------+    
   int Seconds()
     {
      return ((int)((m_ticks/TicksPerSecond)%60));
     }
   //+------------------------------------------------------------------+
   //| Gets toatal days.                                                |
   //+------------------------------------------------------------------+       
   double TotalDays()
     {
      return (((double)m_ticks)*DaysPerTick);
     }
   //+------------------------------------------------------------------+
   //| Gets toatal housrs.                                              |
   //+------------------------------------------------------------------+       
   double TotalHours()
     {
      return ((double)m_ticks*HoursPerTick);
     }
   //+------------------------------------------------------------------+
   //| Gets toatal milliseconds.                                        |
   //+------------------------------------------------------------------+    
   double TotalMilliseconds() const
     {
      double temp=(double)m_ticks*MillisecondsPerTick;
      if(temp>MaxMilliSeconds)
        {
         //---
         return ((double)MaxMilliSeconds);
        }
      if(temp<MinMilliSeconds)
        {
         //---
         return (double)MinMilliSeconds;
        }
      //--- 
      return (temp);
     }
   //+------------------------------------------------------------------+
   //| Gets toatal minutes.                                             |
   //+------------------------------------------------------------------+   
   double TotalMinutes()
     {
      return ((double)m_ticks*MinutesPerTick);
     }
   //+------------------------------------------------------------------+
   //| Gets toatal seconds.                                             |
   //+------------------------------------------------------------------+   
   double TotalSeconds()
     {
      return ((double)m_ticks*SecondsPerTick);
     }
   //+------------------------------------------------------------------+
   //| Add.                                                             |
   //+------------------------------------------------------------------+   
   TimeSpan Add(const TimeSpan &ts)
     {
      long result=m_ticks+ts.m_ticks;
      //--- Overflow if signs of operands was identical and result's
      //--- sign was opposite.
      //--- >> 63 gives the sign bit (either 64 1's or 64 0's).
      if((m_ticks>>63==ts.m_ticks>>63) && (m_ticks>>63!=result>>63))
        {
         Print("Overflow."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (TimeSpan::Zero());
        }
      TimeSpan r(result);
      return (r);
     }
   //+------------------------------------------------------------------+
   //| Compares two TimeSpan values, returning an integer that indicates|
   //| their relationship.                                              |
   //+------------------------------------------------------------------+
   static int Compare(TimeSpan &t1,TimeSpan &t2)
     {
      if(t1.m_ticks>t2.m_ticks)
        {
         return (1);
        }
      if(t1.m_ticks<t2.m_ticks)
        {
         return (-1);
        }
      return (0);
     }
   //+------------------------------------------------------------------+
   //| Returns a value less than zero if this  object.                  |
   //+------------------------------------------------------------------+
   template<typename T>
   int CompareTo(T &value)
     {
      if(!(IsTypeOf(value,"TimeSpan")))
        {
         Print("Different types."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (0);
        }
      long t=((TimeSpan)value)._ticks;
      if(_ticks>t)
        {
         return (1);
        }
      if(_ticks<t)
        {
         return (-1);
        }
      return (0);
     }
   //+------------------------------------------------------------------+
   //| Returns a TimeSpan that represents a specified number of days,   |
   //| where the specification is accurate to the nearest millisecond.  |
   //+------------------------------------------------------------------+     
   static TimeSpan FromDays(const double value)
     {
      return Interval(value, MillisPerDay);
     }
   //+------------------------------------------------------------------+
   //| Returns a new TimeSpan object whose value is the absolute value  |
   //| of the current TimeSpan object.                                  |
   //+------------------------------------------------------------------+     
   TimeSpan Duration()
     {
      if(Ticks()==MinValue().Ticks())
        {
         Print("Overflow."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (TimeSpan::Zero());
        }
      TimeSpan result(m_ticks>=0? m_ticks: -m_ticks);
      return (result);
     }
   //+------------------------------------------------------------------+
   //| Returns a value indicating whether two instances of TimeSpan are |
   //| equal.                                                           |
   //+------------------------------------------------------------------+     
   template<typename T>
   bool Equals(const T &value)
     {
      if(IsTypeOf(value,"TimeSpan"))
        {
         return (_ticks == ((TimeSpan)value)._ticks);
        }
      return (false);
     }
   //+------------------------------------------------------------------+
   //| Returns a value indicating whether two instances of TimeSpan are |
   //| equal.                                                           |
   //+------------------------------------------------------------------+ 
   static bool Equals(TimeSpan &t1,TimeSpan &t2)
     {
      return (t1.m_ticks == t2.m_ticks);
     }
   //+------------------------------------------------------------------+
   //| Returns a hash code for this instance.                           |
   //+------------------------------------------------------------------+     
   int GetHashCode()
     {
      return (int)m_ticks ^ (int)(m_ticks >> 32);
     }
   //+------------------------------------------------------------------+
   //| Returns a TimeSpan that represents a specified number of hours,  |
   //| where the specification is accurate to the nearest millisecond.  |
   //+------------------------------------------------------------------+     
   static TimeSpan FromHours(const double value)
     {
      return Interval(value, MillisPerHour);
     }
private:
   //+------------------------------------------------------------------+
   //| Interval.                                                        |
   //+------------------------------------------------------------------+
   static TimeSpan Interval(const double value,const int scale)
     {
      if(Double::IsNaN(value))
        {
         Print("Argument 'value' = NaN."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (TimeSpan::Zero());
        }
      double tmp=value*scale;
      double millis=tmp+(value>=0? 0.5: -0.5);
      if((millis>Int64::MaxValue/TicksPerMillisecond) || (millis<Int64::MinValue/TicksPerMillisecond))
        {
         Print("Overflow."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (TimeSpan::Zero());
        }
      TimeSpan result((long)millis*TicksPerMillisecond);
      return (result);
     }
public:
   //+------------------------------------------------------------------+
   //| Returns a TimeSpan that represents a specified number of         |
   //| milliseconds.                                                    |
   //+------------------------------------------------------------------+
   static TimeSpan FromMilliseconds(const double value)
     {
      return Interval(value, 1);
     }
   //+------------------------------------------------------------------+
   //| Returns a TimeSpan that represents a specified number of minutes,|
   //| where the specification is accurate to the nearest millisecond.  |
   //+------------------------------------------------------------------+
   static TimeSpan FromMinutes(const double value)
     {
      return Interval(value, MillisPerMinute);
     }
   //+------------------------------------------------------------------+
   //| Returns a new TimeSpan object whose value is the negated value of|
   //| this instance.                                                   |
   //+------------------------------------------------------------------+
   TimeSpan Negate()
     {
      if(Ticks()==MinValue().Ticks())
        {
         Print("Overflow."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (TimeSpan::Zero());
        }
      TimeSpan result(-m_ticks);
      return (result);
     }
   //+------------------------------------------------------------------+
   //| Returns a TimeSpan that represents a specified number of seconds,|
   //| where the specification is accurate to the nearest millisecond.  |
   //+------------------------------------------------------------------+
   static TimeSpan FromSeconds(const double value)
     {
      return Interval(value, MillisPerSecond);
     }
   //+------------------------------------------------------------------+
   //| Returns a new TimeSpan object whose value is the difference      |
   //| between the specified TimeSpan object and this instance.         |
   //+------------------------------------------------------------------+     
   TimeSpan Subtract(TimeSpan &ts)
     {
      long result=m_ticks-ts.m_ticks;
      //--- Overflow if signs of operands was different and result's
      //--- sign was opposite from the first argument's sign.
      //--- >> 63 gives the sign bit (either 64 1's or 64 0's).
      if((m_ticks>>63!=ts.m_ticks>>63) && (m_ticks>>63!=result>>63))
        {
         Print("Overflow."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (TimeSpan::Zero());
        }
      TimeSpan r(result);
      return (r);
     }
   //+------------------------------------------------------------------+
   //| Returns a TimeSpan that represents a specified time, where the   |
   //| specification is in units of ticks.                              |
   //+------------------------------------------------------------------+
   static TimeSpan FromTicks(const long value)
     {
      TimeSpan result(value);
      return (result);
     }
   //+------------------------------------------------------------------+
   //| TimeToTicks.                                                     |
   //+------------------------------------------------------------------+     
   static long TimeToTicks(const int hour,const int minute,const int second)
     {
      //--- totalSeconds is bounded by 2^31 * 2^12 + 2^31 * 2^8 + 2^31, which is less than 2^44, meaning we won't overflow totalSeconds.
      long totalSeconds=(long)hour*3600+(long)minute*60+(long)second;
      if(totalSeconds>MaxSeconds || totalSeconds<MinSeconds)
        {
         Print("Argument 'totalSeconds' = out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (0);
        }
      return (totalSeconds * TicksPerSecond);
     }
   //+------------------------------------------------------------------+
   //| Converts the string representation of a time interval to its     |
   //| TimeSpan equivalent.                                             |
   //+------------------------------------------------------------------+
   static TimeSpan Parse(const string s)
     {
      TimeSpan result;
      if(TryParese(s,result))
        {
         return (result);
        }
      else
        {
         Print("Wrong string."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (TimeSpan::Zero());
        }
     }
   //+------------------------------------------------------------------+
   //| Converts the specified string representation of a time interval  |
   //| to its TimeSpan equivalent and returns a value that indicates    |
   //| whether the conversion succeeded.                                |
   //+------------------------------------------------------------------+     
   static bool TryParese(const string s,TimeSpan &result)
     {
      string str=s;
      const int maxDays=10675199;
      const int maxHours    = 23;
      const int maxMinutes  = 59;
      const int maxSeconds  = 59;
      StringTrimLeft(str);
      StringTrimRight(str);
      if(StringLen(str)==0)
        {
         Print("String is empty."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (false);
        }
      string strs[];
      int count=StringSplit(s,':',strs);
      Alert(count);
      for(int i=0; i<count; i++)
        {
         string value=strs[i];
         if(value=="")
           {
            Print("Wrong string."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
            return (false);
           }
        }
      int days=0;
      int hours=0;
      int minutes=0;
      int seconds=0;
      count--;
      switch(count)
        {
         case 0:
           {
            days=(int)StringToInteger(strs[0]);
            break;
           }
         case 1:
           {
            days=(int)StringToInteger(strs[0]);
            hours=(int)StringToInteger(strs[1]);
            break;
           }
         case 2:
           {
            days=(int)StringToInteger(strs[0]);
            hours=(int)StringToInteger(strs[1]);
            minutes=(int)StringToInteger(strs[2]);
            break;
           }
         case 3:
           {
            days=(int)StringToInteger(strs[0]);
            hours=(int)StringToInteger(strs[1]);
            minutes=(int)StringToInteger(strs[2]);
            seconds=(int)StringToInteger(strs[3]);
            break;
           }
         default:
           {
            Print("String is empty."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
            return (false);
           }
        }
      if(days>maxDays || hours>maxHours || minutes>maxMinutes || seconds>maxSeconds)
        {
         Print("String is empty."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (false);
        }
      long ticks=((long)days*3600*24+(long)hours*3600+(long)minutes*60+seconds)*1000;
      if(ticks>MaxMilliSeconds || ticks<MinMilliSeconds)
        {
         Print("Overflow."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (false);
        }
      result.m_ticks=((long)ticks*TimeSpan::TicksPerMillisecond);
      return (true);
     }
   //+------------------------------------------------------------------+
   //| Converts the value of the current TimeSpan object to its         |
   //| equivalent string representation.                                |
   //+------------------------------------------------------------------+     
   string ToString()
     {
      return (TimeSpanFormat::Format(this,NULL));
     }
   //+------------------------------------------------------------------+
   //| Converts the value of the current TimeSpan object to its         |
   //| equivalent string representation.                                |
   //+------------------------------------------------------------------+ 
   string ToString(const string format)
     {
      return (TimeSpanFormat::Format(this,format));
     }
   //+------------------------------------------------------------------+
   //| Operator (-).                                                    |
   //+------------------------------------------------------------------+ 
   TimeSpan operator -()
     {
      if(this.m_ticks==MinValue().m_ticks)
        {
         Print("Overflow."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (TimeSpan::Zero());
        }
      TimeSpan result(-this.m_ticks);
      return (result);
     }
   //+------------------------------------------------------------------+
   //| Operator (-).                                                    |
   //+------------------------------------------------------------------+      
   TimeSpan operator -(TimeSpan &t)
     {
      return (this.Subtract(t));
     }
   //+------------------------------------------------------------------+
   //| Operator (+).                                                    |
   //+------------------------------------------------------------------+      
   TimeSpan operator+()
     {
      return (this);
     }
   //+------------------------------------------------------------------+
   //| Operator (+).                                                    |
   //+------------------------------------------------------------------+ 
   TimeSpan operator+(const TimeSpan &t)
     {
      return (this.Add(t));
     }
   //+------------------------------------------------------------------+
   //| Operator (==).                                                   |
   //+------------------------------------------------------------------+ 
   bool operator==(const TimeSpan &t) const
     {
      return (this.m_ticks == t.m_ticks);
     }
   //+------------------------------------------------------------------+
   //| Operator (!=).                                                   |
   //+------------------------------------------------------------------+ 
   bool operator!=(const TimeSpan &t) const
     {
      return (this.m_ticks != t.m_ticks);
     }
   //+------------------------------------------------------------------+
   //| Operator (<).                                                    |
   //+------------------------------------------------------------------+ 
   bool operator<(const TimeSpan &t) const
     {
      return (this.m_ticks < t.m_ticks);
     }
   //+------------------------------------------------------------------+
   //| Operator (<=).                                                   |
   //+------------------------------------------------------------------+   
   bool operator<=(const TimeSpan &t) const
     {
      return (this.m_ticks <= t.m_ticks);
     }
   //+------------------------------------------------------------------+
   //| Operator (>).                                                    |
   //+------------------------------------------------------------------+   
   bool operator>(const TimeSpan &t) const
     {
      return (this.m_ticks > t.m_ticks);
     }
   //+------------------------------------------------------------------+
   //| Operator (>=).                                                   |
   //+------------------------------------------------------------------+   
   bool operator>=(const TimeSpan &t) const
     {
      return (this.m_ticks >= t.m_ticks);
     }
   //+------------------------------------------------------------------+
   //| Operator (=).                                                    |
   //+------------------------------------------------------------------+     
   void operator=(const TimeSpan &t)
     {
      this.m_ticks=t.Ticks();
     }
  };
static const long TimeSpan::TicksPerMillisecond=10000;
static const double TimeSpan::MillisecondsPerTick=1.0/TimeSpan::TicksPerMillisecond;
static const long TimeSpan::TicksPerSecond=TimeSpan::TicksPerMillisecond*1000;  // 10,000,000
static const double TimeSpan::SecondsPerTick=1.0/TimeSpan::TicksPerSecond;      // 0.0001
static const long TimeSpan::TicksPerMinute=TimeSpan::TicksPerSecond*60;         // 600,000,000
static const double TimeSpan::MinutesPerTick=1.0/TimeSpan::TicksPerMinute;      // 1.6666666666667e-9
static const long TimeSpan::TicksPerHour=TimeSpan::TicksPerMinute*60;           // 36,000,000,000
static const double TimeSpan::HoursPerTick=1.0/TimeSpan::TicksPerHour;          // 2.77777777777777778e-11
static const long TimeSpan::TicksPerDay=TimeSpan::TicksPerHour*24;              // 864,000,000,000
static const double TimeSpan::DaysPerTick=1.0/TimeSpan::TicksPerDay;            // 1.1574074074074074074e-12
static const int TimeSpan::MillisPerSecond = 1000;
static const int TimeSpan::MillisPerMinute = TimeSpan::MillisPerSecond * 60;    // 60,000
static const int TimeSpan::MillisPerHour = TimeSpan::MillisPerMinute * 60;      // 3,600,000
static const int TimeSpan::MillisPerDay = TimeSpan::MillisPerHour * 24;         // 86,400,000
static const long TimeSpan::MaxSeconds = Int64::MaxValue / TimeSpan::TicksPerSecond;
static const long TimeSpan::MinSeconds = Int64::MinValue / TimeSpan::TicksPerSecond;
static const long TimeSpan::MaxMilliSeconds = Int64::MaxValue / TimeSpan::TicksPerMillisecond;
static const long TimeSpan::MinMilliSeconds = Int64::MinValue / TimeSpan::TicksPerMillisecond;
static const long TimeSpan::TicksPerTenthSecond=TimeSpan::TicksPerMillisecond*100;
//+------------------------------------------------------------------+
