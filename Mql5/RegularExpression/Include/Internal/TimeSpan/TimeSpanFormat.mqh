//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
struct TimeSpan;
#include "TimeSpan.mqh"
//+------------------------------------------------------------------+
//| Purpose: Processing TimeSpan formats.                            |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Enum of Pattern.                                                 |
//+------------------------------------------------------------------+
enum Pattern
  {
   NONE_PATTERN    = 0,
   MINIMUN_PATTERN = 1,
   FULL_PATTERN    = 2,
  };
//+------------------------------------------------------------------+
//| Enum of Format.                                                  |
//+------------------------------------------------------------------+
enum Format
  {
   FullTimeSpanNegativePattern,
   FullTimeSpanPositivePattern
  };
//+------------------------------------------------------------------+
//| TimeSpanFormat.                                                  |
//+------------------------------------------------------------------+
class TimeSpanFormat
  {
private:
   //+------------------------------------------------------------------+
   //| Struct FormatLiterals.                                           |
   //+------------------------------------------------------------------+  
   struct FormatLiterals
     {
   public:
      string            AppCompatLiteral;
      int               dd;
      int               hh;
      int               mm;
      int               ss;
      int               ff;
      string            literals[];
      //+------------------------------------------------------------------+
      //| Constructor without parameters.                                  |
      //+------------------------------------------------------------------+
                        FormatLiterals()
        {
        }
      //+------------------------------------------------------------------+
      //| Constructor with parameters.                                     |
      //+------------------------------------------------------------------+
                        FormatLiterals(FormatLiterals &format)
        {
         this.AppCompatLiteral=format.AppCompatLiteral;
         this.dd=format.dd;
         this.hh=format.hh;
         this.mm=format.mm;
         this.ss=format.ss;
         this.ff=format.ff;
         ArrayCopy(this.literals,format.literals);
        }
      //+------------------------------------------------------------------+
      //| Operator (=).                                                    |
      //+------------------------------------------------------------------+       
      void operator=(FormatLiterals &format)
        {
         this.AppCompatLiteral=format.AppCompatLiteral;
         this.dd=format.dd;
         this.hh=format.hh;
         this.mm=format.mm;
         this.ss=format.ss;
         this.ff=format.ff;
         ArrayCopy(this.literals,format.literals);
        }
      //+------------------------------------------------------------------+
      //| Return zero literal.                                             |
      //+------------------------------------------------------------------+
      string Start()
        {
         return (literals[0]);
        }
      //+------------------------------------------------------------------+
      //| Return first literal.                                            |
      //+------------------------------------------------------------------+
      string DayHourSep()
        {
         return (literals[1]);
        }
      //+------------------------------------------------------------------+
      //| Return second literal.                                           |
      //+------------------------------------------------------------------+
      string HourMinuteSep()
        {
         return (literals[2]);
        }
      //+------------------------------------------------------------------+
      //| Return third literal.                                            |
      //+------------------------------------------------------------------+
      string MinuteSecondSep()
        {
         return (literals[3]);
        }
      //+------------------------------------------------------------------+
      //| Return fourth literal.                                           |
      //+------------------------------------------------------------------+
      string SecondFractionSep()
        {
         return (literals[4]);
        }
      //+------------------------------------------------------------------+
      //| Return fifth literal.                                            |
      //+------------------------------------------------------------------+
      string End()
        {
         return (literals[5]);
        }
      //+------------------------------------------------------------------+
      //| InitInvariant.                                                   |
      //+------------------------------------------------------------------+
      static FormatLiterals InitInvariant(const bool isNegative)
        {
         FormatLiterals x();
         ArrayResize(x.literals,6);
         x.literals[0] = isNegative ? "-" : "";
         x.literals[1] = ".";
         x.literals[2] = ":";
         x.literals[3] = ":";
         x.literals[4] = ".";
         x.literals[5] = "";
         x.AppCompatLiteral=":."; // MinuteSecondSep+SecondFractionSep;       
         x.dd = 2;
         x.hh = 2;
         x.mm = 2;
         x.ss = 2;
         const int MaxSecondsFractionDigits=7;
         x.ff=MaxSecondsFractionDigits;
         return (x);
        }
      //+------------------------------------------------------------------+
      //| Init.                                                            |
      //+------------------------------------------------------------------+  
      void Init(string format,bool useInvariantFieldLengths)
        {
         ArrayResize(literals,6);
         const int MaxSecondsFractionDigits=7;
         for(int i=0; i<ArraySize(literals); i++)
           {
            literals[i]="";
           }
         dd = 0;
         hh = 0;
         mm = 0;
         ss = 0;
         ff = 0;
         string sb;
         bool inQuote=false;
         char quote = '\'';
         int  field = 0;
         for(int i=0; i<StringLen(format); i++)
           {
            switch((char)StringGetCharacter(format,i))
              {
               case '\'':
               case '\"':
                  if(inQuote && (quote==(char)StringGetCharacter(format,i)))
                    {
                     if(field>=0 && field<=5)
                       {
                        literals[field]=sb;
                        StringInit(sb,0);
                        inQuote=false;
                       }
                     else
                       {
                        //---
                        return;
                       }
                    }
                  else if(!inQuote)
                    {
                     quote=(char)StringGetCharacter(format,i);
                     inQuote=true;
                    }
                  else
                    {
                     //--- we were in a quote and saw the other type of quote character, so we are still in a quote
                    }
                  break;
               case '%':
                 {
                  sb+=(string)StringGetCharacter(format,i);
                  break;
                 }
               case '\\':
                 {
                  if(!inQuote)
                    {
                     i++; //--- skip next character that is escaped by this backslash or percent sign
                     break;
                    }
                  sb+=CharToString((char)StringGetCharacter(format,i));
                  break;
                 }
               case 'd':
                  if(!inQuote)
                    {
                     field=1; // DayHourSep
                     dd++;
                    }
                  break;
               case 'h':
                  if(!inQuote)
                    {
                     field=2; // HourMinuteSep
                     hh++;
                    }
                  break;
               case 'm':
                  if(!inQuote)
                    {
                     field=3; // MinuteSecondSep
                     mm++;
                    }
                  break;
               case 's':
                  if(!inQuote)
                    {
                     field=4; // SecondFractionSep
                     ss++;
                    }
                  break;
               case 'f':
               case 'F':
                  if(!inQuote)
                    {
                     field=5; // End
                     ff++;
                    }
                  break;
               default:
                 {
                  sb+=CharToString((char)StringGetCharacter(format,i));
                  break;
                 }
              }
           }
         AppCompatLiteral=MinuteSecondSep()+SecondFractionSep();
         if(useInvariantFieldLengths)
           {
            dd = 2;
            hh = 2;
            mm = 2;
            ss = 2;
            ff = MaxSecondsFractionDigits;
           }
         else
           {
            if(dd<1 || dd>2) dd=2;   // The DTFI property has a problem. let's try to make the best of the situation.
            if(hh < 1 || hh > 2) hh = 2;
            if(mm < 1 || mm > 2) mm = 2;
            if(ss < 1 || ss > 2) ss = 2;
            if(ff < 1 || ff > 7) ff = 7;
           }
        }
     };
   //+------------------------------------------------------------------+
   //| End of struct FormatLiterals.                                    |
   //+------------------------------------------------------------------+
private:
   static FormatLiterals PositiveInvariantFormatLiterals;// isPositive
   static FormatLiterals NegativeInvariantFormatLiterals;// isNegative
   //--- Methods:
   //+------------------------------------------------------------------+
   //| IntToString.                                                     |
   //+------------------------------------------------------------------+
   static string IntToString(int n,int digits)
     {
      //--- return result
      return (IntegerToString(n, digits, '0'));
     }
public:
   //+------------------------------------------------------------------+
   //| Format.                                                          |
   //+------------------------------------------------------------------+
   static string Format(TimeSpan &value,string format)
     {
      if(format==NULL || StringLen(format))
        {
         format="c";
        }
      //--- standard formats
      if(StringLen(format)==1)
        {
         char f=(char)StringGetCharacter(format,0);
         if(f=='c' || f=='t' || f=='T')
           {
            //--- return result
            return (FormatStandard(value, true, format, MINIMUN_PATTERN));
           }
         if(f=='g' || f=='G')
           {
            Pattern pattern;
            if(value.Ticks()<0)
              {
               format="-";
              }
            else
              {
               format="";
              }
            if(f=='g')
              {
               pattern=MINIMUN_PATTERN;
              }
            else
              {
               pattern=FULL_PATTERN;
              }
            return (FormatStandard(value, false, format, pattern));
           }
         return (NULL);
        }
      return (FormatCustomized(value, format));
     }
private:
   //+------------------------------------------------------------------+
   //| Format the TimeSpan instance using the specified format.         |
   //+------------------------------------------------------------------+
   static string FormatStandard(TimeSpan &value,bool isInvariant,string format,Pattern pattern)
     {
      const int MaxSecondsFractionDigits=7;
      string sb;
      int day=(int)(value.Ticks()/TimeSpan::TicksPerDay);
      long time=value.Ticks()%TimeSpan::TicksPerDay;
      if(value.Ticks()<0)
        {
         day=-day;
         time=-time;
        }
      int hours    = (int)(time / TimeSpan::TicksPerHour % 24);
      int minutes  = (int)(time / TimeSpan::TicksPerMinute % 60);
      int seconds  = (int)(time / TimeSpan::TicksPerSecond % 60);
      int fraction = (int)(time % TimeSpan::TicksPerSecond);
      FormatLiterals literal;
      if(isInvariant)
        {
         if(value.Ticks()<0)
            literal=NegativeInvariantFormatLiterals;
         else
            literal=PositiveInvariantFormatLiterals;
        }
      else
        {
         literal.Init(format,pattern==FULL_PATTERN);
        }
      if(fraction!=0)
        {
         //--- truncate the partial second to the specified length
         fraction=(int)((long)fraction/(long)MathPow(10,MaxSecondsFractionDigits-literal.ff));
        }
      //--- Full: [-]dd.hh:mm:ss.fffffff
      //--- Minimum: [-][d.]hh:mm:ss[.fffffff] 
      sb+=literal.Start();                            // [-]
      if(pattern==FULL_PATTERN || day!=0)
        {          //
         sb+=IntegerToString(day);                    // [dd]
         sb+=literal.DayHourSep();                    // [.]
        }
      sb+=IntToString(hours,literal.hh);              // hh
      sb+=literal.HourMinuteSep();                    // :
      sb+=IntToString(minutes,literal.mm);            // mm
      sb+=literal.MinuteSecondSep();                  // :
      sb+=IntToString(seconds,literal.ss);            // ss
      if(!isInvariant && pattern==MINIMUN_PATTERN)
        {
         int effectiveDigits=literal.ff;
         while(effectiveDigits>0)
           {
            if(fraction%10==0)
              {
               fraction=fraction/10;
               effectiveDigits--;
              }
            else
              {
               break;
              }
           }
         if(effectiveDigits>0)
           {
            sb+=literal.SecondFractionSep();           // [.FFFFFFF]
            string fixedNumberFormats[]=
              {
               "0",
               "00",
               "000",
               "0000",
               "00000",
               "000000",
               "0000000",
              };
            string stringFraction=IntegerToString(fraction);
            while(StringLen(stringFraction)>=StringLen(fixedNumberFormats[effectiveDigits-1]))
              {
               stringFraction="0"+stringFraction;
              }
            sb+=stringFraction;
           }
        }
      else if(pattern==FULL_PATTERN || fraction!=0)
        {
         sb+=literal.SecondFractionSep();             // [.]
         sb+=IntToString(fraction,literal.ff);        // [fffffff]
        }
      sb+=literal.End();
      return (sb);
     }
private:
   //+------------------------------------------------------------------+
   //| ParseRepeatPattern.                                              |
   //+------------------------------------------------------------------+
   static int ParseRepeatPattern(string format,int pos,char patternChar)
     {
      int len=StringLen(format);
      int index=pos+1;
      while((index<len) && (((char)StringGetCharacter(format,index))==patternChar))
        {
         index++;
        }
      return (index - pos);
     }
   //+------------------------------------------------------------------+
   //| FormatDigits.                                                    |
   //+------------------------------------------------------------------+    
   static void FormatDigits(string &outputBuffer,int value,int len,bool overrideLengthLimit)
     {
      if(!overrideLengthLimit && len>2)
        {
         len=2;
        }
      char buffer[16];
      int p =16;
      int n = value;
      do
        {
         --p;
         buffer[p]=(char)(n%10+'0');
         n/=10;
        }
      while((n!=0) && (p>0));
      int digits=(int)(16-p);
      while((digits<len) && (p>0))
        {
         --p;
         buffer[p]='0';
         digits++;
        }
      outputBuffer+=CharArrayToString(buffer);
     }
   //+------------------------------------------------------------------+
   //| ParseQuoteString.                                                |
   //+------------------------------------------------------------------+
   static int ParseQuoteString(string format,int pos,string &result)
     {
      //--- NOTE : pos will be the index of the quote character in the 'format' string.
      int formatLen= StringLen(format);
      int beginPos = pos;
      char quoteChar=(char)StringGetCharacter(format,pos++); // Get the character used to quote the following string.
      bool foundQuote=false;
      while(pos<formatLen)
        {
         char ch=(char)StringGetCharacter(format,pos++);
         if(ch==quoteChar)
           {
            foundQuote=true;
            break;
           }
         else if(ch=='\\')
           {
            //--- The following are used to support escaped character. Escaped character is also supported in the quoted string.
            //--- Therefore, someone can use a format like "'minute:' mm\"" to display: minute: 45" because the second double quote is escaped.
            if(pos<formatLen)
              {
               result+=CharToString((char)StringGetCharacter(format,pos++));
              }
            else
              {
               //--- This means that '\' is at the end of the formatting string.
               Print("Format_InvalidString."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
               //--- return NULL
               return (NULL);
              }
           }
         else
           {
            result+=CharToString(ch);
           }
        }
      if(!foundQuote)
        {
         //--- Here we can't find the matching quote.
         Print("Format_BadQuote."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (0);
        }

      //--- return the character count including the begin/end quote characters and enclosed string.
      return (pos - beginPos);
     }
   //+------------------------------------------------------------------+
   //| ParseNextChar.                                                   |
   //+------------------------------------------------------------------+
   static int ParseNextChar(string format,int pos)
     {
      if(pos>=StringLen(format)-1)
        {
         return (-1);
        }
      return ((int)StringGetCharacter(format,pos+1));
     }
   //+------------------------------------------------------------------+
   //| Actions: Format the TimeSpan instance using the specified format.|
   //+------------------------------------------------------------------+ 
   static string FormatCustomized(TimeSpan &value,string format)
     {
      const int MaxSecondsFractionDigits=7;
      int day=(int)(value.Ticks()/TimeSpan::TicksPerDay);
      long time=value.Ticks()%TimeSpan::TicksPerDay;
      if(value.Ticks()<0)
        {
         day=-day;
         time=-time;
        }
      int hours    = (int)(time / TimeSpan::TicksPerHour % 24);
      int minutes  = (int)(time / TimeSpan::TicksPerMinute % 60);
      int seconds  = (int)(time / TimeSpan::TicksPerSecond % 60);
      int fraction = (int)(time % TimeSpan::TicksPerSecond);
      long tmp=0;
      int i=0;
      int tokenLen;
      string result;
      while(i<StringLen(format))
        {
         char ch=(char)StringGetCharacter(format,i);
         int nextChar;
         switch(ch)
           {
            case 'h':
              {
               tokenLen=ParseRepeatPattern(format,i,ch);
               if(tokenLen>2)
                 {
                  Print("Format_InvalidString."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                  return (NULL);
                 }
               FormatDigits(result,hours,tokenLen,false);
               break;
              }
            case 'm':
              {
               tokenLen=ParseRepeatPattern(format,i,ch);
               if(tokenLen>2)
                 {
                  Print("Format_InvalidString."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                  return (NULL);
                 }
               FormatDigits(result,minutes,tokenLen,false);
               break;
              }
            case 's':
               tokenLen=ParseRepeatPattern(format,i,ch);
               if(tokenLen>2)
                 {
                  Print("Format_InvalidString."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                  return (NULL);
                 }
               FormatDigits(result,seconds,tokenLen,false);
               break;
            case 'f':
              {
               //--- The fraction of a second in single-digit precision. The remaining digits are truncated. 
               tokenLen=ParseRepeatPattern(format,i,ch);
               if(tokenLen>MaxSecondsFractionDigits)
                 {
                  Print("Format_InvalidString."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                  return (NULL);
                 }
               tmp =(long)fraction;
               tmp/=(long)MathPow(10,MaxSecondsFractionDigits-tokenLen);
               string fixedNumberFormats[]=
                 {
                  "0",
                  "00",
                  "000",
                  "0000",
                  "00000",
                  "000000",
                  "0000000",
                 };
               string stringTmp=IntegerToString(tmp);
               while(StringLen(stringTmp)>=StringLen(fixedNumberFormats[tokenLen-1]))
                 {
                  stringTmp="0"+stringTmp;
                 }
               result+=stringTmp;
               break;
              }
            case 'F':
              {
               //--- Displays the most significant digit of the seconds fraction. Nothing is displayed if the digit is zero.
               tokenLen=ParseRepeatPattern(format,i,ch);
               if(tokenLen>MaxSecondsFractionDigits)
                 {
                  Print("Format_InvalidString."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                  return (NULL);
                 }
               tmp =(long)fraction;
               tmp/=(long)MathPow(10,MaxSecondsFractionDigits-tokenLen);
               int effectiveDigits=tokenLen;
               while(effectiveDigits>0)
                 {
                  if(tmp%10==0)
                    {
                     tmp=tmp/10;
                     effectiveDigits--;
                    }
                  else
                    {
                     break;
                    }
                 }
               if(effectiveDigits>0)
                 {
                  string fixedNumberFormats[]=
                    {
                     "0",
                     "00",
                     "000",
                     "0000",
                     "00000",
                     "000000",
                     "0000000",
                    };
                  string stringTmp=IntegerToString(tmp);
                  while(StringLen(stringTmp)>=StringLen(fixedNumberFormats[effectiveDigits-1]))
                    {
                     stringTmp="0"+stringTmp;
                    }
                  result+=stringTmp;
                 }
               break;
              }
            case 'd':
              {
               //--- tokenLen == 1 : Day as digits with no leading zero.
               //--- tokenLen == 2+: Day as digits with leading zero for single-digit days.
               tokenLen=ParseRepeatPattern(format,i,ch);
               if(tokenLen>8)
                 {
                  Print("Format_InvalidString."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                  return (NULL);
                 }
               FormatDigits(result,day,tokenLen,true);
               break;
              }
            case '\'':
            case '\"':
              {
               string enquotedString;
               tokenLen=ParseQuoteString(format,i,enquotedString);
               result+=enquotedString;
               break;
              }
            case '%':
              {
               //--- Optional format character.
               //--- For example, format string "%d" will print day Most of the cases, "%" can be ignored.
               nextChar=ParseNextChar(format,i);
               //--- nextChar will be -1 if we already reach the end of the format string.
               //--- Besides, we will not allow "%%" appear in the pattern.
               if(nextChar>=0 && nextChar!=(int)'%')
                 {
                  result+=TimeSpanFormat::FormatCustomized(value,CharToString((uchar)nextChar));
                  tokenLen=2;
                 }
               else
                 {
                  //--- This means that '%' is at the end of the format string or "%%" appears in the format string.
                  Print("Format_InvalidString."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                  return (NULL);
                 }
               break;
              }
            case '\\':
               //--- Escaped character.  Can be used to insert character into the format string.
               //--- For example, "\d" will insert the character 'd' into the string.
               nextChar=ParseNextChar(format,i);
               if(nextChar>=0)
                 {
                  result+=(string)(char)nextChar;
                  tokenLen=2;
                 }
               else
                 {
                  //--- This means that '\' is at the end of the formatting string.
                  Print("Format_InvalidString."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                  return (NULL);
                 }
               break;
            default:
              {
               Print("Format_InvalidString."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
               return (NULL);
              }
           }
         i+=tokenLen;
        }
      //--- return result
      return (result);
     }
  };
static TimeSpanFormat::FormatLiterals TimeSpanFormat::PositiveInvariantFormatLiterals = FormatLiterals::InitInvariant(false);
static TimeSpanFormat::FormatLiterals TimeSpanFormat::NegativeInvariantFormatLiterals = FormatLiterals::InitInvariant(true);
//+------------------------------------------------------------------+
