//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
#include <Internal\IComparable.mqh>
//+------------------------------------------------------------------+
//| Purpose: Generic key-value pair for dictionary enumerators.      |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| A KeyValuePair holds a key and a value from a dictionary. It is  |
//| used by the IEnumerable<T> implementation for both               |
//| IDictionary<TKey, TValue>.                                       |
//+------------------------------------------------------------------+  
template<typename TKey,typename TValue>
class KeyValuePair : public IComparable
  {
private:
   TKey              m_key;
   TValue            m_value;
public:
   //+------------------------------------------------------------------+
   //| Initializes a new instance of the KeyValuePair<TKey,TValue>      |
   //| structure with the specified key and value.                      |
   //+------------------------------------------------------------------+
                     KeyValuePair(TKey key,TValue value)
     {
      this.m_key=key;
      this.m_value=value;
     }
   //+------------------------------------------------------------------+
   //| Gets the key in the key/value pair.                              |
   //+------------------------------------------------------------------+
   TKey Key()
     {
      return (m_key);
     }
   //+------------------------------------------------------------------+
   //| Gets the value in the key/value pair.                            |
   //+------------------------------------------------------------------+     
   TValue Value()
     {
      return (m_value);
     }
   //+------------------------------------------------------------------+
   //| Sets the key in the key/value pair.                              |
   //+------------------------------------------------------------------+     
   void Key(TKey key)
     {
      m_key=key;
     }
   //+------------------------------------------------------------------+
   //| Sets the value in the key/value pair.                            |
   //+------------------------------------------------------------------+     
   void Value(TValue value)
     {
      m_value=m_value;
     }
   //+------------------------------------------------------------------+
   //| Indicates whether this instance and a specified object are equal.|
   //+------------------------------------------------------------------+     
   void operator=(const KeyValuePair<TKey,TValue>&pair)
     {
      this.m_key=pair.m_key;
      this.m_value=pair.m_value;
     }
  };
//+------------------------------------------------------------------+
