//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
//+------------------------------------------------------------------+
//| Purpose: Base interface for all generic enumerators.             |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Base interface for all generic enumerators, providing a simple   |
//| approach to iterating over a collection.                         |
//+------------------------------------------------------------------+
template<typename T>
interface IEnumerator
  {
//--- Methods:
//+------------------------------------------------------------------+
//| Interfaces are not serializable Advances the enumerator to the   |
//| next element of the enumeration and returns a boolean indicating |
//| whether an element is available. Upon creation, an enumerator is |
//| conceptually positioned before the first element of the          |
//| enumeration, and the first call to MoveNext brings the first     |
//| element of the enumeration into view.                            |
//+------------------------------------------------------------------+
   bool MoveNext();
//+------------------------------------------------------------------+
//| Returns the current element of the enumeration. The returned     |
//| value is undefined before the first call to MoveNext and         |
//| following a call to MoveNext that returned false. Multiple calls |
//| to GetCurrent with no intervening calls to MoveNext will return  |
//| the same object.                                                 |
//+------------------------------------------------------------------+
   T Current();
//+------------------------------------------------------------------+
//| Resets the enumerator to the beginning of the enumeration,       |
//| starting over. The preferred behavior for Reset is to return the |
//| exact same enumeration. This means if you modify the underlying  |
//| collection then call Reset, your IEnumerator will be invalid,    |
//| just as it would have been if you had called MoveNext or Current.|
//+------------------------------------------------------------------+
   void Reset();
  };
//+------------------------------------------------------------------+
//| Implementation of standard generic enumerators.                  |
//+------------------------------------------------------------------+
template<typename T>
class  StandardEnumerator : public IEnumerator<T>
  {
public:
   T                 m_current;
   bool              m_free;
   StandardEnumerator<T>*m_next;
   StandardEnumerator<T>*m_first;
public:
   //+------------------------------------------------------------------+
   //| Constructor without parameters.                                  |
   //+------------------------------------------------------------------+
                     StandardEnumerator(): m_free(true)
     {
     }
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~StandardEnumerator()
     {
      if(CheckPointer(m_next)==POINTER_DYNAMIC)
        {
         delete m_next;
        }
      if(CheckPointer(m_first)==POINTER_DYNAMIC)
        {
         delete m_first;
        }
     }
   //+------------------------------------------------------------------+
   //| Interfaces are not serializable Advances the enumerator to the   |
   //| next element of the enumeration and returns a boolean indicating |
   //| whether an element is available. Upon creation, an enumerator is |
   //| conceptually positioned before the first element of the          |
   //| enumeration, and the first call to MoveNext brings the first     |
   //| element of the enumeration into view.                            |
   //+------------------------------------------------------------------+
   virtual bool MoveNext()
     {
      if((m_next==NULL) || (m_next.m_free==true))
        {
         return (false);
        }
      else
        {
         m_current=m_next.m_current;
         m_next=m_next.m_next;
         return (true);
        }
     }
   //+------------------------------------------------------------------+
   //| Returns the current element of the enumeration. The returned     |
   //| value is undefined before the first call to MoveNext and         |
   //| following a call to MoveNext that returned false. Multiple calls |
   //| to GetCurrent with no intervening calls to MoveNext will return  |
   //| the same object.                                                 |
   //+------------------------------------------------------------------+
   virtual T Current()
     {
      return (m_current);
     }
   //+------------------------------------------------------------------+
   //| Resets the enumerator to the beginning of the enumeration,       |
   //| starting over. The preferred behavior for Reset is to return the |
   //| exact same enumeration. This means if you modify the underlying  |
   //| collection then call Reset, your IEnumerator will be invalid,    |
   //| just as it would have been if you had called MoveNext or Current.|
   //+------------------------------------------------------------------+
   virtual void Reset()
     {
      this.m_current=m_first.m_current;
      this.m_next=m_first.m_next;
     }
   //+------------------------------------------------------------------+
   //| Add new elemeth in Enumerator.                                   |
   //+------------------------------------------------------------------+
   void Add(T value)
     {
      if(m_next==NULL)
        {
         m_current=(T)NULL;
         m_next=new StandardEnumerator();
         m_first=new StandardEnumerator();
         m_first.m_current=this.m_current;
         m_first.m_next=this.m_next;
         m_free=false;
         this.Add(value);
        }
      else
        {
         StandardEnumerator *last=this.m_next;
         while(last.m_next!=NULL)
           {
            last=last.m_next;
           }
         last.m_current=value;
         last.m_next=new StandardEnumerator();
         last.m_free= false;
        }
     }
   //+------------------------------------------------------------------+
   //| Creates a clone of current object.                               |
   //+------------------------------------------------------------------+    
   IEnumerator<T>*Clone()
     {
      StandardEnumerator<T>*clone=new StandardEnumerator<T>;
      this.Reset();
      while(this.MoveNext())
        {
         clone.Add(this.Current());
        }
      this.Reset();
      return (clone);
     }
  };
//+------------------------------------------------------------------+
