//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
#include "IEnumerator.mqh"
#include "EqualityComparer.mqh"
#include "IDictionary.mqh"
#include <Internal\Wrappers.mqh>
//+------------------------------------------------------------------+
//| Purpose: Generic hash table implementation.                      |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| #DictionaryVersusHashtableThreadSafety                           |
//| Hashtable has multiple reader/single writer (MR/SW) thread safety|
//| built into certain methods and properties, whereas Dictionary    |
//| doesn't. If you're converting framework code that formerly used  |
//| Hashtable to Dictionary, it's important to consider whether      |
//| callers may have taken a dependence on MR/SW thread safety. If a |
//| reader writer lock is available, then that may be used with a    |
//| Dictionary to get the same thread safety guarantee.              | 
//|                                                                  |
//| Reader writer locks don't exist in silverlight, so we do the     |
//| following as a result of removing non-generic collections from   |
//| silverlight:                                                     |
//| 1. If the Hashtable was fully synchronized, then we replace it   |
//|    with a Dictionary with full locks around reads/writes (same   |
//|    thread safety guarantee).                                     |
//| 2. Otherwise, the Hashtable has the default MR/SW thread safety  |
//|    behavior, so we do one of the following on a case-by-case     |
//|    basis:                                                        |
//|    a. If the ---- can be addressed by rearranging the code and   |
//|       using a temp variable (for example, it's only populated    |
//|       immediately after created) then we address the ---- this   |
//|       way and use Dictionary.                                    |
//|    b. If there's concern about degrading performance with the    |
//|       increased locking, we ifdef with                           | 
//|       FEATURE_NONGENERIC_COLLECTIONS so we can at least use      |
//|       Hashtable in the desktop build, but Dictionary with full   | 
//|       locks in silverlight builds. Note that this is heavier     |
//|       locking than MR/SW, but this is the only option without    |
//|       rewriting (or adding back) the reader writer lock.         |
//|    c. If there's no performance concern (e.g. debug-only code) we| 
//|       consistently replace Hashtable with Dictionary plus full   |
//|       locks to reduce complexity.                                |
//|    d. Most of serialization is dead code in silverlight. Instead |
//|       of updating those Hashtable occurences in serialization, we| 
//|       carved out references to serialization such that this code |
//|       doesn't need to build in silverlight.                      |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Class Enumerator for Dictionary<TKey,TValue>.                    |
//+------------------------------------------------------------------+ 
template<typename TKey,typename TValue>
class DictionaryEnumerator: public IEnumerator<KeyValuePair<TKey,TValue>*>
  {
private:
   Dictionary<TKey,TValue>*m_dictionary;
   KeyValuePair<TKey,TValue>*m_current;
   int               m_index;
   int               m_version;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     DictionaryEnumerator(Dictionary<TKey,TValue>*dictionary)
     {
      this.m_dictionary=dictionary;
      m_index=0;
      m_current=NULL;
      m_version=dictionary.Version();
     }
   //+------------------------------------------------------------------+
   //| Advances the enumerator to the next element of the collection.   |
   //+------------------------------------------------------------------+
   bool MoveNext()
     {
      if(m_version!=m_dictionary.Version())
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__",__LINE__);
         return (false);
        }
      //--- Use unsigned comparison since we set index to dictionary.count+1 when the enumeration ends.
      while((uint)m_index<(uint)(m_dictionary.Count()+m_dictionary.FreeCount()))
        {
         Entry<TKey,TValue>entry=m_dictionary.ElementAt(m_index);
         if(entry.hashCode>=0)
           {
            if(CheckPointer(m_current)==POINTER_DYNAMIC)
              {
               delete m_current;
              }
            m_current=new KeyValuePair<TKey,TValue>(entry.key,entry.value);
            m_index++;
            return (true);
           }
         m_index++;
        }
      if(CheckPointer(m_current)==POINTER_DYNAMIC)
        {
         delete m_current;
        }
      m_index=m_dictionary.Count()+1;
      m_current=NULL;
      return (false);
     }
   //+------------------------------------------------------------------+
   //| Gets key-value pair.                                             |
   //+------------------------------------------------------------------+     
   KeyValuePair<TKey,TValue>*Current()
     {
      return (m_current);
     }
   //+------------------------------------------------------------------+
   //| Reset.                                                           |
   //+------------------------------------------------------------------+     
   void Reset()
     {
      if(m_version!=m_dictionary.Version())
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      m_index=0;
     }
   //+------------------------------------------------------------------+
   //| Gets key.                                                        |
   //+------------------------------------------------------------------+
   TKey Key()
     {
      if(m_index==0 || (m_index==m_dictionary.Count()+1))
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return NULL
         return (NULL);
        }
      return (m_current.Key());
     }
   //+------------------------------------------------------------------+
   //| Gets value.                                                      |
   //+------------------------------------------------------------------+
   TValue Value()
     {
      if(m_index==0 || (m_index==m_dictionary.Count()+1))
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return (m_current.Value());
     }
  };
//+------------------------------------------------------------------+
//| Class Enumerator for KeyCollection<TKey,TValue>.                 |
//+------------------------------------------------------------------+ 
template<typename TKey,typename TValue>
class KeyCollectionEnumerator : public IEnumerator<TKey>
  {
private:
   Dictionary<TKey,TValue>*m_dictionary;
   int               m_index;
   int               m_version;
   TKey              m_currentKey;
public:
   //--- Constructors:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     KeyCollectionEnumerator(Dictionary<TKey,TValue>*dictionary)
     {
      this.m_dictionary=dictionary;
      m_version=dictionary.Version();
      m_index=0;
      m_currentKey=NULL;
     }
   //--- Methods:
   //+------------------------------------------------------------------+
   //| Advances the enumerator to the next element of the collection.   |
   //+------------------------------------------------------------------+
   bool MoveNext()
     {
      if(m_version!=m_dictionary.Version())
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (false);
        }
      while((uint)m_index<(uint)m_dictionary.Count()+m_dictionary.FreeCount())
        {
         if(m_dictionary.ElementAt(m_index).hashCode>=0)
           {
            m_currentKey=m_dictionary.ElementAt(m_index).key;
            m_index++;
            return (true);
           }
         m_index++;
        }
      m_index=m_dictionary.Count()+m_dictionary.FreeCount()+1;
      m_currentKey=NULL;
      return (false);
     }
   //+------------------------------------------------------------------+
   //| Gets the current key.                                            |
   //+------------------------------------------------------------------+
   TKey Current()
     {
      return (m_currentKey);
     }
   //+------------------------------------------------------------------+
   //| Reset.                                                           |
   //+------------------------------------------------------------------+
   void Reset()
     {
      if(m_version!=m_dictionary.Version())
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      m_index=0;
      m_currentKey=NULL;
     }
  };
//+------------------------------------------------------------------+
//| Class Enumerator for KeyCollection<TKey,TValue>.                 |
//+------------------------------------------------------------------+ 
template<typename TKey,typename TValue>
class ValueCollectionEnumerator : public IEnumerator<TValue>
  {
private:
   Dictionary<TKey,TValue>*m_dictionary;
   int               m_index;
   int               m_version;
   TValue            m_currentValue;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     ValueCollectionEnumerator(Dictionary<TKey,TValue>*dictionary)
     {
      this.m_dictionary=dictionary;
      m_version=dictionary.Version();
      m_index=0;
      m_currentValue=NULL;
     }
   //+------------------------------------------------------------------+
   //| Advances the enumerator to the next element of the collection.   |
   //+------------------------------------------------------------------+
   bool MoveNext()
     {
      if(m_version!=m_dictionary.Version())
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (false);
        }
      while((uint)m_index<(uint)m_dictionary.Count()+m_dictionary.FreeCount())
        {
         if(m_dictionary.ElementAt(m_index).hashCode>=0)
           {
            m_currentValue=m_dictionary.ElementAt(m_index).value;
            m_index++;
            //--- return true;
            return (true);
           }
         m_index++;
        }
      m_index=m_dictionary.Count()+m_dictionary.FreeCount()+1;
      m_currentValue=NULL;
      return (false);
     }
   //+------------------------------------------------------------------+
   //| Gets the current value.                                          |
   //+------------------------------------------------------------------+
   TValue Current()
     {
      return (m_currentValue);
     }
   //+------------------------------------------------------------------+
   //| Reset.                                                           |
   //+------------------------------------------------------------------+
   void Reset()
     {
      if(m_version!=m_dictionary.Version())
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      m_index=0;
      m_currentValue=NULL;
     }
  };
//+------------------------------------------------------------------+
//| Entry - structure containing the elements of the dictionary.     |
//+------------------------------------------------------------------+
template<typename TKey,typename TValue>
struct Entry
  {
public:
   int               hashCode;      // Lower 31 bits of hash code, -1 if unused
   int               next;          // Index of next entry, -1 if last
   TKey              key;           // Key of entry
   TValue            value;         // Value of entry
   //+------------------------------------------------------------------+
   //| Constructor without parameters.                                  |
   //+------------------------------------------------------------------+
                     Entry()
     {
     }
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+   
                     Entry(const Entry<TKey,TValue>&entry)
     {
      hashCode=entry.hashCode;
      next= entry.next;
      key = entry.key;
      value=entry.value;
     }
   //+------------------------------------------------------------------+
   //| Operator (=).                                                    |
   //+------------------------------------------------------------------+
   void operator=(const Entry<TKey,TValue>&entry)
     {
      hashCode=entry.hashCode;
      next= entry.next;
      key = entry.key;
      value=entry.value;
     }
  };
//+------------------------------------------------------------------+
//| Class KeyCollection.                                             |
//+------------------------------------------------------------------+
template<typename TKey,typename TValue>
class KeyCollection: public ICollection<TKey>
  {
private:
   Dictionary<TKey,TValue>*m_dictionary;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     KeyCollection(Dictionary<TKey,TValue>*dictionary)
     {
      if(dictionary==NULL)
        {
         Print("Argument 'dictionary' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
        }
      this.m_dictionary=dictionary;
     }
   //+------------------------------------------------------------------+
   //| Number of items in the collections.                              |
   //+------------------------------------------------------------------+     
   int Count()
     {
      return m_dictionary.Count();
     }
   //+------------------------------------------------------------------+
   //| Determines whether an element is in the collection.              |
   //+------------------------------------------------------------------+  
   bool Contains(TKey item)
     {
      return m_dictionary.ContainsKey(item);
     }
   //+------------------------------------------------------------------+
   //| CopyTo copies a collection into an Array, starting at a          |
   //| particular index into the array.                                 |
   //+------------------------------------------------------------------+    
   void CopyTo(TKey &array[],const int arrayIndex)
     {
      int index=arrayIndex;
      ArrayResize(array,arrayIndex+m_dictionary.Count());
      for(int i=0; i<m_dictionary.Count(); i++)
        {
         if(m_dictionary.ElementAt(i).hashCode>=0)
           {
            array[index++]=m_dictionary.ElementAt(i).key;
           }
        }
     }
   //+------------------------------------------------------------------+
   //| Returns an enumerator for this object.                           |
   //+------------------------------------------------------------------+  
   KeyCollectionEnumerator<TKey,TValue>*GetEnumerator()
     {
      return new KeyCollectionEnumerator<TKey,TValue>(m_dictionary);
     }
  };
//+------------------------------------------------------------------+
//| Class ValueCollection.                                           |
//+------------------------------------------------------------------+
template<typename TKey,typename TValue>
class ValueCollection: public ICollection<TValue>
  {
private:
   Dictionary<TKey,TValue>*m_dictionary;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     ValueCollection(Dictionary<TKey,TValue>*dictionary)
     {
      if(dictionary==NULL)
        {
         Print("Argument 'dictionary' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
        }
      this.m_dictionary=dictionary;
     }
   //+------------------------------------------------------------------+
   //| Count.                                                           |
   //+------------------------------------------------------------------+         
   int Count()
     {
      return (m_dictionary.Count());
     }
   //+------------------------------------------------------------------+
   //| Contains.                                                        |
   //+------------------------------------------------------------------+     
   bool Contains(TValue item)
     {
      return (m_dictionary.ContainsValue(item));
     }
   //+------------------------------------------------------------------+
   //| CopyTo copies a collection into an Array, starting at a          |
   //| particular index into the array.                                 |
   //+------------------------------------------------------------------+    
   void CopyTo(TKey &array[],const int arrayIndex)
     {
      int index=arrayIndex;
      ArrayResize(array,arrayIndex+m_dictionary.Count());
      for(int i=0; i<m_dictionary.Count(); i++)
        {
         if(m_dictionary.ElementAt(i).hashCode>=0)
           {
            array[index++]=(TKey)m_dictionary.ElementAt(i).key;
           }
        }
     }
   //+------------------------------------------------------------------+
   //| Returns an enumerator for this object.                           |
   //+------------------------------------------------------------------+  
   ValueCollectionEnumerator<TKey,TValue>*GetEnumerator()
     {
      return new ValueCollectionEnumerator<TKey,TValue>(m_dictionary);
     }
  };
//+------------------------------------------------------------------+
//| Represents a collection of keys and values.                      |
//+------------------------------------------------------------------+
template<typename TKey,typename TValue>
class Dictionary : public IDictionary<TKey,TValue>
  {
private:
   IEqualityComparer<TKey>*m_comparer;
   int               m_buckets[];
   Entry<TKey,TValue>m_entries[];
   int               m_count;
   int               m_freeList;
   int               m_freeCount;
   KeyCollection<TKey,TValue>*m_keys;
   ValueCollection<TKey,TValue>*m_values;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     Dictionary(IEqualityComparer<TKey>*comparer): m_freeList(0),m_freeCount(0),m_count(0)
     {
      m_version=0;
      int capacity=0;
      if(capacity>0)
        {
         Initialize(capacity);
        }
      if(comparer!=NULL)
        {
         this.m_comparer=comparer;
        }
      else
        {
         this.m_comparer=EqualityComparer<TKey>::Default();
        }
     }
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     Dictionary(const int capacity=0,IEqualityComparer<TKey>*comparer=NULL): m_freeList(0),m_freeCount(0),m_count(0)
     {
      m_version=0;
      if(capacity<0)
        {
         Print("Argument 'capacity' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
        }
      else
        {
         if(capacity>0)
           {
            Initialize(capacity);
           }
         if(comparer!=NULL)
           {
            this.m_comparer=comparer;
           }
         else
           {
            this.m_comparer=EqualityComparer<TKey>::Default();
           }
        }
     }
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     Dictionary(Dictionary<TKey,TValue>*dictionary,IEqualityComparer<TKey>*comparer=NULL): m_freeList(0),m_freeCount(0),m_count(0)
     {
      m_version=0;
      int capacity=(dictionary!=NULL)? dictionary.Count() : 0;
      if(dictionary==NULL)
        {
         Print("Argument 'dictionary' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
        }
      else if(capacity<0)
        {
         Print("Argument 'capacity' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
        }
      else
        {
         if(capacity>0)
           {
            Initialize(capacity);
           }
         DictionaryEnumerator<TKey,TValue>*enumerator=dictionary.GetEnumerator();
         if(comparer!=NULL)
           {
            this.m_comparer=comparer;
           }
         else
           {
            this.m_comparer=EqualityComparer<TKey>::Default();
           }
         while(enumerator.MoveNext())
           {
            Add(enumerator.Current().Key(),enumerator.Current().Value());
           }
         delete enumerator;
        }
     }
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~Dictionary()
     {
      if(CheckPointer(m_comparer)==POINTER_DYNAMIC)
        {
         delete m_comparer;
        }
      if(CheckPointer(m_keys)==POINTER_DYNAMIC)
        {
         delete m_keys;
        }
      if(CheckPointer(m_values)==POINTER_DYNAMIC)
        {
         delete m_values;
        }
     }
   //+------------------------------------------------------------------+
   //| Gets version of dictionary.                                      |
   //+------------------------------------------------------------------+
   int Version()
     {
      return (m_version);
     }
   //+------------------------------------------------------------------+
   //| Gets the freeCount.                                              |
   //+------------------------------------------------------------------+
   int FreeCount()
     {
      return (m_freeCount);
     }
   //+------------------------------------------------------------------+
   //| Gets the number of key/value pairs contained in the              |
   //| Dictionary<TKey, TValue>.                                        |
   //+------------------------------------------------------------------+ 
   int Count()
     {
      return (m_count-m_freeCount);
     }
   //+------------------------------------------------------------------+
   //| Gets a collection containing the keys in the                     |
   //| Dictionary<TKey, TValue>.                                        |
   //+------------------------------------------------------------------+
   KeyCollection<TKey,TValue>*Keys()
     {
      if(m_keys==NULL)
        {
         m_keys=new KeyCollection<TKey,TValue>(GetPointer(this));
        }
      return (m_keys);
     }
   //+------------------------------------------------------------------+
   //| Gets a collection containing the values in the                   |
   //| Dictionary<TKey, TValue>.                                        |
   //+------------------------------------------------------------------+
   ValueCollection<TKey,TValue>*Values()
     {
      if(m_values==NULL)
        {
         m_values=new ValueCollection<TKey,TValue>(GetPointer(this));
        }
      return (m_values);
     }
   //+------------------------------------------------------------------+
   //| Gets the value associated with the specified key.                |
   //+------------------------------------------------------------------+
   TValue operator[](TKey key)
     {
      int i = FindEntry(key);
      if(i >= 0)
        {
         return (m_entries[i].value);
        }
      Print("Key not found."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
      TValue zero;
      ZeroMemory(zero);
      return (zero);
     }
   //+------------------------------------------------------------------+
   //| Sets the value associated with the specified key.                |
   //+------------------------------------------------------------------+  
   void Set(TKey key,TValue value)
     {
      Insert(key,value,false);
     }
   //+------------------------------------------------------------------+
   //| Gets the element at a specified index in a sequence.             |
   //+------------------------------------------------------------------+
   Entry<TKey,TValue>ElementAt(const int index)
     {
      Entry<TKey,TValue>entry=m_entries[index];
      if(index<0 || index>=ArraySize(m_entries))
        {
         ZeroMemory(entry);
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (entry);
        }
      return (entry);
     }
   //+------------------------------------------------------------------+
   //| Adds the specified key and value to the dictionary.              |
   //+------------------------------------------------------------------+
   void Add(TKey key,TValue value)
     {
      Insert(key,value,true);
     }
   //+------------------------------------------------------------------+
   //| Determines whether the Dictionary<TKey, TValue> contains the     |
   //| specified key.                                                   |
   //+------------------------------------------------------------------+
   void Clear()
     {
      if(m_count>0)
        {
         for(int i=0; i<ArraySize(m_buckets); i++)
           {
            m_buckets[i]=-1;
           }
         ArrayFree(m_entries);
         m_freeList=-1;
         m_count=0;
         m_freeCount=0;
         m_version++;
        }
     }
   //+------------------------------------------------------------------+
   //| Determines whether the Dictionary<TKey, TValue> contains the     |
   //| specified key.                                                   |
   //+------------------------------------------------------------------+
   bool ContainsKey(TKey key)
     {
      return (FindEntry(key) >= 0);
     }
   //+------------------------------------------------------------------+
   //| Determines whether the Dictionary<TKey, TValue> contains a       |
   //| specific value.                                                  |
   //+------------------------------------------------------------------+
   bool ContainsValue(TValue value)
     {
      if(value==NULL)
        {
         for(int i=0; i<m_count; i++)
           {
            if(m_entries[i].hashCode>=0 && m_entries[i].value==NULL)
              {
               return (true);
              }
           }
        }
      else
        {
         EqualityComparer<TValue>*c=EqualityComparer<TValue>::Default();
         for(int i=0; i<m_count; i++)
           {
            if(m_entries[i].hashCode>=0 && c.Equals(m_entries[i].value,value))
              {
               return (true);
              }
           }
         delete c;
        }
      return (false);
     }
   //+------------------------------------------------------------------+
   //| Returns an enumerator for this list with the given permission for|
   //| removal of elements. If modifications made to the list while an  |
   //| enumeration is in progress, the MoveNext and GetObject methods of|
   //| the enumerator will throw an exception.                          |
   //+------------------------------------------------------------------+  
   DictionaryEnumerator<TKey,TValue>*GetEnumerator()
     {
      return new DictionaryEnumerator<TKey,TValue>(GetPointer(this));
     }
private:
   //+------------------------------------------------------------------+
   //| Method FindEntry.                                                |
   //+------------------------------------------------------------------+
   int FindEntry(TKey key)
     {
      if(key==NULL && StringFind(typename(TKey),"*")!=-1)
        {
         Print("Argument 'key' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      if(ArraySize(m_buckets)!=NULL)
        {
         int hashCode=m_comparer.GetHashCode(key) &0x7FFFFFFF;
         for(int i=m_buckets[hashCode%ArraySize(m_buckets)]; i>=0; i=m_entries[i].next)
           {
            if(m_entries[i].hashCode==hashCode && m_comparer.Equals(m_entries[i].key,key))
              {
               return (i);
              }
           }
        }
      return (-1);
     }
   //+------------------------------------------------------------------+
   //| Method IsPrime from class HashHelpers.                           |
   //+------------------------------------------------------------------+
   bool IsPrime(const int candidate)
     {
      if((candidate  &1)!=0)
        {
         int limit=(int)MathSqrt(candidate);
         for(int divisor=3; divisor<=limit; divisor+=2)
           {
            if((candidate%divisor)==0)
              {
               return (false);
              }
           }
         return (true);
        }
      return (candidate == 2);
     }
   //+------------------------------------------------------------------+
   //| Method GetPrime from class HashHelpers.                          |
   //+------------------------------------------------------------------+
   int GetPrime(int min)
     {
      const int primes[]=
        {
         3,7,11,17,23,29,37,47,59,71,89,107,131,163,197,239,293,353,431,521,631,761,919,
         1103,1327,1597,1931,2333,2801,3371,4049,4861,5839,7013,8419,10103,12143,14591,
         17519,21023,25229,30293,36353,43627,52361,62851,75431,90523,108631,130363,156437,
         187751,225307,270371,324449,389357,467237,560689,672827,807403,968897,1162687,1395263,
         1674319,2009191,2411033,2893249,3471899,4166287,4999559,5999471,7199369
        };
      int HashPrime=101;
      if(min<0)
        {
         return (0);
        }
      for(int i=0; i<ArraySize(primes); i++)
        {
         int prime = primes[i];
         if(prime >= min)
           {
            //--- return result
            return (prime);
           }
        }
      //--- outside of our predefined table. 
      //--- compute the hard way. 
      for(int i=(min|1); i<Int32::MaxValue;i+=2)
        {
         if(IsPrime(i) && ((i-1)%HashPrime!=0))
           {
            return (i);
           }
        }
      return (min);
     }
   //+------------------------------------------------------------------+
   //| Method Initialize.                                               |
   //+------------------------------------------------------------------+
   void Initialize(int capacity)
     {
      int size=GetPrime(capacity);
      ArrayResize(m_buckets,size);
      ArrayFill(m_buckets,0,ArraySize(m_buckets),-1);
      ArrayResize(m_entries,size);
      m_freeList=-1;
     }
   //+------------------------------------------------------------------+
   //| Method Insert.                                                   |
   //+------------------------------------------------------------------+
   void Insert(TKey key,TValue value,bool add)
     {
      if(key==NULL && StringFind(typename(TKey),"*")!=-1)
        {
         Print("Argument 'key' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      if(ArraySize(m_buckets)==NULL)
        {
         Initialize(0);
        }
      int hashCode=m_comparer.GetHashCode(key) &0x7FFFFFFF;
      int targetBucket=hashCode%ArraySize(m_buckets);
      for(int i=m_buckets[targetBucket]; i>=0; i=m_entries[i].next)
        {
         if(m_entries[i].hashCode==hashCode && m_comparer.Equals(m_entries[i].key,key))
           {
            if(add)
              {              
               Print("Argument adding duplicate.",__FILE__," __LINE__",__LINE__);
               return;
              }
            m_entries[i].value=value;
            m_version++;
            //--- return
            return;
           }
        }
      int index;
      if(m_freeCount>0)
        {
         index=m_freeList;
         m_freeList=m_entries[index].next;
         m_freeCount--;
        }
      else
        {
         if(m_count==ArraySize(m_entries))
           {
            Resize();
            targetBucket=hashCode%ArraySize(m_buckets);
           }
         index=m_count;
         m_count++;
        }
      m_entries[index].hashCode=hashCode;
      m_entries[index].next= m_buckets[targetBucket];
      m_entries[index].key = key;
      m_entries[index].value=value;
      m_buckets[targetBucket]=index;
      m_version++;
     }
   //+------------------------------------------------------------------+
   //| Method ExpandPrime from class HashHelpers.                       |
   //+------------------------------------------------------------------+
   int ExpandPrime(int oldSize)
     {
      int newSize=2*oldSize;
      const int MaxPrimeArrayLength=0x7FEFFFFD;
      //--- Allow the hashtables to grow to maximum possible size (~2G elements) before encoutering capacity overflow.
      //--- Note that this check works even when _items.Length overflowed thanks to the (uint) cast
      if((uint)newSize>(uint)MaxPrimeArrayLength && MaxPrimeArrayLength>oldSize)
        {
         return (MaxPrimeArrayLength);
        }
      return GetPrime(newSize);
     }
   //+------------------------------------------------------------------+
   //| Method Resize.                                                   |
   //+------------------------------------------------------------------+
   void Resize()
     {
      Resize(ExpandPrime(m_count),false);
     }
   //+------------------------------------------------------------------+
   //| Method Resize.                                                   |
   //+------------------------------------------------------------------+
   void Resize(int newSize,bool forceNewHashCodes)
     {
      int newBuckets[];
      ArrayResize(newBuckets,newSize);
      ArrayFill(newBuckets,0,ArraySize(newBuckets),-1);
      Entry<TKey,TValue>newEntries[];
      ArrayResize(newEntries,newSize);
      for(int i=0; i<ArraySize(m_entries); i++)
        {
         newEntries[i]=m_entries[i];
        }
      if(forceNewHashCodes)
        {
         for(int i=0; i<m_count; i++)
           {
            if(newEntries[i].hashCode!=-1)
              {
               newEntries[i].hashCode=(m_comparer.GetHashCode(newEntries[i].key) &0x7FFFFFFF);
              }
           }
        }
      for(int i=0; i<m_count; i++)
        {
         if(newEntries[i].hashCode>=0)
           {
            int bucket=newEntries[i].hashCode%newSize;
            newEntries[i].next = newBuckets[bucket];
            newBuckets[bucket] = i;
           }
        }
      ArrayResize(m_buckets,newSize);
      ArrayResize(m_entries,newSize);
      ArrayCopy(m_buckets,newBuckets);
      for(int i=0; i<newSize; i++)
        {
         m_entries[i]=newEntries[i];
        }
     }
public:
   //+------------------------------------------------------------------+
   //| Removes the value with the specified key from the                |
   //| Dictionary<TKey, TValue>.                                        |
   //+------------------------------------------------------------------+
   bool Remove(TKey key)
     {
      if(key==NULL && StringFind(typename(TKey),"*")!=-1)
        {
         Print("Argument 'key' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (false);
        }
      if(ArraySize(m_buckets)!=NULL)
        {
         int hashCode=m_comparer.GetHashCode(key) &0x7FFFFFFF;
         int bucket=hashCode%ArraySize(m_buckets);
         int last=-1;
         for(int i=m_buckets[bucket]; i>=0; last=i,i=m_entries[i].next)
           {
            if(m_entries[i].hashCode==hashCode && m_comparer.Equals(m_entries[i].key,key))
              {
               if(last<0)
                 {
                  m_buckets[bucket]=m_entries[i].next;
                 }
               else
                 {
                  m_entries[last].next=m_entries[i].next;
                 }
               m_entries[i].hashCode=-1;
               m_entries[i].next=m_freeList;
               TKey zeroTKey;
               ZeroMemory(zeroTKey);
               TValue zeroTValue;
               ZeroMemory(zeroTValue);
               m_entries[i].key=zeroTKey;
               m_entries[i].value=zeroTValue;
               m_freeList=i;
               m_freeCount++;
               m_version++;
               return (true);
              }
           }
        }
      return (false);
     }
   //+------------------------------------------------------------------+
   //| Gets the value associated with the specified key.                |
   //+------------------------------------------------------------------+
   bool TryGetValue(TKey key,TValue &value)
     {
      int i= FindEntry(key);
      if(i>=0)
        {
         value=m_entries[i].value;
         return (true);
        }
      ZeroMemory(value);
      return (false);
     }
   //+------------------------------------------------------------------+
   //| This is a convenience method for the internal callers that were  |
   //| converted from using Hashtable. Many were combining key doesn't  |
   //| exist and key exists but null value (for non-value types) checks.|
   //| This allows them to continue getting that behavior with minimal  |
   //| code delta. This is basically TryGetValue without the out param. |
   //+------------------------------------------------------------------+   
   TValue GetValueOrDefault(TKey key)
     {
      int i= FindEntry(key);
      if(i>=0)
        {
         return (m_entries[i].value);
        }
      Print("Key not found."," __FILE__ = "," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
      TValue zero;
      ZeroMemory(zero);
      return (zero);
     }
  };
//+------------------------------------------------------------------+
