//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
#include "List.mqh"
//+------------------------------------------------------------------+
//| Purpose: Provides the base class for a generic collection.       |
//+------------------------------------------------------------------+
template<typename T>
class Collection: public IList<T>
  {
private:
   IList<T>*m_items;
public:
   //+------------------------------------------------------------------+
   //| Initializes a new instance of the Collection<T> class that is    |
   //| empty.                                                           |
   //+------------------------------------------------------------------+
                     Collection()
     {
      m_items=new List<T>();
     }
   //+------------------------------------------------------------------+
   //| Initializes a new instance of the Collection<T> class as a       |
   //| wrapper for the specified list.                                  |
   //+------------------------------------------------------------------+
                     Collection(IList<T>*list)
     {
      if(list==NULL)
        {
         Print("Argument 'list' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
        }
      else
        {
         m_items=new List<T>(list);
        }
     }
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~Collection()
     {
      delete m_items;
     }
   //+------------------------------------------------------------------+
   //| Gets the number of elements actually contained in the            |
   //| Collection<T>.                                                   |
   //+------------------------------------------------------------------+
   int Count()
     {
      return (m_items.Count());
     }
public:
   //+------------------------------------------------------------------+
   //| Gets the element at the specified index.                         |
   //+------------------------------------------------------------------+
   T operator[](int index)
     {
      return (m_items[index]);
     }
   //+------------------------------------------------------------------+
   //| Sets the element at the specified index.                         |
   //+------------------------------------------------------------------+
   void Set(const int index,T value)
     {
      if(m_items.IsReadOnly())
        {
         Print("Not supported."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      if(index<0 || index>=m_items.Count())
        {
         Print("Argument 'index' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      SetItem(index,value);
     }
   //+------------------------------------------------------------------+
   //| Adds an object to the end of the Collection<T>.                  |
   //+------------------------------------------------------------------+     
   void Add(T item)
     {
      if(m_items.IsReadOnly())
        {
         Print("Not supported."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      int index=m_items.Count();
      InsertItem(index,item);
     }
   //+------------------------------------------------------------------+
   //| Removes all elements from the Collection<T>.                     |
   //+------------------------------------------------------------------+
   void Clear()
     {
      if(m_items.IsReadOnly())
        {
         Print("Not supported."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      ClearItems();
     }
   //+------------------------------------------------------------------+
   //| Copies the entire Collection<T> to a compatible one-dimensional  |
   //| Array, starting at the specified index of the target array.      |
   //+------------------------------------------------------------------+
   void CopyTo(T &array[],const int index)
     {
      m_items.CopyTo(array,index);
     }
   //+------------------------------------------------------------------+
   //| Determines whether an element is in the Collection<T>.           |
   //+------------------------------------------------------------------+
   bool Contains(T item)
     {
      //--- return result
      return (m_items.Contains(item));
     }
   //+------------------------------------------------------------------+
   //| Returns an enumerator that iterates through the Collection<T>.   |
   //+------------------------------------------------------------------+
   IEnumerator<T>*GetEnumerator()
     {
      return (m_items.GetEnumerator());
     }
   //+------------------------------------------------------------------+
   //| Searches for the specified object and returns the zero-based     |
   //| index of the first occurrence within the entire Collection<T>.   |
   //+------------------------------------------------------------------+
   int IndexOf(T item)
     {
      return (m_items.IndexOf(item));
     }
   //+------------------------------------------------------------------+
   //| Inserts an element into the Collection<T> at the specified index.|
   //+------------------------------------------------------------------+
   void Insert(const int index,T item)
     {
      if(m_items.IsReadOnly())
        {
         Print("Not supported."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      if(index<0 || index>=m_items.Count())
        {
         Print("Argument 'index' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      InsertItem(index,item);
     }
   //+------------------------------------------------------------------+
   //| Removes the first occurrence of a specific object from the       |
   //| Collection<T>.                                                   |
   //+------------------------------------------------------------------+     
   bool Remove(T item)
     {
      if(m_items.IsReadOnly())
        {
         Print("Not supported."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (false);
        }
      int index=m_items.IndexOf(item);
      if(index<0)
        {
         return (false);
        }
      RemoveItem(index);
      return (true);
     }
   //+------------------------------------------------------------------+
   //| Removes the element at the specified index of the Collection<T>. |
   //+------------------------------------------------------------------+
   void RemoveAt(const int index)
     {
      if(m_items.IsReadOnly())
        {
         Print("Not supported."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      if(index<0 || index>=m_items.Count())
        {
         Print("Argument 'index' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      RemoveItem(index);
     }
   //+------------------------------------------------------------------+
   //| Gets a value indicating whether the ICollection<T> is read-only. |
   //+------------------------------------------------------------------+
   bool IsReadOnly()
     {
      return (m_items.IsReadOnly());
     }
protected:
   //+------------------------------------------------------------------+
   //| Removes all elements from the Collection<T>.                     |
   //+------------------------------------------------------------------+
   virtual void ClearItems()
     {
      m_items.Clear();
     }
   //+------------------------------------------------------------------+
   //| Inserts an element into the Collection<T> at the specified index.|
   //+------------------------------------------------------------------+
   virtual void InsertItem(const int index,T item)
     {
      m_items.Insert(index,item);
     }
   //+------------------------------------------------------------------+
   //| Removes the element at the specified index of the Collection<T>. |
   //+------------------------------------------------------------------+
   virtual void RemoveItem(const int index)
     {
      m_items.RemoveAt(index);
     }
   //+------------------------------------------------------------------+
   //| Replaces the element at the specified index.                     |
   //+------------------------------------------------------------------+
   virtual void SetItem(int index,T item)
     {
      m_items.Set(index,item);
     }
   //+------------------------------------------------------------------+
   //| Gets a IList<T> wrapper around the Collection<T>.                |
   //+------------------------------------------------------------------+
   IList<T>*Items()
     {
      return (m_items);
     }
  };
//+------------------------------------------------------------------+
