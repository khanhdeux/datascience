//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
#include "IEqualityComparer.mqh"
//+------------------------------------------------------------------+
//| Purpose: Provides a base class for implementations of the        |
//| IEqualityComparer<T> generic interface.                          |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Class EqualityComparer.                                          |
//+------------------------------------------------------------------+
template<typename T>
class EqualityComparer:  public IEqualityComparer<T>
  {
private:
   static            EqualityComparer<T>*defaultComparer;
public:
   //+------------------------------------------------------------------+
   //| Constructor without parameters.                                  |
   //+------------------------------------------------------------------+
                     EqualityComparer()
     {
     }
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     EqualityComparer(EqualityComparer &comparer)
     {
      this.defaultComparer=comparer.defaultComparer;
     }
   //+------------------------------------------------------------------+
   //| Returns a default equality comparer for the type specified by the|
   //| generic argument.                                                |
   //+------------------------------------------------------------------+
   static EqualityComparer<T>*Default()
     {
      EqualityComparer<T>*comparer;
      if(CheckPointer(defaultComparer)==POINTER_INVALID)
        {
         EqualityComparerDefault<T>*defCopmarer=new EqualityComparerDefault<T>();
         return (defCopmarer);
        }
      else
        {
         comparer=defaultComparer;
        }
      return (comparer);
     }
private:
   //+------------------------------------------------------------------+
   //| Create EqualityComparerDefault.                                  |
   //+------------------------------------------------------------------+   
   static EqualityComparer<T>*CreateComparer()
     {
      EqualityComparerDefault<T>*defCopmarer=new EqualityComparerDefault<T>();
      return (defCopmarer);
     }
public:
   //+------------------------------------------------------------------+
   //| When overridden in a derived class, determines whether two       |
   //| objects of type T are equal.                                     |
   //+------------------------------------------------------------------+
   virtual bool Equals(T x,T y)
     {
      return (false);
     }
   //+------------------------------------------------------------------+
   //| When overridden in a derived class, serves as a hash function for|
   //| the specified object for hashing algorithms and data structures, |
   //| such as a hash table.                                            |
   //+------------------------------------------------------------------+     
   virtual int GetHashCode(T obj)
     {
      return (0);
     }
  };
template<typename T>
static EqualityComparer<T>*EqualityComparer::defaultComparer=NULL;
//+------------------------------------------------------------------+
//| Class EqualityComparerDefault.                                   |
//+------------------------------------------------------------------+
template<typename T>
class EqualityComparerDefault: public EqualityComparer<T>
  {
public:
   //+------------------------------------------------------------------+
   //| Determines whether two objects of type T are equal.              |
   //+------------------------------------------------------------------+
   bool Equals(T x,T y)
     {
      return (Compare(x,y));
     }
   //+------------------------------------------------------------------+
   //| Serves as a hash function for the specified object. Default      |
   //| returns 0.                                                       |
   //+------------------------------------------------------------------+   
   int GetHashCode(T obj)
     {
      return (0);
     }
  };
//+------------------------------------------------------------------+
//| Determines whether two pointers to the object of type T are      |
//| equal.                                                           |
//+------------------------------------------------------------------+
template<typename T>
bool Compare(T *x,T*y)
  {
   if(x==NULL || y==NULL)
     {
      return (x==y);
     }
   else
     {
      return (*x==y || x==y);
     }
  }
//+------------------------------------------------------------------+
//| Determines whether two objects of type T are equal.              |
//+------------------------------------------------------------------+
template<typename T>
bool Compare(T x,T y)
  {
   return (x==y);
  }
//+------------------------------------------------------------------+
