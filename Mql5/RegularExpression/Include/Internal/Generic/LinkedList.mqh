//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
#include "EqualityComparer.mqh"
#include "ICollection.mqh"
//+------------------------------------------------------------------+
//| Class Enumerator for LinkedList<T>.                              |
//+------------------------------------------------------------------+ 
template<typename T>
class LinkedListEnumerator : public IEnumerator<T>
  {
private:
   LinkedList<T>*m_list;
   LinkedListNode<T>*m_node;
   T                 m_current;
   int               m_version;
   int               m_index;
public:
   //+------------------------------------------------------------------+
   //| Constructor without parameters.                                  |
   //+------------------------------------------------------------------+
                     LinkedListEnumerator(LinkedList<T>*list)
     {
      this.m_list=list;
      m_version=list.Version();
      m_node=list.First();
      m_current=NULL;
      m_index=0;
     }
   //+------------------------------------------------------------------+
   //| Gets current element.                                            |
   //+------------------------------------------------------------------+
   T Current()
     {
      return (m_current);
     }
   //+------------------------------------------------------------------+
   //| Advances the enumerator to the next element of the collection.   |
   //+------------------------------------------------------------------+ 
   bool MoveNext()
     {
      if(m_version!=m_list.Version())
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (false);
        }
      if(m_node==NULL)
        {
         m_index=m_list.Count()+1;
         //--- return result
         return (false);
        }
      ++m_index;
      m_current=m_node.Value();
      m_node=m_node.next();
      if(m_node==m_list.First())
        {
         m_node=NULL;
        }
      //--- return result
      return (true);
     }
   //+------------------------------------------------------------------+
   //| Reset.                                                           |
   //+------------------------------------------------------------------+  
   void Reset()
     {
      if(m_version!=m_list.Version())
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      m_current=NULL;
      m_node=m_list.First();
      m_index=0;
     }
  };
//+------------------------------------------------------------------+
//| Purpose: Represents a doubly linked list.                        |
//+------------------------------------------------------------------+
template<typename T>
class LinkedList : public ICollection<T>
  {
private:
   LinkedListNode<T>*m_head;
   int               m_count;
public:
   //+------------------------------------------------------------------+
   //| Initializes a new instance of the LinkedList<T> class that is    |
   //| empty.                                                           |
   //+------------------------------------------------------------------+
                     LinkedList() : m_count(0)
     {
     }
   //+------------------------------------------------------------------+
   //| Initializes a new instance of the LinkedList<T> class that       |
   //| contains elements copied from the specified IEnumerable and has  |
   //| sufficient capacity to accommodate the number of elements copied.|
   //+------------------------------------------------------------------+
                     LinkedList(IEnumerable<T>*collection) : m_count(0)
     {
      if(collection==NULL)
        {
         Print("Argument 'collection' = Null");
        }
      else
        {
         IEnumerator<T>*en=collection.GetEnumerator();
         while(en.MoveNext())
           {
            AddLast(en.Current());
           }
        }
     }
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~LinkedList()
     {
      if(CheckPointer(m_head)==POINTER_DYNAMIC)
        {
         delete m_head;
        }
     }
   //+------------------------------------------------------------------+
   //| Gets version of list.                                            |
   //+------------------------------------------------------------------+
   int Version()
     {
      return (m_version);
     }
   //+------------------------------------------------------------------+
   //| Gets the number of nodes actually contained in the LinkedList<T>.|
   //+------------------------------------------------------------------+
   int Count()
     {
      return (m_count);
     }
   //+------------------------------------------------------------------+
   //| Gets the first node of the LinkedList<T>.                        |
   //+------------------------------------------------------------------+
   LinkedListNode<T>*First()
     {
      return (m_head);
     }
   //+------------------------------------------------------------------+
   //| Gets the last node of the LinkedList<T>.                         |
   //+------------------------------------------------------------------+
   LinkedListNode<T>*Last()
     {
      return (m_head==NULL? NULL: m_head.prev());
     }
   //+------------------------------------------------------------------+
   //| Adds an item at the end of the ICollection<T>.                   |
   //+------------------------------------------------------------------+
   void Add(T value)
     {
      AddLast(value);
     }
   //+------------------------------------------------------------------+
   //| Adds a new node containing the specified value after the         |
   //| specified existing node in the LinkedList<T>.                    |
   //+------------------------------------------------------------------+
   LinkedListNode<T>*AddAfter(LinkedListNode<T>*node,T value)
     {
      ValidateNode(node);
      LinkedListNode<T>*result=new LinkedListNode<T>(node.List(),value);
      InternalInsertNodeBefore(node.next(),result);
      return (result);
     }
   //+------------------------------------------------------------------+
   //| Adds the specified new node after the specified existing node in |
   //| the LinkedList<T>.                                               |
   //+------------------------------------------------------------------+
   void AddAfter(LinkedListNode<T>*node,LinkedListNode<T>*newNode)
     {
      ValidateNode(node);
      ValidateNewNode(newNode);
      InternalInsertNodeBefore(node.next(),newNode);
      newNode.List(GetPointer(this));
     }
   //+------------------------------------------------------------------+
   //| Adds a new node containing the specified value before the        |
   //| specified existing node in the LinkedList<T>.                    |
   //+------------------------------------------------------------------+
   LinkedListNode<T>*AddBefore(LinkedListNode<T>*node,T value)
     {
      ValidateNode(node);
      LinkedListNode<T>*result=new LinkedListNode<T>(node.List(),value);
      InternalInsertNodeBefore(node,result);
      if(node==m_head)
        {
         if(m_head!=NULL)
           {
            delete m_head;
           }
         m_head=result;
        }
      return (result);
     }
   //+------------------------------------------------------------------+
   //| Adds the specified new node before the specified existing node in|
   //| the LinkedList<T>.                                               |
   //+------------------------------------------------------------------+
   void AddBefore(LinkedListNode<T>*node,LinkedListNode<T>*newNode)
     {
      ValidateNode(node);
      ValidateNewNode(newNode);
      InternalInsertNodeBefore(node,newNode);
      if(newNode.List()!=NULL)
        {
         delete newNode.List();
        }
      newNode.List(GetPointer(this));
      if(node==m_head)
        {
         if(m_head!=NULL)
           {
            delete m_head;
           }
         m_head=newNode;
        }
     }
   //+------------------------------------------------------------------+
   //| Adds a new node containing the specified value at the start of   |
   //| the LinkedList<T>.                                               |
   //+------------------------------------------------------------------+
   LinkedListNode<T>*AddFirst(T value)
     {
      LinkedListNode<T>*result=new LinkedListNode<T>(GetPointer(this),value);
      if(m_head==NULL)
        {
         InternalInsertNodeToEmptyList(result);
        }
      else
        {
         InternalInsertNodeBefore(m_head,result);
         m_head=result;
        }
      return (result);
     }
   //+------------------------------------------------------------------+
   //| Adds the specified new node at the start of the LinkedList<T>.   |
   //+------------------------------------------------------------------+
   void AddFirst(LinkedListNode<T>*node)
     {
      ValidateNewNode(node);
      if(m_head==NULL)
        {
         InternalInsertNodeToEmptyList(node);
        }
      else
        {
         InternalInsertNodeBefore(m_head,node);
         m_head=node;
        }
      if(node.List()!=NULL)
        {
         delete node.List();
        }
      node.List(GetPointer(this));
     }
   //+------------------------------------------------------------------+
   //| Adds a new node containing the specified value at the end of the |
   //| LinkedList<T>.                                                   |
   //+------------------------------------------------------------------+
   LinkedListNode<T>*AddLast(T value)
     {
      LinkedListNode<T>*result=new LinkedListNode<T>(GetPointer(this),value);
      if(m_head==NULL)
        {
         InternalInsertNodeToEmptyList(result);
        }
      else
        {
         InternalInsertNodeBefore(m_head,result);
        } 
      return (result);
     }
   //+------------------------------------------------------------------+
   //| Adds the specified new node at the end of the LinkedList<T>.     |
   //+------------------------------------------------------------------+
   void AddLast(LinkedListNode<T>*node)
     {
      ValidateNewNode(node);
      if(m_head==NULL)
        {
         InternalInsertNodeToEmptyList(node);
        }
      else
        {
         InternalInsertNodeBefore(m_head,node);
        }
      if(node.List()!=NULL)
        {
         delete node.List();
        }
      node.List(GetPointer(this));
     }
   //+------------------------------------------------------------------+
   //| Removes all nodes from the LinkedList<T>.                        |
   //+------------------------------------------------------------------+
   void Clear()
     {
      LinkedListNode<T>*current=m_head;
      while(current!=NULL)
        {
         LinkedListNode<T>*temp=current;
         current=current.Next();  // use Next the instead of next, otherwise it will loop forever
         temp.Invalidate();
        }
      delete m_head;
      m_count=0;
      m_version++;
     }
   //+------------------------------------------------------------------+
   //| Determines whether a value is in the LinkedList<T>.              |
   //+------------------------------------------------------------------+
   bool Contains(T value)
     {
      return (Find(value) != NULL);
     }
   //+------------------------------------------------------------------+
   //| Copies the entire LinkedList<T> to a compatible one-dimensional  |
   //| Array, starting at the specified index of the target array.      |
   //+------------------------------------------------------------------+
   void CopyTo(T &array[],const int index)
     {
      int i=index;
      if(i<0 || i>ArraySize(array))
        {
         Print("Argument 'i' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      if(ArraySize(array)-i<Count())
        {
         Print("invalid argument."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      LinkedListNode<T>*node=m_head;
      if(node!=NULL)
        {
         do
           {
            array[i++]=node.Value();
            node=node.next();
           }
         while(node!=m_head);
        }
     }
   //+------------------------------------------------------------------+
   //| Finds the first node that contains the specified value.          |
   //+------------------------------------------------------------------+
   LinkedListNode<T>*Find(T value)
     {
      LinkedListNode<T>*node = m_head;
      EqualityComparer<T> *c = EqualityComparer<T>::Default();
      if(node!=NULL)
        {
         if(value!=NULL)
           {
            do
              {
               if(c.Equals(node.Value(),value))
                 {
                  //--- return node
                  return (node);
                 }
               node=node.next();
              }
            while(node!=m_head);
           }
         else
           {
            do
              {
               if(node.Value()==NULL)
                 {
                  //--- return node
                  return (node);
                 }
               node=node.next();
              }
            while(node!=m_head);
           }
        }
      //--- return NULL
      return (NULL);
     }
   //+------------------------------------------------------------------+
   //| Finds the last node that contains the specified value.           |
   //+------------------------------------------------------------------+
   LinkedListNode<T>*FindLast(T value)
     {
      if(m_head==NULL)
        {
         //--- return NULL
         return (NULL);
        }
      LinkedListNode<T> *last = m_head.prev();
      LinkedListNode<T> *node = last;
      EqualityComparer<T>*c=EqualityComparer<T>::Default();
      if(node!=NULL)
        {
         if(value!=NULL)
           {
            do
              {
               if(c.Equals(node.Value(),value))
                 {
                  //--- return node
                  return (node);
                 }
               node=node.prev();
              }
            while(node!=last);
           }
         else
           {
            do
              {
               if(node.Value()==NULL)
                 {
                  //--- return node
                  return (node);
                 }
               node=node.prev();
              }
            while(node!=last);
           }
        }
      //--- return NULL
      return (NULL);
     }
   //+------------------------------------------------------------------+
   //| Returns an enumerator that iterates through the LinkedList<T>.   |
   //+------------------------------------------------------------------+     
   LinkedListEnumerator<T>*GetEnumerator()
     {
      return new LinkedListEnumerator<T>(GetPointer(this));
     }
   //+------------------------------------------------------------------+
   //| Removes the first occurrence of the specified value from the     |
   //| LinkedList<T>.                                                   |
   //+------------------------------------------------------------------+
   bool Remove(T value)
     {
      LinkedListNode<T>*node=Find(value);
      if(node!=NULL)
        {
         InternalRemoveNode(node);
         return (true);
        }     
      return (false);
     }
   //+------------------------------------------------------------------+
   //| Removes the specified node from the LinkedList<T>.               |
   //+------------------------------------------------------------------+
   void Remove(LinkedListNode<T>*node)
     {
      ValidateNode(node);
      InternalRemoveNode(node);
     }
   //+------------------------------------------------------------------+
   //| Removes the node at the start of the LinkedList<T>.              |
   //+------------------------------------------------------------------+
   void RemoveFirst()
     {
      if(m_head==NULL)
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      InternalRemoveNode(m_head);
     }
   //+------------------------------------------------------------------+
   //| Removes the node at the end of the LinkedList<T>.                |
   //+------------------------------------------------------------------+
   void RemoveLast()
     {
      if(m_head==NULL)
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      InternalRemoveNode(m_head.prev());
     }
private:
   //+------------------------------------------------------------------+
   //| InternalInsertNodeBefore.                                        |
   //+------------------------------------------------------------------+
   void InternalInsertNodeBefore(LinkedListNode<T>*node,LinkedListNode<T>*newNode)
     {
      newNode.Next(node);
      newNode.Previous(node.prev());
      LinkedListNode<T>*n=node.prev();
      node.prev().Next(newNode);
      node.Previous(newNode);
      m_version++;
      m_count++;
     }
   //+------------------------------------------------------------------+
   //| InternalInsertNodeToEmptyList.                                   |
   //+------------------------------------------------------------------+
   void InternalInsertNodeToEmptyList(LinkedListNode<T>*newNode)
     {
      newNode.Next(newNode);
      newNode.Previous(newNode);
      m_head=newNode;
      m_version++;
      m_count++;
     }
   //+------------------------------------------------------------------+
   //| InternalRemoveNode.                                              |
   //+------------------------------------------------------------------+
   void InternalRemoveNode(LinkedListNode<T>*node)
     {
      if(node.next()==node)
        {
         m_head=NULL;
        }
      else
        {
         node.next().Previous(node.prev());
         node.prev().Next(node.next());
         if(m_head==node)
           {
            m_head=node.next();
           }
        }
      node.Invalidate();
      m_count--;
      m_version++;
     }
   //+------------------------------------------------------------------+
   //| ValidateNewNode.                                                 |
   //+------------------------------------------------------------------+
   void ValidateNewNode(LinkedListNode<T>*node)
     {
      if(node==NULL)
        {
         Print("Argument 'node' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      if(node.List()!=NULL)
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
     }
   //+------------------------------------------------------------------+
   //| ValidateNode.                                                    |
   //+------------------------------------------------------------------+
   void ValidateNode(LinkedListNode<T>*node)
     {
      if(node==NULL)
        {
         Print("Argument 'node' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      if(node.List()!=GetPointer(this))
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return
         return;
        }
     }
  };
//+------------------------------------------------------------------+
//| Purpose: Represents a node in a LinkedList<T>.This class cannot  |
//| be inherited.                                                    |
//+------------------------------------------------------------------+
template<typename T>
class LinkedListNode : public IComparable
  {
private:
   LinkedList<T>*m_list;
   LinkedListNode<T>*m_next;
   LinkedListNode<T>*m_prev;
   T                 m_item;
public:
   //+------------------------------------------------------------------+
   //| Initializes a new instance of the LinkedListNode<T> class,       |
   //| containing the specified value.                                  |
   //+------------------------------------------------------------------+
                     LinkedListNode(T value)
     {
      this.m_item=value;
     }
   //+------------------------------------------------------------------+
   //| Initializes a new instance of the LinkedListNode<T> class,       |
   //| containing the specified value.                                  |
   //+------------------------------------------------------------------+
                     LinkedListNode(LinkedList<T>*list,T value)
     {
      this.m_list = list;
      this.m_item = value;
     }
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~LinkedListNode()
     {
      if(CheckPointer(Next())==POINTER_DYNAMIC)
        {
         delete Next();
        }
     }  
   //+------------------------------------------------------------------+
   //| Gets the LinkedList<T> that the LinkedListNode<T> belongs to.    |
   //+------------------------------------------------------------------+
   LinkedList<T>*List()
     {
      return (m_list);
     }
   //+------------------------------------------------------------------+
   //| Sets the LinkedList<T> that the LinkedListNode<T> belongs to.    |
   //+------------------------------------------------------------------+
   void List(LinkedList<T>*value)
     {
      m_list=value;
     }
   //+------------------------------------------------------------------+
   //| Gets the next node in the LinkedList<T>.                         |
   //+------------------------------------------------------------------+
   LinkedListNode<T>*Next()
     {
      return (m_next==NULL || m_list == NULL || m_next==m_list.First()? NULL : m_next);
     }
   //+------------------------------------------------------------------+
   //| Sets the next node in the LinkedList<T>.                         |
   //+------------------------------------------------------------------+  
   void Next(LinkedListNode<T>*value)
     {
      m_next=value;
     }
   //+------------------------------------------------------------------+
   //| Gets the previous node in the LinkedList<T>.                     |
   //+------------------------------------------------------------------+
   LinkedListNode<T>*Previous()
     {
      return (m_prev==NULL || GetPointer(this)==m_list.First()? NULL : m_prev);
     }
   //+------------------------------------------------------------------+
   //| Sets the previous node in the LinkedList<T>.                     |
   //+------------------------------------------------------------------+ 
   void Previous(LinkedListNode<T>*value)
     {
      m_prev=value;
     }
   //+------------------------------------------------------------------+
   //| Gets the next node.                                              |
   //+------------------------------------------------------------------+
   LinkedListNode<T>*next()
     {
      return (m_next);
     }
   //+------------------------------------------------------------------+
   //| Gets the prev node.                                              |
   //+------------------------------------------------------------------+
   LinkedListNode<T>*prev()
     {
      return (m_prev);
     }
   //+------------------------------------------------------------------+
   //| Gets the value contained in the node.                            |
   //+------------------------------------------------------------------+
   T Value()
     {
      return (m_item);
     }
   //+------------------------------------------------------------------+
   //| Sets the value contained in the node.                            |
   //+------------------------------------------------------------------+
   void Value(T value)
     {
      m_item=value;
     }
   //+------------------------------------------------------------------+
   //| Invalidate.                                                      |
   //+------------------------------------------------------------------+
   void Invalidate()
     {
      m_list = NULL;
      m_next = NULL;
      m_prev = NULL;
     }
  };
//+------------------------------------------------------------------+
