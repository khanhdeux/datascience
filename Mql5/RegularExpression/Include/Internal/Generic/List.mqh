//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
#include "IList.mqh"
#include "IComparer.mqh"
#include <Internal\Array.mqh>
#include <Internal\Generic\IComparer.mqh>
#include <Internal\DynamicMatrix.mqh>
//+------------------------------------------------------------------+
//| Purpose: Represents the method that defines a set of criteria and|
//| determines whether the specified object meets those criteria.    |
//+------------------------------------------------------------------+
typedef bool(*Predicate)(IComparable*);
//+------------------------------------------------------------------+
//| Purpose: Implements a generic, dynamically sized list as an      |
//| array.                                                           |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Class Enumerator for List<T>.                                    |
//+------------------------------------------------------------------+ 
template<typename T>
class ListEnumerator : public IEnumerator<T>
  {
private:
   List<T>*m_list;
   T                 m_current;
   int               m_index;
   int               m_version;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     ListEnumerator(List<T>*list)
     {
      this.m_list=list;
      m_index=0;
      m_current=NULL;
      m_version=list.Version();
     }
   //+------------------------------------------------------------------+
   //| Advances the enumerator to the next element of the collection.   |
   //+------------------------------------------------------------------+
   bool MoveNext()
     {
      if(m_version==m_list.Version() && (uint)m_index<(uint)m_list.Count())
        {
         m_current=m_list[m_index];
         m_index++;
         return (true);
        }
      return MoveNextRare();
     }
   //+------------------------------------------------------------------+
   //| Advances the enumerator to emty element of the collection.       |
   //+------------------------------------------------------------------+
   bool MoveNextRare()
     {
      if(m_version!=m_list.Version())
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (false);
        }
      m_index=m_list.Count()+1;
      m_current=NULL;
      return (false);
     }
   //+------------------------------------------------------------------+
   //| Gets current element.                                            |
   //+------------------------------------------------------------------+
   T Current()
     {
      return (m_current);
     }
   //+------------------------------------------------------------------+
   //| Reset.                                                           |
   //+------------------------------------------------------------------+  
   void Reset()
     {
      if(m_version!=m_list.Version())
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      m_index=0;
     }
  };
//+------------------------------------------------------------------+
//| Implements a variable-size List that uses an array of objects to |
//| store the elements. A List has a capacity, which is the allocated|
//| length of the internal array. As elements are added to a List,   |
//| the capacity of the List is automatically increased as required  |
//| by reallocating the internal array.                              |
//+------------------------------------------------------------------+
template<typename T>
class List: public IList<T>
  {
private:
   const int         m_defaultCapacity;
   T                 m_items[];
   int               m_size;
   T                 m_emptyArray[];
public:
   //+------------------------------------------------------------------+
   //| Constructs a List. The list is initially empty and has a capacity|
   //| of zero. Upon adding the first element to the list the capacity  |
   //| is increased to 16, and then increased in multiples of two as    |
   //| required.                                                        |
   //+------------------------------------------------------------------+
                     List(): m_defaultCapacity(4),m_size(0)
     {
      m_version=0;
     }
   //+------------------------------------------------------------------+
   //| Constructs a List with a given initial capacity. The list is     |
   //| initially empty, but will have room for the given number of      |
   //| elements before any reallocations are required.                  |
   //+------------------------------------------------------------------+
                     List(const int capacity): m_defaultCapacity(4),m_size(0)
     {
      m_version=0;
      if(capacity<0)
        {
         Print("Argument 'capacity' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
        }
      else
        {
         ArrayResize(m_items,capacity);
         ZeroMemory(m_items);
        }
     }
   //+------------------------------------------------------------------+
   //| Constructs a List, copying the contents of the given array.      |
   //+------------------------------------------------------------------+
                     List(const T &array[]): m_defaultCapacity(4),m_size(0)
     {
      m_version=0;
      int count=ArraySize(array);
      if(count==0)
        {
         ArrayCopy(m_items,m_emptyArray);
        }
      else
        {
         ArrayCopy(m_items,array);
         m_size=count;
        }
     }
   //+------------------------------------------------------------------+
   //| Constructs a List, copying the contents of the given collection. |
   //| The size and capacity of the new list will both be equal to the  |
   //| size of the given collection.                                    |
   //+------------------------------------------------------------------+
                     List(IEnumerable<T>*collection): m_defaultCapacity(4),m_size(0)
     {
      m_version=0;
      if(collection==NULL)
        {
         Print("Argument 'collection' = Null");
         return;
        }
      else
        {
         ICollection<T>*c=dynamic_cast<ICollection<T>*>(collection);
         if(c!=NULL)
           {
            int count= c.Count();
            if(count == 0)
              {
               ArrayCopy(m_items,m_emptyArray);
              }
            else
              {
               ArrayResize(m_items,count);
               c.CopyTo(m_items,0);
               m_size=count;
              }
           }
         else
           {
            m_size=0;
            ArrayCopy(m_items,m_emptyArray);
            //+------------------------------------------------------------------+
            //| This enumerable could be empty.  Let Add allocate a new array, if|
            //| needed. Note it will also go to _defaultCapacity first, not 1,   |
            //| then 2, etc.                                                     |
            //+------------------------------------------------------------------+
            IEnumerator<T>*en=collection.GetEnumerator();
            while(en.MoveNext())
              {
               Add(en.Current());
              }
            delete en;
           }
        }
     }
   //+------------------------------------------------------------------+
   //| Gets version of list.                                            |
   //+------------------------------------------------------------------+
   int Version()
     {
      return (m_version);
     }
   //+------------------------------------------------------------------+
   //| Gets the capacity of this list.  The capacity is the size of the |
   //| internal array used to hold items.  When set, the internal array |
   //| of the list is reallocated to the given capacity.                |
   //+------------------------------------------------------------------+
   int Capacity()
     {
      return (ArraySize(m_items));
     }
   //+------------------------------------------------------------------+
   //| Sets the capacity of this list.  The capacity is the size of the |
   //| internal array used to hold items.  When set, the internal array |
   //| of the list is reallocated to the given capacity.                |
   //+------------------------------------------------------------------+
   void Capacity(const int value)
     {
      if(value<m_size)
        {
         Print("Argument 'capacity' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      if(value>ArraySize(m_items))
        {
         if(value>0)
           {
            ArrayResize(m_items,value);
           }
         else
           {
            ArrayCopy(m_items,m_emptyArray);
           }
        }
     }
   //+------------------------------------------------------------------+
   //| Read-only property describing how many elements are in the List. |
   //+------------------------------------------------------------------+
   int Count()
     {
      //--- return count
      return (m_size);
     }
   //+------------------------------------------------------------------+
   //| Gets the element at the given index.                             |
   //+------------------------------------------------------------------+
   T operator[](int index)
     {
      //--- Following trick can reduce the range check by one
      if((uint) index>=(uint)m_size)
        {
         Print("Argument 'index' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return (m_items[index]);
     }
   //+------------------------------------------------------------------+
   //| Gets the element at the given index.                             |
   //+------------------------------------------------------------------+
   void Set(const int index,T value)
     {
      if((uint) index>=(uint)m_size)
        {
         Print("Argument 'index' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      m_items[index]=value;
      m_version++;
     }
   //+------------------------------------------------------------------+
   //| Gets the element at a specified index in a sequence.             |
   //+------------------------------------------------------------------+
   T ElementAt(const int index)
     {
      T element=m_items[index];
      if(index<0 || index>=ArraySize(m_items))
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return (element);
     }
   //+------------------------------------------------------------------+
   //| Adds the given object to the end of this list. The size of the   |
   //| list is increased by one. If required, the capacity of the list  |
   //| is doubled before adding the new element.                        |
   //+------------------------------------------------------------------+
   void Add(T item)
     {
      if(m_size==ArraySize(m_items))
        {
         EnsureCapacity(m_size+1);
        }
      m_items[m_size++]=item;
      m_version++;
     }
   //+------------------------------------------------------------------+
   //| Adds the elements of the given collection to the end of this     |
   //| list. If required, the capacity of the list is increased to twice|
   //| the previous capacity or the new size, whichever is larger.      |
   //+------------------------------------------------------------------+
   void AddRange(IEnumerable<T>*collection)
     {
      InsertRange(m_size,collection);
     }
   //+------------------------------------------------------------------+
   //| Searches a section of the list for a given element using a binary| 
   //| search algorithm. Elements of the list are compared to the search|
   //| value using the given IComparer interface. If comparer is null,  |
   //| elements of the list are compared to the search value using the  |
   //| IComparable interface, which in that case must be implemented by |
   //| all elements of the list and the given search value. This method |
   //| assumes that the given section of the list is already sorted; if |
   //| this is not the case, the result will be incorrect.              |
   //|                                                                  |
   //| The method returns the index of the given value in the list. If  |
   //| the list does not contain the given value, the method returns a  |
   //| negative integer. The bitwise complement operator (~) can be     |
   //| applied to a negative result to produce the index of the first   |
   //| element (if any) that is larger than the given search value. This|
   //| is also the index at which the search value should be inserted   |
   //| into the list in order for the list to remain sorted.            |
   //|                                                                  |
   //| The method uses the Array.BinarySearch method to perform the     |
   //| search.                                                          |
   //+------------------------------------------------------------------+
   //+------------------------------------------------------------------+
   //| BinarySearch.                                                    |
   //+------------------------------------------------------------------+
   template<typename TComparer>
   int BinarySearch(const int index,const int count,T item,TComparer *comparer)
     {
      if(index<0)
        {
         Print("Argument 'index' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      if(count<0)
        {
         Print("Argument 'count' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
        }
      if(m_size-index<count)
        {
         Print("The argument 'number' must be greater than the difference: size of list and argument 'index'."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return ArrayBinarySearch(m_items, index, count, item, comparer);
     }
   //+------------------------------------------------------------------+
   //| BinarySearch.                                                    |
   //+------------------------------------------------------------------+
   template<typename TComparer>
   int BinarySearch(T item,IComparer<T>*comparer)
     {
      return List::BinarySearch(0, Count(), item, TComparer *comparer);
     }
   //+------------------------------------------------------------------+
   //| Clears the contents of List.                                     |
   //+------------------------------------------------------------------+
   void Clear()
     {
      if(m_size>0)
        {
         ArrayFree(m_items);
         m_size=0;
        }
      m_version++;
     }
   //+------------------------------------------------------------------+
   //| Contains returns true if the specified element is in the List.   |
   //| It does a linear, O(n) search.  Equality is determined by calling|
   //| item.Equals().                                                   |
   //+------------------------------------------------------------------+
   bool Contains(T item)
     {
      if(item==NULL)
        {
         for(int i=0; i<m_size; i++)
           {
            if(m_items[i]==NULL)
              {
               return (true);
              }
            return (false);
           }
        }
      else
        {
         for(int i=0; i<m_size; i++)
           {
            if(m_items[i]==item)
              {
               return (true);
              }
           }
         return (false);
        }
      return (false);
     }
   //+------------------------------------------------------------------+
   //| Copies this List into array, which must be of a compatible array |
   //| type.                                                            |
   //+------------------------------------------------------------------+
   void CopyTo(T &array[])
     {
      CopyTo(array,0);
     }
   //+------------------------------------------------------------------+
   //| Copies this List into array, which must be of a compatible array |
   //| type.                                                            |
   //+------------------------------------------------------------------+
   void CopyTo(T &array[],const int arrayIndex)
     {
      ArrayCopy(array,m_items,0,arrayIndex,m_size);
     }
   //+------------------------------------------------------------------+
   //| Copies a section of this list to the given array at the given    |
   //| index. The method uses the ArrayCopy method to copy the elements.|
   //+------------------------------------------------------------------+
   void CopyTo(int index,T &array[],int arrayIndex,int count)
     {
      if(m_size-index<count)
        {
         Print("The argument 'count' must be greater than the difference: size of list and argument 'index'."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return 
         return;
        }
      ArrayCopy(array,m_items,index,arrayIndex,count);
     }
private:
   //+------------------------------------------------------------------+
   //| Ensures that the capacity of this list is at least the given     |
   //| minimum value. If the currect capacity of the list is less than  |
   //| min, the capacity is increased to twice the current capacity or  |
   //| to min, whichever is larger.                                     |
   //+------------------------------------------------------------------+
   void EnsureCapacity(int min)
     {
      int MaxArrayLength=0X7FEFFFFF;
      if(ArraySize(m_items)<min)
        {
         int newCapacity=ArraySize(m_items)==0? m_defaultCapacity : ArraySize(m_items)*2;
         //--- Allow the list to grow to maximum possible capacity (~2G elements) before encountering overflow.
         //--- Note that this check works even when _items.Length overflowed thanks to the (uint) cast
         if(newCapacity>MaxArrayLength)
           {
            newCapacity=MaxArrayLength;
           }
         if(newCapacity<min)
           {
            newCapacity=min;
           }
         Capacity(newCapacity);
        }
     }
public:
   //+------------------------------------------------------------------+
   //| Returns an enumerator for this list with the given permission for|
   //| removal of elements. If modifications made to the list while an  |
   //| enumeration is in progress, the MoveNext and GetObject methods of|
   //| the enumerator will throw an exception.                          |
   //+------------------------------------------------------------------+   
   ListEnumerator<T>*GetEnumerator()
     {
      return new ListEnumerator<T>(GetPointer(this));
     }
   //+------------------------------------------------------------------+
   //| GetRange.                                                        |
   //+------------------------------------------------------------------+
   List<T>*GetRange(int index,int count)
     {
      if(index<0)
        {
         Print("Argument 'index' out of range");
         return (NULL);
        }
      if(count<0)
        {
         Print("Argument 'count' out of range");
         return (NULL);
        }
      if(m_size-index<count)
        {
         Print("The argument 'count' must be greater than the difference: size of list and argument 'index'."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      List<T>*list=new List<T>(count);
      ArrayCopy(m_items,list.m_items,index,0,count);
      list.m_size=count;
      return (list);
     }
   //+------------------------------------------------------------------+
   //| Returns the index of the first occurrence of a given value in a  |
   //| range of this list.                                              |
   //|                                                                  |
   //| This method uses the ArrayIndexOf method to perform the search.  |
   //+------------------------------------------------------------------+
   int IndexOf(T item)
     {
      return Array::IndexOf(m_items, item, 0,m_size);
     }
   //+------------------------------------------------------------------+
   //| Returns the index of the first occurrence of a given value in a  |
   //| range of this list.                                              |
   //|                                                                  |
   //| This method uses the ArrayIndexOf method to perform the search.  |
   //+------------------------------------------------------------------+
   int IndexOf(T item,int index)
     {
      if(index>m_size)
        {
         Print("Argument 'index' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (-1);
        }
      return Array::IndexOf(m_items, item, index, m_size - index);
     }
   //+------------------------------------------------------------------+
   //| Returns the index of the first occurrence of a given value in a  |
   //| range of this list.                                              |
   //|                                                                  |
   //| This method uses the ArrayIndexOf method to perform the search.  |
   //+------------------------------------------------------------------+
   int IndexOf(T item,int index,int count)
     {
      if(index>m_size)
        {
         Print("Argument 'index' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (-1);
        }

      if(count<0 || index>m_size-count)
        {
         Print("The argument 'index' must be greater than the difference: size of list and argument 'count'."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (-1);
        }
      return Array::IndexOf(m_items, item, index, count);
     }
   //+------------------------------------------------------------------+
   //| Inserts an element into this list at a given index. The size of  |
   //| the list is increased by one. If required, the capacity of the   |
   //| list is doubled before inserting the new element.                |
   //+------------------------------------------------------------------+
   void Insert(const int index,T item)
     {
      //--- Note that insertions at the end are legal.
      if((uint) index>(uint)m_size)
        {
         Print("Argument 'index' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      if(m_size==ArraySize(m_items))
        {
         EnsureCapacity(m_size+1);
        }
      if(index<m_size)
        {
         ArrayCopy(m_items,m_items,index,index+1,m_size-index);
        }
      m_items[index]=item;
      m_size++;
      m_version++;
     }
   //+------------------------------------------------------------------+
   //| Inserts the elements of the given collection at a given index. If|
   //| required, the capacity of the list is increased to twice the     |
   //| previous capacity or the new size, whichever is larger. Ranges   |
   //| may be added to the end of the list by setting index to the      |
   //| List's size.                                                     |
   //+------------------------------------------------------------------+
   void InsertRange(int index,IEnumerable<T>*collection)
     {
      if(collection==NULL)
        {
         Print("Argument 'collection' = NULL");
         return;
        }
      if((uint)index>(uint)m_size)
        {
         Print("Argument 'index' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      ICollection<T>*c=dynamic_cast<ICollection<T>*>(collection);
      if(c!=NULL)
        {
         //--- if collection is ICollection<T>
         int count=c.Count();
         if(count>0)
           {
            EnsureCapacity(m_size+count);
            if(index<m_size)
              {
               ArrayCopy(m_items,m_items,index,index+count,m_size-index);
              }
            //-- If we're inserting a List into itself, we want to be able to deal with that.
            if(GetPointer(this)==GetPointer(c))
              {
               //--- Copy first part of _items to insert location
               ArrayCopy(m_items,m_items,0,index,index);
               //--- Copy last part of _items back to inserted location
               ArrayCopy(m_items,m_items,index+count,index*2,m_size-index);
              }
            else
              {
               T itemsToInsert[];
               ArrayResize(itemsToInsert,count);
               c.CopyTo(itemsToInsert,0);
               ArrayCopy(m_items,itemsToInsert,index,0);
              }
            m_size+=count;
           }
        }
      else
        {
         IEnumerator<T>*en=collection.GetEnumerator();
         while(en.MoveNext())
           {
            en=collection.GetEnumerator();
            Insert(index++,en.Current());
           }
        }
      m_version++;
     }
   //+------------------------------------------------------------------+
   //| Returns the index of the last occurrence of a given value in a   |
   //| range of this list. The list is searched backwards, starting at  |
   //| the end and ending at the first element in the list.             |
   //|                                                                  |
   //| This method uses the Array.LastIndexOf method to perform the     |
   //| search.                                                          |
   //+------------------------------------------------------------------+
   int LastIndexOf(T item)
     {
      if(m_size==0)
        {
         //--- Special case for empty list
         return (-1);
        }
      else
        {
         return LastIndexOf(item, m_size - 1, m_size);
        }
     }
   //+------------------------------------------------------------------+
   //| Returns the index of the last occurrence of a given value in a   |
   //| range of this list. The list is searched backwards, starting at  |
   //| the end and ending at the first element in the list.             |
   //|                                                                  |
   //| This method uses the Array.LastIndexOf method to perform the     |
   //| search.                                                          |
   //+------------------------------------------------------------------+
   int LastIndexOf(T item,int index)
     {
      if(index>=m_size)
        {
         Print("Argument 'index' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return LastIndexOf(item, index, index + 1);
     }
   //+------------------------------------------------------------------+
   //| Returns the index of the last occurrence of a given value in a   |
   //| range of this list. The list is searched backwards, starting at  |
   //| the end and ending at the first element in the list.             |
   //|                                                                  |
   //| This method uses the Array.LastIndexOf method to perform the     |
   //| search.                                                          |
   //+------------------------------------------------------------------+
   int LastIndexOf(T item,int index,int count)
     {
      if((Count()!=0) && (index<0))
        {
         Print("Argument 'index' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      if((Count()!=0) && (count<0))
        {
         Print("Argument 'count' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      if(m_size==0)
        {
         //--- Special case for empty list
         return -1;
        }
      if(index>=m_size)
        {
         Print("Argument 'index' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      if(count>index+1)
        {
         Print("Argument 'count' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return Array::LastIndexOf(m_items, item, index, count);
     }
   //+------------------------------------------------------------------+
   //| Removes the element at the given index. The size of the list is  |
   //| decreased by one.                                                |
   //+------------------------------------------------------------------+
   bool Remove(T item)
     {
      int index= IndexOf(item);
      if(index>=0)
        {
         RemoveAt(index);
         return (true);
        }
      return (false);
     }
   //+------------------------------------------------------------------+
   //| Removes the element at the specified index of the List<T>.       |
   //+------------------------------------------------------------------+
   void RemoveAt(const int index)
     {
      if((uint)index>=(uint)m_size)
        {
         Print("Argument 'index' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      m_size--;
      if(index<m_size)
        {
         ArrayCopy(m_items,m_items,index,index+1,m_size-index);
        }
      m_items[m_size]=NULL;
      m_version++;
     }
   //+------------------------------------------------------------------+
   //| Removes a range of elements from this list.                      |
   //+------------------------------------------------------------------+
   void RemoveRange(int index,int count)
     {
      if(index<0)
        {
         Print("Argument 'index' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      if(count<0)
        {
         Print("Argument 'count' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      if(m_size-index<count)
        {
         Print("The argument 'count' must be greater than the difference: size of list and argument 'index'."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      if(count>0)
        {
         int i=m_size;
         m_size-=count;
         if(index<m_size)
           {
            ArrayCopy(m_items,m_items,index+count,index,m_size-index);
           }
         ArrayResize(m_items,m_size);
         m_version++;
        }
     }
   //+------------------------------------------------------------------+
   //| Reverses the elements in this list.                              |
   //+------------------------------------------------------------------+
   void Reverse()
     {
      Reverse(0,Count());
     }
   //+------------------------------------------------------------------+
   //| Reverses the elements in a range of this list. Following a call  |
   //| to this method, an element in the range given by index and count |
   //| which was previously located at index i will now be located at   |
   //| index index + (index + count - i - 1).                           |
   //+------------------------------------------------------------------+
   void Reverse(const int index,const int count)
     {
      if(index<0)
        {
         Print("Argument 'index' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      if(count<0)
        {
         Print("Argument 'count' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      if(m_size-index<count)
        {
         Print("Invalid argument."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      Array::Reverse(m_items,index,count);
      m_version++;
     }
   //+------------------------------------------------------------------+
   //| Sorts the elements in this list.  Uses the default comparer.     |
   //+------------------------------------------------------------------+
   void Sort()
     {
      Sort(0,Count(),NULL);
     }
   //+------------------------------------------------------------------+
   //| Sorts the elements in this list.  Uses Array.Sort with the       |
   //| provided comparer.                                               |
   //+------------------------------------------------------------------+
   void Sort(IComparer<T>*comparer)
     {
      Sort(0,Count(),comparer);
     }
   //+------------------------------------------------------------------+
   //| Sorts the elements in a section of this list. The sort compares  |
   //| the elements to each other using the given IComparer interface.  |
   //| If comparer is NULL, the elements are compared to each other     |
   //| using the IComparable interface, which in that case must be      |
   //| implemented by all elements of the list.                         |
   //+------------------------------------------------------------------+
   void Sort(const int index,const int count,IComparer<T>*comparer)
     {
      if(index<0)
        {
         Print("Argument 'index' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      if(count<0)
        {
         Print("Argument 'count' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      if(m_size-index<count)
        {
         Print("Invalid argument."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      Array::Sort(m_items,m_emptyArray,index,count,comparer);
      m_version++;
     }
   //+------------------------------------------------------------------+
   //| Copies the elements of the List<T> to a new array CRow<T>.       |
   //+------------------------------------------------------------------+
   CRow<T>*ToArray()
     {
      CRow<T>*array=new CRow<T>(this.Count());
      CRow<T>::Copy(array,m_items,0,0,m_size);
      return (array);
     }
   //+------------------------------------------------------------------+
   //| Retrieves all the elements that match the conditions defined by  |
   //| the specified predicate.                                         |
   //+------------------------------------------------------------------+
   List<T>*FindAll(Predicate match)
     {
      if(match==NULL)
        {
         Print("Argument 'match' = Null."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      List<T>*list=new List<T>();
      for(int i=0; i<m_size; i++)
        {
         IComparable *value=dynamic_cast<IComparable*>(m_items[i]);
         if(match(value))
           {
            list.Add(m_items[i]);
           }
        }
      return (list);
     }
  };
//+------------------------------------------------------------------+
