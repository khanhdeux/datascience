//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
#include <Internal\IComparable.mqh>
#include <Internal\Generic\EqualityComparer.mqh>
//+------------------------------------------------------------------+
//| Purpose: The generic IComparer interface implements a method that|
//| compares two objects. It is used in conjunction with the Sort and|
//| BinarySearch methods on the Array, List, and SortedList classes. |
//+------------------------------------------------------------------+
template<typename T>
class IComparer
  {
public:
   //+------------------------------------------------------------------+
   //| Compares two objects. An implementation of this method must      |
   //| return a value less than zero if x is less than y, zero if x is  |
   //| equal to y, or a value greater than zero if x is greater than y. |
   //+------------------------------------------------------------------+
   virtual int       Compare(T x,T y)
     {
      return (NULL);
     }
  };
//+------------------------------------------------------------------+
//| DefaultComparer.                                                 |
//+------------------------------------------------------------------+
template<typename T>
class DefaultComparer: public IComparer<T>
  {
public:
   //+------------------------------------------------------------------+
   //| Compares two objects. An implementation of this method must      |
   //| return a value less than zero if x is less than y, zero if x is  |
   //| equal to y, or a value greater than zero if x is greater than y. |
   //+------------------------------------------------------------------+
   int       Compare(T x,T y)
     {
      if(::Compare(x,y))
        {
         return (0);
        }
      else if(x>y)
        {
         return (1);
        }
      else if(x<y)
        {
         return (-1);
        }
      else
        {
         Print("Comparing objects can not be."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
     }
  };
//+------------------------------------------------------------------+
//| ReverseComparer.                                                 |
//+------------------------------------------------------------------+
template<typename T>
class ReverseComparer: public IComparer<T>
  {
private:
   IComparer<T>*m_comparer;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     ReverseComparer(IComparer<T>*comparer)
     {
      m_comparer=comparer;
     }
   //+------------------------------------------------------------------+
   //| Compares two objects.                                            |
   //+------------------------------------------------------------------+
   int Compare(T x,T y)
     {
      return (-1*m_comparer.Compare(x,y));
     }
  };
//+------------------------------------------------------------------+
