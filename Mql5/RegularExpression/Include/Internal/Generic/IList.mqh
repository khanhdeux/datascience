//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
#include "ICollection.mqh"
//+------------------------------------------------------------------+
//| Purpose: Represents a non-generic collection of objects that can |
//| be individually accessed by index.                               |
//+------------------------------------------------------------------+
template<typename T>
class IList : public ICollection<T>
  {
public:
   //+------------------------------------------------------------------+
   //| The Item property provides methods to read entries in the List.  |
   //+------------------------------------------------------------------+
   virtual T         operator[](int i)=NULL;
   //+------------------------------------------------------------------+
   //| The Item property provides methods to edit entries in the List.  |
   //+------------------------------------------------------------------+
   virtual void      Set(const int i,T value)=NULL;
   //+------------------------------------------------------------------+
   //| Returns the index of a particular item, if it is in the list.    |
   //| Returns -1 if the item isn't in the list.                        |
   //+------------------------------------------------------------------+   
   virtual int       IndexOf(T item)=NULL;
   //+------------------------------------------------------------------+
   //| Inserts value into the list at position index. Index must be     |
   //| non-negative and less than or equal to the number of elements in |
   //| the list. If index equals the number of items in the list, then  |
   //| value is appended to the end.                                    |
   //+------------------------------------------------------------------+
   virtual void      Insert(const int index,T item)=NULL;
   //+------------------------------------------------------------------+
   //| Removes the item at position index.                              |
   //+------------------------------------------------------------------+
   virtual void      RemoveAt(const int index)=NULL;
  };
//+------------------------------------------------------------------+
