//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
#include "ICollection.mqh"
#include "KeyValuePair.mqh"
//+------------------------------------------------------------------+
//| Purpose: Base interface for all generic dictionaries.            |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| An IDictionary is a possibly unordered set of key-value pairs.   |
//| Keys can be any non-null object.  Values can be any object.      |
//| You can look up a value in an IDictionary via the default indexed|
//| property, Items.                                                 |
//+------------------------------------------------------------------+
template<typename TKey,typename TValue>
class IDictionary: public ICollection<KeyValuePair<TKey,TValue>*>
  {
public:
   //+------------------------------------------------------------------+
   //| The Item property provides methods to read and edit entries in   |
   //| the Dictionary.                                                  |
   //+------------------------------------------------------------------+
   virtual TValue    operator[](TKey key)=NULL;
   //+------------------------------------------------------------------+
   //| Sets the element with the specified key.                         |
   //+------------------------------------------------------------------+
   virtual void      Set(TKey key,TValue value)=NULL;
   //+------------------------------------------------------------------+
   //| Returns whether this dictionary contains a particular key.       |
   //+------------------------------------------------------------------+
   virtual bool      ContainsKey(TKey key)=NULL;
   //+------------------------------------------------------------------+
   //| Adds a key-value pair to the dictionary.                         |
   //+------------------------------------------------------------------+
   virtual void      Add(TKey key,TValue value)=NULL;
   //+------------------------------------------------------------------+
   //| Removes a particular key from the dictionary.                    |
   //+------------------------------------------------------------------+
   virtual bool      Remove(TKey key)=NULL;
   //+------------------------------------------------------------------+
   //| Gets the value associated with the specified key.                |
   //+------------------------------------------------------------------+
   virtual bool      TryGetValue(TKey key,TValue &value)=NULL;
   //+------------------------------------------------------------------+
   //| Gets an ICollection<T> containing the keys of the                |
   //| IDictionary<TKey,TValue>.                                        |
   //+------------------------------------------------------------------+
   virtual           ICollection<TKey>*Keys()=NULL;
   //+------------------------------------------------------------------+
   //| Gets an ICollection<T> containing the values in the              |
   //| IDictionary<TKey,TValue>.                                        |
   //+------------------------------------------------------------------+
   virtual           ICollection<TValue>*Values()=NULL;
  };
//+------------------------------------------------------------------+
