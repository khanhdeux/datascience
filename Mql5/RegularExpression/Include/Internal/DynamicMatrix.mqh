//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
#include <Internal\Generic\IEnumerable.mqh>
#include <Internal\IComparable.mqh>
#include <Internal\Generic\IEnumerator.mqh>
#include <Internal\Generic\ICollection.mqh>
#include <Internal\Array.mqh>
//+------------------------------------------------------------------+
//| Purpose: Create dynamic maxtrix and array.                       |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| ArrayResizeAll Row                                               |
//+------------------------------------------------------------------+
template<typename T>
int ArrayResizeRow(T &arr[],const int size)
  {
   int old_size=ArraySize(arr);
   int res=ArrayResize(arr,size);
   if(old_size<size)
     {
      for(int i=old_size; i<size; i++)
        {
         T new_value;
         ZeroMemory(new_value);
         arr[i]=new_value;
        }
     }
   return (res);
  }
//+------------------------------------------------------------------+
//| Class "CRow"                                                     |
//+------------------------------------------------------------------+
template<typename T>
class CRow : public IEnumerable<T>
  {
private:
   T                 m_array[];
public:
   //+------------------------------------------------------------------+
   //| Constructor with parametrs.                                      |
   //+------------------------------------------------------------------+
                     CRow(const T &row[])
     {
      m_version=0;
      ArrayCopy(this.m_array,row);
     }
   //+------------------------------------------------------------------+
   //| Constructor with parametrs.                                      |
   //+------------------------------------------------------------------+
                     CRow(const CRow<T>&row) 
     {
      m_version=0;
      ArrayCopy(this.m_array,row.m_array);
     }
   //+------------------------------------------------------------------+
   //| Constructor with parametrs.                                      |
   //+------------------------------------------------------------------+
                     CRow(const int n) 
     {
      m_version=0;
      this.Resize(n);
     }
   //+------------------------------------------------------------------+
   //| Constructor without parametrs.                                   |
   //+------------------------------------------------------------------+
                     CRow() 
     {
      m_version=0;
     }
   //+------------------------------------------------------------------+
   //| Row size.                                                        |
   //+------------------------------------------------------------------+
   int Size(void)
     {
      return (ArraySize(m_array));
     }
   //+------------------------------------------------------------------+
   //| Resize.                                                          |
   //+------------------------------------------------------------------+  
   void Resize(const int n)
     {
      ArrayResizeRow(m_array,n);
      m_version++;
     }
   //+------------------------------------------------------------------+
   //| Set value.                                                       |
   //+------------------------------------------------------------------+     
   void Set(const int i,T d)
     {
      m_array[i]=d;
      m_version++;
     }
   //+------------------------------------------------------------------+
   //| Operator ([]).                                                   |
   //+------------------------------------------------------------------+     
   T operator[](const int i)
     {
      return(m_array[i]);
     }
   //+------------------------------------------------------------------+
   //| Operator (=).                                                    |
   //+------------------------------------------------------------------+     
   void operator=(T &array[])
     {
      int size=ArraySize(array);
      //--- check
      if(size==0)
        {
         return;
        }
      //--- filling array
      ArrayResizeRow(m_array,size);
      for(int i=0;i<size;i++)
        {
         m_array[i]=array[i];
        }
      m_version++;
     }
   //+------------------------------------------------------------------+
   //| Operator (=).                                                    |
   //+------------------------------------------------------------------+       
   void operator=(const CRow<T>&row)
     {
      ArrayFree(this.m_array);
      ArrayCopy(this.m_array,row.m_array);
      m_version++;
     }
   //+------------------------------------------------------------------+
   //| Sorts the elements in a one-dimensional array.                   |
   //+------------------------------------------------------------------+   
   void Sort()
     {
      int empty[];
      Array::Sort(m_array,empty);
     }
   //+------------------------------------------------------------------+
   //| Sorts the elements in a one-dimensional array.                   |
   //+------------------------------------------------------------------+   
   void Sort(IComparer<T>*comparer)
     {
      int empty[];
      Array::Sort(m_array,empty,comparer);
     }
   //+------------------------------------------------------------------+
   //| Sorts the elements in a one-dimensional array.                   |
   //+------------------------------------------------------------------+   
   template<typename TItem>
   void Sort(CRow<TItem>*items,IComparer<T>*comparer=NULL)
     {
      TItem arr[];
      if(items!=NULL)
        {
         CRow<TItem>::Copy(arr,items);
        }
      Array::Sort(this.m_array,arr,comparer);
      if(items!=NULL)
        {
         CRow<TItem>::Copy(items,arr);
        }
     }
   //+------------------------------------------------------------------+
   //| Sorts the elements in a one-dimensional array.                   |
   //+------------------------------------------------------------------+   
   template<typename TItem>
   void Sort(CRow<TItem>*items,const int index,const int length,IComparer<T>*comparer=NULL)
     {
      TItem arr[];
      if(items!=NULL)
        {
         CRow<TItem>::Copy(arr,items);
        }
      Array::Sort(this.m_array,arr,index,length,comparer);
      if(items!=NULL)
        {
         CRow<TItem>::Copy(items,arr);
        }
     }
   //+------------------------------------------------------------------+
   //| Determines whether an element is in the CRow<T>.                 |
   //+------------------------------------------------------------------+
   bool Contains(T value,const int index=0)
     {
      for(int i=index; i<ArraySize(m_array); i++)
        {
         if(m_array[i]==value)
           {
            return (true);
           }
        }
      return (false);
     }
   //+------------------------------------------------------------------+
   //| Creates a shallow copy of the CRow<T>.                           |
   //+------------------------------------------------------------------+     
   CRow<T>*Clone()
     {
      CRow<T>*clone=new CRow<T>(this.Size());
      for(int i=0; i<ArraySize(m_array); i++)
        {
         clone.Set(i,this[i]);
        }
      clone.m_version=this.m_version;
      return (clone);
     }
   //+------------------------------------------------------------------+
   //| Copy.                                                            |
   //+------------------------------------------------------------------+
   static void Copy(CRow<T>&dst_array,const CRow<T>&src_array,int dst_start=0,int src_start=0,int count=WHOLE_ARRAY)
     {
      ArrayCopy(dst_array.m_array,src_array.m_array,dst_start,src_start,count);
      dst_array.m_version++;
     }
   //+------------------------------------------------------------------+
   //| Copy.                                                            |
   //+------------------------------------------------------------------+  
   static void Copy(T &dst_array[],const CRow<T>&src_array,int dst_start=0,int src_start=0,int count=WHOLE_ARRAY)
     {
      ArrayCopy(dst_array,src_array.m_array,dst_start,src_start,count);
     }
   //+------------------------------------------------------------------+
   //| Copy.                                                            |
   //+------------------------------------------------------------------+
   static void Copy(CRow<T>&dst_array,T &src_array[],int dst_start=0,int src_start=0,int count=WHOLE_ARRAY)
     {
      ArrayCopy(dst_array.m_array,src_array,dst_start,src_start,count);
      dst_array.m_version++;
     }
   //+------------------------------------------------------------------+
   //| Gets the type as string of the current instance.                 |
   //+------------------------------------------------------------------+
   string GetType(void)
     {
      return (typename(T));
     }
   //+------------------------------------------------------------------+
   //| Returns an enumerator for this row with the given permission for |
   //| removal of elements.                                             |
   //+------------------------------------------------------------------+   
   CRowEnumerator<T>*GetEnumerator()
     {
      return new CRowEnumerator<T>(GetPointer(this));
     }
   //+------------------------------------------------------------------+
   //| Clear row.                                                       |
   //+------------------------------------------------------------------+  
   void Clear(const int index=0,int length=WHOLE_ARRAY)
     {
      if(length==WHOLE_ARRAY)
        {
         length=ArraySize(m_array);
        }
      for(int i=index; i<length; i++)
        {
         m_array[i]=(T)NULL;
        }
     }
   //+------------------------------------------------------------------+
   //| Reverses the sequence of the elements.                           |
   //+------------------------------------------------------------------+
   void Reverse()
     {
      int i = 0;
      int j = ArraySize(m_array) - 1;
      while(i<j)
        {
         T temp=m_array[i];
         m_array[i] = m_array[j];
         m_array[j] = temp;
         i++;
         j--;
        }
     }
   //+------------------------------------------------------------------+
   //| Searches for the specified object and returns the index of its   |
   //| first occurrence in a one-dimensional array.                     |
   //+------------------------------------------------------------------+
   int IndexOf(T value)
     {
      return Array::IndexOf(m_array,value,0,ArraySize(m_array));
     }
   //+------------------------------------------------------------------+
   //| Searches for the specified object and returns the index of its   |
   //| first occurrence in a one-dimensional array.                     |
   //+------------------------------------------------------------------+
   int IndexOf(T value,const int startIndex,const int count)
     {
      return Array::IndexOf(m_array,value,startIndex,count);
     }
  };
//+------------------------------------------------------------------+
//| Class Enumerator for CRow<T>.                                    |
//+------------------------------------------------------------------+ 
template<typename T>
class CRowEnumerator : public IEnumerator<T>
  {
private:
   CRow<T>*m_row;
   T                 m_current;
   int               m_index;
   int               m_version;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CRowEnumerator(CRow<T>*row)
     {
      this.m_row=row;
      m_index=0;
      m_current=(T)NULL;
      m_version=row.Version();
     }
   //+------------------------------------------------------------------+
   //| Advances the enumerator to the next element of the collection.   |
   //+------------------------------------------------------------------+
   bool MoveNext()
     {
      if(m_version==m_row.Version() && (uint)m_index<(uint)m_row.Size())
        {
         m_current=m_row[m_index];
         m_index++;
         //--- return rsult
         return (true);
        }
      //--- return result
      return MoveNextRare();
     }
   //+------------------------------------------------------------------+
   //| Advances the enumerator to emty element of the collection.       |
   //+------------------------------------------------------------------+
   bool MoveNextRare()
     {
      if(m_version!=m_row.Version())
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (false);
        }
      m_index=m_row.Size()+1;
      m_current=(T)NULL;
      return (false);
     }
   //+------------------------------------------------------------------+
   //| Gets current element.                                            |
   //+------------------------------------------------------------------+
   T Current()
     {
      return (m_current);
     }
   //+------------------------------------------------------------------+
   //| Reset.                                                           |
   //+------------------------------------------------------------------+  
   void Reset()
     {
      if(m_version!=m_row.Version())
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      m_index=0;
     }
  };
//+------------------------------------------------------------------+
//| Class "CMatrix"                                                  |
//+------------------------------------------------------------------+
template<typename T>
class CMatrix : public Array
  {
private:
   CRow<T>m_rows[];
public:
   //+------------------------------------------------------------------+
   //| Constructor with parametrs                                       |
   //+------------------------------------------------------------------+
                     CMatrix(const int n,const int m)
     {
      ArrayResize(m_rows,n);
      for(int i=0;i<n;i++)
        {
         m_rows[i].Resize(m);
        }
     }
   //+------------------------------------------------------------------+
   //| Constructor with parametrs                                       |
   //+------------------------------------------------------------------+
                     CMatrix(const int n)
     {
      ArrayResize(m_rows,n);
     }
   //+------------------------------------------------------------------+
   //| Constructor without parametrs                                    |
   //+------------------------------------------------------------------+
                     CMatrix()
     {
     }
   //+------------------------------------------------------------------+
   //| Operator (=).                                                    |
   //+------------------------------------------------------------------+
   void operator=(CMatrix<T>&matr)
     {
      ArrayFree(this.m_rows);
      ArrayResize(this.m_rows,matr.Size());
      for(int i=0; i<matr.Size(); i++)
        {
         this[i]=matr[i];
        }
     }
   //+------------------------------------------------------------------+
   //| Matrix size.                                                     |
   //+------------------------------------------------------------------+
   int Size(void)
     {
      return(ArraySize(m_rows));
     }
   //+------------------------------------------------------------------+
   //| Matrix length.                                                   |
   //+------------------------------------------------------------------+
   int Length(void)
     {
      return(ArraySize(m_rows)*m_rows[0].Size());
     }
   //+------------------------------------------------------------------+
   //| Resize.                                                          |
   //+------------------------------------------------------------------+ 
   void Resize(const int n)
     {
      ArrayResize(m_rows,n);
     }
   //+------------------------------------------------------------------+
   //| Resize.                                                          |
   //+------------------------------------------------------------------+   
   void Resize(const int n,const int m)
     {
      //--- check
      if(n<0 || m<0)
        {
         return;
        }
      //--- change sizes
      ArrayResize(m_rows,n);
      for(int i=0;i<n;i++)
        {
         m_rows[i].Resize(m);
        }
     }
   //+------------------------------------------------------------------+
   //| Operator ([]).                                                   |
   //+------------------------------------------------------------------+     
   CRow<T>*operator[](const int i) const
     {
      return(GetPointer(m_rows[i]));
     }
   //+------------------------------------------------------------------+
   //| Creates a shallow copy of the CMatrix<T>.                        |
   //+------------------------------------------------------------------+     
   CMatrix<T>*Clone()
     {
      CMatrix<T>*clone=new CMatrix<T>(this.Size());
      for(int i=0; i<this.Size(); i++)
        {
         clone[i]=this[i];
        }
      return (clone);
     }
   //+------------------------------------------------------------------+
   //| Gets the type as string of the current instance.                 |
   //+------------------------------------------------------------------+
   virtual string GetType(void)
     {
      return (typename(T));
     }
   //+------------------------------------------------------------------+
   //| Clear matrix.                                                    |
   //+------------------------------------------------------------------+  
   void Clear(const int index=0,int length=WHOLE_ARRAY)
     {
      if(length==WHOLE_ARRAY)
        {
         length=ArraySize(m_rows);
        }
      for(int i=index; i<length; i++)
        {
         m_rows[i].Clear();
        }
     }
  };
//+------------------------------------------------------------------+
