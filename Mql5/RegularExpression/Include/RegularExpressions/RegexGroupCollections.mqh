//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
class CGroupCollection;
class CMatch;
class CRegexReplacement;
#include <Internal\Generic\Dictionary.mqh>
#include "RegexGroup.mqh"
#include "RegexOptions.mqh"
#include "Regex.mqh"
#include "RegexMatch.mqh"
//+------------------------------------------------------------------+
//| Purpose: Represents a collection of CGroup objects.              |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| The CGroupCollection lists the captured CCapture numbers contained|
//| in a compiled Regex.                                             |
//|                                                                  |
//| Represents a sequence of capture substrings. The object is used  |
//| to return the set of captures done by a single capturing group.  |
//+------------------------------------------------------------------+
class CGroupCollection : public ICollection<CGroup*>
  {
private:
   CMatch            *m_match;
   Dictionary<int,int>*m_captureMap;
   CGroup            *m_groups[];      // cache of CGroup objects fed to the user
public:
   //+------------------------------------------------------------------+
   //| Constructor with prameters.                                      |
   //+------------------------------------------------------------------+
                     CGroupCollection(CMatch *match,Dictionary<int,int>*caps)
     {
      m_match=match;
      m_captureMap=caps;
     }
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~CGroupCollection()
     {
      for(int i=0; i<ArraySize(m_groups); i++)
        {
         delete m_groups[i];
        }
     }
   //+------------------------------------------------------------------+
   //| Returns the number of groups.                                    |
   //+------------------------------------------------------------------+
   int Count()
     {
      int matchcount[];
      m_match.GetMatchCount(matchcount);
      return (ArraySize(matchcount));
     }
   //+------------------------------------------------------------------+
   //| Enable access to a member of the collection by integer index.    |
   //+------------------------------------------------------------------+
   CGroup *operator[](const int groupnum)
     {
      return GetGroup(groupnum);
     }
   //+------------------------------------------------------------------+
   //| Enable access to a member of the collection by string index.     |
   //+------------------------------------------------------------------+
   CGroup *operator[](const string groupname)
     {
      if(m_match.Regex()==NULL)
        {
         return CGroup::Empty();
        }
      return GetGroup(m_match.Regex().GroupNumberFromName(groupname));
     }
   //+------------------------------------------------------------------+
   //| GetGroup.                                                        |
   //+------------------------------------------------------------------+
   CGroup *GetGroup(const int groupnum)
     {
      if(m_captureMap!=NULL)
        {
         return GetGroupImpl(m_captureMap[groupnum]);
        }
      else
        {
         int matchcount[];
         m_match.GetMatchCount(matchcount);
         if(groupnum>=ArraySize(matchcount) || groupnum<0)
           {
            return CGroup::Empty();
           }
         return GetGroupImpl(groupnum);
        }
     }
   //+------------------------------------------------------------------+
   //| Caches the group objects.                                        |
   //+------------------------------------------------------------------+
   CGroup *GetGroupImpl(const int groupnum)
     {
      if(groupnum==0)
        {
         //--- return 
         return (m_match);
        }
      //--- Construct all the CGroup objects the first time GetGroup is called
      if(ArraySize(m_groups)==0)
        {
         int matchcount[];
         m_match.GetMatchCount(matchcount);
         ArrayResize(m_groups,ArraySize(matchcount)-1);
         for(int i=0; i<ArraySize(m_groups); i++)
           {
            int match[];
            m_match.GetMatch(match,i+1);
            m_groups[i]=new CGroup(m_match.GetOriginalString(),match,matchcount[i+1]);
           }
        }
      //--- return 
      return (m_groups[groupnum - 1]);
     }
   //+------------------------------------------------------------------+
   //| Copies all the elements of the collection to the given array     |
   //| beginning at the given index.                                    |
   //+------------------------------------------------------------------+
   void CopyTo(CGroup *&array[],const int arrayIndex)
     {
      ArrayFree(array);
      ArrayResize(array,arrayIndex+Count());
      for(int i=arrayIndex,j=0; j<Count(); i++,j++)
        {
         array[i]=this[j];
        }
     }
   //+------------------------------------------------------------------+
   //| Provides an enumerator that iterates through the collection.     |
   //+------------------------------------------------------------------+
   IEnumerator<CGroup*>*GetEnumerator()
     {
      return new GroupEnumerator(GetPointer(this));
     }
  };
//+------------------------------------------------------------------+
//| Purpose: Enumerator lists all the captures.                      |
//+------------------------------------------------------------------+
class GroupEnumerator : public IEnumerator<CGroup*>
  {
private:
   CGroupCollection *m_rgc;
   int               m_curindex;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     GroupEnumerator(CGroupCollection *rgc)
     {
      m_curindex=-1;
      m_rgc=rgc;
     }
   //+------------------------------------------------------------------+
   //| Advances the enumerator to the next element of the collection.   |
   //+------------------------------------------------------------------+
   bool MoveNext()
     {
      int size=m_rgc.Count();
      if(m_curindex>=size)
        {
         return (false);
        }
      m_curindex++;
      return(m_curindex < size);
     }
   //+------------------------------------------------------------------+
   //| Gets current element.                                            |
   //+------------------------------------------------------------------+
   CGroup *Current()
     {
      return (dynamic_cast<CGroup*>(this.Capture()));
     }
   //+------------------------------------------------------------------+
   //| Returns the current capture.                                     |
   //+------------------------------------------------------------------+
   CCapture *Capture()
     {
      if(m_curindex<0 || m_curindex>=m_rgc.Count())
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return (m_rgc[m_curindex]);
     }
   //+------------------------------------------------------------------+
   //| Reset to before the first item.                                  |
   //+------------------------------------------------------------------+  
   void Reset()
     {
      m_curindex=-1;
     }
  };
//+------------------------------------------------------------------+
