//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
class CRegexReplacement;
#include "RegexOptions.mqh"
#include "RegexNode.mqh"
#include "RegexTree.mqh"
#include "RegexReplacement.mqh"
//+------------------------------------------------------------------+
//| Purpose: This CRegexParser class is internal to the Regex        |
//| package. It builds a tree of RegexNodes from a regular           |
//| expression.                                                      |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| It would be nice to get rid of the comment modes, since the      |
//| ScanBlank() calls are just kind of duct-taped in.                |
//+------------------------------------------------------------------+
class CRegexParser
  {
private:
   static int        s_id;
   CRegexNode       *m_stack;
   CRegexNode       *m_group;
   CRegexNode       *m_alternation;
   CRegexNode       *m_concatenation;
   CRegexNode       *m_unit;
   string            m_pattern;
   int               m_currentPos;
   int               m_autocap;
   int               m_capcount;
   int               m_captop;
   int               m_capsize;
   int               m_capnumlist[];
   RegexOptions      m_options;
   bool              m_ignoreNextParen;
   List<string>*m_capnamelist;
   List<RegexOptions>*m_optionsStack;
   Dictionary<int,int>*m_caps;
   Dictionary<string,int>*m_capnames;
public:
   static const int  MaxValueDiv10;
   static const int  MaxValueMod10;
   //+------------------------------------------------------------------+
   //| This static call constructs a CRegexTree from a regular          |
   //| expression pattern string and an option string.                  |
   //|                                                                  |
   //| The method creates, drives, and drops a parser instance.         |
   //+------------------------------------------------------------------+
   static CRegexTree *Parse(string re,RegexOptions op)
     {
      CRegexParser *p=new CRegexParser();
      CRegexNode *root;
      string capnamelist[];
      p.m_options=op;
      p.SetPattern(re);
      p.CountCaptures();
      p.Reset(op);
      root=p.ScanRegex();
      if(p.m_capnamelist!=NULL)
        {
         p.m_capnamelist.CopyTo(capnamelist);
        }
      CRegexTree *tree=new CRegexTree(root,p.m_caps,p.m_capnumlist,p.m_captop,p.m_capnames,capnamelist,op);
      delete p;
      //--- return result
      return (tree);
     }
   //+------------------------------------------------------------------+
   //| This static call constructs a flat concatenation node given a    |
   //| replacement pattern.                                             |
   //+------------------------------------------------------------------+
   static CRegexReplacement *ParseReplacement(const string rep,Dictionary<int,int>*caps,const int capsize,Dictionary<string,int>*capnames,RegexOptions op)
     {
      CRegexParser p();
      CRegexNode *root;
      p.m_options=op;
      p.NoteCaptures(caps, capsize, capnames);
      p.SetPattern(rep);
      root=p.ScanReplacement();
      CRegexReplacement *result=new CRegexReplacement(rep,root,caps);
      delete root;
      //--- return result
      return (result);
     }
   //+------------------------------------------------------------------+
   //| Escapes all metacharacters (including |,(,),[,{,|,^,$,*,+,?,\,   |
   //| spaces and #).                                                   |
   //+------------------------------------------------------------------+
   static string Escape(const string in)
     {
      for(int i=0; i<StringLen(in); i++)
        {
         if(IsMetachar(StringGetCharacter(in,i)))
           {
            string text;
            ushort ch=StringGetCharacter(in,i);
            int lastpos;
            #ifdef __MQL4__
            if(i>0)
               text+=StringSubstr(in,0,i);
            #else
               text+=StringSubstr(in,0,i);
            #endif 
            do
              {
               text+=ShortToString('\\');
               switch(ch)
                 {
                  case '\n':
                    {
                     ch='n';
                     break;
                    }
                  case '\r':
                    {
                     ch='r';
                     break;
                    }
                  case '\t':
                    {
                     ch='t';
                     break;
                    }
                  case '\x000C':
                    {
                     ch='f';
                     break;
                    }
                 }
               text+=ShortToString(ch);
               i++;
               lastpos=i;
               while(i<StringLen(in))
                 {
                  ch=StringGetCharacter(in,i);
                  if(IsMetachar(ch))
                    {
                     break;
                    }
                  i++;
                 }
               text+=StringSubstr(in,lastpos,i-lastpos);
              }
            while(i<StringLen(in));
            //--- return 
            return (text);
           }
        }
      //--- return 
      return (in);
     }
   //+------------------------------------------------------------------+
   //| Escapes all metacharacters (including (,),[,],{,},|,^,$,*,+,?,\, |
   //| spaces and #).                                                   |
   //+------------------------------------------------------------------+
   static string Unescape(const string in)
     {
      for(int i=0; i<StringLen(in); i++)
        {
         if(StringGetCharacter(in,i)=='\\')
           {
            string text;
            CRegexParser *p=new CRegexParser();
            int lastpos;
            p.SetPattern(in);
            text+=StringSubstr(in,0,i);
            do
              {
               i++;
               p.Textto(i);
               if(i<StringLen(in))
                 {
                  text+=ShortToString(p.ScanCharEscape());
                 }
               i=p.Textpos();
               lastpos=i;
               while(i<StringLen(in) && StringGetCharacter(in,i)!='\\')
                 {
                  i++;
                 }
               text+=StringSubstr(in,lastpos,i-lastpos);
              }
            while(i<StringLen(in));
            delete p;
            //--- return 
            return (text);
           }
        }
      //--- return 
      return (in);
     }
private:
   //--- Constructors:
   //+------------------------------------------------------------------+
   //| Private constructor without parameters.                          |
   //+------------------------------------------------------------------+
                     CRegexParser() :  m_ignoreNextParen(false), m_currentPos(0), m_autocap(0), m_capcount(0), m_captop(0), m_capsize(0)
     {
      s_id++;
      m_optionsStack=new List<RegexOptions>();
      m_caps=new Dictionary<int,int>();
     }
   //--- Destructors:     
   //+------------------------------------------------------------------+
   //| Destructors without parameters.                                  |
   //+------------------------------------------------------------------+
                    ~CRegexParser()
     {
      s_id++;
      if(CheckPointer(m_optionsStack)==POINTER_DYNAMIC)
        {
         delete m_optionsStack;
        }
      if(CheckPointer(m_caps)==POINTER_DYNAMIC)
        {
         delete m_caps;
        }
      if(CheckPointer(m_capnamelist)==POINTER_DYNAMIC)
        {
         delete m_capnamelist;
        }
     }
public:
   //--- Methods:
   //+------------------------------------------------------------------+
   //| Get the current id.                                              |
   //+------------------------------------------------------------------+
   static int CurrentID()
     {
      return (s_id);
     }
   //+------------------------------------------------------------------+
   //| Drops a string into the pattern buffer.                          |
   //+------------------------------------------------------------------+
   void SetPattern(string Re)
     {
      m_pattern=Re;
      m_currentPos=0;
     }
   //+------------------------------------------------------------------+
   //| Resets parsing to the beginning of the pattern.                  |
   //+------------------------------------------------------------------+
   void Reset(RegexOptions topopts)
     {
      m_currentPos=0;
      m_autocap=1;
      m_ignoreNextParen=false;
      if(m_optionsStack.Count()>0)
        {
         m_optionsStack.RemoveRange(0,m_optionsStack.Count()-1);
        }
      m_options=topopts;
      m_stack=NULL;
     }
   //+------------------------------------------------------------------+
   //| The main parsing function.                                       |
   //+------------------------------------------------------------------+
   CRegexNode *ScanRegex()
     {
      ushort ch=(ushort)'@'; // nonspecial ch, means at beginning
      bool isQuantifier=false;
      StartGroup(new CRegexNode(Capture_Type,m_options,0,-1));
      while(CharsRight()>0)
        {
         bool BreakOuterScan=false;
         bool wasPrevQuantifier=isQuantifier;
         isQuantifier=false;
         ScanBlank();
         int startpos=Textpos();
         //--- Move past all of the normal characters.  We'll stop when we hit some kind of control character, 
         //--- or if IgnorePatternWhiteSpace is on, we'll stop when we see some whitespace. 
         if(UseOptionX())
           {
            while(CharsRight()>0 && (!IsStopperX(ch=RightChar()) || (ch=='{' && !IsTrueQuantifier())))
              {
               MoveRight();
              }
           }
         else
         while(CharsRight()>0 && (!IsSpecial(ch=RightChar()) || (ch=='{' && !IsTrueQuantifier())))
           {
            MoveRight();
           }
         int endpos=Textpos();
         ScanBlank();
         if(CharsRight()==0)
           {
            ch='!'; // nonspecial, means at end
           }
         else if(IsSpecial(ch=RightChar()))
           {
            isQuantifier=IsQuantifier(ch);
            MoveRight();
           }
         else
           {
            ch=' '; // nonspecial, means at ordinary char
           }
         if(startpos<endpos)
           {
            int cchUnquantified=endpos-startpos -(isQuantifier ? 1 : 0);
            wasPrevQuantifier=false;
            if(cchUnquantified>0)
              {
               AddConcatenate(startpos,cchUnquantified,false);
              }
            if(isQuantifier)
              {
               AddUnitOne(CharAt(endpos-1));
              }
           }
         switch(ch)
           {
            case '!':
              {
               BreakOuterScan=true;
               break;
              }
            case ' ':
              {
               continue;
              }
            case '[':
              {
               CRegexCharClass *rcc=ScanCharClass(UseOptionI());
               AddUnitSet(rcc.ToStringClass());
               delete rcc;
               break;
              }
            case '(':
              {
               CRegexNode *grouper;
               PushOptions();
               grouper=ScanGroupOpen();
               if(NULL==grouper)
                 {
                  PopKeepOptions();
                 }
               else
                 {
                  PushGroup();
                  StartGroup(grouper);
                 }
               continue;
              }
            case '|':
              {
               AddAlternate();
               continue;
              }
            case ')':
              {
               if(EmptyStack())
                 {
                  Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                  //--- return NULL
                  return (NULL);
                 }
               AddGroup();
               PopGroup();
               PopOptions();
               if(Unit()==NULL)
                 {
                  continue;
                 }
               break;
              }
            case '\\':
              {
               AddUnitNode(ScanBackslash());
               break;
              }
            case '^':
              {
               AddUnitType(UseOptionM() ? Bol : Beginning);
               break;
              }
            case '$':
              {
               AddUnitType(UseOptionM() ? Eol : EndZ);
               break;
              }
            case '.':
              {
               if(UseOptionS())
                 {
                  AddUnitSet(CRegexCharClass::AnyClass);
                 }
               else
                 {
                  AddUnitNotone('\n');
                 }
               break;
              }
            case '{':
            case '*':
            case '+':
            case '?':
              {
               if(Unit()==NULL)
                 {
                  Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                  //--- return NULL
                  return (NULL);
                 }
               MoveLeft();
               break;
              }
            default:
              {
               Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
               //--- return NULL
               return (NULL);
              }
           }
         if(BreakOuterScan)
           {
            break;
           }
         ScanBlank();
         if(CharsRight()==0 || !(isQuantifier=IsTrueQuantifier()))
           {
            AddConcatenate();
            continue;
           }
         ch=MoveRightGetChar();
         //--- Handle quantifiers
         while(Unit()!=NULL)
           {
            int min;
            int max;
            bool lazy;
            switch(ch)
              {
               case '*':
                 {
                  min = 0;
                  max = Int32::MaxValue;
                  break;
                 }
               case '?':
                 {
                  min = 0;
                  max = 1;
                  break;
                 }
               case '+':
                 {
                  min = 1;
                  max = Int32::MaxValue;
                  break;
                 }
               case '{':
                 {
                  startpos=Textpos();
                  max=min=ScanDecimal();
                  if(startpos<Textpos())
                    {
                     if(CharsRight()>0 && RightChar()==',')
                       {
                        MoveRight();
                        if(CharsRight()==0 || RightChar()=='}')
                          {
                           max=Int32::MaxValue;
                          }
                        else
                          {
                           max=ScanDecimal();
                          }
                       }
                    }
                  if(startpos==Textpos() || CharsRight()==0 || MoveRightGetChar()!='}')
                    {
                     AddConcatenate();
                     Textto(startpos-1);
                     continue;
                    }
                  break;
                 }
               default:
                 {
                  Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                  //--- return NULL
                  return (NULL);
                 }
              }
            ScanBlank();
            if(CharsRight()==0 || RightChar()!='?')
              {
               lazy=false;
              }
            else
              {
               MoveRight();
               lazy=true;
              }
            if(min>max)
              {
               Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
               //--- return NULL
               return (NULL);
              }
            AddConcatenate(lazy,min,max);
           }
        }
      if(!EmptyStack())
        {
         Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return NULL
         return (NULL);
        }
      AddGroup();
      //--- return 
      return Unit();
     }
   //+------------------------------------------------------------------+
   //| Simple parsing for replacement patterns.                         |
   //+------------------------------------------------------------------+
   CRegexNode *ScanReplacement()
     {
      int c;
      int startpos;
      m_concatenation=new CRegexNode(Concatenate_Type,m_options);
      for(;;)
        {
         c=CharsRight();
         if(c==0)
           {
            break;
           }
         startpos=Textpos();
         while(c>0 && RightChar()!='$')
           {
            MoveRight();
            c--;
           }
         AddConcatenate(startpos,Textpos()-startpos,true);
         if(c>0)
           {
            if(MoveRightGetChar()=='$')
              {
               AddUnitNode(ScanDollar());
              }
            AddConcatenate();
           }
        }
      return (m_concatenation);
     }
   //+------------------------------------------------------------------+
   //| Scans contents of [] (not including []'s), and converts to a     |
   //| CRegexCharClass.                                                 |
   //+------------------------------------------------------------------+
   CRegexCharClass *ScanCharClass(const bool caseInsensitive)
     {
      return ScanCharClass(caseInsensitive, false);
     }
   //+------------------------------------------------------------------+
   //| Scans contents of [] (not including []'s), and converts to a     |
   //| CRegexCharClass.                                                 |
   //+------------------------------------------------------------------+
   CRegexCharClass *ScanCharClass(const bool caseInsensitive,const bool scanOnly)
     {
      ushort ch=(ushort)'\0';
      ushort chPrev=(ushort)'\0';
      bool inRange=false;
      bool firstChar=true;
      bool closed=false;
      CRegexCharClass *cc;
      cc=scanOnly ? NULL : new CRegexCharClass();
      if(CharsRight()>0 && RightChar()=='^')
        {
         MoveRight();
         if(!scanOnly)
           {
            cc.Negate(true);
           }
        }
      for(; CharsRight()>0; firstChar=false)
        {
         bool fTranslatedChar=false;
         ch=MoveRightGetChar();
         if(ch==']')
           {
            if(!firstChar)
              {
               closed=true;
               break;
              }
           }
         else if(ch=='\\' && CharsRight()>0)
           {
            switch(ch=MoveRightGetChar())
              {
               case 'D':
               case 'd':
                 {
                  if(!scanOnly)
                    {
                     if(inRange)
                       {
                        Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                        //--- return NULL
                        return (NULL);
                       }
                     cc.AddDigit(UseOptionE(),ch=='D',m_pattern);
                    }
                  continue;
                 }
               case 'S':
               case 's':
                 {
                  if(!scanOnly)
                    {
                     if(inRange)
                       {
                        Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                        //--- return NULL
                        return (NULL);
                       }
                     cc.AddSpace(UseOptionE(),ch=='S');
                    }
                  continue;
                 }
               case 'W':
               case 'w':
                 {
                  if(!scanOnly)
                    {
                     if(inRange)
                       {
                        Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                        //--- return NULL
                        return (NULL);
                       }
                     cc.AddWord(UseOptionE(),ch=='W');
                    }
                  continue;
                 }
               case 'p':
               case 'P':
                 {
                  if(!scanOnly)
                    {
                     if(inRange)
                       {
                        Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                        //--- return NULL
                        return (NULL);
                       }
                     cc.AddCategoryFromName(ParseProperty(),(ch!='p'),caseInsensitive,m_pattern);
                    }
                  else
                    {
                     ParseProperty();
                    }
                  continue;
                 }
               case '-':
                 {
                  if(!scanOnly)
                    {
                     cc.AddRange(ch,ch);
                    }
                  continue;
                 }
               default:
                 {
                  MoveLeft();
                  ch=ScanCharEscape(); // non-literal character
                  fTranslatedChar=true;
                  break; // this break will only break out of the switch
                 }
              }
           }
         else if(ch=='[')
           {
            //--- This is code for Posix style properties - [:Ll:] or [:IsTibetan:].
            //--- It currently doesn't do anything other than skip the whole thing!
            if(CharsRight()>0 && RightChar()==':' && !inRange)
              {
               string name;
               int savePos=Textpos();
               MoveRight();
               name=ScanCapname();
               if(CharsRight()<2 || MoveRightGetChar()!=':' || MoveRightGetChar()!=']')
                 {
                  Textto(savePos);
                 }
              }
           }
         if(inRange)
           {
            inRange=false;
            if(!scanOnly)
              {
               if(ch=='[' && !fTranslatedChar && !firstChar)
                 {
                  //--- We thought we were in a range, but we're actually starting a subtraction. 
                  //--- In that case, we'll add chPrev to our char class, skip the opening [, and
                  //--- scan the new character class recursively. 
                  cc.AddChar(chPrev);
                  cc.AddSubtraction(ScanCharClass(caseInsensitive, false));
                  if(CharsRight()>0 && RightChar()!=']')
                    {
                     Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                     //--- return NULL
                     return (NULL);
                    }
                 }
               else
                 {
                  //--- a regular range, like a-z
                  if(chPrev>ch)
                    {
                     Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                     //--- return NULL
                     return (NULL);
                    }
                  cc.AddRange(chPrev,ch);
                 }
              }
           }
         else if(CharsRight()>=2 && RightChar()=='-' && RightChar(1)!=']')
           {
            //--- this could be the start of a range
            chPrev=ch;
            inRange=true;
            MoveRight();
           }
         else if(CharsRight()>=1 && ch=='-' && !fTranslatedChar && RightChar()=='[' && !firstChar)
           {
            // we aren't in a range, and now there is a subtraction.  Usually this happens
            // only when a subtraction follows a range, like [a-z-[b]]
            if(!scanOnly)
              {
               MoveRight(1);
               cc.AddSubtraction(ScanCharClass(caseInsensitive,false));
               if(CharsRight()>0 && RightChar()!=']')
                 {
                  Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                  //--- return NULL
                  return (NULL);
                 }
              }
            else
              {
               MoveRight(1);
               ScanCharClass(caseInsensitive,true);
              }
           }
         else
           {
            if(!scanOnly)
              {
               cc.AddRange(ch,ch);
              }
           }
        }
      if(!closed)
        {
         Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return NULL
         return (NULL);
        }
      if(!scanOnly && caseInsensitive)
        {
         cc.AddLowercase();
        }
      //--- return 
      return cc;
     }
   //+------------------------------------------------------------------+
   //| Scans chars following a '(' (not counting the '('), and returns  |
   //| a CRegexNode for the type of group scanned, or NULL if the group |
   //| simply changed options (?cimsx-cimsx) or was a comment (#...).   |
   //+------------------------------------------------------------------+
   CRegexNode *ScanGroupOpen()
     {
      ushort ch=(ushort)'\0';
      int NodeType=0;
      char close='>';
      //--- just return a CRegexNode if we have:
      //--- 1. "(" followed by nothing
      //--- 2. "(x" where x != ?
      //--- 3. "(?)"
      if(CharsRight()==0 || RightChar()!='?' || (RightChar()=='?' && (CharsRight()>1 && RightChar(1)==')')))
        {
         if(UseOptionN() || m_ignoreNextParen)
           {
            m_ignoreNextParen=false;
            //--- return 
            return new CRegexNode(Group_Type, m_options);
           }
         else
           {
            //--- return 
            return new CRegexNode(Capture_Type, m_options, m_autocap++, -1);
           }
        }
      MoveRight();
      for(;;)
        {
         bool BreakRecognize=false;
         if(CharsRight()==0)
           {
            break;
           }
         switch(ch=MoveRightGetChar())
           {
            case ':':
              {
               NodeType=Group_Type;
               break;
              }
            case '=':
              {
               m_options &=(RegexOptions)(~(RightToLeft));
               NodeType=Require_Type;
               break;
              }
            case '!':
              {
               m_options &=(RegexOptions)(~(RightToLeft));
               NodeType=Prevent_Type;
               break;
              }
            case '>':
              {
               NodeType=Greedy_Type;
               break;
              }
            case '\'':
              {
               close='\'';
              }
            case '<':
              {
               if(CharsRight()==0)
                 {
                  BreakRecognize=true;
                  break;
                 }
               switch(ch=MoveRightGetChar())
                 {
                  case '=':
                    {
                     if(close=='\'')
                       {
                        BreakRecognize=true;
                       }
                     m_options|=RightToLeft;
                     NodeType=Require_Type;
                     break;
                    }
                  case '!':
                    {
                     if(close=='\'')
                       {
                        BreakRecognize=true;
                       }
                     m_options|=RightToLeft;
                     NodeType=Prevent_Type;
                     break;
                    }
                  default:
                    {
                     MoveLeft();
                     int capnum=-1;
                     int uncapnum = -1;
                     bool proceed = false;
                     //--- grab part before -
                     if(ch>='0' && ch<='9')
                       {
                        capnum=ScanDecimal();
                        if(!IsCaptureSlot(capnum))
                          {
                           capnum=-1;
                          }
                        //--- check if we have bogus characters after the number
                        if(CharsRight()>0 && !(RightChar()==close || RightChar()=='-'))
                          {
                           Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                           //--- return NULL
                           return (NULL);
                          }
                        if(capnum==0)
                          {
                           Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                           //--- return NULL
                           return (NULL);
                          }
                       }
                     else if(CRegexCharClass::IsWordChar(ch))
                       {
                        string capname=ScanCapname();
                        if(IsCaptureName(capname))
                          {
                           capnum=CaptureSlotFromName(capname);
                          }
                        //--- check if we have bogus character after the name
                        if(CharsRight()>0 && !(RightChar()==close || RightChar()=='-'))
                          {
                           Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                           //--- return NULL
                           return (NULL);
                          }
                       }
                     else if(ch=='-')
                       {
                        proceed=true;
                       }
                     else
                       {
                        //--- bad group name - starts with something other than a word character and isn't a number
                        Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                        //--- return NULL
                        return (NULL);
                       }
                     //--- grab part after - if any
                     if((capnum!=-1 || proceed==true) && CharsRight()>0 && RightChar()=='-')
                       {
                        MoveRight();
                        ch=RightChar();
                        if(ch>='0' && ch<='9')
                          {
                           uncapnum=ScanDecimal();
                           if(!IsCaptureSlot(uncapnum))
                             {
                              Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                              //--- return NULL
                              return (NULL);
                             }
                           //--- check if we have bogus characters after the number
                           if(CharsRight()>0 && RightChar()!=close)
                             {
                              Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                              //--- return NULL
                              return (NULL);
                             }
                          }
                        else if(CRegexCharClass::IsWordChar(ch))
                          {
                           string uncapname=ScanCapname();
                           if(IsCaptureName(uncapname))
                             {
                              uncapnum=CaptureSlotFromName(uncapname);
                             }
                           else
                             {
                              Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                              //--- return NULL
                              return (NULL);
                             }
                           //--- check if we have bogus character after the name
                           if(CharsRight()>0 && RightChar()!=close)
                             {
                              Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                              //--- return NULL
                              return (NULL);
                             }
                          }
                        else
                          {
                           //--- bad group name - starts with something other than a word character and isn't a number
                             {
                              Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                              //--- return NULL
                              return (NULL);
                             }
                          }
                       }
                     //--- actually make the node
                     if((capnum!=-1 || uncapnum!=-1) && CharsRight()>0 && MoveRightGetChar()==close)
                       {
                        //--- return 
                        return new CRegexNode(Capture_Type, m_options, capnum, uncapnum);
                       }
                     BreakRecognize=true;
                    }
                 }
               break;
              }
            case '(':
              {
               //--- alternation construct (?(...) | )
               int parenPos=Textpos();
               if(CharsRight()>0)
                 {
                  ch=RightChar();
                  //--- check if the alternation condition is a backref
                  if(ch>='0' && ch<='9')
                    {
                     int capnum=ScanDecimal();
                     if(CharsRight()>0 && MoveRightGetChar()==')')
                       {
                        if(IsCaptureSlot(capnum))
                          {
                           //--- return 
                           return new CRegexNode(Testrefnode_Type, m_options, capnum);
                          }
                        else
                          {
                           Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                           //--- return NULL
                           return (NULL);
                          }
                       }
                     else
                       {
                        Print("Internal error! On file:'"+__FILE__+"'; in function:'"+__FUNCTION__+"'.");
                        //--- return NULL
                        return (NULL);
                       }
                    }
                  else if(CRegexCharClass::IsWordChar(ch))
                    {
                     string capname=ScanCapname();
                     if(IsCaptureName(capname) && CharsRight()>0 && MoveRightGetChar()==')')
                       {
                        //--- return 
                        return new CRegexNode(Testrefnode_Type, m_options, CaptureSlotFromName(capname));
                       }
                    }
                 }
               //--- not a backref
               NodeType=Testgroup_Type;
               Textto(parenPos-1);        // jump to the start of the parentheses
               m_ignoreNextParen=true;    // but make sure we don't try to capture the insides
               int charsRight= CharsRight();
               if(charsRight>=3 && RightChar(1)=='?')
                 {
                  ushort rightchar2=RightChar(2);
                  //--- disallow comments in the condition
                  if(rightchar2=='#')
                    {
                     Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                     //--- return NULL
                     return (NULL);
                    }
                  //--- disallow named capture group (?<..>..) in the condition
                  if(rightchar2=='\'')
                    {
                     Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                     //--- return NULL
                     return (NULL);
                    }
                  else
                    {
                     if(charsRight>=4 && (rightchar2=='<' && RightChar(3)!='!' && RightChar(3)!='='))
                       {
                        Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                        //--- return NULL
                        return (NULL);
                       }
                    }
                 }
               break;
              }
            default:
              {
               MoveLeft();
               NodeType=Group_Type;
               ScanOptions();
               if(CharsRight()==0)
                 {
                  BreakRecognize=true;
                 }
               if((ch=MoveRightGetChar())==')')
                 {
                  //--- return result
                  return (NULL);
                 }
               if(ch!=':')
                 {
                  BreakRecognize=true;
                 }
               break;
              }
           }
         if(BreakRecognize)
           {
            break;
           }
         //--- return result
         return new CRegexNode(NodeType, m_options);
        }
      Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
      //--- return NULL
      return (NULL);
     }
   //+------------------------------------------------------------------+
   //| Scans whitespace or x-mode comments.                             |
   //+------------------------------------------------------------------+
   void ScanBlank()
     {
      if(UseOptionX())
        {
         for(;;)
           {
            while(CharsRight()>0 && IsSpace(RightChar()))
              {
               MoveRight();
              }
            if(CharsRight()==0)
              {
               break;
              }
            if(RightChar()=='#')
              {
               while(CharsRight()>0 && RightChar()!='\n')
                 {
                  MoveRight();
                 }
              }
            else if(CharsRight()>=3 && RightChar(2)=='#' && RightChar(1)=='?' && RightChar()=='(')
              {
               while(CharsRight()>0 && RightChar()!=')')
                 {
                  MoveRight();
                 }
               if(CharsRight()==0)
                 {
                  Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                  //--- return 
                  return;
                 }
               MoveRight();
              }
            else
              {
               break;
              }
           }
        }
      else
        {
         for(;;)
           {
            if(CharsRight()<3 || RightChar(2)!='#' || RightChar(1)!='?' || RightChar()!='(')
              {
               //--- return 
               return;
              }
            while(CharsRight()>0 && RightChar()!=')')
              {
               MoveRight();
              }
            if(CharsRight()==0)
              {
               Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
               //--- return 
               return;
              }
            MoveRight();
           }
        }
     }
   //+------------------------------------------------------------------+
   //| Scans chars following a '\' (not counting the '\'), and returns  |
   //| a CRegexNode for the type of atom scanned.                       |
   //+------------------------------------------------------------------+
   CRegexNode *ScanBackslash()
     {
      ushort ch;
      if(CharsRight()==0)
        {
         Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return NULL
         return (NULL);
        }
      switch(ch=RightChar())
        {
         case 'b':
         case 'B':
         case 'A':
         case 'G':
         case 'Z':
         case 'z':
           {
            MoveRight();
            //--- return 
            return new CRegexNode(TypeFromCode(ch), m_options);
           }
         case 'w':
           {
            MoveRight();
            if(UseOptionE())
              {
               //--- return 
               return new CRegexNode(Set, m_options, CRegexCharClass::ECMAWordClass);
              }
            //--- return 
            return new CRegexNode(Set, m_options, CRegexCharClass::WordClass);
           }
         case 'W':
           {
            MoveRight();
            if(UseOptionE())
              {
               //--- return 
               return new CRegexNode(Set, m_options, CRegexCharClass::NotECMAWordClass);
              }
            //--- return 
            return new CRegexNode(Set, m_options, CRegexCharClass::NotWordClass);
           }
         case 's':
           {
            MoveRight();
            if(UseOptionE())
              {
               //--- return 
               return new CRegexNode(Set, m_options, CRegexCharClass::ECMASpaceClass);
              }
            //--- return 
            return new CRegexNode(Set, m_options, CRegexCharClass::SpaceClass);
           }
         case 'S':
           {
            MoveRight();
            if(UseOptionE())
              {
               //--- return 
               return new CRegexNode(Set, m_options, CRegexCharClass::NotECMASpaceClass);
              }
            //--- return
            return new CRegexNode(Set, m_options, CRegexCharClass::NotSpaceClass);
           }
         case 'd':
           {
            MoveRight();
            if(UseOptionE())
              {
               //--- return 
               return new CRegexNode(Set, m_options, CRegexCharClass::ECMADigitClass);
              }
            //--- return 
            return new CRegexNode(Set, m_options, CRegexCharClass::DigitClass);
           }
         case 'D':
           {
            MoveRight();
            if(UseOptionE())
              {
               //--- return 
               return new CRegexNode(Set, m_options, CRegexCharClass::NotECMADigitClass);
              }
            //--- return 
            return new CRegexNode(Set, m_options, CRegexCharClass::NotDigitClass);
           }
         case 'p':
         case 'P':
           {
            MoveRight();
            CRegexCharClass cc();
            cc.AddCategoryFromName(ParseProperty(),(ch!='p'),UseOptionI(),m_pattern);
            if(UseOptionI())
              {
               cc.AddLowercase();
              }
            //--- return 
            return new CRegexNode(Set, m_options, cc.ToStringClass());
           }
         default:
           {
            //--- return result
            return ScanBasicBackslash();
           }
        }
     }
   //+------------------------------------------------------------------+
   //| Scans \-style backreferences and character escapes.              |
   //+------------------------------------------------------------------+
   CRegexNode *ScanBasicBackslash()
     {
      if(CharsRight()==0)
        {
         Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return 
         return (NULL);
        }
      ushort ch;
      bool angled= false;
      char close = '\0';
      int backpos;
      backpos=Textpos();
      ch=RightChar();
      //--- allow \k<foo> instead of \<foo>, which is now deprecated
      if(ch=='k')
        {
         if(CharsRight()>=2)
           {
            MoveRight();
            ch=MoveRightGetChar();
            if(ch=='<' || ch=='\'')
              {
               angled= true;
               close =(ch == '\'') ? '\'' : '>';
              }
           }
         if(!angled || CharsRight()<=0)
           {
            Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
            //--- return 
            return (NULL);
           }
         ch=RightChar();
        }
      //--- Note angle without \g <
      else if((ch=='<' || ch=='\'') && CharsRight()>1)
        {
         angled=true;
         close=(ch=='\'') ? '\'' : '>';
         MoveRight();
         ch=RightChar();
        }
      //--- Try to parse backreference: \<1> or \<cap>
      if(angled && ch>='0' && ch<='9')
        {
         int capnum=ScanDecimal();
         if(CharsRight()>0 && MoveRightGetChar()==close)
           {
            if(IsCaptureSlot(capnum))
              {
               //--- return 
               return new CRegexNode(Ref, m_options, capnum);
              }
            else
              {
               Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
               //--- return NULL
               return (NULL);
              }
           }
        }
      //--- Try to parse backreference or octal: \1
      else if(!angled && ch>='1' && ch<='9')
        {
         if(UseOptionE())
           {
            int capnum=-1;
            int newcapnum=(int)(ch-'0');
            int pos=Textpos()-1;
            while(newcapnum<=m_captop)
              {
               if(IsCaptureSlot(newcapnum) && (m_caps==NULL || (int)m_caps[newcapnum]<pos))
                 {
                  capnum=newcapnum;
                 }
               MoveRight();
               if(CharsRight()==0 || (ch=RightChar())<'0' || ch>'9')
                 {
                  break;
                 }
               newcapnum=newcapnum*10+(int)(ch-'0');
              }
            if(capnum>=0)
              {
               //--- return 
               return new CRegexNode(Ref, m_options, capnum);
              }
           }
         else
           {
            int capnum=ScanDecimal();
            if(IsCaptureSlot(capnum))
              {
               //--- return 
               return new CRegexNode(Ref, m_options, capnum);
              }
            else if(capnum<=9)
              {
               Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
               //--- return NULL
               return (NULL);
              }
           }
        }
      else if(angled && CRegexCharClass::IsWordChar(ch))
        {
         string capname=ScanCapname();
         if(CharsRight()>0 && MoveRightGetChar()==close)
           {
            if(IsCaptureName(capname))
              {
               //--- return result
               return new CRegexNode(Ref, m_options, CaptureSlotFromName(capname));
              }
            else
              {
               Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
               //--- return NULL
               return (NULL);
              }
           }
        }
      //--- Not backreference: must be char code
      Textto(backpos);
      ch=ScanCharEscape();
      if(UseOptionI())
        {
         string s=ShortToString(ch);
         StringToLower(s);
         ch=StringGetCharacter(s,0);
        }
      //--- return 
      return new CRegexNode(One, m_options, ushort(ch));
     }
   //+------------------------------------------------------------------+
   //| Scans $ patterns recognized within replacment patterns.          |
   //+------------------------------------------------------------------+
   CRegexNode *ScanDollar()
     {
      if(CharsRight()==0)
        {
         //--- return 
         return new CRegexNode((int)One, (RegexOptions)m_options, (ushort)'$');
        }
      ushort ch=RightChar();
      bool angled;
      int backpos=Textpos();
      int lastEndPos=backpos;
      //--- Note angle
      if(ch=='{' && CharsRight()>1)
        {
         angled=true;
         MoveRight();
         ch=RightChar();
        }
      else
        {
         angled=false;
        }
      //--- Try to parse backreference: \1 or \{1} or \{cap}
      if(ch>='0' && ch<='9')
        {
         if(!angled && UseOptionE())
           {
            int capnum=-1;
            int newcapnum=(int)(ch-'0');
            MoveRight();
            if(IsCaptureSlot(newcapnum))
              {
               capnum=newcapnum;
               lastEndPos=Textpos();
              }
            while(CharsRight()>0 && (ch=RightChar())>='0' && ch<='9')
              {
               int digit=(int)(ch-'0');
               if(newcapnum>(MaxValueDiv10) || (newcapnum==(MaxValueDiv10) && digit>(MaxValueMod10)))
                 {
                  Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                  //--- return NULL
                  return (NULL);
                 }
               newcapnum=newcapnum*10+digit;
               MoveRight();
               if(IsCaptureSlot(newcapnum))
                 {
                  capnum=newcapnum;
                  lastEndPos=Textpos();
                 }
              }
            Textto(lastEndPos);
            if(capnum>=0)
              {
               //--- return 
               return new CRegexNode(Ref, m_options, capnum);
              }
           }
         else
           {
            int capnum=ScanDecimal();
            if(!angled || (CharsRight()>0 && MoveRightGetChar()=='}'))
              {
               if(IsCaptureSlot(capnum))
                 {
                  //--- return 
                  return new CRegexNode(Ref, m_options, capnum);
                 }
              }
           }
        }
      else if(angled && CRegexCharClass::IsWordChar(ch))
        {
         string capname=ScanCapname();
         if(CharsRight()>0 && MoveRightGetChar()=='}')
           {
            if(IsCaptureName(capname))
              {
               //--- return 
               return new CRegexNode(Ref, m_options, CaptureSlotFromName(capname));
              }
           }
        }
      else if(!angled)
        {
         int capnum=1;
         switch(ch)
           {
            case '$':
              {
               MoveRight();
               //--- return 
               return new CRegexNode(One, m_options, (ushort)'$');
              }
            case '&':
              {
               capnum=0;
               break;
              }
            case '`':
              {
               capnum=LeftPortion;
               break;
              }
            case '\'':
              {
               capnum=RightPortion;
               break;
              }
            case '+':
              {
               capnum=LastGroup;
               break;
              }
            case '_':
              {
               capnum=WholeString;
               break;
              }
           }
         if(capnum!=1)
           {
            MoveRight();
            //--- return 
            return new CRegexNode(Ref, m_options, capnum);
           }
        }
      //--- unrecognized $: literalize
      Textto(backpos);
      //--- return 
      return new CRegexNode(One, m_options, (ushort)'$');
     }
   //+------------------------------------------------------------------+
   //| Scans a capture name: consumes word chars.                       |
   //+------------------------------------------------------------------+
   string ScanCapname()
     {
      int startpos=Textpos();
      while(CharsRight()>0)
        {
         if(!CRegexCharClass::IsWordChar(MoveRightGetChar()))
           {
            MoveLeft();
            break;
           }
        }
      return StringSubstr(m_pattern,startpos, Textpos() - startpos);
     }
   //+------------------------------------------------------------------+
   //| Scans up to three octal digits (stops before exceeding 0377).    |
   //+------------------------------------------------------------------+
   char ScanOctal()
     {
      int d;
      int i;
      int c;
      //--- Consume octal chars only up to 3 digits and value 0377
      c=3;
      if(c>CharsRight())
        {
         c=CharsRight();
        }
      for(i=0; c>0 && (uint)(d=RightChar()-'0')<=7; c-=1)
        {
         MoveRight();
         i *= 8;
         i += d;
         if(UseOptionE() && i>=0x20)
           {
            break;
           }
        }
      //--- Octal codes only go up to 255.  Any larger and the behavior that Perl follows is simply to truncate the high bits. 
      i &=0xFF;
      return(char)i;
     }
   //+------------------------------------------------------------------+
   //| Scans any number of decimal digits (pegs value at 2^31-1 if too  |
   //| large).                                                          |
   //+------------------------------------------------------------------+
   int ScanDecimal()
     {
      int i=0;
      int d;
      while(CharsRight()>0 && (uint)(d=(char)(RightChar()-'0'))<=9)
        {
         MoveRight();
         if(i>(MaxValueDiv10) || (i==(MaxValueDiv10) && d>(MaxValueMod10)))
           {
            Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
            return (0);
           }
         i *= 10;
         i += d;
        }
      return (i);
     }
   //+------------------------------------------------------------------+
   //| Scans exactly c hex digits (c=2 for \xFF, c=4 for \xFFFF).       |
   //+------------------------------------------------------------------+
   ushort ScanHex(int c)
     {
      ushort i;
      ushort d;
      i=0;
      if(CharsRight()>=c)
        {
         for(; c>0 && ((d=HexDigit(MoveRightGetChar()))>=0); c-=1)
           {
            i *= 0x10;
            i += d;
           }
        }
      if(c>0)
        {
         Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (0);
        }
      //--- return result
      return (i);
     }
   //+------------------------------------------------------------------+
   //| Returns n <= 0xF for a hex digit.                                |
   //+------------------------------------------------------------------+
   static ushort HexDigit(const ushort ch)
     {
      short d;
      if((ushort)(d=(short)ch-(short)'0')<=9)
        {
         return (d);
        }
      if((ushort)(d=(short)ch-(short)'a')<=5)
        {
         return (d + 0xa);
        }
      if((ushort)(d=(short)ch-(short)'A')<=5)
        {
         return (d + 0xa);
        }
      return (-1);
     }
   //+------------------------------------------------------------------+
   //| Grabs and converts an ascii control character.                   |
   //+------------------------------------------------------------------+
   ushort ScanControl()
     {
      ushort ch;
      if(CharsRight()<=0)
        {
         Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return 
         return (0);
        }
      ch=MoveRightGetChar();
      //--- \ca interpreted as \cA
      if(ch>='a' && ch<='z')
        {
         ch=(char)(ch -('a'-'A'));
        }
      if((ch=(char)(ch-'@'))<' ')
        {
         //--- return 
         return (ch);
        }
      Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
      //--- return 
      return (0);
     }
   //+------------------------------------------------------------------+
   //| Returns true for options allowed only at the top level.          |
   //+------------------------------------------------------------------+
   bool IsOnlyTopOption(RegexOptions option)
     {
      return(option == RightToLeft || option == ECMAScript);
     }
   //+------------------------------------------------------------------+
   //| Scans cimsx-cimsx option string, stops at the first unrecognized |
   //| char.                                                            |
   //+------------------------------------------------------------------+
   void ScanOptions()
     {
      ushort ch;
      bool off;
      RegexOptions option;
      for(off=false; CharsRight()>0; MoveRight())
        {
         ch=RightChar();
         if(ch=='-')
           {
            off=true;
           }
         else if(ch=='+')
           {
            off=false;
           }
         else
           {
            option=OptionFromCode(ch);
            if(option==0 || IsOnlyTopOption(option))
              {
               //--- return
               return;
              }
            if(off)
              {
               m_options &=~option;
              }
            else
              {
               m_options|=option;
              }
           }
        }
     }
   //+------------------------------------------------------------------+
   //| Scans \ code for escape codes that map to single unicode chars.  |
   //+------------------------------------------------------------------+
   ushort ScanCharEscape()
     {
      ushort ch=0;
      ch=MoveRightGetChar();
      if(ch>='0' && ch<='7')
        {
         MoveLeft();
         //--- return result
         return ScanOctal();
        }
      switch(ch)
        {
         case 'x':
           {
            return ScanHex(2);
           }
         case 'u':
           {
            return ScanHex(4);
           }
         case 'a':
           {
            return ('\x0007');
           }
         case 'b':
           {
            return ('\x0008');
           }
         case 'e':
           {
            return ('\x001B');
           }
         case 'f':
           {
            return ('\x000C');
           }
         case 'n':
           {
            return ('\n');
           }
         case 'r':
           {
            return ('\r');
           }
         case 't':
           {
            return ('\t');
           }
         case 'v':
           {
            return ('\x000B');
           }
         case 'c':
           {
            return ScanControl();
           }
         default:
           {
            if(!UseOptionE() && CRegexCharClass::IsWordChar(ch))
              {
               Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
               return (NULL);
              }
            return (ch);
           }
        }
     }
   //+------------------------------------------------------------------+
   //| Scans X for \p{X} or \P{X}.                                      |
   //+------------------------------------------------------------------+
   string ParseProperty()
     {
      if(CharsRight()<3)
        {
         Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return 
         return (NULL);
        }
      ushort ch=MoveRightGetChar();
      if(ch!='{')
        {
         Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return 
         return (NULL);
        }
      int startpos=Textpos();
      while(CharsRight()>0)
        {
         ch=MoveRightGetChar();
         if(!(CRegexCharClass::IsWordChar(ch) || ch=='-'))
           {
            MoveLeft();
            break;
           }
        }
      string capname=StringSubstr(m_pattern,startpos,Textpos()-startpos);
      if(CharsRight()==0 || MoveRightGetChar()!='}')
        {
         Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return 
         return (NULL);
        }
      //--- return 
      return (capname);
     }
   //+------------------------------------------------------------------+
   //| Returns ReNode type for zero-length assertions with a \ code.    |
   //+------------------------------------------------------------------+
   int TypeFromCode(const ushort ch)
     {
      switch(ch)
        {
         case 'b':
           {
            return (UseOptionE() ? ECMABoundary : (int)Boundary);
           }
         case 'B':
           {
            return (UseOptionE() ? NonECMABoundary : (int)Nonboundary);
           }
         case 'A':
           {
            return (Beginning);
           }
         case 'G':
           {
            return (Start);
           }
         case 'Z':
           {
            return (EndZ);
           }
         case 'z':
           {
            return (End);
           }
         default:
           {
            return (Nothing);
           }
        }
     }
   //+------------------------------------------------------------------+
   //| Returns option bit from single-char (?cimsx) code.               |
   //+------------------------------------------------------------------+
   static RegexOptions OptionFromCode(ushort ch)
     {
      //--- case-insensitive
      if(ch>='A' && ch<='Z')
        {
         ch+=(char)('a'-'A');
        }
      switch(ch)
        {
         case 'i':
           {
            return (IgnoreCase);
           }
         case 'r':
           {
            return (RightToLeft);
           }
         case 'm':
           {
            return (Multiline);
           }
         case 'n':
           {
            return (ExplicitCapture);
           }
         case 's':
           {
            return (Singleline);
           }
         case 'x':
           {
            return (IgnorePatternWhitespace);
           }
#ifdef _DEBUG
         case 'd':
           {
            return (Debug);
           }
#endif
         case 'e':
           {
            return (ECMAScript);
           }
         default:
           {
            return ((RegexOptions)0);
           }
        }
     }
   //+------------------------------------------------------------------+
   //| A prescanner for deducing the slots used for captures by doing a |
   //| partial tokenization of the pattern.                             |
   //+------------------------------------------------------------------+
   void CountCaptures()
     {
      ushort ch;
      NoteCaptureSlot(0,0);
      m_autocap=1;
      while(CharsRight()>0)
        {
         int pos=Textpos();
         ch=MoveRightGetChar();
         switch(ch)
           {
            case '\\':
              {
               if(CharsRight()>0)
                 {
                  MoveRight();
                 }
               break;
              }
            case '#':
              {
               if(UseOptionX())
                 {
                  MoveLeft();
                  ScanBlank();
                 }
               break;
              }
            case '[':
              {
               ScanCharClass(false,true);
               break;
              }
            case ')':
              {
               if(!EmptyOptionsStack())
                 {
                  PopOptions();
                 }
               break;
              }
            case '(':
              {
               if(CharsRight()>=2 && RightChar(1)=='#' && RightChar()=='?')
                 {
                  MoveLeft();
                  ScanBlank();
                 }
               else
                 {
                  PushOptions();
                  if(CharsRight()>0 && RightChar()=='?')
                    {
                     //--- we have (?...
                     MoveRight();
                     if(CharsRight()>1 && (RightChar()=='<' || RightChar()=='\''))
                       {
                        //--- named group: (?<... or (?'...
                        MoveRight();
                        ch=RightChar();
                        if(ch!='0' && CRegexCharClass::IsWordChar(ch))
                          {
                           if(ch>='1' && ch<='9')
                             {
                              NoteCaptureSlot(ScanDecimal(),pos);
                             }
                           else
                             {
                              NoteCaptureName(ScanCapname(),pos);
                             }
                          }
                       }
                     else
                       {
                        //--- (?...
                        //--- get the options if it's an option construct (?cimsx-cimsx...)
                        ScanOptions();
                        if(CharsRight()>0)
                          {
                           if(RightChar()==')')
                             {
                              //--- (?cimsx-cimsx)
                              MoveRight();
                              PopKeepOptions();
                             }
                           else if(RightChar()=='(')
                             {
                              //--- alternation construct: (?(foo)yes|no)
                              //--- ignore the next paren so we don't capture the condition
                              m_ignoreNextParen=true;
                              //--- break from here so we don't reset _ignoreNextParen
                              break;
                             }
                          }
                       }
                    }
                  else
                    {
                     if(!UseOptionN() && !m_ignoreNextParen)
                       {
                        NoteCaptureSlot(m_autocap++,pos);
                       }
                    }
                 }
               m_ignoreNextParen=false;
               break;
              }
           }
        }
      AssignNameSlots();
     }
   //+------------------------------------------------------------------+
   //| Notes a used capture slot.                                       |
   //+------------------------------------------------------------------+
   void NoteCaptureSlot(const int i,const int pos)
     {
      if(!m_caps.ContainsKey(i))
        {
         //--- the rhs of the hashtable isn't used in the parser
         m_caps.Add(i,pos);
         m_capcount++;
         if(m_captop<=i)
           {
            if(i==Int32::MaxValue)
              {
               m_captop=i;
              }
            else
              {
               m_captop=i+1;
              }
           }
        }
     }
   //+------------------------------------------------------------------+
   //| Notes a used capture slot.                                       |
   //+------------------------------------------------------------------+
   void NoteCaptureName(const string name,const int pos)
     {
      if(m_capnames==NULL)
        {
         m_capnames=new Dictionary<string,int>(new StringEqualityComparer);
         m_capnamelist=new List<string>();
        }
      if(!m_capnames.ContainsKey(name))
        {
         m_capnames.Add(name,pos);
         m_capnamelist.Add(name);
        }
     }
   //+------------------------------------------------------------------+
   //| For when all the used captures are known: note them all at once. |
   //+------------------------------------------------------------------+
   void NoteCaptures(Dictionary<int,int>*caps,const int capsize,Dictionary<string,int>*capnames)
     {
      if(CheckPointer(m_caps)==POINTER_DYNAMIC)
        {
         delete m_caps;
        }
      m_caps=caps;
      m_capsize=capsize;
      if(CheckPointer(m_capnames)==POINTER_DYNAMIC)
        {
         delete m_capnames;
        }
      m_capnames=capnames;
     }
   //+------------------------------------------------------------------+
   //| Assigns unused slot numbers to the capture names.                |
   //+------------------------------------------------------------------+
   void AssignNameSlots()
     {
      if(m_capnames!=NULL)
        {
         for(int i=0; i<m_capnamelist.Count(); i++)
           {
            while(IsCaptureSlot(m_autocap))
              {
               m_autocap++;
              }
            string name=m_capnamelist[i];
            int pos=(int)m_capnames[name];
            m_capnames.Set(name,m_autocap);
            NoteCaptureSlot(m_autocap,pos);
            m_autocap++;
           }
        }
      //--- if the caps array has at least one gap, construct the list of used slots
      if(m_capcount<m_captop)
        {
         ArrayResize(m_capnumlist,m_capcount);
         int i=0;
         DictionaryEnumerator<int,int>*de=m_caps.GetEnumerator();
         while(de.MoveNext())
           {
            m_capnumlist[i++]=(int)de.Key();
           }
         delete de;
         int empty[];
         Array::Sort(m_capnumlist,empty);
        }
      //--- merge capsnumlist into capnamelist
      if(m_capnames!=NULL || ArraySize(m_capnumlist)!=NULL)
        {
         List<string>*oldcapnamelist;
         int next;
         int k=0;
         if(m_capnames==NULL)
           {
            delete oldcapnamelist=NULL;
            if(CheckPointer(m_capnames)==POINTER_DYNAMIC)
              {
               delete m_capnames;
              }
            if(CheckPointer(m_capnamelist)==POINTER_DYNAMIC)
              {
               delete m_capnamelist;
              }
            m_capnames=new Dictionary<string,int>();
            m_capnamelist=new List<string>();
            next=-1;
           }
         else
           {
            oldcapnamelist=m_capnamelist;
            m_capnamelist=new List<string>();
            next=(int)m_capnames[oldcapnamelist[0]];
           }
         for(int i=0; i<m_capcount; i++)
           {
            int j=(ArraySize(m_capnumlist)==NULL) ? i :(int)m_capnumlist[i];
            if(next==j)
              {
               m_capnamelist.Add(oldcapnamelist[k++]);
               next=(k==oldcapnamelist.Count()) ? -1 :(int)m_capnames[oldcapnamelist[k]];
              }
            else
              {
               string str=(string)j;
               m_capnamelist.Add(str);
               m_capnames.Set(str,j);
              }
           }
         if(oldcapnamelist!=NULL)
           {
            delete oldcapnamelist;
           }
        }
     }
   //+------------------------------------------------------------------+
   //| Looks up the slot number for a given name.                       |
   //+------------------------------------------------------------------+
   int CaptureSlotFromName(const string capname)
     {
      return((int)m_capnames[capname]);
     }
   //+------------------------------------------------------------------+
   //| True if the capture slot was noted.                              |
   //+------------------------------------------------------------------+
   bool IsCaptureSlot(const int i)
     {
      if(m_caps!=NULL)
        {
         return (m_caps.ContainsKey(i));
        }
      return(i >= 0 && i < m_capsize);
     }
   //+------------------------------------------------------------------+
   //| Looks up the slot number for a given name.                       |
   //+------------------------------------------------------------------+
   bool IsCaptureName(string capname)
     {
      if(m_capnames==NULL)
        {
         return (false);
        }
      return (m_capnames.ContainsKey(capname));
     }
   //+------------------------------------------------------------------+
   //| True if N option disabling '(' autocapture is on.                |
   //+------------------------------------------------------------------+
   bool UseOptionN()
     {
      return((m_options & ExplicitCapture) != 0);
     }
   //+------------------------------------------------------------------+
   //| True if I option enabling case-insensitivity is on.              |
   //+------------------------------------------------------------------+
   bool UseOptionI()
     {
      return((m_options & IgnoreCase) != 0);
     }
   //+------------------------------------------------------------------+
   //| True if M option altering meaning of $ and ^ is on.              |
   //+------------------------------------------------------------------+
   bool UseOptionM()
     {
      return((m_options & Multiline) != 0);
     }
   //+------------------------------------------------------------------+
   //| True if S option altering meaning of . is on.                    |
   //+------------------------------------------------------------------+
   bool UseOptionS()
     {
      return((m_options & Singleline) != 0);
     }
   //+------------------------------------------------------------------+
   //| True if X option enabling whitespace/comment mode is on.         |
   //+------------------------------------------------------------------+
   bool UseOptionX()
     {
      return((m_options & IgnorePatternWhitespace) != 0);
     }
   //+------------------------------------------------------------------+
   //| True if E option enabling ECMAScript behavior is on.             |
   //+------------------------------------------------------------------+
   bool UseOptionE()
     {
      return((m_options & ECMAScript) != 0);
     }
   static const char Q;            // quantifier
   static const char S;            // ordinary stoppper
   static const char Z;            // ScanBlank stopper
   static const char X;            // whitespace
   static const char E;            // should be escaped
   //--- For categorizing ascii characters.
   static const int  s_category[];
   //+------------------------------------------------------------------+
   //| Returns true for those characters that terminate a string of     |
   //| ordinary chars.                                                  |
   //+------------------------------------------------------------------+
   static bool IsSpecial(const ushort ch)
     {
      return(ch <= '|' && s_category[ch] >= S);
     }
   //+------------------------------------------------------------------+
   //| Returns true for those characters that terminate a string of     |
   //| ordinary chars.                                                  |
   //+------------------------------------------------------------------+
   static bool IsStopperX(const ushort ch)
     {
      return(ch <= '|' && s_category[ch] >= X);
     }
   //+------------------------------------------------------------------+
   //| Returns true for those characters that begin a quantifier.       |
   //+------------------------------------------------------------------+
   static bool IsQuantifier(ushort ch)
     {
      return(ch <= '{' && s_category[ch] >= Q);
     }
   //+------------------------------------------------------------------+
   //| IsTrueQuantifier.                                                |
   //+------------------------------------------------------------------+
   bool IsTrueQuantifier()
     {
      int nChars= CharsRight();
      if(nChars == 0)
        {
         //--- return 
         return (false);
        }
      int startpos=Textpos();
      ushort ch=CharAt(startpos);
      if(ch!='{')
        {
         //--- return 
         return (ch <= '{' && s_category[ch] >= Q);
        }
      int pos=startpos;
      while(--nChars>0 && (ch=CharAt(++pos))>='0' && ch<='9');
      if(nChars==0 || pos-startpos==1)
        {
         //--- return 
         return (false);
        }
      if(ch=='}')
        {
         //--- return 
         return (true);
        }
      if(ch!=',')
        {
         //--- return 
         return (false);
        }
      while(--nChars>0 && (ch=CharAt(++pos))>='0' && ch<='9');
      //--- return 
      return (nChars > 0 && ch == '}');
     }
   //+------------------------------------------------------------------+
   //| Returns true for whitespace.                                     |
   //+------------------------------------------------------------------+
   static bool IsSpace(const ushort ch)
     {
      return(ch <= ' ' && s_category[ch] == X);
     }
   //+------------------------------------------------------------------+
   //| Returns true for chars that should be escaped.                   |
   //+------------------------------------------------------------------+
   static bool IsMetachar(const ushort ch)
     {
      return(ch <= '|' && s_category[ch] >= E);
     }
   //+------------------------------------------------------------------+
   //| Add a string to the last concatenate.                            |
   //+------------------------------------------------------------------+
   void AddConcatenate(const int pos,const int cch,const bool isReplacement)
     {
      CRegexNode *node;
      if(cch==0)
        {
         //--- return
         return;
        }
      if(cch>1)
        {
         string str=StringSubstr(m_pattern,pos,cch);
         if(UseOptionI() && !isReplacement)
           {
            //--- We do the ToLower character by character for consistency.  With surrogate chars, doing
            //--- a ToLower on the entire string could actually change the surrogate pair.  This is more correct
            //--- linguistically, but since Regex doesn't support surrogates, it's more important to be 
            //--- consistent. 
            StringToLower(str);
           }
         node=new CRegexNode(Multi,m_options,str);
        }
      else
        {
         ushort ch;
         if(UseOptionI() && !isReplacement)
           {
            string s=m_pattern;
            StringToLower(s);
            ch=StringGetCharacter(s,pos);
           }
         else
           {
            ch=StringGetCharacter(m_pattern,pos);
           }
         node=new CRegexNode(One,m_options,(ushort)ch);
        }
      m_concatenation.AddChild(node);
     }
   //+------------------------------------------------------------------+
   //| Push the parser state (in response to an open paren).            |
   //+------------------------------------------------------------------+
   void PushGroup()
     {
      m_group.Next(m_stack);
      m_alternation.Next(m_group);
      m_concatenation.Next(m_alternation);
      m_stack=m_concatenation;
     }
   //+------------------------------------------------------------------+
   //| Remember the pushed state (in response to a ')').                |
   //+------------------------------------------------------------------+
   void PopGroup()
     {
      m_concatenation=m_stack;
      m_alternation=m_concatenation.Next();
      m_group = m_alternation.Next();
      m_stack = m_group.Next();
      //--- The first () inside a Testgroup group goes directly to the group
      if(m_group.Type()==Testgroup_Type && m_group.ChildCount()==0)
        {
         if(m_unit==NULL)
           {
            Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
            return;
           }
         m_group.AddChild(m_unit);
         m_unit=NULL;
        }
     }
   //+------------------------------------------------------------------+
   //| True if the group stack is empty.                                |
   //+------------------------------------------------------------------+
   bool EmptyStack()
     {
      return (m_stack == NULL);
     }
   //+------------------------------------------------------------------+
   //| Start a new round for the parser state (in response to an open   |
   //| paren or string start).                                          |
   //+------------------------------------------------------------------+
   void StartGroup(CRegexNode *openGroup)
     {
      m_group=openGroup;
      m_alternation=new CRegexNode(Alternate_Type,m_options);
      m_concatenation=new CRegexNode(Concatenate_Type,m_options);
     }
   //+------------------------------------------------------------------+
   //| Finish the current concatenation (in response to a |).           |
   //+------------------------------------------------------------------+
   void AddAlternate()
     {
      //--- The | parts inside a Testgroup group go directly to the group
      if(m_group.Type()==Testgroup_Type || m_group.Type()==Testrefnode_Type)
        {
         m_group.AddChild(m_concatenation.ReverseLeft());
        }
      else
        {
         m_alternation.AddChild(m_concatenation.ReverseLeft());
        }
      m_concatenation=new CRegexNode(Concatenate_Type,m_options);
     }
   //+------------------------------------------------------------------+
   //| Finish the current quantifiable (when a quantifier is not found  |
   //| or is not possible).                                             |
   //+------------------------------------------------------------------+
   void AddConcatenate()
     {
      //--- The first (| inside a Testgroup group goes directly to the group
      m_concatenation.AddChild(m_unit);
      m_unit=NULL;
     }
   //+------------------------------------------------------------------+
   //| Finish the current quantifiable (when a quantifier is found).    |
   //+------------------------------------------------------------------+
   void AddConcatenate(const bool lazy,const int min,const int max)
     {
      m_concatenation.AddChild(m_unit.MakeQuantifier(lazy,min,max));
      m_unit=NULL;
     }
   //+------------------------------------------------------------------+
   //| Returns the current unit.                                        |
   //+------------------------------------------------------------------+
   CRegexNode *Unit()
     {
      return (m_unit);
     }
   //+------------------------------------------------------------------+
   //| Sets the current unit to a single char node.                     |
   //+------------------------------------------------------------------+
   void AddUnitOne(ushort ch)
     {
      if(UseOptionI())
        {
         string s=ShortToString(ch);
         StringToLower(s);
         ch=StringGetCharacter(s,0);
        }
      m_unit=new CRegexNode(One,m_options,ushort(ch));
     }
   //+------------------------------------------------------------------+
   //| Sets the current unit to a single inverse-char node.             |
   //+------------------------------------------------------------------+
   void AddUnitNotone(ushort ch)
     {
      if(UseOptionI())
        {
         string s=ShortToString(ch);
         StringToLower(s);
         ch=StringGetCharacter(s,0);
        }
      m_unit=new CRegexNode(Notone,m_options,(ushort)ch);
     }
   //+------------------------------------------------------------------+
   //| Sets the current unit to a single set node.                      |
   //+------------------------------------------------------------------+
   void AddUnitSet(const string cc)
     {
      m_unit=new CRegexNode(Set,m_options,cc);
     }
   //+------------------------------------------------------------------+
   //| Sets the current unit to a subtree.                              |
   //+------------------------------------------------------------------+
   void AddUnitNode(CRegexNode *node)
     {
      m_unit=node;
     }
   //+------------------------------------------------------------------+
   //| Sets the current unit to an assertion of the specified type.     |
   //+------------------------------------------------------------------+
   void AddUnitType(const int type)
     {
      m_unit=new CRegexNode(type,m_options);
     }
   //+------------------------------------------------------------------+
   //| Finish the current group (in response to a ')' or end).          |
   //+------------------------------------------------------------------+
   void AddGroup()
     {
      if(m_group.Type()==Testgroup_Type || m_group.Type()==Testrefnode_Type)
        {
         m_group.AddChild(m_concatenation.ReverseLeft());
         if((m_group.Type()==Testrefnode_Type) && (m_group.ChildCount()>2 || m_group.ChildCount()>3))
           {
            Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
            return;
           }
        }
      else
        {
         m_alternation.AddChild(m_concatenation.ReverseLeft());
         m_group.AddChild(m_alternation);
        }
      m_unit=m_group;
     }
   //+------------------------------------------------------------------+
   //| Saves options on a stack.,                                       |
   //+------------------------------------------------------------------+
   void PushOptions()
     {
      m_optionsStack.Add(m_options);
     }
   //+------------------------------------------------------------------+
   //| Recalls options from the stack.                                  |
   //+------------------------------------------------------------------+
   void PopOptions()
     {
      m_options=m_optionsStack[m_optionsStack.Count()-1];
      m_optionsStack.RemoveAt(m_optionsStack.Count()-1);
     }
   //+------------------------------------------------------------------+
   //| True if options stack is empty.                                  |
   //+------------------------------------------------------------------+
   bool EmptyOptionsStack()
     {
      return(m_optionsStack.Count() == 0);
     }
   //+------------------------------------------------------------------+
   //| Pops the option stack, but keeps the current options unchanged.  |
   //+------------------------------------------------------------------+
   void PopKeepOptions()
     {
      m_optionsStack.RemoveAt(m_optionsStack.Count()-1);
     }
   //+------------------------------------------------------------------+
   //| Returns the current parsing position.                            |
   //+------------------------------------------------------------------+
   int Textpos()
     {
      return (m_currentPos);
     }
   //+------------------------------------------------------------------+
   //| Zaps to a specific parsing position.                             |
   //+------------------------------------------------------------------+
   void Textto(const int pos)
     {
      m_currentPos=pos;
     }
   //+------------------------------------------------------------------+
   //| Returns the char at the right of the current parsing position and|
   //| advances to the right.                                           |
   //+------------------------------------------------------------------+
   ushort MoveRightGetChar()
     {
      return (StringGetCharacter(m_pattern,m_currentPos++));
     }
   //+------------------------------------------------------------------+
   //| Moves the current position to the right.                         |
   //+------------------------------------------------------------------+
   void MoveRight()
     {
      MoveRight(1);
     }
   //+------------------------------------------------------------------+
   //| Moves the current position to the right.                         |
   //+------------------------------------------------------------------+
   void MoveRight(int i)
     {
      m_currentPos+=i;
     }
   //+------------------------------------------------------------------+
   //| Moves the current parsing position one to the left.              |
   //+------------------------------------------------------------------+
   void MoveLeft()
     {
      --m_currentPos;
     }
   //+------------------------------------------------------------------+
   //| Returns the char left of the current parsing position.           |
   //+------------------------------------------------------------------+
   ushort CharAt(const int i)
     {
      return (StringGetCharacter(m_pattern,i));
     }
   //+------------------------------------------------------------------+
   //| Returns the char right of the current parsing position.          |
   //+------------------------------------------------------------------+
   ushort RightChar()
     {
      return (StringGetCharacter(m_pattern,m_currentPos));
     }
   //+------------------------------------------------------------------+
   //| Returns the char i chars right of the current parsing position.  |
   //+------------------------------------------------------------------+
   ushort RightChar(int i)
     {
      return (StringGetCharacter(m_pattern,m_currentPos + i));
     }
   //+------------------------------------------------------------------+
   //| Number of characters to the right of the current parsing         |
   //| position.                                                        |
   //+------------------------------------------------------------------+
   int CharsRight()
     {
      return (StringLen(m_pattern) - m_currentPos);
     }
  };
static const int  CRegexParser::MaxValueDiv10=Int32::MaxValue/10;
static const int  CRegexParser::MaxValueMod10=Int32::MaxValue%10;
static const char CRegexParser::Q = 5;
static const char CRegexParser::S = 4;
static const char CRegexParser::Z = 3;
static const char CRegexParser::X = 2;
static const char CRegexParser::E = 1;
static const int  CRegexParser::s_category[]=
  {
/* 0 1 2 3 4 5 6 7 8 9 A B C D E F 0 1 2 3 4 5 6 7 8 9 A B C D E F   */
   0,0,0,0,0,0,0,0,0,2,2,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
/*   ! " # $ % & ' ( ) * + , - . / 0 1 2 3 4 5 6 7 8 9 : ; < = > ?   */
   2,0,0,3,4,0,0,0,4,4,5,5,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,
/* @ A B C D E F G H I J K L M N O P Q R S T U V W X Y Z [ \ ] ^ _   */
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,0,4,0,
/* ' a b c d e f g h i j k l m n o p q r s t u v w x y z { | } ~     */
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,4,0,0,0
  };
static int CRegexParser::s_id=0;
//+------------------------------------------------------------------+
