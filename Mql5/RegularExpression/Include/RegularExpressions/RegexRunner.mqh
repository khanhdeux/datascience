//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
class CRegex;
class CMatch;
#include <Internal\TimeSpan\TimeSpan.mqh>
//+------------------------------------------------------------------+
//| Purpose: This CRegexRunner class is a base class for compiled     |
//| regex code.                                                      |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| CRegexRunner provides a common calling convention and a common    |
//| runtime environment for the interpreter and the compiled code.   |
//|                                                                  |
//| It provides the driver code that call's the subclass's Go()      |
//| method for either scanning or direct execution.                  |
//|                                                                  |
//| It also maintains memory allocation for the backtracking stack,  |
//| the grouping stack and the longjump crawlstack, and provides     |
//| methods to push new subpattern match results into (or remove     |
//| backtracked results from) the CMatch instance.                   |
//+------------------------------------------------------------------+
class CRegexRunner
  {
protected:
   int               m_runtextbeg;    // beginning of text to search
   int               m_runtextend;    // end of text to search
   int               m_runtextstart;  // starting point for search
   string            m_runtext;       // text to search
   int               m_runtextpos;    // current position in text
   //+------------------------------------------------------------------+
   //| The backtracking stack.  Opcodes use this to store data regarding|      
   //| what they have matched and where to backtrack to.  Each "frame"  |
   //| on the stack takes the form of [CodePosition Data1 Data2...],    |
   //| where CodePosition is the position of the current opcode and the |
   //| data values are all optional.  The CodePosition can be negative, |
   //| and these values (also called "back2") are used by the BranchMark|
   //| family of opcodes to indicate whether they are backtracking after|
   //| a successful or failed match.                                    | 
   //| When we backtrack, we pop the CodePosition off the stack, set the|
   //| current instruction pointer to that code position, and mark the  |
   //| opcode with a backtracking flag ("Back").  Each opcode then knows| 
   //| how to handle its own data.                                      |
   //+------------------------------------------------------------------+
   int               m_runtrack[];
   int               m_runtrackpos;
   //+------------------------------------------------------------------+
   //| This stack is used to track text positions across different      |
   //| opcodes. For example, in /(a*b)+/, the parentheses result in a   |
   //| SetMark/CaptureMark pair. SetMark records the text position      |
   //| before we match a*b. Then CaptureMark uses that position to      |
   //| figure out where the capture starts. Opcodes which push onto this| 
   //| stack are always paired with other opcodes which will pop the    |
   //| value from it later.  A successful match should mean that this   |
   //| stack is empty.                                                  |
   //+------------------------------------------------------------------+
   int               m_runstack[];
   int               m_runstackpos;
   //+------------------------------------------------------------------+
   //| The crawl stack is used to keep track of captures.  Every time a |
   //| group has a capture, we push its group number onto the runcrawl  |
   //| stack. In the case of a balanced match, we push BOTH groups onto |
   //| the stack.                                                       |
   //+------------------------------------------------------------------+
   int               m_runcrawl[];
   int               m_runcrawlpos;
   int               m_runtrackcount; // count of states that may do backtracking
   CMatch            *m_runmatch;      // result object
   CRegex            *m_runregex;      // regex object
private:
   int               m_timeout;       // timeout in millisecs (needed for actual)        
   bool              m_ignoreTimeout;
   int               m_timeoutOccursAt;
   //+------------------------------------------------------------------+
   //| GPaperin: We have determined this value in a series of           |
   //| experiments where x86 retail builds (ono-lab-optimised) were run |
   //| on different pattern/input pairs. Larger values of               |
   //| TimeoutCheckFrequency did not tend to increase performance;      |
   //| smaller values of TimeoutCheckFrequency tended to slow down the  |
   //| execution.                                                       |
   //+------------------------------------------------------------------+
   static const int  TimeoutCheckFrequency;
   int               m_timeoutChecksToSkip;
public:
   //+------------------------------------------------------------------+
   //| Constructor without parameters.                                  |
   //+------------------------------------------------------------------+
                     CRegexRunner(): m_runtextbeg(0), m_runtextend(0), m_runtextstart(0), m_runtextpos(0),
                     m_runtrackpos(0), m_runstackpos(0), m_runcrawlpos(0), m_runtrackcount(0),
                     m_timeoutChecksToSkip(0)
     {
     }
   //--- Destructors:
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~CRegexRunner()
     {
      if(CheckPointer(m_runmatch)==POINTER_DYNAMIC)
        {
         delete m_runmatch;
        }
     }
   //+------------------------------------------------------------------+
   //| RunRegex.                                                        |
   //+------------------------------------------------------------------+
   CRegex *RunRegex()
     {
      //--- return runregex
      return (m_runregex);
     }
   //+------------------------------------------------------------------+
   //| Scans the string to find the first match. Uses the CMatch object |
   //| both to feed text in and as a place to store matches that come   |
   //| out.                                                             |
   //| All the action is in the abstract Go() method defined by         |
   //| subclasses. Our responsibility is to load up the class members(as| 
   //| done here) before calling Go.                                    |
   //+------------------------------------------------------------------+
   CMatch *Scan(CRegex *regex,const string text,const int textbeg,const int textend,const int textstart,const int prevlen,const bool quick)
     {
      return Scan(regex, text, textbeg, textend, textstart, prevlen, quick, regex.MatchTimeout());
     }
   //+------------------------------------------------------------------+
   //| Scans the string to find the first match. Uses the CMatch object |
   //| both to feed text in and as a place to store matches that come   |
   //| out.                                                             |
   //+------------------------------------------------------------------+
   CMatch *Scan(CRegex *regex,const string text,const int textbeg,const int textend,const int textstart,const int prevlen,const bool quick,const TimeSpan &timeout)
     {
      int bump;
      int stoppos;
      bool initted=false;
      //--- We need to re-validate timeout here because Scan is historically protected and
      //--- thus there is a possibility it is called from user code:
      CRegex::ValidateMatchTimeout(timeout);
      this.m_ignoreTimeout=(CRegex::InfiniteMatchTimeout==timeout);
      this.m_timeout=this.m_ignoreTimeout
                     ?(int)CRegex::InfiniteMatchTimeout.TotalMilliseconds()
                     :(int)(timeout.TotalMilliseconds()+0.5); // Round   
      if(CheckPointer(m_runregex)==POINTER_DYNAMIC && m_runregex!=regex)
        {
         delete m_runregex;
        }
      m_runregex      = regex;
      m_runtext       = text;
      m_runtextbeg    = textbeg;
      m_runtextend    = textend;
      m_runtextstart  = textstart;
      bump    = m_runregex.RightToLeft() ? -1 : 1;
      stoppos = m_runregex.RightToLeft() ? m_runtextbeg : m_runtextend;
      m_runtextpos=textstart;
      //--- If previous match was empty or failed, advance by one before matching
      if(prevlen==0)
        {
         if(m_runtextpos==stoppos)
           {
            //--- return 
            return (CMatch::Empty());
           }
         m_runtextpos+=bump;
        }
      StartTimeoutWatch();
      for(;;)
        {
#ifdef _DEBUG
         if(m_runregex.Debug())
           {
            Print("\0");
            Print("Search range: from "+(string)m_runtextbeg+" to "+(string)m_runtextend);
            Print("Firstchar search starting at "+(string)m_runtextpos+" stopping at "+(string)stoppos);
           }
#endif
         if(FindFirstChar())
           {
            CheckTimeout();
            if(!initted)
              {
               InitMatch();
               initted=true;
              }
#ifdef _DEBUG
            if(m_runregex.Debug())
              {
               Print("Executing engine starting at "+(string)m_runtextpos);
               Print("");
              }
#endif
            Go();
            int matchcount[];
            m_runmatch.GetMatchCount(matchcount);
            if(matchcount[0]>0)
              {
               //--- return result
               return (TidyMatch(quick));
              }
            //--- reset state for another go
            m_runtrackpos = ArraySize(m_runtrack);
            m_runstackpos = ArraySize(m_runstack);
            m_runcrawlpos = ArraySize(m_runcrawl);
           }
         //--- failure!
         if(m_runtextpos==stoppos)
           {
            TidyMatch(true);
            //--- return 
            return (CMatch::Empty());
           }
         //--- Bump by one and start again
         m_runtextpos+=bump;
        }
      //--- We never get here
      return (NULL);
     }
private:
   //+------------------------------------------------------------------+
   //| StartTimeoutWatch.                                               |
   //+------------------------------------------------------------------+
   void StartTimeoutWatch()
     {
      if(m_ignoreTimeout)
        {
         return;
        }
      m_timeoutChecksToSkip=TimeoutCheckFrequency;
      //--- We are using Environment.TickCount and not Timewatch for performance reasons.
      //--- Environment.TickCount is an int that cycles. We intentionally let timeoutOccursAt
      //--- overflow it will still stay ahead of Environment.TickCount for comparisons made
      //--- in DoCheckTimeout():
      m_timeoutOccursAt=(int)GetTickCount()+m_timeout;
     }
protected:
   //+------------------------------------------------------------------+
   //| CheckTimeout.                                                    |
   //+------------------------------------------------------------------+
   void CheckTimeout()
     {
      if(m_ignoreTimeout)
        {
         return;
        }
      if(--m_timeoutChecksToSkip!=0)
        {
         return;
        }
      m_timeoutChecksToSkip=TimeoutCheckFrequency;
      DoCheckTimeout();
     }
private:
   void DoCheckTimeout()
     {
      //--- Note that both, Environment.TickCount and timeoutOccursAt are ints and can overflow and become negative.
      //--- See the comment in StartTimeoutWatch().
      int currentMillis=(int)GetTickCount();
      if(currentMillis<m_timeoutOccursAt)
        {
         return;
        }
      if(0>m_timeoutOccursAt && 0<currentMillis)
        {
         return;
        }
#ifdef _DEBUG
      if(m_runregex.Debug())
        {
         Print("");
         Print("RegEx match timeout occurred!");
         Print("Specified timeout:       "+TimeSpan::FromMilliseconds(m_timeout).ToString());
         Print("Timeout check frequency: "+IntegerToString(TimeoutCheckFrequency));
         Print("Search pattern:          "+m_runregex.Pattern());
         Print("Input:                   "+m_runtext);
         Print("About to throw RegexMatchTimeoutException.");
        }
#endif
      Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
      return;
     }
protected:
   //+------------------------------------------------------------------+
   //| The responsibility of Go() is to run the regular expression at   |
   //| runtextpos and call Capture() on all the captured subexpressions,|
   //| then to leave runtextpos at the ending position. It should leave |
   //| runtextpos where it started if there was no match.               |
   //+------------------------------------------------------------------+
   virtual void      Go()=NULL;
   //+------------------------------------------------------------------+
   //| The responsibility of FindFirstChar() is to advance runtextpos   |
   //| until it is at the next position which is a candidate for the    |
   //| beginning of a successful match.                                 |
   //+------------------------------------------------------------------+
   virtual bool      FindFirstChar()=NULL;
   //+------------------------------------------------------------------+
   //| InitTrackCount must initialize the runtrackcount field; this is  |
   //| used to know how large the initial runtrack and runstack arrays  |
   //| must be.                                                         |
   //+------------------------------------------------------------------+
   virtual void      InitTrackCount()=NULL;
   //+------------------------------------------------------------------+
   //| Initializes all the data members that are used by Go().          |
   //+------------------------------------------------------------------+
private:
   void InitMatch()
     {
      //--- Use a hashtable'ed CMatch object if the capture numbers are sparse
      if(m_runmatch==NULL)
        {
         if(m_runregex.Caps()!=NULL)
           {
            if(CheckPointer(m_runmatch)==POINTER_DYNAMIC)
              {
               delete m_runmatch;
              }
            m_runmatch=new MatchSparse(m_runregex,m_runregex.Caps(),m_runregex.CapSize(),m_runtext,m_runtextbeg,m_runtextend-m_runtextbeg,m_runtextstart);
           }
         else
           {
            if(CheckPointer(m_runmatch)==POINTER_DYNAMIC)
              {
               delete m_runmatch;
              }
            m_runmatch=new CMatch(m_runregex,m_runregex.CapSize(),m_runtext,m_runtextbeg,m_runtextend-m_runtextbeg,m_runtextstart);
           }
        }
      else
        {
         m_runmatch.Reset(m_runregex,m_runtext,m_runtextbeg,m_runtextend,m_runtextstart);
        }
      //--- note we test runcrawl, because it is the last one to be allocated
      //--- If there is an alloc failure in the middle of the three allocations,
      //--- we may still return to reuse this instance, and we want to behave
      //--- as if the allocations didn't occur. (we used to test _trackcount != 0)
      if(ArraySize(m_runcrawl)!=NULL)
        {
         m_runtrackpos = ArraySize(m_runtrack);
         m_runstackpos = ArraySize(m_runstack);
         m_runcrawlpos = ArraySize(m_runcrawl);
         //--- return 
         return;
        }
      InitTrackCount();
      int tracksize = m_runtrackcount * 8;
      int stacksize = m_runtrackcount * 8;
      if(tracksize<32)
        {
         tracksize=32;
        }
      if(stacksize<16)
        {
         stacksize=16;
        }
      ArrayResize(m_runtrack,tracksize);
      m_runtrackpos=tracksize;
      ArrayResize(m_runstack,stacksize);
      m_runstackpos=stacksize;
      ArrayResize(m_runcrawl,32);
      m_runcrawlpos=32;
     }
   //+------------------------------------------------------------------+
   //| Put match in its canonical form before returning it.             |
   //+------------------------------------------------------------------+
   CMatch *TidyMatch(const bool quick)
     {
      if(!quick)
        {
         CMatch *match=m_runmatch;
         m_runmatch=NULL;
         match.Tidy(m_runtextpos);
         return (match);
        }
      else
        {
         //--- in quick mode, a successful match returns null, and
         //--- the allocated match object is left in the cache
         return (NULL);
        }
     }
protected:
   //+------------------------------------------------------------------+
   //| Called by the implemenation of Go() to increase the size of      |
   //| storage.                                                         |
   //+------------------------------------------------------------------+
   void EnsureStorage()
     {
      if(m_runstackpos<m_runtrackcount*4)
        {
         DoubleStack();
        }
      if(m_runtrackpos<m_runtrackcount*4)
        {
         DoubleTrack();
        }
     }
   //+------------------------------------------------------------------+
   //| Called by the implemenation of Go() to decide whether the pos at |
   //| the specified index is a boundary or not. It's just not worth    |
   //| emitting inline code for this logic.                             |
   //+------------------------------------------------------------------+
   bool IsBoundary(const int index,const int startpos,const int endpos)
     {
      return (index > startpos &&
              CRegexCharClass::IsWordChar(StringGetCharacter(m_runtext,index-1)))!=
              (index<endpos && CRegexCharClass::IsWordChar(StringGetCharacter(m_runtext,index)));
     }
   //+------------------------------------------------------------------+
   //| IsECMABoundary.                                                  |
   //+------------------------------------------------------------------+
   bool IsECMABoundary(const int index,const int startpos,const int endpos)
     {
      return (index > startpos &&
              CRegexCharClass::IsECMAWordChar(StringGetCharacter(m_runtext,index-1)))!=
              (index<endpos && CRegexCharClass::IsECMAWordChar(StringGetCharacter(m_runtext,index)));
     }
   //+------------------------------------------------------------------+
   //| CharInSet.                                                       |
   //+------------------------------------------------------------------+
   static bool CharInSet(const char ch,const string set,const string category)
     {
      string charClass=CRegexCharClass::ConvertOldstringsToClass(set,category);
      return CRegexCharClass::CharInClass(ch, charClass);
     }
   //+------------------------------------------------------------------+
   //| CharInClass.                                                     |
   //+------------------------------------------------------------------+
   static bool CharInClass(const char ch,const string charClass)
     {
      return CRegexCharClass::CharInClass(ch, charClass);
     }
   //+------------------------------------------------------------------+
   //| Called by the implemenation of Go() to increase the size of the  |
   //| backtracking stack.                                              |
   //+------------------------------------------------------------------+
   void DoubleTrack()
     {
      int newtrack[];
      ArrayResize(newtrack,ArraySize(m_runtrack)*2);
      ArrayCopy(newtrack,m_runtrack,ArraySize(m_runtrack),0,ArraySize(m_runtrack));
      m_runtrackpos+=ArraySize(m_runtrack);
      ArrayCopy(m_runtrack,newtrack);
     }
   //+------------------------------------------------------------------+
   //| Called by the implemenation of Go() to increase the size of the  |
   //| grouping stack.                                                  |
   //+------------------------------------------------------------------+
   void DoubleStack()
     {
      int newstack[];
      ArrayResize(newstack,ArraySize(m_runstack)*2);
      ArrayCopy(newstack,m_runstack,ArraySize(m_runstack),0,ArraySize(m_runstack));
      m_runstackpos+=ArraySize(m_runstack);
      ArrayCopy(m_runstack,newstack);
     }
   //+------------------------------------------------------------------+
   //| Increases the size of the longjump unrolling stack.              |
   //+------------------------------------------------------------------+
   void DoubleCrawl()
     {
      int newcrawl[];
      ArrayResize(newcrawl,ArraySize(m_runcrawl)*2);
      ArrayCopy(newcrawl,m_runcrawl,ArraySize(m_runcrawl),0,ArraySize(m_runcrawl));
      m_runcrawlpos+=ArraySize(m_runcrawl);
      ArrayCopy(m_runcrawl,newcrawl);
     }
   //+------------------------------------------------------------------+
   //| Save a number on the longjump unrolling stack.                   |
   //+------------------------------------------------------------------+
   void Crawl(const int i)
     {
      if(m_runcrawlpos==0)
        {
         DoubleCrawl();
        }
      m_runcrawl[--m_runcrawlpos]=i;
     }
   //+------------------------------------------------------------------+
   //| Remove a number from the longjump unrolling stack.               |
   //+------------------------------------------------------------------+
   int Popcrawl()
     {
      return (m_runcrawl[m_runcrawlpos++]);
     }
   //+------------------------------------------------------------------+
   //| Get the height of the stack.                                     |
   //+------------------------------------------------------------------+
   int Crawlpos()
     {
      return (ArraySize(m_runcrawl) - m_runcrawlpos);
     }
   //+------------------------------------------------------------------+
   //| Called by Go() to capture a subexpression. Note that the capnum  |
   //| used here has already been mapped to a non-sparse index (by the  |
   //| code generator RegexWriter).                                     |
   //+------------------------------------------------------------------+
   void Capture(const int capnum,int start,int end)
     {
      if(end<start)
        {
         int T;
         T=end;
         end=start;
         start=T;
        }
      Crawl(capnum);
      m_runmatch.AddMatch(capnum,start,end-start);
     }
   //+------------------------------------------------------------------+
   //| Called by Go() to capture a subexpression. Note that the capnum  |
   //| used here has already been mapped to a non-sparse index (by the  |
   //| code generator RegexWriter).                                     |
   //+------------------------------------------------------------------+
   void TransferCapture(const int capnum,const int uncapnum,int start,int end)
     {
      int start2;
      int end2;
      //--- these are the two intervals that are cancelling each other
      if(end<start)
        {
         int T;
         T=end;
         end=start;
         start=T;
        }
      start2=MatchIndex(uncapnum);
      end2=start2+MatchLength(uncapnum);
      //--- The new capture gets the innermost defined interval
      if(start>=end2)
        {
         end=start;
         start=end2;
        }
      else if(end<=start2)
        {
         start=start2;
        }
      else
        {
         if(end>end2)
           {
            end=end2;
           }
         if(start2>start)
           {
            start=start2;
           }
        }
      Crawl(uncapnum);
      m_runmatch.BalanceMatch(uncapnum);
      if(capnum!=-1)
        {
         Crawl(capnum);
         m_runmatch.AddMatch(capnum,start,end-start);
        }
     }
   //+------------------------------------------------------------------+
   //| Called by Go() to revert the last capture.                       |
   //+------------------------------------------------------------------+
   void Uncapture()
     {
      int capnum=Popcrawl();
      m_runmatch.RemoveMatch(capnum);
     }
   //+------------------------------------------------------------------+
   //| Call out to runmatch to get around visibility issues.            |
   //+------------------------------------------------------------------+
   bool IsMatched(const int cap)
     {
      return (m_runmatch.IsMatched(cap));
     }
   //+------------------------------------------------------------------+
   //| Call out to runmatch to get around visibility issues.            |
   //+------------------------------------------------------------------+
   int MatchIndex(const int cap)
     {
      return (m_runmatch.MatchIndex(cap));
     }
   //+------------------------------------------------------------------+
   //| Call out to runmatch to get around visibility issues.            |
   //+------------------------------------------------------------------+
   int MatchLength(const int cap)
     {
      return (m_runmatch.MatchLength(cap));
     }
#ifdef _DEBUG
   //+------------------------------------------------------------------+
   //| Dump the current state.                                          |
   //+------------------------------------------------------------------+
   virtual void DumpState()
     {
      Print("Text:  "+TextposDescription());
      Print("Track: "+StackDescription(m_runtrack,m_runtrackpos));
      Print("Stack: "+StackDescription(m_runstack,m_runstackpos));
     }
   //+------------------------------------------------------------------+
   //| StackDescription.                                                |
   //+------------------------------------------------------------------+
   static string StackDescription(const int &A[],const int Index)
     {
      string text;
      text+=IntegerToString(ArraySize(A) - (Index));
      text+=CharToString('/');
      text+=IntegerToString(ArraySize(A));
      if(StringLen(text)<8)
        {
         string append;
         StringInit(append,8-StringLen(text),' ');
         text+=append;
        }
      text+=("(");
      for(int i=Index; i<ArraySize(A); i++)
        {
         if(i>Index)
           {
            text+=CharToString(' ');
           }
         text+=IntegerToString(A[i]);
        }
      text+=CharToString(')');
      return (text);
     }
   //+------------------------------------------------------------------+
   //| TextposDescription.                                              |
   //+------------------------------------------------------------------+
   virtual string TextposDescription()
     {
      string text;
      int remaining;
      text+=IntegerToString(m_runtextpos);
      if(StringLen(text)<8)
        {
         string append;
         StringInit(append,8-StringLen(text),' ');
         text+=append;
        }
      if(m_runtextpos>m_runtextbeg)
        {
         text+=(CRegexCharClass::CharDescription(StringGetCharacter(m_runtext,m_runtextpos-1)));
        }
      else
        {
         text+=CharToString('^');
        }
      text+=CharToString('>');
      remaining=m_runtextend-m_runtextpos;
      for(int i=m_runtextpos; i<m_runtextend; i++)
        {
         text+=(CRegexCharClass::CharDescription(StringGetCharacter(m_runtext,i)));
        }
      if(StringLen(text)>=64)
        {
         text = StringSubstr(text,0,61);
         text+=("...");
        }
      else
        {
         text+=CharToString('$');
        }
      return (text);
     }
#endif
  };
static const int CRegexRunner::TimeoutCheckFrequency=1000;
#include "Regex.mqh"
//+------------------------------------------------------------------+
