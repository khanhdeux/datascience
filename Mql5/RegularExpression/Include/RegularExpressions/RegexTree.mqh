//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
#include "RegexNode.mqh"
//+------------------------------------------------------------------+
//| Purpose: CRegexTree is just a wrapper for a node tree with some  |
//| global information attached.                                     |
//+------------------------------------------------------------------+
class CRegexTree
  {
private:
   CRegexNode        *m_root;
   int               m_capnumlist[];
   string            m_capslist[];
   RegexOptions      m_options;
   int               m_captop;
   Dictionary<int,int>*m_caps;
   Dictionary<string,int>*m_capnames;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CRegexTree(CRegexNode *root,Dictionary<int,int>*caps,
                                                 int &capnumlist[],const int captop,Dictionary<string,int>*capnames,
                                                 string &capslist[],const RegexOptions opts)

     {
      m_root = root;
      m_caps = caps;
      ArrayCopy(m_capnumlist,capnumlist);
      m_capnames=capnames;
      ArrayCopy(m_capslist,capslist);
      m_captop=captop;
      m_options=opts;
     } 
   //+------------------------------------------------------------------+
   //| Destructor without parametres.                                   |
   //+------------------------------------------------------------------+
                    ~CRegexTree()
     {
      int id=m_root.ID();
      if(CheckPointer(m_root)==POINTER_DYNAMIC)
        {
         for(int i=0; i<CRegexNode::s_allNodes.Count(); i++)
           {
            if(CheckPointer(CRegexNode::s_allNodes[i])==POINTER_DYNAMIC)
              {
               if(CRegexNode::s_allNodes[i].ID()==id)
                 {
                  delete CRegexNode::s_allNodes[i];
                 }
              }
           }
        }
      if(CheckPointer(m_capnames)==POINTER_DYNAMIC)
        {
         delete m_capnames;
        }
     }
   //+------------------------------------------------------------------+
   //| Gets the root.                                                   |
   //+------------------------------------------------------------------+
   CRegexNode *Root()
     {
      return (m_root);
     }
   //+------------------------------------------------------------------+
   //| GetCapNumList.                                                   |
   //+------------------------------------------------------------------+
   void GetCapNumList(int &array[])
     {
      ArrayCopy(array,m_capnumlist);
     }
   //+------------------------------------------------------------------+
   //| GetCapsList.                                                     |
   //+------------------------------------------------------------------+     
   void GetCapsList(string &array[])
     {
      ArrayCopy(array,m_capslist);
     }
   //+------------------------------------------------------------------+
   //| CapNames.                                                        |
   //+------------------------------------------------------------------+
   Dictionary<string,int>*CapNames()
     {
      return (m_capnames);
     }
   //+------------------------------------------------------------------+
   //| Caps.                                                            |
   //+------------------------------------------------------------------+
   Dictionary<int,int>*Caps()
     {
      return (m_caps);
     }
   //+------------------------------------------------------------------+
   //| Options.                                                         |
   //+------------------------------------------------------------------+
   RegexOptions Options()
     {
      return (m_options);
     }
   //+------------------------------------------------------------------+
   //| CapTop.                                                          |
   //+------------------------------------------------------------------+
   int CapTop()
     {
      return (m_captop);
     }
#ifdef _DEBUG
   //+------------------------------------------------------------------+
   //| Dump.                                                            |
   //+------------------------------------------------------------------+
   void Dump()
     {
      m_root.Dump();
     }
   //+------------------------------------------------------------------+
   //| Debug.                                                           |
   //+------------------------------------------------------------------+
   bool Debug()
     {
      //--- return result
      return ((m_options & RegexOptions::Debug) != 0);
     }
#endif
  };
//+------------------------------------------------------------------+
