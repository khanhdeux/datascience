//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
class CRegex;
#include "Regex.mqh"
#include "RegexNode.mqh"
#include "RegexGroupCollections.mqh"
//+------------------------------------------------------------------+
//| Constants for special insertion patterns.                        |
//+------------------------------------------------------------------+
enum SPECIAL_INSERTION_PARTTERNS
  {
   Specials=4,
   LeftPortion=-1,
   RightPortion=-2,
   LastGroup=-3,
   WholeString=-4,
  };
//+------------------------------------------------------------------+
//| Purpose: The CRegexReplacement class represents a substitution    |
//| string for use when using regexs to search/replace, etc. It's    |
//| logically a sequence intermixed (1) constant strings and (2)     |
//| group numbers.                                                   |
//+------------------------------------------------------------------+
class CRegexReplacement
  {
private:
   string            m_rep;
   List<string>*m_strings;          // table of string constants
   List<int>*m_rules;            // negative -> group #, positive -> string #
public:
   //+------------------------------------------------------------------+
   //| Since CRegexReplacement shares the same parser as CRegex, the    |
   //| constructor takes a CRegexNode which is a concatenation of       |
   //| constant strings and backreferences.                             |
   //+------------------------------------------------------------------+
                     CRegexReplacement(const string rep,CRegexNode *concat,Dictionary<int,int>*caps)
     {
      string text;
      List<string>*strings;
      List<int>*rules;
      int slot;
      m_rep=rep;
      if(concat.Type()!=Concatenate_Type)
        {
         Print("Invalid arguments."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return 
         return;
        }
      strings=new List<string>();
      rules=new List<int>();
      for(int i=0; i<concat.ChildCount(); i++)
        {
         CRegexNode *child=concat.Child(i);
         switch(child.Type())
           {
            case Multi:
              {
               text+=(child.Str());
               break;
              }
            case One:
              {
               text+=ShortToString(child.Ch());
               break;
              }
            case Ref:
              {
               if(StringLen(text)>0)
                 {
                  rules.Add(strings.Count());
                  strings.Add(text);
                  text=NULL;
                 }
               slot=child.Min();
               if(caps!=NULL && slot>=0)
                 {
                  slot=(int)caps[slot];
                 }
               rules.Add(-Specials-1-slot);
               break;
              }
            default:
              {
               Print("Invalid arguments."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
               //--- return 
               return;
              }
           }
        }
      if(StringLen(text)>0)
        {
         rules.Add(strings.Count());
         strings.Add(text);
        }
      m_strings=strings;
      m_rules=rules;
     }
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~CRegexReplacement()
     {
      delete m_strings;
      delete m_rules;
     }
private:
   //+------------------------------------------------------------------+
   //| Given a CMatch, emits into the StringBuilder the evaluated       |
   //| substitution pattern.                                            |
   //+------------------------------------------------------------------+
   void ReplacementImpl(string &text,CMatch *match)
     {
      for(int i=0; i<m_rules.Count(); i++)
        {
         int r= m_rules[i];
         if(r>=0) // string lookup
           {
            text+=m_strings[r];
           }
         else if(r<-Specials) // group lookup
           {
            text+=(match.GroupToStringImpl(-Specials-1-r));
           }
         else
           {
            switch(-Specials-1-r)
              {
               //--- special insertion patterns
               case LeftPortion:
                 {
                  text+=(match.GetLeftSubstring());
                  break;
                 }
               case RightPortion:
                 {
                  text+=(match.GetRightSubstring());
                  break;
                 }
               case LastGroup:
                 {
                  text+=(match.LastGroupToStringImpl());
                  break;
                 }
               case WholeString:
                 {
                  text+=(match.GetOriginalString());
                  break;
                 }
              }
           }
        }
     }
   //+------------------------------------------------------------------+
   //| Given a CMatch, emits into the List<String> the evaluated        |
   //| Right-to-Left substitution pattern.                              |
   //+------------------------------------------------------------------+
   void ReplacementImplRTL(List<string>*al,CMatch *match)
     {
      for(int i=m_rules.Count()-1; i>=0; i--)
        {
         int r= m_rules[i];
         if(r>=0) // string lookup
           {
            al.Add(m_strings[r]);
           }
         else if(r<-Specials) // group lookup
         al.Add(match.GroupToStringImpl(-Specials-1-r));
         else
           {
            switch(-Specials-1-r)
              {
               //--- special insertion patterns
               case LeftPortion:
                 {
                  al.Add(match.GetLeftSubstring());
                  break;
                 }
               case RightPortion:
                 {
                  al.Add(match.GetRightSubstring());
                  break;
                 }
               case LastGroup:
                 {
                  al.Add(match.LastGroupToStringImpl());
                  break;
                 }
               case WholeString:
                 {
                  al.Add(match.GetOriginalString());
                  break;
                 }
              }
           }
        }
     }
public:
   //+------------------------------------------------------------------+
   //| The original pattern string.                                     |
   //+------------------------------------------------------------------+
   string Pattern()
     {
      return (m_rep);
     }
   //+------------------------------------------------------------------+
   //| Returns the replacement result for a single match.               |
   //+------------------------------------------------------------------+
   string Replacement(CMatch *match)
     {
      string text;
      ReplacementImpl(text,match);
      return (text);
     }
   //+------------------------------------------------------------------+
   //| Three very similar algorithms appear below: replace (pattern),   |
   //|  replace (evaluator), and split.                                 |
   //|                                                                  |
   //| Replaces all ocurrances of the regex in the string with the      |
   //| replacement pattern.                                             |
   //|                                                                  |
   //| Note that the special case of no matches is handled on its own:  |
   //| with no matches,the input string is returned unchanged.          |
   //| The right-to-left case is split out because StringBuilder doesn't|
   //| handle right-to-left string building directly very well.         |
   //+------------------------------------------------------------------+
   string Replace(CRegex *regex,const string in,int count,const int startat)
     {
      CMatch *match;
      if(count<-1)
        {
         Print("Argument 'count' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return NULL
         return (NULL);
        }
      if(startat<0 || startat>StringLen(in))
        {
         Print("Argument 'startat' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return NULL
         return (NULL);
        }
      if(count==0)
        {
         //--- return 
         return (in);
        }
      match=regex.Match(in,startat);
      if(!match.Success())
        {
         if(CheckPointer(match)==POINTER_DYNAMIC)
           {
            delete match;
           }
         //--- return 
         return (in);
        }
      else
        {
         string text;
         if(!regex.RightToLeft())
           {
            int prevat=0;
            do
              {
               if(match.Index()!=prevat)
                 {
                  text+=StringSubstr(in,prevat,match.Index()-prevat);
                 }
               prevat=match.Index()+match.Length();
               ReplacementImpl(text,match);
               if(--count==0)
                 {
                  break;
                 }
               CMatch *old=match;
               match=match.NextMatch();
               delete old;
              }
            while(match.Success());
            if(prevat<StringLen(in))
              {
               text+=StringSubstr(in,prevat,StringLen(in)-prevat);
              }
           }
         else
           {
            List<string>al;
            int prevat=StringLen(in);
            do
              {
               if(match.Index()+match.Length()!=prevat)
                 {
                  al.Add(StringSubstr(in,match.Index()+match.Length(),prevat-match.Index()-match.Length()));
                 }
               prevat=match.Index();
               ReplacementImplRTL(GetPointer(al),match);
               if(--count==0)
                 {
                  break;
                 }
               match=match.NextMatch();
              }
            while(match.Success());
            if(prevat>0)
              {
               text+=StringSubstr(in,0,prevat);
              }
            for(int i=al.Count()-1; i>=0; i--)
              {
               text+=(al[i]);
              }
           }
         if(CheckPointer(match)==POINTER_DYNAMIC)
           {
            delete match;
           }
         //--- return 
         return (text);
        }
     }
   //+------------------------------------------------------------------+
   //| Replaces all ocurrances of the regex in the string with the      |
   //| replacement evaluator.                                           |
   //|                                                                  |
   //| Note that the special case of no matches is handled on its own:  |
   //| with no matches,the input string is returned unchanged.          |
   //| The right-to-left case is split out because string doesn't       |
   //| handle right-to-left string building directly very well.         |
   //+------------------------------------------------------------------+
   static string Replace(MatchEvaluator evaluator,CRegex *regex,const string in,int count,const int startat)
     {
      CMatch *match;
      if(evaluator==NULL)
        {
         Print("Argument 'evaluator' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return NULL
         return (NULL);
        }
      if(count<-1)
        {
         Print("Argument 'count' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return NULL
         return (NULL);
        }
      if(startat<0 || startat>StringLen(in))
        {
         Print("Argument 'startat' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return NULL
         return (NULL);
        }
      if(count==0)
        {
         //--- return reult
         return (in);
        }
      match=regex.Match(in,startat);
      if(!match.Success())
        {
         //--- return 
         return (in);
        }
      else
        {
         string text;
         if(!regex.RightToLeft())
           {
            int prevat=0;
            do
              {
               if(match.Index()!=prevat)
                 {
                  text+=StringSubstr(in,prevat,match.Index()-prevat);
                 }
               prevat=match.Index()+match.Length();
               text+=(evaluator(match));
               if(--count==0)
                 {
                  break;
                 }
               CMatch *old=match;
               match=match.NextMatch();
               delete old;
              }
            while(match.Success());
            if(prevat<StringLen(in))
              {
               text+=StringSubstr(in,prevat,StringLen(in)-prevat);
              }
           }
         else
           {
            List<string>al();
            int prevat=StringLen(in);
            do
              {
               if(match.Index()+match.Length()!=prevat)
                 {
                  al.Add(StringSubstr(in,match.Index()+match.Length(),prevat-match.Index()-match.Length()));
                 }
               prevat=match.Index();
               al.Add(evaluator(match));
               if(--count==0)
                 {
                  break;
                 }
               match=match.NextMatch();
              }
            while(match.Success());
            if(prevat>0)
              {
               text+=StringSubstr(in,0,prevat);
              }
            for(int i=al.Count()-1; i>=0; i--)
              {
               text+=(string)(al[i]);
              }
           }
         //--- return 
         return (text);
        }
     }
   //+------------------------------------------------------------------+
   //| Does a split. In the right-to-left case we reorder the array to  |
   //| be forwards.                                                     |
   //+------------------------------------------------------------------+
   static void Split(string &result[],CRegex *regex,const string in,int count,const int startat)
     {
      CMatch *match;
      if(count<0)
        {
         Print("Argument 'count' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return 
         return;
        }
      if(startat<0 || startat>StringLen(in))
        {
         Print("Argument 'startat' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return 
         return;
        }
      if(count==1)
        {
         ArrayResize(result,1);
         result[0]=in;
         //--- return 
         return;
        }
      count-=1;
      match=regex.Match(in,startat);
      if(!match.Success())
        {
         ArrayResize(result,1);
         result[0]=in;
         if(CheckPointer(match)==POINTER_DYNAMIC)
           {
            delete match;
           }
         //--- return 
         return;
        }
      else
        {
         List<string>al();
         if(!regex.RightToLeft())
           {
            int prevat=0;
            for(;;)
              {
               al.Add(StringSubstr(in,prevat,match.Index()-prevat));
               prevat=match.Index()+match.Length();
               //--- add all matched capture groups to the list.
               int j=match.Groups().Count();
               for(int i=1; i<match.Groups().Count(); i++)
                 {
                  if(match.IsMatched(i))
                    {
                     al.Add(match.Groups()[i].ToString());
                    }
                 }
               if(--count==0)
                 {
                  break;
                 }
               CMatch *new_match=match.NextMatch();
               delete match;
               match=new_match;
               if(!match.Success())
                 {
                  break;
                 }
              }
            al.Add(StringSubstr(in,prevat,StringLen(in)-prevat));
           }
         else
           {
            int prevat=StringLen(in);
            for(;;)
              {
               al.Add(StringSubstr(in,match.Index()+match.Length(),prevat-match.Index()-match.Length()));
               prevat=match.Index();
               //--- add all matched capture groups to the list.
               for(int i=1; i<match.Groups().Count(); i++)
                 {
                  if(match.IsMatched(i))
                    {
                     al.Add(match.Groups()[i].ToString());
                    }
                 }
               if(--count==0)
                 {
                  break;
                 }

               match=match.NextMatch();
               if(!match.Success())
                 {
                  break;
                 }
              }
            al.Add(StringSubstr(in,0,prevat));
            al.Reverse(0,al.Count());
           }
         al.CopyTo(result);
        }
      if(CheckPointer(match)==POINTER_DYNAMIC)
        {
         delete match;
        }
     }
  };
//+------------------------------------------------------------------+
