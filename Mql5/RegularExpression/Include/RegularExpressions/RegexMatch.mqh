//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
class CGroupCollection;
class CMatch;
#include "RegexGroup.mqh"
#include "RegexGroupCollections.mqh"
#include "RegexParser.mqh"
#include "Regex.mqh"
//+------------------------------------------------------------------+
//| Purpose: Represents the results from a single regular expression |
//| match.                                                           |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| CMatch is the result class for a regex search. It returns the    |
//| location, length, and substring for the entire match as well as  |
//| every captured group.                                            |
//|                                                                  |
//| CMatch is also used during the search to keep track of each      |
//| capture for each group. This is done using the "m_matches"       |
//| array. m_matches[x] represents an array of the captures for group|
//| x. This array consists of start and length pairs, and may have   |
//| empty entries at the end. m_matchcount[x] stores how many        |
//| captures a group has. Note that m_matchcount[x]*2 is the length  |
//| of all the valid values in m_matches.  m_matchcount[x]*2-2 is the|
//| Start of the last capture, and m_matchcount[x]*2-1 is the Length |
//| of the last capture.                                             |
//|                                                                  |
//| For example, if group 2 has one capture starting at position 4   |
//| with length 6:                                                   |
//|                                                                  | 
//| m_matchcount[2] == 1;                                            |
//| m_matches[2][0] == 4;                                            |
//| m_matches[2][1] == 6;                                            |
//|                                                                  |
//| Values in the _matches array can also be negative. This happens  |
//| when using the balanced match construct, "(?<start-end>...)".    |
//| When the "end" group matches, a capture is added for both the    |
//| "start" and "end" groups. The capture added for "start" receives |
//| the negative values, and these values point to the next capture  |
//| to be balanced. They do NOT point to the capture that "end" just |
//| balanced out. The negative values are indices into the _matches  |
//| array transformed by the formula -3-x. This formula also         |
//| untransforms.                                                    |
//+------------------------------------------------------------------+
class CMatch : public CGroup
  {
protected:
   CGroupCollection *m_groupcoll;
   CRegex            *m_regex;
   int               m_textbeg;
   int               m_textpos;
   int               m_textend;
   int               m_textstart;
   int               m_matchcount[];
   int               m_posMatche;
   bool              m_balancing;
   CMatrix<int>m_matches;
public:
   //+------------------------------------------------------------------+
   //| Returns an empty CMatch object.                                  |
   //+------------------------------------------------------------------+
   static CMatch *Empty()
     {
      static CMatch empty(NULL,1,NULL,0,0,0);
      return (GetPointer(empty));
     }
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CMatch(CRegex *regex,const int capcount,const string text,const int begpos,const int len,const int startpos)
   : CGroup(text,0)
     {
      m_regex=regex;
      ArrayResize(m_matchcount,capcount);
      ZeroMemory(m_matchcount);
      m_matches.Resize(capcount);
      m_matches[0] = m_caps;
      m_textbeg    = begpos;
      m_textend    = begpos + len;
      m_textstart  = startpos;
      m_balancing  = false;
      //--- No need for an exception here. 
      if(m_textbeg<0 || m_textstart<m_textbeg || m_textend<m_textstart || StringLen(m_text)<m_textend)
        {
#ifdef _DEBUG
         Print("The parameters are out of range.");
         DebugBreak();
#endif
        }
     }
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~CMatch()
     {
      if(CheckPointer(m_groupcoll)==POINTER_DYNAMIC)
        {
         delete m_groupcoll;
        }
     }
   //+------------------------------------------------------------------+
   //| TextPos.                                                         |
   //+------------------------------------------------------------------+
   int TextPos()
     {
      return (m_textpos);
     }
   //+------------------------------------------------------------------+
   //| Get current Regex.                                               |
   //+------------------------------------------------------------------+
   CRegex *Regex()
     {
      return (m_regex);
     }
   //+------------------------------------------------------------------+
   //| Get matches count array.                                         |
   //+------------------------------------------------------------------+
   void GetMatchCount(int &array[])
     {
      ArrayCopy(array,m_matchcount);
     }
   //+------------------------------------------------------------------+
   //| Get match at index.                                              |
   //+------------------------------------------------------------------+
   void GetMatch(int &array[],const int index)
     {
      CRow<int>::Copy(array,m_matches[index]);
     }
   //+------------------------------------------------------------------+
   //| Nonpublic set-text method.                                       |
   //+------------------------------------------------------------------+
   virtual void Reset(CRegex *regex,string text,const int textbeg,const int textend,const int textstart)
     {
      m_regex= regex;
      m_text = text;
      m_textbeg = textbeg;
      m_textend = textend;
      m_textstart=textstart;
      for(int i=0; i<ArraySize(m_matchcount); i++)
        {
         m_matchcount[i]=0;
        }
      m_balancing=false;
     }
   //+------------------------------------------------------------------+
   //| Gets a collection of groups corresponding to the regular         |
   //| expression.                                                      |
   //+------------------------------------------------------------------+
   virtual CGroupCollection *Groups()
     {
      if(m_groupcoll==NULL)
        {
         m_groupcoll=new CGroupCollection(GetPointer(this),NULL);
        }
      //--- return collection of groups 
      return (m_groupcoll);
     }
   //+------------------------------------------------------------------+
   //| Returns the next match.                                          |
   //+------------------------------------------------------------------+
   CMatch *NextMatch()
     {
      if(m_regex==NULL)
        {
         return (GetPointer(this));
        }
      return (m_regex.Run(false, m_length, m_text, m_textbeg, m_textend - m_textbeg, m_textpos));
     }
   //+------------------------------------------------------------------+
   //| Return the result string (using the replacement pattern).        |
   //+------------------------------------------------------------------+
   virtual string Result(const string replacement)
     {
      CRegexReplacement *repl;
      if(replacement==NULL)
        {
         Print("Argument 'replacement' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      if(m_regex==NULL)
        {
         Print("Not supported action."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      repl=(CRegexReplacement*)m_regex.ReplacementReference().Get();
      if(repl==NULL || !(repl.Pattern()==replacement))
        {
         repl=CRegexParser::ParseReplacement(replacement,m_regex.Caps(),m_regex.CapSize(),m_regex.CapNames(),m_regex.Options());
         m_regex.ReplacementReference().Set(repl);
        }
      return (repl.Replacement(GetPointer(this)));
     }
   //+------------------------------------------------------------------+
   //| Used by the replacement code.                                    |
   //+------------------------------------------------------------------+
   virtual string GroupToStringImpl(const int groupnum)
     {
      int c= m_matchcount[groupnum];
      if(c == 0)
        {
         return (NULL);
        }
      int matches[];
      GetMatch(matches,groupnum);
      return StringSubstr(m_text,matches[(c - 1) * 2], matches[(c * 2) - 1]);
     }
   //+------------------------------------------------------------------+
   //| Used by the replacement code.                                    |
   //+------------------------------------------------------------------+
   string LastGroupToStringImpl()
     {
      return GroupToStringImpl(ArraySize(m_matchcount) - 1);
     }
   //+------------------------------------------------------------------+
   //| Convert to a thread-safe object by precomputing cache contents.  |
   //+------------------------------------------------------------------+
public:
   static CMatch *Synchronized(CMatch *inner)
     {
      if(inner==NULL)
        {
         Print("Argument 'inner' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      int numgroups=ArraySize(inner.m_matchcount);
      //--- Populate all groups by looking at each one
      for(int i=0; i<numgroups; i++)
        {
         CGroup *group=inner.Groups()[i];
        }
      return (inner);
     }
   //+------------------------------------------------------------------+
   //| Add a capture to the group specified by "cap".                   |
   //+------------------------------------------------------------------+
   virtual void AddMatch(const int cap,const int start,const int len)
     {
      int capcount=0;
      if(m_matches[cap].Size()<=0)
        {
         m_matches[cap].Resize(2);
        }
      capcount=m_matchcount[cap];
      if(capcount*2+2>m_matches[cap].Size())
        {
         int oldmatches[];
         GetMatch(oldmatches,cap);
         int newmatches[];
         ArrayResize(newmatches,capcount*8);
         for(int j=0; j<capcount*2; j++)
           {
            newmatches[j]=oldmatches[j];
           }
         m_matches[cap]=newmatches;
        }
      m_matches[cap].Set(capcount*2,start);
      m_matches[cap].Set(capcount*2+1,len);
      m_caps[0]=m_matches[0][0];
      m_caps[1]=m_matches[0][1];
      m_matchcount[cap]=capcount+1;
     }
   //+------------------------------------------------------------------+
   //| Add a capture to balance the specified group.  This is used by   |
   //| the balanced match construct. (?<foo-foo2>...)                   |
   //|                                                                  |
   //| If there were no such thing as backtracking, this would be as    |
   //| simple as calling RemoveMatch(cap). However, since we have       |
   //| backtracking, we need to keep track of everything.               | 
   //+------------------------------------------------------------------+
   virtual void BalanceMatch(const int cap)
     {
      int capcount;
      int target;
      m_balancing=true;
      //--- we'll look at the last capture first
      capcount=m_matchcount[cap];
      target=capcount*2-2;
      //--- first see if it is negative, and therefore is a reference to the next available
      //--- capture group for balancing.  If it is, we'll reset target to point to that capture.
      if(m_matches[cap][target]<0)
        {
         target=-3-m_matches[cap][target];
        }
      //--- move back to the previous capture
      target-=2;
      //--- if the previous capture is a reference, just copy that reference to the end.  Otherwise, point to it. 
      if(target>=0 && m_matches[cap][target]<0)
        {
         AddMatch(cap,m_matches[cap][target],m_matches[cap][target+1]);
        }
      else
        {
         AddMatch(cap,-3-target,-4-target);
        }
     }
   //+------------------------------------------------------------------+
   //| Removes a group match by capnum.                                 |
   //+------------------------------------------------------------------+
   virtual void RemoveMatch(const int cap)
     {
      m_matchcount[cap]--;
     }
   //+------------------------------------------------------------------+
   //| Tells if a group was matched by capnum.                          |
   //+------------------------------------------------------------------+
   virtual bool IsMatched(const int cap)
     {
      return (cap < ArraySize(m_matchcount) && m_matchcount[cap] > 0 && m_matches[cap][m_matchcount[cap] * 2 - 1] != (-3 + 1));
     }
   //+------------------------------------------------------------------+
   //| Returns the index of the last specified matched group by capnum. |
   //+------------------------------------------------------------------+
   virtual int MatchIndex(const int cap)
     {
      int i = m_matches[cap][m_matchcount[cap] * 2 - 2];
      if(i >= 0)
        {
         return (i);
        }
      return (m_matches[cap][-3 - i]);
     }
   //+------------------------------------------------------------------+
   //| Returns the length of the last specified matched group by capnum.|
   //+------------------------------------------------------------------+
   virtual int MatchLength(const int cap)
     {
      int i = m_matches[cap][m_matchcount[cap] * 2 - 1];
      if(i >= 0)
        {
         return (i);
        }
      return (m_matches[cap][-3 - i]);
     }
   //+------------------------------------------------------------------+
   //| Tidy the match so that it can be used as an immutable result.    |
   //+------------------------------------------------------------------+
   virtual void Tidy(const int textpos)
     {
      int interval[];
      GetMatch(interval,0);
      m_index=interval[0];
      m_length   = interval[1];
      m_textpos  = textpos;
      m_capcount = m_matchcount[0];
      if(m_balancing)
        {
         //--- The idea here is that we want to compact all of our unbalanced captures.  To do that we
         //--- use j basically as a count of how many unbalanced captures we have at any given time 
         //--- (really j is an index, but j/2 is the count).  First we skip past all of the real captures
         //--- until we find a balance captures.  Then we check each subsequent entry.  If it's a balance
         //--- capture (it's negative), we decrement j.  If it's a real capture, we increment j and copy 
         //--- it down to the last free position. 
         for(int cap=0; cap<ArraySize(m_matchcount); cap++)
           {
            int limit;
            int matcharray[];
            limit=m_matchcount[cap]*2;
            GetMatch(matcharray,cap);
            int i=0;
            int j;
            for(i=0; i<limit; i++)
              {
               if(matcharray[i]<0)
                 {
                  break;
                 }
              }
            for(j=i; i<limit; i++)
              {
               if(matcharray[i]<0)
                 {
                  //--- skip negative values
                  j--;
                 }
               else
                 {
                  //--- but if we find something positive (an actual capture), copy it back to the last 
                  //--- unbalanced position. 
                  if(i!=j)
                    {
                     matcharray[j]=matcharray[i];
                    }
                  j++;
                 }
              }
            m_matchcount[cap]=j/2;
           }
         m_balancing=false;
        }
     }
#ifdef _DEBUG
   //+------------------------------------------------------------------+
   //| Debug.                                                           |
   //+------------------------------------------------------------------+
   bool Debug()
     {
      if(m_regex==NULL)
        {
         return (false);
        }
      return (m_regex.Debug());
     }
   //+------------------------------------------------------------------+
   //| Dump.                                                            |
   //+------------------------------------------------------------------+
   virtual void Dump()
     {
      int i,j;
      for(i=0; i<ArraySize(m_matchcount); i++)
        {
         Print("Capnum "+IntegerToString(i)+":");
         for(j=0; j<m_matchcount[i]; j++)
           {
            string text="";
            if(m_matches[i][j*2]>=0)
              {
               text=StringSubstr(m_text,m_matches[i][j*2],m_matches[i][j*2+1]);
              }
            Print("  ("+(string)m_matches[i][j*2]+","+(string)m_matches[i][j*2+1]+") "+text);
           }
        }
     }
#endif
  };
//+------------------------------------------------------------------+
//| Purpose: MatchSparse is for handling the case where slots are    |
//| sparsely arranged (e.g., if somebody says use slot 100000).      |
//+------------------------------------------------------------------+
class MatchSparse : public CMatch
  {
private:
   Dictionary<int,int>*m_dictionaryCaps;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     MatchSparse(CRegex *regex,Dictionary<int,int>*caps,const int capcount,
                                                   const string text,const int begpos,const int len,const int startpos)
   : CMatch(regex,capcount,text,begpos,len,startpos)
     {
      m_dictionaryCaps=caps;
     }
   //+------------------------------------------------------------------+
   //| Get groups.                                                      |
   //+------------------------------------------------------------------+
   CGroupCollection *Groups()
     {
      if(m_groupcoll==NULL)
        {
         m_groupcoll=new CGroupCollection(GetPointer(this),m_dictionaryCaps);
        }
      //--- return groups
      return (m_groupcoll);
     }
#ifdef _DEBUG
   //+------------------------------------------------------------------+
   //| Dump.                                                            |
   //+------------------------------------------------------------------+
   void Dump()
     {
      if(m_dictionaryCaps!=NULL)
        {
         IEnumerator<int>*e=m_dictionaryCaps.Keys().GetEnumerator();
         while(e.MoveNext())
           {
            Print("Slot "+IntegerToString(e.Current())+" -> "+IntegerToString(m_caps[e.Current()]));
           }
         delete e;
        }
      CMatch::Dump();
     }
#endif
  };
//+------------------------------------------------------------------+
