//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
class CRegexPrefix;
#include <Internal\Generic\Dictionary.mqh>
#include <Internal\Generic\List.mqh>
#include "RegexBoyerMoore.mqh"
#include "RegexCharClass.mqh"
#include "RegexFCD.mqh"
//+------------------------------------------------------------------+
//| Purpose: It provides operator constants for use by the Builder   |
//| and the Machine.                                                 |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Regexps are built into RegexCodes, which contain an operation    |
//| array, a string table, and some constants.                       |
//|                                                                  |
//| Each operation is one of the codes below, followed by the integer|
//| operands specified for each op.                                  |
//|                                                                  |
//| Strings and sets are indices into a string table.                |
//+------------------------------------------------------------------+
class CRegexCode
  {
private:
   //--- The code
   int               m_codes[];       // the code
   string            m_strings[];     // the string/set table
   int               m_trackcount;    // how many instructions use backtracking
   Dictionary<int,int>*m_caps;        // mapping of user group numbers -> impl group slots
   int               m_capsize;       // number of impl group slots
   CRegexPrefix      *m_fcPrefix;      // the set of candidate first characters (may be null)
   CRegexBoyerMoore *m_bmPrefix;       // the fixed prefix string as a Boyer-Moore machine (may be null)
   int               m_anchors;       // the set of zero-length start anchors (CRegexFCD.Bol, etc)
   bool              m_rightToLeft;   // true if right to left
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CRegexCode(int &codes[],List<string>*stringlist,const int trackcount,
                                                 Dictionary<int,int>*caps,const int capsize,
                                                 CRegexBoyerMoore *bmPrefix,CRegexPrefix *fcPrefix,
                                                 const int anchors,const bool rightToLeft)
     {
      ArrayCopy(m_codes,codes);
      ArrayResize(m_strings,stringlist.Count());
      m_trackcount=trackcount;
      m_caps=caps;
      m_capsize=capsize;
      m_bmPrefix = bmPrefix;
      m_fcPrefix = fcPrefix;
      m_anchors=anchors;
      m_rightToLeft=rightToLeft;
      stringlist.CopyTo(0,m_strings,0,stringlist.Count());
     }
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~CRegexCode()
     {
      if(CheckPointer(m_caps)==POINTER_DYNAMIC)
        {
         delete m_caps;
        }
      if(CheckPointer(m_fcPrefix)==POINTER_DYNAMIC)
        {
         delete m_fcPrefix;
        }
      if(CheckPointer(m_bmPrefix)==POINTER_DYNAMIC)
        {
         delete m_bmPrefix;
        }
     }
   //+------------------------------------------------------------------+
   //| RightToLeft.                                                     |
   //+------------------------------------------------------------------+
   bool RightToLeft()
     {
      return (m_rightToLeft);
     }
   //+------------------------------------------------------------------+
   //| TrackCount.                                                      |
   //+------------------------------------------------------------------+
   int TrackCount()
     {
      return (m_trackcount);
     }
   //+------------------------------------------------------------------+
   //| Anchors.                                                         |
   //+------------------------------------------------------------------+
   int Anchors()
     {
      return (m_anchors);
     }
   //+------------------------------------------------------------------+
   //| BMPrefix.                                                        |
   //+------------------------------------------------------------------+
   CRegexBoyerMoore *BMPrefix()
     {
      return (m_bmPrefix);
     }
   //+------------------------------------------------------------------+
   //| FCPrefix.                                                        |
   //+------------------------------------------------------------------+
   CRegexPrefix *FCPrefix()
     {
      return (m_fcPrefix);
     }
   //+------------------------------------------------------------------+
   //| GetStrings.                                                      |
   //+------------------------------------------------------------------+
   void GetStrings(string &array[])
     {
      ArrayCopy(array,m_strings);
     }
   //+------------------------------------------------------------------+
   //| GetCodes.                                                        |
   //+------------------------------------------------------------------+
   void GetCodes(int &array[])
     {
      ArrayCopy(array,m_codes);
     }
   //+------------------------------------------------------------------+
   //| Caps.                                                            |
   //+------------------------------------------------------------------+
   Dictionary<int,int>*Caps()
     {
      return (m_caps);
     }
   //+------------------------------------------------------------------+
   //| CapSize.                                                         |
   //+------------------------------------------------------------------+
   int CapSize()
     {
      return (m_capsize);
     }
   //+------------------------------------------------------------------+
   //| OpcodeBacktracks.                                                |
   //+------------------------------------------------------------------+
   static bool OpcodeBacktracks(int Op)
     {
      Op &=Mask;
      switch(Op)
        {
         case Oneloop:
         case Notoneloop:
         case Setloop:
         case Onelazy:
         case Notonelazy:
         case Setlazy:
         case Lazybranch:
         case Branchmark:
         case Lazybranchmark:
         case Nullcount:
         case Setcount:
         case Branchcount:
         case Lazybranchcount:
         case Setmark:
         case Capturemark:
         case Getmark:
         case Setjump:
         case Backjump:
         case Forejump:
         case Goto:
           {
            return (true);
           }
         default:
           {
            return (false);
           }
        }
     }
   //+------------------------------------------------------------------+
   //| OpcodeSize.                                                      |
   //+------------------------------------------------------------------+
   static int OpcodeSize(int Opcode)
     {
      Opcode &=Mask;
      switch(Opcode)
        {
         case Nothing:
         case Bol:
         case Eol:
         case Boundary:
         case Nonboundary:
         case ECMABoundary:
         case NonECMABoundary:
         case Beginning:
         case Start:
         case EndZ:
         case End:
         case Nullmark:
         case Setmark:
         case Getmark:
         case Setjump:
         case Backjump:
         case Forejump:
         case Stop:
           {
            return (1);
           }
         case One:
         case Notone:
         case Multi:
         case Ref:
         case Testrefstruct:
         case Goto:
         case Nullcount:
         case Setcount:
         case Lazybranch:
         case Branchmark:
         case Lazybranchmark:
         case Prune:
         case Set:
           {
            return (2);
           }
         case Capturemark:
         case Branchcount:
         case Lazybranchcount:
         case Onerep:
         case Notonerep:
         case Oneloop:
         case Notoneloop:
         case Onelazy:
         case Notonelazy:
         case Setlazy:
         case Setrep:
         case Setloop:
           {
            return (3);
           }
         default:
           {
            Print("Internal Error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
            return (-1);
           }
        }
     }
#ifdef _DEBUG
   static const string CodeStr[];
   //+------------------------------------------------------------------+
   //| OperatorDescription.                                             |
   //+------------------------------------------------------------------+
   static string OperatorDescription(const int Opcode)
     {
      bool isCi   = ((Opcode & Ci) != 0);
      bool isRtl  = ((Opcode & Rtl) != 0);
      bool isBack = ((Opcode & Back) != 0);
      bool isBack2=((Opcode & Back2) != 0);
      return (CodeStr[Opcode & Mask] + (isCi ? "-Ci" : "")+(isRtl ? "-Rtl" : "")+(isBack ? "-Back" : "")+(isBack2 ? "-Back2" : ""));
     }
   //+------------------------------------------------------------------+
   //| OpcodeDescription.                                               |
   //+------------------------------------------------------------------+
   string OpcodeDescription(const int offset)
     {
      string text;
      int opcode=m_codes[offset];
      text+=StringFormat("{0:D6} ", offset);
      text+=(OpcodeBacktracks(opcode  &Mask) ? CharToString('*') : CharToString(' '));
      text+=(OperatorDescription(opcode));
      text+=CharToString('(');
      opcode &=Mask;
      switch(opcode)
        {
         case One:
         case Notone:
         case Onerep:
         case Notonerep:
         case Oneloop:
         case Notoneloop:
         case Onelazy:
         case Notonelazy:
           {
            text+=("Ch = ");
            text+=(CRegexCharClass::CharDescription((char)m_codes[offset+1]));
            break;
           }
         case Set:
         case Setrep:
         case Setloop:
         case Setlazy:
           {
            text+=("Set = ");
            text+=(CRegexCharClass::SetDescription(m_strings[m_codes[offset+1]]));
            break;
           }
         case Multi:
           {
            text+=("String = ");
            text+=(m_strings[m_codes[offset+1]]);
            break;
           }
         case Ref:
         case Testrefstruct:
           {
            text+=("Index = ");
            text+=IntegerToString(m_codes[offset+1]);
            break;
           }
         case Capturemark:
           {
            text+=("Index = ");
            text+=IntegerToString(m_codes[offset+1]);
            if(m_codes[offset+2]!=-1)
              {
               text+=(", Unindex = ");
               text+=IntegerToString(m_codes[offset+2]);
              }
            break;
           }
         case Nullcount:
         case Setcount:
           {
            text+=("Value = ");
            text+=IntegerToString(m_codes[offset+1]);
            break;
           }
         case Goto:
         case Lazybranch:
         case Branchmark:
         case Lazybranchmark:
         case Branchcount:
         case Lazybranchcount:
           {
            text+=("Addr = ");
            text+=IntegerToString(m_codes[offset+1]);
            break;
           }
        }
      switch(opcode)
        {
         case Onerep:
         case Notonerep:
         case Oneloop:
         case Notoneloop:
         case Onelazy:
         case Notonelazy:
         case Setrep:
         case Setloop:
         case Setlazy:
           {
            text+=(", Rep = ");
            if(m_codes[offset+2]==Int32::MaxValue)
              {
               text+=("inf");
              }
            else
              {
               text+=IntegerToString(m_codes[offset+2]);
              }
            break;
           }
         case Branchcount:
         case Lazybranchcount:
            text+=(", Limit = ");
            if(m_codes[offset+2]==Int32::MaxValue)
              {
               text+=("inf");
              }
            else
              {
               text+=IntegerToString(m_codes[offset+2]);
              }
            break;
        }
      text+=(")");
      return (text);
     }
   //+------------------------------------------------------------------+
   //| Dump.                                                            |
   //+------------------------------------------------------------------+
   void Dump()
     {
      int i;
      Print("Direction:  "+(m_rightToLeft ? "right-to-left" : "left-to-right"));
      Print("Firstchars: " + (m_fcPrefix == NULL ? "n/a" : CRegexCharClass::SetDescription(m_fcPrefix.Prefix())));
      Print("Prefix:     " + (m_bmPrefix == NULL ? "n/a" : CRegex::Escape(m_bmPrefix.ToString())));
      Print("Anchors:    "+CRegexFCD::AnchorDescription(m_anchors));
      Print("");
      if(m_bmPrefix!=NULL)
        {
         Print("BoyerMoore:");
         Print(m_bmPrefix.Dump("    "));
        }
      for(i=0; i<ArraySize(m_codes);)
        {
         Print(OpcodeDescription(i));
         i+=OpcodeSize(m_codes[i]);
        }
      Print("");
     }
#endif
  };
#ifdef _DEBUG
static const string CRegexCode::CodeStr[]=
  {
   "Onerep","Notonerep","Setrep",
   "Oneloop","Notoneloop","Setloop",
   "Onelazy","Notonelazy","Setlazy",
   "One","Notone","Set",
   "Multi","Ref",
   "Bol","Eol","Boundary","Nonboundary","Beginning","Start","EndZ","End",
   "Nothing",
   "Lazybranch","Branchmark","Lazybranchmark",
   "Nullcount","Setcount","Branchcount","Lazybranchcount",
   "Nullmark","Setmark","Capturemark","Getmark",
   "Setjump","Backjump","Forejump","Testref","Goto",
   "Prune","Stop",
  };
#endif   
//+------------------------------------------------------------------+
//| The following primitive operations come directly from the parser.|
//+------------------------------------------------------------------+
enum PRIMITIVE_OPERATIONS
  {
   //--- lef/back operands     
   Onerep         =0,                 // lef,back char,min,max    a {n}
   Notonerep      =1,                 // lef,back char,min,max    .{n}
   Setrep         =2,                 // lef,back set,min,max     [\d]{n}
   Oneloop        =3,                 // lef,back char,min,max    a {,n}
   Notoneloop     =4,                 // lef,back char,min,max    .{,n}
   Setloop        =5,                 // lef,back set,min,max     [\d]{,n}
   Onelazy        =6,                 // lef,back char,min,max    a {,n}?
   Notonelazy     =7,                 // lef,back char,min,max    .{,n}?
   Setlazy        =8,                 // lef,back set,min,max     [\d]{,n}?
   One            =9,                 // lef      char            a
   Notone         =10,                // lef      char            [^a]
   Set            =11,                // lef      set             [a-z\s]  \w \s \d
   Multi          =12,                // lef      string          abcd
   Ref            =13,                // lef      group           \#
   Bol            =14,                //                          ^
   Eol            =15,                //                          $
   Boundary       =16,                //                          \b
   Nonboundary    =17,                //                          \B
   Beginning      =18,                //                          \A
   Start          =19,                //                          \G
   EndZ           =20,                //                          \Z
   End            =21,                //                          \Z
   Nothing        =22,                //                          Reject!
  };
//+------------------------------------------------------------------+
//| Primitive control structures.                                    |
//+------------------------------------------------------------------+  
enum PRIMITIVE_CONTROL_STRUCTURES
  {
   //--- lef/back operands                               
   Lazybranch     =23,                // back     jump            straight first
   Branchmark     =24,                // back     jump            branch first for loop
   Lazybranchmark =25,                // back     jump            straight first for loop
   Nullcount      =26,                // back     val             set counter, null mark
   Setcount       =27,                // back     val             set counter, make mark
   Branchcount    =28,                // back     jump,limit      branch++ if zero<=c<limit
   Lazybranchcount=29,                // back     jump,limit      same, but straight first
   Nullmark       =30,                // back                     save position
   Setmark        =31,                // back                     save position
   Capturemark    =32,                // back     group           define group
   Getmark        =33,                // back                     recall position
   Setjump        =34,                // back                     save backtrack state
   Backjump       =35,                //                          zap back to saved state
   Forejump       =36,                //                          zap backtracking state
   Testrefstruct  =37,                //                          backtrack if ref undefined
   Goto           =38,                //          jump            just go
   Prune          =39,                //                          prune it baby
   Stop           =40,                //                          done!
   ECMABoundary   =41,                //                           \b
   NonECMABoundary=42,                //                          \B
  };
//+------------------------------------------------------------------+
//| Modifiers for alternate modes.                                   |
//+------------------------------------------------------------------+
enum MODIFIERS_FOR_ALTERNATE_MODES
  {
   Mask  =63,                         // Mask to get unmodified ordinary operator
   Rtl   =64,                         // bit to indicate that we're reverse scanning.
   Back =128,                         // bit to indicate that we're backtracking.
   Back2=256,                         // bit to indicate that we're backtracking on a second branch.
   Ci   =512,                         // bit to indicate that we're case-insensitive.  
  };
//+------------------------------------------------------------------+
