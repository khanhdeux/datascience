//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
#include "Regex.mqh"
#include "RegexRunner.mqh"
//+------------------------------------------------------------------+
//| Purpose: This CRegexInterpreter class is internal to the         |
//| RegularExpression package. It executes a block of regular        |
//| expression codes while consuming input.                          |
//+------------------------------------------------------------------+
class CRegexInterpreter : public CRegexRunner
  {
private:
   int               m_runoperator;
   int               m_runcodes[];
   int               m_runcodepos;
   string            m_runstrings[];
   CRegexCode        *m_runcode;
   CRegexPrefix      *m_runfcPrefix;
   CRegexBoyerMoore *m_runbmPrefix;
   int               m_runanchors;
   bool              m_runrtl;
   bool              m_runci;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CRegexInterpreter(CRegexCode *code)
     {
      m_runcode=code;
      code.GetCodes(m_runcodes);
      code.GetStrings(m_runstrings);
      m_runfcPrefix   = code.FCPrefix();
      m_runbmPrefix   = code.BMPrefix();
      m_runanchors    = code.Anchors();
     }
   //+------------------------------------------------------------------+
   //| InitTrackCount.                                                  |
   //+------------------------------------------------------------------+
   void InitTrackCount()
     {
      m_runtrackcount=m_runcode.TrackCount();
     }
private:
   //+------------------------------------------------------------------+
   //| Advance.                                                         |
   //+------------------------------------------------------------------+
   void Advance()
     {
      Advance(0);
     }
   //+------------------------------------------------------------------+
   //| Advance.                                                         |
   //+------------------------------------------------------------------+
   void Advance(const int i)
     {
      m_runcodepos+=(i+1);
      SetOperator(m_runcodes[m_runcodepos]);
     }
   //+------------------------------------------------------------------+
   //| Goto.                                                            |
   //+------------------------------------------------------------------+
   void Goto(const int newpos)
     {
      //--- when branching backward, ensure storage
      if(newpos<m_runcodepos)
        {
         EnsureStorage();
        }
      SetOperator(m_runcodes[newpos]);
      m_runcodepos=newpos;
     }
   //+------------------------------------------------------------------+
   //| Textto.                                                          |
   //+------------------------------------------------------------------+
   void Textto(int newpos)
     {
      m_runtextpos=newpos;
     }
   //+------------------------------------------------------------------+
   //| Trackto.                                                         |
   //+------------------------------------------------------------------+
   void Trackto(const int newpos)
     {
      m_runtrackpos=ArraySize(m_runtrack)-newpos;
     }
   //+------------------------------------------------------------------+
   //| Textstart.                                                       |
   //+------------------------------------------------------------------+
   int Textstart()
     {
      //--- return result
      return (m_runtextstart);
     }
   //+------------------------------------------------------------------+
   //| Textpos.                                                         |
   //+------------------------------------------------------------------+
   int Textpos()
     {
      //--- return result
      return (m_runtextpos);
     }
   //+------------------------------------------------------------------+
   //| Push onto the backtracking stack.                                |
   //+------------------------------------------------------------------+
   int Trackpos()
     {
      //--- return result
      return (ArraySize(m_runtrack) - m_runtrackpos);
     }
   //+------------------------------------------------------------------+
   //| TrackPush.                                                       |
   //+------------------------------------------------------------------+
   void TrackPush()
     {
      m_runtrack[--m_runtrackpos]=m_runcodepos;
     }
   //+------------------------------------------------------------------+
   //| TrackPush.                                                       |
   //+------------------------------------------------------------------+
   void TrackPush(const int I1)
     {
      m_runtrack[--m_runtrackpos] = I1;
      m_runtrack[--m_runtrackpos] = m_runcodepos;
     }
   //+------------------------------------------------------------------+
   //| TrackPush.                                                       |
   //+------------------------------------------------------------------+
   void TrackPush(const int I1,const int I2)
     {
      m_runtrack[--m_runtrackpos] = I1;
      m_runtrack[--m_runtrackpos] = I2;
      m_runtrack[--m_runtrackpos] = m_runcodepos;
     }
   //+------------------------------------------------------------------+
   //| TrackPush.                                                       |
   //+------------------------------------------------------------------+
   void TrackPush(const int I1,const int I2,const int I3)
     {
      m_runtrack[--m_runtrackpos] = I1;
      m_runtrack[--m_runtrackpos] = I2;
      m_runtrack[--m_runtrackpos] = I3;
      m_runtrack[--m_runtrackpos] = m_runcodepos;
     }
   //+------------------------------------------------------------------+
   //| TrackPush2.                                                      |
   //+------------------------------------------------------------------+
   void TrackPush2(const int I1)
     {
      m_runtrack[--m_runtrackpos] = I1;
      m_runtrack[--m_runtrackpos] = -m_runcodepos;
     }
   //+------------------------------------------------------------------+
   //| TrackPush2.                                                      |
   //+------------------------------------------------------------------+
   void TrackPush2(const int I1,const int I2)
     {
      m_runtrack[--m_runtrackpos] = I1;
      m_runtrack[--m_runtrackpos] = I2;
      m_runtrack[--m_runtrackpos] = -m_runcodepos;
     }
   //+------------------------------------------------------------------+
   //| Backtrack.                                                       |
   //+------------------------------------------------------------------+
   void Backtrack()
     {
      int newpos=m_runtrack[m_runtrackpos++];
#ifdef _DEBUG
      if(m_runmatch.Debug())
        {
         if(newpos<0)
           {
            Print("       Backtracking (back2) to code position "+IntegerToString(-newpos));
           }
         else
           {
            Print("       Backtracking to code position "+IntegerToString(newpos));
           }
        }
#endif
      if(newpos<0)
        {
         newpos=-newpos;
         SetOperator(m_runcodes[newpos]|Back2);
        }
      else
        {
         SetOperator(m_runcodes[newpos]|Back);
        }
      //--- When branching backward, ensure storage
      if(newpos<m_runcodepos)
        {
         EnsureStorage();
        }
      m_runcodepos=newpos;
     }
   //+------------------------------------------------------------------+
   //| SetOperator.                                                     |
   //+------------------------------------------------------------------+
   void SetOperator(const int op)
     {
      m_runci         = (0 != (op & Ci));
      m_runrtl        = (0 != (op & Rtl));
      m_runoperator   = op & ~(Rtl | Ci);
     }
   //+------------------------------------------------------------------+
   //| TrackPop.                                                        |
   //+------------------------------------------------------------------+
   void TrackPop()
     {
      m_runtrackpos++;
     }
   //+------------------------------------------------------------------+
   //| Pop framesize items from the backtracking stack.                 |
   //+------------------------------------------------------------------+ 
   void TrackPop(const int framesize)
     {
      m_runtrackpos+=framesize;
     }
   //+------------------------------------------------------------------+
   //| Technically we are actually peeking at items already popped. So  |
   //| if you want to get and pop the top item from the stack, you do:  |
   //| TrackPop();                                                      |
   //| TrackPeek();                                                     |
   //+------------------------------------------------------------------+
   int TrackPeek()
     {
      return (m_runtrack[m_runtrackpos - 1]);
     }
   //+------------------------------------------------------------------+
   //| Get the ith element down on the backtracking stack.              |
   //+------------------------------------------------------------------+
   int TrackPeek(const int i)
     {
      return (m_runtrack[m_runtrackpos - i - 1]);
     }
   //+------------------------------------------------------------------+
   //| Push onto the grouping stack.                                    |
   //+------------------------------------------------------------------+
   void StackPush(const int I1)
     {
      m_runstack[--m_runstackpos]=I1;
     }
   //+------------------------------------------------------------------+
   //| Push onto the grouping stack.                                    |
   //+------------------------------------------------------------------+
   void StackPush(const int I1,const int I2)
     {
      m_runstack[--m_runstackpos] = I1;
      m_runstack[--m_runstackpos] = I2;
     }
   //+------------------------------------------------------------------+
   //| Pop framesize items from the grouping stack.                     |
   //+------------------------------------------------------------------+
   void StackPop()
     {
      m_runstackpos++;
     }
   //+------------------------------------------------------------------+
   //| Pop framesize items from the grouping stack.                     |
   //+------------------------------------------------------------------+
   void StackPop(const int framesize)
     {
      m_runstackpos+=framesize;
     }
   //+------------------------------------------------------------------+
   //| Technically we are actually peeking at items already popped. So  |
   //| if you want to get and pop the top item from the stack, you do:  |
   //| StackPop();                                                      |
   //| StackPeek();                                                     |
   //+------------------------------------------------------------------+
   int StackPeek()
     {
      //--- return result
      return (m_runstack[m_runstackpos - 1]);
     }
   //+------------------------------------------------------------------+
   //| Get the ith element down on the grouping stack.                  |
   //+------------------------------------------------------------------+
   int StackPeek(const int i)
     {
      //--- return result
      return (m_runstack[m_runstackpos - i - 1]);
     }
   //+------------------------------------------------------------------+
   //| Operator.                                                        |
   //+------------------------------------------------------------------+
   int Operator()
     {
      return (m_runoperator);
     }
   //+------------------------------------------------------------------+
   //| Operand.                                                         |
   //+------------------------------------------------------------------+
   int Operand(const int i)
     {
      return (m_runcodes[m_runcodepos + i + 1]);
     }
   //+------------------------------------------------------------------+
   //| Leftchars.                                                       |
   //+------------------------------------------------------------------+
   int Leftchars()
     {
      return (m_runtextpos - m_runtextbeg);
     }
   //+------------------------------------------------------------------+
   //| Rightchars.                                                      |
   //+------------------------------------------------------------------+
   int Rightchars()
     {
      return (m_runtextend - m_runtextpos);
     }
   //+------------------------------------------------------------------+
   //| Bump.                                                            |
   //+------------------------------------------------------------------+
   int Bump()
     {
      return (m_runrtl ? -1 : 1);
     }
   //+------------------------------------------------------------------+
   //| Forwardchars.                                                    |
   //+------------------------------------------------------------------+
   int Forwardchars()
     {
      return (m_runrtl ? m_runtextpos - m_runtextbeg : m_runtextend - m_runtextpos);
     }
   //+------------------------------------------------------------------+
   //| Forwardcharnext.                                                 |
   //+------------------------------------------------------------------+
   ushort Forwardcharnext()
     {
      ushort ch=(m_runrtl ?StringGetCharacter(m_runtext,--m_runtextpos):StringGetCharacter(m_runtext,m_runtextpos++));
      string str=ShortToString(ch);
      StringToLower(str);
      return(m_runci ? StringGetCharacter(str,0) : ch);
     }
   //+------------------------------------------------------------------+
   //| Stringmatch.                                                     |
   //+------------------------------------------------------------------+
   bool Stringmatch(const string str)
     {
      int c;
      int pos;
      if(!m_runrtl)
        {
         if(m_runtextend-m_runtextpos<(c=StringLen(str)))
           {
            //--- return 
            return (false);
           }
         pos=m_runtextpos+c;
        }
      else
        {
         if(m_runtextpos-m_runtextbeg<(c=StringLen(str)))
           {
            //--- return 
            return (false);
           }
         pos=m_runtextpos;
        }
      if(!m_runci)
        {
         while(c!=0)
           {
            if(StringGetCharacter(str,--c)!=StringGetCharacter(m_runtext,--pos))
              {
               //--- return 
               return (false);
              }
           }
        }
      else
        {
         while(c!=0)
           {
            string runtextLow=m_runtext;
            StringToLower(runtextLow);
            if(StringGetCharacter(str,--c)!=StringGetCharacter(runtextLow,--pos))
              {
               //--- return 
               return (false);
              }
           }
        }
      if(!m_runrtl)
        {
         pos+=StringLen(str);
        }
      m_runtextpos=pos;
      //--- return 
      return (true);
     }
   //+------------------------------------------------------------------+
   //| Refmatch.                                                        |
   //+------------------------------------------------------------------+
   bool Refmatch(const int index,const int len)
     {
      int c;
      int pos;
      int cmpos;
      if(!m_runrtl)
        {
         if(m_runtextend-m_runtextpos<len)
           {
            //--- return 
            return (false);
           }
         pos=m_runtextpos+len;
        }
      else
        {
         if(m_runtextpos-m_runtextbeg<len)
           {
            //--- return 
            return (false);
           }
         pos=m_runtextpos;
        }
      cmpos=index+len;
      c=len;
      if(!m_runci)
        {
         while(c--!=0)
           {
            if(StringGetCharacter(m_runtext,--cmpos)!=StringGetCharacter(m_runtext,--pos))
              {
               //--- return 
               return (false);
              }
           }
        }
      else
        {
         while(c--!=0)
           {
            string runtextLow=m_runtext;
            StringToLower(runtextLow);
            if(StringGetCharacter(runtextLow,--cmpos)!=StringGetCharacter(runtextLow,--pos))
              {
               //--- return 
               return (false);
              }
           }
        }
      if(!m_runrtl)
        {
         pos+=len;
        }
      m_runtextpos=pos;
      //--- return 
      return (true);
     }
   //+------------------------------------------------------------------+
   //| Backwardnext.                                                    |
   //+------------------------------------------------------------------+
   void Backwardnext()
     {
      m_runtextpos+=m_runrtl ? 1 : -1;
     }
   //+------------------------------------------------------------------+
   //| CharAt.                                                          |
   //+------------------------------------------------------------------+
   ushort CharAt(const int j)
     {
      return (StringGetCharacter(m_runtext,j));
     }
protected:
   //+------------------------------------------------------------------+
   //| FindFirstChar.                                                   |
   //+------------------------------------------------------------------+
   bool FindFirstChar()
     {
      int i;
      string set;
      if(0!=(m_runanchors &(Beginning_PEG|Start_PEG|EndZ_PEG|End_PEG)))
        {
         if(!m_runcode.RightToLeft())
           {
            if((0!=(m_runanchors  &Beginning_PEG) && m_runtextpos>m_runtextbeg) ||
               (0!=(m_runanchors  &Start_PEG) && m_runtextpos>m_runtextstart))
              {
               m_runtextpos=m_runtextend;
               //--- return 
               return (false);
              }
            if(0!=(m_runanchors  &EndZ_PEG) && m_runtextpos<m_runtextend-1)
              {
               m_runtextpos=m_runtextend-1;
              }
            else if(0!=(m_runanchors  &End_PEG) && m_runtextpos<m_runtextend)
              {
               m_runtextpos=m_runtextend;
              }
           }
         else
           {
            if((0!=(m_runanchors  &End_PEG) && m_runtextpos<m_runtextend) ||
               (0!=(m_runanchors  &EndZ_PEG) && (m_runtextpos<m_runtextend-1 ||
               (m_runtextpos==m_runtextend-1 && CharAt(m_runtextpos)!='\n'))) || 
               (0!=(m_runanchors  &Start_PEG) && m_runtextpos<m_runtextstart))
              {
               m_runtextpos=m_runtextbeg;
               //--- return 
               return (false);
              }
            if(0!=(m_runanchors  &Beginning_PEG) && m_runtextpos>m_runtextbeg)
              {
               m_runtextpos=m_runtextbeg;
              }
           }
         if(m_runbmPrefix!=NULL)
           {
            //--- return 
            return (m_runbmPrefix.IsMatch(m_runtext, m_runtextpos, m_runtextbeg, m_runtextend));
           }
         //--- return 
         return(true); // found a valid start or end anchor
        }
      else if(m_runbmPrefix!=NULL)
        {
         m_runtextpos=m_runbmPrefix.Scan(m_runtext,m_runtextpos,m_runtextbeg,m_runtextend);
         if(m_runtextpos==-1)
           {
            m_runtextpos=(m_runcode.RightToLeft() ? m_runtextbeg : m_runtextend);
            //--- return result
            return (false);
           }
         //--- return 
         return (true);
        }
      else if(m_runfcPrefix==NULL)
        {
         //--- return 
         return (true);
        }
      m_runrtl= m_runcode.RightToLeft();
      m_runci = m_runfcPrefix.CaseInsensitive();
      set=m_runfcPrefix.Prefix();
      if(CRegexCharClass::IsSingleton(set))
        {
         ushort ch=CRegexCharClass::SingletonChar(set);
         for(i=Forwardchars(); i>0; i--)
           {
            if(ch==Forwardcharnext())
              {
               Backwardnext();
               //--- return 
               return (true);
              }
           }
        }
      else
        {
         for(i=Forwardchars(); i>0; i--)
           {
            if(CRegexCharClass::CharInClass(Forwardcharnext(),set))
              {
               Backwardnext();
               //--- return 
               return (true);
              }
           }
        }
      //--- return 
      return (false);
     }
   //+------------------------------------------------------------------+
   //| Go.                                                              |
   //+------------------------------------------------------------------+
   void Go()
     {
      Goto(0);
      for(;;)
        {
         bool BreakBackward=false;
#ifdef _DEBUG
         if(m_runmatch.Debug())
           {
            DumpState();
           }
#endif
         CheckTimeout();
         switch(Operator())
           {
            case Stop:
              {
               //--- return 
               return;
              }
            case Nothing:
              {
               break;
              }
            case PRIMITIVE_CONTROL_STRUCTURES::Goto:
              {
               Goto(Operand(0));
               continue;
              }
            case Testrefstruct:
              {
               if(!IsMatched(Operand(0)))
                 {
                  break;
                 }
               Advance(1);
               continue;
              }
            case Lazybranch:
              {
               TrackPush(Textpos());
               Advance(1);
               continue;
              }
            case Lazybranch|Back:
              {
               TrackPop();
               Textto(TrackPeek());
               Goto(Operand(0));
               continue;
              }
            case Setmark:
              {
               StackPush(Textpos());
               TrackPush();
               Advance();
               continue;
              }
            case Nullmark:
              {
               StackPush(-1);
               TrackPush();
               Advance();
               continue;
              }
            case Setmark|Back:
            case Nullmark|Back:
              {
               StackPop();
               break;
              }
            case Getmark:
              {
               StackPop();
               TrackPush(StackPeek());
               Textto(StackPeek());
               Advance();
               continue;
              }
            case Getmark|Back:
              {
               TrackPop();
               StackPush(TrackPeek());
               break;
              }
            case Capturemark:
              {
               if(Operand(1)!=-1 && !IsMatched(Operand(1)))
                 {
                  break;
                 }
               StackPop();
               if(Operand(1)!=-1)
                 {
                  TransferCapture(Operand(0),Operand(1),StackPeek(),Textpos());
                 }
               else
                 {
                  Capture(Operand(0),StackPeek(),Textpos());
                 }
               TrackPush(StackPeek());
               Advance(2);
               continue;
              }
            case Capturemark|Back:
              {
               TrackPop();
               StackPush(TrackPeek());
               Uncapture();
               if(Operand(0)!=-1 && Operand(1)!=-1)
                 {
                  Uncapture();
                 }
               break;
              }
            case Branchmark:
              {
               int matched;
               StackPop();
               matched=Textpos()-StackPeek();
               if(matched!=0)
                 {
                  //--- Nonempty match -> loop now
                  TrackPush(StackPeek(), Textpos());// Save old mark, textpos
                  StackPush(Textpos());             // Make new mark
                  Goto(Operand(0));                 // Loop
                 }
               else
                 {
                  //--- Empty match -> straight now
                  TrackPush2(StackPeek());          // Save old mark
                  Advance(1);                       // Straight
                 }
               continue;
              }
            case Branchmark|Back:
              {
               TrackPop(2);
               StackPop();
               Textto(TrackPeek(1));                // Recall position
               TrackPush2(TrackPeek());             // Save old mark
               Advance(1);                          // Straight
               continue;
              }
            case Branchmark|Back2:
              {
               TrackPop();
               StackPush(TrackPeek());              // Recall old mark
               break;                               // Backtrack
              }
            case Lazybranchmark:
              {
               //--- We hit this the first time through a lazy loop and after each 
               //--- successful match of the inner expression.  It simply continues
               //--- on and doesn't loop. 
               StackPop();
               int oldMarkPos=StackPeek();
               if(Textpos()!=oldMarkPos)
                 {
                  //--- Nonempty match -> try to loop again by going to 'back' state
                  if(oldMarkPos!=-1)
                    {
                     TrackPush(oldMarkPos,Textpos());// Save old mark, textpos
                    }
                  else
                    {
                     TrackPush(Textpos(),Textpos());
                    }
                 }
               else
                 {
                  //--- The inner expression found an empty match, so we'll go directly to 'back2' if we
                  //--- backtrack.  In this case, we need to push something on the stack, since back2 pops.
                  //--- However, in the case of ()+? or similar, this empty match may be legitimate, so push the text 
                  //--- position associated with that empty match.
                  StackPush(oldMarkPos);
                  TrackPush2(StackPeek());          // Save old mark
                 }
               Advance(1);
               continue;
              }
            case Lazybranchmark|Back:
              {
               //--- After the first time, Lazybranchmark | CRegexCode.Back occurs
               //--- with each iteration of the loop, and therefore with every attempted
               //--- match of the inner expression.  We'll try to match the inner expression, 
               //--- then go back to Lazybranchmark if successful.  If the inner expression 
               //--- failes, we go to Lazybranchmark | CRegexCode.Back2
               int pos;
               TrackPop(2);
               pos=TrackPeek(1);
               TrackPush2(TrackPeek());             // Save old mark
               StackPush(pos);                      // Make new mark
               Textto(pos);                         // Recall position
               Goto(Operand(0));                    // Loop
               continue;
              }
            case Lazybranchmark|Back2:
              {
               //--- The lazy loop has failed.  We'll do a true backtrack and 
               //--- start over before the lazy loop. 
               StackPop();
               TrackPop();
               StackPush(TrackPeek());              // Recall old mark
               break;
              }
            case Setcount:
              {
               StackPush(Textpos(),Operand(0));
               TrackPush();
               Advance(1);
               continue;
              }
            case Nullcount:
              {
               StackPush(-1,Operand(0));
               TrackPush();
               Advance(1);
               continue;
              }
            case Setcount|Back:
              {
               StackPop(2);
               break;
              }
            case Nullcount|Back:
              {
               StackPop(2);
               break;
              }
            case Branchcount:
              {
               //--- StackPush:
               //---  0: Mark
               //---  1: Count              
               StackPop(2);
               int mark=StackPeek();
               int count=StackPeek(1);
               int matched=Textpos()-mark;
               if(count>=Operand(1) || (matched==0 && count>=0))
                 {
                  //--- Max loops or empty match -> straight now
                  TrackPush2(mark, count);          // Save old mark, count
                  Advance(2);                       // Straight
                 }
               else
                 {
                  //--- Nonempty match -> count+loop now
                  TrackPush(mark);                  // remember mark
                  StackPush(Textpos(), count + 1);  // Make new mark, incr count
                  Goto(Operand(0));                 // Loop
                 }
               continue;
              }
            case Branchcount|Back:
              {
               //--- TrackPush:
               //---  0: Previous mark
               //--- StackPush:
               //---  0: Mark (= current pos, discarded)
               //---  1: Count
               TrackPop();
               StackPop(2);
               if(StackPeek(1)>0)
                 {
                  //--- Positive -> can go straight
                  Textto(StackPeek());                        // Zap to mark
                  TrackPush2(TrackPeek(), StackPeek(1) - 1);  // Save old mark, old count
                  Advance(2);                                 // Straight
                  continue;
                 }
               StackPush(TrackPeek(),StackPeek(1)-1);         // recall old mark, old count
               break;
              }
            case Branchcount|Back2:
              {
               //--- TrackPush:
               //---  0: Previous mark
               //---  1: Previous count
               TrackPop(2);
               StackPush(TrackPeek(), TrackPeek(1));           // Recall old mark, old count
               break;                                          // Backtrack
              }
            case Lazybranchcount:
              {
               //--- StackPush:
               //---  0: Mark
               //---  1: Count              
               StackPop(2);
               int mark=StackPeek();
               int count=StackPeek(1);
               if(count<0)
                 {
                  //--- Negative count -> loop now
                  TrackPush2(mark);                   // Save old mark
                  StackPush(Textpos(), count + 1);    // Make new mark, incr count
                  Goto(Operand(0));                   // Loop
                 }
               else
                 {
                  //--- Nonneg count -> straight now
                  TrackPush(mark, count, Textpos());  // Save mark, count, position
                  Advance(2);                         // Straight
                 }
               continue;
              }
            case Lazybranchcount|Back:
              {
               //--- TrackPush:
               //---  0: Mark
               //---  1: Count
               //---  2: Textpos             
               TrackPop(3);
               int mark=TrackPeek();
               int textpos=TrackPeek(2);
               if(TrackPeek(1)<Operand(1) && textpos!=mark)
                 {
                  //--- Under limit and not empty match -> loop
                  Textto(textpos);                            // Recall position
                  StackPush(textpos, TrackPeek(1) + 1);       // Make new mark, incr count
                  TrackPush2(mark);                           // Save old mark
                  Goto(Operand(0));                           // Loop
                  continue;
                 }
               else
                 {
                  //--- Max loops or empty match -> backtrack
                  StackPush(TrackPeek(), TrackPeek(1));       // Recall old mark, count
                  break;                                      // backtrack
                 }
              }
            case Lazybranchcount|Back2:
              {
               //--- TrackPush:
               //---  0: Previous mark
               //--- StackPush:
               //---  0: Mark (== current pos, discarded)
               //---  1: Count
               TrackPop();
               StackPop(2);
               StackPush(TrackPeek(), StackPeek(1) - 1);   // Recall old mark, count
               break;                                      // Backtrack
              }
            case Setjump:
              {
               StackPush(Trackpos(),Crawlpos());
               TrackPush();
               Advance();
               continue;
              }
            case Setjump|Back:
              {
               StackPop(2);
               break;
              }
            case Backjump:
              {
               //--- StackPush:
               //---  0: Saved trackpos
               //---  1: Crawlpos
               StackPop(2);
               Trackto(StackPeek());
               while(Crawlpos()!=StackPeek(1))
                 {
                  Uncapture();
                 }
               break;
              }
            case Forejump:
              {
               //--- StackPush:
               //---  0: Saved trackpos
               //---  1: Crawlpos
               StackPop(2);
               Trackto(StackPeek());
               TrackPush(StackPeek(1));
               Advance();
               continue;
              }
            case Forejump|Back:
              {
               //--- TrackPush:
               //---  0: Crawlpos
               TrackPop();
               while(Crawlpos()!=TrackPeek())
                 {
                  Uncapture();
                 }
               break;
              }
            case Bol:
              {
               if(Leftchars()>0 && CharAt(Textpos()-1)!='\n')
                 {
                  break;
                 }
               Advance();
               continue;
              }
            case Eol:
              {
               if(Rightchars()>0 && CharAt(Textpos())!='\n')
                 {
                  break;
                 }
               Advance();
               continue;
              }
            case Boundary:
              {
               if(!IsBoundary(Textpos(),m_runtextbeg,m_runtextend))
                 {
                  break;
                 }
               Advance();
               continue;
              }
            case Nonboundary:
              {
               if(IsBoundary(Textpos(),m_runtextbeg,m_runtextend))
                 {
                  break;
                 }
               Advance();
               continue;
              }
            case ECMABoundary:
              {
               if(!IsECMABoundary(Textpos(),m_runtextbeg,m_runtextend))
                 {
                  break;
                 }
               Advance();
               continue;
              }
            case NonECMABoundary:
              {
               if(IsECMABoundary(Textpos(),m_runtextbeg,m_runtextend))
                 {
                  break;
                 }
               Advance();
               continue;
              }
            case Beginning:
              {
               if(Leftchars()>0)
                 {
                  break;
                 }
               Advance();
               continue;
              }
            case Start:
              {
               if(Textpos()!=Textstart())
                 {
                  break;
                 }
               Advance();
               continue;
              }
            case EndZ:
              {
               if(Rightchars()>1 || (Rightchars()==1 && CharAt(Textpos())!='\n'))
                 {
                  break;
                 }
               Advance();
               continue;
              }
            case End:
              {
               if(Rightchars()>0)
                 {
                  break;
                 }
               Advance();
               continue;
              }
            case One:
              {
               if(Forwardchars()<1 || Forwardcharnext()!=(ushort)Operand(0))
                 {
                  break;
                 }
               Advance(1);
               continue;
              }
            case Notone:
              {
               if(Forwardchars()<1 || Forwardcharnext()==(char)Operand(0))
                 {
                  break;
                 }
               Advance(1);
               continue;
              }
            case Set:
              {
               if(Forwardchars()<1 || !CRegexCharClass::CharInClass(Forwardcharnext(),m_runstrings[Operand(0)]))
                 {
                  break;
                 }
               Advance(1);
               continue;
              }
            case Multi:
              {
               if(!Stringmatch(m_runstrings[Operand(0)]))
                 {
                  break;
                 }
               Advance(1);
               continue;
              }
            case Ref:
              {
               int capnum=Operand(0);
               if(IsMatched(capnum))
                 {
                  if(!Refmatch(MatchIndex(capnum),MatchLength(capnum)))
                    {
                     break;
                    }
                 }
               else
                 {
                  if((m_runregex.Options() &ECMAScript)==0)
                    {
                     break;
                    }
                 }
               Advance(1);
               continue;
              }
            case Onerep:
              {
               int c=Operand(1);
               if(Forwardchars()<c)
                 {
                  break;
                 }
               char ch=(char)Operand(0);
               while(c-->0)
                 {
                  if(Forwardcharnext()!=ch)
                    {
                     BreakBackward=true;
                     break;
                    }
                 }
               if(BreakBackward)
                 {
                  break;
                 }
               Advance(2);
               continue;
              }

            case Notonerep:
              {
               int c=Operand(1);
               if(Forwardchars()<c)
                 {
                  break;
                 }
               char ch=(char)Operand(0);
               while(c-->0)
                 {
                  if(Forwardcharnext()==ch)
                    {
                     BreakBackward=true;
                     break;
                    }
                 }
               if(BreakBackward)
                 {
                  break;
                 }
               Advance(2);
               continue;
              }
            case Setrep:
              {
               int c=Operand(1);
               if(Forwardchars()<c)
                 {
                  break;
                 }
               string set=m_runstrings[Operand(0)];
               while(c-->0)
                 {
                  if(!CRegexCharClass::CharInClass(Forwardcharnext(),set))
                    {
                     BreakBackward=true;
                     break;
                    }
                 }
               if(BreakBackward)
                 {
                  break;
                 }
               Advance(2);
               continue;
              }
            case Oneloop:
              {
               int c=Operand(1);
               if(c>Forwardchars())
                 {
                  c=Forwardchars();
                 }
               char ch=(char)Operand(0);
               int i;
               for(i=c; i>0; i--)
                 {
                  if(Forwardcharnext()!=ch)
                    {
                     Backwardnext();
                     break;
                    }
                 }
               if(c>i)
                 {
                  TrackPush(c-i-1,Textpos()-Bump());
                 }
               Advance(2);
               continue;
              }
            case Notoneloop:
              {
               int c=Operand(1);
               if(c>Forwardchars())
                 {
                  c=Forwardchars();
                 }
               char ch=(char)Operand(0);
               int i;
               for(i=c; i>0; i--)
                 {
                  if(Forwardcharnext()==ch)
                    {
                     Backwardnext();
                     break;
                    }
                 }
               if(c>i)
                 {
                  TrackPush(c-i-1,Textpos()-Bump());
                 }
               Advance(2);
               continue;
              }
            case Setloop:
              {
               int c=Operand(1);
               if(c>Forwardchars())
                 {
                  c=Forwardchars();
                 }
               string set=m_runstrings[Operand(0)];
               int i;
               for(i=c; i>0; i--)
                 {
                  if(!CRegexCharClass::CharInClass(Forwardcharnext(),set))
                    {
                     Backwardnext();
                     break;
                    }
                 }
               if(c>i)
                 {
                  TrackPush(c-i-1,Textpos()-Bump());
                 }
               Advance(2);
               continue;
              }
            case Oneloop|Back:
            case Notoneloop|Back:
              {
               TrackPop(2);
               int i   = TrackPeek();
               int pos = TrackPeek(1);
               Textto(pos);
               if(i>0)
                 {
                  TrackPush(i-1,pos-Bump());
                 }
               Advance(2);
               continue;
              }
            case Setloop|Back:
              {
               TrackPop(2);
               int i   = TrackPeek();
               int pos = TrackPeek(1);
               Textto(pos);
               if(i>0)
                 {
                  TrackPush(i-1,pos-Bump());
                 }
               Advance(2);
               continue;
              }
            case Onelazy:
            case Notonelazy:
              {
               int c=Operand(1);
               if(c>Forwardchars())
                 {
                  c=Forwardchars();
                 }
               if(c>0)
                 {
                  TrackPush(c-1,Textpos());
                 }
               Advance(2);
               continue;
              }
            case Setlazy:
              {
               int c=Operand(1);
               if(c>Forwardchars())
                 {
                  c=Forwardchars();
                 }
               if(c>0)
                 {
                  TrackPush(c-1,Textpos());
                 }
               Advance(2);
               continue;
              }
            case Onelazy|Back:
              {
               TrackPop(2);
               int pos=TrackPeek(1);
               Textto(pos);
               if(Forwardcharnext()!=(char)Operand(0))
                 {
                  break;
                 }
               int i=TrackPeek();
               if(i>0)
                 {
                  TrackPush(i-1,pos+Bump());
                 }
               Advance(2);
               continue;
              }
            case Notonelazy|Back:
              {
               TrackPop(2);
               int pos=TrackPeek(1);
               Textto(pos);
               if(Forwardcharnext()==(char)Operand(0))
                 {
                  break;
                 }
               int i=TrackPeek();
               if(i>0)
                 {
                  TrackPush(i-1,pos+Bump());
                 }
               Advance(2);
               continue;
              }
            case Setlazy|Back:
              {
               TrackPop(2);
               int pos=TrackPeek(1);
               Textto(pos);
               if(!CRegexCharClass::CharInClass(Forwardcharnext(),m_runstrings[Operand(0)]))
                 {
                  break;
                 }
               int i=TrackPeek();
               if(i>0)
                 {
                  TrackPush(i-1,pos+Bump());
                 }
               Advance(2);
               continue;
              }
            default:
              {
               Print("Internal error! On file:'"+__FILE__+"'; in function:'"+__FUNCTION__+"'.");
               //--- retrun 
               return;
              }
           }
         //--- "BreakBackward" comes here:
         Backtrack();
        }

     }
#ifdef _DEBUG
   //+------------------------------------------------------------------+
   //| DumpState.                                                       |
   //+------------------------------------------------------------------+
   void DumpState()
     {
      CRegexRunner::DumpState();
      Print("       "+m_runcode.OpcodeDescription(m_runcodepos)+
            ((m_runoperator  &Back)!=0 ? " Back" : "")+
            ((m_runoperator  &Back2)!=0 ? " Back2" : ""));
      Print("");
     }
#endif
  };
//+------------------------------------------------------------------+
