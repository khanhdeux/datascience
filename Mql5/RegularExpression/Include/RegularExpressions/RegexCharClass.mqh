//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
#include <Internal\Generic\IComparer.mqh>
#include <Internal\Generic\List.mqh>
#include <Internal\Generic\Dictionary.mqh>
//+------------------------------------------------------------------+
//| Purpose: Defines the Unicode category of a character.            |
//+------------------------------------------------------------------+
enum UnicodeCategory
  {
   UppercaseLetter=0,
   LowercaseLetter=1,
   TitlecaseLetter=2,
   ModifierLetter=3,
   OtherLetter=4,
   NonSpacingMark=5,
   SpacingCombiningMark=6,
   EnclosingMark=7,
   DecimalDigitNumber=8,
   LetterNumber=9,
   OtherNumber=10,
   SpaceSeparator=11,
   LineSeparator=12,
   ParagraphSeparator=13,
   Control=14,
   Format=15,
   Surrogate=16,
   PrivateUse=17,
   ConnectorPunctuation=18,
   DashPunctuation=19,
   OpenPunctuation=20,
   ClosePunctuation=21,
   InitialQuotePunctuation=22,
   FinalQuotePunctuation=23,
   OtherPunctuation=24,
   MathSymbol=25,
   CurrencySymbol=26,
   ModifierSymbol=27,
   OtherSymbol=28,
   OtherNotAssigned=29,
  };
//+------------------------------------------------------------------+
//| Retrieves information about a Unicode character. This class      |
//| cannot be inherited.                                             |
//+------------------------------------------------------------------+
class CCharUnicodeInfo
  {
private:
   static uchar      s_unicodeCategory[];
   static const int  s_categoryForLatin1[];
   //+------------------------------------------------------------------+
   //| Get latin UnicodeCategory.                                       |
   //+------------------------------------------------------------------+   
   static UnicodeCategory GetLatin1UnicodeCategory(const ushort ch)
     {
      return (UnicodeCategory)(s_categoryForLatin1[(int)ch]);
     }
   //+------------------------------------------------------------------+
   //| Indicates whether a Latinn Unicode character is categorized as   |
   //| white space.                                                     |
   //+------------------------------------------------------------------+
   static bool IsWhiteSpaceLatin1(const ushort c)
     {
      //--- There are characters which belong to UnicodeCategory.Control but are considered as white spaces.
      //--- We use code point comparisons for these characters here as a temporary fix.
      //--- U+0009 = <control> HORIZONTAL TAB
      //--- U+000a = <control> LINE FEED
      //--- U+000b = <control> VERTICAL TAB
      //--- U+000c = <contorl> FORM FEED
      //--- U+000d = <control> CARRIAGE RETURN
      //--- U+0085 = <control> NEXT LINE
      //--- U+00a0 = NO-BREAK SPACE
      if((c==' ') || ((ushort)c>=0x0009 && (ushort)c<=0x000d) || (ushort)c==0x00a0 || (ushort)c==0x0085)
        {
         return (true);
        }
      return (false);
     }
   //+------------------------------------------------------------------+
   //| Indicates whether a Unicode character is latin.                  |
   //+------------------------------------------------------------------+
   static bool IsLatin1(const ushort ch)
     {
      return (ch <= '\x00ff');
     }
public:
   //+------------------------------------------------------------------+
   //| Gets the Unicode category of a Unicode character.                |
   //+------------------------------------------------------------------+
   static UnicodeCategory GetUnicodeCategory(const ushort c)
     {
      if(IsLatin1(c))
        {
         return (GetLatin1UnicodeCategory(c));
        }
      if(ArraySize(s_unicodeCategory)==0)
        {
         uchar archArray[]={ #include "CharUnicodeCategory.txt" };
         CryptDecode(CRYPT_ARCH_ZIP,archArray,s_unicodeCategory,s_unicodeCategory);
        }
      return ((UnicodeCategory)s_unicodeCategory[c]);
     }
   //+------------------------------------------------------------------+
   //| Indicates whether a Unicode character is categorized as white    |
   //| space.                                                           |
   //+------------------------------------------------------------------+
   static bool IsWhiteSpace(const ushort c)
     {
      if(IsLatin1(c))
        {
         return (IsWhiteSpaceLatin1(c));
        }
      UnicodeCategory uc=GetUnicodeCategory(c);
      //--- In Unicode 3.0, U+2028 is the only character which is under the category "LineSeparator".
      //--- And U+2029 is th eonly character which is under the category "ParagraphSeparator".
      switch(uc)
        {
         case(SpaceSeparator):
         case (LineSeparator):
         case(ParagraphSeparator):
           {
            return (true);
           }
        }
      return (false);
     }
  };
static const int CCharUnicodeInfo::s_categoryForLatin1[]=
  {
   UnicodeCategory::Control,UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control,    // 0000 - 0007
   UnicodeCategory::Control,UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control,    // 0008 - 000F
   UnicodeCategory::Control,UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control,    // 0010 - 0017
   UnicodeCategory::Control,UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control,    // 0018 - 001F
   UnicodeCategory::SpaceSeparator,UnicodeCategory::OtherPunctuation,UnicodeCategory::OtherPunctuation,UnicodeCategory::OtherPunctuation,UnicodeCategory::CurrencySymbol,UnicodeCategory::OtherPunctuation,UnicodeCategory::OtherPunctuation,UnicodeCategory::OtherPunctuation,// 0020 - 0027
   UnicodeCategory::OpenPunctuation,UnicodeCategory::ClosePunctuation,UnicodeCategory::OtherPunctuation,UnicodeCategory::MathSymbol,UnicodeCategory::OtherPunctuation,UnicodeCategory::DashPunctuation,UnicodeCategory::OtherPunctuation,UnicodeCategory::OtherPunctuation,    // 0028 - 002F
   UnicodeCategory::DecimalDigitNumber,UnicodeCategory::DecimalDigitNumber,UnicodeCategory::DecimalDigitNumber,UnicodeCategory::DecimalDigitNumber,UnicodeCategory::DecimalDigitNumber,UnicodeCategory::DecimalDigitNumber,UnicodeCategory::DecimalDigitNumber,UnicodeCategory::DecimalDigitNumber,// 0030 - 0037
   UnicodeCategory::DecimalDigitNumber,UnicodeCategory::DecimalDigitNumber,UnicodeCategory::OtherPunctuation,UnicodeCategory::OtherPunctuation,UnicodeCategory::MathSymbol,UnicodeCategory::MathSymbol,UnicodeCategory::MathSymbol,UnicodeCategory::OtherPunctuation,// 0038 - 003F
   UnicodeCategory::OtherPunctuation,UnicodeCategory::UppercaseLetter,UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter,    // 0040 - 0047
   UnicodeCategory::UppercaseLetter,UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter,    // 0048 - 004F
   UnicodeCategory::UppercaseLetter,UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter,    // 0050 - 0057
   UnicodeCategory::UppercaseLetter,UnicodeCategory::UppercaseLetter,UnicodeCategory::UppercaseLetter,UnicodeCategory::OpenPunctuation,UnicodeCategory::OtherPunctuation,UnicodeCategory::ClosePunctuation,UnicodeCategory::ModifierSymbol,UnicodeCategory::ConnectorPunctuation,// 0058 - 005F
   UnicodeCategory::ModifierSymbol,UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,// 0060 - 0067
   UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter,    // 0068 - 006F
   UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter,    // 0070 - 0077
   UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,UnicodeCategory::OpenPunctuation,UnicodeCategory::MathSymbol,UnicodeCategory::ClosePunctuation,UnicodeCategory::MathSymbol,UnicodeCategory::Control,// 0078 - 007F
   UnicodeCategory::Control,UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control,    // 0080 - 0087
   UnicodeCategory::Control,UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control,    // 0088 - 008F
   UnicodeCategory::Control,UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control,    // 0090 - 0097
   UnicodeCategory::Control,UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control, UnicodeCategory::Control,    // 0098 - 009F
   UnicodeCategory::SpaceSeparator,UnicodeCategory::OtherPunctuation,UnicodeCategory::CurrencySymbol,UnicodeCategory::CurrencySymbol,UnicodeCategory::CurrencySymbol,UnicodeCategory::CurrencySymbol,UnicodeCategory::OtherSymbol,UnicodeCategory::OtherSymbol,// 00A0 - 00A7
   UnicodeCategory::ModifierSymbol,UnicodeCategory::OtherSymbol,UnicodeCategory::LowercaseLetter,UnicodeCategory::InitialQuotePunctuation,UnicodeCategory::MathSymbol,UnicodeCategory::DashPunctuation,UnicodeCategory::OtherSymbol,UnicodeCategory::ModifierSymbol,// 00A8 - 00AF
   UnicodeCategory::OtherSymbol,UnicodeCategory::MathSymbol,UnicodeCategory::OtherNumber,UnicodeCategory::OtherNumber,UnicodeCategory::ModifierSymbol,UnicodeCategory::LowercaseLetter,UnicodeCategory::OtherSymbol,UnicodeCategory::OtherPunctuation,// 00B0 - 00B7
   UnicodeCategory::ModifierSymbol,UnicodeCategory::OtherNumber,UnicodeCategory::LowercaseLetter,UnicodeCategory::FinalQuotePunctuation,UnicodeCategory::OtherNumber,UnicodeCategory::OtherNumber,UnicodeCategory::OtherNumber,UnicodeCategory::OtherPunctuation,// 00B8 - 00BF
   UnicodeCategory::UppercaseLetter,UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter,    // 00C0 - 00C7
   UnicodeCategory::UppercaseLetter,UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter,    // 00C8 - 00CF
   UnicodeCategory::UppercaseLetter,UnicodeCategory::UppercaseLetter,UnicodeCategory::UppercaseLetter,UnicodeCategory::UppercaseLetter,UnicodeCategory::UppercaseLetter,UnicodeCategory::UppercaseLetter,UnicodeCategory::UppercaseLetter,UnicodeCategory::MathSymbol,// 00D0 - 00D7
   UnicodeCategory::UppercaseLetter,UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::UppercaseLetter, UnicodeCategory::LowercaseLetter,    // 00D8 - 00DF
   UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter,    // 00E0 - 00E7
   UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter, UnicodeCategory::LowercaseLetter,    // 00E8 - 00EF
   UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,UnicodeCategory::MathSymbol,// 00F0 - 00F7
   UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,UnicodeCategory::LowercaseLetter,// 00F8 - 00FF
  };
static uchar CCharUnicodeInfo::s_unicodeCategory[];
//+------------------------------------------------------------------+
//| Purpose: A first/last pair representing a single range of        |
//| characters.                                                      |
//+------------------------------------------------------------------+
class CSingleRange : public IComparable
  {
private:
   ushort            m_first;
   ushort            m_last;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+   
                     CSingleRange(const ushort first,const ushort last)
     {
      m_first= first;
      m_last = last;
     }
   //+------------------------------------------------------------------+
   //| Get first char.                                                  |
   //+------------------------------------------------------------------+
   ushort First()
     {
      return (m_first);
     }
   //+------------------------------------------------------------------+
   //| Set first char.                                                  |
   //+------------------------------------------------------------------+
   void First(const ushort value)
     {
      m_first=value;
     }
   //+------------------------------------------------------------------+
   //| Get last char.                                                   |
   //+------------------------------------------------------------------+
   ushort Last()
     {
      return (m_last);
     }
   //+------------------------------------------------------------------+
   //| Set last char.                                                   |
   //+------------------------------------------------------------------+
   void Last(const ushort value)
     {
      m_last=value;
     }
   //+------------------------------------------------------------------+
   //| Creates a new CSingleRange that is a copy of the current         |
   //| instance.                                                        |
   //+------------------------------------------------------------------+
   CSingleRange *Clone()
     {
      return new CSingleRange(this.m_first,this.m_last);
     }
  };
//+------------------------------------------------------------------+
//| Purpose: For sorting ranges; compare based on the first char in  |
//| the range.                                                       |
//+------------------------------------------------------------------+
class CSingleRangeComparer : public IComparer<CSingleRange*>
  {
public:
   //+------------------------------------------------------------------+
   //| Compare.                                                         |
   //+------------------------------------------------------------------+
   int Compare(CSingleRange *x,CSingleRange *y)
     {
      return((x).First() < (y).First() ? -1:((x).First()>(y).First() ? 1 : 0));
     }
  };
//+------------------------------------------------------------------+
//| Purpose: Lower case mapping descriptor.                          |
//+------------------------------------------------------------------+
struct LowerCaseMapping
  {
public:
   ushort            m_chMin;
   ushort            m_chMax;
   int               m_lcOp;
   int               m_data;
  };
//+------------------------------------------------------------------+
//| Purpose: This CRegexCharClass class provides the "set of Unicode |
//| chars" functionality used by the regexp engine.                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| The main function of CRegexCharClass is as a builder to turn     |
//| ranges, characters and Unicode categories into a single string.  |
//| This string is used as a black box  representation of a character|
//| class by the rest of Regex.  The format is as follows.           |
//|                                                                  |
//| Char index   Use:                                                |
//|      0       Flags - currently this only holds the "negate" flag |
//|      1       length of the string representing the "set" portion,|
//|              eg [a-z0-9] only has a "set"                        |
//|      2       length of the string representing the "category"    |
//|              portion, eg [\p{Lu}] only has a "category"          |
//|      3...m   The set.  These are a series of ranges which define |
//|              the characters included in the set. To determine if |
//|              a given character is in the set, we binary search   |
//|              over this set of ranges and see where the character |
//|              should go.  Based on whether the ending index is odd|
//|              or even, we know if the character is in the set.    |
//|      m+1...n The categories.  This is a list of UnicodeCategory  |
//|              enum values which describe categories included in   |
//|              this class.                                         |
//+------------------------------------------------------------------+
class CRegexCharClass
  {
private:
   //--- Instance data
   List<CSingleRange*>*m_rangelist;
   string            m_categories;
   bool              m_canonical;
   bool              m_negate;
   CRegexCharClass *m_subtractor;
   static            Dictionary<string,string>m_definedCategories;
   static const string m_propTable[112][2];
public:
   //--- Constants
   static const int  FLAGS;           // 0
   static const int  SETLENGTH;       // 1
   static const int  CATEGORYLENGTH;  // 2
   static const int  SETSTART;        // 3
   static const char Nullchar;        // '\0'
   static const ushort Lastchar;      // '\uFFFF'
   static const string GroupChar;     // (char)0
   static const short SpaceConst;     // 100
   static const short NotSpaceConst;  // -100
   static const ushort ZeroWidthJoiner; // '\u200D'
   static const ushort ZeroWidthNonJoiner;// '\u200C'
   static const string ECMASpaceSet;  // "\u0009\u000E\u0020\u0021"
   static const string NotECMASpaceSet;// "\0\u0009\u000E\u0020\u0021"
   static const string ECMAWordSet;   // "\u0030\u003A\u0041\u005B\u005F\u0060\u0061\u007B\u0130\u0131"
   static const string NotECMAWordSet;// "\0\u0030\u003A\u0041\u005B\u005F\u0060\u0061\u007B\u0130\u0131"
   static const string ECMADigitSet;  // "\u0030\u003A"
   static const string NotECMADigitSet;// "\0\u0030\u003A"
   static const string ECMASpaceClass;// "\x00\x04\x00"+ECMASpaceSet
   static const string NotECMASpaceClass;// "\x01\x04\x00"+ECMASpaceSet
   static const string ECMAWordClass; // "\x00\x0A\x00"+ECMAWordSet
   static const string NotECMAWordClass;// "\x01\x0A\x00"+ECMAWordSet
   static const string ECMADigitClass;// "\x00\x02\x00"+ECMADigitSet
   static const string NotECMADigitClass;// "\x01\x02\x00"+ECMADigitSet
   static const string AnyClass;      // "\x00\x01\x00\x00"
   static const string EmptyClass;    // "\x00\x00\x00"
   static const string InternalRegexIgnoreCase;// "__InternalRegexIgnoreCase__"
   static const string Space;         // "\x64"
   static const string NotSpace;      // NegateCategory(Space)
   static const string Word;
   static const string NotWord;
   static const string SpaceClass;
   static const string NotSpaceClass;
   static const string WordClass;
   static const string NotWordClass;
   static const string DigitClass;
   static const string NotDigitClass;
private:
   //+------------------------------------------------------------------+
   //| Let U be the set of Unicode character values and let L be the    |
   //| lowercase function,mapping from U to U. To perform case          |
   //| insensitive matching of character sets,we need to be able to map |
   //| an interval I in U,say                                           |
   //|                                                                  |
   //| I=[chMin,chMax]={ ch : chMin<=ch<=chMax }                        |
   //|                                                                  |
   //| to a set A such that A contains L(I) and A is contained in the   |
   //| union of I and L(I).                                             |
   //|                                                                  |
   //| The table below partitions U into intervals on which L is        |
   //| non-decreasing. Thus,for any interval J=[a,b]contained in one of |
   //| these intervals, L(J) is contained in[L(a),L(b)].                |
   //|                                                                  |
   //| It is also true that for any such J,[L(a),L(b)]is contained in   | 
   //| the union of J and L(J). This does not follow from L being       | 
   //| non-decreasing on these intervals. It follows from the nature of |
   //| the L on each interval. On each interval,L has one of the        |
   //| following forms:                                                 |
   //|                                                                  |
   //| (1) L(ch) = constant            (LowercaseSet)                   |
   //| (2) L(ch) = ch + offset         (LowercaseAdd)                   |
   //| (3) L(ch) = ch | 1              (LowercaseBor)                   |
   //| (4) L(ch) = ch + (ch & 1)       (LowercaseBad)                   |
   //|                                                                  |
   //| It is easy to verify that for any of these forms[L(a),L(b)]is    |
   //| contained in the union of[a,b]and L([a,b]).                      |
   //+------------------------------------------------------------------+
   static const int  LowercaseSet;    // 0 Set to arg.
   static const int  LowercaseAdd;    // 1 Add arg.
   static const int  LowercaseBor;    // 2 Bitwise or with 1.
   static const int  LowercaseBad;    // 3 Bitwise and with 1 and add original.
   static const LowerCaseMapping m_lcTable[94];
public:
   //+------------------------------------------------------------------+
   //| Initialize variable and constants.                               |
   //+------------------------------------------------------------------+
   static const string Initialize()
     {
      //+------------------------------------------------------------------+
      //| addressing Dictionary versus Hashtable thread safety difference  |
      //| by using a temp Dictionary. Note that this is just a theoretical |
      //| concern since this is a static ctor and getter methods aren't    |
      //| called until after this is done; this is just to avoid the       |
      //| long-term possibility of thread safety problems.                 |
      //+------------------------------------------------------------------+
      char groups[9];
      string word="";
      word+="\x0";
      groups[0]=(char)GroupChar;
      //+------------------------------------------------------------------+
      //| We need the UnicodeCategory enum values as a char so we can put  |
      //| them in a string in the hashtable.  In order to get there, we    |
      //| first must cast to an int, then cast to a char.                  |
      //| Also need to distinguish between positive and negative values.   |
      //| UnicodeCategory is zero based, so we add one to each value and   |
      //| subtract it off later.                                           |
      //+------------------------------------------------------------------+
      //-- Others:
      groups[1]=(char)((int)(Control)+1);
      m_definedCategories.Set("Cc",CharToString(groups[1]));     // Control 
      groups[2]=(char)((int)(Format)+1);
      m_definedCategories.Set("Cf",CharToString(groups[2]));     // Format
      groups[3]=(char)((int) OtherNotAssigned+1);
      m_definedCategories.Set("Cn",CharToString(groups[3]));     // Not assigned
      groups[4]=(char)((int) PrivateUse+1);
      m_definedCategories.Set("Co",CharToString(groups[4]));     // Private use
      groups[5]=(char)((int) Surrogate+1);
      m_definedCategories.Set("Cs",CharToString(groups[5]));     // Surrogate
      groups[6]=(char)GroupChar;
      m_definedCategories.Set("C",CharArrayToString(groups,0,7));
      //--- Letters
      groups[1]=(char)((int) LowercaseLetter+1);
      m_definedCategories.Set("Ll",CharToString(groups[1]));     // Lowercase
      groups[2]=(char)((int) ModifierLetter+1);
      m_definedCategories.Set("Lm",CharToString(groups[2]));     // Modifier
      groups[3]=(char)((int) OtherLetter+1);
      m_definedCategories.Set("Lo",CharToString(groups[3]));     // Other 
      groups[4]=(char)((int) TitlecaseLetter+1);
      m_definedCategories.Set("Lt",CharToString(groups[4]));     // Titlecase
      groups[5]=(char)((int) UppercaseLetter+1);
      m_definedCategories.Set("Lu",CharToString(groups[5]));     // Uppercase
      m_definedCategories.Set("L",CharArrayToString(groups,0,7));
      word+=CharArrayToString(groups,1,5);
      //--- !!!This category should only ever be used in conjunction with RegexOptions.IgnoreCase code paths!!!
      m_definedCategories.Set(InternalRegexIgnoreCase,(GroupChar+
                              CharToString(groups[1])+CharToString(groups[4])+
                              CharToString(groups[5])+CharToString(groups[6])));
      //--- Marks        
      groups[1]=(char)((int) SpacingCombiningMark+1);
      m_definedCategories.Set("Mc",CharToString(groups[1]));     // Spacing combining
      groups[2]=(char)((int) EnclosingMark+1);
      m_definedCategories.Set("Me",CharToString(groups[2]));     // Enclosing
      groups[3]=(char)((int) NonSpacingMark+1);
      m_definedCategories.Set("Mn",CharToString(groups[3]));     // Non-spacing
      groups[4]=(char)GroupChar;
      m_definedCategories.Set("M",CharArrayToString(groups,0,5));
      word+=CharToString(groups[3]);
      //--- Numbers
      groups[1]=(char)((int) DecimalDigitNumber+1);
      m_definedCategories.Set("Nd",CharToString(groups[1]));     // Decimal digit
      groups[2]=(char)((int) LetterNumber+1);
      m_definedCategories.Set("Nl",CharToString(groups[2]));     // Letter
      groups[3]=(char)((int) OtherNumber+1);
      m_definedCategories.Set("No",CharToString(groups[3]));     // Other 
      m_definedCategories.Set("N",CharArrayToString(groups,0,5));
      word+=CharToString(groups[1]);
      //--- Punctuation
      groups[1]=(char)((int) ConnectorPunctuation+1);
      m_definedCategories.Set("Pc",CharToString(groups[1]));     // Connector
      groups[2]=(char)((int) DashPunctuation+1);
      m_definedCategories.Set("Pd",CharToString(groups[2]));     // Dash
      groups[3]=(char)((int) ClosePunctuation+1);
      m_definedCategories.Set("Pe",CharToString(groups[3]));     // Close
      groups[4]=(char)((int) OtherPunctuation+1);
      m_definedCategories.Set("Po",CharToString(groups[4]));     // Other
      groups[5]=(char)((int) OpenPunctuation+1);
      m_definedCategories.Set("Ps",CharToString(groups[5]));     // Open
      groups[6]=(char)((int) FinalQuotePunctuation+1);
      m_definedCategories.Set("Pf",CharToString(groups[6]));     // Inital quote
      groups[7]=(char)((int) InitialQuotePunctuation+1);
      m_definedCategories.Set("Pi",CharToString(groups[7]));     // Final quote
      groups[8]=(char)GroupChar;
      m_definedCategories.Set("P",CharArrayToString(groups,0,9));
      word+=CharToString(groups[1]);
      //--- Symbols
      groups[1]=(char)((int) CurrencySymbol+1);
      m_definedCategories.Set("Sc",CharToString(groups[1]));     // Currency
      groups[2]=(char)((int) ModifierSymbol+1);
      m_definedCategories.Set("Sk",CharToString(groups[2]));     // Modifier
      groups[3]=(char)((int) MathSymbol+1);
      m_definedCategories.Set("Sm",CharToString(groups[3]));     // Math
      groups[4]=(char)((int) OtherSymbol+1);
      m_definedCategories.Set("So",CharToString(groups[4]));     // Other
      groups[5]=(char)GroupChar;
      m_definedCategories.Set("S",CharArrayToString(groups,0,6));
      //--- Separators
      groups[1]=(char)((int) LineSeparator+1);
      m_definedCategories.Set("Zl",CharToString(groups[1]));     // Line
      groups[2]=(char)((int) ParagraphSeparator+1);
      m_definedCategories.Set("Zp",CharToString(groups[2]));     // Paragraph
      groups[3]=(char)((int) SpaceSeparator+1);
      m_definedCategories.Set("Zs",CharToString(groups[3]));     // Space
      groups[4]=(char)GroupChar;
      m_definedCategories.Set("Z",CharArrayToString(groups,0,5));
      word+="\x0";
      //--- return 
      return (word);
     }
   //+------------------------------------------------------------------+
   //| Creates an empty character class.                                |
   //+------------------------------------------------------------------+
                     CRegexCharClass() : m_negate(false)
     {
      m_rangelist = new List<CSingleRange*>(6);
      m_canonical = true;
     }
private:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CRegexCharClass(const bool negate,List<CSingleRange*>*ranges,const string categories,CRegexCharClass *subtraction)
     {
      m_rangelist=ranges;
      m_categories=categories;
      m_canonical=true;
      m_negate=negate;
      m_subtractor=subtraction;
     }
public:
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~CRegexCharClass()
     {
      for(int i=0; i<m_rangelist.Count(); i++)
        {
         if(CheckPointer(m_rangelist[i])==POINTER_DYNAMIC)
           {
            delete m_rangelist[i];
           }
        }
      if(CheckPointer(m_rangelist)==POINTER_DYNAMIC)
        {
         delete m_rangelist;
        }
     }
   //+------------------------------------------------------------------+
   //| CanMerge.                                                        |
   //+------------------------------------------------------------------+
   bool CanMerge()
     {
      return (!m_negate && m_subtractor == NULL);
     }
   //+------------------------------------------------------------------+
   //| Negate.                                                          |
   //+------------------------------------------------------------------+
   void Negate(const bool value)
     {
      m_negate=value;
     }
   //+------------------------------------------------------------------+
   //| AddChar.                                                         |
   //+------------------------------------------------------------------+
   void AddChar(const ushort c)
     {
      AddRange(c,c);
     }
   //+------------------------------------------------------------------+
   //| Adds a regex char class.                                         |
   //+------------------------------------------------------------------+
   void AddCharClass(CRegexCharClass *cc)
     {
      int i;
      if(!(cc.CanMerge() && this.CanMerge()))
        {
         Print("Both character classes added together must be able to merge."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         DebugBreak();
        }
      if(!cc.m_canonical)
        {
         //--- if the new char class to add isn't canonical, we're not either.
         m_canonical=false;
        }
      else if(m_canonical && RangeCount()>0 && cc.RangeCount()>0 && cc.GetRangeAt(0).First()<=GetRangeAt(RangeCount()-1).Last())
        {
         m_canonical=false;
        }
      for(i=0; i<cc.RangeCount(); i+=1)
        {
         m_rangelist.Add(cc.GetRangeAt(i).Clone());
        }
      m_categories+=cc.m_categories;
     }
private:
   //+------------------------------------------------------------------+
   //| Adds a set (specified by its string represenation) to the class. |
   //+------------------------------------------------------------------+
   void AddSet(const string set)
     {
      int i;
      if(m_canonical && RangeCount()>0 && StringLen(set)>0 && StringGetCharacter(set,0)<=(ushort)GetRangeAt(RangeCount()-1).Last())
        {
         m_canonical=false;
        }
      for(i=0; i<StringLen(set)-1; i+=2)
        {
         m_rangelist.Add(new CSingleRange(StringGetCharacter(set,i),(ushort)(StringGetCharacter(set,i+1)-1)));
        }
      if(i<StringLen(set))
        {
         m_rangelist.Add(new CSingleRange(StringGetCharacter(set,i),Lastchar));
        }
     }
public:
   //+------------------------------------------------------------------+
   //| AddSubtraction.                                                  |
   //+------------------------------------------------------------------+
   void AddSubtraction(CRegexCharClass *sub)
     {
      if(m_subtractor!=NULL)
        {
         Print("Can't add two subtractions to a char class."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         DebugBreak();
        }
      m_subtractor=sub;
     }
   //+------------------------------------------------------------------+
   //| Adds a single range of characters to the class.                  |
   //+------------------------------------------------------------------+
   void AddRange(const ushort first,const ushort last)
     {
      m_rangelist.Add(new CSingleRange(first,last));
      if(m_canonical && m_rangelist.Count()>0 && first<=m_rangelist[m_rangelist.Count()-1].Last())
        {
         m_canonical=false;
        }
     }
   //+------------------------------------------------------------------+
   //| AddCategoryFromName.                                             |
   //+------------------------------------------------------------------+
   void AddCategoryFromName(const string categoryName,const bool invert,const bool caseInsensitive,const string pattern)
     {
      string cat;
      m_definedCategories.TryGetValue(categoryName,cat);
      if(StringLen(cat)!=NULL && StringCompare(categoryName,CRegexCharClass::InternalRegexIgnoreCase)!=0)
        {
         string catstr=cat;
         if(caseInsensitive)
           {
            if(StringCompare(categoryName,"Ll") || StringCompare(categoryName,"Lu") || StringCompare(categoryName,"Lt"))
              {
               //---- when RegexOptions.IgnoreCase is specified then {Ll}, {Lu}, and {Lt} cases should all match
               catstr=(string)m_definedCategories[InternalRegexIgnoreCase];
              }
           }

         if(invert)
           {
            catstr=NegateCategory(catstr); // negate the category
           }
         m_categories+=((string) catstr);
        }
      else
        {
         AddSet(SetFromProperty(categoryName,invert,pattern));
        }
     }
private:
   //+------------------------------------------------------------------+
   //| AddCategory.                                                     |
   //+------------------------------------------------------------------+
   void AddCategory(const string category)
     {
      m_categories+=category;
     }
public:
   //+------------------------------------------------------------------+
   //| Adds to the class any lowercase versions of characters already in|
   //| the class. Used for case-insensitivity.                          |
   //+------------------------------------------------------------------+
   void AddLowercase()
     {
      int               i;
      int               origSize;
      CSingleRange       *range;
      m_canonical=false;
      for(i=0,origSize=m_rangelist.Count(); i<origSize; i++)
        {
         range=m_rangelist[i];
         if(range.First()==range.Last())
           {
            string lower=ShortToString(range.First());
            StringToLower(lower);
            range.Last(StringGetCharacter(lower,0));
            range.First(StringGetCharacter(lower,0));
           }
         else
           {
            AddLowercaseRange(range.First(),range.Last());
           }
        }
     }
private:
   //+------------------------------------------------------------------+
   //| For a single range that's in the set, adds any additional ranges |
   //| necessary to ensure that lowercase equivalents are also included.|
   //+------------------------------------------------------------------+
   void AddLowercaseRange(const ushort chMin,const ushort chMax)
     {
      int i=0,iMax=0,iMid=0;
      ushort chMinT=0,chMaxT=0;
      LowerCaseMapping lc={};
      for(i=0,iMax=ArraySize(m_lcTable); i<iMax;)
        {
         iMid=(i+iMax)/2;
         if(m_lcTable[iMid].m_chMax<chMin)
           {
            i=iMid+1;
           }
         else
           {
            iMax=iMid;
           }
        }
      if(i>=ArraySize(m_lcTable))
        {
         //--- return 
         return;
        }
      for(; i<ArraySize(m_lcTable) && lc.m_chMin<=chMax; i++)
        {
         if((chMinT=lc.m_chMin)<chMin)
           {
            chMinT=chMin;
           }
         if((chMaxT=lc.m_chMax)>chMax)
           {
            chMaxT=chMax;
           }
         switch(lc.m_lcOp)
           {
            case 0: // LowercaseSet
              {
               chMinT = (ushort)lc.m_data;
               chMaxT = (ushort)lc.m_data;
               break;
              }
            case 1: // LowercaseAdd
               chMinT += (ushort)lc.m_data;
               chMaxT += (ushort)lc.m_data;
               break;
            case 2: // LowercaseBor
               chMinT |= (ushort)1;
               chMaxT |= (ushort)1;
               break;
            case 3: //LowercaseBad
               chMinT += (ushort)(chMinT & 1);
               chMaxT += (ushort)(chMaxT & 1);
               break;
           }
         if(chMinT<chMin || chMaxT>chMax)
           {
            AddRange(chMinT,chMaxT);
           }
         lc=m_lcTable[i];
        }
     }
public:
   //+------------------------------------------------------------------+
   //| AddWord.                                                         |
   //+------------------------------------------------------------------+
   void AddWord(const bool ecma,const bool negate)
     {
      if(negate)
        {
         if(ecma)
           {
            AddSet(CRegexCharClass::NotECMAWordSet);
           }
         else
           {
            AddCategory(CRegexCharClass::NotWord);
           }
        }
      else
        {
         if(ecma)
           {
            AddSet(CRegexCharClass::ECMAWordSet);
           }
         else
           {
            AddCategory(CRegexCharClass::Word);
           }
        }
     }
   //+------------------------------------------------------------------+
   //| AddSpace.                                                        |
   //+------------------------------------------------------------------+
   void AddSpace(const bool ecma,const bool negate)
     {
      if(negate)
        {
         if(ecma)
           {
            AddSet(CRegexCharClass::NotECMASpaceSet);
           }
         else
           {
            AddCategory(CRegexCharClass::NotSpace);
           }
        }
      else
        {
         if(ecma)
           {
            AddSet(CRegexCharClass::ECMASpaceSet);
           }
         else
           {
            AddCategory(CRegexCharClass::Space);
           }
        }
     }
   //+------------------------------------------------------------------+
   //| AddDigit.                                                        |
   //+------------------------------------------------------------------+
   void AddDigit(const bool ecma,const bool negate,const string pattern)
     {
      if(ecma)
        {
         if(negate)
           {
            AddSet(CRegexCharClass::NotECMADigitSet);
           }
         else
           {
            AddSet(CRegexCharClass::ECMADigitSet);
           }
        }
      else
        {
         AddCategoryFromName("Nd",negate,false,pattern);
        }
     }
   //+------------------------------------------------------------------+
   //| ConvertOldstringsToClass.                                        |
   //+------------------------------------------------------------------+
   static string ConvertOldstringsToClass(const string set,const string category)
     {
      string text;
      if(StringLen(set)>=2 && StringGetCharacter(set,0)=='\0' && StringGetCharacter(set,1)=='\0')
        {
         text+=((string) 0x1);
         text+=((string)(StringLen(set)-2));
         text+=((string) StringLen(category));
         text+=(StringSubstr(set,2));
        }
      else
        {
         text+=((string) 0x0);
         text+=((string) StringLen(set));
         text+=((string) StringLen(category));
         text+=(set);
        }
      text+=category;
      return (text);
     }
   //+------------------------------------------------------------------+
   //| Returns the char.                                                |
   //+------------------------------------------------------------------+
   static ushort SingletonChar(const string set)
     {
      if(!(IsSingleton(set) || IsSingletonInverse(set)))
        {
         Print("Tried to get the singleton char out of a non singleton character class."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         DebugBreak();
        }
      return (StringGetCharacter(set,SETSTART));
     }
   //+------------------------------------------------------------------+
   //| IsMergeable.                                                     |
   //+------------------------------------------------------------------+
   static bool IsMergeable(const string charClass)
     {
      return (!IsNegated(charClass) && !IsSubtraction(charClass));
     }
   //+------------------------------------------------------------------+
   //| IsEmpty.                                                         |
   //+------------------------------------------------------------------+
   static bool IsEmpty(const string charClass)
     {
      if(StringGetCharacter(charClass,CATEGORYLENGTH)==0 && 
         StringGetCharacter(charClass,FLAGS)==0 && 
         StringGetCharacter(charClass,SETLENGTH)==0
         && !IsSubtraction(charClass))
        {
         return (true);
        }
      else
        {
         return (false);
        }
     }
   //+------------------------------------------------------------------+
   //| True if the set contains a single character only.                |
   //+------------------------------------------------------------------+
   static bool IsSingleton(const string set)
     {
      if(StringGetCharacter(set,FLAGS)==0 && StringGetCharacter(set,CATEGORYLENGTH)==0 && 
         StringGetCharacter(set,SETLENGTH)==2 && !IsSubtraction(set) &&
         (StringGetCharacter(set,SETSTART)==Lastchar || StringGetCharacter(set,SETSTART)+1==StringGetCharacter(set,SETSTART+1)))
        {
         return (true);
        }
      else
        {
         //--- return result
         return (false);
        }
     }
   //+------------------------------------------------------------------+
   //| IsSingletonInverse.                                              |
   //+------------------------------------------------------------------+
   static bool IsSingletonInverse(const string set)
     {
      if(StringGetCharacter(set,FLAGS)==1 && StringGetCharacter(set,CATEGORYLENGTH)==0 && 
         StringGetCharacter(set,SETLENGTH)==2 && !IsSubtraction(set) &&
         (StringGetCharacter(set,SETSTART)==Lastchar || StringGetCharacter(set,SETSTART)+1==StringGetCharacter(set,SETSTART+1)))
        {
         return (true);
        }
      else
        {
         return (false);
        }
     }
private:
   //+------------------------------------------------------------------+
   //| IsSubtraction.                                                   |
   //+------------------------------------------------------------------+
   static bool IsSubtraction(const string charClass)
     {
      return (StringLen(charClass) > SETSTART + StringGetCharacter(charClass,SETLENGTH) + StringGetCharacter(charClass,CATEGORYLENGTH));
     }
public:
   //+------------------------------------------------------------------+
   //| IsNegated.                                                       |
   //+------------------------------------------------------------------+
   static bool IsNegated(const string set)
     {
      return (StringLen(set) != NULL && StringGetCharacter(set,FLAGS) == 1);
     }
   //+------------------------------------------------------------------+
   //| IsECMAWordChar.                                                  |
   //+------------------------------------------------------------------+
   static bool IsECMAWordChar(const ushort ch)
     {
      //--- According to ECMA-262, \s, \S, ., ^, and $ use Unicode-based interpretations of
      //--- whitespace and newline, while \d, \D\, \w, \W, \b, and \B use ASCII-only 
      //--- interpretations of digit, word character, and word boundary.  In other words,
      //--- no special treatment of Unicode ZERO WIDTH NON-JOINER (ZWNJ U+200C) and 
      //--- ZERO WIDTH JOINER (ZWJ U+200D) is required for ECMA word boundaries.
      return CharInClass(ch, ECMAWordClass);
     }
   //+------------------------------------------------------------------+
   //| IsWordChar.                                                      |
   //+------------------------------------------------------------------+
   static bool IsWordChar(const ushort ch)
     {
      //--- According to UTS#18 Unicode Regular Expressions (http://www.unicode.org/reports/tr18/)
      //--- RL 1.4 Simple Word Boundaries  The class of <word_character> includes all Alphabetic
      //--- values from the Unicode character database, from UnicodeData.txt [UData], plus the U+200C
      //--- ZERO WIDTH NON-JOINER and U+200D ZERO WIDTH JOINER.
      return CharInClass(ch, WordClass) || ch == ZeroWidthJoiner || ch == ZeroWidthNonJoiner;
     }
   //+------------------------------------------------------------------+
   //| CharInClass.                                                     |
   //+------------------------------------------------------------------+
   static bool CharInClass(const ushort ch,const string set)
     {
      return CharInClassRecursive(ch, set, 0);
     }
   //+------------------------------------------------------------------+
   //| CharInClassRecursive.                                            |
   //+------------------------------------------------------------------+
   static bool CharInClassRecursive(const ushort ch,const string set,const int start)
     {
      int mySetLength=StringGetCharacter(set,start+SETLENGTH);
      int myCategoryLength=StringGetCharacter(set,start+CATEGORYLENGTH);
      int myEndPosition=start+SETSTART+mySetLength+myCategoryLength;
      bool subtracted=false;
      if(StringLen(set)>myEndPosition)
        {
         subtracted=CharInClassRecursive(ch,set,myEndPosition);
        }
      bool b=CharInClassInternal(ch,set,start,mySetLength,myCategoryLength);
      //--- Note that we apply the negation *before* performing the subtraction.  
      //--- This is because the negation only applies to the first char class, not the entire subtraction. 
      if(StringGetCharacter(set,start+FLAGS)==1)
        {
         b=!b;
        }
      bool res=(b && !subtracted);
      return (b && !subtracted);
     }
private:
   //+------------------------------------------------------------------+
   //| Determines a character's membership in a character class (via the|
   //| string representation of the class).                             |
   //+------------------------------------------------------------------+
   static bool CharInClassInternal(const ushort ch,const string set,const int start,const int mySetLength,const int myCategoryLength)
     {
      int min;
      int max;
      int mid;
      min = start + SETSTART;
      max = min + mySetLength;
      while(min!=max)
        {
         mid=(min+max)/2;
         if((ushort)ch<StringGetCharacter(set,mid))
           {
            max=mid;
           }
         else
           {
            min=mid+1;
           }
        }
      //+------------------------------------------------------------------+
      //| The starting position of the set within the character class      |
      //| determines whether what an odd or even ending position means. If |
      //| the start is odd, an *even* ending position means the character  |
      //| was in the set.  With recursive subtractions in the mix, the     |
      //| starting position = start+SETSTART.  Since we know that SETSTART |
      //| is odd, we can simplify it out of the equation.  But if it       |
      //| changes we need to reverse this check.                           |
      //+------------------------------------------------------------------+ 
      if((SETSTART  &0x1)!=1)
        {
         Print("If SETSTART is not odd, the calculation below this will be reversed."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         DebugBreak();
        }
      if((min  &0x1)==(start  &0x1))
        {
         //--- return 
         return (true);
        }
      else
        {
         if(myCategoryLength==0)
           {
            //--- return 
            return (false);
           }
         //--- return 
         return CharInCategory(ch, set, start, mySetLength, myCategoryLength);
        }
     }
   //+------------------------------------------------------------------+
   //| CharInCategory.                                                  |
   //+------------------------------------------------------------------+
   static bool CharInCategory(const ushort ch,const string set,const int start,const int mySetLength,const int myCategoryLength)
     {
      UnicodeCategory chcategory=CCharUnicodeInfo::GetUnicodeCategory(ch);
      int i=start+SETSTART+mySetLength;
      int end=i+myCategoryLength;
      while(i<end)
        {
         int curcat=(short)StringGetCharacter(set,i);
         if(curcat==0)
           {
            //--- zero is our marker for a group of categories - treated as a unit
            if(CharInCategoryGroup(ch,chcategory,set,i))
              {
               //--- return 
               return (true);
              }
           }
         else if(curcat>0)
           {
            //--- greater than zero is a positive case
            if(curcat==SpaceConst)
              {
               if(CCharUnicodeInfo::IsWhiteSpace(ch))
                 {
                  //--- return 
                  return (true);
                 }
               else
                 {
                  i++;
                  continue;
                 }
              }
            --curcat;
            if(chcategory==(UnicodeCategory) curcat)
              {
               //--- return 
               return (true);
              }
           }
         else
           {
            //--- less than zero is a negative case
            if(curcat==NotSpaceConst)
              {
               if(!CCharUnicodeInfo::IsWhiteSpace(ch))
                 {
                  //--- return 
                  return true;
                 }
               else
                 {
                  i++;
                  continue;
                 }
              }
            curcat=-1-curcat;
            if(chcategory!=(UnicodeCategory) curcat)
              {

               //--- return 
               return (true);
              }
           }
         i++;
        }
      //--- return 
      return (false);
     }
   //+------------------------------------------------------------------+
   //| This is used for categories which are composed of other          |
   //| categories - L, N, Z, W... These groups need special treatment   |
   //| when they are negated.                                           |
   //+------------------------------------------------------------------+
   static bool CharInCategoryGroup(const ushort ch,const UnicodeCategory chcategory,const string category,int &i)
     {
      i++;
      int curcat=(short)StringGetCharacter(category,i);
      if(curcat>0)
        {
         //--- positive case - the character must be in ANY of the categories in the group
         bool answer=false;
         while(curcat!=0)
           {
            if(!answer)
              {
               --curcat;
               if(chcategory==(UnicodeCategory) curcat)
                 {
                  answer=true;
                 }
              }
            i++;
            curcat=(short)StringGetCharacter(category,i);
           }
         //--- return 
         return (answer);
        }
      else
        {
         //--- negative case - the character must be in NONE of the categories in the group
         bool answer=true;
         while(curcat!=0)
           {
            if(answer)
              {
               curcat=-1-curcat;
               if(chcategory==(UnicodeCategory) curcat)
                 {
                  answer=false;
                 }
              }
            i++;
            curcat=(short)StringGetCharacter(category,i);
           }
         //--- return 
         return (answer);
        }
     }
public:
   //+------------------------------------------------------------------+
   //| NegateCategory.                                                  |
   //+------------------------------------------------------------------+
   static string NegateCategory(const string category)
     {
      if(StringLen(category)==NULL)
        {
         return (NULL);
        }
      string text="";
      for(int i=0; i<StringLen(category); i++)
        {
         short ch=(short)StringGetCharacter(category,i);
         if(ch==0)
           {
            text+="\0";
           }
         else
           {
            text+=ShortToString((ushort)-ch);
           }
        }
      return (text);
     }
   //+------------------------------------------------------------------+
   //| Parse.                                                           |
   //+------------------------------------------------------------------+
   static CRegexCharClass *Parse(string const charClass)
     {
      return ParseRecursive(charClass, 0);
     }
private:
   //+------------------------------------------------------------------+
   //| ParseRecursive.                                                  |
   //+------------------------------------------------------------------+
   static CRegexCharClass *ParseRecursive(const string charClass,const int start)
     {
      int mySetLength=StringGetCharacter(charClass,start+SETLENGTH);
      int myCategoryLength=StringGetCharacter(charClass,start+CATEGORYLENGTH);
      int myEndPosition=start+SETSTART+mySetLength+myCategoryLength;
      List<CSingleRange*>*ranges=new List<CSingleRange*>(mySetLength);
      int i=start+SETSTART;
      int end=i+mySetLength;
      while(i<end)
        {
         ushort first=StringGetCharacter(charClass,i);
         i++;
         ushort last;
         if(i<end)
           {
            last=(StringGetCharacter(charClass,i)-1);
           }
         else
           {
            last=Lastchar;
           }
         i++;
         ranges.Add(new CSingleRange(first,last));
        }
      CRegexCharClass *sub=NULL;
      if(StringLen(charClass)>myEndPosition)
        {
         sub=ParseRecursive(charClass,myEndPosition);
        }
      return new CRegexCharClass(StringGetCharacter(charClass,start+FLAGS) == 1, ranges, StringSubstr(charClass,end, myCategoryLength), sub);
     }
   //+------------------------------------------------------------------+
   //| The number of single ranges that have been accumulated so far.   |
   //+------------------------------------------------------------------+
   int RangeCount()
     {
      return (m_rangelist.Count());
     }
public:
   //+------------------------------------------------------------------+
   //| Constructs the string representation of the class.               |
   //+------------------------------------------------------------------+
   string ToStringClass()
     {
      if(!m_canonical)
        {
         Canonicalize();
        }
      //--- Make a guess about the length of the ranges.  We'll update this at the end. 
      //--- This is important because if the last range ends in LastChar, we won't append LastChar to the list. 
      int rangeLen=m_rangelist.Count()*2;
      string text;
      int flags;
      if(m_negate)
        {
         flags=1;
        }
      else
        {
         flags=0;
        }
      text+=(flags==0) ? "\0" : ShortToString((ushort)flags);
      text+=(rangeLen==0) ? "\0" : ShortToString((ushort)rangeLen);
      text+=(StringLen(m_categories)==0) ? "\0" : ShortToString((ushort)StringLen(m_categories));
      for(int i=0; i<m_rangelist.Count(); i++)
        {
         CSingleRange *currentRange=m_rangelist[i];
         text+=(currentRange.First()==0) ? "\0" : ShortToString((currentRange.First()));
         if(currentRange.Last()!=Lastchar)
           {
            text+=(currentRange.Last()+1==0) ? "\0" :(ShortToString((short)(currentRange.Last()+1)));
           }
        }
      int val=(StringLen(text)-SETSTART);
      if(val==0)
        {
         text=StringSubstr(text,0,SETLENGTH)+"\0"+StringSubstr(text,SETLENGTH+1);
        }
      else
        {
         StringSetCharacter(text,SETLENGTH,(ushort)(StringLen(text)-SETSTART));
        }
      text+=m_categories;
      if(m_subtractor!=NULL)
        {
         text+=(m_subtractor.ToStringClass());
        }
      return (text);
     }
private:
   //+------------------------------------------------------------------+
   //| The ith range.                                                   |
   //+------------------------------------------------------------------+
   CSingleRange *GetRangeAt(const int i)
     {
      return (m_rangelist[i]);
     }
   //+------------------------------------------------------------------+
   //| Logic to reduce a character class to a unique, sorted form.      |
   //+------------------------------------------------------------------+
   void Canonicalize()
     {
      CSingleRange *CurrentRange;
      int i;
      int j;
      ushort last;
      bool Done;
      m_canonical=true;
      CSingleRangeComparer comparer();
      m_rangelist.Sort(0,m_rangelist.Count(),GetPointer(comparer));
      //--- Find and eliminate overlapping or abutting ranges
      if(m_rangelist.Count()>1)
        {
         Done=false;
         for(i=1,j=0;; i++)
           {
            for(last=m_rangelist[j].Last();; i++)
              {
               if(i==m_rangelist.Count() || last==Lastchar)
                 {
                  Done=true;
                  break;
                 }
               if((CurrentRange=m_rangelist[i]).First()>last+1)
                 {
                  break;
                 }
               if(last<CurrentRange.Last())
                 {
                  last=CurrentRange.Last();
                 }
              }
            m_rangelist[j].Last(last);
            j++;
            if(Done)
              {
               break;
              }
            if(j<i)
              {
               if(m_rangelist.IndexOf(m_rangelist[j])==m_rangelist.LastIndexOf(m_rangelist[j]))
                 {
                  delete m_rangelist[j];
                 }
               m_rangelist.Set(j,m_rangelist[i]);
              }
           }
         for(int t=j; t<m_rangelist.Count(); t++)
           {
            if(m_rangelist.IndexOf(m_rangelist[t])>=j)
              {
               delete m_rangelist[t];
              }
           }
         m_rangelist.RemoveRange(j,m_rangelist.Count()-j);
        }
     }
   //+------------------------------------------------------------------+
   //| SetFromProperty.                                                 |
   //+------------------------------------------------------------------+
   static string SetFromProperty(const string capname,const bool invert,const string pattern)
     {
      int min = 0;
      int max = 112;
      while(min!=max)
        {
         int mid = (min + max) / 2;
         int res = StringCompare(capname, m_propTable[mid][0]);
         if(res<0)
           {
            max=mid;
           }
         else if(res>0)
           {
            min=mid+1;
           }
         else
           {
            string set=m_propTable[mid][1];
            if(StringLen(set)==NULL)
              {
               Print("Found a null/empty element in CRegexCharClass prop table."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
               DebugBreak();
              }
            if(invert)
              {
               if(StringGetCharacter(set,0)==Nullchar)
                 {
                  //--- return 
                  return StringSubstr(set,1);
                 }
               //--- return 
               return ((string)Nullchar + set);
              }
            else
              {
               //--- return 
               return (set);
              }
           }
        }
      Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
      return (NULL);
     }
#ifdef _DEBUG
   static const char Hex[];
   static const string Categories[];
public:
   //+------------------------------------------------------------------+
   //| Produces a human-readable description for a set string.          |
   //+------------------------------------------------------------------+
   static string SetDescription(const string set)
     {
      int mySetLength=StringGetCharacter(set,SETLENGTH);
      int myCategoryLength=StringGetCharacter(set,CATEGORYLENGTH);
      int myEndPosition=SETSTART+mySetLength+myCategoryLength;
      string desc=("[");
      int index=SETSTART;
      ushort ch1;
      ushort ch2;
      if(IsNegated(set))
        {
         desc+=(string)('^');
        }
      while(index<SETSTART+StringGetCharacter(set,SETLENGTH))
        {
         ch1=StringGetCharacter(set,index);
         if(index+1<StringLen(set))
           {
            ch2=(StringGetCharacter(set,index+1)-1);
           }
         else
           {
            ch2=Lastchar;
           }
         desc+=(CharDescription(ch1));
         if(ch2!=ch1)
           {
            if(ch1+1!=ch2)
              {
               desc+=(string)('-');
              }
            desc+=(CharDescription(ch2));
           }
         index+=2;
        }
      while(index<SETSTART+StringGetCharacter(set,SETLENGTH)+StringGetCharacter(set,CATEGORYLENGTH))
        {
         ch1=StringGetCharacter(set,index);
         if(ch1==0)
           {
            bool found=false;
            int lastindex= StringFind(set,GroupChar,index+1);
            string group = StringSubstr(set,index,lastindex-index+1);
            DictionaryEnumerator<string,string>*en=m_definedCategories.GetEnumerator();
            while(en.MoveNext())
              {
               if(group==en.Value())
                 {
                  if((short) StringGetCharacter(set,index+1)>0)
                    {
                     desc+=("\\p{"+en.Key()+"}");
                    }
                  else
                    {
                     desc+=("\\P{"+en.Key()+"}");
                    }
                  found=true;
                  break;
                 }
              }
            delete en;
            if(!found)
              {
               if(group==Word)
                 {
                  desc+=("\\w");
                 }
               else if(group==NotWord)
                 {
                  desc+=("\\W");
                 }
               else
                 {
                  Print("Couldn't find a goup to match '"+group+"'."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
                  DebugBreak();
                 }
              }
            index=lastindex;
           }
         else
           {
            desc+=(CategoryDescription(ch1));
           }
         index++;
        }
      if(StringLen(set)>myEndPosition)
        {
         desc+=(string)('-');
         desc+=(SetDescription(StringSubstr(set,myEndPosition)));
        }
      desc+=(string)(']');
      //--- return result
      return (desc);
     }
   //+------------------------------------------------------------------+
   //| Produces a human-readable description for a single character.    |
   //+------------------------------------------------------------------+
   static string CharDescription(const ushort ch)
     {
      string text;
      int shift;
      if(ch=='\\')
        {
         //--- return 
         return "\\\\";
        }
      if(ch>=' ' && ch<='~')
        {
         text+=(string)(ch);
         //--- return 
         return (text);
        }
      if((int)ch<256)
        {
         text+=("\\x");
         shift=8;
        }
      else
        {
         text+=("\\u");
         shift=16;
        }
      while(shift>0)
        {
         shift-=4;
         text+=((string)Hex[(ch>>shift) &0xF]);
        }
      //--- return 
      return (text);
     }
private:
   //+------------------------------------------------------------------+
   //| CategoryDescription.                                             |
   //+------------------------------------------------------------------+
   static string CategoryDescription(const ushort ch)
     {
      if(ch==SpaceConst)
        {
         return ("\\s");
        }
      else if((short) ch==NotSpaceConst)
        {
         return ("\\S");
        }
      else if((short) ch<0)
        {
         return ("\\P{" + Categories[(- ((short)ch) - 1)] + "}");
        }
      else
        {
         return ("\\p{" + Categories[(ch - 1)] + "}");
        }
     }
#endif
  };
#ifdef  _DEBUG
static const char CRegexCharClass::Hex[]={'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
static const string CRegexCharClass::Categories[]=
  {
   "Lu","Ll","Lt","Lm","Lo","__InternalRegexIgnoreCase__",
   "Mn","Mc","Me",
   "Nd","Nl","No",
   "Zs","Zl","Zp",
   "Cc","Cf","Cs","Co",
   "Pc","Pd","Ps","Pe","Pi","Pf","Po",
   "Sm","Sc","Sk","So",
   "Cn"
  };
#endif
static const string CRegexCharClass::InternalRegexIgnoreCase="__InternalRegexIgnoreCase__";
static const string CRegexCharClass::Space="\x64";
static const string CRegexCharClass::NotSpace=CRegexCharClass::NegateCategory(CRegexCharClass::Space);
static const int CRegexCharClass::LowercaseSet=0;
static const int CRegexCharClass::LowercaseAdd=1;
static const int CRegexCharClass::LowercaseBor=2;
static const int CRegexCharClass::LowercaseBad=3;
static const int  CRegexCharClass::FLAGS=0;
static const int  CRegexCharClass::SETLENGTH=1;
static const int  CRegexCharClass::CATEGORYLENGTH=2;
static const int  CRegexCharClass::SETSTART=3;
static const char CRegexCharClass::Nullchar='\0';
static const ushort CRegexCharClass::Lastchar=0xFFFF;
static const string CRegexCharClass::GroupChar="\x0";
static const short CRegexCharClass::SpaceConst=100;
static const short CRegexCharClass::NotSpaceConst=-100;
static const ushort CRegexCharClass::ZeroWidthJoiner=0x200D;
static const ushort CRegexCharClass::ZeroWidthNonJoiner=0x200C;
static const string CRegexCharClass::ECMASpaceSet=ShortToString(0x0009)+ShortToString(0x000E)+ShortToString(0x0020)+ShortToString(0x0021);
static const string CRegexCharClass::NotECMASpaceSet="\0"+ShortToString(0x0009)+ShortToString(0x000E)+ShortToString(0x0020)+ShortToString(0x0021);
static const string CRegexCharClass::ECMAWordSet=ShortToString(0x0030)+ShortToString(0x003A)+ShortToString(0x0041)+ShortToString(0x005B)+ShortToString(0x005F)+ShortToString(0x0060)+ShortToString(0x0061)+ShortToString(0x007B)+ShortToString(0x0130)+ShortToString(0x0131);
static const string CRegexCharClass::NotECMAWordSet="\0"+ShortToString(0x0030)+ShortToString(0x003A)+ShortToString(0x0041)+ShortToString(0x005B)+ShortToString(0x005F)+ShortToString(0x0060)+ShortToString(0x0061)+ShortToString(0x007B)+ShortToString(0x0130)+ShortToString(0x0131);
static const string CRegexCharClass::ECMADigitSet=ShortToString(0x0030)+ShortToString(0x003A);
static const string CRegexCharClass::NotECMADigitSet="\0"+ShortToString(0x0030)+ShortToString(0x003A);
static const string CRegexCharClass::ECMASpaceClass=ShortToString(0x0000)+ShortToString(0x0004)+ShortToString(0x0000)+CRegexCharClass::ECMASpaceSet;
static const string CRegexCharClass::NotECMASpaceClass=ShortToString(0x0001)+ShortToString(0x0004)+ShortToString(0x0000)+CRegexCharClass::ECMASpaceSet;
static const string CRegexCharClass::ECMAWordClass=ShortToString(0x0000)+ShortToString(0x000A)+ShortToString(0x0000)+CRegexCharClass::ECMAWordSet;
static const string CRegexCharClass::NotECMAWordClass=ShortToString(0x0001)+ShortToString(0x00A)+ShortToString(0x0000)+CRegexCharClass::ECMAWordSet;
static const string CRegexCharClass::ECMADigitClass=ShortToString(0x0000)+ShortToString(0x0002)+ShortToString(0x0000)+CRegexCharClass::ECMADigitSet;
static const string CRegexCharClass::NotECMADigitClass=ShortToString(0x0001)+ShortToString(0x0002)+ShortToString(0x0000)+CRegexCharClass::ECMADigitSet;
static const string CRegexCharClass::AnyClass=ShortToString(0x0000)+ShortToString(0x0001)+ShortToString(0x0000)+ShortToString(0x0000);
static const string CRegexCharClass::EmptyClass=ShortToString(0x0000)+ShortToString(0x0000)+ShortToString(0x0000);
static const string CRegexCharClass::m_propTable[112][2]=
  {
     {"IsAlphabeticPresentationForms",       "\xFB00\xFB50"},
     {"IsArabic",                            "\x0600\x0700"},
     {"IsArabicPresentationForms-A",         "\xFB50\xFE00"},
     {"IsArabicPresentationForms-B",         "\xFE70\xFF00"},
     {"IsArmenian",                          "\x0530\x0590"},
     {"IsArrows",                            "\x2190\x2200"},
     {"IsBasicLatin",                        "\x0000\x0080"},
     {"IsBengali",                           "\x0980\x0A00"},
     {"IsBlockElements",                     "\x2580\x25A0"},
     {"IsBopomofo",                          "\x3100\x3130"},
     {"IsBopomofoExtended",                  "\x31A0\x31C0"},
     {"IsBoxDrawing",                        "\x2500\x2580"},
     {"IsBraillePatterns",                   "\x2800\x2900"},
     {"IsBuhid",                             "\x1740\x1760"},
     {"IsCJKCompatibility",                  "\x3300\x3400"},
     {"IsCJKCompatibilityForms",             "\xFE30\xFE50"},
     {"IsCJKCompatibilityIdeographs",        "\xF900\xFB00"},
     {"IsCJKRadicalsSupplement",             "\x2E80\x2F00"},
     {"IsCJKSymbolsandPunctuation",          "\x3000\x3040"},
     {"IsCJKUnifiedIdeographs",              "\x4E00\xA000"},
     {"IsCJKUnifiedIdeographsExtensionA",    "\x3400\x4DC0"},
     {"IsCherokee",                          "\x13A0\x1400"},
     {"IsCombiningDiacriticalMarks",         "\x0300\x0370"},
     {"IsCombiningDiacriticalMarksforSymbols","\x20D0\x2100"},
     {"IsCombiningHalfMarks",                "\xFE20\xFE30"},
     {"IsCombiningMarksforSymbols",          "\x20D0\x2100"},
     {"IsControlPictures",                   "\x2400\x2440"},
     {"IsCurrencySymbols",                   "\x20A0\x20D0"},
     {"IsCyrillic",                          "\x0400\x0500"},
     {"IsCyrillicSupplement",                "\x0500\x0530"},
     {"IsDevanagari",                        "\x0900\x0980"},
     {"IsDingbats",                          "\x2700\x27C0"},
     {"IsEnclosedAlphanumerics",             "\x2460\x2500"},
     {"IsEnclosedCJKLettersandMonths",       "\x3200\x3300"},
     {"IsEthiopic",                          "\x1200\x1380"},
     {"IsGeneralPunctuation",                "\x2000\x2070"},
     {"IsGeometricShapes",                   "\x25A0\x2600"},
     {"IsGeorgian",                          "\x10A0\x1100"},
     {"IsGreek",                             "\x0370\x0400"},
     {"IsGreekExtended",                     "\x1F00\x2000"},
     {"IsGreekandCoptic",                    "\x0370\x0400"},
     {"IsGujarati",                          "\x0A80\x0B00"},
     {"IsGurmukhi",                          "\x0A00\x0A80"},
     {"IsHalfwidthandFullwidthForms",        "\xFF00\xFFF0"},
     {"IsHangulCompatibilityJamo",           "\x3130\x3190"},
     {"IsHangulJamo",                        "\x1100\x1200"},
     {"IsHangulSyllables",                   "\xAC00\xD7B0"},
     {"IsHanunoo",                           "\x1720\x1740"},
     {"IsHebrew",                            "\x0590\x0600"},
     {"IsHighPrivateUseSurrogates",          "\xDB80\xDC00"},
     {"IsHighSurrogates",                    "\xD800\xDB80"},
     {"IsHiragana",                          "\x3040\x30A0"},
     {"IsIPAExtensions",                     "\x0250\x02B0"},
     {"IsIdeographicDescriptionCharacters",  "\x2FF0\x3000"},
     {"IsKanbun",                            "\x3190\x31A0"},
     {"IsKangxiRadicals",                    "\x2F00\x2FE0"},
     {"IsKannada",                           "\x0C80\x0D00"},
     {"IsKatakana",                          "\x30A0\x3100"},
     {"IsKatakanaPhoneticExtensions",        "\x31F0\x3200"},
     {"IsKhmer",                             "\x1780\x1800"},
     {"IsKhmerSymbols",                      "\x19E0\x1A00"},
     {"IsLao",                               "\x0E80\x0F00"},
     {"IsLatin-1Supplement",                 "\x0080\x0100"},
     {"IsLatinExtended-A",                   "\x0100\x0180"},
     {"IsLatinExtended-B",                   "\x0180\x0250"},
     {"IsLatinExtendedAdditional",           "\x1E00\x1F00"},
     {"IsLetterlikeSymbols",                 "\x2100\x2150"},
     {"IsLimbu",                             "\x1900\x1950"},
     {"IsLowSurrogates",                     "\xDC00\xE000"},
     {"IsMalayalam",                         "\x0D00\x0D80"},
     {"IsMathematicalOperators",             "\x2200\x2300"},
     {"IsMiscellaneousMathematicalSymbols-A","\x27C0\x27F0"},
     {"IsMiscellaneousMathematicalSymbols-B","\x2980\x2A00"},
     {"IsMiscellaneousSymbols",              "\x2600\x2700"},
     {"IsMiscellaneousSymbolsandArrows",     "\x2B00\x2C00"},
     {"IsMiscellaneousTechnical",            "\x2300\x2400"},
     {"IsMongolian",                         "\x1800\x18B0"},
     {"IsMyanmar",                           "\x1000\x10A0"},
     {"IsNumberForms",                       "\x2150\x2190"},
     {"IsOgham",                             "\x1680\x16A0"},
     {"IsOpticalCharacterRecognition",       "\x2440\x2460"},
     {"IsOriya",                             "\x0B00\x0B80"},
     {"IsPhoneticExtensions",                "\x1D00\x1D80"},
     {"IsPrivateUse",                        "\xE000\xF900"},
     {"IsPrivateUseArea",                    "\xE000\xF900"},
     {"IsRunic",                             "\x16A0\x1700"},
     {"IsSinhala",                           "\x0D80\x0E00"},
     {"IsSmallFormVariants",                 "\xFE50\xFE70"},
     {"IsSpacingModifierLetters",            "\x02B0\x0300"},
     {"IsSpecials",                          "\xFFF0"},
     {"IsSuperscriptsandSubscripts",         "\x2070\x20A0"},
     {"IsSupplementalArrows-A",              "\x27F0\x2800"},
     {"IsSupplementalArrows-B",              "\x2900\x2980"},
     {"IsSupplementalMathematicalOperators", "\x2A00\x2B00"},
     {"IsSyriac",                            "\x0700\x0750"},
     {"IsTagalog",                           "\x1700\x1720"},
     {"IsTagbanwa",                          "\x1760\x1780"},
     {"IsTaiLe",                             "\x1950\x1980"},
     {"IsTamil",                             "\x0B80\x0C00"},
     {"IsTelugu",                            "\x0C00\x0C80"},
     {"IsThaana",                            "\x0780\x07C0"},
     {"IsThai",                              "\x0E00\x0E80"},
     {"IsTibetan",                           "\x0F00\x1000"},
     {"IsUnifiedCanadianAboriginalSyllabics","\x1400\x1680"},
     {"IsVariationSelectors",                "\xFE00\xFE10"},
     {"IsYiRadicals",                        "\xA490\xA4D0"},
     {"IsYiSyllables",                       "\xA000\xA490"},
     {"IsYijingHexagramSymbols",             "\x4DC0\x4E00"},
     {
      "_xmlC",/* Name Char              */   "\x002D\x002F\x0030\x003B\x0041\x005B\x005F\x0060\x0061\x007B\x00B7\x00B8\x00C0\x00D7\x00D8\x00F7\x00F8\x0132\x0134\x013F\x0141\x0149\x014A\x017F\x0180\x01C4\x01CD\x01F1\x01F4\x01F6\x01FA\x0218\x0250\x02A9\x02BB\x02C2\x02D0\x02D2\x0300\x0346\x0360\x0362\x0386\x038B\x038C\x038D\x038E\x03A2\x03A3\x03CF\x03D0\x03D7\x03DA\x03DB\x03DC\x03DD\x03DE\x03DF\x03E0\x03E1\x03E2\x03F4\x0401\x040D\x040E\x0450\x0451\x045D\x045E\x0482\x0483\x0487\x0490\x04C5\x04C7\x04C9\x04CB\x04CD\x04D0\x04EC\x04EE\x04F6\x04F8\x04FA\x0531\x0557\x0559\x055A\x0561\x0587\x0591\x05A2\x05A3\x05BA\x05BB\x05BE\x05BF\x05C0\x05C1\x05C3\x05C4\x05C5\x05D0\x05EB\x05F0\x05F3\x0621\x063B\x0640\x0653\x0660\x066A\x0670\x06B8\x06BA\x06BF\x06C0\x06CF\x06D0\x06D4\x06D5\x06E9\x06EA\x06EE\x06F0\x06FA\x0901\x0904\x0905\x093A\x093C\x094E\x0951\x0955\x0958\x0964\x0966\x0970\x0981\x0984\x0985\x098D\x098F\x0991\x0993\x09A9\x09AA\x09B1\x09B2\x09B3\x09B6\x09BA\x09BC\x09BD\x09BE\x09C5\x09C7\x09C9\x09CB\x09CE\x09D7\x09D8\x09DC"
      +"\x09DE\x09DF\x09E4\x09E6\x09F2\x0A02\x0A03\x0A05\x0A0B\x0A0F\x0A11\x0A13\x0A29\x0A2A\x0A31\x0A32\x0A34\x0A35\x0A37\x0A38\x0A3A\x0A3C\x0A3D\x0A3E\x0A43\x0A47\x0A49\x0A4B\x0A4E\x0A59\x0A5D\x0A5E\x0A5F\x0A66\x0A75\x0A81\x0A84\x0A85\x0A8C\x0A8D\x0A8E\x0A8F\x0A92\x0A93\x0AA9\x0AAA\x0AB1\x0AB2\x0AB4\x0AB5\x0ABA\x0ABC\x0AC6\x0AC7\x0ACA\x0ACB\x0ACE\x0AE0\x0AE1\x0AE6\x0AF0\x0B01\x0B04\x0B05\x0B0D\x0B0F\x0B11\x0B13\x0B29\x0B2A\x0B31\x0B32\x0B34\x0B36\x0B3A\x0B3C\x0B44\x0B47\x0B49\x0B4B\x0B4E\x0B56\x0B58\x0B5C\x0B5E\x0B5F\x0B62\x0B66\x0B70\x0B82\x0B84\x0B85\x0B8B\x0B8E\x0B91\x0B92\x0B96\x0B99\x0B9B\x0B9C\x0B9D\x0B9E\x0BA0\x0BA3\x0BA5\x0BA8\x0BAB\x0BAE\x0BB6\x0BB7\x0BBA\x0BBE\x0BC3\x0BC6\x0BC9\x0BCA\x0BCE\x0BD7\x0BD8\x0BE7\x0BF0\x0C01\x0C04\x0C05\x0C0D\x0C0E\x0C11\x0C12\x0C29\x0C2A\x0C34\x0C35\x0C3A\x0C3E\x0C45\x0C46\x0C49\x0C4A\x0C4E\x0C55\x0C57\x0C60\x0C62\x0C66\x0C70\x0C82\x0C84\x0C85\x0C8D\x0C8E\x0C91\x0C92\x0CA9\x0CAA\x0CB4\x0CB5\x0CBA\x0CBE\x0CC5\x0CC6\x0CC9\x0CCA\x0CCE\x0CD5\x0CD7\x0CDE\x0CDF\x0CE0\x0CE2"
      +"\x0CE6\x0CF0\x0D02\x0D04\x0D05\x0D0D\x0D0E\x0D11\x0D12\x0D29\x0D2A\x0D3A\x0D3E\x0D44\x0D46\x0D49\x0D4A\x0D4E\x0D57\x0D58\x0D60\x0D62\x0D66\x0D70\x0E01\x0E2F\x0E30\x0E3B\x0E40\x0E4F\x0E50\x0E5A\x0E81\x0E83\x0E84\x0E85\x0E87\x0E89\x0E8A\x0E8B\x0E8D\x0E8E\x0E94\x0E98\x0E99\x0EA0\x0EA1\x0EA4\x0EA5\x0EA6\x0EA7\x0EA8\x0EAA\x0EAC\x0EAD\x0EAF\x0EB0\x0EBA\x0EBB\x0EBE\x0EC0\x0EC5\x0EC6\x0EC7\x0EC8\x0ECE\x0ED0\x0EDA\x0F18\x0F1A\x0F20\x0F2A\x0F35\x0F36\x0F37\x0F38\x0F39\x0F3A\x0F3E\x0F48\x0F49\x0F6A\x0F71\x0F85\x0F86\x0F8C\x0F90\x0F96\x0F97\x0F98\x0F99\x0FAE\x0FB1\x0FB8\x0FB9\x0FBA\x10A0\x10C6\x10D0\x10F7\x1100\x1101\x1102\x1104\x1105\x1108\x1109\x110A\x110B\x110D\x110E\x1113\x113C\x113D\x113E\x113F\x1140\x1141\x114C\x114D\x114E\x114F\x1150\x1151\x1154\x1156\x1159\x115A\x115F\x1162\x1163\x1164\x1165\x1166\x1167\x1168\x1169\x116A\x116D\x116F\x1172\x1174\x1175\x1176\x119E\x119F\x11A8\x11A9\x11AB\x11AC\x11AE\x11B0\x11B7\x11B9\x11BA\x11BB\x11BC\x11C3\x11EB\x11EC\x11F0\x11F1\x11F9\x11FA\x1E00\x1E9C\x1EA0\x1EFA\x1F00"
      +"\x1F16\x1F18\x1F1E\x1F20\x1F46\x1F48\x1F4E\x1F50\x1F58\x1F59\x1F5A\x1F5B\x1F5C\x1F5D\x1F5E\x1F5F\x1F7E\x1F80\x1FB5\x1FB6\x1FBD\x1FBE\x1FBF\x1FC2\x1FC5\x1FC6\x1FCD\x1FD0\x1FD4\x1FD6\x1FDC\x1FE0\x1FED\x1FF2\x1FF5\x1FF6\x1FFD\x20D0\x20DD\x20E1\x20E2\x2126\x2127\x212A\x212C\x212E\x212F\x2180\x2183\x3005\x3006\x3007\x3008\x3021\x3030\x3031\x3036\x3041\x3095\x3099\x309B\x309D\x309F\x30A1\x30FB\x30FC\x30FF\x3105\x312D\x4E00\x9FA6\xAC00\xD7A4"
     }
      ,
     {"_xmlD",                                "\x0030\x003A\x0660\x066A\x06F0\x06FA\x0966\x0970\x09E6\x09F0\x0A66\x0A70\x0AE6\x0AF0\x0B66\x0B70\x0BE7\x0BF0\x0C66\x0C70\x0CE6\x0CF0\x0D66\x0D70\x0E50\x0E5A\x0ED0\x0EDA\x0F20\x0F2A\x1040\x104A\x1369\x1372\x17E0\x17EA\x1810\x181A\xFF10\xFF1A"},
     {
      "_xmlI",/* Start Name Char       */    "\x003A\x003B\x0041\x005B\x005F\x0060\x0061\x007B\x00C0\x00D7\x00D8\x00F7\x00F8\x0132\x0134\x013F\x0141\x0149\x014A\x017F\x0180\x01C4\x01CD\x01F1\x01F4\x01F6\x01FA\x0218\x0250\x02A9\x02BB\x02C2\x0386\x0387\x0388\x038B\x038C\x038D\x038E\x03A2\x03A3\x03CF\x03D0\x03D7\x03DA\x03DB\x03DC\x03DD\x03DE\x03DF\x03E0\x03E1\x03E2\x03F4\x0401\x040D\x040E\x0450\x0451\x045D\x045E\x0482\x0490\x04C5\x04C7\x04C9\x04CB\x04CD\x04D0\x04EC\x04EE\x04F6\x04F8\x04FA\x0531\x0557\x0559\x055A\x0561\x0587\x05D0\x05EB\x05F0\x05F3\x0621\x063B\x0641\x064B\x0671\x06B8\x06BA\x06BF\x06C0\x06CF\x06D0\x06D4\x06D5\x06D6\x06E5\x06E7\x0905\x093A\x093D\x093E\x0958\x0962\x0985\x098D\x098F\x0991\x0993\x09A9\x09AA\x09B1\x09B2\x09B3\x09B6\x09BA\x09DC\x09DE\x09DF\x09E2\x09F0\x09F2\x0A05\x0A0B\x0A0F\x0A11\x0A13\x0A29\x0A2A\x0A31\x0A32\x0A34\x0A35\x0A37\x0A38\x0A3A\x0A59\x0A5D\x0A5E\x0A5F\x0A72\x0A75\x0A85\x0A8C\x0A8D\x0A8E\x0A8F\x0A92\x0A93\x0AA9\x0AAA\x0AB1\x0AB2\x0AB4\x0AB5\x0ABA\x0ABD\x0ABE\x0AE0\x0AE1\x0B05\x0B0D\x0B0F"
      +"\x0B11\x0B13\x0B29\x0B2A\x0B31\x0B32\x0B34\x0B36\x0B3A\x0B3D\x0B3E\x0B5C\x0B5E\x0B5F\x0B62\x0B85\x0B8B\x0B8E\x0B91\x0B92\x0B96\x0B99\x0B9B\x0B9C\x0B9D\x0B9E\x0BA0\x0BA3\x0BA5\x0BA8\x0BAB\x0BAE\x0BB6\x0BB7\x0BBA\x0C05\x0C0D\x0C0E\x0C11\x0C12\x0C29\x0C2A\x0C34\x0C35\x0C3A\x0C60\x0C62\x0C85\x0C8D\x0C8E\x0C91\x0C92\x0CA9\x0CAA\x0CB4\x0CB5\x0CBA\x0CDE\x0CDF\x0CE0\x0CE2\x0D05\x0D0D\x0D0E\x0D11\x0D12\x0D29\x0D2A\x0D3A\x0D60\x0D62\x0E01\x0E2F\x0E30\x0E31\x0E32\x0E34\x0E40\x0E46\x0E81\x0E83\x0E84\x0E85\x0E87\x0E89\x0E8A\x0E8B\x0E8D\x0E8E\x0E94\x0E98\x0E99\x0EA0\x0EA1\x0EA4\x0EA5\x0EA6\x0EA7\x0EA8\x0EAA\x0EAC\x0EAD\x0EAF\x0EB0\x0EB1\x0EB2\x0EB4\x0EBD\x0EBE\x0EC0\x0EC5\x0F40\x0F48\x0F49\x0F6A\x10A0\x10C6\x10D0\x10F7\x1100\x1101\x1102\x1104\x1105\x1108\x1109\x110A\x110B\x110D\x110E\x1113\x113C\x113D\x113E\x113F\x1140\x1141\x114C\x114D\x114E\x114F\x1150\x1151\x1154\x1156\x1159\x115A\x115F\x1162\x1163\x1164\x1165\x1166\x1167\x1168\x1169\x116A\x116D\x116F\x1172\x1174\x1175\x1176\x119E\x119F\x11A8\x11A9\x11AB\x11AC"
      +"\x11AE\x11B0\x11B7\x11B9\x11BA\x11BB\x11BC\x11C3\x11EB\x11EC\x11F0\x11F1\x11F9\x11FA\x1E00\x1E9C\x1EA0\x1EFA\x1F00\x1F16\x1F18\x1F1E\x1F20\x1F46\x1F48\x1F4E\x1F50\x1F58\x1F59\x1F5A\x1F5B\x1F5C\x1F5D\x1F5E\x1F5F\x1F7E\x1F80\x1FB5\x1FB6\x1FBD\x1FBE\x1FBF\x1FC2\x1FC5\x1FC6\x1FCD\x1FD0\x1FD4\x1FD6\x1FDC\x1FE0\x1FED\x1FF2\x1FF5\x1FF6\x1FFD\x2126\x2127\x212A\x212C\x212E\x212F\x2180\x2183\x3007\x3008\x3021\x302A\x3041\x3095\x30A1\x30FB\x3105\x312D\x4E00\x9FA6\xAC00\xD7A4"
     }
      ,
     {
      "_xmlW","\x0024\x0025\x002B\x002C\x0030\x003A\x003C\x003F\x0041\x005B\x005E\x005F\x0060\x007B\x007C\x007D\x007E\x007F\x00A2\x00AB\x00AC\x00AD\x00AE\x00B7\x00B8\x00BB\x00BC\x00BF\x00C0\x0221\x0222\x0234\x0250\x02AE\x02B0\x02EF\x0300\x0350\x0360\x0370\x0374\x0376\x037A\x037B\x0384\x0387\x0388\x038B\x038C\x038D\x038E\x03A2\x03A3\x03CF\x03D0\x03F7\x0400\x0487\x0488\x04CF\x04D0\x04F6\x04F8\x04FA\x0500\x0510\x0531\x0557\x0559\x055A\x0561\x0588\x0591\x05A2\x05A3\x05BA\x05BB\x05BE\x05BF\x05C0\x05C1\x05C3\x05C4\x05C5\x05D0\x05EB\x05F0\x05F3\x0621\x063B\x0640\x0656\x0660\x066A\x066E\x06D4\x06D5\x06DD\x06DE\x06EE\x06F0\x06FF\x0710\x072D\x0730\x074B\x0780\x07B2\x0901\x0904\x0905\x093A\x093C\x094E\x0950\x0955\x0958\x0964\x0966\x0970\x0981\x0984\x0985\x098D\x098F\x0991\x0993\x09A9\x09AA\x09B1\x09B2\x09B3\x09B6\x09BA\x09BC\x09BD\x09BE\x09C5\x09C7\x09C9\x09CB\x09CE\x09D7\x09D8\x09DC\x09DE\x09DF\x09E4\x09E6\x09FB\x0A02\x0A03\x0A05\x0A0B\x0A0F\x0A11\x0A13\x0A29\x0A2A\x0A31\x0A32\x0A34\x0A35"
      +"\x0A37\x0A38\x0A3A\x0A3C\x0A3D\x0A3E\x0A43\x0A47\x0A49\x0A4B\x0A4E\x0A59\x0A5D\x0A5E\x0A5F\x0A66\x0A75\x0A81\x0A84\x0A85\x0A8C\x0A8D\x0A8E\x0A8F\x0A92\x0A93\x0AA9\x0AAA\x0AB1\x0AB2\x0AB4\x0AB5\x0ABA\x0ABC\x0AC6\x0AC7\x0ACA\x0ACB\x0ACE\x0AD0\x0AD1\x0AE0\x0AE1\x0AE6\x0AF0\x0B01\x0B04\x0B05\x0B0D\x0B0F\x0B11\x0B13\x0B29\x0B2A\x0B31\x0B32\x0B34\x0B36\x0B3A\x0B3C\x0B44\x0B47\x0B49\x0B4B\x0B4E\x0B56\x0B58\x0B5C\x0B5E\x0B5F\x0B62\x0B66\x0B71\x0B82\x0B84\x0B85\x0B8B\x0B8E\x0B91\x0B92\x0B96\x0B99\x0B9B\x0B9C\x0B9D\x0B9E\x0BA0\x0BA3\x0BA5\x0BA8\x0BAB\x0BAE\x0BB6\x0BB7\x0BBA\x0BBE\x0BC3\x0BC6\x0BC9\x0BCA\x0BCE\x0BD7\x0BD8\x0BE7\x0BF3\x0C01\x0C04\x0C05\x0C0D\x0C0E\x0C11\x0C12\x0C29\x0C2A\x0C34\x0C35\x0C3A\x0C3E\x0C45\x0C46\x0C49\x0C4A\x0C4E\x0C55\x0C57\x0C60\x0C62\x0C66\x0C70\x0C82\x0C84\x0C85\x0C8D\x0C8E\x0C91\x0C92\x0CA9\x0CAA\x0CB4\x0CB5\x0CBA\x0CBE\x0CC5\x0CC6\x0CC9\x0CCA\x0CCE\x0CD5\x0CD7\x0CDE\x0CDF\x0CE0\x0CE2\x0CE6\x0CF0\x0D02\x0D04\x0D05\x0D0D\x0D0E\x0D11\x0D12\x0D29\x0D2A\x0D3A\x0D3E\x0D44\x0D46\x0D49"
      +"\x0D4A\x0D4E\x0D57\x0D58\x0D60\x0D62\x0D66\x0D70\x0D82\x0D84\x0D85\x0D97\x0D9A\x0DB2\x0DB3\x0DBC\x0DBD\x0DBE\x0DC0\x0DC7\x0DCA\x0DCB\x0DCF\x0DD5\x0DD6\x0DD7\x0DD8\x0DE0\x0DF2\x0DF4\x0E01\x0E3B\x0E3F\x0E4F\x0E50\x0E5A\x0E81\x0E83\x0E84\x0E85\x0E87\x0E89\x0E8A\x0E8B\x0E8D\x0E8E\x0E94\x0E98\x0E99\x0EA0\x0EA1\x0EA4\x0EA5\x0EA6\x0EA7\x0EA8\x0EAA\x0EAC\x0EAD\x0EBA\x0EBB\x0EBE\x0EC0\x0EC5\x0EC6\x0EC7\x0EC8\x0ECE\x0ED0\x0EDA\x0EDC\x0EDE\x0F00\x0F04\x0F13\x0F3A\x0F3E\x0F48\x0F49\x0F6B\x0F71\x0F85\x0F86\x0F8C\x0F90\x0F98\x0F99\x0FBD\x0FBE\x0FCD\x0FCF\x0FD0\x1000\x1022\x1023\x1028\x1029\x102B\x102C\x1033\x1036\x103A\x1040\x104A\x1050\x105A\x10A0\x10C6\x10D0\x10F9\x1100\x115A\x115F\x11A3\x11A8\x11FA\x1200\x1207\x1208\x1247\x1248\x1249\x124A\x124E\x1250\x1257\x1258\x1259\x125A\x125E\x1260\x1287\x1288\x1289\x128A\x128E\x1290\x12AF\x12B0\x12B1\x12B2\x12B6\x12B8\x12BF\x12C0\x12C1\x12C2\x12C6\x12C8\x12CF\x12D0\x12D7\x12D8\x12EF\x12F0\x130F\x1310\x1311\x1312\x1316\x1318\x131F\x1320\x1347\x1348\x135B\x1369\x137D\x13A0"
      +"\x13F5\x1401\x166D\x166F\x1677\x1681\x169B\x16A0\x16EB\x16EE\x16F1\x1700\x170D\x170E\x1715\x1720\x1735\x1740\x1754\x1760\x176D\x176E\x1771\x1772\x1774\x1780\x17D4\x17D7\x17D8\x17DB\x17DD\x17E0\x17EA\x180B\x180E\x1810\x181A\x1820\x1878\x1880\x18AA\x1E00\x1E9C\x1EA0\x1EFA\x1F00\x1F16\x1F18\x1F1E\x1F20\x1F46\x1F48\x1F4E\x1F50\x1F58\x1F59\x1F5A\x1F5B\x1F5C\x1F5D\x1F5E\x1F5F\x1F7E\x1F80\x1FB5\x1FB6\x1FC5\x1FC6\x1FD4\x1FD6\x1FDC\x1FDD\x1FF0\x1FF2\x1FF5\x1FF6\x1FFF\x2044\x2045\x2052\x2053\x2070\x2072\x2074\x207D\x207F\x208D\x20A0\x20B2\x20D0\x20EB\x2100\x213B\x213D\x214C\x2153\x2184\x2190\x2329\x232B\x23B4\x23B7\x23CF\x2400\x2427\x2440\x244B\x2460\x24FF\x2500\x2614\x2616\x2618\x2619\x267E\x2680\x268A\x2701\x2705\x2706\x270A\x270C\x2728\x2729\x274C\x274D\x274E\x274F\x2753\x2756\x2757\x2758\x275F\x2761\x2768\x2776\x2795\x2798\x27B0\x27B1\x27BF\x27D0\x27E6\x27F0\x2983\x2999\x29D8\x29DC\x29FC\x29FE\x2B00\x2E80\x2E9A\x2E9B\x2EF4\x2F00\x2FD6\x2FF0\x2FFC\x3004\x3008\x3012\x3014\x3020\x3030\x3031\x303D\x303E\x3040"
      +"\x3041\x3097\x3099\x30A0\x30A1\x30FB\x30FC\x3100\x3105\x312D\x3131\x318F\x3190\x31B8\x31F0\x321D\x3220\x3244\x3251\x327C\x327F\x32CC\x32D0\x32FF\x3300\x3377\x337B\x33DE\x33E0\x33FF\x3400\x4DB6\x4E00\x9FA6\xA000\xA48D\xA490\xA4C7\xAC00\xD7A4\xF900\xFA2E\xFA30\xFA6B\xFB00\xFB07\xFB13\xFB18\xFB1D\xFB37\xFB38\xFB3D\xFB3E\xFB3F\xFB40\xFB42\xFB43\xFB45\xFB46\xFBB2\xFBD3\xFD3E\xFD50\xFD90\xFD92\xFDC8\xFDF0\xFDFD\xFE00\xFE10\xFE20\xFE24\xFE62\xFE63\xFE64\xFE67\xFE69\xFE6A\xFE70\xFE75\xFE76\xFEFD\xFF04\xFF05\xFF0B\xFF0C\xFF10\xFF1A\xFF1C\xFF1F\xFF21\xFF3B\xFF3E\xFF3F\xFF40\xFF5B\xFF5C\xFF5D\xFF5E\xFF5F\xFF66\xFFBF\xFFC2\xFFC8\xFFCA\xFFD0\xFFD2\xFFD8\xFFDA\xFFDD\xFFE0\xFFE7\xFFE8\xFFEF\xFFFC\xFFFE"
     }
      ,
  };
//--- Initialize static members:
static const LowerCaseMapping CRegexCharClass::m_lcTable[94]=
  {
     {(ushort)'\x0041',(ushort)'\x005A',1,32},
     {(ushort)'\x00C0',(ushort)'\x00DE',1,32},
     {(ushort)'\x0100',(ushort)'\x012E',3,0},
     {(ushort)'\x0130',(ushort)'\x0130',0,0x0069},
     {(ushort)'\x0132',(ushort)'\x0136',3,0},
     {(ushort)'\x0139',(ushort)'\x0147',2,0},
     {(ushort)'\x014A',(ushort)'\x0176',3,0},
     {(ushort)'\x0178',(ushort)'\x0178',0,0x00FF},
     {(ushort)'\x0179',(ushort)'\x017D',2,0},
     {(ushort)'\x0181',(ushort)'\x0181',0,0x0253},
     {(ushort)'\x0182',(ushort)'\x0184',3,0},
     {(ushort)'\x0186',(ushort)'\x0186',0,0x0254},
     {(ushort)'\x0187',(ushort)'\x0187',0,0x0188},
     {(ushort)'\x0189',(ushort)'\x018A',1,205},
     {(ushort)'\x018B',(ushort)'\x018B',0,0x018C},
     {(ushort)'\x018E',(ushort)'\x018E',0,0x01DD},
     {(ushort)'\x018F',(ushort)'\x018F',0,0x0259},
     {(ushort)'\x0190',(ushort)'\x0190',0,0x025B},
     {(ushort)'\x0191',(ushort)'\x0191',0,0x0192},
     {(ushort)'\x0193',(ushort)'\x0193',0,0x0260},
     {(ushort)'\x0194',(ushort)'\x0194',0,0x0263},
     {(ushort)'\x0196',(ushort)'\x0196',0,0x0269},
     {(ushort)'\x0197',(ushort)'\x0197',0,0x0268},
     {(ushort)'\x0198',(ushort)'\x0198',0,0x0199},
     {(ushort)'\x019C',(ushort)'\x019C',0,0x026F},
     {(ushort)'\x019D',(ushort)'\x019D',0,0x0272},
     {(ushort)'\x019F',(ushort)'\x019F',0,0x0275},
     {(ushort)'\x01A0',(ushort)'\x01A4',3,0},
     {(ushort)'\x01A7',(ushort)'\x01A7',0,0x01A8},
     {(ushort)'\x01A9',(ushort)'\x01A9',0,0x0283},
     {(ushort)'\x01AC',(ushort)'\x01AC',0,0x01AD},
     {(ushort)'\x01AE',(ushort)'\x01AE',0,0x0288},
     {(ushort)'\x01AF',(ushort)'\x01AF',0,0x01B0},
     {(ushort)'\x01B1',(ushort)'\x01B2',1,217},
     {(ushort)'\x01B3',(ushort)'\x01B5',2,0},
     {(ushort)'\x01B7',(ushort)'\x01B7',0,0x0292},
     {(ushort)'\x01B8',(ushort)'\x01B8',0,0x01B9},
     {(ushort)'\x01BC',(ushort)'\x01BC',0,0x01BD},
     {(ushort)'\x01C4',(ushort)'\x01C5',0,0x01C6},
     {(ushort)'\x01C7',(ushort)'\x01C8',0,0x01C9},
     {(ushort)'\x01CA',(ushort)'\x01CB',0,0x01CC},
     {(ushort)'\x01CD',(ushort)'\x01DB',2,0},
     {(ushort)'\x01DE',(ushort)'\x01EE',3,0},
     {(ushort)'\x01F1',(ushort)'\x01F2',0,0x01F3},
     {(ushort)'\x01F4',(ushort)'\x01F4',0,0x01F5},
     {(ushort)'\x01FA',(ushort)'\x0216',3,0},
     {(ushort)'\x0386',(ushort)'\x0386',0,0x03AC},
     {(ushort)'\x0388',(ushort)'\x038A',1,37},
     {(ushort)'\x038C',(ushort)'\x038C',0,0x03CC},
     {(ushort)'\x038E',(ushort)'\x038F',1,63},
     {(ushort)'\x0391',(ushort)'\x03AB',1,32},
     {(ushort)'\x03E2',(ushort)'\x03EE',3,0},
     {(ushort)'\x0401',(ushort)'\x040F',1,80},
     {(ushort)'\x0410',(ushort)'\x042F',1,32},
     {(ushort)'\x0460',(ushort)'\x0480',3,0},
     {(ushort)'\x0490',(ushort)'\x04BE',3,0},
     {(ushort)'\x04C1',(ushort)'\x04C3',2,0},
     {(ushort)'\x04C7',(ushort)'\x04C7',0,0x04C8},
     {(ushort)'\x04CB',(ushort)'\x04CB',0,0x04CC},
     {(ushort)'\x04D0',(ushort)'\x04EA',3,0},
     {(ushort)'\x04EE',(ushort)'\x04F4',3,0},
     {(ushort)'\x04F8',(ushort)'\x04F8',0,0x04F9},
     {(ushort)'\x0531',(ushort)'\x0556',1,48},
     {(ushort)'\x10A0',(ushort)'\x10C5',1,48},
     {(ushort)'\x1E00',(ushort)'\x1EF8',3,0},
     {(ushort)'\x1F08',(ushort)'\x1F0F',1,-8},
     {(ushort)'\x1F18',(ushort)'\x1F1F',1,-8},
     {(ushort)'\x1F28',(ushort)'\x1F2F',1,-8},
     {(ushort)'\x1F38',(ushort)'\x1F3F',1,-8},
     {(ushort)'\x1F48',(ushort)'\x1F4D',1,-8},
     {(ushort)'\x1F59',(ushort)'\x1F59',0,0x1F51},
     {(ushort)'\x1F5B',(ushort)'\x1F5B',0,0x1F53},
     {(ushort)'\x1F5D',(ushort)'\x1F5D',0,0x1F55},
     {(ushort)'\x1F5F',(ushort)'\x1F5F',0,0x1F57},
     {(ushort)'\x1F68',(ushort)'\x1F6F',1,-8},
     {(ushort)'\x1F88',(ushort)'\x1F8F',1,-8},
     {(ushort)'\x1F98',(ushort)'\x1F9F',1,-8},
     {(ushort)'\x1FA8',(ushort)'\x1FAF',1,-8},
     {(ushort)'\x1FB8',(ushort)'\x1FB9',1,-8},
     {(ushort)'\x1FBA',(ushort)'\x1FBB',1,-74},
     {(ushort)'\x1FBC',(ushort)'\x1FBC',0,0x1FB3},
     {(ushort)'\x1FC8',(ushort)'\x1FCB',1,-86},
     {(ushort)'\x1FCC',(ushort)'\x1FCC',0,0x1FC3},
     {(ushort)'\x1FD8',(ushort)'\x1FD9',1,-8},
     {(ushort)'\x1FDA',(ushort)'\x1FDB',1,-100},
     {(ushort)'\x1FE8',(ushort)'\x1FE9',1,-8},
     {(ushort)'\x1FEA',(ushort)'\x1FEB',1,-112},
     {(ushort)'\x1FEC',(ushort)'\x1FEC',0,0x1FE5},
     {(ushort)'\x1FF8',(ushort)'\x1FF9',1,-128},
     {(ushort)'\x1FFA',(ushort)'\x1FFB',1,-126},
     {(ushort)'\x1FFC',(ushort)'\x1FFC',0,0x1FF3},
     {(ushort)'\x2160',(ushort)'\x216F',1,16},
     {(ushort)'\x24B6',(ushort)'\x24D0',1,26},
     {(ushort)'\xFF21',(ushort)'\xFF3A',1,32},
  };
static Dictionary<string,string>CRegexCharClass::m_definedCategories(32);
static const string CRegexCharClass::Word=CRegexCharClass::Initialize();
static const string CRegexCharClass::NotWord=NegateCategory(CRegexCharClass::Word);
static const string CRegexCharClass::SpaceClass="\0\0\x01"+CRegexCharClass::Space;
static const string CRegexCharClass::NotSpaceClass="\x01\0\x01"+CRegexCharClass::Space;
static const string CRegexCharClass::WordClass="\0\0"+ShortToString((ushort)StringLen(CRegexCharClass::Word))+CRegexCharClass::Word;
static const string CRegexCharClass::NotWordClass=ShortToString('\x01')+"\0"+ShortToString((ushort)(StringLen(CRegexCharClass::Word)))+CRegexCharClass::Word;;
static const string CRegexCharClass::DigitClass="\0\0"+ShortToString('\x01')+ShortToString((ushort)DecimalDigitNumber+1);
static const string CRegexCharClass::NotDigitClass="\0\0"+ShortToString('\x01')+ShortToString((ushort)(-(DecimalDigitNumber+1)));
//+------------------------------------------------------------------+
