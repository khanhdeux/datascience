//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
//+------------------------------------------------------------------+
//| Purpose: The CRegexBoyerMoore object precomputes the Boyer-Moore |
//| tables for fast string scanning. These tables allow you to scan  |
//| for the first occurance of a string within a large body of text  |
//| without examining every character. The performance of the        |
//| heuristic depends on the actual string and the text being        |
//| searched, but usually, the longer the string that is being       |
//| searched for, the fewer characters need to be examined.          |
//+------------------------------------------------------------------+
class CRegexBoyerMoore
  {
private:
   int               m_positive[];
   int               m_negativeASCII[];
   int               m_pos;
   int               m_negativeUnicode[];
   string            m_pattern;
   int               m_lowASCII;
   int               m_highASCII;
   bool              m_rightToLeft;
   bool              m_caseInsensitive;
   static const int  infinite;
public:
   //+------------------------------------------------------------------+
   //| Constructs a Boyer-Moore state machine for searching for the     |
   //| string pattern. The string must not be zero-length.              |
   //+------------------------------------------------------------------+
                     CRegexBoyerMoore(string pattern,const bool caseInsensitive,const bool rightToLeft)
     {
      if(StringLen(pattern)==0)
        {
         Print("CRegexBoyerMoore called with an empty string.  This is bad for perf.");
         DebugBreak();
        }
      int beforefirst;
      int last;
      int bump;
      int examine;
      int scan;
      int match;
      ushort ch;
      //+------------------------------------------------------------------+
      //| We do the ToLower character by character for consistency. With   |
      //| surrogate chars, doing a ToLower on the entire string could      |
      //| actually change the surrogate pair.  This is more correct        |
      //| linguistically, but since Regex doesn't support surrogates, it's |
      //| more important to be consistent.                                 |
      //+------------------------------------------------------------------+
      if(caseInsensitive)
        {
         StringToLower(pattern);
        }
      m_pattern=pattern;
      m_rightToLeft=rightToLeft;
      m_caseInsensitive=caseInsensitive;
      if(!rightToLeft)
        {
         beforefirst=-1;
         last = StringLen(pattern) - 1;
         bump = 1;
        }
      else
        {
         beforefirst=StringLen(pattern);
         last = 0;
         bump = -1;
        }
      //+------------------------------------------------------------------+
      //| PART I - the good-suffix shift table.                            |
      //|                                                                  |
      //| compute the positive requirement:                                |
      //| if char "i" is the first one from the right that doesn't match,  |
      //| then we know the matcher can advance by _positive[i].            |
      //+------------------------------------------------------------------+
      ArrayResize(m_positive,StringLen(pattern));
      ZeroMemory(m_positive);
      examine=last;
      ch=StringGetCharacter(pattern,examine);
      m_positive[examine]=bump;
      examine-=bump;
      for(;;)
        {
         //--- find an  char (examine) that matches the tail
         for(;;)
           {
            if(examine==beforefirst)
              {
               OuterloopBreak(match,examine,ch,pattern,last,bump,beforefirst);
               //--- return
               return;
              }
            if(StringGetCharacter(pattern,examine)==ch)
              {
               break;
              }
            examine-=bump;
           }
         match= last;
         scan = examine;
         //---- find the length of the match
         for(;;)
           {
            if(scan==beforefirst || StringGetCharacter(pattern,match)!=StringGetCharacter(pattern,scan))
              {
               //--- at the end of the match, note the difference in _positive this is not the length of the match,
               //--- but the distance from the  match to the tail suffix. 
               if(m_positive[match]==0)
                 {
                  m_positive[match]=match-scan;
                 }
               break;
              }
            scan-=bump;
            match-=bump;
           }
         examine-=bump;
        }
      OuterloopBreak(match,examine,ch,pattern,last,bump,beforefirst);
     }
   //+------------------------------------------------------------------+
   //| OuterloopBreak.                                                  |
   //+------------------------------------------------------------------+
   void OuterloopBreak(int &match,int &examine,ushort &ch,string &pattern,
                       const int last,const int bump,const int beforefirst)
     {
      match=last-bump;
      //--- scan for the chars for which there are no shifts that yield a different candidate
      while(match!=beforefirst)
        {
         if(m_positive[match]==0)
           {
            m_positive[match]=bump;
           }
         match-=bump;
        }
      //+------------------------------------------------------------------+
      //| PART II - the bad-character shift table.                         |
      //|                                                                  |
      //| compute the negative requirement:                                |
      //| if char "ch" is the reject character when testing position "i",  |
      //| we can slide up by _negative[ch];                                |
      //| (_negative[ch] = str.Length - 1 - str.LastIndexOf(ch))           |
      //|                                                                  |       
      //| the lookup table is divided into ASCII and Unicode portions;     |
      //| only those parts of the Unicode 16-bit code set that actually    |
      //| appear in the string are in the table. (Maximum size with Unicode|
      //| is 65K; ASCII only case is 512 bytes.)                           |
      //+------------------------------------------------------------------+
      ArrayResize(m_negativeASCII,128);
      for(int i=0; i<128; i++)
        {
         m_negativeASCII[i]=last-beforefirst;
        }
      m_lowASCII=127;
      m_highASCII=0;
      for(examine=last; examine!=beforefirst; examine-=bump)
        {
         ch=StringGetCharacter(pattern,examine);
         if((int)ch<128)
           {
            if(m_lowASCII>ch)
              {
               m_lowASCII=ch;
              }
            if(m_highASCII<ch)
              {
               m_highASCII=ch;
              }
            if(m_negativeASCII[ch]==last-beforefirst)
              {
               m_negativeASCII[ch]=last-examine;
              }
           }
         else
           {
            int i = ch >> 8;
            int j = ch & 0xFF;
            m_pos = 256;
            if(ArraySize(m_negativeUnicode)<i*m_pos)
              {
               int newarray[256];
               for(int k=0; k<256; k++)
                 {
                  newarray[k]=last-beforefirst;
                 }
               if(i==0)
                 {
                  ArrayCopy(newarray,m_negativeASCII,0,0,128);
                  ArrayCopy(m_negativeASCII,newarray);
                 }
               ArrayCopy(m_negativeUnicode,newarray,256*i);
              }

            if(m_negativeUnicode[256*i+j]==last-beforefirst)
              {
               m_negativeUnicode[256*i+j]=last-examine;
              }
           }
        }
     }
   //+------------------------------------------------------------------+
   //| MatchPattern.                                                    |
   //+------------------------------------------------------------------+
   bool MatchPattern(const string text,const int index)
     {
      if(m_caseInsensitive)
        {
         if(StringLen(text)-index<StringLen(m_pattern))
           {
            return (false);
           }
         return (true);
        }
      else
        {         
         return(0 == StringCompare(m_pattern,StringSubstr(text,index,StringLen(m_pattern))));
        }
     }
   //+------------------------------------------------------------------+
   //| When a regex is anchored, we can do a quick IsMatch test instead |
   //| of a Scan.                                                       |
   //+------------------------------------------------------------------+
   bool IsMatch(const string text,const int index,const int beglimit,const int endlimit)
     {
      if(!m_rightToLeft)
        {
         if(index<beglimit || endlimit-index<StringLen(m_pattern))
           {
            return (false);
           }
         return MatchPattern(text, index);
        }
      else
        {
         if(index>endlimit || index-beglimit<StringLen(m_pattern))
           {
            return (false);
           }
         return MatchPattern(text, index - StringLen(m_pattern));
        }
     }
   //+------------------------------------------------------------------+
   //| Scan uses the Boyer-Moore algorithm to find the first occurrance |
   //| of the specified string within text,beginning at index,and       |
   //| constrained within beglimit and endlimit.                        |
   //|                                                                  |
   //| The direction and case-sensitivity of the match is determined by |
   //| the arguments to the CRegexBoyerMoore constructor.               |
   //+------------------------------------------------------------------+
   int Scan(string text,const int index,const int beglimit,const int endlimit)
     {
      int test;
      int test2;
      int match;
      int startmatch;
      int endmatch;
      int advance=0;
      int defadv;
      int bump;
      ushort chMatch;
      ushort chTest;
      int unicodeLookup[];
      if(!m_rightToLeft)
        {
         defadv=StringLen(m_pattern);
         startmatch=StringLen(m_pattern)-1;
         endmatch=0;
         test = index + defadv - 1;
         bump = 1;
        }
      else
        {
         defadv=-StringLen(m_pattern);
         startmatch=0;
         endmatch=-defadv-1;
         test = index + defadv;
         bump = -1;
        }
      chMatch=StringGetCharacter(m_pattern,startmatch);
      if(m_caseInsensitive)
        {
         StringToLower(text);
        }
      for(;;)
        {
         if(test>=endlimit || test<beglimit)
           {
            return (-1);
           }
         chTest=StringGetCharacter(text,test);
         if(chTest!=chMatch)
           {
            if((int)chTest<128)
              {
               advance=m_negativeASCII[chTest];
              }
            else if(NULL!=ArraySize(m_negativeUnicode))
              {
               ArrayCopy(unicodeLookup,m_negativeUnicode,0,(chTest>>8)*256,256);
               if(ArraySize(unicodeLookup)!=NULL)
                 {
                  advance=unicodeLookup[chTest  &0xFF];
                 }
              }
            else
              {
               advance=defadv;
              }
            test+=advance;
           }
         else
           {
            test2 = test;
            match = startmatch;
            if(m_caseInsensitive)
              {
               StringToLower(text);
              }
            for(;;)
              {
               if(match==endmatch)
                 {
                  //--- return result
                  return(m_rightToLeft ? test2 + 1 : test2);
                 }
               match -= bump;
               test2 -= bump;
               chTest=StringGetCharacter(text,test2);
               if(chTest!=StringGetCharacter(m_pattern,match))
                 {
                  advance=m_positive[match];
                  if((chTest  &0xFF80)==0)
                    {
                     test2=(match-startmatch)+m_negativeASCII[chTest];
                    }
                  else if(NULL!=ArraySize(m_negativeUnicode))
                    {
                     ArrayCopy(unicodeLookup,m_negativeUnicode,0,256*(chTest>>8),256);
                     if(ArraySize(unicodeLookup)!=NULL)
                       {
                        test2=(match-startmatch)+unicodeLookup[chTest  &0xFF];
                       }
                    }
                  else
                    {
                     test+=advance;
                     break;
                    }
                  if(m_rightToLeft ? test2<advance : test2>advance)
                    {
                     advance=test2;
                    }
                  test+=advance;
                  break;
                 }
              }
           }
        }
      return (0);
     }
public:
   //+------------------------------------------------------------------+
   //| Used when dumping for debugging.                                 |
   //+------------------------------------------------------------------+
   string ToString()
     {
      return (m_pattern);
     }
#ifdef _DEBUG
   //+------------------------------------------------------------------+
   //| Dump.                                                            |
   //+------------------------------------------------------------------+
   string Dump(const string indent)
     {
      string text="";
      text+=indent + "BM Pattern: " + m_pattern + "\n";
      text+=indent + "Positive: ";
      for(int i=0; i<ArraySize(m_positive); i++)
        {
         text+=IntegerToString(m_positive[i])+" ";
        }
      text+=("\n");
      if(ArraySize(m_negativeASCII)!=NULL)
        {
         text+=(indent+"Negative table\n");
         for(int i=0; i<ArraySize(m_negativeASCII); i++)
           {
            if(m_negativeASCII[i]!=StringLen(m_pattern))
              {
               text+=(indent+"  "+CRegex::Escape(IntegerToString(i)+" "+IntegerToString(m_negativeASCII[i])+"\n"));
              }
           }
        }
      return (text);
     }
#endif
  };
static const int CRegexBoyerMoore::infinite=0x7FFFFFFF;
//+------------------------------------------------------------------+
