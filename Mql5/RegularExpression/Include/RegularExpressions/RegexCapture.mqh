//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
#include <Internal\IComparable.mqh>
//+------------------------------------------------------------------+
//| Purpose: Represents the results from a single successful         |
//| subexpression capture.                                           |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| CCapture is just a location/length pair that indicates the       |
//| location of a regular expression match. A single regexp search   |
//| may return multiple CCapture within each capturing RegexGroup.   |
//|                                                                  |
//| Represents the results from a single subexpression capture. The  |
//| object represents one substring for a single successful capture. |
//+------------------------------------------------------------------+
class CCapture : public IComparable
  {
protected:
   string            m_text;
   int               m_index;
   int               m_length;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+   
                     CCapture(const string text,const int i,const int l)
     {
      m_text=text;
      m_index=i;
      m_length=l;
     }
   //+------------------------------------------------------------------+
   //| The position in the original string where the first character of |
   //| the captured substring is found.                                 |
   //+------------------------------------------------------------------+
   int Index()
     {
      return (m_index);
     }
   //+------------------------------------------------------------------+
   //| Gets the length of the captured substring.                       |
   //+------------------------------------------------------------------+
   int Length()
     {
      return (m_length);
     }
   //+------------------------------------------------------------------+
   //| Gets the captured substring from the input string.               |
   //+------------------------------------------------------------------+
   string Value()
     {
      return StringSubstr(m_text,m_index,m_length);
     }
   //+------------------------------------------------------------------+
   //| Retrieves the captured substring from the input string by calling|
   //| the Value() method.                                              |
   //+------------------------------------------------------------------+
   string ToString()
     {
      return Value();
     }
   //+------------------------------------------------------------------+
   //| Gets the original string.                                        |
   //+------------------------------------------------------------------+
   string GetOriginalString()
     {
      return (m_text);
     }
   //+------------------------------------------------------------------+
   //| Gets the substring to the left of the capture.                   |
   //+------------------------------------------------------------------+
   string GetLeftSubstring()
     {
      return StringSubstr(m_text,0,m_index);
     }
   //+------------------------------------------------------------------+
   //| Gets the substring to the right of the capture.                  |
   //+------------------------------------------------------------------+
   string GetRightSubstring()
     {
      return StringSubstr(m_text,m_index + m_length, StringLen(m_text) - m_index - m_length);
     }
#ifdef _DEBUG
   //+------------------------------------------------------------------+
   //| Gets the description.                                            |
   //+------------------------------------------------------------------+
   virtual string Description()
     {
      string text;
      text += ("(I = ");
      text += IntegerToString(m_index);
      text += (", L = ");
      text += IntegerToString(m_length);
      text += ("): ");
      text += StringSubstr(m_text, m_index, m_length);
      return (text);
     }
#endif
  };
//+------------------------------------------------------------------+
