//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
#include "RegexMatch.mqh"
//+------------------------------------------------------------------+
//| Purpose: The CMatchCollection lists the successful matches that  |
//| result when searching a string for a regular expression.         |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| This collection returns a sequence of successful match results,  |
//| either from GetMatchCollection() or GetExecuteCollection(). It   |
//| stops when the first failure is encountered (it does not return  |
//| the failed match).                                               |
//|                                                                  |
//| Represents the set of names appearing as capturing group names in|
//| a regular expression.                                            |
//+------------------------------------------------------------------+
class CMatchCollection : ICollection<CMatch*>
  {
private:
   CRegex            *m_regex;
   bool              m_done;
   string            m_in;
   int               m_beginning;
   int               m_length;
   int               m_startat;
   int               m_prevlen;
   static int        s_infinite;
   List<CMatch*>*m_matches;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CMatchCollection(CRegex *regex,const string in,const int beginning,const int length,const int startat)
     {
      if(startat<0 || startat>StringLen(in))
        {
         Print("Argument 'startat' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      m_regex=regex;
      m_in=in;
      m_beginning=beginning;
      m_length=length;
      m_startat = startat;
      m_prevlen = -1;
      m_matches=new List<CMatch*>();
      m_done=false;
     }
   //--- Destructors:
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~CMatchCollection()
     {
      for(int i=0; i<m_matches.Count(); i++)
        {
         if(CheckPointer(m_matches[i])==POINTER_DYNAMIC)
           {
            delete m_matches[i];
           }
        }
      if(CheckPointer(m_matches)==POINTER_DYNAMIC)
        {
         delete m_matches;
        }
     }
   //+------------------------------------------------------------------+
   //| GetMatch.                                                        |
   //+------------------------------------------------------------------+
   CMatch *GetMatch(int i)
     {
      if(i<0)
        {
         //--- return NULL
         return (NULL);
        }
      if(m_matches.Count()>i)
        {
         //--- return 
         return (m_matches[i]);
        }
      if(m_done)
        {
         //--- return NULL
         return (NULL);
        }
      CMatch *match;
      do
        {
         match=m_regex.Run(false,m_prevlen,m_in,m_beginning,m_length,m_startat);
         if(!match.Success())
           {
            m_done=true;
            //--- return NULL
            return (NULL);
           }
         m_matches.Add(match);
         m_prevlen = match.Length();
         m_startat = match.TextPos();
        }
      while(m_matches.Count()<=i);
      //--- return 
      return (match);
     }
   //+------------------------------------------------------------------+
   //| Returns the number of captures.                                  |
   //+------------------------------------------------------------------+
   int Count()
     {
      if(m_done)
        {
         //--- return 
         return (m_matches.Count());
        }
      GetMatch(s_infinite);
      //--- return 
      return (m_matches.Count());
     }
   //+------------------------------------------------------------------+
   //| Returns the i-th CMatch in the collection.                       |
   //+------------------------------------------------------------------+
   virtual CMatch *operator[](const int i)
     {
      CMatch *match;
      match=GetMatch(i);
      if(match==NULL)
        {
         Print("Argument 'i' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return (match);
     }
   //+------------------------------------------------------------------+
   //| Copies all the elements of the collection to the given array     |
   //| starting at the given index.                                     |
   //+------------------------------------------------------------------+
   void CopyTo(CMatch *&array[],const int arrayIndex)
     {
      m_matches.CopyTo(array,arrayIndex);
     }
   //+------------------------------------------------------------------+
   //| Provides an enumerator in the same order as Item[i].             |
   //+------------------------------------------------------------------+
   IEnumerator<CMatch*>*GetEnumerator()
     {
      return new CMatchEnumerator(GetPointer(this));
     }
  };
static int CMatchCollection::s_infinite=0x7FFFFFFF;
//+------------------------------------------------------------------+
//| Enumerator lists all the group matches.                          |
//+------------------------------------------------------------------+
class CMatchEnumerator : public IEnumerator<CMatch*>
  {
private:
   CMatchCollection *m_matchcoll;
   CMatch            *m_match;
   int               m_curindex;
   bool              m_done;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CMatchEnumerator(CMatchCollection *matchcoll) : m_match(NULL)
     {
      m_matchcoll=matchcoll;
     }
   //+------------------------------------------------------------------+
   //| Advance to the next match.                                       |
   //+------------------------------------------------------------------+
   bool MoveNext()
     {
      if(m_done)
        {
         return (false);
        }
      m_match=m_matchcoll.GetMatch(m_curindex);
      m_curindex++;
      if(m_match==NULL)
        {
         m_done=true;
         return (false);
        }
      return (true);
     }
   //+------------------------------------------------------------------+
   //| The current match.                                               |
   //+------------------------------------------------------------------+
   CMatch *Current()
     {
      if(m_match==NULL)
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return (m_match);
     }
   //+------------------------------------------------------------------+
   //| Position before the first item.                                  |
   //+------------------------------------------------------------------+
   void Reset()
     {
      m_curindex=0;
      m_done=false;
      m_match=NULL;
     }
  };
//+------------------------------------------------------------------+
