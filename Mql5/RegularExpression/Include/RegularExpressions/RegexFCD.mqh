//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
class CRegexTree;
class CRegexNode;
class CRegexFC;
#include "RegexNode.mqh"
#include "RegexTree.mqh"
#include "RegexCode.mqh"
//+------------------------------------------------------------------+
//| This CRegexFCD class is internal to the Regex package. It builds |
//| a bunch of FC information (CRegexFC) about the regex for         |
//| optimization purposes.                                           |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| This step is as simple as walking the tree and emitting sequences|
//| of codes.                                                        |
//+------------------------------------------------------------------+
class CRegexFCD
  {
private:
   int               m_intStack[];
   int               m_intDepth;
   CRegexFC          *m_fcStack[];
   int               m_fcDepth;
   bool              m_skipAllChildren;      // don't process any more children at the current level
   bool              m_skipchild;            // don't process the current child. 
   bool              m_failed;
public:
   //+------------------------------------------------------------------+
   //| This is the one of the only two functions that should be called  |
   //| from outside. It takes a CRegexTree and computes the set of chars|
   //| that can start it.                                               |
   //+------------------------------------------------------------------+
   static CRegexPrefix *FirstChars(CRegexTree *t)
     {
      CRegexFCD s();
      CRegexFC *fc=s.RegexFCFromRegexTree(t);
      if(fc==NULL || fc.NULLable())
        {
         return (NULL);
        }
      CRegexPrefix *prefix=new CRegexPrefix(fc.GetFirstChars(),fc.IsCaseInsensitive());
      return (prefix);
     }
   //+------------------------------------------------------------------+
   //| This is a related computation: it takes a CRegexTree and computes|
   //| the leading substring if it see one. It's quite trivial and gives|
   //| up easily.                                                       |
   //+------------------------------------------------------------------+
   static CRegexPrefix *Prefix(CRegexTree *tree)
     {
      CRegexNode *curNode;
      CRegexNode *concatNode=NULL;
      int nextChild=0;
      curNode=tree.Root();
      for(;;)
        {
         switch(curNode.Type())
           {
            case Concatenate_Type:
              {
               if(curNode.ChildCount()>0)
                 {
                  concatNode= curNode;
                  nextChild = 0;
                 }
               break;
              }
            case Greedy_Type:
            case Capture_Type:
              {
               curNode=curNode.Child(0);
               concatNode=NULL;
               continue;
              }
            case Oneloop:
            case Onelazy:
              {
               if(curNode.Max()>0)
                 {
                  string pref;
                  StringInit(pref,curNode.Min(),curNode.Ch());
                  //--- retrun 
                  return new CRegexPrefix(pref, 0 != (curNode.Options() & IgnoreCase));
                 }
               else
                 {
                  //--- return 
                  return CRegexPrefix::Empty();
                 }
              }
            case One:
              {
               //--- return 
               return new CRegexPrefix(ShortToString(curNode.Ch()), 0 != (curNode.Options() & IgnoreCase));
              }
            case Multi:
              {
               //--- return 
               return new CRegexPrefix(curNode.Str(), 0 != (curNode.Options() & IgnoreCase));
              }
            case PRIMITIVE_OPERATIONS::Bol:
            case PRIMITIVE_OPERATIONS::Eol:
            case PRIMITIVE_OPERATIONS::Boundary:
            case PRIMITIVE_CONTROL_STRUCTURES::ECMABoundary:
            case PRIMITIVE_OPERATIONS::Beginning:
            case PRIMITIVE_OPERATIONS::Start:
            case PRIMITIVE_OPERATIONS::EndZ:
            case PRIMITIVE_OPERATIONS::End:
            case REGEX_NODE_TYPES::Empty_Type:
            case Require_Type:
            case Prevent_Type:
              {
               break;
              }
            default:
              {
               //--- return 
               return (CRegexPrefix::Empty());
              }
           }
         if(concatNode==NULL || nextChild>=concatNode.ChildCount())
           {
            //--- return 
            return (CRegexPrefix::Empty());
           }
         curNode=concatNode.Child(nextChild++);
        }
      //--- return NULL
      return (NULL);
     }
   //+------------------------------------------------------------------+
   //| Yet another related computation: it takes a CRegexTree and       |
   //| computes the leading anchors that it encounters.                 |
   //+------------------------------------------------------------------+
   static int Anchors(CRegexTree *tree)
     {
      CRegexNode *curNode;
      CRegexNode *concatNode=NULL;
      int nextChild=0;
      int result=0;
      curNode=tree.Root();
      for(;;)
        {
         switch(curNode.Type())
           {
            case Concatenate_Type:
              {
               if(curNode.ChildCount()>0)
                 {
                  concatNode= curNode;
                  nextChild = 0;
                 }
               break;
              }
            case Greedy_Type:
            case Capture_Type:
              {
               curNode=curNode.Child(0);
               concatNode=NULL;
               continue;
              }
            case PRIMITIVE_OPERATIONS::Bol:
            case PRIMITIVE_OPERATIONS::Eol:
            case PRIMITIVE_OPERATIONS::Boundary:
            case PRIMITIVE_CONTROL_STRUCTURES::ECMABoundary:
            case PRIMITIVE_OPERATIONS::Beginning:
            case PRIMITIVE_OPERATIONS::Start:
            case PRIMITIVE_OPERATIONS::EndZ:
            case PRIMITIVE_OPERATIONS::End:
              {
               //--- return result
               return result | AnchorFromType(curNode.Type());
              }
            case REGEX_NODE_TYPES::Empty_Type:
            case Require_Type:
            case Prevent_Type:
              {
               break;
              }
            default:
              {
               //--- return 
               return (result);
              }
           }
         if(concatNode==NULL || nextChild>=concatNode.ChildCount())
           {
            //--- return 
            return (result);
           }
         curNode=concatNode.Child(nextChild++);
        }
      //--- retunrn 
      return (0);
     }
private:
   //+------------------------------------------------------------------+
   //| Convert anchor type to anchor bit.                               |
   //+------------------------------------------------------------------+
   static int AnchorFromType(int type)
     {
      switch(type)
        {
         case PRIMITIVE_OPERATIONS::Bol:
           { 
            return (Bol_PEG);
           }
         case PRIMITIVE_OPERATIONS::Eol:
           {
            return (Eol_PEG);
           }
         case PRIMITIVE_OPERATIONS::Boundary:
           {
            return (Boundary_PEG);
           }
         case PRIMITIVE_CONTROL_STRUCTURES::ECMABoundary:
           {
            return(ECMABoundary_PEG);
           }
         case PRIMITIVE_OPERATIONS::Beginning:
           {
            return (Beginning_PEG);
           }
         case PRIMITIVE_OPERATIONS::Start:
           {
            return (Start_PEG);
           }
         case PRIMITIVE_OPERATIONS::EndZ:
           {
            return (EndZ_PEG);
           }
         case PRIMITIVE_OPERATIONS::End:
           {
            return (End_PEG);
           }
         default:
           {
            return (0);
           }
        }
     }
#ifdef _DEBUG
public:
   //+------------------------------------------------------------------+
   //| AnchorDescription.                                               |
   //+------------------------------------------------------------------+
   static string AnchorDescription(const int anchors)
     {
      string text;
      if(0 != (anchors & Beginning))     { text+=(", Beginning");    }
      if(0 != (anchors & Start))         { text+=(", Start");        }
      if(0 != (anchors & Bol))           { text+=(", Bol");          }
      if(0 != (anchors & Boundary))      { text+=(", Boundary");     }
      if(0 != (anchors & ECMABoundary))  { text+=(", ECMABoundary"); }
      if(0 != (anchors & Eol))           { text+=(", Eol");          }
      if(0 != (anchors & End))           { text+=(", End");          }
      if(0 != (anchors & EndZ))          { text+=(", EndZ");         }
      if(StringLen(text)>=2)
        {
         return(StringSubstr(text,2, StringLen(text) - 2));
        }
      return ("None");
     }
#endif
private:
   //+------------------------------------------------------------------+
   //| Private constructor. Can't be created outside.                   |
   //+------------------------------------------------------------------+
                     CRegexFCD() : m_intDepth(0),m_fcDepth(0),m_skipAllChildren(false),m_skipchild(false),m_failed(0)
     {
      ArrayResize(m_fcStack,32);
      ArrayResize(m_intStack,32);
     }
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~CRegexFCD()
     {
      for(int i=0; i<ArraySize(m_fcStack); i++)
        {
         if(CheckPointer(m_fcStack[i])==POINTER_DYNAMIC)
           {
            delete m_fcStack[i];
           }
        }
     }
   //+------------------------------------------------------------------+
   //| To avoid recursion, we use a simple integer stack. This is the   |
   //| push.                                                            |
   //+------------------------------------------------------------------+
   void PushInt(const int I)
     {
      if(m_intDepth>=ArraySize(m_intStack))
        {
         ArrayResize(m_intStack,m_intDepth*2);
         ArrayFill(m_intStack,m_intDepth,m_intDepth,0);
        }
      m_intStack[m_intDepth++]=I;
     }
   //+------------------------------------------------------------------+
   //| True if the stack is empty.                                      |
   //+------------------------------------------------------------------+
   bool IntIsEmpty()
     {
      return (m_intDepth == 0);
     }
   //+------------------------------------------------------------------+
   //| This is the pop.                                                 |
   //+------------------------------------------------------------------+
   int PopInt()
     {
      return m_intStack[--m_intDepth];
     }
   //+------------------------------------------------------------------+
   //| We also use a stack of CRegexFC objects. This is the push.       |
   //+------------------------------------------------------------------+
   void PushFC(CRegexFC *fc)
     {
      if(m_fcDepth>=ArraySize(m_fcStack))
        {
         CRegexFC *expanded[];
         ArrayResize(expanded,m_fcDepth*2);
         ArrayCopy(expanded,m_fcStack,0,0,m_fcDepth);
         ArrayCopy(m_fcStack,expanded);
        }
      if(CheckPointer(m_fcStack[m_fcDepth])==POINTER_DYNAMIC)
        {
         delete m_fcStack[m_fcDepth];
        }
      m_fcStack[m_fcDepth++]=fc;
     }
   //+------------------------------------------------------------------+
   //| True if the stack is empty.                                      |
   //+------------------------------------------------------------------+
   bool FCIsEmpty()
     {
      return (m_fcDepth == 0);
     }
   //+------------------------------------------------------------------+
   //| This is the pop.                                                 |
   //+------------------------------------------------------------------+
   CRegexFC *PopFC()
     {
      return (m_fcStack[--m_fcDepth]);
     }
   //+------------------------------------------------------------------+
   //| This is the top.                                                 |
   //+------------------------------------------------------------------+
   CRegexFC *TopFC()
     {
      return (m_fcStack[m_fcDepth - 1]);
     }
   //+------------------------------------------------------------------+
   //| The main FC computation. It does a shortcutted depth-first walk  |
   //| through the tree and calls CalculateFC to emits code before and  |
   //| after each child of an interior node,and at each leaf.           |
   //+------------------------------------------------------------------+
   CRegexFC *RegexFCFromRegexTree(CRegexTree *tree)
     {
      CRegexNode *curNode;
      int curChild;
      curNode=tree.Root();
      curChild=0;
      for(;;)
        {
         if(curNode.Children()==NULL)
           {
            //--- This is a leaf node
            CalculateFC(curNode.Type(),curNode,0);
           }
         else if(curChild<curNode.Children().Count() && !m_skipAllChildren)
           {
            //--- This is an interior node, and we have more children to analyze
            CalculateFC(curNode.Type()|BeforeChild,curNode,curChild);
            if(!m_skipchild)
              {
               curNode=(CRegexNode*)curNode.Child(curChild);
               //--- this stack is how we get a depth first walk of the tree. 
               PushInt(curChild);
               curChild=0;
              }
            else
              {
               curChild++;
               m_skipchild=false;
              }
            continue;
           }
         //--- This is an interior node where we've finished analyzing all the children, or the end of a leaf node. 
         m_skipAllChildren=false;
         if(IntIsEmpty())
           {
            break;
           }
         curChild= PopInt();
         curNode = curNode.Next();
         CalculateFC(curNode.Type()|AfterChild,curNode,curChild);
         if(m_failed)
           {
            //--- return 
            return (NULL);
           }
         curChild++;
        }
      if(FCIsEmpty())
        {
         //--- return 
         return (NULL);
        }
      //--- return 
      return PopFC();
     }
   //+------------------------------------------------------------------+
   //| Called in Beforechild to prevent further processing of the       |
   //| current child.                                                   |
   //+------------------------------------------------------------------+
   void SkipChild()
     {
      m_skipchild=true;
     }
   //+------------------------------------------------------------------+
   //| FC computation and shortcut cases for each node type.            |
   //+------------------------------------------------------------------+
   void CalculateFC(const int NodeType,CRegexNode *node,const int CurIndex)
     {
      bool ci=false;
      bool rtl=false;
      if(NodeType<=Ref)
        {
         if((node.Options() &IgnoreCase)!=0)
           {
            ci=true;
           }
         if((node.Options() &RightToLeft)!=0)
           {
            rtl=true;
           }
        }
      switch(NodeType)
        {
         case Concatenate_Type|BeforeChild:
         case Alternate_Type|BeforeChild:
         case Testrefnode_Type|BeforeChild:
         case Loop_Type|BeforeChild:
         case Lazyloop_Type|BeforeChild:
           {
            break;
           }
         case Testgroup_Type|BeforeChild:
           {
            if(CurIndex==0)
              {
               SkipChild();
              }
            break;
           }
         case Empty_Type:
           {
            PushFC(new CRegexFC(true));
            break;
           }
         case Concatenate_Type|AfterChild:
           {
            if(CurIndex!=0)
              {
               CRegexFC *child = PopFC();
               CRegexFC *cumul = TopFC();
               m_failed=!cumul.AddFC(child,true);
              }
            if(!TopFC().NULLable())
              {
               m_skipAllChildren=true;
              }
            break;
           }
         case Testgroup_Type|AfterChild:
           {
            if(CurIndex>1)
              {
               CRegexFC *child = PopFC();
               CRegexFC *cumul = TopFC();
               m_failed=!cumul.AddFC(child,false);
              }
            break;
           }
         case Alternate_Type|AfterChild:
         case Testrefnode_Type|AfterChild:
           {
            if(CurIndex!=0)
              {
               CRegexFC *child = PopFC();
               CRegexFC *cumul = TopFC();
               m_failed=!cumul.AddFC(child,false);
              }
            break;
           }
         case Loop_Type|AfterChild:
         case Lazyloop_Type|AfterChild:
           {
            if(node.Min()==0)
              {
               TopFC().NULLable(true);
              }
            break;
           }
         case Group_Type| BeforeChild:
         case Group_Type | AfterChild:
         case Capture_Type| BeforeChild:
         case Capture_Type | AfterChild:
         case Greedy_Type | BeforeChild:
         case Greedy_Type|AfterChild:
           {
            break;
           }
         case Require_Type | BeforeChild:
         case Prevent_Type | BeforeChild:
           {
            SkipChild();
            PushFC(new CRegexFC(true));
            break;
           }
         case Require_Type | AfterChild:
         case Prevent_Type | AfterChild:
           {
            break;
           }
         case One:
         case Notone:
           {
            PushFC(new CRegexFC(node.Ch(),NodeType==Notone,false,ci));
            break;
           }
         case Oneloop:
         case Onelazy:
           {
            PushFC(new CRegexFC(node.Ch(),false,node.Min()==0,ci));
            break;
           }
         case Notoneloop:
         case Notonelazy:
           {
            PushFC(new CRegexFC(node.Ch(),true,node.Min()==0,ci));
            break;
           }
         case Multi:
           {
            if(StringLen(node.Str())==0)
              {
               PushFC(new CRegexFC(true));
              }
            else if(!rtl)
              {
               PushFC(new CRegexFC(StringGetCharacter(node.Str(),0),false,false,ci));
              }
            else
              {
               PushFC(new CRegexFC(StringGetCharacter(node.Str(),StringLen(node.Str())-1),false,false,ci));
              }
            break;
           }
         case Set:
           {
            PushFC(new CRegexFC(node.Str(),false,ci));
            break;
           }
         case Setloop:
         case Setlazy:
           {
            PushFC(new CRegexFC(node.Str(),node.Min()==0,ci));
            break;
           }
         case Ref:
           {
            PushFC(new CRegexFC(CRegexCharClass::AnyClass,true,false));
            break;
           }
         case Nothing:
         case Bol:
         case Eol:
         case Boundary:
         case Nonboundary:
         case ECMABoundary:
         case NonECMABoundary:
         case Beginning:
         case Start:
         case EndZ:
         case End:
           {
            PushFC(new CRegexFC(true));
            break;
           }
         default:
           {
            Print("Internal error! On file:'"+__FILE__+"'; in function:'"+__FUNCTION__+"'.");
            return;
           }
        }
     }
  };
//+------------------------------------------------------------------+
//| CHILD.                                                           |
//+------------------------------------------------------------------+
enum CHILD
  {
   BeforeChild= 64,
   AfterChild = 128,
  };
//+------------------------------------------------------------------+
//| Where the regex can be pegged.                                   |
//+------------------------------------------------------------------+
enum REGEX_PEG
  {
   Beginning_PEG=0x0001,
   Bol_PEG=0x0002,
   Start_PEG=0x0004,
   Eol_PEG=0x0008,
   EndZ_PEG=0x0010,
   End_PEG=0x0020,
   Boundary_PEG=0x0040,
   ECMABoundary_PEG=0x0080,
  };
//+------------------------------------------------------------------+
//| CRegexFC.                                                        |
//+------------------------------------------------------------------+
class CRegexFC : public IComparable
  {
private:
   bool              m_NULLable;
   CRegexCharClass   *m_cc;
   bool              m_caseInsensitive;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CRegexFC(CRegexFC  &value) : m_NULLable(0)
     {
      m_cc=value.m_cc;
      m_NULLable=value.m_NULLable;
      m_caseInsensitive=value.m_caseInsensitive;
     }
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CRegexFC(const bool NULLable) : m_caseInsensitive(0),m_NULLable(0)
     {
      m_cc=new CRegexCharClass();
      m_NULLable=NULLable;
     }
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CRegexFC(const ushort ch,const bool not,const bool NULLable,const bool caseInsensitive) : m_NULLable(0)
     {
      m_cc=new CRegexCharClass();
      if(not)
        {
         if(ch>0)
           {
            m_cc.AddRange('\x0000',ushort(ch-1));
           }
         if((int)ch<0xFFFF)
           {
            m_cc.AddRange(ushort(ch+1),'\xFFFF');
           }
        }
      else
        {
         m_cc.AddRange(ch,ch);
        }
      m_caseInsensitive=caseInsensitive;
      m_NULLable=NULLable;
     }
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+   
                     CRegexFC(const string charClass,const bool NULLable,const bool caseInsensitive) : m_NULLable(0)
     {
      m_cc=CRegexCharClass::Parse(charClass);
      m_NULLable=NULLable;
      m_caseInsensitive=caseInsensitive;
     }
   //+------------------------------------------------------------------+
   //| Constructors without parameters.                                 |
   //+------------------------------------------------------------------+     
                    ~CRegexFC()
     {
      if(CheckPointer(m_cc)==POINTER_DYNAMIC)
        {
         delete m_cc;
        }
     }
   //+------------------------------------------------------------------+
   //| Operator (=).                                                    |
   //+------------------------------------------------------------------+
   void operator=(const CRegexFC &value)
     {
      this.m_cc=value.m_cc;
      this.m_NULLable=value.m_NULLable;
      this.m_caseInsensitive=value.m_caseInsensitive;
     }
   //+------------------------------------------------------------------+
   //| Gets the NULLable.                                               |
   //+------------------------------------------------------------------+
   bool NULLable()
     {
      return (m_NULLable);
     }
   //+------------------------------------------------------------------+
   //| Sets the NULLable.                                               |
   //+------------------------------------------------------------------+
   void NULLable(const bool value)
     {
      m_NULLable=value;
     }
   //+------------------------------------------------------------------+
   //| AddFC.                                                           |
   //+------------------------------------------------------------------+
   bool AddFC(CRegexFC *fc,const bool concatenate)
     {
      if(!m_cc.CanMerge() || !fc.m_cc.CanMerge())
        {
         //--- return 
         return (false);
        }
      if(concatenate)
        {
         if(!m_NULLable)
           {
            //--- return 
            return (true);
           }
         if(!fc.m_NULLable)
           {
            m_NULLable=false;
           }
        }
      else
        {
         if(fc.m_NULLable)
           {
            m_NULLable=true;
           }
        }
      m_caseInsensitive|=fc.m_caseInsensitive;
      m_cc.AddCharClass(fc.m_cc);
      //--- return 
      return (true);
     }
   //+------------------------------------------------------------------+
   //| GetFirstChars.                                                   |
   //+------------------------------------------------------------------+
   string GetFirstChars()
     {
      if(m_caseInsensitive)
        {
         m_cc.AddLowercase();
        }
      return (m_cc.ToStringClass());
     }
   //+------------------------------------------------------------------+
   //| IsCaseInsensitive.                                               |
   //+------------------------------------------------------------------+
   bool IsCaseInsensitive()
     {
      return (m_caseInsensitive);
     }
  };
//+------------------------------------------------------------------+
//| Class CRegexPrefix.                                              |
//+------------------------------------------------------------------+
class CRegexPrefix
  {
private:
   string            m_prefix;
   bool              m_caseInsensitive;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CRegexPrefix(const string prefix,const bool ci)
     {
      m_prefix=prefix;
      m_caseInsensitive=ci;
     }
public:
   //+------------------------------------------------------------------+
   //| Prefix.                                                          |
   //+------------------------------------------------------------------+
   string Prefix()
     {
      //--- return prefix
      return (m_prefix);
     }
   //+------------------------------------------------------------------+
   //| CaseInsensitive.                                                 |
   //+------------------------------------------------------------------+
   bool CaseInsensitive()
     {
      //--- return CaseInsensitive
      return (m_caseInsensitive);
     }
   //+------------------------------------------------------------------+
   //| Empty.                                                           |
   //+------------------------------------------------------------------+
   static CRegexPrefix *Empty()
     {
      static CRegexPrefix empty("",false);
      //--- retrun empty prefix
      return (GetPointer(empty));
     }
  };
//+------------------------------------------------------------------+
