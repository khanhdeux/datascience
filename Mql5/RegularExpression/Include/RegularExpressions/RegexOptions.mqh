//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
//+------------------------------------------------------------------+
//| Provides enumerated values to use to set regular expression      |
//| options.                                                         |
//+------------------------------------------------------------------+
enum RegexOptions
  {
   //+------------------------------------------------------------------+
   //| Specifies that no options are set.                               |
   //+------------------------------------------------------------------+
   None=0x0000,
   //+------------------------------------------------------------------+
   //| Specifies case-insensitive matching.                             |
   //+------------------------------------------------------------------+
   IgnoreCase=0x0001,          // "i"
   //+------------------------------------------------------------------+
   //| Multiline mode. Changes the meaning of ^ and $ so they match at  |
   //| the beginning and end, respectively, of any line, and not just   |
   //| the beginning and end of the entire string.                      |
   //+------------------------------------------------------------------+
   Multiline=0x0002,           // "m"
   //+------------------------------------------------------------------+
   //| Specifies that the only valid captures are explicitly named or   |
   //| numbered groups of the form (?<name>…). This allows unnamed      |
   //| parentheses to act as noncapturing groups without the syntactic  |
   //| clumsiness of the expression (?:…). For more information, see the|
   //| "Explicit Captures Only" section in the Regular Expression       |
   //| Options topic.                                                   |
   //+------------------------------------------------------------------+
   ExplicitCapture=0x0004,     // "n"
   //+------------------------------------------------------------------+
   //| Specifies single-line mode. Changes the meaning of the dot (.) so|
   //| it matches every character (instead of every character except    |
   //| \n).                                                             |
   //+------------------------------------------------------------------+
   Singleline=0x0010,          // "s"
   //+------------------------------------------------------------------+
   //| Eliminates unescaped white space from the pattern and enables    |
   //| comments marked with #. However, the IgnorePatternWhitespace     |
   //| value does not affect or eliminate white space in character      |
   //| classes.                                                         |
   //+------------------------------------------------------------------+
   IgnorePatternWhitespace=0x0020,// "x"
   //+------------------------------------------------------------------+
   //| Specifies that the search will be from right to left instead of  |
   //| from left to right.                                              |
   //+------------------------------------------------------------------+
   RightToLeft=0x0040,         // "r"
#ifdef _DEBUG
   //+------------------------------------------------------------------+
   //| It indicates that the program is running under a debugger.       |
   //+------------------------------------------------------------------+
   Debug=0x0080,               // "d"
#endif
   //+------------------------------------------------------------------+
   //| Enables ECMAScript-compliant behavior for the expression. This   |
   //| value can be used only in conjunction with the IgnoreCase,       |
   //| Multiline.                                                       |
   //+------------------------------------------------------------------+
   ECMAScript=0x0100,          // "e"
  };
//+------------------------------------------------------------------+
