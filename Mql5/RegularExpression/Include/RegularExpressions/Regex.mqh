//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
class CMatch;
class CMatchCollection;
class CCachedCodeEntry;
class CReplacementReference;
class CRunnerReference;
class CRegexRunner;
//+------------------------------------------------------------------+
//| Callback class.                                                  |
//+------------------------------------------------------------------+
typedef string(*MatchEvaluator)(CMatch*);
#include <Internal\TimeSpan\TimeSpan.mqh>
#include <Internal\Generic\LinkedList.mqh>
#include <Internal\Generic\Dictionary.mqh>
#include "RegexOptions.mqh"
#include "RegexCode.mqh"
#include "RegexTree.mqh"
#include "RegexParser.mqh"
#include "RegexReplacement.mqh"
#include "RegexWriter.mqh"
#include "RegexMatchCollection.mqh"
#include "RegexRunner.mqh"
#include "RegexInterpreter.mqh"
//+------------------------------------------------------------------+
//| Purpose: The CRegex class represents a single compiled instance  |
//| of a regular expression.                                         |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Represents an immutable, compiled regular expression. Also       |
//| contains static methods that allow use of regular expressions    |
//| without instantiating a CRegex explicitly.                       |
//+------------------------------------------------------------------+
class CRegex
  {
private:
   static const TimeSpan MaximumMatchTimeout;
   static const string DefaultMatchTimeout_ConfigKeyName;
protected:
   string            m_pattern;
   RegexOptions      m_roptions;
   TimeSpan          m_internalMatchTimeout;// timeout for the execution of this regex
   Dictionary<int,int>*m_caps;        // if captures are sparse, this is the hashtable capnum->index
   Dictionary<string,int>*m_capnames; // if named captures are used, this maps names->index
   string            m_capslist[];    // if captures are sparse or named captures are used, this is the sorted list of names
   int               m_capsize;       // the size of the capture array
   CRegexTree        *m_tree;
   CRunnerReference  *m_runnerref;      // cached runner
   CReplacementReference*m_replref;    // cached parsed replacement pattern
   CRegexCode        *m_code;          // if interpreted, this is the code for RegexIntepreter
   bool              m_refsInitialized;// Default is false
   static            LinkedList<CCachedCodeEntry*>m_livecode;// the cached of code that are currently loaded
   static int        s_cacheSize;     // Default is 15
public:
   static const int  MaxOptionShift;
   static const TimeSpan FallbackDefaultMatchTimeout;
   static const TimeSpan DefaultMatchTimeout;
   static const TimeSpan InfiniteMatchTimeout;
   //+------------------------------------------------------------------+
   //| Initializes a new instance of the CRegex class.                  |
   //+------------------------------------------------------------------+
                     CRegex() : m_refsInitialized(0), m_capsize(0)
     {
      this.m_internalMatchTimeout=DefaultMatchTimeout;
     }
   //+------------------------------------------------------------------+
   //| Creates and compiles a regular expression object for the         |
   //| specified regular expression.                                    |
   //+------------------------------------------------------------------+
                     CRegex(const string pattern) : m_refsInitialized(0), m_capsize(0)
     {
      Initialize(pattern,None,DefaultMatchTimeout,false);
     }
   //+------------------------------------------------------------------+
   //| Creates and compiles a regular expression object for the         |
   //| specified regular expression with options that modify the        |
   //| pattern.                                                         |
   //+------------------------------------------------------------------+
                     CRegex(const string pattern,RegexOptions options) : m_refsInitialized(0), m_capsize(0)
     {
      Initialize(pattern,options,DefaultMatchTimeout,false);
     }
   //+------------------------------------------------------------------+
   //| Initializes a new instance of the CRegex class for the specified |
   //| regular expression,with options that modify the pattern and a    |
   //| value that specifies how long a pattern matching method should   |
   //| attempt a match before it times out.                             |
   //+------------------------------------------------------------------+
                     CRegex(const string pattern,RegexOptions options,const TimeSpan &matchTimeout) : m_refsInitialized(0), m_capsize(0)
     {
      Initialize(pattern,options,matchTimeout,false);
     }
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~CRegex()
     {
      if(CheckPointer(m_tree)==POINTER_DYNAMIC)
        {
         delete m_tree;
        }
      if(CheckPointer(m_caps)==POINTER_DYNAMIC)
        {
         delete m_caps;
        }
      bool deleteRun=true;
      bool deleteRepl = true;
      bool deleteCode = true;
      for(LinkedListNode<CCachedCodeEntry*>*current=m_livecode.First(); current!=NULL; current=current.Next())
        {
         if(CheckPointer(current.Value())==POINTER_DYNAMIC)
           {
            if(current.Value().RunnerRef()==m_runnerref)
              {
               deleteRun=false;
              }
            if(current.Value().ReplRef()==m_replref)
              {
               deleteRepl=false;
              }
            if(current.Value().Code()==m_code)
              {
               deleteCode=false;
              }
           }
        }
      if(CheckPointer(m_replref)==POINTER_DYNAMIC && deleteRepl)
        {
         delete m_replref;
        }
      if(CheckPointer(m_runnerref)==POINTER_DYNAMIC && deleteRun)
        {
         delete m_runnerref;
        }
      if(CheckPointer(m_code)==POINTER_DYNAMIC && deleteCode)
        {
         delete m_code;
        }
     }
private:
   //+------------------------------------------------------------------+
   //| General constructor with parameters.                             |
   //+------------------------------------------------------------------+
                     CRegex(const string pattern,RegexOptions options,const TimeSpan &matchTimeout,const bool useCache) : m_refsInitialized(0)
     {
      Initialize(pattern,options,matchTimeout,useCache);
     }
   //+------------------------------------------------------------------+
   //| Initialize.                                                      |
   //+------------------------------------------------------------------+
   void              Initialize(const string pattern,RegexOptions options,const TimeSpan &matchTimeout,const bool useCache)
     {
      CRegexTree *tree;
      CCachedCodeEntry *cached=NULL;
      if(pattern==NULL)
        {
         Print("Argument 'pattern'= NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      if(options<None || (((int) options)>>MaxOptionShift)!=0)
        {
         Print("Argument 'options' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      if((options    &ECMAScript)!=0
         && (options  &~(ECMAScript|IgnoreCase|Multiline
#ifdef _DEBUG
                         |RegexOptions::Debug
#endif
                        ))!=0)
        {
         Print("Argument 'options' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      ValidateMatchTimeout(matchTimeout);
      //--- Try to look up this regex in the cache.  We do this regardless of whether useCache is true since there's really no reason not to.
      string key=IntegerToString(options)+":"+pattern;
      cached=LookupCachedAndUpdate(key);
      this.m_pattern=pattern;
      this.m_roptions=options;
      this.m_internalMatchTimeout=matchTimeout;
      if(cached==NULL)
        {
         //--- Parse the input
         tree=CRegexParser::Parse(pattern,(RegexOptions)m_roptions);
         //--- Extract the relevant information
         m_capnames=tree.CapNames();
         tree.GetCapsList(m_capslist);
         m_code       = CRegexWriter::Write(tree);
         m_caps       = m_code.Caps();
         m_capsize    = m_code.CapSize();
         InitializeReferences();
         m_tree=tree;
         if(useCache)
           {
            cached=CacheCode(key);
           }
        }
      else
        {
         m_caps       = cached.Caps();
         m_capnames   = cached.CapNames();
         cached.GetCapList(m_capslist);
         m_capsize    = cached.CapSize();
         m_code       = cached.Code();
         m_runnerref  = cached.RunnerRef();
         m_replref    = cached.ReplRef();
         m_refsInitialized=true;
        }
     }
public:
   //+------------------------------------------------------------------+
   //| Pattern.                                                         |
   //+------------------------------------------------------------------+
   string            Pattern()
     {
      return (m_pattern);
     }
   //+------------------------------------------------------------------+
   //| Validates that the specified match timeout value is valid.       |
   //| The valid range is:                                              |
   //| TimeSpan::Zero < matchTimeout <= CRegex::MaximumMatchTimeout.    |
   //+------------------------------------------------------------------+
   static void       ValidateMatchTimeout(const TimeSpan &matchTimeout)
     {
      if(InfiniteMatchTimeout==matchTimeout)
        {
         return;
        }
      //--- Change this to make sure timeout is not longer then Environment.Ticks cycle length:
      if(TimeSpan::Zero()<matchTimeout && matchTimeout<=MaximumMatchTimeout)
        {
         return;
        }
      Print("Argument 'matchTimeout' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
     }
   //+------------------------------------------------------------------+
   //| Specifies the default RegEx matching timeout value (i.e. the     |
   //| timeout that will be used if no explicit timeout is specified).  |
   //+------------------------------------------------------------------+
   static TimeSpan   InitDefaultMatchTimeout()
     {
      return (FallbackDefaultMatchTimeout);
     }
   //+------------------------------------------------------------------+
   //| Gets the runner reference.                                       |
   //+------------------------------------------------------------------+
                     CRunnerReference*RunnerReference()
     {
      return (m_runnerref);
     }
   //+------------------------------------------------------------------+
   //| Gets the weak reference.                                         |
   //+------------------------------------------------------------------+
                     CReplacementReference*ReplacementReference()
     {
      return (m_replref);
     };
   //+------------------------------------------------------------------+
   //| Gets the Caps.                                                   |
   //+------------------------------------------------------------------+
                     Dictionary<int,int>*Caps()
     {
      return (m_caps);
     }
   //+------------------------------------------------------------------+
   //| Gets the CapNames.                                               |
   //+------------------------------------------------------------------+
                     Dictionary<string,int>*CapNames()
     {
      return (m_capnames);
     }
   //+------------------------------------------------------------------+
   //| Gets the CapSize.                                                |
   //+------------------------------------------------------------------+
   int               CapSize()
     {
      return (m_capsize);
     }
   //+------------------------------------------------------------------+
   //| Returns the options passed into the constructor.                 |
   //+------------------------------------------------------------------+
   RegexOptions      Options()
     {
      return (m_roptions);
     }
   //+------------------------------------------------------------------+
   //| Escape metacharacters within the string.                         |
   //+------------------------------------------------------------------+
   static string     Escape(const string str)
     {
      if(StringLen(str)==NULL)
        {
         Print("Argument 'str' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return CRegexParser::Escape(str);
     }
   //+------------------------------------------------------------------+
   //| Unescape character codes within the string.                      |
   //+------------------------------------------------------------------+
   static string     Unescape(const string str)
     {
      if(StringLen(str)==NULL)
        {
         Print("Argument 'str' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return CRegexParser::Unescape(str);
     }
   //+------------------------------------------------------------------+
   //| CacheCount.                                                      |
   //+------------------------------------------------------------------+
   static int        CacheCount()
     {
      return (m_livecode.Count());
     }
   //+------------------------------------------------------------------+
   //| CacheSize.                                                       |
   //+------------------------------------------------------------------+
   static int        CacheSize()
     {
      return (s_cacheSize);
     }
   //+------------------------------------------------------------------+
   //| CacheSize.                                                       |
   //+------------------------------------------------------------------+
   static void       CacheSize(const int value)
     {
      if(value<0)
        {
         Print("Argument 'value' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      s_cacheSize=value;
      if(m_livecode.Count()>s_cacheSize)
        {
         while(m_livecode.Count()>s_cacheSize)
           {
            m_livecode.RemoveLast();
           }
        }
     }
   //+------------------------------------------------------------------+
   //| The match timeout used by this CRegex instance.                  |
   //+------------------------------------------------------------------+
   TimeSpan          MatchTimeout()
     {
      return (m_internalMatchTimeout);
     }
   //+------------------------------------------------------------------+
   //| True if the regex is leftward.                                   |
   //|                                                                  |
   //| Indicates whether the regular expression matches from right to   |
   //| left.                                                            |
   //+------------------------------------------------------------------+
   bool              RightToLeft()
     {
      return UseOptionR();
     }
   //+------------------------------------------------------------------+
   //| Returns the regular expression pattern passed into the           |
   //| constructor.                                                     |
   //+------------------------------------------------------------------+
   string            ToString()
     {
      return (m_pattern);
     }
   //+------------------------------------------------------------------+
   //| Returns an array of the group names that are used to capture     |
   //| groups in the regular expression. Only needed if the regex is not|
   //| known until runtime, and one wants to extract captured groups.   |
   //| (Probably unusual, but supplied for completeness.).              |
   //+------------------------------------------------------------------+
   void              GetGroupNames(string &result[])
     {
      if(ArraySize(m_capslist)==NULL)
        {
         int max=m_capsize;
         ArrayResize(result,max);
         for(int i=0; i<max; i++)
           {
            result[i]=IntegerToString(i);
           }
        }
      else
        {
         ArrayCopy(result,m_capslist,0,0);
        }
     }
   //+------------------------------------------------------------------+
   //| Returns an array of the group names that are used to capture     |
   //| groups in the regular expression. Only needed if the regex is not|
   //| known until runtime, and one wants to extract captured groups.   |
   //| (Probably unusual, but supplied for completeness.).              |
   //+------------------------------------------------------------------+
   void              GetGroupNumbers(int &result[])
     {
      if(m_caps==NULL)
        {
         int max=m_capsize;
         ArrayResize(result,max);
         for(int i=0; i<max; i++)
           {
            result[i]=i;
           }
        }
      else
        {
         ArrayResize(result,m_caps.Count());
         DictionaryEnumerator<int,int>*de=m_caps.GetEnumerator();
         while(de.MoveNext())
           {
            result[(int)de.Value()]=(int)de.Key();
           }
         delete de;
        }
     }
   //+------------------------------------------------------------------+
   //| Given a group number, maps it to a group name. Note that nubmered|
   //| groups automatically get a group name that is the decimal string |
   //| equivalent of its number.                                        |
   //+------------------------------------------------------------------+
   string            GroupNameFromNumber(const int index)
     {
      int i=index;
      if(ArraySize(m_capslist)==NULL)
        {
         if(i>=0 && i<m_capsize)
           {
            //--- return
            return IntegerToString(i);
           }
         //--- return
         return ("");
        }
      else
        {
         if(m_caps!=NULL)
           {
            if(!m_caps.ContainsKey(i))
              {
               //--- return
               return ("");
              }
            i=m_caps[i];
           }
         if(i>=0 && i<ArraySize(m_capslist))
           {
            //--- return
            return (m_capslist[i]);
           }
         //--- return
         return ("");
        }
     }
   //+------------------------------------------------------------------+
   //| Given a group name, maps it to a group number. Note that nubmered|
   //| groups automatically get a group name that is the decimal string |
   //| equivalent of its number.                                        |
   //|                                                                  |
   //| Returns -1 if the name is not a recognized group name.           |
   //+------------------------------------------------------------------+
   int               GroupNumberFromName(const string name)
     {
      int result=-1;
      if(name==NULL)
        {
         Print("Argument 'name' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      //--- look up name if we have a hashtable of names
      if(m_capnames!=NULL)
        {
         if(!m_capnames.ContainsKey(name))
           {
            //--- return
            return (-1);
           }
         //--- return result
         return (m_capnames[name]);
        }
      //--- convert to an int if it looks like a number
      result=0;
      for(int i=0; i<StringLen(name); i++)
        {
         ushort ch=StringGetCharacter(name,i);
         if(ch>'9' || ch<'0')
           {
            //--- return
            return (-1);
           }
         result *= 10;
         result += (ch - '0');
        }
      //--- return int if it's in range
      if(result>=0 && result<m_capsize)
        {
         //--- return
         return (result);
        }
      //--- return
      return (-1);
     }
   //+------------------------------------------------------------------+
   //| Searches the input string for one or more occurrences of the text|
   //| supplied in the pattern parameter.                               |
   //+------------------------------------------------------------------+
   static bool       IsMatch(const string in,const string pattern)
     {
      return IsMatch(in, pattern, None, DefaultMatchTimeout);
     }
   //+------------------------------------------------------------------+
   //| Searches the in string for one or more occurrences of the text   |
   //| supplied in the pattern parameter.                               |
   //+------------------------------------------------------------------+
   static bool       IsMatch(const string in,const string pattern,const RegexOptions options)
     {
      return IsMatch(in, pattern, options, DefaultMatchTimeout);
     }
   //+------------------------------------------------------------------+
   //| Searches the in string for one or more occurrences of the text   |
   //| supplied in the pattern parameter.                               |
   //+------------------------------------------------------------------+
   static bool       IsMatch(const string in,const string pattern,const RegexOptions options,const TimeSpan &matchTimeout)
     {
      CRegex regex(pattern,options,matchTimeout,true);
      return (regex.IsMatch(in));
     }
   //+------------------------------------------------------------------+
   //| Searches the in string for one or more matches using the         |
   //| previous pattern, options, and starting position.                |
   //+------------------------------------------------------------------+
   bool              IsMatch(const string in)
     {
      if(in==NULL)
        {
         Print("Argument 'in' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (false);
        }
      return IsMatch(in, UseOptionR() ? StringLen(in) : 0);
     }
   //+------------------------------------------------------------------+
   //| Searches the in string for one or more matches using the         |
   //| previous pattern and options, with a new starting position.      |
   //+------------------------------------------------------------------+
   bool              IsMatch(const string in,const int startat)
     {
      if(in==NULL)
        {
         Print("Argument 'in' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (false);
        }
      CMatch *run=Run(true,-1,in,0,StringLen(in),startat);
      bool result=(NULL==run);
      if(CheckPointer(run)==POINTER_DYNAMIC)
        {
         delete run;
        }
      return (result);
     }
   //+------------------------------------------------------------------+
   //| Searches the in string for one or more occurrences of the text   |
   //| supplied in the pattern parameter.                               |
   //+------------------------------------------------------------------+
   static CMatch     *Match(const string in,const string pattern)
     {
      return CRegex::Match(in, pattern, None, DefaultMatchTimeout);
     }
   //+------------------------------------------------------------------+
   //| Searches the in string for one or more occurrences of the text|
   //| supplied in the pattern parameter. Matching is modified with an  |
   //| option string.                                                   |
   //+------------------------------------------------------------------+
   static CMatch     *Match(const string in,const string pattern,const RegexOptions options)
     {
      return CRegex::Match(in, pattern, options, DefaultMatchTimeout);
     }
   //+------------------------------------------------------------------+
   //| Searches the in string for one or more occurrences of the text|
   //| supplied in the pattern parameter. Matching is modified with an  |
   //| option string.                                                   |
   //+------------------------------------------------------------------+
   static CMatch     *Match(string in,string pattern,RegexOptions options,const TimeSpan &matchTimeout)
     {
      CRegex *regex=new CRegex(pattern,options,matchTimeout,true);
      return (regex.Match(in));
     }
   //+------------------------------------------------------------------+
   //| Matches a regular expression with a string and returns the       |
   //| precise result as a RegexMatch object.                           |
   //+------------------------------------------------------------------+
   CMatch            *Match(const string in)
     {
      if(in==NULL)
        {
         Print("Argument 'in' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return CRegex::Match(in, UseOptionR() ? StringLen(in) : 0);
     }
   //+------------------------------------------------------------------+
   //| Matches a regular expression with a string and returns the       |
   //| precise result as a RegexMatch object.                           |
   //+------------------------------------------------------------------+
   CMatch            *Match(const string in,const int startat)
     {
      if(in==NULL)
        {
         Print("Argument 'in' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return Run(false, -1, in, 0, StringLen(in), startat);
     }
   //+------------------------------------------------------------------+
   //| Matches a regular expression with a string and returns the       |
   //| precise result as a RegexMatch object.                           |
   //+------------------------------------------------------------------+
   CMatch            *Match(const string in,const int beginning,const int length)
     {
      if(in==NULL)
        {
         Print("Argument 'in' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return Run(false, -1, in, beginning, length, UseOptionR() ? beginning + length : beginning);
     }
   //+------------------------------------------------------------------+
   //| Returns all the successful matches as if CMatch was called       |
   //| iteratively numerous times.                                      |
   //+------------------------------------------------------------------+
   static CMatchCollection *Matches(const string in,const string pattern)
     {
      return CRegex::Matches(in, pattern, None, DefaultMatchTimeout);
     }
   //+------------------------------------------------------------------+
   //| Returns all the successful matches as if CMatch was called       |
   //| iteratively numerous times.                                      |
   //+------------------------------------------------------------------+
   static CMatchCollection *Matches(const string in,const string pattern,const RegexOptions options)
     {
      return CRegex::Matches(in, pattern, options, DefaultMatchTimeout);
     }
   //+------------------------------------------------------------------+
   //| Returns all the successful matches as if CMatch was called       |
   //| iteratively numerous times.                                      |
   //+------------------------------------------------------------------+
   static CMatchCollection *Matches(const string in,const string pattern,const RegexOptions options,const TimeSpan &matchTimeout)
     {
      CRegex *regex=new CRegex(pattern,options,matchTimeout,true);
      return (regex.Matches(in));
     }
   //+------------------------------------------------------------------+
   //| Returns all the successful matches as if CMatch was called       |
   //| iteratively numerous times.                                      |
   //+------------------------------------------------------------------+
   CMatchCollection  *Matches(const string in)
     {
      if(in==NULL)
        {
         Print("Argument 'in' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return Matches(in, UseOptionR() ? StringLen(in) : 0);
     }
   //+------------------------------------------------------------------+
   //| Returns all the successful matches as if CMatch was called       |
   //| iteratively numerous times.                                      |
   //+------------------------------------------------------------------+
   CMatchCollection  *Matches(const string in,const int startat)
     {
      if(in==NULL)
        {
         Print("Argument 'in' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return new CMatchCollection(GetPointer(this), in, 0, StringLen(in), startat);
     }
   //+------------------------------------------------------------------+
   //| Replaces all occurrences of the pattern with the "replacement"   |
   //| pattern, starting at the first character in the in string.       |
   //+------------------------------------------------------------------+
   static string     Replace(const string in,const string pattern,const string replacement)
     {
      return Replace(in, pattern, replacement, None, DefaultMatchTimeout);
     }
   //+------------------------------------------------------------------+
   //| Replaces all occurrences of the "pattern" with the "replacement" |
   //| pattern, starting at the first character in the in string.       |
   //+------------------------------------------------------------------+
   static string     Replace(const string in,const string pattern,const string replacement,const RegexOptions options)
     {
      return Replace(in, pattern, replacement, options, DefaultMatchTimeout);
     }
   //+------------------------------------------------------------------+
   //| Replaces all occurrences of the "pattern" with the "replacement" |
   //| pattern, starting at the first character in the in string.       |
   //+------------------------------------------------------------------+
   static string     Replace(const string in,const string pattern,const string replacement,const RegexOptions options,const TimeSpan &matchTimeout)
     {
      CRegex *regex = new CRegex(pattern,options,matchTimeout,true);
      string result=regex.Replace(in,replacement);
      delete regex;
      return (result);
     }
   //+------------------------------------------------------------------+
   //| Replaces all occurrences of the "pattern " with the "replacement"|
   //| pattern, starting at the first character in the in string, using |
   //| the previous patten.                                             |
   //+------------------------------------------------------------------+
   string            Replace(const string in,const string replacement)
     {
      if(in==NULL)
        {
         Print("Argument 'in' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return Replace(in, replacement, -1, UseOptionR() ? StringLen(in) : 0);
     }
   //+------------------------------------------------------------------+
   //| Replaces all occurrences of the (previously defined) "pattern"   |
   //| with the "replacement" pattern, starting at the first character  |
   //| in the in string.                                                |
   //+------------------------------------------------------------------+
   string            Replace(const string in,const string replacement,const int count)
     {
      if(in==NULL)
        {
         Print("Argument 'in' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return Replace(in, replacement, count, UseOptionR() ? StringLen(in) : 0);
     }
   //+------------------------------------------------------------------+
   //| Replaces all occurrences of the "pattern" with the recent        |
   //| "replacement" pattern, starting at the character position        |
   //| "startat.".                                                      |
   //+------------------------------------------------------------------+
   string            Replace(const string in,const string replacement,const int count,const int startat)
     {
      if(in==NULL)
        {
         Print("Argument 'in' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      if(replacement==NULL)
        {
         Print("Argument 'replacement' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      //--- A little code to grab a cached parsed replacement object
      CRegexReplacement *repl=m_replref.Get();
      if(repl==NULL || !(repl.Pattern()==replacement))
        {
         repl=CRegexParser::ParseReplacement(replacement,m_caps,m_capsize,m_capnames,this.m_roptions);
         if(CheckPointer(m_replref.Get())==POINTER_DYNAMIC)
           {
            delete m_replref.Get();
           }
         m_replref.Set(repl);
        }
      return repl.Replace(GetPointer(this), in, count, startat);
     }
   //+------------------------------------------------------------------+
   //| Replaces all occurrences of the "pattern" with the "replacement" |
   //| pattern ".".                                                     |
   //+------------------------------------------------------------------+
   static string     Replace(const string in,const string pattern,MatchEvaluator evaluator)
     {
      return Replace(in, pattern, evaluator, None, DefaultMatchTimeout);
     }
   //+------------------------------------------------------------------+
   //| Replaces all occurrences of the "pattern " with the recent       |
   //| "replacement" pattern, starting at the first character ".".      |
   //+------------------------------------------------------------------+
   static string     Replace(const string in,const string pattern,MatchEvaluator evaluator,const RegexOptions options)
     {
      return Replace(in, pattern, evaluator, options, DefaultMatchTimeout);
     }
   //+------------------------------------------------------------------+
   //| Replaces all occurrences of the "pattern " with the recent       |
   //| "replacement" pattern, starting at the first character ".".      |
   //+------------------------------------------------------------------+
   static string     Replace(const string in,const string pattern,MatchEvaluator evaluator,const RegexOptions options,const TimeSpan &matchTimeout)
     {
      CRegex regex(pattern,options,matchTimeout,true);
      string result=regex.Replace(in,evaluator);
      return (result);
     }
   //+------------------------------------------------------------------+
   //| Replaces all occurrences of the "pattern" with the recent        |
   //| "replacement" pattern, starting at the first character position  |
   //| ".".                                                             |
   //+------------------------------------------------------------------+
   string            Replace(const string in,MatchEvaluator evaluator)
     {
      if(in==NULL)
        {
         Print("Argument 'in' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return Replace(in, evaluator, -1, UseOptionR() ? StringLen(in) : 0);
     }
   //+------------------------------------------------------------------+
   //| Replaces all occurrences of the "pattern" with the recent        |
   //| "replacement" pattern, starting at the first character position  |
   //| ".".                                                             |
   //+------------------------------------------------------------------+
   string            Replace(const string in,MatchEvaluator evaluator,const int count)
     {
      if(in==NULL)
        {
         Print("Argument 'in' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return Replace(in, evaluator, count, UseOptionR() ? StringLen(in) : 0);
     }
   //+------------------------------------------------------------------+
   //| Replaces all occurrences of the (previouly defined) "pattern"    |
   //| with the recent "replacement" pattern, starting at the character |
   //| position "startat."                                              |
   //+------------------------------------------------------------------+
   string            Replace(const string in,MatchEvaluator evaluator,const int count,const int startat)
     {
      if(in==NULL)
        {
         Print("Argument 'in' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return CRegexReplacement::Replace(evaluator, GetPointer(this), in, count, startat);
     }
   //+------------------------------------------------------------------+
   //| Splits the "in" string at the position defined by "pattern".     |
   //+------------------------------------------------------------------+
   static void       Split(string &result[],const string in,const string pattern)
     {
      Split(result,in,pattern,None,DefaultMatchTimeout);
     }
   //+------------------------------------------------------------------+
   //| Splits the "in" string at the position defined by "pattern".     |
   //+------------------------------------------------------------------+
   static void       Split(string &result[],const string in,const string pattern,RegexOptions options)
     {
      Split(result,in,pattern,options,DefaultMatchTimeout);
     }
   //+------------------------------------------------------------------+
   //| Splits the "in" string at the position defined by "pattern".     |
   //+------------------------------------------------------------------+
   static void       Split(string &result[],const string in,const string pattern,const RegexOptions options,const TimeSpan &matchTimeout)
     {
      CRegex regex(pattern,options,matchTimeout,true);
      regex.Split(result,in);
     }
   //+------------------------------------------------------------------+
   //| Splits the "in" string at the position defined by a previous     |
   //| "pattern".                                                       |
   //+------------------------------------------------------------------+
   void              Split(string &result[],const string in)
     {
      if(in==NULL)
        {
         Print("Argument 'in' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return
         return;
        }
      Split(result,in,0,UseOptionR() ? StringLen(in) : 0);
     }
   //+------------------------------------------------------------------+
   //| Splits the "in" string at the position defined by a previous     |
   //| "pattern".                                                       |
   //+------------------------------------------------------------------+
   void              Split(string &result[],const string in,const int count)
     {
      if(in==NULL)
        {
         Print("Argument 'in' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      CRegexReplacement::Split(result,GetPointer(this),in,count,UseOptionR() ? StringLen(in) : 0);
     }
   //+------------------------------------------------------------------+
   //| Splits the "in" string at the position defined by a previous     |
   //| "pattern".                                                       |
   //+------------------------------------------------------------------+
   void              Split(string &result[],const string in,const int count,const int startat)
     {
      if(in==NULL)
        {
         Print("Argument 'in' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return;
        }
      CRegexReplacement::Split(result,GetPointer(this),in,count,startat);
     }
protected:
   //+------------------------------------------------------------------+
   //| InitializeReferences.                                            |
   //+------------------------------------------------------------------+
   void              InitializeReferences()
     {
      if(m_refsInitialized)
        {
         Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         //--- return
         return;
        }
      m_refsInitialized=true;
      m_runnerref  = new CRunnerReference();
      m_replref    = new CReplacementReference();
     }
public:
   //+------------------------------------------------------------------+
   //| Internal worker called by all the  APIs.                         |
   //+------------------------------------------------------------------+
   CMatch            *Run(const bool quick,const int prevlen,const string in,const int beginning,const int length,const int startat)
     {
      CMatch *match;
      CRegexRunner *runner=NULL;
      if(startat<0 || startat>StringLen(in))
        {
         Print("Argument 'start' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      if(length<0 || length>StringLen(in))
        {
         Print("Argument 'length' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      bool fromCache=false;
      //--- There may be a cached runner; grab ownership of it if we can.
      if(m_runnerref!=NULL)
        {
         fromCache=true;
         runner=m_runnerref.Get();
        }
      //--- Create a CRegexRunner instance if we need to
      if(runner==NULL)
        {
         fromCache=false;
         //--- Use the compiled CRegexRunner factory if the code was compiled to MSIL
         runner=new CRegexInterpreter(m_code);
        }
      //--- Do the scan starting at the requested position
      match=runner.Scan(GetPointer(this),in,beginning,beginning+length,startat,prevlen,quick,m_internalMatchTimeout);
      if(m_runnerref!=NULL && !fromCache)
        {
         if(CheckPointer(m_runnerref.Get())==POINTER_DYNAMIC)
           {
            delete m_runnerref.Get();
           }
         //--- Release or fill the cache slot
         m_runnerref.Set(runner);
        }
      else
         if(!fromCache)
           {
            delete runner;
           }
#ifdef _DEBUG
      if(Debug() && match!=NULL)
        {
         match.Dump();
        }
#endif
      //--- return match
      return (match);
     }
   //+------------------------------------------------------------------+
   //| Find code cache based on options+pattern.                        |
   //+------------------------------------------------------------------+
   static CCachedCodeEntry *LookupCachedAndUpdate(const string key)
     {
      for(LinkedListNode<CCachedCodeEntry*>*current=m_livecode.First(); current!=NULL; current=current.Next())
        {
         if(current.Value().Key()==key)
           {
            //--- If we find an entry in the cache, move it to the head at the same time.
            m_livecode.Remove(current);
            m_livecode.AddFirst(current);
            return (current.Value());
           }
        }
      return (NULL);
     }
   //+------------------------------------------------------------------+
   //| Add current code to the cache.                                   |
   //+------------------------------------------------------------------+
   CCachedCodeEntry  *CacheCode(const string key)
     {
      CCachedCodeEntry *newcached=NULL;
      //--- first look for it in the cache and move it to the head
      for(LinkedListNode<CCachedCodeEntry*>*current=m_livecode.First(); current!=NULL; current=current.Next())
        {
         if(current.Value().Key()==key)
           {
            m_livecode.Remove(current);
            m_livecode.AddFirst(current);
            return (current.Value());
           }
        }
      //--- it wasn't in the cache, so we'll add a new one.  Shortcut out for the case where cacheSize is zero.
      if(s_cacheSize!=0)
        {
         newcached=new CCachedCodeEntry(key,m_capnames,m_capslist,m_code,m_caps,m_capsize,m_runnerref,m_replref);
         m_livecode.AddFirst(newcached);
         if(m_livecode.Count()>s_cacheSize)
           {
            m_livecode.RemoveLast();
           }
        }
      return (newcached);
     }
   //+------------------------------------------------------------------+
   //| Delete all objects in cache.                                     |
   //+------------------------------------------------------------------+
   static void       ClearCache()
     {
      IEnumerator<CCachedCodeEntry*>*en=m_livecode.GetEnumerator();
      while(en.MoveNext())
        {
         if(CheckPointer(en.Current())==POINTER_DYNAMIC)
           {
            CCachedCodeEntry *entry = en.Current();
            CRunnerReference *runnerref = entry.RunnerRef();
            CRegexRunner *runner = runnerref.Get();
            if(runner!=NULL && CheckPointer(runner.RunRegex())==POINTER_DYNAMIC)
              {
               delete runner.RunRegex();
              }
            delete entry;
           }
        }
      delete en;
     }
   //+------------------------------------------------------------------+
   //| True if the O option was set.                                    |
   //+------------------------------------------------------------------+
   bool              UseOptionC()
     {
      return(m_roptions) != 0;
     }
   //+------------------------------------------------------------------+
   //| True if the L option was set.                                    |
   //+------------------------------------------------------------------+
   bool              UseOptionR()
     {
      return(m_roptions & RegexOptions::RightToLeft) != 0;
     }
   //+------------------------------------------------------------------+
   //| UseOptionInvariant.                                              |
   //+------------------------------------------------------------------+
   bool              UseOptionInvariant()
     {
      return(m_roptions) != 0;
     }
#ifdef _DEBUG
   //+------------------------------------------------------------------+
   //| True if the regex has debugging enabled.                         |
   //+------------------------------------------------------------------+
   bool              Debug()
     {
      return((m_roptions & RegexOptions::Debug) != 0);
     }
#endif
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
static const TimeSpan   CRegex::MaximumMatchTimeout=TimeSpan::FromMilliseconds(Int32::MaxValue-1);
static const TimeSpan   CRegex::InfiniteMatchTimeout(0,0,0,0,-1);
static const string CRegex::DefaultMatchTimeout_ConfigKeyName="REGEX_DEFAULT_MATCH_TIMEOUT";
static const TimeSpan   CRegex::FallbackDefaultMatchTimeout=CRegex::InfiniteMatchTimeout;
static const TimeSpan   CRegex::DefaultMatchTimeout=CRegex::InitDefaultMatchTimeout();
static LinkedList<CCachedCodeEntry*>CRegex::m_livecode();
static int CRegex::s_cacheSize=15;
static const int CRegex::MaxOptionShift=10;
//+------------------------------------------------------------------+
//| Purpose: Used to cache byte codes or compiled factories.         |
//+------------------------------------------------------------------+
class CCachedCodeEntry : public IComparable
  {
private:
   string            m_key;
   CRegexCode        *m_code;
   Dictionary<int,int>*m_caps;
   Dictionary<string,int>*m_capnames;
   string            m_capslist[];
   int               m_capsize;
   CRunnerReference  *m_runnerref;
   CReplacementReference *m_replref;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CCachedCodeEntry(const string key,Dictionary<string,int>*capnames,const string &capslist[],
                    CRegexCode *code,Dictionary<int,int>*caps,const int capsize,
                    CRunnerReference *runner,CReplacementReference *repl)
     {
      m_key=key;
      m_capnames=capnames;
      ArrayCopy(m_capslist,capslist);
      m_code= code;
      m_caps= caps;
      m_capsize=capsize;
      m_runnerref=runner;
      m_replref=repl;
     }
   //+------------------------------------------------------------------+
   //| destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~CCachedCodeEntry()
     {
      if(CheckPointer(m_code)==POINTER_DYNAMIC)
        {
         delete m_code;
        }
      if(CheckPointer(m_caps)==POINTER_DYNAMIC)
        {
         delete m_caps;
        }
      if(CheckPointer(m_capnames)==POINTER_DYNAMIC)
        {
         delete m_capnames;
        }
      if(CheckPointer(m_replref)==POINTER_DYNAMIC)
        {
         delete m_replref;
        }
      if(CheckPointer(m_runnerref)==POINTER_DYNAMIC)
        {
         delete m_runnerref;
        }
     }
   //+------------------------------------------------------------------+
   //| Gets the runnerref.                                              |
   //+------------------------------------------------------------------+
   CRunnerReference  *RunnerRef()
     {
      return (m_runnerref);
     }
   //+------------------------------------------------------------------+
   //| Gets the replref.                                                |
   //+------------------------------------------------------------------+
   CReplacementReference *ReplRef()
     {
      return (m_replref);
     }
   //+------------------------------------------------------------------+
   //| Gets the key.                                                    |
   //+------------------------------------------------------------------+
   string            Key()
     {
      return (m_key);
     }
   //+------------------------------------------------------------------+
   //| Gets the caps.                                                   |
   //+------------------------------------------------------------------+
                     Dictionary<int,int>*Caps()
     {
      return (m_caps);
     }
   //+------------------------------------------------------------------+
   //| Gets the capnames.                                               |
   //+------------------------------------------------------------------+
                     Dictionary<string,int>*CapNames()
     {
      return (m_capnames);
     }
   //+------------------------------------------------------------------+
   //| Gets the capsize.                                                |
   //+------------------------------------------------------------------+
   int               CapSize()
     {
      return (m_capsize);
     }
   //+------------------------------------------------------------------+
   //| Gets the code.                                                   |
   //+------------------------------------------------------------------+
   CRegexCode        *Code()
     {
      return (m_code);
     }
   //+------------------------------------------------------------------+
   //| Gets the caplist.                                                |
   //+------------------------------------------------------------------+
   void              GetCapList(string &array[])
     {
      ArrayCopy(array,m_capslist);
     }
  };
//+------------------------------------------------------------------+
//| Used to cache a weak reference in a threadsafe way.              |
//+------------------------------------------------------------------+
class CReplacementReference
  {
private:
   CRegexReplacement *m_obj;
public:
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~CReplacementReference()
     {
      if(CheckPointer(m_obj)==POINTER_DYNAMIC)
        {
         delete m_obj;
        }
     }
   //+------------------------------------------------------------------+
   //| Get CRegexReplacement.                                           |
   //+------------------------------------------------------------------+
   CRegexReplacement *Get()
     {
      //--- return pointer
      return (m_obj);
     }
   //+------------------------------------------------------------------+
   //| Set CRegexReplacement.                                           |
   //+------------------------------------------------------------------+
   void              Set(CRegexReplacement *obj)
     {
      m_obj=obj;
     }
  };
//+------------------------------------------------------------------+
//| Used to cache one exclusive runner reference.                    |
//+------------------------------------------------------------------+
class CRunnerReference
  {
private:
   CRegexRunner      *m_obj;
public:
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~CRunnerReference()
     {
      if(CheckPointer(m_obj)==POINTER_DYNAMIC)
        {
         delete m_obj;
        }
     }
   //+------------------------------------------------------------------+
   //| Get CRegexRunner.                                                |
   //+------------------------------------------------------------------+
   CRegexRunner      *Get()
     {
      return (m_obj);
     }
   //+------------------------------------------------------------------+
   //| Set CRegexRunner.                                                |
   //+------------------------------------------------------------------+
   void              Set(CRegexRunner *obj)
     {
      m_obj=obj;
     }
  };
//+------------------------------------------------------------------+
