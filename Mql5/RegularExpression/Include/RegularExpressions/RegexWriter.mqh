//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
class CRegexCode;
#include "RegexFCD.mqh"
#include "RegexTree.mqh"
//+------------------------------------------------------------------+
//| Purpose: Defines a method that a type implements to compare two  |
//| strings.                                                         |
//+------------------------------------------------------------------+
class StringEqualityComparer : public IEqualityComparer<string>
  {
public:
   //+------------------------------------------------------------------+
   //| Determines whether the specified objects are equal.              |
   //+------------------------------------------------------------------+
   virtual bool      Equals(string x,string y)
     {
      if(StringLen(x)!=StringLen(y))
        {
         return (false);
        }
      else
        {
         for(int i=0; i<StringLen(x); i++)
           {
            if(StringGetCharacter(x,i)!=StringGetCharacter(y,i))
              {
               return (false);
              }
           }
        }
      return (true);
     }
   //+------------------------------------------------------------------+
   //| Returns a hash code for the specified object.                    |
   //+------------------------------------------------------------------+
   int               GetHashCode(string obj)
     {
      return (::GetHashCode(obj));
     }
  };
//+------------------------------------------------------------------+
//| Purpose: This CRegexWriter class is internal to the Regex        |
//| package. It builds a block of regular expression codes           |
//| (CRegexCode) from a CRegexTree parse tree.                       |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| This step is as simple as walking the tree and emitting sequences|
//| of codes.                                                        |
//+------------------------------------------------------------------+
class CRegexWriter
  {
private:
   int               m_intStack[];
   int               m_depth;
   int               m_emitted[];
   int               m_curpos;
   bool              m_counting;
   int               m_count;
   int               m_trackcount;
   Dictionary<int,int>*m_caps;
   Dictionary<string,int>*m_stringhash;
   List<string>*m_stringtable;
public:
   //+------------------------------------------------------------------+
   //| This is the only function that should be called from outside.    |
   //| It takes a CRegexTree and creates a corresponding CRegexCode.    |
   //+------------------------------------------------------------------+
   static CRegexCode *Write(CRegexTree *t)
     {
      CRegexWriter *w=new CRegexWriter();
      CRegexCode *retval=w.RegexCodeFromRegexTree(t);
#ifdef _DEBUG
      if(t.Debug())
        {
         t.Dump();
         retval.Dump();
        }
#endif
      delete w;
      return (retval);
     }
private:
   //+------------------------------------------------------------------+
   //| Constructor without parameters.                                  |
   //+------------------------------------------------------------------+
                     CRegexWriter() : m_depth(0), m_curpos(0), m_counting(0), m_count(0), m_trackcount(0)
     {
      ArrayResize(m_intStack,32);
      ZeroMemory(m_intStack);
      ArrayResize(m_emitted,32);
      ZeroMemory(m_emitted);
      m_stringhash=new Dictionary<string,int>(new StringEqualityComparer());
      m_stringtable=new List<string>();
     }
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~CRegexWriter()
     {
      delete m_stringhash;
      delete m_stringtable;
     }
   //+------------------------------------------------------------------+
   //| To avoid recursion, we use a simple integer stack. This is the   |
   //| push.                                                            |
   //+------------------------------------------------------------------+
   void              PushInt(const int I)
     {
      if(m_depth>=ArraySize(m_intStack))
        {
         ArrayResize(m_intStack,m_depth*2);
         ArrayFill(m_intStack,m_depth,m_depth,0);
        }
      m_intStack[m_depth++]=I;
     }
   //+------------------------------------------------------------------+
   //| True if the stack is empty.                                      |
   //+------------------------------------------------------------------+
   bool              EmptyStack()
     {
      return (m_depth == 0);
     }
   //+------------------------------------------------------------------+
   //| This is the pop.                                                 |
   //+------------------------------------------------------------------+
   int               PopInt()
     {
      return (m_intStack[--m_depth]);
     }
   //+------------------------------------------------------------------+
   //| Returns the current position in the emitted code.                |
   //+------------------------------------------------------------------+
   int               CurPos()
     {
      return (m_curpos);
     }
   //+------------------------------------------------------------------+
   //| Fixes up a jump instruction at the specified offset so that it   |
   //| jumps to the specified jumpDest.                                 |
   //+------------------------------------------------------------------+
   void              PatchJump(const int Offset,const int jumpDest)
     {
      m_emitted[Offset+1]=jumpDest;
     }
   //+------------------------------------------------------------------+
   //| Emits a zero-argument operation. Note that the emit functions all|
   //| run in two modes: they can emit code, or they can just count the |
   //| size of the code.                                                |
   //+------------------------------------------------------------------+
   void              Emit(const int op)
     {
      if(m_counting)
        {
         m_count+=1;
         if(CRegexCode::OpcodeBacktracks(op))
           {
            m_trackcount+=1;
           }
         return;
        }
      m_emitted[m_curpos++]=op;
     }
   //+------------------------------------------------------------------+
   //| Emits a one-argument operation.                                  |
   //+------------------------------------------------------------------+
   void              Emit(const int op,const int opd1)
     {
      if(m_counting)
        {
         m_count+=2;
         if(CRegexCode::OpcodeBacktracks(op))
           {
            m_trackcount+=1;
           }
         return;
        }
      m_emitted[m_curpos++] = op;
      m_emitted[m_curpos++] = opd1;
     }
   //+------------------------------------------------------------------+
   //| Emits a two-argument operation.                                  |
   //+------------------------------------------------------------------+
   void              Emit(const int op,const int opd1,const int opd2)
     {
      if(m_counting)
        {
         m_count+=3;
         if(CRegexCode::OpcodeBacktracks(op))
           {
            m_trackcount+=1;
           }
         return;
        }
      m_emitted[m_curpos++] = op;
      m_emitted[m_curpos++] = opd1;
      m_emitted[m_curpos++] = opd2;
     }
   //+------------------------------------------------------------------+
   //| Returns an index in the string table for a string.               |
   //+------------------------------------------------------------------+
   int               StringCode(const string str)
     {
      int i;
      if(m_counting)
        {
         return (0);
        }
      if(m_stringhash.ContainsKey(str))
        {
         i=(int)m_stringhash[str];
        }
      else
        {
         i=m_stringtable.Count();
         m_stringhash.Set(str,i);
         m_stringtable.Add(str);
        }
      return (i);
     }
   //+------------------------------------------------------------------+
   //| When generating code on a regex that uses a sparse set of capture|
   //| slots, we hash them to a dense set of indices for an array of    |
   //| capture slots. Instead of doing the hash at match time, it's done|
   //| at compile time, here.                                           |
   //+------------------------------------------------------------------+
   int               MapCapnum(const int capnum)
     {
      if(capnum==-1)
        {
         return (-1);
        }
      if(m_caps!=NULL)
        {
         return (m_caps[capnum]);
        }
      else
        {
         //--- return result
         return (capnum);
        }
     }
   //+------------------------------------------------------------------+
   //| The top level CRegexCode generator. It does a depth-first walk   |
   //| through the tree and calls EmitFragment to emits code before and |
   //| after each child of an interior node,and at each leaf.           |
   //|                                                                  |
   //| It runs two passes,first to count the size of the generated code,|
   //| and second to generate the code.                                 |
   //+------------------------------------------------------------------+
   CRegexCode        *RegexCodeFromRegexTree(CRegexTree *tree)
     {
      CRegexNode *curNode;
      int curChild;
      int capsize;
      CRegexPrefix *fcPrefix;
      CRegexPrefix *prefix;
      int anchors;
      CRegexBoyerMoore *bmPrefix;
      bool rtl;
      //--- construct sparse capnum mapping if some numbers are unused
      int capnumlist[];
      tree.GetCapNumList(capnumlist);
      if(ArraySize(capnumlist)==NULL || tree.CapTop()==ArraySize(capnumlist))
        {
         capsize=tree.CapTop();
         m_caps=NULL;
        }
      else
        {
         capsize=ArraySize(capnumlist);
         m_caps=tree.Caps();
         for(int i=0; i<ArraySize(capnumlist); i++)
           {
            m_caps.Set(capnumlist[i],i);
           }
        }
      m_counting=true;
      for(;;)
        {
         if(!m_counting)
           {
            ArrayResize(m_emitted,m_count);
           }
         curNode=tree.Root();
         curChild=0;
         Emit(Lazybranch,0);
         for(;;)
           {
            if(curNode.Children()==NULL)
              {
               EmitFragment(curNode.Type(),curNode,0);
              }
            else
               if(curChild<curNode.Children().Count())
                 {
                  EmitFragment(curNode.Type()|BeforeChild,curNode,curChild);
                  curNode=(CRegexNode*)curNode.Children()[curChild];
                  PushInt(curChild);
                  curChild=0;
                  continue;
                 }
            if(EmptyStack())
              {
               break;
              }
            curChild= PopInt();
            curNode = curNode.Next();
            EmitFragment(curNode.Type()|AfterChild,curNode,curChild);
            curChild++;
           }
         PatchJump(0,CurPos());
         Emit(Stop);
         if(!m_counting)
           {
            break;
           }
         m_counting=false;
        }
      fcPrefix=CRegexFCD::FirstChars(tree);
      prefix=CRegexFCD::Prefix(tree);
      rtl=((tree.Options() &RightToLeft)!=0);
      if(prefix!=NULL && StringLen(prefix.Prefix())>0)
        {
         bmPrefix=new CRegexBoyerMoore(prefix.Prefix(),prefix.CaseInsensitive(),rtl);
        }
      else
        {
         bmPrefix=NULL;
        }
      anchors=CRegexFCD::Anchors(tree);
      if(CheckPointer(prefix)==POINTER_DYNAMIC)
        {
         delete prefix;
        }
      return new CRegexCode(m_emitted, m_stringtable, m_trackcount, m_caps, capsize, bmPrefix, fcPrefix, anchors, rtl);
     }
   //+------------------------------------------------------------------+
   //| The main CRegexCode generator. It does a depth-first walk through|
   //| the tree and calls EmitFragment to emits code before and after   |
   //| each child of an interior node, and at each leaf.                |
   //+------------------------------------------------------------------+
   void              EmitFragment(const int nodetype,CRegexNode *node,const int CurIndex)
     {
      int bits=0;
      if(nodetype<=Ref)
        {
         if(node.UseOptionR())
           {
            bits|=Rtl;
           }
         if((node.Options() &IgnoreCase)!=0)
           {
            bits|=Ci;
           }
        }
      switch(nodetype)
        {
         case Concatenate_Type| BeforeChild:
         case Concatenate_Type | AfterChild:
         case Empty_Type:
           {
            break;
           }
         case Alternate_Type|BeforeChild:
           {
            if(CurIndex<node.Children().Count()-1)
              {
               PushInt(CurPos());
               Emit(Lazybranch,0);
              }
            break;
           }
         case Alternate_Type|AfterChild:
           {
            if(CurIndex<node.Children().Count()-1)
              {
               int LBPos=PopInt();
               PushInt(CurPos());
               Emit(Goto,0);
               PatchJump(LBPos,CurPos());
              }
            else
              {
               int I;
               for(I=0; I<CurIndex; I++)
                 {
                  PatchJump(PopInt(),CurPos());
                 }
              }
            break;
           }
         case Testrefnode_Type|BeforeChild:
           {
            switch(CurIndex)
              {
               case 0:
                 {
                  Emit(Setjump);
                  PushInt(CurPos());
                  Emit(Lazybranch,0);
                  Emit(Testrefstruct,MapCapnum(node.Min()));
                  Emit(Forejump);
                  break;
                 }
              }
            break;
           }
         case Testrefnode_Type|AfterChild:
           {
            switch(CurIndex)
              {
               case 0:
                 {
                  int Branchpos=PopInt();
                  PushInt(CurPos());
                  Emit(Goto,0);
                  PatchJump(Branchpos,CurPos());
                  Emit(Forejump);
                  if(node.Children().Count()>1)
                    {
                     break;
                    }
                 }
               case 1:
                 {
                  PatchJump(PopInt(),CurPos());
                  break;
                 }
              }
            break;
           }
         case Testgroup_Type|BeforeChild:
           {
            switch(CurIndex)
              {
               case 0:
                 {
                  Emit(Setjump);
                  Emit(Setmark);
                  PushInt(CurPos());
                  Emit(Lazybranch,0);
                  break;
                 }
              }
            break;
           }
         case Testgroup_Type|AfterChild:
           {
            switch(CurIndex)
              {
               case 0:
                 {
                  Emit(Getmark);
                  Emit(Forejump);
                  break;
                 }
               case 1:
                 {
                  int Branchpos=PopInt();
                  PushInt(CurPos());
                  Emit(Goto,0);
                  PatchJump(Branchpos,CurPos());
                  Emit(Getmark);
                  Emit(Forejump);
                  if(node.Children().Count()>2)
                    {
                     break;
                    }
                 }
               case 2:
                 {
                  PatchJump(PopInt(),CurPos());
                  break;
                 }
              }
            break;
           }
         case Loop_Type|BeforeChild:
         case Lazyloop_Type|BeforeChild:
           {
            if(node.Max()<Int32::MaxValue || node.Min()>1)
              {
               Emit(node.Min()==0 ? Nullcount : Setcount,node.Min()==0 ? 0 : 1-node.Min());
              }
            else
              {
               Emit(node.Min()==0 ? Nullmark : Setmark);
              }
            if(node.Min()==0)
              {
               PushInt(CurPos());
               Emit(Goto,0);
              }
            PushInt(CurPos());
            break;
           }
         case Loop_Type|AfterChild:
         case Lazyloop_Type|AfterChild:
           {
              {
               int StartJumpPos=CurPos();
               int Lazy=(nodetype -(Loop_Type|AfterChild));
               if(node.Max()<Int32::MaxValue || node.Min()>1)
                 {
                  Emit(Branchcount+Lazy,PopInt(),node.Max()==Int32::MaxValue ? Int32::MaxValue : node.Max()-node.Min());
                 }
               else
                 {
                  Emit(Branchmark+Lazy,PopInt());
                 }
               if(node.Min()==0)
                 {
                  PatchJump(PopInt(),StartJumpPos);
                 }
              }
            break;
           }
         case Group_Type| BeforeChild:
         case Group_Type | AfterChild:
           {
            break;
           }
         case Capture_Type|BeforeChild:
           {
            Emit(Setmark);
            break;
           }
         case Capture_Type|AfterChild:
           {
            Emit(Capturemark,MapCapnum(node.Min()),MapCapnum(node.Max()));
            break;
           }
         case Require_Type|BeforeChild:
           {
            //--- NOTE: the following line causes lookahead/lookbehind to be
            //--- NON-BACKTRACKING. It can be commented out with (*)
            Emit(Setjump);
            Emit(Setmark);
            break;
           }
         case Require_Type|AfterChild:
           {
            Emit(Getmark);
            //--- NOTE: the following line causes lookahead/lookbehind to be
            //--- NON-BACKTRACKING. It can be commented out with (*)
            Emit(Forejump);
            break;
           }
         case Prevent_Type|BeforeChild:
           {
            Emit(Setjump);
            PushInt(CurPos());
            Emit(Lazybranch,0);
            break;
           }
         case Prevent_Type|AfterChild:
           {
            Emit(Backjump);
            PatchJump(PopInt(),CurPos());
            Emit(Forejump);
            break;
           }
         case Greedy_Type|BeforeChild:
           {
            Emit(Setjump);
            break;
           }
         case Greedy_Type|AfterChild:
           {
            Emit(Forejump);
            break;
           }
         case One:
         case Notone:
           {
            Emit(node.Type()|bits,(int)node.Ch());
            break;
           }
         case Notoneloop:
         case Notonelazy:
         case Oneloop:
         case Onelazy:
           {
            if(node.Min()>0)
              {
               Emit(((node.Type()==Oneloop || node.Type()==Onelazy) ? Onerep : Notonerep)|bits,(int)node.Ch(),node.Min());
              }
            if(node.Max()>node.Min())
              {
               Emit(node.Type()|bits,(int)node.Ch(),node.Max()==Int32::MaxValue ? Int32::MaxValue : node.Max()-node.Min());
              }
            break;
           }
         case Setloop:
         case Setlazy:
           {
            if(node.Min()>0)
              {
               Emit(Setrep|bits,StringCode(node.Str()),node.Min());
              }
            if(node.Max()>node.Min())
              {
               Emit(node.Type()|bits,StringCode(node.Str()),(node.Max()==Int32::MaxValue) ? Int32::MaxValue : node.Max()-node.Min());
              }
            break;
           }
         case Multi:
           {
            Emit(node.Type()|bits,StringCode(node.Str()));
            break;
           }
         case Set:
           {
            Emit(node.Type()|bits,StringCode(node.Str()));
            break;
           }
         case Ref:
           {
            Emit(node.Type()|bits,MapCapnum(node.Min()));
            break;
           }
         case Nothing:
         case Bol:
         case Eol:
         case Boundary:
         case Nonboundary:
         case ECMABoundary:
         case NonECMABoundary:
         case Beginning:
         case Start:
         case EndZ:
         case End:
           {
            Emit(node.Type());
            break;
           }
         default:
           {
            Print("Internal error!"," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
            //--- return
            return;
           }
        }
     }
  };
//+------------------------------------------------------------------+
