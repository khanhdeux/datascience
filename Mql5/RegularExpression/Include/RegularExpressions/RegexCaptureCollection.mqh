//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
class CGroup;
#include <Internal\Generic\ICollection.mqh>
#include "RegexGroup.mqh"
#include "RegexCapture.mqh"
//+------------------------------------------------------------------+
//| Purpose: Represents the set of captures made by a single         |
//| capturing group.                                                 |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| The CCaptureCollection lists the captured CCapture numbers       |
//| contained in a compiled Regex.                                   |
//|                                                                  |
//| This collection returns the Captures for a group in the order in |
//| which they were matched (left to right or right to left).        |
//|                                                                  |
//| Represents a sequence of capture substrings. The object is used  |
//| to return the set of captures done by a single capturing group.  |
//+------------------------------------------------------------------+
class CCaptureCollection : public ICollection<CCapture*>
  {
private:
   CGroup            *m_group;
   int               m_capcount;
   CCapture          *m_captures[];
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CCaptureCollection(CGroup *group)
     {
      m_group=group;
      m_capcount=m_group.Count();
     }
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~CCaptureCollection()
     {
      for(int i=0; i<ArraySize(m_captures); i++)
        {
         if(CheckPointer(m_captures[i])==POINTER_DYNAMIC)
           {
            delete m_captures[i];
           }
        }
     }
   //+------------------------------------------------------------------+
   //| Gets the number of substrings captured by the group.             |
   //+------------------------------------------------------------------+
   int Count()
     {
      return (m_capcount);
     }
   //+------------------------------------------------------------------+
   //| Gets an individual member of the collection.                     |
   //+------------------------------------------------------------------+
   CCapture *operator[](const int i)
     {
      return GetCapture(i);
     }
   //+------------------------------------------------------------------+
   //| Copies all the elements of the collection to the given array     |
   //| beginning at the given index.                                    |
   //+------------------------------------------------------------------+
   void CopyTo(CCapture *&array[],const int arrayIndex)
     {
      ArrayResize(array,arrayIndex+Count());
      for(int i=arrayIndex,j=0; j<Count(); i++,j++)
        {
         array[i]=this[j];
        }
     }
   //+------------------------------------------------------------------+
   //| Provides an enumerator that iterates through the collection.     |
   //+------------------------------------------------------------------+
   IEnumerator<CCapture*>*GetEnumerator()
     {
      return new CCaptureEnumerator(GetPointer(this));
     }
protected:
   //+------------------------------------------------------------------+
   //| Gets an individual member of the collection.                     |
   //+------------------------------------------------------------------+
   CCapture *GetCapture(const int i)
     {
      if(i==m_capcount-1 && i>=0)
        {
         return (m_group);
        }
      if(i>=m_capcount || i<0)
        {
         Print("Argument 'i' out of range."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      //--- first time a capture is accessed, compute them all
      if(ArraySize(m_captures)==0)
        {
         ArrayResize(m_captures,m_capcount);
         for(int j=0; j<m_capcount-1; j++)
           {
            m_captures[j]=new CCapture(m_group.GetOriginalString(),m_group[j*2],m_group[j*2+1]);
           }
        }
      return (m_captures[i]);
     }
  };
//+------------------------------------------------------------------+
//| Purpose: Enumerator lists all the captures.                      |
//+------------------------------------------------------------------+
class CCaptureEnumerator : public IEnumerator<CCapture*>
  {
private:
   CCaptureCollection *m_rcc;
   int               m_curindex;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CCaptureEnumerator(CCaptureCollection *rcc)
     {
      m_curindex=-1;
      m_rcc=rcc;
     } 
   //+------------------------------------------------------------------+
   //| Advances the enumerator to the next element of the collection.   |
   //+------------------------------------------------------------------+
   bool MoveNext()
     {
      int size=m_rcc.Count();
      if(m_curindex>=size)
        {
         return (false);
        }
      m_curindex++;
      return(m_curindex < size);
     }
   //+------------------------------------------------------------------+
   //| Gets current element.                                            |
   //+------------------------------------------------------------------+
   CCapture *Current()
     {
      return (this.CCapture());
     }
   //+------------------------------------------------------------------+
   //| Returns the current capture.                                     |
   //+------------------------------------------------------------------+
   CCapture *CCapture()
     {
      if(m_curindex<0 || m_curindex>=m_rcc.Count())
        {
         Print("Invalid operation."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      return (m_rcc[m_curindex]);
     }
   //+------------------------------------------------------------------+
   //| Reset to before the first item.                                  |
   //+------------------------------------------------------------------+  
   void Reset()
     {
      m_curindex=-1;
     }
  };
//+------------------------------------------------------------------+
