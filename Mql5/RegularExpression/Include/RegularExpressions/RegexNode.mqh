//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
#include <Internal\Generic\List.mqh>
class CRegexParser;
#include "RegexOptions.mqh"
#include "RegexCode.mqh"
#include "RegexCharClass.mqh"
#include "RegexParser.mqh"
//+------------------------------------------------------------------+
//| Purpose: It is built into a parsed tree for a regular expression.|
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Since the node tree is a temporary data structure only used      |
//| during compilation of the regexp to integer codes, it's designed |
//| for clarity and convenience rather than space efficiency.        |
//|                                                                  |
//| RegexNodes are built into a tree, linked by the m_children list. |
//| Each node also has a m_parent and m_ichild member indicating its |
//| parent and which child # it is in its parent's list.             |
//|                                                                  |
//| RegexNodes come in as many types as there are constructs in a    |
//| regular expression, for example, "concatenate", "alternate",     |
//| "one", "rept", "group". There are also node types for basic      |
//| peephole optimizations, e.g., "onerep", "notsetrep", etc.        |
//|                                                                  |
//| Because perl 5 allows "lookback" groups that scan backwards, each|
//| node also gets a "direction". Normally the value of boolean      |
//| m_backward = false.                                              |
//|                                                                  |
//| During parsing, top-level nodes are also stacked onto a parse    |
//| stack (a stack of trees). For this purpose we have a m_next      |
//| pointer. [Note that to save a few bytes, we could overload the   |
//| m_parent pointer instead.]                                       |
//|                                                                  |
//| On the parse stack, each tree has a "role" - basically, the      |
//| nonterminal in the grammar that the parser has currently assigned|
//| to the tree. That code is stored in m_role.                      |
//|                                                                  |
//| Finally, some of the different kinds of nodes have data. Two     |
//| integers (for the looping constructs) are stored in m_operands,  |
//| an an object (either a string or a set) is stored in m_data.     |
//+------------------------------------------------------------------+
class CRegexNode : public IComparable
  {
public:
   static            List<CRegexNode*>s_allNodes;
private:
   int               m_id;
   int               m_type;
   List<CRegexNode*>*m_children;
   string            m_str;
   ushort            m_ch;
   int               m_m;
   int               m_n;
   RegexOptions      m_options;
   CRegexNode        *m_next;
   static const string Space;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CRegexNode(const int type,const RegexOptions options)
     {
      s_allNodes.Add(GetPointer(this));
      m_id=CRegexParser::CurrentID();
      m_type=type;
      m_options=options;
     }
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CRegexNode(const int type,const RegexOptions options,const ushort ch)
     {
      s_allNodes.Add(GetPointer(this));
      m_id=CRegexParser::CurrentID();
      m_type=type;
      m_options=options;
      m_ch=ch;
     }
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CRegexNode(const int type,const RegexOptions options,const string str)
     {
      s_allNodes.Add(GetPointer(this));
      m_id=CRegexParser::CurrentID();
      m_type=type;
      m_options=options;
      m_str=str;
     }
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CRegexNode(const int type,const RegexOptions options,const int m)
     {
      s_allNodes.Add(GetPointer(this));
      m_id=CRegexParser::CurrentID();
      m_type=type;
      m_options=options;
      m_m=m;
     }
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CRegexNode(const int type,const RegexOptions options,const int m,const int n)
     {
      s_allNodes.Add(GetPointer(this));
      m_id=CRegexParser::CurrentID();
      m_type=type;
      m_options=options;
      m_m = m;
      m_n = n;
     }
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~CRegexNode()
     {
      if(CheckPointer(m_children)==POINTER_DYNAMIC)
        {
         for(int i=0; i<m_children.Count(); i++)
           {
            if(CheckPointer(m_children[i])==POINTER_DYNAMIC)
              {
               delete m_children[i];
              }
           }
         if(CheckPointer(m_children)==POINTER_DYNAMIC)
           {
            delete m_children;
           }
        }
     }
   //+------------------------------------------------------------------+
   //| ID.                                                              |
   //+------------------------------------------------------------------+
   int ID()
     {
      return (m_id);
     }
   //+------------------------------------------------------------------+
   //| UseOptionR.                                                      |
   //+------------------------------------------------------------------+
   bool UseOptionR()
     {
      return(m_options & RightToLeft) != 0;
     }
   //+------------------------------------------------------------------+
   //| ReverseLeft.                                                     |
   //+------------------------------------------------------------------+
   CRegexNode *ReverseLeft()
     {
      if(UseOptionR() && m_type==Concatenate_Type && m_children!=NULL)
        {
         m_children.Reverse(0,m_children.Count());
        }
      return GetPointer(this);
     }
   //+------------------------------------------------------------------+
   //| Pass type as OneLazy or OneLoop.                                 |
   //+------------------------------------------------------------------+
   void MakeRep(const int type,const int min,const int max)
     {
      m_type+=(type-One);
      m_m = min;
      m_n = max;
     }
   //+------------------------------------------------------------------+
   //| Removes redundant nodes from the subtree, and returns a reduced  |
   //| subtree.                                                         |
   //+------------------------------------------------------------------+
   CRegexNode *Reduce()
     {
      CRegexNode *n;
      switch(Type())
        {
         case Alternate_Type:
           {
            n=ReduceAlternation();
            break;
           }
         case Concatenate_Type:
           {
            n=ReduceConcatenation();
            break;
           }
         case Loop_Type:
         case Lazyloop_Type:
           {
            n=ReduceRep();
            break;
           }
         case Group_Type:
           {
            n=ReduceGroup();
            break;
           }
         case Set:
         case Setloop:
           {
            n=ReduceSet();
            break;
           }
         default:
           {
            n=GetPointer(this);
            break;
           }
        }
      return (n);
     }
   //+------------------------------------------------------------------+
   //| Simple optimization. If a concatenation or alternation has only  |
   //| one child strip out the intermediate node. If it has zero        |
   //| children, turn it into an empty.                                 |
   //+------------------------------------------------------------------+
   CRegexNode *StripEnation(const int emptyType)
     {
      switch(ChildCount())
        {
         case 0:
           {
            return new CRegexNode(emptyType, m_options, -1);
           }
         case 1:
           {
            return Child(0);
           }
         default:
           {
            return GetPointer(this);
           }
        }
     }
   //+------------------------------------------------------------------+
   //| Simple optimization. Once parsed into a tree, noncapturing groups|
   //| serve no function,so strip them out.                             |
   //+------------------------------------------------------------------+
   CRegexNode *ReduceGroup()
     {
      CRegexNode *u;
      for(u=GetPointer(this); u.Type()==Group_Type;)
        {
         u=u.Child(0);
        }
      return (u);
     }
   //+------------------------------------------------------------------+
   //| Nested repeaters just get multiplied with each other if they're  |
   //| not too lumpy.                                                   |
   //+------------------------------------------------------------------+
   CRegexNode *ReduceRep()
     {
      CRegexNode *u;
      CRegexNode *child;
      int type;
      int min;
      int max;
      u=GetPointer(this);
      type= Type();
      min = m_m;
      max = m_n;
      for(;;)
        {
         if(u.ChildCount()==0)
           {
            break;
           }
         child=u.Child(0);
         //--- multiply reps of the same type only
         if(child.Type()!=type)
           {
            int childType=child.Type();
            if(!((childType>=Oneloop && childType<=Setloop && type==Loop_Type) || 
               (childType>=Onelazy && childType<=Setlazy && type==Lazyloop_Type)))
              {
               break;
              }
           }
         //--- child can be too lumpy to blur, e.g., (a {100,105}) {3} or (a {2,})? [but things like (a {2,})+ are not too lumpy...]
         if((u.m_m==0 && child.m_m>1) || (child.m_n<child.m_m*2))
           {
            break;
           }
         u=child;
         if(u.m_m>0)
           {
            u.m_m=min=((Int32::MaxValue-1)/u.m_m<min) ? Int32::MaxValue : u.m_m*min;
           }
         if(u.m_n>0)
           {
            u.m_n=max=((Int32::MaxValue-1)/u.m_n<max) ? Int32::MaxValue : u.m_n*max;
           }
        }
      return (min == Int32::MaxValue ? new CRegexNode(Nothing, m_options, -1) : u);
     }
   //+------------------------------------------------------------------+
   //| Simple optimization. If a set is a singleton, an inverse         |
   //| singleton, or empty, it's transformed accordingly.               |
   //+------------------------------------------------------------------+
   CRegexNode *ReduceSet()
     {
      //--- Extract empty-set, one and not-one case as special
      if(CRegexCharClass::IsEmpty(m_str))
        {
         m_type= Nothing;
         m_str = NULL;
        }
      else if(CRegexCharClass::IsSingleton(m_str))
        {
         m_ch=CRegexCharClass::SingletonChar(m_str);
         m_str=NULL;
         m_type+=(One-Set);
        }
      else if(CRegexCharClass::IsSingletonInverse(m_str))
        {
         m_ch=CRegexCharClass::SingletonChar(m_str);
         m_str=NULL;
         m_type+=(Notone-Set);
        }
      return GetPointer(this);
     }
   //+------------------------------------------------------------------+
   //| Basic optimization. Single-letter alternations can be replaced by|
   //| faster set specifications, and nested alternations with no       |
   //| intervening operators can be flattened:                          |
   //|                                                                  |
   //| a|b|c|def|g|h -> [a-c]|def|[gh]                                  |
   //| apple|(?:orange|pear)|grape -> apple|orange|pear|grape           |
   //+------------------------------------------------------------------+
   CRegexNode *ReduceAlternation()
     {
      //--- Combine adjacent sets/ushorts
      bool wasLastSet;
      bool lastNodeCannotMerge;
      RegexOptions optionsLast;
      RegexOptions optionsAt;
      int i;
      int j;
      CRegexNode *at;
      CRegexNode *prev;
      if(m_children==NULL)
        {
         //--- return 
         return new CRegexNode(Nothing, m_options, -1);
        }
      wasLastSet=false;
      lastNodeCannotMerge=false;
      optionsLast=(RegexOptions)0;
      for(i=0,j=0; i<m_children.Count(); i++,j++)
        {
         at=m_children[i];
         if(j<i)
           {
            m_children.Set(j,at);
           }
         for(;;)
           {
            if(at.m_type==Alternate_Type)
              {
               for(int k=0; k<at.m_children.Count(); k++)
                 {
                  at.m_children[k].m_next=GetPointer(this);
                 }
               m_children.InsertRange(i+1,at.m_children);
               j--;
              }
            else if(at.m_type==Set || at.m_type==One)
              {
               //--- Cannot merge sets if L or I options differ, or if either are negated.
               optionsAt=(RegexOptions)(at.m_options &(RightToLeft|IgnoreCase));
               if(at.m_type==Set)
                 {
                  if(!wasLastSet || optionsLast!=optionsAt || lastNodeCannotMerge || !CRegexCharClass::IsMergeable(at.m_str))
                    {
                     wasLastSet=true;
                     lastNodeCannotMerge=!CRegexCharClass::IsMergeable(at.m_str);
                     optionsLast=optionsAt;
                     break;
                    }
                 }
               else if(!wasLastSet || optionsLast!=optionsAt || lastNodeCannotMerge)
                 {
                  wasLastSet=true;
                  lastNodeCannotMerge=false;
                  optionsLast=optionsAt;
                  break;
                 }
               //--- The last node was a Set or a One, we're a Set or One and our options are the same.
               //--- Merge the two nodes.
               j--;
               prev=m_children[j];
               CRegexCharClass *prevCharClass;
               if(prev.m_type==One)
                 {
                  prevCharClass=new CRegexCharClass();
                  prevCharClass.AddChar(prev.m_ch);
                 }
               else
                 {
                  prevCharClass=CRegexCharClass::Parse(prev.m_str);
                 }
               if(at.m_type==One)
                 {
                  prevCharClass.AddChar(at.m_ch);
                 }
               else
                 {
                  CRegexCharClass *atCharClass=CRegexCharClass::Parse(at.m_str);
                  prevCharClass.AddCharClass(atCharClass);
                 }
               prev.m_type = Set;
               prev.m_str  = prevCharClass.ToStringClass();
              }
            else if(at.m_type==Nothing)
              {
               j--;
              }
            else
              {
               wasLastSet=false;
               lastNodeCannotMerge=false;
              }
            break;
           }
        }
      if(j<i)
        {
         m_children.RemoveRange(j,i-j);
        }
      //--- return
      return StripEnation(Nothing);
     }
   //+------------------------------------------------------------------+
   //| Basic optimization. Adjacent strings can be concatenated.        |
   //|                                                                  |
   //| (?:abc)(?:def) -> abcdef                                         |
   //+------------------------------------------------------------------+
   CRegexNode *ReduceConcatenation()
     {
      //--- Eliminate empties and concat adjacent strings/ushorts
      bool wasLastString;
      RegexOptions optionsLast;
      RegexOptions optionsAt;
      int i;
      int j;
      if(m_children==NULL)
        {
         //--- return 
         return new CRegexNode(Empty_Type, m_options, -1);
        }
      wasLastString=false;
      optionsLast=(RegexOptions)0;
      for(i=0,j=0; i<m_children.Count(); i++,j++)
        {
         CRegexNode *at;
         CRegexNode *prev;
         at=m_children[i];
         if(j<i)
           {
            m_children.Set(j,at);
           }
         if(at.m_type==Concatenate_Type && 
            ((at.m_options  &RightToLeft)==(m_options  &RightToLeft)))
           {
            for(int k=0; k<at.m_children.Count(); k++)
              {
               at.m_children[k].m_next=GetPointer(this);
              }
            m_children.InsertRange(i+1,at.m_children);
            j--;
           }
         else if(at.m_type==Multi || at.m_type==One)
           {
            //--- Cannot merge strings if L or I options differ
            optionsAt=(RegexOptions)(at.m_options &(RightToLeft|IgnoreCase));
            if(!wasLastString || optionsLast!=optionsAt)
              {
               wasLastString=true;
               optionsLast=optionsAt;
               continue;
              }
            prev=m_children[--j];
            if(prev.m_type==One)
              {
               prev.m_type= Multi;
               prev.m_str = ShortToString(prev.m_ch);
              }
            if((optionsAt  &RightToLeft)==0)
              {
               if(at.m_type==One)
                 {
                  prev.m_str+=ShortToString(at.m_ch);
                 }
               else
                 {
                  prev.m_str+=at.m_str;
                 }
              }
            else
              {
               if(at.m_type==One)
                 {
                  prev.m_str=ShortToString(at.m_ch)+prev.m_str;
                 }
               else
                 {
                  prev.m_str=at.m_str+prev.m_str;
                 }
              }
           }
         else if(at.m_type==Empty_Type)
           {
            j--;
           }
         else
           {
            wasLastString=false;
           }
        }
      if(j<i)
        {
         m_children.RemoveRange(j,i-j);
        }
      //--- return 
      return StripEnation(Empty_Type);
     }
   //+------------------------------------------------------------------+
   //| MakeQuantifier.                                                  |
   //+------------------------------------------------------------------+
   CRegexNode *MakeQuantifier(const bool lazy,const int min,const int max)
     {
      CRegexNode *result;
      if(min==0 && max==0)
        {
         //--- return 
         return new CRegexNode(Empty_Type, m_options, -1);
        }
      if(min==1 && max==1)
        {
         //--- return 
         return GetPointer(this);
        }
      switch(m_type)
        {
         case One:
         case Notone:
         case Set:
           {
            MakeRep(lazy ? Onelazy : Oneloop,min,max);
            //--- return 
            return GetPointer(this);
           }
         default:
           {
            result=new CRegexNode(lazy ? Lazyloop_Type : Loop_Type,m_options,min,max);
            result.AddChild(GetPointer(this));
            //--- return 
            return (result);
           }
        }
     }
   //+------------------------------------------------------------------+
   //| AddChild.                                                        |
   //+------------------------------------------------------------------+
   void AddChild(CRegexNode *newChild)
     {
      CRegexNode *reducedChild;
      if(m_children==NULL)
        {
         m_children=new List<CRegexNode*>(4);
        }
      reducedChild=newChild.Reduce();
      m_children.Add(reducedChild);
      reducedChild.m_next=GetPointer(this);
     }
   //+------------------------------------------------------------------+
   //| Child.                                                           |
   //+------------------------------------------------------------------+     
   CRegexNode *Child(const int i)
     {
      return (m_children[i]);
     }
   //+------------------------------------------------------------------+
   //| Gets the next node.                                              |
   //+------------------------------------------------------------------+
   CRegexNode *Next()
     {
      return (m_next);
     }
   //+------------------------------------------------------------------+
   //| Sets the next node.                                              |
   //+------------------------------------------------------------------+     
   void Next(CRegexNode *value)
     {
      m_next=value;
     }
   //+------------------------------------------------------------------+
   //| Childrens.                                                       |
   //+------------------------------------------------------------------+     
   List<CRegexNode*>*Children()
     {
      return (m_children);
     }
   //+------------------------------------------------------------------+
   //| ChildCount.                                                      |
   //+------------------------------------------------------------------+
   int ChildCount()
     {
      return (m_children == NULL ? 0 : m_children.Count());
     }
   //+------------------------------------------------------------------+
   //| Options.                                                         |
   //+------------------------------------------------------------------+
   RegexOptions Options()
     {
      return (m_options);
     }
   //+------------------------------------------------------------------+
   //| Str.                                                             |
   //+------------------------------------------------------------------+
   string Str()
     {
      return (m_str);
     }
   //+------------------------------------------------------------------+
   //| Ch.                                                              |
   //+------------------------------------------------------------------+
   ushort Ch()
     {
      return (m_ch);
     }
   //+------------------------------------------------------------------+
   //| Max.                                                             |
   //+------------------------------------------------------------------+
   int Max()
     {
      return (m_n);
     }
   //+------------------------------------------------------------------+
   //| Min.                                                             |
   //+------------------------------------------------------------------+
   int Min()
     {
      return (m_m);
     }
   //+------------------------------------------------------------------+
   //| Type.                                                            |
   //+------------------------------------------------------------------+
   int Type()
     {
      return (m_type);
     }
#ifdef _DEBUG
   static string     TypeStr[];
   //+------------------------------------------------------------------+
   //| Description.                                                     |
   //+------------------------------------------------------------------+
   string Description()
     {
      string ArgText;
      ArgText+=(TypeStr[m_type]);
      if((m_options  &ExplicitCapture)!=0)         { ArgText+=("-C"); }
      if((m_options  &IgnoreCase)!=0)              { ArgText+=("-I"); }
      if((m_options  &RightToLeft)!=0)             { ArgText+=("-L"); }
      if((m_options  &Multiline)!=0)               { ArgText+=("-M"); }
      if((m_options  &Singleline)!=0)              { ArgText+=("-S"); }
      if((m_options  &IgnorePatternWhitespace)!=0) { ArgText+=("-X"); }
      if((m_options  &ECMAScript)!=0)              { ArgText+=("-E"); }
      switch(m_type)
        {
         case Oneloop:
         case Notoneloop:
         case Onelazy:
         case Notonelazy:
         case One:
         case Notone:
           {
            ArgText+=("(Ch = "+CRegexCharClass::CharDescription(m_ch)+")");
            break;
           }
         case Capture_Type:
           {
            ArgText+=("(index = "+(string)m_m+", unindex = "+(string)m_n+")");
            break;
           }
         case Ref:
         case Testrefnode_Type:
           {
            ArgText+=("(index = "+(string)m_m+")");
            break;
           }
         case Multi:
           {
            ArgText+=("(String = "+m_str+")");
            break;
           }
         case Set:
         case Setloop:
         case Setlazy:
           {
            ArgText+=("(Set = "+CRegexCharClass::SetDescription(m_str)+")");
            break;
           }
        }
      switch(m_type)
        {
         case Oneloop:
         case Notoneloop:
         case Onelazy:
         case Notonelazy:
         case Setloop:
         case Setlazy:
         case Loop_Type:
         case Lazyloop_Type:
           {
            ArgText+=("(Min = "+(string)m_m+", Max = "+(m_n==Int32::MaxValue ? "inf" :(string)m_n)+")");
            break;
           }
        }
      //--- return 
      return (ArgText);
     }
   //+------------------------------------------------------------------+
   //| Dump.                                                            |
   //+------------------------------------------------------------------+     
   void Dump()
     {
      List<int>Stack();
      CRegexNode *CurNode;
      int CurChild;
      CurNode=GetPointer(this);
      CurChild=0;
      Print(CurNode.Description());
      for(;;)
        {
         if(CurNode.m_children!=NULL && CurChild<CurNode.m_children.Count())
           {
            Stack.Add(CurChild+1);
            CurNode=CurNode.m_children[CurChild];
            CurChild=0;
            int Depth=Stack.Count();
            if(Depth>32)
              {
               Depth=32;
              }
            Print(StringSubstr(Space,0,Depth)+CurNode.Description());
           }
         else
           {
            if(Stack.Count()==0)
              {
               break;
              }
            CurChild=Stack[Stack.Count()-1];
            Stack.RemoveAt(Stack.Count()-1);
            CurNode=CurNode.m_next;
           }
        }
     }
#endif
  };
#ifdef _DEBUG
static const string      CRegexNode::Space="                                ";
static string CRegexNode::TypeStr[]=
  {
   "Onerep","Notonerep","Setrep",
   "Oneloop","Notoneloop","Setloop",
   "Onelazy","Notonelazy","Setlazy",
   "One","Notone","Set",
   "Multi","Ref",
   "Bol","Eol","Boundary","Nonboundary",
   "ECMABoundary","NonECMABoundary",
   "Beginning","Start","EndZ","End",
   "Nothing","Empty",
   "Alternate","Concatenate",
   "Loop","Lazyloop",
   "Capture","Group","Require","Prevent","Greedy",
   "Testref","Testgroup"
  };
#endif
//+------------------------------------------------------------------+
//| CRegexNode types.                                                 |
//+------------------------------------------------------------------+
enum REGEX_NODE_TYPES
  {
   //--- interior nodes do not correpond to primitive operations, 
   //--- but control structures compositing other operations
   //--- concat and alternate take n children, and can run forward or backwards
   Empty_Type        =23,             //          ()
   Alternate_Type    =24,             //          a|b
   Concatenate_Type  =25,             //          ab
   Loop_Type         =26,             // m,x      * + ? {,}
   Lazyloop_Type     =27,             // m,x      *? +? ?? {,}?
   Capture_Type      =28,             // n        ()
   Group_Type        =29,             //          (?:)
   Require_Type      =30,             //          (?=) (?<=)
   Prevent_Type      =31,             //          (?!) (?<!)
   Greedy_Type       =32,             //          (?>) (?<)
   Testrefnode_Type  =33,             //          (?(n) | )
   Testgroup_Type    =34,             //          (?(...) | )  
  };
static List<CRegexNode*>CRegexNode::s_allNodes();
//+------------------------------------------------------------------+
