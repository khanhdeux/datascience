//+------------------------------------------------------------------+
//|                                               Regular Expression |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//| Regular Expression Library from .NET Framework 4.6.1 implemented |
//| in MetaQuotes Language 5 (MQL5)                                  |
//| Original sources at https://github.com/Microsoft/referencesource |
//|                                                                  |
//| The capabilities of the Regular Expression Library include:      |
//| - Lazy quantifiers                                               |
//| - Positive and negative lookbehind                               |
//| - Conditional evaluation                                         |
//| - Balancing group definitions                                    |
//| - Nonbacktracking subexpressions                                 |
//| - Right-to-left matching                                         |
//|                                                                  |
//| If you find any functional differences between Regular Expression|
//| Library for MQL5 and the original .NET Framework 4.6.1 project,  |
//| please contact developers of MQL5 on the Forum at www.mql5.com.  |
//|                                                                  |
//| You can report bugs found in the computational algorithms of the |
//| Regular Expression Library from .Net Framework 4.6.1 by notifying|
//| the project coordinators.                                        |
//+------------------------------------------------------------------+
//|                     The MIT License (MIT)                        |
//|                                                                  |
//| Permission is hereby granted, free of charge, to any person      |
//| obtaining a copy of this software and associated documentation   |
//| files (the "Software"), to deal in the Software without          |
//| restriction, including without limitation the rights to use,     |
//| copy, modify, merge, publish, distribute, sublicense, and/or sell|
//| copies of the Software, and to permit persons to whom the        |
//| Software is furnished to do so, subject to the following         |
//| conditions:                                                      |
//|                                                                  |
//| The above copyright notice and this permission notice shall be   |
//| included in all copies or substantial portions of the Software.  |
//|                                                                  |
//| THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,  |
//| EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES  |
//| OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         |
//| NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      |
//| HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,     |
//| WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     |
//| FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR    |
//| OTHER DEALINGS IN THE SOFTWARE.                                  |
//|                                                                  |
//| A copy of the MIT License (MIT) is available at                  |
//| https://opensource.org/licenses/MIT                              |
//+------------------------------------------------------------------+
#property strict
class CCaptureCollection;
#include "RegexCaptureCollection.mqh"
#include "RegexCapture.mqh"
//+------------------------------------------------------------------+
//| Purpose: Represents the results from a single capturing group.   |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| CGroup represents the substring or substrings that are captured  |
//| by a single capturing group after one regular expression match.  |
//|                                                                  |
//| CGroup represents the results from a single capturing group. A   |
//| capturing group can  capture zero,one,or more strings in a single|
//| match because of quantifiers,so  CGroup supplies a collection of |
//| CCapture objects.                                                |
//+------------------------------------------------------------------+
class CGroup : public CCapture
  {
protected:
   int               m_caps[];
   int               m_capcount;
   CCaptureCollection *m_capcoll;
public:
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CGroup(const string text,const int capcount)
   : CCapture(text,0,0)
     {
      ArrayResize(m_caps,2);
      ZeroMemory(m_caps);
      m_capcount=capcount;
     }
   //+------------------------------------------------------------------+
   //| Constructor with parameters.                                     |
   //+------------------------------------------------------------------+
                     CGroup(const string text,int &caps[],const int capcount)
   : CCapture(text,capcount==0 ? 0 : caps[(capcount-1)*2],capcount==0 ? 0 : caps[(capcount*2)-1])
     {
      ArrayCopy(m_caps,caps);
      m_capcount=capcount;
     }
   //+------------------------------------------------------------------+
   //| Destructor without parameters.                                   |
   //+------------------------------------------------------------------+
                    ~CGroup()
     {
      if(CheckPointer(m_capcoll)==POINTER_DYNAMIC)
        {
         delete m_capcoll;
        }
     }  
   //+------------------------------------------------------------------+
   //| Gets the count of matches.                                       |
   //+------------------------------------------------------------------+
   int Count()
     {
      return (m_capcount);
     }
   //+------------------------------------------------------------------+
   //| Gets a value indicating whether the match is successful.         |
   //+------------------------------------------------------------------+
   bool Success()
     {
      return (m_capcount != 0);
     }
   //+------------------------------------------------------------------+
   //| Gets a collection of all the captures matched by the capturing   |
   //| group, in innermost-leftmost-first order (or                     |
   //| innermost-rightmost-first order if the regular expression is     |
   //| modified with the RegexOptions::RightToLeft option).The          |
   //| collection may have zero or more items.                          |
   //+------------------------------------------------------------------+
   CCaptureCollection *Captures()
     {
      if(m_capcoll==NULL)
        {
         m_capcoll=new CCaptureCollection(GetPointer(this));
        }
      return (m_capcoll);
     }
   //+------------------------------------------------------------------+
   //| Returns a CGroup object equivalent to the one supplied that is   |
   //| safe to share between multiple threads.                          |
   //+------------------------------------------------------------------+
   static CGroup *Synchronized(CGroup *inner)
     {
      if(inner==NULL)
        {
         Print("Argument 'inner' = NULL."," __FILE__ = ",__FILE__," __LINE__ = ",__LINE__);
         return (NULL);
        }
      //--- force Captures to be computed.
      CCaptureCollection *capcoll;
      CCapture *dummy;
      capcoll=inner.Captures();
      if(inner.m_capcount>0)
        {
         dummy=capcoll[0];
        }
      return (inner);
     }
   //+------------------------------------------------------------------+
   //| Operator ([]).                                                   |
   //+------------------------------------------------------------------+
   int operator[](const int i)
     {
      return (m_caps[i]);
     }
   //+------------------------------------------------------------------+
   //| Gets the empty group object.                                     |
   //+------------------------------------------------------------------+     
   static CGroup *Empty()
     {
      static int empty_array[];
      static CGroup empty("",empty_array,0); // the empty group object
      return GetPointer(empty);
     }
  };
//+------------------------------------------------------------------+
