/*
 @license Copyright 2021 akumidv (https://github.com/akumidv/)
 SPDX-License-Identifier: Apache-2.0
*/
'use strict';

let filtersCount = 0

function getFilterRowHtml(filterNum) {
  return `<td><select id="filterMode${filterNum}"><option value="off">off</option><option value="more">more</option><option value="less">less</option></select></td>
        <td><input id="filterValue${filterNum}" type="number" style="width: 5em"></td>
        <td><select id="filterParamName${filterNum}">
          <option value="Net Profit: All">Net Profit: All</option>
          <option value="Net Profit %: All">Net Profit %: All</option>
          <option value="Net Profit: Long">Net Profit: Long</option>
          <option value="Net Profit %: Long">Net Profit %: Long</option>
          <option value="Net Profit: Short">Net Profit: Short</option>
          <option value="Net Profit %: Short">Net Profit %: Short</option>
          <option value="Gross Profit: All">Gross Profit: All</option>
          <option value="Gross Profit %: All">Gross Profit %: All</option>
          <option value="Gross Profit: Long">Gross Profit: Long</option>
          <option value="Gross Profit %: Long">Gross Profit %: Long</option>
          <option value="Gross Profit %: Short">Gross Profit %: Short</option>
          <option value="Gross Loss: All">Gross Loss: All</option>
          <option value="Gross Loss %: All">Gross Loss: All</option>
          <option value="Gross Loss: Long">Gross Loss: Long</option>
          <option value="Gross Loss %: Long">Gross Loss %: Long</option>
          <option value="Gross Loss: Short">Gross Loss: Short</option>
          <option value="Gross Loss %: Short">Gross Loss %: Short</option>
          <option value="Max Drawdown">Max Drawdown</option>
          <option value="Max Drawdown %">Max Drawdown %</option>
          <option value="Buy & Hold Return">Buy & Hold Return</option>
          <option value="Buy & Hold Return %">Buy & Hold Return %</option>
          <option value="Sharpe Ratio">Sharpe Ratio</option>
          <option value="Sortino Ratio">Sortino Ratio</option>
          <option value="Profit Factor: All">Profit Factor: All</option>
          <option value="Profit Factor: Long">Profit Factor: Long</option>
          <option value="Profit Factor: Short">Profit Factor: Short</option>
          <option value="Max Contracts Held: All">Max Contracts Held: All</option>
          <option value="Max Contracts Held: Long">Max Contracts Held: Long
          </option>
          <option value="Max Contracts Held: Short">Max Contracts Held: Short
          </option>
          <option value="Open PL">Open PL</option>
          <option value="Open PL %">Open PL %</option>
          <option value="Commission Paid: All">Commission Paid: All</option>
          <option value="Commission Paid: Long">Commission Paid: Long"</option>
          <option value="Commission Paid: Short">Commission Paid: Short</option>
          <option value="Total Closed Trades: All">Total Closed Trades: All
          </option>
          <option value="Total Closed Trades: Long">Total Closed Trades: Long
          </option>
          <option value="Total Closed Trades: Short">Total Closed Trades: Short
          </option>
          <option value="Total Open Trades: All">Total Open
              Trades: All
          </option>
          <option value="Total Open Trades: Long">Total Open Trades: Long</option>
          <option value="Total Open Trades: Short">Total Open Trades: Short
          </option>

          <option value="Number Winning Trades: All">Number Winning Trades: All
          </option>
          <option value="Number Winning Trades: Long">Number Winning Trades:
              Long
          </option>
          <option value="Number Winning Trades: Short">Number Winning Trades:
              Short
          </option>
          <option value="Number Losing Trades: All">Number Losing Trades: All
          </option>
          <option value="Number Losing Trades: Long">Number Losing Trades: Long
          </option>
          <option value="Number Losing Trades: Short">Number Losing Trades:
              Short
          </option>

          <option value="Percent Profitable: All">Percent Profitable: All</option>
          <option value="Percent Profitable: Long">Percent Profitable: Long
          </option>
          <option value="Percent Profitable: Short">Percent Profitable: Short
          </option>
          <option value="Avg Trade: All">Avg Trade: All</option>
          <option value="Avg Trade %: All">Avg Trade %: All</option>
          <option value="Avg Trade: Long">Avg Trade: Long</option>
          <option value="Avg Trade: Long %">Avg Trade: Long %</option>
          <option value="Avg Trade: Short">Avg Trade: Short</option>
          <option value="Avg Trade: Short %">Avg Trade: Short %</option>
          <option value="Avg Winning Trade: All">Avg Winning Trade: All</option>
          <option value="Avg Winning Trade %: All">Avg Winning Trade %: All
          </option>

          <option value="Avg Winning Trade: Long">Avg Winning Trade: Long</option>
          <option value="Avg Winning Trade: Long %">Avg Winning Trade: Long %
          </option>
          <option value="Avg Winning Trade: Short">Avg Winning Trade: Short
          </option>
          <option value="Avg Winning Trade: Short %">Avg Winning Trade: Short %
          </option>

          <option value="Avg Losing Trade: All">Avg Losing Trade: All</option>
          <option value="Avg Losing Trade %: All">Avg Losing Trade: All</option>
          <option value="Avg Losing Trade: Long">Avg Losing Trade: Long</option>
          <option value="Avg Losing Trade: Long %">Avg Losing Trade: Long %
          </option>
          <option value="Avg Losing Trade: Short">Avg Losing Trade: Short</option>
          <option value="Avg Losing Trade: Short %">Avg Losing Trade: Short %
          </option>


          <option value="Ratio Avg Win / Avg Loss: All">Ratio Avg Win / Avg Loss:
              All
          </option>
          <option value="Ratio Avg Win / Avg Loss: Long">Ratio Avg Win / Avg Loss:
              Long
          </option>
          <option value="Ratio Avg Win / Avg Loss: Short">Ratio Avg Win / Avg
              Loss: Short
          </option>


          <option value="Largest Winning Trade: All">Largest Winning Trade: All
          </option>
          <option value="Largest Winning Trade %: All">Largest Winning Trade %:
              All
          </option>
          <option value="Largest Winning Trade: Long">Largest Winning Trade:
              Long
          </option>
          <option value="Largest Winning Trade %: Long">Largest Winning Trade %:
              Long
          </option>
          <option value="Largest Winning Trade: Short">Largest Winning Trade:
              Short
          </option>
          <option value="Largest Winning Trade %: Short">Largest Winning Trade %:
              Short
          </option>

          <option value="Largest Losing Trade: All">Largest Losing Trade: All
          </option>
          <option value="Largest Losing Trade %: All">Largest Losing Trade %:
              All
          </option>
          <option value="Largest Losing Trade: Long">Largest Losing Trade: Long
          </option>
          <option value="Largest Losing Trade %: Long">Largest Losing Trade %:
              Long
          </option>
          <option value="Largest Losing Trade: Short">Largest Losing Trade:
              Short
          </option>
          <option value="Largest Losing Trade %: Short">Largest Losing Trade %:
              Short
          </option>

          <option value="Avg # Bars in Trades: All">Avg # Bars in Trades: All
          </option>
          <option value="Avg # Bars in Trades: Long">Avg # Bars in Trades: Long
          </option>
          <option value="Avg # Bars in Trades: Short">Avg # Bars in Trades:
              Short
          </option>
          <option value="Avg # Bars in Winning Trades: All">Avg # Bars in Winning
              Trades: All
          </option>
          <option value="Avg # Bars in Winning Trades: Long">Avg # Bars in Winning
              Trades: Long
          </option>
          <option value="Avg # Bars in Winning Trades: Short">Avg # Bars in
              Winning Trades: Short
          </option>
          <option value="Avg # Bars in Losing Trades: All">Avg # Bars in Losing
              Trades: All
          </option>
          <option value="Avg # Bars in Losing Trades: Long">Avg # Bars in Losing
              Trades: Long
          </option>
          <option value="Avg # Bars in Losing Trades: Short">Avg # Bars in Losing
              Trades: Short
          </option>
          <option value="Margin Calls: All">Margin Calls: All</option>
          <option value="Margin Calls: Long">Margin Calls: Long</option>
          <option value="Margin Calls: Short">Margin Calls: Short</option>
        </select></td>
        <td><button id="filterRemove${filterNum}" class="button-small button-red">x</button></td>`
}

function addFilterRow(mode=null, value=null, parameterName=null) {
  let tbody = document.getElementById('filtersParams')
  const row = document.createElement('tr')
  row.innerHTML = getFilterRowHtml(filtersCount)
  tbody.appendChild(row)
  const filterModeEl = document.getElementById(`filterMode${filtersCount}`)
  if(filterModeEl) {
    filterModeEl.addEventListener('change', filtersUpdate)
    filterModeEl.value = mode !== null ? mode : 'off'
  }
  const filterValueEl = document.getElementById(`filterValue${filtersCount}`)
  if(filterValueEl) {
    filterValueEl.addEventListener('change', filtersUpdate)
    filterValueEl.value = value !== null ? value : '5'
  }
  const filterParamNameEl = document.getElementById(`filterParamName${filtersCount}`)
  if(filterParamNameEl) {
    filterParamNameEl.addEventListener('change', filtersUpdate)
    filterParamNameEl.value = parameterName !== null ? parameterName : 'Total Closed Trades: All'
  }

  const idName = `filterRemove${filtersCount}`
  if (document.getElementById(idName)) {
    document.getElementById(idName).addEventListener('click', () => {
      const rowEl = document.getElementById(idName).parentNode.parentNode
      rowEl.parentNode.removeChild(rowEl)
      filtersUpdate()
    })
  }

  filtersCount += 1
}

function filtersUpdate() {
  saveOptions('filtersUpdate')
}

function getOptions(signal) {
  const daviddTechOptions = {}
  daviddTechOptions.isMaximizing = document.getElementById('optMinmax').checked
  daviddTechOptions.optParamName = document.getElementById('optParamName').value || 'Net Profit %: All'
  daviddTechOptions.optMethod = document.getElementById('optMethod').value || 'random'

  daviddTechOptions.wlMaxCycles = document.getElementById('wlMaxCycles').value || 100
  daviddTechOptions.wlTimeFrameList = document.getElementById('wlTimeFrameList').value || '30m,1h,4h,1d'
  daviddTechOptions.filters = getFilters()
  daviddTechOptions['disclaimer'] = document.getElementById('disclaimer').checked
  daviddTechOptions['randomDelay'] = document.getElementById('randomDelay').checked
  daviddTechOptions.backtestDelay = document.getElementById('backtestDelay').value
  daviddTechOptions.shouldDeepBacktest = document.getElementById('shouldDeepBacktest').checked
  daviddTechOptions.deepStartDate = document.getElementById('deepStartDate').value
  daviddTechOptions.deepEndDate = document.getElementById('deepEndDate').value
  daviddTechOptions.isInOutSample = document.getElementById('isInOutSample').checked
  daviddTechOptions.inOutRates = document.getElementById('inOutRates').value
  // daviddTechOptions.activatedDiscordUpload = document.getElementById('activatedDiscordUpload').checked
  daviddTechOptions.dataLoadingTime = document.getElementById('dataLoadingTime').value
  daviddTechOptions.tabId = signal === 'wlMaxCycles' || signal.startsWith('wl') ? 4 : 2 // All main option on the second tab
  return daviddTechOptions
}

function saveOptions(signal) {
  const daviddTechOptions = getOptions(signal)
  chrome.storage.local.set({'daviddTech_options': daviddTechOptions})
  console.log('Saved options', daviddTechOptions)
}

async function sendSignalToActiveTab(signal, body = null) {
  const isDisclaimerAccepted = document.getElementById('disclaimer').checked
  if (!isDisclaimerAccepted) {
    document.getElementById('msg-text').innerHTML = 'To continue working with the extension, accept the disclaimer at the bottom of the window'
    document.getElementById('shim').style.display= 'block'
    document.getElementById('shim').style.height = document.body && document.body.scrollHeight ? document.body.scrollHeight : 1024
    document.getElementById('msgbx').style.display = 'block'
    return
  }

  let daviddTechOptions = null
  if (['clearAll', 'clearResults', 'testAction'].includes(signal)) {
    chrome.runtime.sendMessage({ action: signal}, async function (apiResponse) {
      console.log('Done', signal)
    });
    return
  }
  daviddTechOptions = getOptions(signal)
  chrome.storage.local.set({'daviddTech_options': daviddTechOptions})
  if (signal === 'disclaimer')
    return
  chrome.tabs.query({active: true, currentWindow: true}, (tabs) => {
    if(!tabs[0].url.includes('tradingview.com')) {
      document.getElementById('msg-text').innerHTML = 'To work with the extension, activate it on the tab with the opened <a href="https://www.tradingview.com/chart" target="_blank">Tradingview chart</a>.'
      document.getElementById('shim').style.display= 'block'
      document.getElementById('shim').style.height = document.body && document.body.scrollHeight ? document.body.scrollHeight : 1024
      document.getElementById('msgbx').style.display = 'block'
      return
    } else if (!tabs[0].url.startsWith('https://www.tradingview.com') && !tabs[0].url.startsWith('https://en.tradingview.com')) {
      document.getElementById('msg-text').innerHTML = 'This extension only works with the English version of tradingview. Please open it by  the  <a href="https://en.tradingview.com/chart" target="_blank">link</a>.'
      document.getElementById('shim').style.display= 'block'
      document.getElementById('msgbx').style.display = 'block'
      return
    }
    const message = {action: signal, body}
    if(daviddTechOptions)
      message.options = daviddTechOptions
    if(signal === 'wlExportParameters') {
      message.ticker = document.getElementById('wlTicker').value
    }
    chrome.tabs.sendMessage(tabs[0].id, message, function(response) {
      window.close();
    });
  });
}

function getFilters() {
  const filters = []
  let allFiltersRows = document.getElementById('filtersParams').getElementsByTagName('tr')
  if (allFiltersRows) {
    let idx = 0

    for(let row of allFiltersRows) {
      const cells = row.getElementsByTagName('td')
      if (!cells || cells.length !== 4)
        continue
      const modeEl = cells[0].getElementsByTagName('select')
      const valueEl = cells[1].getElementsByTagName('input')
      const paramEl = cells[2].getElementsByTagName('select')
      if(!modeEl || !modeEl.length || !valueEl || !valueEl.length || !paramEl || !paramEl.length)
        continue
      const filterMode = modeEl[0].value
      const filterVal = valueEl[0].value
      const paramName = paramEl[0].value
      console.log(idx, filterMode, filterVal, paramName)
      filters.push({mode: filterMode, value: filterVal, parameterName: paramName})
      idx += 1
    }
  } else {
    console.log('Empty. Remove all filters')
  }
  console.log('Filters', filters)
  return filters
}

function sendAlertSignalToActiveTab(message) {
  sendSignalToActiveTab('showAlert', message)
}

function handleTestStrategy() {
  const isDeepTest = document.querySelector('#shouldDeepBacktest').checked
  const isInOutSample = document.querySelector('#isInOutSample').checked
  const optMethod = document.querySelector('#optMethod').value

  if (!isDeepTest && isInOutSample) {
    sendAlertSignalToActiveTab("In & out sample feature requires deep backtesting. Enable deep backtesting.")
    return
  }

  if (isDeepTest && isInOutSample && optMethod !== "random") {
    sendAlertSignalToActiveTab("Only random, all random is currently available for Walk Forward.")
    return
  }
  
  sendSignalToActiveTab('testStrategy')
}

document.addEventListener('DOMContentLoaded', () => {
  chrome.tabs.query({active: true, currentWindow: true}, (tabs) => {
    if (!tabs[0].url.startsWith('https://www.tradingview.com') && !tabs[0].url.startsWith('https://en.tradingview.com')) {
      const rootElement = document.querySelector(':root')
      rootElement.style.setProperty('--warningVisible', 'block')
      for(let id of ['testStrategy', 'wlCheckStrategy', 'wlTestStrategy', 'wlCheckStrategyOnTimeframe']) {
        if(document.getElementById(id))
          document.getElementById(id).disabled = true
      }
    } else {
      const rootElement = document.querySelector(':root')
      rootElement.style.setProperty('--warningVisible', 'none')
    }
  })
  chrome.storage.local.get('daviddTech_options', async (getResults) => {
    // console.log('daviddTech_options',getResults)
    let tabId = 1
    if(getResults['daviddTech_options']) {
      const daviddTechOptions = getResults['daviddTech_options']
      console.log('daviddTech_options',daviddTechOptions)
      tabId = daviddTechOptions['tabId'] ? daviddTechOptions['tabId'] : 1
      if(document.getElementById('optMinmax') && daviddTechOptions.hasOwnProperty('isMaximizing'))
        document.getElementById('optMinmax').checked = daviddTechOptions.isMaximizing
      if(document.getElementById('optParamName') && daviddTechOptions.hasOwnProperty('optParamName'))
        document.getElementById('optParamName').value = daviddTechOptions.optParamName
      if(document.getElementById('optMethod') && daviddTechOptions.hasOwnProperty('optMethod'))
        document.getElementById('optMethod').value = daviddTechOptions.optMethod
      if(document.getElementById('filtersParams') && daviddTechOptions.hasOwnProperty('filters')) {
        for (let filter of daviddTechOptions['filters']) {
          addFilterRow(filter['mode'], filter['value'], filter['parameterName'])
        }
      }
      if(document.getElementById('disclaimer')) {
        const disclaimer = daviddTechOptions.hasOwnProperty('disclaimer') ? daviddTechOptions['disclaimer'] : false
        console.log('disclaimer', disclaimer)
        document.getElementById('disclaimer').checked = disclaimer
      }
      if(document.getElementById('wlMaxCycles') && daviddTechOptions.hasOwnProperty('wlMaxCycles'))
        document.getElementById('wlMaxCycles').value = daviddTechOptions.wlMaxCycles

      if(document.getElementById('randomDelay') && daviddTechOptions.hasOwnProperty('randomDelay'))
        document.getElementById('randomDelay').checked = daviddTechOptions.randomDelay
      if(document.getElementById('backtestDelay') && daviddTechOptions.hasOwnProperty('backtestDelay'))
        document.getElementById('backtestDelay').value = daviddTechOptions.backtestDelay
      if(document.getElementById('dataLoadingTime') && daviddTechOptions.hasOwnProperty('dataLoadingTime'))
        document.getElementById('dataLoadingTime').value = daviddTechOptions.dataLoadingTime

      if(document.getElementById('shouldDeepBacktest') && daviddTechOptions.hasOwnProperty('shouldDeepBacktest'))
        document.getElementById('shouldDeepBacktest').checked = daviddTechOptions.shouldDeepBacktest
      if(document.getElementById('deepStartDate') && daviddTechOptions.hasOwnProperty('deepStartDate'))
        document.getElementById('deepStartDate').value = daviddTechOptions.deepStartDate
      if(document.getElementById('deepEndDate') && daviddTechOptions.hasOwnProperty('deepEndDate'))
        document.getElementById('deepEndDate').value = daviddTechOptions.deepEndDate

      if (document.getElementById('isInOutSample') && daviddTechOptions.hasOwnProperty('isInOutSample')) {
        document.getElementById('isInOutSample').checked = daviddTechOptions.isInOutSample
      }
      if (document.getElementById('inOutRates') && daviddTechOptions.hasOwnProperty('inOutRates')) {
        document.getElementById('inOutRates').value = daviddTechOptions.inOutRates
      }
      // if (document.getElementById('activatedDiscordUpload') && daviddTechOptions.hasOwnProperty('activatedDiscordUpload')) {
      //   document.getElementById('activatedDiscordUpload').checked = daviddTechOptions.activatedDiscordUpload
      // }
    }
    const link = document.querySelector(`div.tabs__links > a${typeof tabId === 'number' && tabId > 0 && tabId < 5 ? ':nth-child('+tabId+')' : ''}`);
    if (link) // Activate saved or fist tab
      link.click();
  })
  chrome.storage.local.get('daviddTech_bestBacktestResultsForList', async (getResults) => {
    const backtest = getResults['daviddTech_bestBacktestResultsForList']
    if(document.getElementById('wlTicker') && backtest && backtest.type) {
      let optionHtml = ''
      if (backtest.type === 'tickers' && backtest.tickers) {
        for (let ticker of backtest.tickers) {
          optionHtml += `<option value="${ticker}">${ticker}</option>`
        }
      }
      else if (backtest.type === 'timeframes' && backtest.timeframes) {
        optionHtml += `<option value="none">none</option>`
        // for (let timeframe of backtest.timeframes) { // Need to change save tickers to timeframe. Does it have sense?
        //   optionHtml += `<option value="${timeframe}">${timeframe}</option>`
        // }
      }
      document.getElementById('wlTicker').innerHTML = optionHtml
    }
  })

  for(let id of ['downloadStrategyTestResults', 'getStrategyTemplate', 'uploadStrategyTestParameters',
    'uploadToDiscord',
    'wlCheckStrategy', 'wlTestStrategy', 'wlCheckStrategyOnTimeframe', 'wlDownloadStrategyTestResults', 'wlExportParameters',
    'clearAll', 'show3DChart', 'disclaimer', 'testAction']) {
    if (document.getElementById(id))
      document.getElementById(id).addEventListener('click', function () {
        sendSignalToActiveTab(id)
      });
  }

  if (document.getElementById('testStrategy')) {
    document.getElementById('testStrategy').addEventListener('click', () => {
      handleTestStrategy()
    })
  }

  document.getElementById('closeMsg').addEventListener('click', function () {
    document.getElementById('shim').style.display = 'none'
    document.getElementById('msgbx').style.display = 'none';
  });

  for(let optId of ['optMinmax', 'optParamName', 'optMethod', 'optFilterMore', 'optFilterLess', // 'optFilterOff',
    'optFilterValue', 'optFilterParamName', 'randomDelay']) { //, 'wlMaxCycles', 'wlTimeFrameList'
    if (document.getElementById(optId))
      document.getElementById(optId).addEventListener('click', () => {saveOptions(optId)});
  }

  if (document.getElementById('isInOutSample')) {
    document.getElementById('isInOutSample').addEventListener('click', () =>
    {
      if (document.getElementById('isInOutSample').checked && !document.getElementById("shouldDeepBacktest").checked) {
        document.getElementById("shouldDeepBacktest").click();
      }
      saveOptions('isInOutSample')
    });
  }

  if (document.getElementById('inOutRates')) {
    document.getElementById('inOutRates').addEventListener('change', () => {saveOptions('inOutRates')});
  }

  // if (document.getElementById('activatedDiscordUpload')) {
  //   document.getElementById('activatedDiscordUpload').addEventListener('change', () => saveOptions('activatedDiscordUpload'))
  // }

  if (document.getElementById("shouldDeepBacktest")) {
    document.getElementById("shouldDeepBacktest").addEventListener('click', () => {
      if (document.getElementById("shouldDeepBacktest").checked) {
        const startDate = new Date(new Date().setFullYear(new Date().getFullYear() - 2)).toISOString().split('T')[0];
        document.getElementById('deepStartDate').value = startDate;

        const endDate = new Date(new Date().setFullYear(new Date().getFullYear())).toISOString().split('T')[0];
        document.getElementById('deepEndDate').value = endDate;
      }
      saveOptions("shouldDeepBacktest")
    });
  }

  for(let optId of ['optFilterValue', 'wlMaxCycles', 'wlTimeFrameList', 'backtestDelay',
      'dataLoadingTime', 'deepStartDate', 'deepEndDate']) {
    if(document.getElementById(optId))
      document.getElementById(optId).addEventListener('blur', () => {saveOptions(optId)});
  }

  document.getElementById('addFilter').addEventListener('click', () => {
    addFilterRow()
    filtersUpdate()
  })
});
