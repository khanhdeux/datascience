const ui = {
  isMsgShown: false
}

const scriptFonts = document.createElement('style');
scriptFonts.innerHTML = '@font-face {' +
  '    font-family: "Font Awesome 5 Free";' +
  '    font-style: normal;\n' +
  '    font-weight: 900;' +
  '    font-display: block;' +
  `    src: url(${chrome.runtime.getURL("fonts/fa-solid-900.woff2")}) format('woff2');` +
  '}\n' +
  '.davidd_icon::before {\n' +
  '    display: inline-block;\n' +
  '    font-style: normal;\n' +
  '    font-variant: normal;\n' +
  '    text-rendering: auto;\n' +
  '    -webkit-font-smoothing: antialiased;\n' +
  '  }\n' +
  '.davidd_download::before {\n' +
  '    font-family: "Font Awesome 5 Free"; font-weight: 900; font-size: 1.25em; content: "\\f56d";\n' +
  '  }\n' +
  '.davidd_upload::before {\n' +
  '    font-family: "Font Awesome 5 Free"; font-weight: 900; font-size: 1.25em; content: "\\f574";\n' +
  '  }'

document.documentElement.appendChild(scriptFonts);



ui.styleValStausMessage = `
.button {
    background-color: white;
    border: none;
    color: white;
    padding: 10px 2px;
    text-align: center;
    text-decoration: none;
    font-size: 14px;
    margin-top:-10px;
    margin-right:-0px;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
    cursor: pointer;
    width: 50px;
    float: right;
    border-radius: 3px;
    display: inline-block;
    line-height: 0;
}
.button-close:hover {
    background-color: gray;
    color: white;
}
.button-close {
    background-color: white;
    color: black;
    border: 2px solid gray;
}`


ui.styleValStatusMessage = `background-color: #fffde0; color: black;
      width: 800px; height: 175px; position: fixed;       top: 50px;     right: 0;    left: 0;
      margin: auto;       border: 1px solid lightblue;       box-shadow: 3px 3px 7px #777;
      align-items: center;       justify-content: left;       text-align: left;`


ui.styleParamWindow = `<style>
.davidd-button {
  background-color: white;
  border: 1px;
  color: black;
  padding: 10px 10px;
  text-align: center;
  text-decoration: none;
  font-size: 14px;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
  cursor: pointer;
  width: 75px;      
  border-radius: 3px;
  line-height: 0;
}
.davidd-button-close:hover {
  background-color: gray;
  color: white;
}
.davidd-button-close {
  background-color: white;
  color: black;
  border: 2px solid gray;
}
.davidd-button-run:hover {
  background-color: lightgreen;
}
.davidd-button-run {
  background-color: white;
  border: 2px solid lightgreen;
}
.davidd-button-def:hover {
  background-color: skyblue;
}
.davidd-button-def {
  background-color: white;
  border: 2px solid skyblue;
}
table.stratParamTable {
    width: 100%;
     border-collapse: collapse;
    border: 2px solid grey;
    empty-cells: show;
    table-layout: fixed;
}
.stratParamTable thead {
    caption-side: bottom;
   text-align: center;
   padding: 5px 0;
   font-size: 100%;
}
.stratParamTable td {
   border: 1px solid grey;
    font-size: 90%;
    padding: 2px 2px;
}
</style>`

ui.styleValParamWindow = `background-color: white; color: black;
width: 800px; height: 800px; position: fixed; top: 50px; right: 0; left: 0;
margin: auto; border: 1px solid lightblue; box-shadow: 3px 3px 7px #777;
align-items: center;  justify-content: left; text-align: left;`

ui.styleValWindowShadow = `background-color:rgba(0, 0, 0, 0.2);
position:absolute;
width:100%;
height:100%;
top:0px;
left:0px;
z-index:10000;`


ui.checkInjectedElements = () => {
  if (action && !action.workerStatus) { // If do not running process
    const strategyDefaultEl = document.querySelector(SEL.strategyDefaultElement)
    if (!strategyDefaultEl)
      return
    if (document.querySelector(SEL.strategyImportExport))
      return
    const importExportEl = document.createElement('div')
    importExportEl.id = 'daviddImportExport'
    importExportEl.setAttribute('style', 'padding-left: 10px;padding-right: 10px')
    importExportEl.innerHTML = '<a id="daviddImport" style="cursor: pointer;padding-right: 5px"><i class="davidd_icon davidd_upload"></i></a>' +
                               '<a id="daviddExport" style="cursor: pointer;padding-left: 5px;"><i class="davidd_icon davidd_download"></i></a>'
    strategyDefaultEl.after(importExportEl)
    const exportBtn = document.getElementById('daviddExport')
    const importBtn = document.getElementById('daviddImport')
    if (exportBtn) {
      exportBtn.onclick = async () => {
        await action.saveParameters()
      }
    }
    if (importBtn) {
      importBtn.onclick = async () => {
        await action.loadParameters()
      }
    }

  }
}

ui.alertMessage = (message) => {
  alert(message)
}

ui.statusMessageRemove = () => {
  const statusMessageEl = document.getElementById('daviddStatus')
  if(statusMessageEl)
    statusMessageEl.parentNode.removeChild(statusMessageEl)
}

ui.autoCloseAlert = (msg, duration = 2000) => {
  console.log('autoCloseAlert')
  const altEl = document.createElement("div");
  altEl.setAttribute("style","background-color: #ffeaa7;color:black; width: 450px;height: 300px;position: absolute;top:0;bottom:0;left:0;right:0;margin:auto;border: 1px solid black;font-family:arial;font-size:25px;font-weight:bold;display: flex; align-items: center; justify-content: center; text-align: center;");
  altEl.setAttribute("id","daviddAlert");
  altEl.innerHTML = msg;
  setTimeout(function() {
    altEl.parentNode.removeChild(altEl);
  }, duration);
  document.body.appendChild(altEl);
}

ui.statusMessage = (msgText, extraHeader = null) => {
  const isStatusPresent = document.getElementById('daviddStatus')
  const mObj = isStatusPresent ? document.getElementById('daviddStatus') : document.createElement("div");
  let msgEl
  if(!isStatusPresent) {
    mObj.id = "daviddStatus";
    mObj.setAttribute("style","background-color:rgba(0, 0, 0, 0.2);" +
      "position:absolute;" +
      "width:100%;" +
      "height:100%;" +
      "top:0px;" +
      "left:0px;" +
      "z-index:10000;");
    mObj.style.height = document.documentElement.scrollHeight + "px";
    const msgStyleEl = mObj.appendChild(document.createElement("style"));
    msgStyleEl.innerHTML = ".button {\n" +
      "                background-color: white;\n" +
      "                border: none;\n" +
      "                color: white;\n" +
      "                padding: 10px 2px;\n" +
      "                text-align: center;\n" +
      "                text-decoration: none;\n" +
      "                font-size: 14px;\n" +
      "                margin-top:-10px;\n" +
      "                margin-right:-0px;\n" +
      "                -webkit-transition-duration: 0.4s; /* Safari */\n" +
      "                transition-duration: 0.4s;\n" +
      "                cursor: pointer;\n" +
      "                width: 50px;\n" +
      "                float:right;\n" +
      "                border-radius: 3px;\n" +
      "                display: inline-block;\n" +
      "                line-height: 0;\n" +
      "            }\n" +
      "            .button-close:hover {\n" +
      "                background-color: gray;\n" +
      "                color: white;\n" +
      "            }\n" +
      "            .button-close {\n" +
      "                background-color: white;\n" +
      "                color: black;\n" +
      "                border: 2px solid gray;\n" +
      "            }"
    msgEl = mObj.appendChild(document.createElement("div"));
    msgEl.setAttribute("style","background-color: #fffde0;" +
      "color: black;" +
      "width: 800px;" +
      "height: 175px;" +
      "position: fixed;" +
      "top: 50px;" +
      "right: 0;" +
      "left: 0;" +
      "margin: auto;" +
      "border: 1px solid lightblue;" +
      "box-shadow: 3px 3px 7px #777;" +
      // "display: flex;" +
      "align-items: center; " +
      "justify-content: left; " +
      "text-align: left;");
  } else {
    msgEl = mObj.querySelector('div')
  }
  if(isStatusPresent && msgEl && document.getElementById('daviddMsg') && !extraHeader) {
    document.getElementById('daviddMsg').innerHTML = msgText
  } else {
    extraHeader = extraHeader !== null ? `<div style="font-size: 12px;margin-left: 5px;margin-right: 5px;text-align: left;">${extraHeader}</div>` : '' //;margin-bottom: 10px
    msgEl.innerHTML = '<button class="button button-close" id="daviddBoxClose">Stop</button>' +
      '<div style="color: blue;font-size: 26px;margin: 5px 5px;text-align: center;">Attention!</div>' +
      '<div style="font-size: 18px;margin-left: 5px;margin-right: 5px;text-align: center;">The page elements are controlled by the browser extension. Please do not click on the page elements. You can reload the page to stop it.</div>' +
      extraHeader +
      '<div id="daviddMsg" style="margin: 5px 10px">' +
      msgText + '</div>';
  }
  if(!isStatusPresent) {
    const tvDialog = document.getElementById('overlap-manager-root')
    if(tvDialog)
      document.body.insertBefore(mObj, tvDialog) // For avoid problem if msg overlap tv dialog window
    else
      document.body.appendChild(mObj);
  }
  const btnClose = document.getElementById('daviddBoxClose')
  if(btnClose) {
    btnClose.onclick = () => {
      console.log('Stop clicked')
      action.workerStatus = null
    }
  }
}

ui.showAndUpdateStrategyParameters = async (testParams) => {
  return new Promise(resolve => {
    function selectAll(shouldSelect) {
      let allFiltersRows = document.getElementById('stratParamData').getElementsByTagName('tr')
      if (allFiltersRows) {
        for(let row of allFiltersRows) {
          const cells = row.getElementsByTagName('td')
          if (!cells || cells.length !== 9)
            continue
          const activeEl = cells[0].getElementsByTagName('input')
          if (!activeEl || !activeEl[0])
            continue
          const isChecked = Boolean(activeEl[0].getAttribute('checked'))
          if (isChecked !== shouldSelect) {
            const clickEvent = document.createEvent ('MouseEvents');
            clickEvent.initEvent ('click', true, true);
            activeEl[0].dispatchEvent(clickEvent);
            if (shouldSelect) {
              activeEl[0].setAttribute('checked', 'checked')
            } else {
              activeEl[0].removeAttribute('checked')
            }
          }
        }
      }
    }

    function updateParamsAndSpace() {
      const paramRange = {}
      const condParam = {}
      let allFiltersRows = document.getElementById('stratParamData').getElementsByTagName('tr')
      let space = null
      if (allFiltersRows) {
        for(let row of allFiltersRows) {
          const cells = row.getElementsByTagName('td')
          if (!cells || cells.length !== 9)
            continue
          const activeEl = cells[0].getElementsByTagName('input')
          if (!activeEl || !activeEl[0] || !activeEl[0].checked)
            continue
          const nameEl = cells[1]           // const nameEl = cells[1].getElementsByTagName('input')
          const fromEl = cells[2].getElementsByTagName('input')
          const toEl = cells[3].getElementsByTagName('input')
          const stepEl = cells[4].getElementsByTagName('input')
          const defEl = cells[5].getElementsByTagName('input')
          const priorityEl = cells[6].getElementsByTagName('input')
          const condEl = cells[7].getElementsByTagName('select')
          const condSrcEl = cells[8].getElementsByTagName('select')
          if(!nameEl || !nameEl.innerText || !fromEl || !fromEl.length || !toEl || !toEl.length || !stepEl || !stepEl.length ||
            !condEl || !condEl.length || !condSrcEl || !condSrcEl.length)
            continue
          try {
            const key = nameEl.innerText
            let paramSpace = 1
            const priority = parseInt(priorityEl[0].value)
            if(typeof testParams.paramRangeSrc[key][0] === 'boolean') {
              paramRange[key] = [true, false, 0, defEl[0].value.toLowerCase() === 'true', priority]
              paramSpace = 2
            } else if (typeof testParams.paramRangeSrc[key][0] === 'string' && testParams.paramRangeSrc[key][0].includes(';')) {
              paramRange[key] = [fromEl[0].value, '', 0, fromEl[0].value.split(';')[0], priority]
              paramSpace = fromEl[0].value.split(';').length
            } else {
              let isInteger = testParams.paramRangeSrc[key][0] === Math.round(testParams.paramRangeSrc[key][0]) &&
                testParams.paramRangeSrc[key][1] === Math.round(testParams.paramRangeSrc[key][1]) &&
                testParams.paramRangeSrc[key][2] === Math.round(testParams.paramRangeSrc[key][2])
              if (Number(fromEl[0].value) !== NaN) { // Not NaN
                if (parseInt(fromEl[0].value) !== Number(fromEl[0].value) ||
                  (Number(toEl[0].value) && parseInt(toEl[0].value) !== Number(toEl[0].value)) ||
                  (Number(stepEl[0].value) && parseInt(stepEl[0].value) !== Number(stepEl[0].value)))
                  isInteger = false
                paramRange[key] = [isInteger ? parseInt(fromEl[0].value) : Number(fromEl[0].value),
                  Number(toEl[0].value)]
                let step = isInteger ? parseInt(stepEl[0].value) : Number(stepEl[0].value)
                step = step !== 0 ? step : paramRange[key][1] < paramRange[key][0] ? -1 : 1
                paramRange[key].push(step)
                paramRange[key].push(isInteger ? parseInt(defEl[0].value) : Number(defEl[0].value))
                paramRange[key].push(priority)
                paramSpace = Math.floor(Math.abs(paramRange[key][0] - paramRange[key][1]) / paramRange[key][2]) + 1
              } else {
                paramRange[key] = [fromEl[0].value, '', 0, fromEl[0].value, priority]
                paramSpace = 1
              }
            }
            const cond = condEl[0].value
            const condSrc = condSrcEl[0].value
            if (cond && condSrc) {
              condParam[key] = {condition: cond, source: condSrc}
            }
            space = space == null ? paramSpace : space * paramSpace
          } catch (err) {console.error(err)}
        }
      }
      document.getElementById('cyclesAll').innerHTML=String(space !== null ? space : 0)

      if (space !== null) {
        document.getElementById('stratParamCycles').value = parseInt(space)
      }
      // TODO update cells?
      return [paramRange, condParam]
    }


    function prepareRow(name, param, status, condition, condSrc, names) {
      const isBoolean = typeof param[0] === 'boolean'
      let optString = ''
      const condSign = ['', '>=', '<=', '>', '<', '=']
      condSign.forEach(cond => optString += `<option value="${cond}" ${cond === condition ? 'selected="selected"' : ''}>${cond}</option>`)
      let namesString = `<option value="" ${!condSrc ? 'selected="selected"' : ''}></option>`
      names.forEach(paramName => {
        if (paramName !== name)
          namesString += `<option value="${paramName}" ${condSrc === paramName ? 'selected="selected"' : ''}>${paramName}</option>`
      })
      return `<td><input type="checkbox" ${status? 'checked=true' : ''} style="width:1em; color-scheme: white;"></td><td>${name}</td>
              <td><input type="text" value="${isBoolean ? 'true' : param[0]}" style="width:4em; background: white;" ${isBoolean ? 'disabled' :''}></td>
              <td><input type="text" value="${isBoolean ? 'false' : param[1]}" style="width:4em; background: white;" ${isBoolean ? 'disabled' :''}></td>
              <td><input type="number"  step="any" value="${param[2]}" style="width:4em; background: white;" ${isBoolean ? 'disabled' :''}></td>
              <td><input type="text" value="${param[3]}" style="width:4em; background: white;" ></td>
              <td><input type="number" value="${param[4]}" style="width:4em; background: white;"></td>
              <td><select style="width:4em; background: white;">${optString}</select></td>
              <td><select style="width:4em; background: white;">${namesString}</select></td>`
    }

    function removeParamWindow() {
      const stratParamWindowEl = document.getElementById('daviddStratParam')
      if (stratParamWindowEl)
        stratParamWindowEl.parentNode.removeChild(stratParamWindowEl)
    }

    function getCycles() {
      try {
        const cyclesEl = document.getElementById('stratParamCycles')
        return cyclesEl && cyclesEl.value ? parseInt(cyclesEl.value) : 100
      } catch {}
      return 100
    }
    try{
      const isStratParamElPresent = document.getElementById('daviddStratParam')
      let stratParamEl = isStratParamElPresent ? document.getElementById('daviddStratParam') : document.createElement('div')
      let popupEl
      if (!isStratParamElPresent) {
        stratParamEl.id = 'daviddStratParam'
        stratParamEl.setAttribute('style', ui.styleValWindowShadow)
        stratParamEl.style.height = document.documentElement.scrollHeight + 'px'
        const stratParamStyleEl = stratParamEl.appendChild(document.createElement('style'))
        stratParamStyleEl.innerHTML = ui.styleParamWindow
        popupEl = stratParamEl.appendChild(document.createElement('div'))
        popupEl.setAttribute('style', ui.styleValParamWindow )
      } else {
        popupEl = stratParamEl.querySelector('div')
      }
      popupEl.innerHTML = `<div style="height: 150px; overflow-y: hidden; vertical-align:top;">
  <h1 style="padding: 25px">Strategy parameters</h1>
  <div style="align-content: center"><span style="padding:5px 15px">
  Cycles <input id="stratParamCycles" type="number" value="10" style="width:8em; background: white"> from ~<span id="cyclesAll">100</span></span>
  <button id="stratParamSaveRun" class="davidd-button davidd-button-run">Save&Run</button>
  <button id="stratParamDefRun" class="davidd-button davidd-button-def">Skip&Run</button>
  <button id="stratParamCancel" class="davidd-button davidd-button-close">Cancel</button>
  </div>
  </div>
  <div style="height: 640px; overflow-y: auto; vertical-align:top;">
  <table class="stratParamTable">
   <thead><td style="width: 10%"><input type="checkbox" id="stratSelectAll" style="margin-right: 2px; color-scheme: white;"> Active</td><td style="width: 35%">Parameter</td><td>From</td><td>To</td><td>Step</td><td>Default</td><td>Priority</td><td>Condition</td><td>Source</td></thead>
   <tbody id="stratParamData"></tbody>
  </table></div>`

      if (!isStratParamElPresent) {
        const tvDialog = document.getElementById('overlap-manager-root')
        if (tvDialog)
          document.body.insertBefore(stratParamEl, tvDialog) // For avoid problem if msg overlap tv dialog window
        else
          document.body.appendChild(stratParamEl)
      }
      const tbody = document.getElementById('stratParamData')

      let paramRows = ''
      const processedParams = []
      let allParamNames = Object.keys(testParams.paramRange).concat(Object.keys(testParams.paramRangeSrc))
      allParamNames = allParamNames.filter((name, idx) => allParamNames.indexOf(name) === idx)
      for(let name in testParams.paramRange) {
        if (testParams.paramRange[name][0] === null || (isNaN(testParams.paramRange[name][0]) &&
          (typeof testParams.paramRange[name][0] !== 'string' || !testParams.paramRange[name][0].includes(';'))))
          continue
        const condVal = testParams.paramConditions.hasOwnProperty(name) ? testParams.paramConditions[name].condition : ''
        const condSource = testParams.paramConditions.hasOwnProperty(name) ? testParams.paramConditions[name].source : ''
        paramRows += `\n<tr>${prepareRow(name, testParams.paramRange[name], true, condVal, condSource, allParamNames)}</tr>`
        processedParams.push(name)
      }
      for(let name in testParams.paramRangeSrc) {
        if(!processedParams.includes(name) && testParams.paramRangeSrc[name][0] !== null && (
          !isNaN(testParams.paramRangeSrc[name][0]) ||
          (typeof testParams.paramRangeSrc[name][0] === 'string') && testParams.paramRangeSrc[name][0].includes(';'))) {
          const condVal = testParams.paramConditions.hasOwnProperty(name) ? testParams.paramConditions[name].condition : ''
          const condSource = testParams.paramConditions.hasOwnProperty(name) ? testParams.paramConditions[name].source : ''
          paramRows += `\n<tr>${prepareRow(name, testParams.paramRangeSrc[name], false, condVal, condSource, allParamNames)}</tr>`
        }
      }
      if (paramRows)
        tbody.innerHTML = paramRows // tbody.appendChild(paramRows)

      updateParamsAndSpace()
      tbody.addEventListener('change', updateParamsAndSpace)

      const btnSelect = document.getElementById('stratSelectAll')
      if (btnSelect) {
        btnSelect.onclick = () => {
          const isChecked = Boolean(btnSelect.getAttribute('checked'))
          if (isChecked) {
            btnSelect.removeAttribute('checked')
            selectAll(false)
          } else {
            btnSelect.setAttribute('checked', 'checked')
            selectAll(true)
          }
        }
      }
      const btnClose = document.getElementById('stratParamCancel')
      if (btnClose) {
        btnClose.onclick = () => {
          console.log('Cancel')
          removeParamWindow()
          return resolve(null)
        }
      }
      const btnSaveRun = document.getElementById('stratParamSaveRun')
      if (btnSaveRun) {
        btnSaveRun.onclick = () => {
          const [paramRange, paramCond] = updateParamsAndSpace()
          const cycles = getCycles()
          removeParamWindow()
          return resolve({cycles: cycles, paramRange: paramRange, paramConditions: paramCond})
        }
      }
      const btnDefRun = document.getElementById('stratParamDefRun')
      if (btnDefRun) {
        btnDefRun.onclick = () => {
          console.log('Run default')
          const cycles = getCycles()
          removeParamWindow()
          return resolve({cycles: cycles, paramRange: null })
        }
      }
    } catch (err) {
      removeParamWindow()
      throw err
    }
  })
}
