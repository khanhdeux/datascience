/*
 @license Copyright 2021 akumidv (https://github.com/akumidv/)
 SPDX-License-Identifier: Apache-2.0
*/

'use strict';

(async function() {

  setInterval(ui.checkInjectedElements, 1000); // Add action to set strategy parameters window

  chrome.runtime.onMessage.addListener(
    async function(request, sender, sendResponse) {
      if(sender.tab || !request.hasOwnProperty('action') || !request.action) {
        console.log('Not for action message received:', request)
        return sendResponse()
      }
      if(action.workerStatus !== null) {
        console.log('Waiting for end previous work. Status:', action.workerStatus)
        return sendResponse()
      }

      action.workerStatus = request.action
      try {
        sendResponse() // Close popup window
        console.log('Request for:', request.action)
        switch (request.action) {
          // case 'uploadToDiscord':
          //   await action.uploadToDiscord()
          //   break
          case 'uploadStrategyTestParameters':
            await action.uploadStrategyTestParameters()
            break
          case 'getStrategyTemplate':
            await action.getStrategyTemplate()
            break
          case 'testStrategy':
            await action.testStrategy(request)
            break
          case 'downloadStrategyTestResults':
            await action.downloadStrategyTestResults()
            break
          case 'wlCheckStrategy':
            await action.checkStrategyForWatchList(request)
            break
          case 'wlTestStrategy':
            await action.testStrategyForWatchList(request)
            break
          case 'wlCheckStrategyOnTimeframe':
            await action.checkStrategyOnTimeframe(request)
            break
          case 'wlDownloadStrategyTestResults': {
            const bestBacktestResultsForList = await storage.getKey('bestBacktestResultsForList')
            if (bestBacktestResultsForList)
              await action.downloadBestListBacktestResults(bestBacktestResultsForList['data'], bestBacktestResultsForList['name'])
            else
              ui.alertMessage('Do not have stored backtest results. Try backtest again')
            break
          }
          case 'wlExportParameters': {
            const bestBacktestResultsForList = await storage.getKey('bestBacktestResultsForList')
            if(bestBacktestResultsForList)
              await action.downloadTickerParametersForBacktestList(bestBacktestResultsForList['data'],
                bestBacktestResultsForList['name'], bestBacktestResultsForList['strategyName'], request.ticker)
            else
              ui.alertMessage('Do not have stored backtest results. Try backtest again')
            break
          }
          case 'show3DChart':
            await action.show3DChart()
            break
          case 'clearAll':
            await action.clearAll()
            break
          case 'showAlert':
            const { body } = request
            if (body) {
              alert(body)
            }
            break
          default:
            console.log('None of realisation for signal:', request)
        }
      } catch (err) {
        console.log(err)
        ui.alertMessage(`An error has occurred.\n\nReload the page and try again.\n\nError message: ${err.message}`)
      }
      action.workerStatus = null
      ui.statusMessageRemove()
    }
  );


  const dialogWindowNode = await page.waitForSelector(SEL.tvDialogRoot, 0)
  if(dialogWindowNode) {
    const tvObserver = new MutationObserver(tvUi.dialogHandler);
    tvObserver.observe(dialogWindowNode, {
      childList: true,
      subtree: true,
      attributes: false,
      characterData: false
    });
    await tvUi.dialogHandler() // First run
  }

})();
