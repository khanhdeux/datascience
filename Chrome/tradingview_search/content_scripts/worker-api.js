const worker = {}

worker.uploadToDiscord = async (discord_data) => {
  return new Promise((resolve) => {
    chrome.runtime.sendMessage({ action: 'uploadToDiscord', data: discord_data}, async function (apiResponse) {
      return resolve(apiResponse)
    });
  });
}