const page = {}

page.waitForTimeout = async (timeout = 2500) => new Promise(resolve => setTimeout(resolve, timeout))

page.waitForSelector = async function (selector, timeout = 5000, isHide = false, parentEl) {
  parentEl = parentEl ? parentEl : document
  return new Promise(async (resolve) => {
    let iter = 0
    let elem
    const tikTime = timeout === 0 ? 1000 : 50
    do {
      await page.waitForTimeout(tikTime)
      elem = parentEl.querySelector(selector)
      iter += 1
    } while ((timeout === 0 ? true : (tikTime * iter) < timeout) && (isHide ? !!elem : !elem))
    resolve(elem)
  });
}

/**
 * @param {Array<string>} selectors
 * @returns {Promise<HTMLElement>}
 */
page.waitForSelectors = async (selectors) => {
  for (const selector of selectors) {
    const element = await page.waitForSelector(selector);
    if (element) return element;
  }
  return null;
};

/**
 * @param {Array<string>} selectors
 * @returns {HTMLElement}
 */
page.querySelectors = (selectors) => {
  for (const selector of selectors) {
    const element = document.querySelector(selector);
    if (element) return element;
  }
  return null;
};

const reactValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, 'value').set;
const inputEvent = new Event('input', { bubbles: true});
const changeEvent = new Event('change', { bubbles: true});


page._mouseEvents ={};
["mouseover", "mousedown", "mouseup", "click",
  "dblclick", "contextmenu"].forEach(eventType => {
  page._mouseEvents[eventType] = document.createEvent('MouseEvents')
  page._mouseEvents[eventType].initEvent (eventType, true, true)
})

// https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/initMouseEvent
// function mouseTrigger (el, eventType) {
//   const clickEvent = document.createEvent ('MouseEvents');
//   clickEvent.initEvent (eventType, true, true);
//   el.dispatchEvent(clickEvent);
// }

page.mouseClickEl = function (el) {
  if (!el) {
    throw new Error(`The element is missing`);
  }
  // mouseTrigger (el, "mouseover");
  // mouseTrigger (el, "mousedown");
  // mouseTrigger (el, "mouseup");
  // mouseTrigger (el, "click");
  ["mouseover", "mousedown", "mouseup", "click"].forEach((eventType) =>
    el.dispatchEvent(page._mouseEvents[eventType])
  )
}

page.setElValue = function  (element, value, isChange = false) {
  reactValueSetter.call(element, value)
  element.dispatchEvent(inputEvent);
  if(isChange) element.dispatchEvent(changeEvent);
}

page.setSelByText = (selector, textValue) => {
  let isSet = false
  const selectorAllVal = document.querySelectorAll(selector)
  if (!selectorAllVal || !selectorAllVal.length)
    return isSet
  for (let optionsEl of selectorAllVal) {
    if(optionsEl) {//&& options.innerText.startsWith(textValue)) {
      const itemValue = page.getElText(optionsEl).toLowerCase()
      if(itemValue.startsWith(textValue.toLowerCase())) {
        page.mouseClickEl(optionsEl)
        isSet = true
        break
      }
    }
  }
  return isSet
}


page.getElText = (element) => {
  if(!element)
    throw new Error(`The element is missing`)
  return element.innerText.replaceAll('​', '')
}

page.getSelText = (selector, parentEl) => {
  parentEl = parentEl ? parentEl : document
  const element = parentEl.querySelector(selector)
  if (!element)
    throw new Error(`The element ${selector} is missing`)
  return element.innerText && page.getElText(element) ? page.getElText(element) : null
}


page.waitForText = async(selector, textValue, timeout = 5000, isHide = false) => {
  return new Promise(async (resolve) => {
    let iter = 0
    let element
    let text = ''
    const tikTime = 50
    do {
      await page.waitForTimeout(tikTime)
      element = document.querySelector(selector)
      if (element) {
        text = page.getElText(element)
      }
      iter += 1
      if(tikTime * iter >= timeout)
        break
      // throw new Error(`The element ${selector} is missing`) //break
    } while ((!isHide && (!element || text !== textValue)) || (isHide && (!!element || text === textValue)))
    // } while (tikTime * iter < timeout && isHide ? (!!elem || text === textValue) : (!elem || text !== textValue))
    let res
    if (isHide) {
      res = !element ? null : text !== textValue ? null : element
    } else {
      res = !element ? null : text === textValue ? element : null
    }
    resolve(res)
  });
}

page.clickSelAndWaitingForHide = async(selector) => {
  const element = document.querySelector(selector)
  if (!element)
    throw new Error(`The element ${selector} is missing`)
  element.click()
  return await page.waitForSelector(selector, 5000, true)
}

page.clickSel = (selector) => {
  const element = document.querySelector(selector)
  if (!element)
    throw new Error(`The element ${selector} is missing`)
  element.click()
}