tvChart = {}

tvChart.setTicker = async(tickerName, tickerFullName) => { // TODO convert to throw
  let tickerEl = document.querySelector(SEL.chartTicker)
  if (!tickerEl)
    throw new Error(`Can't get TV ticker element on chart`)

  let curTickerName = tickerEl.innerText
  if (curTickerName === tickerName)
    return

  page.mouseClickEl(tickerEl)
  const searchEl = await page.waitForSelector(SEL.chartSearch)
  if (!searchEl)
    throw new Error(`Can't get search dialog window`)
  if(!document.querySelector(SEL.chartSearchAllActive)) {
    document.querySelector(SEL.chartSearchAll).click()
    const isActive = await page.waitForSelector(SEL.chartSearchAllActive, 2500)
    if(!isActive)
      throw new Error(`Can't set type "All" on search dialog window`)
  }
  page.setElValue(searchEl, tickerFullName)
  await page.waitForTimeout(500) // Time for get data from tradingview server TODO check for stability. replace to get item ticker than wait for changes
  const searchItemEl = await page.waitForSelector(SEL.chartSearchItem)
  if (!searchItemEl) {
    await page.clickSelAndWaitingForHide(SEL.dialogCloseBtn)
    throw new Error(`The ticker "${tickerFullName}" was not found`)
  }
  const isExpand = searchItemEl.querySelector(SEL.chartSearchExpand)
  if (isExpand) { // TODO need to implement. Just click expand and click on second element
    await page.clickSelAndWaitingForHide(SEL.dialogCloseBtn)
    throw new Error(`The ticker ${tickerFullName} have additional data. Work with such tickers has not yet been implemented`)
  }
  searchItemEl.click() //page.mouseClickEl(searchItemEl)

  await page.waitForSelector(SEL.chartSearchItem, 5000, true)
  tickerEl = await page.waitForText(SEL.chartTicker, tickerName, 5000, false)
  if (!tickerEl) {
    const curTickerText = page.getSelText(SEL.chartTicker)
    if (!curTickerText.includes(tickerName))
      throw new Error(`Can't get ticker "${tickerFullName}" on chart after search. Current ticker value on chart "${curTickerText}"`)
  }
}


async function selectTimeFrameMenuItem(timeframe) {
  const allMenuTFItems = document.querySelectorAll(SEL.chartTimeframeMenuItem)
  for(let item of allMenuTFItems) {
    const tfVal = item.getAttribute('data-value')
    let tfNormValue = tfVal
    const isMinutes = isTFDataMinutes(tfVal)
    tfNormValue = isMinutes && parseInt(tfVal) % 60 === 0 ? `${parseInt(tfVal) / 60}h` : isMinutes ? `${tfVal}m` : tfNormValue // If hours
    if (tfVal[tfVal.length-1] === 'S')
      tfNormValue = `${tfVal.substr(0,tfVal.length - 1)}s`
    if(tfNormValue === timeframe) {
      page.mouseClickEl(item)
      await page.waitForSelector(SEL.chartTimeframeMenuItem, 1500, true)
      return tfNormValue
    }
  }
  return null
}

function isTFDataMinutes (tf) {return !['S', 'D', 'M', 'W', 'R'].includes(tf[tf.length - 1])}
function correctTF (tf) {return ['D', 'M', 'W'].includes(tf.toUpperCase()) ? `1${tf.toUpperCase()}` :
                                ['D', 'W'].includes(tf[tf.length-1].toUpperCase()) ? tf.substr(0, tf.length -1) + tf[tf.length-1].toUpperCase(): tf}

tvChart.changeTimeFrame = async (timeframe) => {
  if(!timeframe)
    throw new Error ('There is no timeframe value')

  const strategyTF = correctTF(timeframe)

  let curTimeFrameText = await getCurrentTimeFrame()

  if(strategyTF === curTimeFrameText) //`Timeframe already set to ${alertTF}`
    return
  // Search timeframe among favorite timeframes
  const isFavoriteTimeframe = await document.querySelector(SEL.chartTimeframeFavorite)
  if(isFavoriteTimeframe) {
    const allTimeFrameEl = document.querySelectorAll(SEL.chartTimeframeFavorite, 1000)
    for(let tfEl of allTimeFrameEl) {
      const tfVal = !tfEl || !tfEl.innerText ? '' : correctTF(tfEl.innerText)
      if(tfVal === strategyTF) {
        tfEl.click() //`Timeframe changed to
        return
      }
    }
  }

  // Search timeframe among timeframes menu items
  const timeFrameMenuEl = await document.querySelector(SEL.chartTimeframeMenuOrSingle)
  if(!timeFrameMenuEl)
    throw new Error('There is no timeframe selection menu element on the page')
  page.mouseClickEl(timeFrameMenuEl)
  const menuTFItem = await page.waitForSelector(SEL.chartTimeframeMenuItem, 1500)
  if(!menuTFItem)
    throw new Error('There is no items in timeframe menu on the page')

  let foundTF = await selectTimeFrameMenuItem(strategyTF)
  if(foundTF) {
    curTimeFrameText = await getCurrentTimeFrame()
    if(strategyTF !== curTimeFrameText)
      throw new Error(`Failed to set the timeframe value to "${strategyTF}", the current "${curTimeFrameText}"`)
    return //`Timeframe changed to ${alertTF}`
  }

  const tfValueEl = document.querySelector(SEL.chartTimeframeMenuInput)
  if(!tfValueEl)
    throw new Error(`There is no input element to set value of timeframe`)
  tfValueEl.scrollIntoView()
  await page.waitForTimeout(50)
  page.setElValue(tfValueEl, strategyTF.substr(0, strategyTF.length - 1))

  page.clickSel(SEL.chartTimeframeMenuType)
  const isTFTypeEl = page.waitForSelector(SEL.chartTimeframeMenuTypeItems, 1500)
  if(!isTFTypeEl)
    throw new Error(`The elements of the timeframe type did not appear while adding it`)
  switch (strategyTF[strategyTF.length-1]) {
    case 'm':
      page.clickSel(SEL.chartTimeframeMenuTypeItemsMin)
      break;
    case 'h':
      page.clickSel(SEL.chartTimeframeMenuTypeItemsHours)
      break;
    case 'D':
      page.clickSel(SEL.chartTimeframeMenuTypeItemsDays)
      break;
    case 'W':
      page.clickSel(SEL.chartTimeframeMenuTypeItemsWeeks)
      break;
    case 'M':
      page.clickSel(SEL.chartTimeframeMenuTypeItemsMonth)
      break;
    case 'r':
      page.clickSel(SEL.chartTimeframeMenuTypeItemsRange)
      break;
    default:
      return {error: 7, message: `Unknown timeframe type in "${strategyTF}"`}
  }
  page.clickSel(SEL.chartTimeframeMenuAdd)
  await page.waitForTimeout(1000)
  foundTF = await selectTimeFrameMenuItem(strategyTF)
  curTimeFrameText = await getCurrentTimeFrame()
  if (!foundTF)
    throw new Error( `Failed to add a timeframe "${strategyTF}" to the list`)
  else if(strategyTF !== curTimeFrameText)
    throw new Error(`Failed to set the timeframe value to "${strategyTF}" after adding it to timeframe list, the current "${curTimeFrameText}"`)
}



async function getCurrentTimeFrame() {
  const isFavoriteTimeframe = await document.querySelector(SEL.chartTimeframeFavorite)
  const curTimeFrameEl = isFavoriteTimeframe ? await page.waitForSelector(SEL.chartTimeframeActive, 500) :
    await page.waitForSelector(SEL.chartTimeframeMenuOrSingle, 500)

  if(!curTimeFrameEl || !curTimeFrameEl.innerText)
    return null

  let curTimeFrameText = curTimeFrameEl.innerText
  curTimeFrameText = correctTF(curTimeFrameText)
  return curTimeFrameText
}


async function openWatchList() {
  let wlBtn = await page.waitForSelector(SEL.wlButton)
  let alertBtn = await page.waitForSelector(SEL.alertBtn)
  if (!document.querySelector(SEL.widgetWatchList)) {
    alertBtn.click()
    wlBtn.click()
  }
}


tvChart.watchListItemsProcess = async (processFunc, masterData) => {
  await openWatchList()
  if (document.querySelector(SEL.wlListEmpty))
    throw new Error('Empty watch list')

  const isWlItems = await page.waitForSelector(SEL.wlWrapItem, 5000)
  let  wlListEl = document.querySelector(SEL.wlList)
  if (!isWlItems || !wlListEl)
    throw new Error('Error get watch list elements')
  await scrollList(processFunc, masterData, SEL.wlList, SEL.wlWrapItem)
}

function getTVListHeight(tvListEl) {
  return tvListEl.style && tvListEl.style.hasOwnProperty('height') ? parseInt(tvListEl.style.height) : 0
}


async function scrollList(processFunc, masterData, listSelector, listItemSelector) {
  let listEl = document.querySelector(listSelector)
  const listItemElHeight = document.querySelector(listItemSelector).scrollHeight
  let listElHeight = listEl.scrollHeight

  let scrollWindowHeight = getTVListHeight(listEl)
  if(!listElHeight || scrollWindowHeight < listItemElHeight)
    throw new Error(!listElHeight ? `Error get  list height: ${listElHeight}` : `Very small list height: ${scrollWindowHeight}`)

  listEl.scrollBy(0, -listElHeight * 2) // Return scroll to 0.
  let scrollTo = 0
  let isNext = true
  while (isNext && action.workerStatus) {
    try {
      scrollTo = await scrollListAndProcessItem(processFunc, masterData, listSelector, listItemSelector, scrollTo)
      if(scrollTo === null)
        isNext = false
    } catch (err) {
      console.error(err)
    }
  }
  listEl = document.querySelector(listSelector)
  listEl.scrollBy(0, -listElHeight * 2)
}


async function scrollListAndProcessItem(processFunc, masterData, listSelector, listItemSelector, scrollTo) {
  let isNext = false
  let listEl = document.querySelector(listSelector)
  if(!listEl) // If something wrong with list
    return null
  listEl.scrollBy(0, scrollTo)
  await page.waitForTimeout(150) // Waiting for reload tickers
  let scrollWindowHeight = getTVListHeight(listEl)
  let listHeight = listEl.scrollHeight

  let curListTop = listEl.scrollTop
  const listItemAllEl = document.querySelectorAll(listItemSelector)
  if(!listItemAllEl || listItemAllEl.length === 0)
    return null
  const listItemElHeight = document.querySelector(listItemSelector).scrollHeight
  isNext = (curListTop + scrollWindowHeight) < (listHeight - listItemElHeight)
  let isFound = false
  for (let i = 0; i < listItemAllEl.length; i++) {
    if(!action.workerStatus)
      break
    const listItemEl = listItemAllEl[i]
    const tickerTop = listItemEl.style && listItemEl.style.hasOwnProperty('top') ? parseInt(listItemEl.style.top) : null
    if((tickerTop !== null && tickerTop >= curListTop && tickerTop < (curListTop + scrollWindowHeight))) {
      isFound = true
      try {
        await processFunc(listItemEl, masterData)
      } catch (err) {
        console.error(err)
      }
    } else if (tickerTop !== null && tickerTop >= (curListTop + scrollWindowHeight)) {
      isNext = true
      break
    } else if (tickerTop === null) {
    } // else {}
  }
  if(!isFound) {
    scrollTo = -scrollWindowHeight
    alert('Please do not minimize the browser window while the backtesting is processed')
  } else {
    scrollTo = scrollWindowHeight
  }
  if (!isNext)
    return null
  return scrollTo
}