const action = {
  workerStatus: null
}

action.saveParameters = async () => {
  const strategyData = await tvUi.getStrategy(null, true)
  if(!strategyData || !strategyData.hasOwnProperty('name') || !strategyData.hasOwnProperty('properties') || !strategyData.properties) {
    ui.alertMessage('Please open the indicator (strategy) parameters window before saving them to a file.')
    return
  }
  let strategyParamsCSV = `Name,Value\n"__indicatorName",${JSON.stringify(strategyData.name)}\n`
  Object.keys(strategyData.properties).forEach(key => {
    strategyParamsCSV += `${JSON.stringify(key)},${typeof strategyData.properties[key][0] === 'string' ? JSON.stringify(strategyData.properties[key]) : strategyData.properties[key]}\n`
  })
  file.saveAs(strategyParamsCSV, `${strategyData.name}.csv`)
}

action.loadParameters = async () => {
  await file.upload(file.uploadHandler, '', false)
}

action.uploadToDiscord = async () => {
  const testResults = await storage.getKey(storage.STRATEGY_KEY_RESULTS)
  if(!testResults || (!testResults.perfomanceSummary && !testResults.perfomanceSummary.length)) {
    ui.alertMessage('There is no data for conversion. Try to do test again')
    return
  }
  testResults.optParamName = testResults.optParamName || backtest.DEF_MAX_PARAM_NAME
  const CSVResults = file.convertResultsToCSV(testResults)
  const bestResult = testResults.perfomanceSummary ? model.getBestResult(testResults) : {}
  const propVal = {}
  testResults.paramsNames.forEach(paramName => {
    // if(bestResult.hasOwnProperty(`__${paramName}`))
    //   propVal[paramName] = bestResult[`__${paramName}`]
    propVal[paramName] = testResults.bestPropVal[paramName]
  })
  let strategyParamsCSV = `Name,Value\n"__indicatorName",${JSON.stringify(testResults.shortName)}\n`
  Object.keys(propVal).forEach(key => {
    const value = propVal[key]
    strategyParamsCSV += `${JSON.stringify(key)},${typeof value === 'string' ? JSON.stringify(value) : value}\n`
  })

  const discordData = {action: 'uploadBackTest'}
  discordData['strategyName'] = `${testResults.shortName} ${testResults.ticker} ${testResults.timeFrame}`
  // const filterText = testResults.filterAscending === null ? 'Filter off.\n' : `Filter ${testResults.filterAscending ? 'more than ' +
  //   testResults.filterValue : 'less than ' + testResults.filterValue} by "${testResults.filterParamName}" \n`
  discordData['backtestingResult'] = `**"${testResults.optParamName}"** (${testResults.isMaximizing ? 'max' : 'min'}): **${bestResult[testResults.optParamName]}**\n` +
    `**"Sharpe Ratio"**: **${bestResult['Sharpe Ratio']}**\n` +
    `**"Net Profit: All"**: **${bestResult['Net Profit: All']}**\n` +
    `**"Percent Profitable: All"**: **${bestResult['Percent Profitable: All']}**\n` +
    `**"Total Closed Trades: All"**: **${bestResult['Total Closed Trades: All']}**\n` +
    `**"Max Drawdown"**: **${bestResult['Max Drawdown']}**\n` +
    `**"Sortino Ratio"**: **${bestResult['Sortino Ratio']}**\n` +
    `Ticker: **${testResults.ticker}**, timeframe: ${testResults.timeFrame}.\n` +
    `Method "${testResults.method}", cycles ${testResults.cycles}`
  discordData['strategyParamsCSV'] = strategyParamsCSV
  discordData['backtestingResultCSV'] = CSVResults

  const apiResponse = await worker.uploadToDiscord(discordData)
  console.dir(apiResponse)
  if (!apiResponse || apiResponse.error !== 0) {
    //ui.alertMessage(`Error to send data to discord: ${apiResponse.hasOwnProperty('message') ? apiResponse.message : apiResponse}`)
  } else {
    //ui.alertMessage(`The data was send successfully`)
  }
}

action.uploadImageToDiscord = async (testResults) => {
  let strategyOverviewEl = await page.waitForSelector(SEL.strategyOverview, 1000)
  if(!strategyOverviewEl) {
    //ui.alertMessage('There is not strategy performance summary tab on the page. Open correct page please')
    return null
  }
  strategyOverviewEl.click()
  await page.waitForSelector(SEL.strategyOverviewActive, 1000)
  await page.waitForSelector(SEL.strategyReportGraph, 2000);
  const graphPanel = document.querySelector(SEL.strategyReportPanel);
  html2canvas(graphPanel).then((function(e) {
    const image = e.toDataURL("image/png"),
      title = `${testResults.ticker}:${testResults.timeFrame} ${testResults.shortName} - ${testResults.cycles}_${testResults.isMaximizing ? "max" : "min"}_${testResults.optParamName}_${testResults.method}.csv`,
      request = new XMLHttpRequest;
    request.open("POST", "https://SendToDiscord.daviddtech.repl.co/backtest/api/v1.0/results", !0);
    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    request.send("name=" + encodeURIComponent(title) + "&screenshot=" + image);
  }));
}

action.uploadSignals = async () => {
  await file.upload(signal.parseTSSignalsAndGetMsg, `Please check if the ticker and timeframe are set like in the downloaded data and click on the parameters of the "iondvSignals" script to automatically enter new data on the chart.`, true)
}

action.uploadStrategyTestParameters = async () => {
  await file.upload(model.parseStrategyParamsAndGetMsg, '', false)
}

action.getStrategyTemplate = async () => {
  const strategyData = await tvUi.getStrategy()
  if(!strategyData || !strategyData.hasOwnProperty('name') || !strategyData.hasOwnProperty('properties') || !strategyData.properties) {
    ui.alertMessage('It was not possible to find a strategy with parameters among the indicators. Add it to the chart and try again.')
  } else {
    const paramRange = model.getStrategyRange(strategyData)
    console.log(paramRange)
    // await storage.setKeys(storage.STRATEGY_KEY_PARAM, paramRange)
    const strategyRangeParamsCSV = model.convertStrategyRangeToTemplate(paramRange)
    ui.alertMessage('The range of parameters is saved for the current strategy.\n\nYou can start optimizing the strategy parameters by clicking on the "Test strategy" button')
    file.saveAs(strategyRangeParamsCSV, `${strategyData.name}.csv`)
  }
}

action.clearAll = async () => {
  const clearRes = await storage.clearAll()
  ui.alertMessage(clearRes && clearRes.length ? `The data was deleted: \n${clearRes.map(item => '- ' + item).join('\n')}` : 'There was no data in the storage')
}

action.show3DChart= async () => {
  const testResults = await storage.getKey(storage.STRATEGY_KEY_RESULTS)
  if(!testResults || (!testResults.perfomanceSummary && !testResults.perfomanceSummary.length)) {
    ui.alertMessage('There is no results data for to show. Try to backtest again')
    return
  }
  testResults.optParamName = testResults.optParamName || backtest.DEF_MAX_PARAM_NAME
  const eventData = await send3dChartMessage(testResults)
  if (eventData.hasOwnProperty('message'))
    ui.alertMessage(eventData.message)
}

async function send3dChartMessage (testResults) {
  return new Promise(resolve => {
    const url =  window.location && window.location.origin ? window.location.origin : 'https://www.tradingview.com'
    tvPageMessageData['show3DChart'] = resolve
    window.postMessage({name: 'DaviddScript', action: 'show3DChart', data: testResults}, url) // TODO wait for data
  })
}


action.downloadStrategyTestResults = async () => {
  const testResults = await storage.getKey(storage.STRATEGY_KEY_RESULTS)
  if(!testResults || (!testResults.perfomanceSummary && !testResults.perfomanceSummary.length)) {
    ui.alertMessage('There is no results data for conversion. Try to do test again')
    return
  }
  testResults.optParamName = testResults.optParamName || backtest.DEF_MAX_PARAM_NAME
  console.log('testResults', testResults)
  const CSVResults = file.convertResultsToCSV(testResults)
  const bestResult = testResults.perfomanceSummary ? model.getBestResult(testResults) : {}
  const propVal = {}
  testResults.paramsNames.forEach(paramName => {
    if(bestResult.hasOwnProperty(`__${paramName}`))
      propVal[paramName] = bestResult[`__${paramName}`]
  })
  await tvUi.setStrategyParams(testResults.shortName, propVal)
  if(bestResult && bestResult.hasOwnProperty(testResults.optParamName))
    ui.alertMessage(`The best found parameters are set for the strategy\n\nThe best ${testResults.isMaximizing ? '(max) ':'(min)'} ${testResults.optParamName}: ` + bestResult[testResults.optParamName])
  file.saveAs(CSVResults, `${testResults.ticker}:${testResults.timeFrame} ${testResults.shortName} - ${testResults.cycles}_${testResults.isMaximizing ? 'max':'min'}_${testResults.optParamName}_${testResults.method}.csv`)
}


action.testStrategy = async (request) => {
  console.log('request', request)
  const masterData = await getMasterData(request)

  ui.statusMessageRemove()
  const initParams = {}
  initParams.paramRange = masterData.paramRange
  initParams.paramRangeSrc = model.getStrategyRange(masterData.strategyData)
  initParams.paramConditions = masterData.paramConditions
  const changedStrategyParams = await ui.showAndUpdateStrategyParameters(initParams)
  console.log('changedStrategyParams', changedStrategyParams)
  if(changedStrategyParams === null)
    return
  masterData.testParams.cycles = changedStrategyParams.cycles ? changedStrategyParams.cycles : 100
  if (changedStrategyParams.paramRange === null) {
    console.log('Did not change paramRange')
  } else if (typeof changedStrategyParams.paramRange === 'object' && Object.keys(changedStrategyParams.paramRange).length) {
    masterData.paramRange = changedStrategyParams.paramRange
    await model.saveStrategyParameters( masterData.paramRange)
    console.log('ParamRange changes to',  masterData.paramRange)
    masterData.paramConditions = changedStrategyParams.paramConditions
    await model.saveStrategyParameterConditions(masterData.paramConditions)
    console.log('paramConditions changes to',  masterData.paramConditions)
  } else {
    ui.alertMessage('There an error in strategy parameters. Change them or run default parameters set.')
    return
  }
  masterData.allRangeParams = model.createParamsFromRange(masterData.paramRange)
  console.log('allRangeParams', masterData.allRangeParams)
  if(!masterData.allRangeParams) {
    return
  }

  const isSequential = ['sequential'].includes(masterData.testParams.method)

  if(isSequential) {
    masterData.paramSpaceNumber = Object.keys(masterData.allRangeParams).reduce((sum, param) => sum += masterData.allRangeParams[param].length, 0)
    // ui.alertMessage(`For ${masterData.testParams.method} testing, the number of ${masterData.paramSpaceNumber} cycles is automatically determined, which is equal to the size of the parameter space.\n\nYou can interrupt the search for strategy parameters by just reloading the page and at the same time, you will not lose calculations. All data are stored in the storage after each iteration.\nYou can download last test results by clicking on the "Download results" button until you launch new strategy testing.`)
    // masterData.testParams.cycles = masterData.paramSpaceNumber
  } else {
    masterData.paramSpaceNumber = Object.keys(masterData.allRangeParams).reduce((mult, param) => mult *= masterData.allRangeParams[param].length, 1)
    // const cyclesStr = prompt(`Please enter the number of cycles for optimization.\n\nYou can interrupt the search for strategy parameters by just reloading the page and at the same time, you will not lose calculations. All data are stored in the storage after each iteration.\nYou can download last test results by clicking on the "Download results" button until you launch new strategy testing.`, 100)
    // if(!cyclesStr)
    //   return
    // let cycles = parseInt(cyclesStr)
    // if(!cycles || cycles < 1)
    //   return
    // masterData.testParams.cycles = cycles
  }
  console.log('paramSpaceNumber', masterData.paramSpaceNumber)

  let extraHeader = `The search is performed among ${masterData.paramSpaceNumber} possible combinations of parameters (space).`
  extraHeader += (masterData.paramSpaceNumber/masterData.testParams.cycles) > 10 ? `<br />This is too large for ${masterData.testParams.cycles} cycles. It is recommended to use up to 3-4 essential parameters, remove the rest from the strategy parameters file.` : ''
  ui.statusMessage('Started.', extraHeader)
  masterData.testParams.paramSpace = masterData.paramSpaceNumber
  const { isDeepTest, isInOutSample, inOutRates, deepStartDate, deepEndDate, activatedDiscordUpload } = masterData.testParams
  let isDeepTestChanged = false, testResults;
  /** Need to use copy of masterdata to use fresh master data in out test */
  const masterDataCopy = JSON.parse(JSON.stringify(masterData));
  if (isDeepTest && isInOutSample && inOutRates.split('/').length === 2) {
    const percents = inOutRates.split('/')
    const percentA = parseFloat(percents[0]) / 100.0
    const percentB = parseFloat(percents[1]) / 100.0
    const deepTestDuration = Math.abs(new Date(deepEndDate) - new Date(deepStartDate))
    const tmpDeepEndDate = new Date(new Date(deepStartDate).getTime() + (deepTestDuration * percentA)).toISOString().split('T')[0]
    /**
     * Frist step of in & out
     */
    isDeepTestChanged = await tvUi.setDeepTest(isDeepTest, deepStartDate, tmpDeepEndDate)
    let testResultsA = await backtest.testStrategy(masterData.testParams, masterData.strategyData, masterData.allRangeParams, masterData.paramConditions)
    /**
     * Second step of in & out
     */
    const tmpDeepStartDate = new Date(new Date(tmpDeepEndDate).getTime() + 1000 * 60 * 60 * 24).toISOString().split('T')[0]
    await tvUi.setDeepTest(isDeepTest, tmpDeepStartDate, deepEndDate)
    testResults = await backtest.testStrategy(masterDataCopy.testParams, masterDataCopy.strategyData, masterDataCopy.allRangeParams, masterDataCopy.paramConditions, testResultsA.randomPropDataArray)
    await handleTestResultsInOut(testResultsA, testResults, testResultsA.randomPropDataArray, activatedDiscordUpload);
  } else {
    if (isDeepTest) {
      isDeepTestChanged = await tvUi.setDeepTest(isDeepTest, masterData.testParams.deepStartDate, masterData.testParams.deepEndDate)
    }
    testResults = await backtest.testStrategy(masterData.testParams, masterData.strategyData, masterData.allRangeParams, masterData.paramConditions)
    await handleTestResults(testResults, activatedDiscordUpload)
  }

  if(isDeepTest)
    await tvUi.generateDeepTestReport()
  if (isDeepTest && isDeepTestChanged)
    await tvUi.setDeepTest(!isDeepTest)
}

async function handleTestResults(testResults, activatedDiscordUpload) {
  console.log('testResults', testResults) // Result: { bestPropVal : {Show calculation: true, Ks_length: 42, Br_threshold: 0.12} , bestValue : 15.83, performanceSummary: [ {Net Profit: All: 1583.25, Net Profit %: All: 15.83,... ]}
  if(!testResults.perfomanceSummary && !testResults.perfomanceSummary.length) {
    ui.alertMessage('There is no data for conversion. Try to do test again')
    return
  }

  const CSVResults = file.convertResultsToCSV(testResults)
  const bestResult = testResults.perfomanceSummary ? model.getBestResult(testResults) : {}
  const initBestValue = testResults.hasOwnProperty('initBestValue') ? testResults.initBestValue : null
  const propVal = {}
  testResults.paramsNames.forEach(paramName => {
    if(bestResult.hasOwnProperty(`__${paramName}`))
      propVal[paramName] = bestResult[`__${paramName}`]
  })
  await tvUi.setStrategyParams(testResults.shortName, propVal)
  let text = `All done.\n\n`
  text += bestResult && bestResult.hasOwnProperty(testResults.optParamName) ? 'The best '+ (testResults.isMaximizing ? '(max) ':'(min) ') + testResults.optParamName + ': ' + backtest.prepareValueOut(bestResult[testResults.optParamName]) : ''
  text += (initBestValue !== null && bestResult && bestResult.hasOwnProperty(testResults.optParamName) && initBestValue === bestResult[testResults.optParamName]) ? `\nIt isn't improved from the initial value: ${backtest.prepareValueOut(initBestValue)}` : ''
  ui.statusMessage(text)
  ui.alertMessage(text)
  console.log(`All done.\n\n${bestResult && bestResult.hasOwnProperty(testResults.optParamName) ? 'The best ' + (testResults.isMaximizing ? '(max) ' : '(min) ') + testResults.optParamName + ': ' + backtest.prepareValueOut(bestResult[testResults.optParamName]) : ''}`)
  ui.statusMessageRemove()
  // file.saveAs(CSVResults, `${testResults.ticker}:${testResults.timeFrame} ${testResults.shortName} - ${testResults.cycles}_${testResults.isMaximizing ? 'max':'min'}_${testResults.optParamName}_${testResults.method}.csv`)

  // if (activatedDiscordUpload) {
  //   await action.uploadToDiscord()
  //   await action.uploadImageToDiscord(testResults);
  // }
}

async function handleTestResultsInOut(testResultsA, testResultsB, randomPropDataArray, activatedDiscordUpload) {
  console.log('In testResults', testResultsA)
  console.log('Out testResults', testResultsB)
  if(!testResultsA.perfomanceSummary && !testResultsA.perfomanceSummary.length && !testResultsB.perfomanceSummary && !testResultsB.perfomanceSummary.length) {
    ui.alertMessage('There is no data for conversion. Try to do test again')
    return
  }

  let text = `All done.\n\n`

  const bestResultA = testResultsA.perfomanceSummary ? model.getBestResult(testResultsA) : {}
  const initBestValueA = testResultsA.hasOwnProperty('initBestValue') ? testResultsA.initBestValue : null
  const propValA = {}
  testResultsA.paramsNames.forEach(paramName => {
    if(bestResultA.hasOwnProperty(`__${paramName}`))
      propValA[paramName] = bestResultA[`__${paramName}`]
  })
  text += bestResultA && bestResultA.hasOwnProperty(testResultsA.optParamName) ? 'The best '+ (testResultsA.isMaximizing ? '(max) ':'(min) ') + testResultsA.optParamName + ': ' + backtest.prepareValueOut(bestResultA[testResultsA.optParamName]) : ''
  text += (initBestValueA !== null && bestResultA && bestResultA.hasOwnProperty(testResultsA.optParamName) && initBestValueA === bestResultA[testResultsA.optParamName]) ? `\nIt isn't improved from the initial value: ${backtest.prepareValueOut(initBestValueA)}` : ''

  text += `\n\n`

  const bestResultB = testResultsB.perfomanceSummary ? model.getBestResult(testResultsB) : {}
  const initBestValueB = testResultsB.hasOwnProperty('initBestValue') ? testResultsB.initBestValue : null
  const propValB = {}
  testResultsB.paramsNames.forEach(paramName => {
    if(bestResultB.hasOwnProperty(`__${paramName}`))
      propValB[paramName] = bestResultB[`__${paramName}`]
  })
  text += bestResultB && bestResultB.hasOwnProperty(testResultsB.optParamName) ? 'The best '+ (testResultsB.isMaximizing ? '(max) ':'(min) ') + testResultsB.optParamName + ': ' + backtest.prepareValueOut(bestResultB[testResultsB.optParamName]) : ''
  text += (initBestValueB !== null && bestResultB && bestResultB.hasOwnProperty(testResultsB.optParamName) && initBestValueB === bestResultB[testResultsB.optParamName]) ? `\nIt isn't improved from the initial value: ${backtest.prepareValueOut(initBestValueB)}` : ''

  ui.alertMessage(text)

  const CSVResultsA = file.convertResultsToCSV(testResultsA)
  file.saveAs(CSVResultsA, `${testResultsA.ticker}:${testResultsA.timeFrame} ${testResultsA.shortName} - ${testResultsA.cycles}_${testResultsA.isMaximizing ? 'max' : 'min'}_${testResultsA.optParamName}_In_${testResultsA.method}.csv`)
  const CSVResultsB = file.convertResultsToCSV(testResultsB)
  file.saveAs(CSVResultsB, `${testResultsB.ticker}:${testResultsB.timeFrame} ${testResultsB.shortName} - ${testResultsB.cycles}_${testResultsB.isMaximizing ? 'max' : 'min'}_${testResultsB.optParamName}_Out_${testResultsB.method}.csv`)

  const CSVResultC = file.convertInOutResultsToCSV(testResultsA, testResultsB, randomPropDataArray);
  file.saveAs(CSVResultC, `In_Out_Result.csv`)

  await tvUi.setStrategyParams(testResultsB.shortName, propValB);

  if (activatedDiscordUpload) {
    await action.uploadImageToDiscord(testResultsB);
  }
}

async function getStrategyData() {
  ui.statusMessage('Get the initial parameters.')
  const strategyData = await tvUi.getStrategy()
  if(!strategyData || !strategyData.hasOwnProperty('name') || !strategyData.hasOwnProperty('properties') || !strategyData.properties)
    throw new Error('It was not possible to find a strategy with parameters among the indicators. Add it to the chart and try again.')
  return strategyData
}


async function getAllRangeParameters(strategyData) {
  let [paramRange, paramConditions] = await model.getStrategyParameters(strategyData)
  if(!paramRange)
    throw new Error('Error to get range parameters')
  const allRangeParams = model.createParamsFromRange(paramRange)
  if(!allRangeParams) {
    throw new Error('Error create backtest range parameters')
  }
  if(!paramConditions)
    paramConditions = {}
  return [allRangeParams, paramRange, paramConditions]
}

async function getTestParameters(masterData, testMethod) {
  let paramSpaceNumber
  const isSequential = ['sequential'].includes(testMethod)
  if(isSequential)
    paramSpaceNumber = Object.keys(masterData.allRangeParams).reduce((sum, param) => sum += masterData.allRangeParams[param].length, 0)
  else
    paramSpaceNumber = Object.keys(masterData.allRangeParams).reduce((mult, param) => mult *= masterData.allRangeParams[param].length, 1)
  console.log('paramSpaceNumber', paramSpaceNumber)
  masterData.paramSpaceNumber = paramSpaceNumber
  let testParams = await tvUi.switchToStrategyTab()
  if(!testParams)
    throw new Error('Error get base of test parameters')

  testParams.method = testMethod
  testParams.perfomanceSummary = []
  testParams.filteredSummary = []
  let paramPriority = model.getParamPriorityList(masterData.paramRange) // Filter by allRangeParams
  paramPriority = paramPriority.filter(key => masterData.allRangeParams.hasOwnProperty(key))
  testParams.paramPriority = paramPriority

  testParams.startParams = await model.getStartParamValues(masterData.paramRange, masterData.strategyData)
  if(!testParams.hasOwnProperty('startParams') || !testParams.startParams.hasOwnProperty('current') || !testParams.startParams.current) {
    throw new Error('Error.\n\n The current strategy parameters could not be determined.\n Testing aborted')
  }

  return testParams
}

function setTestOptions(testParams, request) {
  if(request.options) {
    testParams.isMaximizing = request.options.hasOwnProperty('isMaximizing') ? request.options.isMaximizing : true
    testParams.optParamName =  request.options.optParamName ? request.options.optParamName : backtest.DEF_MAX_PARAM_NAME
    const filters = []
    if (request.options.hasOwnProperty('filters')) {
      request.options.filters.forEach(filter => {
        if (filter.hasOwnProperty('mode') && ['more', 'less'].includes(filter['mode']) &&
          filter.hasOwnProperty('parameterName') && filter.hasOwnProperty('value'))
          filters.push(filter)
      })
    }
    testParams.filters = filters //request.options.hasOwnProperty('filters') ? request.options.filters : []
    // testParams.filterAscending = request.options.hasOwnProperty('optFilterAscending') ? request.options.optFilterAscending : null
    // testParams.filterValue = request.options.hasOwnProperty('optFilterValue') ? request.options.optFilterValue : 50
    // testParams.filterParamName = request.options.hasOwnProperty('optFilterParamName') ? request.options.optFilterParamName : 'Total Closed Trades: All'
    testParams.deepStartDate = !request.options.hasOwnProperty('deepStartDate') || request.options['deepStartDate'] === '' ? null : request.options['deepStartDate']
    testParams.deepEndDate = !request.options.hasOwnProperty('deepEndDate') || request.options['deepEndDate'] === '' ? null : request.options['deepEndDate']
    testParams.backtestDelay = !request.options.hasOwnProperty('backtestDelay') || !request.options['backtestDelay'] ? 0 : request.options['backtestDelay']
    testParams.randomDelay = request.options.hasOwnProperty('randomDelay') ? Boolean(request.options['randomDelay']) : true
    testParams.isDeepTest = request.options.hasOwnProperty('shouldDeepBacktest') ? Boolean(request.options['shouldDeepBacktest']) : false
    testParams.dataLoadingTime = request.options.hasOwnProperty('dataLoadingTime') && !isNaN(parseInt(request.options['dataLoadingTime'])) ? request.options['dataLoadingTime'] : 30

    testParams.isInOutSample = request.options.hasOwnProperty('isInOutSample') ? Boolean(request.options['isInOutSample']) : false
    testParams.activatedDiscordUpload = request.options.hasOwnProperty('activatedDiscordUpload') ? Boolean(request.options['activatedDiscordUpload']) : true
    testParams.inOutRates = !request.options.hasOwnProperty('inOutRates') || request.options['inOutRates'] === '' ? null : request.options['inOutRates']
  }
  return testParams
}

action.checkStrategyForWatchList = async (request) => {
  try {
    const masterData = await getMasterData(request)
    const isDeepTest = masterData.testParams.isDeepTest
    let isDeepTestChanged = false
    if (isDeepTest) {
      if (!masterData.testParams.deepStartDate) {
        alert("To run watchlist test, You need to put start date in deep backtesting");
        return;
      }
      isDeepTestChanged = await tvUi.setDeepTest(isDeepTest, masterData.testParams.deepStartDate, masterData.testParams.deepEndDate)
    } else {
      //alert("To run watchlist test, You need to check deep backtesting");
      //return;
    }
    await tvChart.watchListItemsProcess(checkWatchListStrategy, masterData)
    if (isDeepTest & isDeepTestChanged)
      await tvUi.setDeepTest(!isDeepTest)
    const name = getBackTestName(masterData.testParams)
    await action.downloadBestListBacktestResults(masterData.bestBacktestResultsForList,
      `Watchlist_backtest_${masterData.testParams.timeFrame}_${name.replaceAll(' ', '_')}`)
  } catch (err) {
    ui.alertMessage(err)
    console.error(err)
  }
}

action.checkStrategyOnTimeframe = async (request) => {
  try {
    if (!request.hasOwnProperty('options') || !request.options.hasOwnProperty('wlTimeFrameList') || !request.options.wlTimeFrameList)
      throw new Error('There do not set timeframe list')
    if (!request.options.wlTimeFrameList.match(/^(\s*\d+[smhdDW]\s*,?)+$/))
      throw new Error('There an error values or symbols in timeframe list. Only numbers and symbols of intervals are allowed separated by comma: 5m,2h,3d')

    let tickerEl = document.querySelector(SEL.chartTicker)
    if (!tickerEl)
      throw new Error(`Can't get TV ticker element on chart`)
    let curTickerName = tickerEl.innerText

    const masterData = await getMasterData(request)

    masterData.backtestTimeframes = []

    let extraHeader = `The checking the strategy results on current parameters is performed for timeframes list.`
    // ui.statusMessage('Processing', extraHeader)
    const isDeepTest = masterData.testParams.isDeepTest
    let isDeepTestChanged = false
    if (isDeepTest)
      isDeepTestChanged = await tvUi.setDeepTest(isDeepTest, masterData.testParams.deepStartDate, masterData.testParams.deepEndDate)
    const timeframes = request.options.wlTimeFrameList.replaceAll(' ', '').split(',')
    for(let timeframe of timeframes) {
      ui.statusMessage(`Processing ${timeframe}`, extraHeader)
      await testTimeFrameListStrategy(timeframe, masterData, curTickerName)
    }
    if (isDeepTest & isDeepTestChanged)
      await tvUi.setDeepTest(!isDeepTest)

    console.log(masterData)
    const name = getBackTestName(masterData.testParams)
    await action.downloadBestListBacktestResults(masterData.bestBacktestResultsForList,
      `Timeframe_backtests_${curTickerName}_${name.replaceAll(' ', '_')}`)
  } catch (err) {
    ui.alertMessage(err)
    console.error(err)
  }
}


async function testTimeFrameListStrategy(timeframe, masterData, curTickerName) {
  await tvChart.changeTimeFrame(timeframe)
  masterData.backtestTimeframes.push(timeframe)

  ui.statusMessage(timeframe)
  masterData.testParams.timeFrame = timeframe
  masterData.testParams.shortName = masterData.strategyData.name
  let propVal = masterData.testParams.startParams.current
  await backtest.delay(masterData.testParams.backtestDelay,  masterData.testParams.randomDelay)
  const resIter = await backtest.getTestIterationResult(masterData.testParams, propVal, true, true)
  if(!resIter || !resIter.data)
    return
  const reportData = resIter.data
  masterData.testParams.perfomanceSummary = [reportData]

  const testResults = masterData.testParams
  if(!testResults.perfomanceSummary && !testResults.perfomanceSummary.length) {
    ui.alertMessage(`For timeframe ${timeframe} there is no data for conversion. Try to do test again`)
  } else {
    await saveBacktestResult(testResults, masterData, curTickerName, reportData, 'timeframes')
  }
}

action.testStrategyForWatchList = async (request) => {
  try {
    const masterData = await getMasterData(request)
    masterData.testParams.cycles = request.options && request.options && request.options.wlMaxCycles ? request.options.wlMaxCycles : 50
    const isDeepTest = masterData.testParams.isDeepTest
    if (isDeepTest)
      await tvUi.setDeepTest(isDeepTest, masterData.testParams.deepStartDate, masterData.testParams.deepEndDate)
    await tvChart.watchListItemsProcess(testWatchListStrategy, masterData)
    if (isDeepTest)
      await tvUi.setDeepTest(!isDeepTest)
    const name = getBackTestName(masterData.testParams)
    await action.downloadBestListBacktestResults(masterData.bestBacktestResultsForList,
      `Watchlist_backtest_${masterData.testParams.timeFrame}_${name.replaceAll(' ', '_')}`)
  } catch (err) {
    ui.alertMessage(err)
    console.error(err)
  }
}

async function getMasterData(request) {
  const masterData = {processed: [], bestBacktestResultsForList: [], backtestTickerList: []}
  masterData.strategyData = await getStrategyData() // Result: { name: "Khanhdeux Strategy", properties: { Br_threshold : 0.12, Ks_length: 42, Show calculation: true} }
  // let allRangeParams, paramRange
  const [allRangeParams, paramRange, paramConditions] = await getAllRangeParameters(masterData.strategyData)
  masterData.allRangeParams = allRangeParams
  masterData.paramRange = paramRange
  masterData.paramConditions = paramConditions
  const testMethod = request.options && request.options.hasOwnProperty('optMethod') ? request.options.optMethod.toLowerCase() : 'random'
  masterData.testParams = await getTestParameters(masterData, testMethod)
  masterData.testParams = setTestOptions(masterData.testParams, request) // Result: { ticker : "NVDA", timeFrame : "1m" }
  console.log('masterData', masterData)
  return masterData
}

action.downloadBestListBacktestResults = async (backtestResults, name) => {
  if(!backtestResults || !backtestResults.length)
    throw new Error('There none data for backtest results')
  let headers = Object.keys(backtestResults[0])

  headers = headers.filter(item => item !== '_ticker' &&  item !== '_timeframe')
  headers = ['_ticker', '_timeframe'].concat(headers)
  let csv = headers.map(header => JSON.stringify(header)).join(',')
  csv += '\n'
  backtestResults.forEach(row => {
    const rowData = headers.map(key => typeof row[key] !== 'undefined' ? JSON.stringify(row[key]) : '')
    csv += rowData.join(',').replaceAll('\\"', '""')
    csv += '\n'
  })
  file.saveAs(csv, `${name}.csv`)
}

action.downloadTickerParametersForBacktestList = async (backtestResults, filename, strategyName, ticker) => {
  console.log(backtestResults)
  if(!ticker || !ticker === 'none')
    throw new Error('There none data for backtest results')
  if(!backtestResults || !backtestResults.length)
    throw new Error('There none data for backtest results')

  let tickerResults = null
  for(let row of backtestResults) {
    if (row['_ticker'] === ticker) {
      tickerResults = row
      break
    }
  }
  console.log(tickerResults)
  if(!tickerResults)
    throw new Error(`There none ${ticker} data in backtest results`)

  const parameterNames = []
  Object.keys(tickerResults).forEach(key => {
    if(key.startsWith('__'))
      parameterNames.push(key.substr(2))
  })
  console.log(parameterNames)

  let csv = `Name,Value\n"__indicatorName",${JSON.stringify(strategyName)}\n`
  parameterNames.forEach(key => {
    csv += `${JSON.stringify(key)},${typeof tickerResults[`__${key}`] === 'string' ? JSON.stringify(tickerResults[`__${key}`]) : tickerResults[`__${key}`]}\n`
  })
  file.saveAs(csv, `${ticker.replace(':','-')}_${filename}.csv`)

}


async function setEnvAndGetTickerName(wlWrapTickerEl, masterData) {
  const wlTickerEl = wlWrapTickerEl.querySelector(SEL.wlItem)
  if (!wlTickerEl)
    return null
  const tickerNameFull = wlTickerEl.getAttribute('data-symbol-full')

  // const tickerName = wlTickerEl.getAttribute('data-symbol-short')
  if(!tickerNameFull || masterData['processed'].includes(tickerNameFull))
    return null
  masterData['processed'].push(tickerNameFull)
  // try {
  await setChartEnv(wlTickerEl)
  // } catch (err) {
  //   console.error(err)
  //   console.log('Skipped', tickerNameFull)
  //   // ui.alertMessage(err.message)
  //   return null
  // }
  return tickerNameFull
}


async function checkWatchListStrategy(wlWrapTickerEl, masterData) {
  let tickerNameFull
  let extraHeader = `The checking the strategy results on current parameters is performed.`
  try {
    tickerNameFull = await setEnvAndGetTickerName(wlWrapTickerEl, masterData)
    if(!tickerNameFull)
      return
  } catch (err) {
    console.error(err)
    // ui.alertMessage(err.message)
    tickerNameFull = masterData['processed'][masterData['processed'].length - 1]
    const msgText = `Error to set environment for ticker ${tickerNameFull}. Skipped.`
    console.log(msgText)
    await saveBacktestResult({}, masterData, tickerNameFull, {'comment': msgText})
    ui.statusMessage(msgText, extraHeader)
    return
  }

  ui.statusMessage(`Processed ${tickerNameFull}.`, extraHeader)

  masterData.testParams.shortName = masterData.strategyData.name
  let propVal = masterData.testParams.startParams.current
  await backtest.delay( masterData.testParams.backtestDelay,  masterData.testParams.randomDelay)
  const resIter = await backtest.getTestIterationResult(masterData.testParams, propVal, true, true)
  if(!resIter || !resIter.data)
    return
  const reportData = resIter.data
  masterData.testParams.perfomanceSummary = [reportData]

  const testResults = masterData.testParams
  if(!testResults.perfomanceSummary && !testResults.perfomanceSummary.length) {
    const msgText = `For ticker ${tickerNameFull} there is no data for conversion.`
    await saveBacktestResult({}, masterData, tickerNameFull, {'comment': msgText})
    ui.statusMessage(msgText, extraHeader)
    // ui.alertMessage(msgText)
  } else {
    await saveBacktestResult(testResults, masterData, tickerNameFull, reportData)
  }
}

async function testWatchListStrategy(wlWrapTickerEl, masterData) {
  let tickerNameFull
  let extraHeader = `The search is performed among ${masterData.paramSpaceNumber} possible combinations of parameters (space).`
  try {
    tickerNameFull = await setEnvAndGetTickerName(wlWrapTickerEl, masterData)
    if(!tickerNameFull)
      return
  } catch (err) {
    console.error(err)
    // ui.alertMessage(err.message)
    tickerNameFull = masterData['processed'][masterData['processed'].length - 1]
    const msgText = `Error to set environment for ticker ${tickerNameFull}. Skipped.`
    console.log(msgText)
    await saveBacktestResult({}, masterData, tickerNameFull, {'comments': msgText})
    ui.statusMessage(msgText, extraHeader)
    return
  }
  extraHeader += (masterData.paramSpaceNumber/masterData.testParams.cycles) > 10 ?
    `<br />This is too large for ${masterData.testParams.cycles} cycles. It is recommended to use up to 3-4 essential parameters, remove the rest from the strategy parameters file.` : ''
  ui.statusMessage(`Processed ${tickerNameFull}`, extraHeader)

  masterData.testParams.isSkipInitBestResult = true

  const testResults = await backtest.testStrategy(masterData.testParams, masterData.strategyData, masterData.allRangeParams)
  if(!testResults.perfomanceSummary || testResults.perfomanceSummary.length === 0) {
    const msgText = `For ticker ${tickerNameFull} there is no data for conversion.`
    await saveBacktestResult({}, masterData, tickerNameFull, {'comments': msgText})
    ui.statusMessage(msgText, extraHeader)
    // ui.alertMessage(`For ticker ${tickerNameFull} there is no data for conversion. Try to do test again`)
  } else {
    await saveBacktestResult(testResults, masterData, tickerNameFull)
  }
}

async function saveBacktestResult(testResults, masterData, tickerNameFull, checkResult = null, backtestType = 'tickers') {
  const bestResult = checkResult ? checkResult : testResults.perfomanceSummary ? model.getBestResult(testResults) : {}
  bestResult['_ticker'] = tickerNameFull
  bestResult['_timeframe'] = masterData.testParams.timeFrame

  masterData.bestBacktestResultsForList.push(bestResult)
  masterData.backtestTickerList.push(tickerNameFull)
  const name = getBackTestName(masterData.testParams)
  await storage.setKeys('bestBacktestResultsForList', {
    name: `Watchlist_backtest_${masterData.testParams.timeFrame}_${name.replaceAll(' ', '_')}`,
    data: masterData.bestBacktestResultsForList, strategyName: testResults.shortName, tickers: masterData.backtestTickerList,
    timeframes: masterData.backtestTimeframes,
    type: backtestType})
}

function getBackTestName(testResults) {
  return `${testResults.shortName} ${testResults.isMaximizing ? 'max' : 'min'}, value of, ${testResults.optParamName},` +
    `by ${testResults.method}, ` +
    (testResults.filters && testResults.filters.length ? `with ${testResults.filters.length} filters` : 'filter off')
    // (testResults.filterAscending === null ? 'filter off' : 'filter ' + (testResults.filterAscending ? 'more' : 'less') + ' than ' +
    //   testResults.filterValue + ' by ' + testResults.filterParamName)
}

async function setChartEnv(wlTickerEl) { // TODO fix ti set environment by click on watchlist element
  const tickerNameFull = wlTickerEl.getAttribute('data-symbol-full')
  const tickerName = wlTickerEl.getAttribute('data-symbol-short')
  console.log(tickerName, tickerNameFull)
  await tvChart.setTicker(tickerName, tickerNameFull)
}
