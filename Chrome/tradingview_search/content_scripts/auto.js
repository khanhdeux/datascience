/**
 * @typedef {{url: string, inputFieldEnter: string, inputValue: string, divClassName: string, divValidator?: Function}} Website
 */
const tvAutoScript = (function () {
  /** @type {Array<Website>} */
  const websites = [
    {
      url: 'bybit.com',
      inputFieldEnter: '.index_ref-code__input__BBv7c',
      inputValue: '35514',
      divClassName: '.cht-gap-x-1'
    },
    {
      url: 'bitget.com',
      divClassName: 'label[data-v-67f205be]',
      inputFieldEnter: "[name='dnPwdConfirm']",
      inputValue: 'rlsi'
    },
    {
      url: 'okx.com',
      divClassName: '.invite-label',
      inputFieldEnter: '#invite-code-input',
      inputValue: '60478762'
    },
    {
      url: 'kucoin.com',
      divClassName:
        'form.KuxForm-form>div.KuxForm-item:nth-child(3)>div.KuxRow-row>div>div>div',
      divValidator: (div) => {
        return div.innerHTML.includes('Referral Code (Optional)');
      },
      inputFieldEnter:
        'form.KuxForm-form>div.KuxForm-item:nth-child(3)>div.KuxRow-row>div #referralCode',
      inputValue: 'rMV8GQN'
    }
  ];
  let oldLocationHref = '';

  /** @param {HTMLElement} element */
  function isElementRendered(element) {
    return element && element.offsetWidth > 0 && element.offsetHeight > 0;
  }

  /**
   * @param {string} selector
   * @param {number} timeout
   * @param {boolean} isHide
   * @returns {Promise<HTMLElement>}
   */
  async function waitForSelector(selector, timeout = 5000, isHide = false) {
    return new Promise(async (resolve) => {
      let iter = 0;
      let elem;
      const tikTime = timeout === 0 ? 1000 : 50;
      do {
        await page.waitForTimeout(tikTime);
        elem = document.querySelector(selector);
        iter += 1;
      } while (
        (timeout === 0 ? true : tikTime * iter < timeout) &&
        (isHide ? !!elem : !elem)
      );
      resolve(elem);
    });
  }

  /**
   * @param {HTMLInputElement} inputField
   * @param {string} inputValue
   * @returns {boolean}
   */
  function fillInputField(inputField, inputValue) {
    if (!inputField) {
      console.log(`Input field not found`);
      return false;
    }

    // console.log(inputField);

    //inputField.focus();
    inputField.value = inputValue;

    const inputEvent = new Event('input', { bubbles: true });
    inputField.dispatchEvent(inputEvent);

    inputField.blur();
    inputField.readOnly = true;

    return true;
  }

  /**
   * @param {Website} website
   * @returns {boolean}
   */
  async function proceedDiv(website) {
    const attributeName = 'data-tv-auto-proceed';
    try {
      const div = await waitForSelector(website.divClassName);
      if (!div || (website.divValidator && !website.divValidator(div))) {
        console.log(`Div not found`);
        return false;
      }

      // console.log(div);

      if (div.getAttribute(attributeName)) {
        return true;
      }

      page.mouseClickEl(div);

      inputField = await waitForSelector(website.inputFieldEnter);
      const success = fillInputField(inputField, website.inputValue);
      if (success) {
        div.setAttribute(attributeName, true);
      }
      return success;
    } catch (error) {
      console.error(error);
    }
    return false;
  }

  /**
   * @param {Website} website
   */
  async function proceed(website) {
    await proceedDiv(website);

    const domObserver = new MutationObserver(async (mutations) => {
      const success = await proceedDiv(website);
      if (success) {
        domObserver.disconnect();
      }
    });
    domObserver.observe(document.body, { childList: true, subtree: true });

    const hrefObserver = new MutationObserver(async (mutations) => {
      mutations.forEach(async (mutation) => {
        if (oldLocationHref !== window.location.href) {
          oldLocationHref = window.location.href;

          const success = await proceedDiv(website);
          if (success) {
            hrefObserver.disconnect();
          }
        }
      });
    });
    hrefObserver.observe(document.body, { childList: true, subtree: true });
  }

  async function main() {
    console.log('main');
    for (const website of websites) {
      if (window.location.href.includes(website.url)) {
        proceed(website);
      }
    }
  }

  return { main };
})();

window.addEventListener('load', () => {
  tvAutoScript.main();
});
