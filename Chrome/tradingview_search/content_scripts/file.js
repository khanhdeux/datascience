const file = {}

file.saveAs = (text, filename) => {
  let aData = document.createElement('a');
  aData.setAttribute('href', 'data:text/plain;charset=urf-8,' + encodeURIComponent(text));
  aData.setAttribute('download', filename);
  aData.click();
  if (aData.parentNode)
    aData.parentNode.removeChild(aData);
}

file.upload = async (handler, endOfMsg, isMultiple = false) => {
  let fileUploadEl = document.createElement('input');
  fileUploadEl.type = 'file';
  if(isMultiple)
    fileUploadEl.multiple = 'multiple';
  fileUploadEl.addEventListener('change', async () => {
    let message = isMultiple ? 'File upload results:\n' : 'File upload result:\n'
    for(let file of fileUploadEl.files) {
      message += await handler(file)
    }
    message += endOfMsg ? '\n' + endOfMsg : ''
    ui.alertMessage(message)
    ui.isMsgShown = false
  });
  fileUploadEl.click();
}



file.parseCSV = async (fileData, isReturnHeadersInFirstRow = false) => {
  return new Promise((resolve, reject) => {
    const CSV_FILENAME = fileData.name
    const isCSV = CSV_FILENAME.toLowerCase().endsWith('.csv')
    if(!isCSV) return reject(`please upload correct file.`)
    const reader = new FileReader();
    reader.addEventListener('load', async (event) => {
      if(!event.target.result) return reject(`there error when loading content from the file ${CSV_FILENAME}`)
      const CSV_VALUE = event.target.result
      try {
        const sep = detectCSVSeparator(CSV_VALUE)
        console.log('CSV Separator', sep)
        const csvData = parseCSV2JSON(CSV_VALUE, sep)
        if (csvData && csvData.length) {
          if (isReturnHeadersInFirstRow) {
            const header = getCSVHeaders(CSV_VALUE, sep)
            csvData.push(header)
            return resolve(csvData)
          }
          return resolve(csvData)
        }
      } catch (err) {
        console.error(err)
        return reject(`CSV parsing error: ${err.message}`)
      }
      return reject(`there is no data in the file`)
    })
    return reader.readAsText(fileData);
  });
}


file.uploadHandler = async (fileData) => {
  const propVal = {}
  let strategyName = null
  let isSetFromBacktest = false

  const csvData = await file.parseCSV(fileData, true)
  const headerCSVInCorrectCase = csvData.pop()
  const headers = Object.keys(csvData[0])
  const headersLowCase = []
  for (const key of headers) {
    headersLowCase.push(key.toLowerCase())
  }
  const missColumns = ['Name','Value'].filter(columnName => !headersLowCase.includes(columnName.toLowerCase()))
  if(missColumns && missColumns.length) {
    // If upload results
    const parametersColumns = headerCSVInCorrectCase.filter(columnName => columnName.startsWith('__'))
    if(!parametersColumns || parametersColumns.length === 0 || !csvData.length) {
      return `  - ${fileData.name}: There is no column(s) "${missColumns.join(', ')}" in CSV.\nPlease add all necessary columns to CSV like showed in the template.\n\nSet parameters canceled.\n`
    } else {
      let indicatorTitleEl = document.querySelector(SEL.indicatorTitle)
      if(!indicatorTitleEl || !indicatorTitleEl.innerText) {
        return  `Impossible to parse the name of the strategy`
      }
      strategyName = indicatorTitleEl.innerText // Only work if strategy popup opened
      for (const key of parametersColumns) {
        const paramName = key.substr(2)
        propVal[paramName] = csvData[0][key.toLowerCase()]
      }
      isSetFromBacktest = true
    }
  } else {
    for (const row of csvData) {
      if(row['name'] === '__indicatorName')
        strategyName = row['value']
      else
        propVal[row['name']] = row['value']
    }
  }

  if(!strategyName)
    return 'The name for indicator in row with name "__indicatorName" is missed in CSV file'
  const res = await tvUi.setStrategyParams(strategyName, propVal, null) // true - removed from task
  if(res) {
    return `The parameters are set${isSetFromBacktest ? ' from the first row of backtest parameters' : ''}`
  }
  return `The name "${strategyName}" of the indicator from the file does not match the name in the open window`
}


function detectCSVSeparator(CSV_VALUE) {
  const csv = CSV_VALUE.split(/\r\n|\r|\n/g).filter(item => item)
  if(csv.length === 0)
    throw new Error('CSV have zero length')
  const header = csv[0]
  const comma = header.split(',')
  const semicolon = header.split(';')
  return comma.length >= semicolon.length ? ',' : ';'
}


function parseCSV2JSON(s, sep= ',') {
  const csv = s.split(/\r\n|\r|\n/g).filter(item => item).map(line => parseCSVLine(line, sep))
  if(!csv || csv.length <= 1) return []
  const headers = csv[0].map(item => item.toLowerCase())
  const JSONData = csv.slice(1).map((line) => {
    const lineObj = {}
    for (let line_index = 0; line_index < line.length; line_index++) {
      const value = line[line_index];
      if (headers[line_index].length)
        lineObj[headers[line_index]] = value
    }
    return lineObj
  })
  console.log({JSONData})
  return JSONData;
}

function getCSVHeaders(s, sep= ',') {
  const csv = s.split(/\r\n|\r|\n/g).filter(item => item).map(line => parseCSVLine(line, sep))
  if(!csv || csv.length <= 1) return []
  const headers = csv[0]
  return headers;
}


function parseCSVLine(text, sep) {
  function replaceEscapedSymbols(textVal) {
    return textVal.replaceAll('\\"', '"')
  }

  if(sep === ',') {
    return text.match( /\s*(".*?"|'.*?'|[^,]+|)\s*(,(?!\s*\\")|$)/g ).map(function (subText) { // \s*(\".*?\"|'.*?'|[^,]+|)\s*(,|$)
      let m;
      if (m = subText.match(/^\s*\"(.*?)\"\s*,?(?!\s*\\")$/))
        return replaceEscapedSymbols(m[1])//m[1] // Double Quoted Text // /^\s*\"(.*?)\"\s*,?$/
      if (m = subText.match(/^\s*'(.*?)'\s*,?$/))
        return replaceEscapedSymbols(m[1]); // Single Quoted Text
      if (m = subText.match(/^\s*(true|false)\s*,?$/i))
        return m[1].toLowerCase() === 'true'; // Boolean
      if (m = subText.match(/^\s*((?:\+|\-)?\d+)\s*,?$/))
        return parseInt(m[1]); // Integer Number
      if (m = subText.match(/^\s*((?:\+|\-)?\d*\.\d*)\s*,?$/))
        return parseFloat(m[1]); // Floating Number
      if (m = subText.match(/^\s*(.*?)\s*,?$/))
        return replaceEscapedSymbols(m[1]); // Unquoted Text
      return subText;
    });
  }
  return text.match( /\s*(".*?"|'.*?'|[^;]+|)\s*(;(?!\s*\\")|$)/g ).map(function (subText) { // \s*(\".*?\"|'.*?'|[^,]+|)\s*(,|$)
    let m;
    if (m = subText.match(/^\s*\"(.*?)\"\s*;?(?!\s*\\")$/))
      return replaceEscapedSymbols(m[1])//m[1] // Double Quoted Text // /^\s*\"(.*?)\"\s*,?$/
    if (m = subText.match(/^\s*'(.*?)'\s*;?$/))
      return replaceEscapedSymbols(m[1]); // Single Quoted Text
    if (m = subText.match(/^\s*(true|false)\s*;?$/i))
      return m[1].toLowerCase() === 'true'; // Boolean
    if (m = subText.match(/^\s*((?:\+|\-)?\d+)\s*;?$/))
      return parseInt(m[1]); // Integer Number
    if (m = subText.match(/^\s*((?:\+|\-)?\d*\.\d*)\s*;?$/))
      return parseFloat(m[1]); // Floating Number
    if (m = subText.match(/^\s*(.*?)\s*;?$/))
      return replaceEscapedSymbols(m[1]); // Unquoted Text
    return subText;
  });

}

function prepareCSVOut(value, key = '') {
  if (!value)
    return 0
  if (typeof value !== 'number')
    return JSON.stringify(value)
  // if (key && key.includes('%'))
  //   return  (value * 100).toFixed(5) // * 100
  return (Math.round(value * 100)/100).toFixed(2)
  // return (value * 10).toFixed(2)/10
}

file.convertResultsToCSV = (testResults) => {
  if(!testResults || !testResults.perfomanceSummary || !testResults.perfomanceSummary.length)
    return 'There is no data for conversion'
  let headers = Object.keys(testResults.perfomanceSummary[0]) // The first test table can be with error and can't have rows with previous values when parsedReport
  if(testResults.hasOwnProperty('paramsNames') && headers.length <= (Object.keys(testResults.paramsNames).length + 1)) { // Find the another header if only params names and 'comment' in headers
    const headersAll = testResults.perfomanceSummary.find(report => Object.keys(report).length > headers.length)
    if(headersAll)
      headers = Object.keys(headersAll)
  }

  headers = headers.reduce(function (csvHeader, header) {
    if (!header.includes("Discord") && !header.includes("discord") && !header.startsWith("Telegram") && !header.startsWith("__API") && !header.startsWith("Email Address")) {
      csvHeader.push(header);
    }
    return csvHeader;
  }, []);

  let csv = headers.map(header => JSON.stringify(header)).join(',')
  csv += '\n'
  // testResults.paramsNames.forEach(paramName => csv.replace(`__${paramName}`, paramName)) // TODO isFirst? or leave it as it is
  for (const row of testResults.perfomanceSummary) {
    // const rowData = headers.map(key => typeof row[key] !== 'undefined' ? JSON.stringify(row[key]) : '')
    const rowData = headers.map(key => typeof row[key] === 'undefined' ? '' : prepareCSVOut(row[key], key))
    csv += rowData.join(',').replaceAll('\\"', '""')
    csv += '\n'
  }
  if(testResults.filteredSummary && testResults.filteredSummary.length) {
    csv += headers.map(key => key !== 'comment' ? '' : 'Bellow filtered results of tests') // Empty line
    csv += '\n'
    for (const row of testResults.filteredSummary) {
      // const rowData = headers.map(key => typeof row[key] !== 'undefined' ? JSON.stringify(row[key]) : '')
      const rowData = headers.map(key => typeof row[key] === 'undefined' ? '' : prepareCSVOut(row[key], key))
      csv += rowData.join(',').replaceAll('\\"', '""')
      csv += '\n'
    }
  }
  return csv
}

file.convertInOutResultsToCSV = (testResultsA, testResultsB, randomPropDataArray) => {
  if(!testResultsA || !testResultsA.perfomanceSummary || !testResultsA.perfomanceSummary.length || !testResultsB || !testResultsB.perfomanceSummary || !testResultsB.perfomanceSummary.length)
    return 'There is no data for conversion'
  console.log(randomPropDataArray);
  let headers = ["Net Profit: All (in sample)", "Net Profit: All (out sample)", "Comment"];
  headers = headers.concat(Object.keys(randomPropDataArray[0].data));
  console.log(headers);
  let csv = headers.map(header => JSON.stringify(header)).join(',')
  csv += '\n'
  randomPropDataArray.map((row, index) => {
    let rowData;
    if (testResultsA.filters.length && testResultsA.filteredSummary && testResultsA.filteredSummary.length) {
      rowData = [testResultsA.filteredSummary[index]["Net Profit: All"], testResultsB.filteredSummary[index]["Net Profit: All"], row.message];
    } else {
      rowData = [testResultsA.perfomanceSummary[index + 1]["Net Profit: All"], testResultsB.perfomanceSummary[index + 1]["Net Profit: All"], row.message];
    }
    headers.map(key => {
      if (key in row.data) {
        rowData.push(row.data[key])
      }
    });
    csv += rowData.join(',').replaceAll('\\"', '""')
    csv += '\n'
  })
  return csv;
}