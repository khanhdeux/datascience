Referennce

https://github.com/marcel-dempers/docker-development-youtube-series/tree/master/kubernetes


# apply deployment
kubectl apply -f deployments/deployment.yaml
kubectl apply -f secrets/secret.yaml

# deploy
kubectl get deploy

# create new namespace
kubectl create ns example-app

# get storageclass
kubectl get storageclass
