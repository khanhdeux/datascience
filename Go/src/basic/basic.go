package main

import "fmt"
import "math"
import "unsafe"
import "errors"

import "time"
import "math/rand"

// import "example/model"

import "os"
import "net"

func main() {
/******************************
Build: install the package into bin directory
cd $GOPATH/src/basic && \ 
go install && \
cd $GOPATH/bin && \
./basic 
*******************************/    
	fmt.Println("Hello, world!")
	// Hello, world!
	fmt.Println("\n%----------VARIABLE------------%\n")
	var age int
	fmt.Println("my age is", age)
	// my age is 0

	age = 29
	fmt.Println("my age is", age)
	// my age is 29

	age = 54
	fmt.Println("my new age is", age)
	// my new age is 54

	var newAge int = 29
	fmt.Println("my age is", newAge)
	// my age is 29

	var newIntAge = 29
	fmt.Println("my age is", newIntAge)
	// my age is 29

	var width, height int = 100, 50 
	fmt.Println("width is", width, "height is", height)
	// width is 100 height is 50

    var newWidth, newHeight = 100, 50
    fmt.Println("width is", newWidth, "height is", newHeight)

	var initWidth, initHeight int
    fmt.Println("width is", initWidth, "height is", initHeight)
    // width is 0 height is 0

    initWidth = 100
    initHeight = 50
    fmt.Println("new width is", initWidth, "new height is ", initHeight)
    // new width is 100 new height is  50

    var (
        cName   = "naveen"
        cAge    = 29
        cHeight int
    )

    fmt.Println("my name is", cName, 
    			", age is", cAge, 
    			"and height is", cHeight)
	// my name is naveen , age is 29 and height is 0    

	sName, sAge := "naveen", 29
	fmt.Println("my name is", sName, "age is", sAge)
	// my name is naveen age is 29

	// Error: name, age := "naveen" 

	a, b := 20, 30
	fmt.Println("a is", a, "b is", b)
	// a is 20 b is 30
    b, c := 40, 50
    fmt.Println("b is", b, "c is", c)
    // b is 40 c is 50
    b, c = 80, 90
    fmt.Println("changed b is", b, "c is", c)
    // changed b is 80 c is 90

    // Error:
    // a, b := 20, 30
    // a, b := 40, 50

    e, f := 145.8, 543.8
    g := math.Min(e, f)
    fmt.Println("minimum value is ", g)
    // minimum value is  145.8

    // Error: 
    // age := 29 
    // age = "naveen

    fmt.Println("\n%----------TYPE------------%\n")

   	i := true
    k := false
    fmt.Println("i:", i, "k:", k)    
    // i: true k: false
    m := i && k
    fmt.Println("m:", m)
    // m: false
    n := i || k
    fmt.Println("n:", n)
    // n: true

    var o int = 89
    p := 95
    fmt.Println("value of o is", o, "and p is", p)
    // value of o is 89 and p is 95
    fmt.Printf("type of o is %T, size of o is %d\n", o, unsafe.Sizeof(o))
    // type of o is int, size of o is 8
    fmt.Printf("type of p is %T, size of p is %d\n", p, unsafe.Sizeof(p))
    // type of p is int, size of p is 8

    a1, b1 := 5.67, 8.97
    fmt.Printf("type of a1 %T b1 %T\n", a1, b1)
    // type of a1 float64 b1 float64
    sum := a1 + b1
    diff := a1 - b1
    fmt.Println("sum", sum, "diff", diff)
    // sum 14.64 diff -3.3000000000000007

    no1, no2 := 56, 89
    fmt.Println("sum", no1+no2, "diff", no1-no2)
    // sum 145 diff -33

    c1 := complex(5, 7)
    c2 := 8 + 27i
    cadd := c1 + c2
    fmt.Println("sum:", cadd)
    // sum: (13+34i)
    cmul := c1 * c2
    fmt.Println("product:", cmul)
    // product: (-149+191i)

    first := "Naveen"
    last := "Ramanathan"
    name := first +" "+ last
    fmt.Println("My name is",name)
    // My name is Naveen Ramanathan

    // Error:
    // i := 55      //int
    // j := 67.8    //float64
    // sum := i + j //int + float64 not allowed
    // fmt.Println(sum)    

    i1 := 55      //int
    j1 := 67.8    //float64
    sum1 := i1 + int(j1) //j is converted into "int"
    fmt.Println(sum1)    
    // 122

    i2 := 10
    var j2 float64 = float64(i2)  
    fmt.Println("j", j2)  
    // 10

    fmt.Println("\n%----------CONSTANST------------%\n")    

    const myFavLanguage = "Python"
	const sunRisesInTheEast = true
	const cA int = 1234
	const cB string = "Hi"
	const country, code = "Vietnam", 84
 
	const (
		employeeId string = "E101"
		salary float64 = 50000.0
	)	

	// Error: 
	// const a = 123
	// a = 321
	
	const cA1 = 1
	const cF1 = 4.5
	const cB1 = true
	const cS1 = "Hello"

	var myOneInt int = 1
	var myOneFloat float64 = 1
	var myOneComplex complex64 = 1	

	fmt.Printf("type of myOneInt is %T\n", myOneInt)
	fmt.Printf("type of myOneFloat is %T\n", myOneFloat)
	fmt.Printf("type of myOneComplex is %T\n", myOneComplex)
	// type of myOneInt is int
	// type of myOneFloat is float64
	// type of myOneComplex is complex64

	type RichString string
	var myString string = "Hello"
	fmt.Println(myString)
	// Hello
	// Error: var myRichString RichString = myString	
	const myUntypedString = "Hello"
	var myRichString RichString = myUntypedString
	fmt.Println(myRichString)
	// Hello

	// Error: const typedInt int = 1 
	// var myFloat64 float64 = typedInt

	const a3 = 5 + 7.5 // Correct
	const b3 = 12/5    // Correct
	const c3 = 'z' + 1 // Correct	 
	// const d = "Hey" + true // Incorrect	
	fmt.Println("a3:",a3,"b3:",b3,"c3:",c3)
	// a3: 12.5 b3: 2 c3: 123

	const a4 = 7.5 > 5        // true
	const b4 = "xyz" < "uvw"  // false	
	fmt.Println("a4:",a4,"b4:",b4)
	// a4: true b4: false

	const a5 = 1 << 5          // 32 (integer)
	const b5 = int32(1) << 4   // 16 (int32) 
	const c5 = 16.0 >> 2       // 4 (integer) - 16.0 can be presented as type `int`
	const d5 = 32 >> 3.0       // 4 (integer) - 3.0 can be presented as type `uint`
	 
	// const e = 10.50 << 2      // error (10.50 không thể biểu diễn giá trị kiểu `int`)
	// const f5 = 64 >> -2 	

    var result = 25/2
    fmt.Printf("result is %v which is of type %T\n", result, result)
    // result is 12 which is of type int 

    var resultFloat = 25.0/2
    resultFloat = float64(25)/2
    fmt.Printf("result is %v which is of type %T\n", resultFloat, resultFloat)
    // result is 12.5 which is of type float64

    var resultInitComplex = 4.5 + (10 - 5) * (3 + 2)/2
    fmt.Println(resultInitComplex)
    // 16.5

    var resultComplex = 4.5 + (10 - 5) * (3 + 2)/2.0
    resultComplex = 4.5 + float64((10 - 5) * (3 + 2))/2
    fmt.Printf("result is %v which is of type %T\n", resultComplex, resultComplex)
    // result is 17 which is of type float64

    fmt.Println("\n%---------FUNCTIONS-------------%\n")    
    x1 := 5.75
    y1 := 6.25    

    result1 := avg(x1, y1)
    fmt.Printf("Average of %.2f and %.2f = %.2f\n", x1, y1, result1)   
    // Average of 5.75 and 6.25 = 6.00 

    prevStockPrice := 75000.0
    currentStockPrice := 100000.0

    change, percentChange := getStockPriceChange(prevStockPrice, currentStockPrice)
 
    if change < 0 {
        fmt.Printf("The Stock Price decreased by $%.2f which is %.2f%% of the prev price\n", math.Abs(change), math.Abs(percentChange))
    } else {
        fmt.Printf("The Stock Price increased by $%.2f which is %.2f%% of the prev price\n", change, percentChange)
    }    
    // The Stock Price increased by $25000.00 which is 33.33% of the prev price

    prevStockPrice = 0.0
    currentStockPrice = 100000.0
     
    change, percentChange, err := getStockPriceChangeWithError(prevStockPrice, currentStockPrice)
    if err != nil {
        fmt.Println("Sorry! There was an error: ", err)
    } else {
        if change < 0 {
            fmt.Printf("The Stock Price decreased by $%.2f which is %.2f%% of the prev price\n", math.Abs(change), math.Abs(percentChange))
        } else {
            fmt.Printf("The Stock Price increased by $%.2f which is %.2f%% of the prev price\n", change, percentChange)
        }
    }
    // Sorry! There was an error:  Previous price cannot be zero

    prevStockPrice = 100000.0
    currentStockPrice = 90000.0
 
    change2, percentChange2 := getNamedStockPriceChange(prevStockPrice, currentStockPrice)
 
    if change2 < 0 {
        fmt.Printf("The Stock Price decreased by $%.2f which is %.2f%% of the prev price\n", math.Abs(change2), math.Abs(percentChange2))
    } else {
        fmt.Printf("The Stock Price increased by $%.2f which is %.2f%% of the prev price\n", change2, percentChange2)
    }
    // The Stock Price decreased by $10000.00 which is 10.00% of the prev price

    prevStockPrice = 80000.0
    currentStockPrice = 120000.0
 
    change3, _ := getStockPriceChange(prevStockPrice, currentStockPrice)
 
    if change3 < 0 {
        fmt.Printf("The Stock Price decreased by $%.2f\n", math.Abs(change3))
    } else {
        fmt.Printf("The Stock Price increased by $%.2f\n", change3)
    }    
    // The Stock Price increased by $40000.00

    fmt.Println("\n%----------PACKAGE------------%\n")    

    // Find the max number
    fmt.Println(math.Max(73.15, 92.46)) 
    // 92.46

    fmt.Println(math.Sqrt(225))
    // 15

    fmt.Println(math.Pi)
    // 3.141592653589793

    // Random integer from 0-100
    epoch := time.Now().Unix()
    rand.Seed(epoch)
    fmt.Println(rand.Intn(100)) 
    // e.g: 59

    // MaxInt64 can be exported
    fmt.Println("Max value of int64: ", int64(math.MaxInt64))
    // Max value of int64:  9223372036854775807
 
    // Phi can be exported
    fmt.Println("Value of Phi (ϕ): ", math.Phi)
    // Value of Phi (ϕ):  1.618033988749895

    // Error
    // fmt.Println("Value of Pi (): ", math.pi)

    fmt.Println("\n%--------ARRAY--------------%\n")   
    
    var x[5] int 
    fmt.Println(x)    
    // [0 0 0 0 0]

    var y [8] string
    fmt.Println(y)    
    // [       ]

    var z [3]complex128
    fmt.Println(z)
    // [(0+0i) (0+0i) (0+0i)]

    var x2 [5]int // Mảng gồm 5 phần tử số nguyên
 
    x2[0] = 100
    x2[1] = 101
    x2[3] = 103
    x2[4] = 105
 
    fmt.Printf("x2[0] = %d, x2[1] = %d, x2[2] = %d\n", x2[0], x2[1], x2[2])
    fmt.Println("x2 = ", x2)    
    // x2[0] = 100, x2[1] = 101, x2[2] = 0
    // x2 =  [100 101 0 103 105]

    var a2 = [5]int{2, 4, 6, 8, 10}
    fmt.Println(a2)
    // [2 4 6 8 10]

    b2 := [5]int{2, 4, 6, 8, 10}
    fmt.Println(b2)    
    // [2 4 6 8 10]

    c4 := [5]int{2}
    fmt.Println(c4)    
    // [2 0 0 0 0]

    d2 := [...]int{3, 5, 7, 9, 11, 13, 17}
    fmt.Println(d2)
    // [3 5 7 9 11 13 17]

    // Error
    // var a = [5]int{3, 5, 7, 9, 11}
    // var b [10]int = a  // a and b is 2 different types

    a6 := [5]string{"English", 
    "Japanese", 
    "Spanish", 
    "French",
     "Hindi"}

    b6 := a6
    b6[1] = "German"

    fmt.Println("a6 = ", a6)
    fmt.Println("b6 = ", b6)    
    // a6 =  [English Japanese Spanish French Hindi]
    // b6 =  [English German Spanish French Hindi]

    names := [3]string{"Mark Zuckerberg", 
    "Bill Gates", 
    "Larry Page"}

    for i := 0; i < len(names); i++ {
        fmt.Println(names[i])
    }
    // Mark Zuckerberg
    // Bill Gates
    // Larry Page
    sum_arr := [4]float64{3.5, 7.2, 4.8, 9.5}
    sum_total := float64(0)
 
    for i := 0; i < len(sum_arr); i++ {
        sum_total += sum_arr[i]
    }
 
    fmt.Printf("Sum of all the elements in array  %v = %f\n", sum_arr, sum_total)
    // Sum of all the elements in array  [3.5 7.2 4.8 9.5] = 25.000000

    daysOfWeek := [7]string{"Mon", 
    "Tue", 
    "Wed", 
    "Thu", 
    "Fri", 
    "Sat", 
    "Sun"}
 
    for index, value := range daysOfWeek {
        fmt.Printf("Day %d of week = %s\n", index, value)
    }
    // Day 0 of week = Mon
    // Day 1 of week = Tue
    // Day 2 of week = Wed
    // Day 3 of week = Thu
    // Day 4 of week = Fri
    // Day 5 of week = Sat
    // Day 6 of week = Sun  

    sum_arr_2 := [4]float64{3.5, 7.2, 4.8, 9.5}
    sum_total_2 := float64(0)
 
    for _, value := range sum_arr_2 {
        sum_total_2 += value
    }
 
    fmt.Printf("Sum of all the elements in array %v = %f\n", sum_arr_2, sum_total_2)
    // Sum of all the elements in array [3.5 7.2 4.8 9.5] = 25.00000

    multi_array := [2][2]int{
        {3, 5},
        {7, 9},
    }
    fmt.Println(multi_array)
    // [[3 5] [7 9]

    multi_missing_array := [3][4]float64{
        {1, 3},
        {4.5, -3, 7.4, 2},
        {6, 2, 11},
    }
    fmt.Println(multi_missing_array)    
    // [[1 3 0 0] [4.5 -3 7.4 2] [6 2 11 0]]

    fmt.Println("\n%----------IF-SWITCH------------%\n")

    var x7 = 25
    if(x7 % 5 == 0) {
        fmt.Printf("%d is a multiple of 5\n", x7)
    }    
    // 25 is a multiple of 5

    var y7 = -1
    if y7 < 0 {
        fmt.Printf("%d is negative\n", y7)
    }
    // -1 is negative

    var age7 = 21
    if age7 >= 17 && age7 <= 30 {
        fmt.Println("My Age is between 17 and 30")
    }    
    // My Age is between 17 and 30

    var age8 = 18
    if age8 >= 18 {
        fmt.Println("You're eligible to vote!")
    } else {
        fmt.Println("You're not eligible to vote!")
    }    
    // You're eligible to vote!

    var BMI = 21.0
    if BMI < 18.5 {
        fmt.Println("You are underweight");
    } else if BMI >= 18.5 && BMI < 25.0 {
        fmt.Println("Your weight is normal");
    } else if BMI >= 25.0 && BMI < 30.0 {
        fmt.Println("You're overweight")
    } else {
        fmt.Println("You're obese")
    }
    // Your weight is normal

    switch dayOfWeek := 6; dayOfWeek {
        case 1: fmt.Println("Monday")
        case 2: fmt.Println("Tuesday")
        case 3: fmt.Println("Wednesday")
        case 4: fmt.Println("Thursday")
        case 5: fmt.Println("Friday")
        case 6: {
            fmt.Println("Saturday")
            fmt.Println("Weekend. Yaay!")
        }
        case 7: {
            fmt.Println("Sunday")
            fmt.Println("Weekend. Yaay!")
        }
        default: fmt.Println("Invalid day")
    }
    // Saturday
    // Weekend. Yaay!

    switch dayOfWeek2 := 5; dayOfWeek2 {
        case 1, 2, 3, 4, 5:
            fmt.Println("Weekday")
        case 6, 7:
            fmt.Println("Weekend")
        default:
            fmt.Println("Invalid Day")      
    }    
    // Weekday

    BMI = 21.0 
    switch {
        case BMI < 18.5:
            fmt.Println("You're underweight")
        case BMI >= 18.5 && BMI < 25.0:
            fmt.Println("Your weight is normal")
        case BMI >= 25.0 && BMI < 30.0:
            fmt.Println("You're overweight")
        default:
            fmt.Println("You're obese") 
    }    
    // Your weight is normal

    fmt.Println("\n%--------LOOP--------------%\n")

    for i := 0; i < 10; i++ {
        fmt.Printf("%d ", i)
    }
    // 0 1 2 3 4 5 6 7 8 9 

    fmt.Printf("\n")
    ix := 2
    for ;ix <= 10; ix += 2 {
        fmt.Printf("%d ", ix)
    }        
    // 2 4 6 8 10 

    fmt.Printf("\n")
    iy := 2
    for ;iy <= 20; {
        fmt.Printf("%d ", iy)
        iy *= 2
    }    
    // 2 4 8 16

    fmt.Printf("\n")
    iz := 2
    for iz <= 20 {
        fmt.Printf("%d ", iz)
        iz *= 2
    }
    // 2 4 8 16

    fmt.Printf("\n")
    for num := 1; num <= 100; num++ {
        if num%3 == 0 && num%5 == 0 {
            fmt.Printf("First positive number divisible by both 3 and 5 is %d\n", num)
            break
        }
    }
    // First positive number divisible by both 3 and 5 is 15

    fmt.Printf("\n")
    for num := 1; num <= 10; num++ {
        if num%2 == 0 {
            continue;
        }
        fmt.Printf("%d ", num)
    }
    // 1 3 5 7 9

    fmt.Printf("\n")
    nu := 5
    for i := 0; i < nu; i++ {
        for j := 0; j <= i; j++ {
            fmt.Print("*")
        }
        fmt.Println()
    }    
    // *
    // **
    // ***
    // ****
    // *****

    fmt.Println("\n%----------SLICE------------%\n") 
    var numbers []int
    fmt.Println(numbers)
    // []
    
    numbers = make([]int,5,5)
    fmt.Println(numbers)
    // [0 0 0 0 0]

    var s = []int{3, 5, 7, 9, 11, 13, 17}
    t := []int{2, 4, 8, 16, 32, 64}

    fmt.Println("s = ", s)
    fmt.Println("t = ", t)    
    // s =  [3 5 7 9 11 13 17]
    // t =  [2 4 8 16 32 64]

    var a8 = [5]string{"Alpha", "Beta", "Gamma", "Delta", "Epsilon"}
    var s4 []string = a8[1:4]

    fmt.Println("Array a8 = ", a8)
    // Array a8 =  [Alpha Beta Gamma Delta Epsilon]

    fmt.Println("Slice s4 = ", s4)
    // Slice s4 =  [Beta Gamma Delta]

    a9 := [5]string{"C", "C++", "Java", "Python", "Go"}

    slice1 := a9[1:4]
    slice2 := a9[:3]
    slice3 := a9[2:]
    slice4 := a9[:]    

    fmt.Println("Array a9 = ", a9)
    fmt.Println("slice1 = ", slice1)
    fmt.Println("slice2 = ", slice2)
    fmt.Println("slice3 = ", slice3)
    fmt.Println("slice4 = ", slice4)    

    // Array a9 =  [C C++ Java Python Go]
    // slice1 =  [C++ Java Python]
    // slice2 =  [C C++ Java]
    // slice3 =  [Java Python Go]
    // slice4 =  [C C++ Java Python Go]

    cities := []string{"New York", 
    "London", 
    "Chicago", 
    "Beijing", 
    "Delhi", 
    "Mumbai", 
    "Bangalore", 
    "Hyderabad", 
    "Hong Kong"}

    asianCities := cities[3:]
    indianCities := asianCities[1:5]
 
    fmt.Println("cities = ", cities)
    fmt.Println("asianCities = ", asianCities)
    fmt.Println("indianCities = ", indianCities)   
    // cities =  [New York London Chicago Beijing Delhi Mumbai Bangalore Hyderabad Hong Kong]
    // asianCities =  [Beijing Delhi Mumbai Bangalore Hyderabad Hong Kong]
    // indianCities =  [Delhi Mumbai Bangalore Hyderabad] 

    a10 := [7]string{"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"}

    slice1 = a10[1:]
    slice2 = a10[3:]

    fmt.Println("------- Before editing -------")
    fmt.Println("a  = ", a10)
    fmt.Println("slice1 = ", slice1)
    fmt.Println("slice2 = ", slice2)

    // a  =  [Mon Tue Wed Thu Fri Sat Sun]
    // slice1 =  [Tue Wed Thu Fri Sat Sun]
    // slice2 =  [Thu Fri Sat Sun]

    slice1[0] = "TUE"
    slice1[1] = "WED"
    slice1[2] = "THU"

    slice2[1] = "FRIDAY"

    fmt.Println("------- After editing -------")
    fmt.Println("a  = ", a10)
    fmt.Println("slice1 = ", slice1)
    fmt.Println("slice2 = ", slice2)    

    // a  =  [Mon TUE WED THU FRIDAY Sat Sun]
    // slice1 =  [TUE WED THU FRIDAY Sat Sun]
    // slice2 =  [THU FRIDAY Sat Sun]

    a11 := [6]int{10, 20, 30, 40, 50, 60}
    s11 := a11[1:4]
 
    fmt.Printf("s = %v, len = %d, cap = %d\n", s11, len(s11), cap(s11))    
    // s = [20 30 40], len = 3, cap = 5

    s12 := []int{10, 20, 30, 40, 50, 60, 70, 80, 90, 100}
    fmt.Println("Init slide")
    fmt.Printf("s = %v, len = %d, cap = %d\n", s12, len(s12), cap(s12))    
    // s = [10 20 30 40 50 60 70 80 90 100], len = 10, cap = 10

    s12 = s12[1:5]
    fmt.Println("\nCut index from 1 to 5")
    fmt.Printf("s = %v, len = %d, cap = %d\n", s12, len(s12), cap(s12))    
    // s = [20 30 40 50], len = 4, cap = 9

    s12 = s12[:8]
    fmt.Println("\nAfter increasing the length")
    fmt.Printf("s = %v, len = %d, cap = %d\n", s12, len(s12), cap(s12))    
    // s = [20 30 40 50 60 70 80 90], len = 8, cap = 9  

    s12 = s12[2:]
    fmt.Println("\nAfter decresing the first two elements")
    fmt.Printf("s = %v, len = %d, cap = %d\n", s12, len(s12), cap(s12))    
    // s = [40 50 60 70 80 90], len = 6, cap = 7

    s13 := make([]int, 5, 10)
    fmt.Printf("s = %v, len = %d, cap = %d\n", s13, len(s13), cap(s13))    
    // s = [0 0 0 0 0], len = 5, cap = 10

    s14 := make([]int, 5)
    fmt.Printf("s = %v, len = %d, cap = %d\n", s14, len(s14), cap(s14))    
    // s = [0 0 0 0 0], len = 5, cap = 5

    var s15 []int
    fmt.Printf("s = %v, len = %d, cap = %d\n", s15, len(s15), cap(s15))    
    if s15 == nil {
        fmt.Println("s is nil")
    }    
    // s is nil

    src := []string{"Sublime", "VSCode", "IntelliJ", "Eclipse"}
    dest := make([]string, 2) 
    numElementsCopied := copy(dest, src)    

    fmt.Println("src = ", src)
    fmt.Println("dest = ", dest)
    fmt.Println("Number of elements copied from src to dest = ", numElementsCopied)    
    // src =  [Sublime VSCode IntelliJ Eclipse]
    // dest =  [Sublime VSCode]
    // Number of elements copied from src to dest =  2

    slice1 = []string{"C", "C++", "Java"}
    slice2 = append(slice1, "Python", "Ruby", "Go")    

    fmt.Printf("slice1 = %v, len = %d, cap = %d\n", slice1, len(slice1), cap(slice1))
    fmt.Printf("slice2 = %v, len = %d, cap = %d\n", slice2, len(slice2), cap(slice2))    
    // slice1 = [C C++ Java], len = 3, cap = 3
    // slice2 = [C C++ Java Python Ruby Go], len = 6, cap = 6

    slice1[0] = "C#"
    fmt.Println("\nslice1 = ", slice1)
    fmt.Println("slice2 = ", slice2)

    // slice1 =  [C# C++ Java]
    // slice2 =  [C C++ Java Python Ruby Go]

    slice1 = make([]string, 3, 10)
    copy(slice1, []string{"C", "C++", "Java"})

    slice2 = append(slice1, "Python", "Ruby", "Go")    

    fmt.Printf("slice1 = %v, len = %d, cap = %d\n", slice1, len(slice1), cap(slice1))
    fmt.Printf("slice2 = %v, len = %d, cap = %d\n", slice2, len(slice2), cap(slice2))    
    
    slice1[0] = "C#"
    fmt.Println("\nslice1 = ", slice1)
    fmt.Println("slice2 = ", slice2)

    // slice1 =  [C# C++ Java]
    // slice2 =  [C# C++ Java Python Ruby Go]

    var s16 []string
    s16 = append(s16, "Cat", "Dog", "Lion", "Tiger")
    fmt.Printf("s = %v, len = %d, cap = %d\n", s16, len(s16), cap(s16))    
    // s = [Cat Dog Lion Tiger], len = 4, cap = 4

    slice1 = []string{"Jack", "John", "Peter"}
    slice2 = []string{"Bill", "Mark", "Steve"}
 
    slice3 = append(slice1, slice2...)
 
    fmt.Println("slice1 = ", slice1)
    fmt.Println("slice2 = ", slice2)
    fmt.Println("After appending slice1 & slice2 = ", slice3)
    // slice1 =  [Jack John Peter]
    // slice2 =  [Bill Mark Steve]
    // After appending slice1 & slice2 =  [Jack John Peter Bill Mark Steve]

    s17 := [][]string{
        {"India", "China"},
        {"USA", "Canada"},
        {"Switzerland", "Germany"},
    }
 
    fmt.Println("Slice s = ", s17)
    fmt.Println("length = ", len(s17))
    fmt.Println("capacity = ", cap(s17))

    // Slice s =  [[India China] [USA Canada] [Switzerland Germany]]
    // length =  3
    // capacity =  3

    fmt.Println("\n%---------CONTROL-------------%\n")  

    var cP *int
    fmt.Println("p = ", cP)
    // p =  <nil>

    var x9 = 100
    var cP1 *int = &x9
    fmt.Println(cP1)
    // 0xc00001a3c8

    var a12 = 5.67
    var cP2 = &a12
 
    fmt.Println("Value stored in variable a = ", a12)
    fmt.Println("Address of variable a = ", &a12)
    fmt.Println("Value stored in variable p = ", cP2)   
    // Value stored in variable a =  5.67
    // Address of variable a =  0xc0000ca320
    // Value stored in variable p =  0xc0000ca320 

    var a13 = 100
    var cP3 = &a13
 
    fmt.Println("a = ", a13)
    fmt.Println("p = ", cP3)
    fmt.Println("*p = ", *cP3)
    // a =  100
    // p =  0xc00001a3f8
    // *p =  100

    var a14 = 1000
    var cP4 = &a14
    fmt.Println("a (before) = ", a14)    
    // a (before) =  1000

    *cP4 = 2000

    fmt.Println("a (after) = ", a14)    
    // a (after) =  2000

    ptr := new(int) // Pointer for type `int`
    *ptr = 100

    fmt.Printf("Ptr = %#x, Ptr value = %d\n", ptr, *ptr)
    // Ptr = 0xc0000b6230, Ptr value = 100

    var a15 = 7.98
    var cP5 = &a15
    var pp = &cP5

    fmt.Println("a = ", a15)
    fmt.Println("address of a = ", &a15)   
    // a =  7.98
    // address of a =  0xc00001a420 

    fmt.Println("p = ", cP5)
    fmt.Println("address of p = ", &cP5)
    // p =  0xc00001a420
    // address of p =  0xc00000e010

    fmt.Println("pp = ", pp)
    // pp =  0xc00000e010

    fmt.Println("*pp = ", *pp)
    fmt.Println("**pp = ", **pp)
    // *pp =  0xc00000e010
    // **pp =  7.98

    var a16 = 75
    var p1 = &a16
    var p2 = &a16

    if p1 == p2 {
        fmt.Println("Both pointers p1 and p2 point to the same variable.")
    }
    // Both pointers p1 and p2 point to the same variable.


    fmt.Println("\n%---------STRUCTURE-------------%\n")    

    type Person struct {
      FirstName, LastName string
      Age  int
    }    

    var person Person
    fmt.Println(person)
    // {  0}

    person = Person{"Rajeev", "Singh", 26}
    fmt.Println(person)
    // {Rajeev Singh 26}
    
    person2 := Person{
        FirstName: "John",
        LastName:  "Snow",
        Age:       45,
    }
    fmt.Println(person2)
    // {John Snow 45}

    person3 := Person{FirstName: "Robert"}
    fmt.Println("Person3: ", person3)    
    // Person3:  {Robert  0}

    type Car struct {
        Name, Model, Color string
        WeightInKg         float64
    }

    car := Car {
        Name:       "Ferrari",
        Model:      "GTC4",
        Color:      "Red",
        WeightInKg: 1920,
    }

    fmt.Println("Car Name: ", car.Name)
    fmt.Println("Car Color: ", car.Color)
    // Car Name:  Ferrari
    // Car Color:  Red

    type Student struct {
        RollNumber int
        Name       string
    }    

    student := Student{11, "Jack"}

    ps := &student
    fmt.Println(ps)
    // &{11 Jack}

    fmt.Println((*ps).Name)
    fmt.Println(ps.Name)
    // Jack
    // Jack    

    ps.RollNumber = 31
    fmt.Println(ps)
    // &{31 Jack}

    type Employee struct {
        Id   int
        Name string
    }

    pEmp := new(Employee)
    pEmp.Id = 1000
    pEmp.Name = "Sachin"
 
    fmt.Println(pEmp)    
    // &{1000 Sachin}

    type Point struct {
        X float64
        Y float64
    }    

    point1 := Point{10, 20}
    point2 := point1

    fmt.Println("p1 = ", point1)
    fmt.Println("p2 = ", point2)
    // p1 =  {10 20}
    // p2 =  {10 20}

    point2.X = 15
    fmt.Println("\nAfter modifying p2:")
    fmt.Println("p1 = ", point1)
    fmt.Println("p2 = ", point2)   
    // p1 =  {10 20}
    // p2 =  {15 20} 

    point3 := Point{3.4, 5.2}
    point4 := Point{3.4, 5.2}

    if point3 == point4 {
        fmt.Println("Point point3 and point4 are equal.")
    } else {
        fmt.Println("Point point3 and point4 are not equal.")
    }    
    // Point point3 and point4 are equal.    

    fmt.Println("\n%----------INTERFACE------------%\n")     

    var sCircle Shape = Circle{5.0}
    fmt.Printf("Shape Type = %T, Shape Value = %v\n", sCircle, sCircle)
    fmt.Printf("Area = %f, Perimeter = %f\n\n", sCircle.Area(), sCircle.Perimeter())    
    // Shape Type = main.Circle, Shape Value = {5}
    // Area = 78.539816, Perimeter = 31.41592

    var s1 Shape = Rectangle{4.0, 6.0}
    fmt.Printf("Shape Type = %T, Shape Value = %v\n", s1, s1)
    fmt.Printf("Area = %f, Perimeter = %f\n", s1.Area(), s1.Perimeter())    
    // Shape Type = main.Rectangle, Shape Value = {4 6}
    // Area = 24.000000, Perimeter = 20.000000

    totalArea := CalculateTotalArea(
        Circle{2}, 
        Rectangle{4, 5}, 
        Circle{10})
    fmt.Println("Total area = ", totalArea)
    // Total area =  346.7256359733385

    drawing := MyDrawing {
        shapes: []Shape{
            Circle{2},
            Rectangle{3, 5},
            Rectangle{4, 7},
        },
        bgColor: "red",
        fgColor: "white",}
    fmt.Println("Drawing", drawing)
    fmt.Println("Drawing Area = ", drawing.Area())
    // Drawing {[{2} {3 5} {4 7}] red white}
    // Drawing Area =  55.56637061435917

    fmt.Printf("(%v, %T)\n", sCircle, sCircle)
    fmt.Printf("Shape area = %v\n", sCircle.Area())    
    // ({5}, main.Circle)
    // Shape area = 78.53981633974483

    var sRect = Rectangle{4, 7}
    fmt.Printf("(%v, %T)\n", sRect, sRect)
    fmt.Printf("Shape area = %v\n", sRect.Area())  
    // ({4 7}, main.Rectangle)
    // Shape area = 28  

    fmt.Println("\n%----------METHOD------------%\n")      

    point5 := NewPoint{2.0, 4.0}
    fmt.Println("Point : ", point5)
    fmt.Println("Is Point p located above the line y = 1.0 ? : ", point5.IsAbove(1))
    // Point :  {2 4}
    // Is Point p located above the line y = 1.0 ? :  true

    point6 := NewPoint{2.5, -3.0}
    fmt.Println("Point : ", point6)
    fmt.Println("Is Point p located above the line y = 1.0 ? : ", IsAboveFunc(point6, 1))
    // Point :  {2.5 -3}
    // Is Point p located above the line y = 1.0 ? :  false

    ap := ArithmeticProgression{1, 2} // AP: 1 3 5 7 9 ...
    gp := GeometricProgression{1, 2}  // GP: 1 2 4 8 16 ...
 
    fmt.Println("5th Term of the Arithmetic series = ", ap.NthTerm(5))
    fmt.Println("5th Term of the Geometric series = ", gp.NthTerm(5))    
    // 5th Term of the Arithmetic series =  9
    // 5th Term of the Geometric series =  16

    point7 := NewPoint{3, 4}
    fmt.Println("Point p = ", point7)
    // Point p =  {3 4}
 
    point7.Translate(7, 9)
    fmt.Println("After Translate, p = ", point7)    
    // After Translate, p =  {10 13}

    point7 = NewPoint{3, 4}    
    TranslateFunc(&point7, 7, 9)
    fmt.Println("After Translate, p = ", point7)
    // After Translate, p =  {10 13}

    ptr7 := &point7
    fmt.Println("Point p = ", point7)    

    point7.Translate(2, 6) // Correct
    ptr7.Translate(5, 10) // Correct
    TranslateFunc(ptr7, 20, 30)  // Correct
    // Error: TranslateFunc(p, 20, 30)

    point8 := NewPoint{0, 4}
    ptr8 := &point8    

    fmt.Println("Point p = ", point8)

    point8.IsAbove(1) // Correct
    ptr8.IsAbove(1) // Correct
    IsAboveFunc(point8, 1) // Correct
    // Error: IsAboveFunc(ptr8, 1)   

    myStr := MyString("OLLEH")
    fmt.Println(myStr.reverse())
    // HELLO

    fmt.Println("\n%----------MAP------------%\n")

    var mp map[string]int
    fmt.Println(mp)
    // map[]

    if mp == nil {
        fmt.Println("mp is nil")
    }    
    // mp is nil

    // Error: // m["one hundred"] = 100
    // panic: assignment to entry in nil map

    var map1 = make(map[string]int)
    fmt.Println(map1)
    // map[]

    if map1 == nil {
        fmt.Println("map1 is nil")
    } else {
        fmt.Println("map1 is not nil")
    }    
    // map1 is not nil

    map1["one hundred"] = 100
    fmt.Println(map1)
    // map1[one hundred:100]

    var map2 = map[string]int{
        "one":   1,
        "two":   2,
        "three": 3,
        "four":  4,
        "five":  5,
    }    
    fmt.Println(map2)
    // map[five:5 four:4 one:1 three:3 two:2]

    var map3 = map[string]int{}
    fmt.Println(map3)
    // map[]

    var tinderMatch = make(map[string]string)

    tinderMatch["Rajeev"] = "Angelina"
    tinderMatch["James"] = "Sophia"
    tinderMatch["David"] = "Emma"

    fmt.Println(tinderMatch)
    // map[David:Emma James:Sophia Rajeev:Angelina]

    tinderMatch["Rajeev"] = "Jennifer"
    fmt.Println(tinderMatch)
    // map[David:Emma James:Sophia Rajeev:Jennifer

    var personMobileNo = map[string]string{
        "John":  "+33-8273658526",
        "Steve": "+1-8579822345",
        "David": "+44-9462834443",
    }

    var mobileNo = personMobileNo["Steve"]
    fmt.Println("Steve's Mobile No : ", mobileNo)
    // Steve's Mobile No :  +1-8579822345

    mobileNo = personMobileNo["Jack"]
    fmt.Println("Jack's Mobile No : ", mobileNo) 
    // Jack's Mobile No :  

    var employees = map[int]string{
        1001: "Rajeev",
        1002: "Sachin",
        1003: "James",
    }

    name, ok := employees[1001] 
    fmt.Printf("name : %s, ok : %t\n", name, ok)
    // name : Rajeev, ok : true

    name2, ok2 := employees[1010] // "", false
    fmt.Printf("name : %s, ok : %t\n", name2, ok2)
    // name : , ok : false
    
    _, ok3 := employees[1005]
    fmt.Printf("ok : %t\n", ok3)
    // ok : false

    employees = map[int]string{
        1001: "John",
        1002: "Steve",
        1003: "Maria",
    }    

    printEmployee(employees, 1001)
    // name = John, ok = true

    printEmployee(employees, 1010)
    // EmployeeId 1010 not found

    if isEmployeeExists(employees, 1002) {
        fmt.Println("EmployeeId 1002 found")
    }
    // EmployeeId 1002 found

    var fileExtensions = map[string]string{
        "Python": ".py",
        "C++":    ".cpp",
        "Java":   ".java",
        "Golang": ".go",
        "Kotlin": ".kt",
    }
    fmt.Println(fileExtensions)
    // map[C++:.cpp Golang:.go Java:.java Kotlin:.kt Python:.py]

    delete(fileExtensions, "Kotlin")
    delete(fileExtensions, "Javascript")

    fmt.Println(fileExtensions)    
    // map[C++:.cpp Golang:.go Java:.java Python:.py]

    var m1 = map[string]int{
        "one":   1,
        "two":   2,
        "three": 3,
        "four":  4,
        "five":  5,
    }    

    var m2 = m1
    fmt.Println("m1 = ", m1)
    fmt.Println("m2 = ", m2)    
    // m1 =  map[five:5 four:4 one:1 three:3 two:2]
    // m2 =  map[five:5 four:4 one:1 three:3 two:2]

    m2["ten"] = 10
    fmt.Println("\nm1 = ", m1)
    fmt.Println("m2 = ", m2)
    // m1 =  map[five:5 four:4 one:1 ten:10 three:3 two:2]
    // m2 =  map[five:5 four:4 one:1 ten:10 three:3 two:2   
    
    var personAge = map[string]int{
        "Rajeev": 25,
        "James":  32,
        "Sarah":  29,
    }
 
    for name, age := range personAge {
        fmt.Println(name, age)
    }
    // Rajeev 25
    // James 32
    // Sarah 29
    fmt.Println("\n%----------ERROR------------%\n") 

    /**
        val, err := myFunction( args... );
        if err != nil {
          // Error handling
        } else {
          // Success
        }    

        type error interface {
          Error() string
        }        
    **/

    myErr := &MyError{}
    fmt.Println(myErr)
    // Something unexpected happend!

    myErr2 := errors.New("Something unexpected happend!")
    fmt.Println(myErr2)
    // Something unexpected happend!

    fmt.Printf("Type of myErr2 is %T \n", myErr2)
    fmt.Printf("Value of myErr2 is %#v \n", myErr2)    
    // Type of myErr2 is *errors.errorString 
    // Value of myErr2 is &errors.errorString{s:"Something unexpected happend!"} 

    /**
        type errorString struct {
            s string
        }
         
        func (e *errorString) Error() string {
            return e.s
        }

        func New(text string) error {
            return &errorString{text}
        }        
    **/

    /**

        // ERROR FILE OPENING

        type PathError struct {  
            Op   string
            Path string
            Err  error
        }        

        func (e *PathError) Error() string { 
            return e.Op + " " + e.Path + ": " + e.Err.Error() 
        }

    **/

    file, err3 := os.Open("test.txt")
    if err3, ok3 := err3.(*os.PathError); ok3 {
        fmt.Println("File at path", err3.Path, "failed to open")
    } else{
        fmt.Println(file.Name(), "opened successfully")    
    } 
    // File at path test.txt failed to open

    /**

        type DNSError struct {  
            ...
        }
         
        func (e *DNSError) Error() string {  
            ...
        }
        func (e *DNSError) Timeout() bool {  
            ... 
        }
        func (e *DNSError) Temporary() bool {  
            ... 
        }    

    **/

    addr, err := net.LookupHost("vngeeks123.com")
    if err, ok := err.(*net.DNSError); ok {
        if err.Timeout() {
            fmt.Println("operation timed out")
        } else if err.Temporary() {
            fmt.Println("temporary error")
        } else {
            fmt.Println("generic error: ", err)
        }
        // return
    }
    fmt.Println(addr)   
    // generic error:  lookup vngeeks123.com: no such host
    // [] 

    fmt.Println("\n%----------------------%\n")        
}

func avg(x float64, y float64) float64 {
    return (x + y) / 2
} 
// Same as func avg(x, y float64) float64 { }

func getStockPriceChange(prevPrice, currentPrice float64) (float64, float64) {
    change := currentPrice - prevPrice
    percentChange := (change / prevPrice) * 100
    return change, percentChange
}

func getStockPriceChangeWithError(prevPrice, currentPrice float64) (float64, float64, error) {
    if prevPrice == 0 {
        err := errors.New("Previous price cannot be zero")
        return 0, 0, err
    }
    change := currentPrice - prevPrice
    percentChange := (change / prevPrice) * 100
    return change, percentChange, nil
}

func getNamedStockPriceChange(prevPrice, currentPrice float64) (change, percentChange float64) {
    change = currentPrice - prevPrice
    percentChange = (change / prevPrice) * 100
    return
}

type Shape interface {
    Area() float64
    Perimeter() float64
}

type Rectangle struct {
    Length, Width float64
}

func (r Rectangle) Area() float64 {
    return r.Length * r.Width
}
 
func (r Rectangle) Perimeter() float64 {
    return 2 * (r.Length + r.Width)
}    

type Circle struct {
    Radius float64
}
 
func (c Circle) Area() float64 {
    return math.Pi * c.Radius * c.Radius
}
 
func (c Circle) Perimeter() float64 {
    return 2 * math.Pi * c.Radius
}
 
func (c Circle) Diameter() float64 {
    return 2 * c.Radius
}    

func CalculateTotalArea(shapes ...Shape) float64 {
    totalArea := 0.0
    for _, s := range shapes {
        totalArea += s.Area()
    }
    return totalArea
}

type MyDrawing struct {
    shapes  []Shape
    bgColor string
    fgColor string
}

func (drawing MyDrawing) Area() float64 {
    totalArea := 0.0
    for _, s := range drawing.shapes {
        totalArea += s.Area()
    }
    return totalArea        
}

type NewPoint struct {
    X, Y float64
}

/** METHODS
func (recevier Type) Method_Name(Parameters) (Return type) {
}
**/
// Method with receiver `Point`
func (p NewPoint) IsAbove(y float64) bool {
    return p.Y > y
}

func IsAboveFunc(p NewPoint, y float64) bool {
    return p.Y > y
}

type ArithmeticProgression struct {
    A, D float64
}
 
func (ap ArithmeticProgression) NthTerm(n int) float64 {
    return ap.A + float64(n-1)*ap.D
}
 
type GeometricProgression struct {
    A, R float64
}
 
func (gp GeometricProgression) NthTerm(n int) float64 {
    return gp.A * math.Pow(gp.R, float64(n-1))
}

func (p *NewPoint) Translate(dx, dy float64) {
    p.X = p.X + dx
    p.Y = p.Y + dy
}

func TranslateFunc(p *NewPoint, dx, dy float64) {
    p.X = p.X + dx
    p.Y = p.Y + dy
}

/**
Error: cannot define new methods 
on non-local type model.Person

func (p model.Person) getFullName() string {
    return p.FirstName + " " + p.LastName
}

**/

type MyString string
 
func (myStr MyString) reverse() string {
    s := string(myStr)
    runes := []rune(s)
 
    for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
        runes[i], runes[j] = runes[j], runes[i]
    }
    return string(runes)
}

func printEmployee(employees map[int]string, employeeId int) {
    if name, ok := employees[employeeId]; ok {
        fmt.Printf("name = %s, ok = %v\n", name, ok)
    } else {
        fmt.Printf("EmployeeId %d not found\n", employeeId)
    }
}

func isEmployeeExists(employees map[int]string, employeeId int) bool {
    _, ok := employees[employeeId]
    return ok
}

type MyError struct{}

func (myErr *MyError) Error() string {
    return "Something unexpected happend!"
}



