package main
 
import (
	"fmt"
	"example/numbers"
	"example/strings"	
	"example/strings/greeting" // Importing a nested package
	str "strings"	// Package Alias
)
 
func main() {
/******************************
Build: install the package into bin directory
cd $GOPATH/src/example/myapp && \ 
go install && \
cd $GOPATH/bin && \
./myapp 
*******************************/
	fmt.Println(numbers.IsPrime(19))
	// false
 
	fmt.Println(greeting.WelcomeText)
	// Hello, World to Golang
 
	fmt.Println(strings.Reverse("callicoder"))
	// redocillac
 
	fmt.Println(str.Count("Go is Awesome. I love Go", "Go"))
	// 2
/******************************
Build: install package
go get -u github.com/jinzhu/gorm
import (
    "github.com/callicoder/example/strings/greeting"
)
*******************************/	
}