package model
 
// struct không được (chỉ truy cập bên trong package `model`)
type address struct {
	houseNo, street, city, state, country string
	zipCode                               int
}