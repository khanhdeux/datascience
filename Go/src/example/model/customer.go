package model
 
type Customer struct {  // kiểu struct được export
	Id int				// trường được export
	Name string			// trường được export
	addr address        // trường không được export (chỉ truy cập bên trong package `model`)
	married bool  		// trường không được export (chỉ truy cập bên trong package `model`)
}