package main
 
import (
	"fmt"
	"example/model"
)
 
func main() {
	c := model.Customer{
		Id: 1, 
		Name: "Rajeev Singh",
	}
 
	// c.married = true	// Error: c.married undefined (cannot refer to unexported field or method married)
 	// a := model.address{} // Error: cannot refer to unexported name model.address
 
	fmt.Println("Programmer = ", c);
	// Programmer =  {1 Rajeev Singh {     0} false}
}