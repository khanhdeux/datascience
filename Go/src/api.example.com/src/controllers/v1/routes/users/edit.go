package users

import (
	"api.example.com/src/controllers/v1/models/users"
	// Users "api.example.com/src/controllers/v1/models/users"
	DB "api.example.com/src/system/db"
	"github.com/gorilla/mux"

	"encoding/json"
	"log"
	"net/http"
	"strconv"
)

func Edit(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id := vars["id"]

	newId,_ := strconv.Atoi(id)

	user := users.User{Id: int64(newId)}

	if err := DB.FindBy(db, &user);err != nil {
		log.Println(err)
		http.Error(w, "Unable to get user", http.StatusInternalServerError)
		return
	}

	user.Password = ""

	packet, err := json.Marshal(user)
	if err != nil {
		log.Println(err)
		http.Error(w, "Unable to parse user", http.StatusUnauthorized)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(packet)
}