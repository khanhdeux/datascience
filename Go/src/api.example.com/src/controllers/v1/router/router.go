package router

import (
	"api.example.com/pkg/types/routes"
	StatusHandler "api.example.com/src/controllers/v1/status"
	UsersHandler "api.example.com/src/controllers/v1/routes/users"
	"github.com/go-xorm/xorm"
	"log"
	"net/http"
)

var db *xorm.Engine

func Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("X-App-Token")
		log.Println("Inside v1 middleware")

		if len(token) < 1 {
			http.Error(w, "Not authorized", http.StatusUnauthorized)
			return
		}
		next.ServeHTTP(w, r)
	})
}

func GetRoutes(Db *xorm.Engine) (SubRoute map[string]routes.SubRoutePackage) {

	db = Db
	StatusHandler.Init(db)
	UsersHandler.Init(db)

	/* ROUTES */
	SubRoute = map[string]routes.SubRoutePackage{
		"/v1": routes.SubRoutePackage {
				Routes: routes.Routes{
					routes.Route{"Status", "GET", "/status", StatusHandler.Index},
					routes.Route{"UsersIndex", "GET", "/users", UsersHandler.Index},
					routes.Route{"UsersStore", "POST", "/users", UsersHandler.Store},
					routes.Route{"UsersEdit", "GET", "/users/{id}/edit", UsersHandler.Edit},
					routes.Route{"UsersUpdate", "PUT", "/users/{id}", UsersHandler.Update},
					routes.Route{"UsersDestroy", "DELETE", "/users/{id}", UsersHandler.Destroy},
				},
				Middleware: Middleware,
		},
	}

	return
}