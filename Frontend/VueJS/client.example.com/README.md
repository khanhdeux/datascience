# client.example.com

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


# Video Reference
https://www.youtube.com/watch?v=YFNUnS1af-s&list=PLTxFJWe_410wQhBrnEhu3XdnXjYreEc-Y