import { createApp } from './main.js'
const { app } = createApp()
// Assume that App.vue element of template has `id="app"`
app.$mount('#app')
