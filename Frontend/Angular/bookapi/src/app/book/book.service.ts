import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Book } from './book';

@Injectable()
export class BookService {

  constructor(private httpService: Http) {

  }

  getAllBooks(): Observable<Book[]> {
    return this.httpService.get('http://localhost:8000/api/book')
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  addBook(book: Book) {
    let body = JSON.stringify(book);
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    if (book.id) {
      return this.httpService.put('http://localhost:8000/api/book/' + book.id, body, options)
    }
    return this.httpService.post('http://localhost:8000/api/book', body, options);
  }

  deleteBook(bookId: string) {
    return this.httpService.delete('http://localhost:8000/api/book/' + bookId);
  }

  getBookById(bookId: string): Observable<Book> {
    return this.httpService.get('http://localhost:8000/api/book/' + bookId)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    return Observable.throw(error);
  }
}
