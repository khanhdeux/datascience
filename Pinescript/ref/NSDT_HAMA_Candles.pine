//@version=4
study("NSDT HAMA Candles", overlay=true)

//The follow gradient code is taken from the Pinecoders Gradient Framework example.
//https://www.tradingview.com/script/hqH4YIFa-Color-Gradient-Framework-PineCoders/
//Pro Advance/Decline Gradient

////////////////////
//GRADIENT AREA
////////////////////
f_c_gradientAdvDecPro(_source, _center, _steps, _c_bearWeak, _c_bearStrong, _c_bullWeak, _c_bullStrong) =>
    var float _qtyAdvDec = 0.
    var float _maxSteps  = max(1, _steps)
    bool  _xUp     = crossover(_source, _center)
    bool  _xDn     = crossunder(_source, _center)
    float _chg     = change(_source)
    bool  _up      = _chg > 0
    bool  _dn      = _chg < 0
    bool  _srcBull = _source > _center
    bool  _srcBear = _source < _center
    _qtyAdvDec := 
      _srcBull ? _xUp ? 1 : _up ? min(_maxSteps, _qtyAdvDec + 1) : _dn ? max(1, _qtyAdvDec - 1) : _qtyAdvDec :
      _srcBear ? _xDn ? 1 : _dn ? min(_maxSteps, _qtyAdvDec + 1) : _up ? max(1, _qtyAdvDec - 1) : _qtyAdvDec : _qtyAdvDec
    var color _return = na
    _return := 
      _srcBull ? color.from_gradient(_qtyAdvDec, 1, _maxSteps, _c_bullWeak, _c_bullStrong) : 
      _srcBear ? color.from_gradient(_qtyAdvDec, 1, _maxSteps, _c_bearWeak, _c_bearStrong) : _return

//MA TYPES
mat(source, length, type) =>
    type == "SMA" ? sma(source, length) : 
     type == "EMA" ? ema(source, length) :
     type == "RMA" ? rma(source, length) : 
     type == "WMA" ? wma(source, length) :
     type == "VWMA" ? vwma(source, length) :
     type == "HMA" ? hma(source, length) :
     type == "TMA" ? sma(sma(source, length), length) :
     na

//INPUTS
bull = input(color.green, title = "Bull Color")
bear = input(color.red, title = "Bear Color")
neutral = input(color.yellow, title = "Neutral Color")
show_ma   = true
ma_type   = "WMA"
ma_source = close
ma_length = 55
UseGradient = input(true, title = "Use Gradient Colors")
stepn = input(5, title = "Max Gradient Steps")
ma = mat(ma_source, ma_length, ma_type)
col = f_c_gradientAdvDecPro(ma, ema(ma,3), stepn , neutral, bear, neutral, bull) 
////////////////////
//END GRADIENT AREA
////////////////////


//MA INFO
WickColor = input(color.rgb(80,80,80,100), title = "Wick Color", tooltip="Suggest Full Transparency.")
OpenLength = input(25, minval=1, title="Length Open", inline="Open")
OpenType = input(type=input.string, defval='EMA', title='Type', options=['EMA', 'SMA', 'WMA'], inline="Open")
HighLength = input(20, minval=1, title="Length High", inline="High")
HighType = input(type=input.string, defval='EMA', title='Type', options=['EMA', 'SMA', 'WMA'], inline="High")
LowLength = input(20, minval=1, title="Length Low", inline="Low")
LowType = input(type=input.string, defval='EMA', title='Type', options=['EMA', 'SMA', 'WMA'], inline="Low")
CloseLength = input(20, minval=1, title="Length Close", inline="Close")
CloseType = input(type=input.string, defval='EMA', title='Type', options=['EMA', 'SMA', 'WMA'], inline="Close")
LengthMA = input(100, minval=1, title="MA Line Length", inline="MA Info")
MAType = input(defval='EMA', type=input.string, title='MA Line Type', options=['EMA', 'SMA', 'WMA'], inline="MA Info")
MASource = input(close, type=input.source, title="MA Source")

TypeOpen = OpenType
SourceOpen = (open[1] + close[1])/2
LengthOpen = OpenLength

TypeHigh = HighType
SourceHigh = max(high, close)
LengthHigh = HighLength

TypeLow = LowType
SourceLow = min(low, close)
LengthLow = LowLength

TypeClose = CloseType
SourceClose = (open + high + low + close)/4
LengthClose = CloseLength

funcCalcMA1(type1, src1, len1) =>
    return_1 = type1 == 'EMA' ? ema(src1, len1) : type1 == 'SMA' ? sma(src1, len1) : type1 == 'WMA' ? wma(src1, len1) : na
    return_1
funcCalcOpen(TypeOpen, SourceOpen, LengthOpen) =>
    return_2 = TypeOpen == 'EMA' ? ema(SourceOpen, LengthOpen) : TypeOpen == 'SMA' ? sma(SourceOpen, LengthOpen) : TypeOpen == 'WMA' ? wma(SourceOpen, LengthOpen) : na
    return_2
funcCalcHigh(TypeHigh, SourceHigh, LengthHigh) =>
    return_3 = TypeHigh == 'EMA' ? ema(SourceHigh, LengthHigh) : TypeHigh == 'SMA' ? sma(SourceHigh, LengthHigh) : TypeHigh == 'WMA' ? wma(SourceHigh, LengthHigh) : na
    return_3
funcCalcLow(TypeLow, SourceLow, LengthLow) =>
    return_4 = TypeLow == 'EMA' ? ema(SourceLow, LengthLow) : TypeLow == 'SMA' ? sma(SourceLow, LengthLow) : TypeLow == 'WMA' ? wma(SourceLow, LengthLow) : na
    return_4
funcCalcClose(TypeClose, SourceClose, LengthClose) =>
    return_5 = TypeClose == 'EMA' ? ema(SourceClose, LengthClose) : TypeClose == 'SMA' ? sma(SourceClose, LengthClose) : TypeClose == 'WMA' ? wma(SourceClose, LengthClose) : na
    return_5

MA1 = (funcCalcMA1(MAType, MASource, LengthMA))

CandleOpen = funcCalcOpen(TypeOpen, SourceOpen, LengthOpen)
CandleHigh = funcCalcHigh(TypeHigh, SourceHigh, LengthHigh)
CandleLow = funcCalcLow(TypeLow, SourceLow, LengthLow)
CandleClose = funcCalcClose(TypeClose, SourceClose, LengthClose)

//PLOT CANDLES
BodyColor = CandleOpen > CandleOpen[1] ? color.green : color.red
barcolor(UseGradient?col:BodyColor)
plotcandle(CandleOpen, CandleHigh, CandleLow, CandleClose, color=UseGradient?col:BodyColor, title="HAMA Candles", wickcolor=WickColor, bordercolor=na)
plot(MA1, title="MA Line", color=UseGradient?col:BodyColor,style=plot.style_line,linewidth=2)

//ALERTS
// Debug tools

plotshape(col == color.green ? 1 : col == color.red ? -1 : 0, title="Col")
plotshape(rising(MA1,2), title="MA Rising")
plotshape(falling(MA1,2), title="MA Falling")
plotshape(crossover(high, MA1), title="High Crossing MA")
plotshape(crossunder(low, MA1), title="Low Crossing MA")

BUY = col == color.green and rising(MA1,2) and low > CandleHigh
SELL = col == color.red and falling(MA1,2) and CandleLow > high
signal = BUY ? 1 : SELL ? -1 : 0
plotshape(signal, title="Buy-Sell")

//WATERMARK
Watermark = table.new(position.bottom_left, 1, 4, border_width = 3)
table.cell(Watermark, 0, 0, text="North Star Day Trading - NSDT HAMA Candles", text_color=color.new(color.white, 95), text_size=size.huge)