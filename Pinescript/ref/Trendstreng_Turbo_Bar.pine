// This source code is subject to the terms of the Mozilla Public License 2.0 at https://mozilla.org/MPL/2.0/
// © Beardy_Fred

//@version=4
study(title="TrendStrength Turbo", shorttitle= "Trend", overlay = true)

//INPUTS
SourceInput = input(close, "Source")
MA_L1 = input(5, "EMA Length #1")
MA_L2 = input(8, "EMA Length #2")
MA_L3 = input(13, "EMA Length #3")
MA_L4 = input(21, "EMA Length #4")
MA_L5 = input(34, "EMA Length #5")
Vol_L = input(20, "Volume Length")
Vol_T = input(50, "Volume Spike Trigger %")

//STACKED EMAs
MA1 = ema(SourceInput, MA_L1)
MA2 = ema(SourceInput, MA_L2)
MA3 = ema(SourceInput, MA_L3)
MA4 = ema(SourceInput, MA_L4)
MA5 = ema(SourceInput, MA_L5)
MA_Stack_Up = (MA1 > MA2) and (MA2 > MA3) and (MA3 > MA4) and (MA4 > MA5)

//CONDITIONS
Uptrend = MA_Stack_Up 
Reversal = ((MA1 < MA2) and (MA2 > MA3)) or ((MA1 > MA2) and (MA2 < MA3))

//COLOR CODING
EMA_Color = Uptrend ? color.new(color.green, 0) : Reversal ? color.new(color.yellow, 0) : color.new(color.red, 0)
barcolor(color=EMA_Color)

//VOLUME SPIKE
Vol_Spike = sma(volume, Vol_L)
Vol_Trigger = (1 + (Vol_T/100)) * Vol_Spike
plotchar(volume > Vol_Trigger, "Volume Spike", char = '◉', location = location.bottom, color = color.aqua, size = size.tiny)