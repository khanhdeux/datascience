//@version=5
strategy(title='Khanhdeux Strategy', shorttitle='Khanhdeux Strategy', initial_capital=10000, default_qty_value = 100 * 10, default_qty_type = strategy.percent_of_equity, currency = currency.EUR, overlay=true, max_bars_back = 1000, calc_on_every_tick=true, calc_on_order_fills=false)

import khanhdeux88/KhanhdeuxLibrary/21 as lib

// ================================== //
// Input variables                    //
// ================================== //
entry_distance = 1

start_time = input.time(timestamp("1 Jan 2021"), title="Start Date", group="Backtest Time Period")
end_time = input.time(timestamp("31 Dec 2023"), title="End Date", group="Backtest Time Period")

is_table_shown = input.bool(true, "Show calculation",   group="Settings")

// ================================== //
// Global functions                   //
// ================================== //
f_date_filter = time >= start_time and time <= end_time

// ================================== //
// Settings functions                 //
// ================================== //
int ks_length = input.int(42, step = 1, group="f_keltner_sma")

float br_threshold = input.float(0.08, step = 0.01, group="f_breakout_reverse")


[long_stop, short_stop] = lib.f_get_stoploss_pivot()
[long_profit, short_profit] = lib.f_get_takeprofit_vpoc()


// ***********************************************************************************************************************
// global variables
long_entry = false
short_entry = false

// variable values available across candles
entry_price = close
var sl_price = 0.0
var exit_price = 0.0
var candle_count = 0       

// ***********************************************************************************************************************
// strategy
f_strategy_signal  = lib.f_breakout_reverse(br_threshold)//f_vpoc_strategy(distance_threshold) //lib.f_keltner_sma(ks_length)
// f_strategy_signal  =  (f_rsi_adx_ema() == 1 and f_stc() == 1) ? 1 : (f_rsi_adx_ema() == -1 and f_stc() == -1) ? -1 : 0
// Debug
plotshape(f_strategy_signal, title="f_strategy_signal")
// ***********************************************************************************************************************
// entry conditions 
long_entry := f_strategy_signal == 1
short_entry := f_strategy_signal == -1

// keep candle count since the alert generated so that order can be cancelled after N number of candle calling it out as invalid alert
candle_count += 1
if long_entry or short_entry
    candle_count := 0
    candle_count

// ***********************************************************************************************************************
// entry
if long_entry and candle_count < entry_distance and strategy.position_size == 0 and f_date_filter // and f_in_session(custom_session)
    exit_price := long_profit
    sl_price := long_stop

    strategy.entry('BUY', strategy.long, stop=entry_price, comment='BUY @ ' + str.tostring(entry_price, format.mintick))

if short_entry and candle_count < entry_distance and strategy.position_size == 0 and f_date_filter // and f_in_session(custom_session)
    exit_price := short_profit
    sl_price := short_stop

    strategy.entry('SELL', strategy.short, stop=entry_price, comment='SELL @ ' + str.tostring(entry_price, format.mintick))


// ***********************************************************************************************************************
// exit

// close if sl hit
if strategy.position_size > 0 and low <= sl_price
    strategy.close('BUY', qty=strategy.position_size, comment='SL @ ' + str.tostring(sl_price, format.mintick), immediately = true)
if strategy.position_size < 0 and high >= sl_price    
    strategy.close('SELL', qty=strategy.position_size, comment='SL @ ' + str.tostring(sl_price, format.mintick), immediately = true)

// close if tp hit
if strategy.position_size > 0 and high >= exit_price
    strategy.close('BUY', qty=strategy.position_size, comment='TP @ ' + str.tostring(exit_price, format.mintick), immediately = true)
if strategy.position_size < 0 and low <= exit_price    
    strategy.close('SELL', qty=strategy.position_size, comment='TP @ ' + str.tostring(exit_price, format.mintick), immediately = true)

// close if end of the day
if session.islastbar
    strategy.close_all(comment = 'EXIT @ ' + str.tostring(close, format.mintick), immediately = true)
// ***********************************************************************************************************************
// plots    
// ***********************************************************************************************************************

// plot Stoploss / Target
P1 = plot(sl_price > 0 ? sl_price : na, "SL", color = color.purple, style = plot.style_linebr)
P2 = plot(exit_price > 0 ? exit_price : na, "Target", color = color.aqua, style = plot.style_linebr)


// ***********************************************************************************************************************
// MONTHLY TABLE    
// ***********************************************************************************************************************
prec = 2 // input(2, title='Return Precision')

new_month = month(time) != month(time[1])
new_year = year(time) != year(time[1])

eq = strategy.equity

bar_pnl = eq / eq[1] - 1

cur_month_pnl = 0.0
cur_year_pnl = 0.0

// Current Monthly P&L
cur_month_pnl := new_month ? 0.0 : (1 + cur_month_pnl[1]) * (1 + bar_pnl) - 1

// Current Yearly P&L
cur_year_pnl := new_year ? 0.0 : (1 + cur_year_pnl[1]) * (1 + bar_pnl) - 1

// Arrays to store Yearly and Monthly P&Ls
var month_pnl = array.new_float(0)
var month_time = array.new_int(0)

var year_pnl = array.new_float(0)
var year_time = array.new_int(0)

last_computed = false

if not na(cur_month_pnl[1]) and (new_month or barstate.islast)
    if last_computed[1]
        array.pop(month_pnl)
        array.pop(month_time)

    array.push(month_pnl, cur_month_pnl[1])
    array.push(month_time, time[1])

if not na(cur_year_pnl[1]) and (new_year or barstate.islast)
    if last_computed[1]
        array.pop(year_pnl)
        array.pop(year_time)

    array.push(year_pnl, cur_year_pnl[1])
    array.push(year_time, time[1])

last_computed := barstate.islast ? true : nz(last_computed[1])

// Monthly P&L Table    
var monthly_table = table(na)

if barstate.islast and is_table_shown == true
    monthly_table := table.new(position.bottom_right, columns=14, rows=array.size(year_pnl) + 1, border_width=1)

    table.cell(monthly_table, 0, 0, syminfo.tickerid + '_' + timeframe.period, bgcolor=#cccccc)
    table.cell(monthly_table, 1, 0, 'Jan', bgcolor=#cccccc)
    table.cell(monthly_table, 2, 0, 'Feb', bgcolor=#cccccc)
    table.cell(monthly_table, 3, 0, 'Mar', bgcolor=#cccccc)
    table.cell(monthly_table, 4, 0, 'Apr', bgcolor=#cccccc)
    table.cell(monthly_table, 5, 0, 'May', bgcolor=#cccccc)
    table.cell(monthly_table, 6, 0, 'Jun', bgcolor=#cccccc)
    table.cell(monthly_table, 7, 0, 'Jul', bgcolor=#cccccc)
    table.cell(monthly_table, 8, 0, 'Aug', bgcolor=#cccccc)
    table.cell(monthly_table, 9, 0, 'Sep', bgcolor=#cccccc)
    table.cell(monthly_table, 10, 0, 'Oct', bgcolor=#cccccc)
    table.cell(monthly_table, 11, 0, 'Nov', bgcolor=#cccccc)
    table.cell(monthly_table, 12, 0, 'Dec', bgcolor=#cccccc)
    table.cell(monthly_table, 13, 0, 'Year', bgcolor=#999999)


    for yi = 0 to array.size(year_pnl) - 1 by 1
        table.cell(monthly_table, 0, yi + 1, str.tostring(year(array.get(year_time, yi))), bgcolor=#cccccc)

        y_color = array.get(year_pnl, yi) > 0 ? color.new(color.green, transp=50) : color.new(color.red, transp=50)
        table.cell(monthly_table, 13, yi + 1, str.tostring(math.round(array.get(year_pnl, yi) * 100, prec)), bgcolor=y_color)

    for mi = 0 to array.size(month_time) - 1 by 1
        m_row = year(array.get(month_time, mi)) - year(array.get(year_time, 0)) + 1
        m_col = month(array.get(month_time, mi))
        m_color = array.get(month_pnl, mi) > 0 ? color.new(color.green, transp=70) : color.new(color.red, transp=70)

        table.cell(monthly_table, m_col, m_row, str.tostring(math.round(array.get(month_pnl, mi) * 100, prec)), bgcolor=m_color)   