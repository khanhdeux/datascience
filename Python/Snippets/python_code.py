#!/usr/bin/env python3

import argparse

# Create a function with two inputs
def sum_two_values(value_one, value_two):
    
    # Add together two values
    _sum = value_one + value_two
    
    # Return sum
    return _sum


# If the script is run
if __name__ == '__main__':
    
    # Create argument parser
    parser = argparse.ArgumentParser()
    
    # Create an argument called v1 or value_1 that is an integer
    parser.add_argument('-v1', '--value_1', type=int, help='The first value.')
    
    # Create a required argument called v2 or value_2 that is an integer
    parser.add_argument('-v2', '--value_2', type=int, help='The second value.', required=True)
    
    # Parse arguments
    args = parser.parse_args()

    # Assign arguments to variables
    value_one = args.v1
    value_two = args.v2
    
    # Run function with argument variables as inputs
    sum_two_values(value_one, value_two)
