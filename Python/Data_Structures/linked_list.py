'''
An unsorted linked list. Nothing fancy here.
Methods available:
insert(value): insert a value into the linked list
remove(value): remove the first occurrence of the value in the list.
                Raise ValueError if that value doesn't exist
Extra usage:
    value in ll: check if a value is in the list
    list(ll): list all values in the list
'''


class Node(object):
    def __init__(self, value, next=None):
        self.value = value
        self.next = next

    def __str__(self):
        return self.value


class LinkedList(object):
    def __init__(self):
        self._head = None
        self._tail = None
        self._len = 0

    def __len__(self):
        return self._len

    def insert(self, value):
        node = Node(value)

        if not self._tail:
            self._head = self._tail = node
        else:
            self._tail.next = node
            self._tail = node
        self._len += 1

    def remove(self, value):
        node, prev, found = self._find_value(self._head, value)

        if not node:
            raise ValueError()
        if prev:
            prev.next = node.next
        else:
            self._head = node.next

        if not node.next:
            self._tail = prev

        self._len -= 1

    def _find_value(self, curr, value):
        while curr:
            if curr.value == value:
                return curr, None, True

            prev = curr
            curr = curr.next
            return curr, prev, False

    def __iter__(self):
        yield from self._iter(self._head)

    def _iter(self, node):
        if node:
            yield node
            yield from self._iter(node.next)

def test_linkedList():
    ll = LinkedList()
    print(len(ll))

    values = [2, 3, 2, 3, 5, -10]
    for value in values:
        ll.insert(value)

    print("---------------")
    print_linkedList(ll)

    for value in values:
        ll.remove(value)

    print("---------------")
    print_linkedList(ll)

    values = [-100, 23, 3, 2, 1, -10]

    for value in values:
        ll.insert(value)

    print("---------------")
    print_linkedList(ll)

def print_linkedList(ll):
    for node in ll:
        print(f"Node:{node.value}")


test_linkedList()
