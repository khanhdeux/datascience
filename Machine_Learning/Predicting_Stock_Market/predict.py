import pandas as pd
from datetime import datetime 
import numpy as np

# Reading the data
df = pd.read_csv('sphist.csv')
print(df.info())
print(df.head(1))

# Convert date column
df['Date'] = pd.to_datetime(df['Date'])
print(df.info())

# Check if date column is converted
print(df[df['Date'] > datetime(year=2015, month=4, day=1)].shape[0])

# Sort dataframe by date
df.sort_values(by='Date', inplace=True, ascending=True)
print(df.head(6))

# Generating indicators
df['day_5'] = df['Close'].rolling(5).mean()
df['day_5'] = df['day_5'].shift()
df['day_30'] = df['Close'].rolling(30).mean()
df['day_30'] = df['day_30'].shift()
df['day_365'] = df['Close'].rolling(365).mean()
df['day_365'] = df['day_365'].shift()

print(df.head(10))
print(df.loc[16339])

# Remove the rows that fall before 1951-01-03
df = df[df['Date'] > datetime(year=1951, month=1, day=2)]

# Drop rows with NaN values
print(df.isnull().sum())
df = df.dropna(axis=0)
print(df.head(5))

# Split to train data (with date less than 2013-01-01)
# and test data (with date greater than or equal 2013-01-01)
train = df[df['Date'] < datetime(year=2013, month=1, day=1)]
test = df[df['Date'] >= datetime(year=2013, month=1, day=1)]

print(train.shape)
print(test.shape)

# Making prediction
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LinearRegression

def calculate_mse(features):
    lr = LinearRegression()
    lr.fit(train[features], train['Close'])
    return mean_squared_error(test['Close'], lr.predict(test[features]))    
# First indicator (average from last 5 days)
features = ['day_5']
print(calculate_mse(features))

# Single indicator (average from last 30 days)
mse_values = []
for feature in list(['day_5', 'day_30', 'day_365']):   
    mse_values.append(calculate_mse([feature]))

print(mse_values)
    
# Multivariate indicators
features = ['day_5', 'day_30']
print(calculate_mse(features))

features = ['day_5', 'day_30', 'day_365']
print(calculate_mse(features))






