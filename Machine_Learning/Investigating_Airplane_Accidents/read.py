 # Read the data
import math
f = open('AviationData.txt')
aviation_data = f.readlines()
aviation_list = []

for line in aviation_data:
    aviation_list.append(line.split(" | "))

# Find the data by normal Search
# Time complexity O(n^2)    
lax_code = []
for line in aviation_data:
    if 'LAX94LA336' in line:
        lax_code.append(line)
        
print('---------------------')        
print(lax_code)

# Find the data by Binary Search
# Time complexity O(log(n))    
sorted_aviation_list = sorted(aviation_list, key = lambda row: row[2])

def log_search(val):
    start = 0
    end = len(aviation_list) - 1
    index = math.floor((start + end)/2)
    
    current = sorted_aviation_list[index][2]
    
    while (current != val) & (end > start):
        if current > val:
            end = index - 1
        else:
            start = index + 1
        index = math.floor((start + end)/2)
        current = sorted_aviation_list[index][2] 
        if current == val:
            return sorted_aviation_list[index] 
    return None

lax_code = log_search('LAX94LA336')
print('---------------------')        
print(lax_code)    

# Store the data as a list of dictionaries    
aviation_dict_list = []
dict_keys = aviation_data[0].split(" | ")
print(dict_keys)

for row in aviation_data[1:]:
    row_dict = {}
    row = row.split(" | ")
    for i, key in enumerate(dict_keys):
        row_dict[key] = row[i]
    aviation_dict_list.append(row_dict)
    
print('---------------------')    
print(aviation_dict_list[:2])

lax_dict = []
lax_dict = [item for item in aviation_dict_list if item['Accident Number'] == 'LAX94LA336'][0]
print('---------------------')    
print(lax_dict)


# Count how many accidents ocurred in the US
print('---------------------')    
state_accidents = [item for item in aviation_dict_list if item['Country'] == 'United States']
print('Accients in US: {}'
      .format(len(state_accidents)))

print('---------------------')    
print(state_accidents[0])
