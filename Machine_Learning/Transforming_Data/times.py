from dateutil.parser import parse
import read

df = read.load_data()

def hour_finder(string):
    timestamp = parse(string)
    hour = timestamp.hour
    return hour

hours = df['submission_time'].apply(hour_finder)
print(hours.value_counts())