import read
from collections import Counter

df = read.load_data()
h_series = df['headline']
joined = h_series.str.cat(sep=" ").lower()
cnt = Counter(joined.split())

if __name__ == '__main__':
    # Get the first common words in headline
    # columns
    print(cnt.most_common(100))