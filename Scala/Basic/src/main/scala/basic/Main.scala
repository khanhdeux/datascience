package basic

import java.util.concurrent.Executors

import scala.util.control.NonFatal

object Main {
  def main(args: Array[String]): Unit = {
    class StudentTest() {
      var id:Int = 0
      var age:Int = 0

      def studentDetails(i:Int,a:Int) {
        id = i
        age = a
        println("Student Id is :"+id);
        println("Student Age is :"+age);
      }
    }

    val stu = new StudentTest()
    stu.studentDetails(10, 8)
    // Student Id is :10
    // Student Age is :8

    println("%-------------------------------%")

    object ValApp extends App {
      val number = { println("Constant number is initialized."); 99 }
      println("Before Accessing 'number' constant:")
      println(number + 1)
      println(number + 1)
      println(number + 1)
    }

    ValApp.main(null)
    // Constant number is initialized.
    // Before Accessing 'number' constant:
    // 100
    // 100
    // 100

    println("%-------------------------------%")

    object VarApp extends App {
      var number = { println("Variable number is initialized."); 99 }
      println("Before Accessing 'number' variable:")
      println(number + 1)
      println(number + 1)
      println(number + 1)
    }

    VarApp.main(null)
    // Variable number is initialized.
    // Before Accessing 'number' variable:
    // 100
    // 100
    // 100

    println("%-------------------------------%")

    def add(num1:Int, num2:Int): Int = num1 + num2
    println(add(11,22))
    // 33

    println("%-------------------------------%")

    object DefApp extends App {
      def tax = {
        println("Function execution started.")
        1100
      }

      println(tax)
      println(tax)
      println(tax)
    }

    DefApp.main(null)
    // Function execution started.
    // 1100
    // Function execution started.
    // 1100
    // Function execution started.
    // 1100

    println("%-------------------------------%")

    object LazyValApp extends App {
      lazy val number = { println("Constant number is initialized."); 99 }
      println("Before Accessing 'number' constant:")
      println(number + 1)
      println(number + 1)
      println(number + 1)
    }

    LazyValApp.main(null)
    // Before Accessing 'number' constant:
    // Constant number is initialized.
    // 100
    // 100
    // 100

    println("%-------------------------------%")

    class Student {
      var sid:Int = 10
      var sname = "John"
      var marks = 72.5f
      val (sage,city) = (13,"Mysore")

      def studdetails() {
        println("Student Id is:"+sid);
        println("Student Name is:"+sname);
        println("Marks secured is:"+marks);
        println("Student Age is:"+sage);
        println("City is:"+city);
      }
    }

    val st = new Student()
    st.studdetails()
    // Student Id is:10
    // Student Name is:John
    // Marks secured is:72.5
    // Student Age is:13
    // City is:Mysore

    println("%-------------------------------%")

    class Window {
      var height = 3.2f
      var width = 5.6f

      def windet() {
        println("Window height is :"+height)
      }

      println("Window width is :"+width);
    }

    val win = new Window()
    win.windet()
    // Window width is :5.6
    // Window height is :3.2

    println("%-------------------------------%")

    class Subtract {
      def sub(s1:Int, s2:Int){
        var res = s1-s2
        println("Result is:"+res);
      }
    }

    val su = new Subtract()
    su.sub(40,10)
    // Result is:30

    println("%-------------------------------%")

    class Addition {
      def add() {
        var(a,b) = (10,20);
        var c = a+b;
        println("Result is:"+c)
      }
    }

    val ad = new Addition()
    ad.add()
    // Result is:30

    println("%-------------------------------%")

    val a:Int = 7
    val b:Float = a
    println(b)
    // 7.0

    val c:Char = 'a'
    val d:Int = c
    println(d)
    // 97

    val e:Char = 'A'
    val f:Float = e
    println(f)
    // 65.0

    println("%-------------------------------%")

    val str = "Strings are immutable in scala"
    val len = str.length()
    println("String Length is : " + len)
    // String Length is : 30

    println("%-------------------------------%")

    val str1 = "String concatenation can be "
    val str2 = "done using concat method"
    val st_concat = str1.concat(str2)
    println("Concatenated String is : " + st_concat)
    // Concatenated String is : String concatenation can be done using concat method
    val str3: String = "Student name  "
    val str4: String = "is Micheal"
    val st_concat2: String = str3 + str4
    println("Concatenated String is : " + st_concat2)
    // Concatenated String is : Student name  is Micheal

    println("%-------------------------------%")
    var n1 = 78.99
    var n2 = 49
    var s1 = "Hello, World!"
    var f1 = printf("The value of the float variable is " +
      "%f, while the value of the integer " +
      "variable is %d, and the string " +
      "is %s", n1, n2, s1)
    println(f1)
    // The value of the float variable is 78,990000,
    // while the value of the integer variable is 49,
    // and the string is Hello, World!()

    val firstName = "Chris"
    val lastName = "Harris"
    val age = 12;
    println("%s %s, %d".format(firstName, lastName, age))
    // Chris Harris, 12

    println("%-------------------------------%")

    val names= "Harry" :: ("Adam" :: ("Jill" :: Nil))
    val age2 = Nil
    println( "Head of names array : " + names.head)
    println( "Tail of names array : " + names.tail)
    println( "Check if names is empty : " + names.isEmpty)
    println( "Check if age is empty : " + age2.isEmpty)
    // Head of names array : Harry
    // Tail of names array : List(Adam, Jill)
    // Check if names is empty : false
    // Check if age is empty : true

    println("%-------------------------------%")

    val country_1 =  List("India","SriLanka","Algeria")
    val country_2 = List("Austria","Belgium","Canada")

    val country = country_1 ::: country_2
    println( "country_1 ::: country_2 : " + country)

    val cont = country_1.:::(country_2)
    println( "country_1.:::(country_2) : " + cont)
    val con = List.concat(country_1, country_2)
    println( "List.concat(country_1, country_2) : " + con)
    // country_1 ::: country_2 : List(India, SriLanka, Algeria, Austria, Belgium, Canada)
    // country_1.:::(country_2) : List(Austria, Belgium, Canada, India, SriLanka, Algeria)
    // List.concat(country_1, country_2) : List(India, SriLanka, Algeria, Austria, Belgium, Canada)

    println("%-------------------------------%")

    val country_rev = List("Denmark","Sweden","France")
    println("Country List before reversal :" + country_rev)
    println("Country List after reversal :" + country_rev.reverse)
    // Country List before reversal :List(Denmark, Sweden, France)
    // Country List after reversal :List(France, Sweden, Denmark)

    println("%-------------------------------%")

    val name = List.fill(6)("Rehan")
    println( "Name : " + name  )

    val id = List.fill(6)(12)
    println( "Id : " + id  )
    // Name : List(Rehan, Rehan, Rehan, Rehan, Rehan, Rehan)
    // Id : List(12, 12, 12, 12, 12, 12)

    println("%-------------------------------%")

    val name_set = Set("Smith", "Brown", "Allen")
    val id_set: Set[Int] = Set()

    println( "Head of name : " + name_set.head )
    println( "Tail of name : " + name_set.tail )
    println( "Check if name is empty : " + name_set.isEmpty )
    println( "Check if id is empty : " + id_set.isEmpty )
    // Head of name : Smith
    // Tail of name : Set(Brown, Allen)
    // Check if name is empty : false
    // Check if id is empty : true

    val furniture_1 = Set("Sofa", "Table", "chair")
    val furniture_2 = Set("Bed", "Door")

    var furniture = furniture_1 ++ furniture_2
    println( "furniture_1 ++ furniture_2 : " + furniture )

    var furn = furniture_1.++(furniture_2)
    println( "furniture_1.++(furniture_2) : " + furn )
    // furniture_1 ++ furniture_2 : HashSet(Door, Sofa, Bed, Table, chair)
    // furniture_1.++(furniture_2) : HashSet(Door, Sofa, Bed, Table, chair)

    val n1_set = Set(11,45,67,78,89,86,90)
    val n2_set = Set(10,20,45,67,34,78,98,89)

    println( "n1.&(n2) : " + n1_set.&(n2_set) )
    println( "n1.intersect(n2) : " + n1_set.intersect(n2_set) )
    // n1.&(n2) : HashSet(78, 89, 45, 67)
    // n1.intersect(n2) : HashSet(78, 89, 45, 67)

    val num1 = Set(125,45,678,34,20,322,10)

    println( "Minimum element in the Set is : " + num1.min )
    println( "Maximum element in the Set is : " + num1.max )
    // Minimum  element in the Set is : 10
    // Maximum  element in the Set is : 678

    println("%-------------------------------%")

    val student_map = Map(12 -> "Reena", 13 -> "Micheal", 14 -> "Peter")
    val marks_map: Map[String, Int] = Map()

    println("Keys : " + student_map.keys)
    println("Values : " + student_map.values)
    println("Check if student is empty : " + student_map.isEmpty)
    println("Check if marks is empty : " + marks_map.isEmpty)
    // Keys : Set(12, 13, 14)
    // Values : View(<not computed>)
    // Check if student is empty : false
    // Check if marks is empty : true

    val stud1 = Map(12 -> "Reena", 13 -> "Micheal" , 14 -> "Peter")
    val stud2 = Map(15 -> "Russel", 16 -> "Mark" , 17 -> "Steve")

    var student = stud1 ++ stud2
    println( "stud1 ++ stud2 : " + student )

    val stu_map = stud1.++(stud2)
    println( "stud1.++(stud2)) : " + stu_map )

    // stud1 ++ stud2 : HashMap(14 -> Peter, 13 -> Micheal, 17 -> Steve, 12 -> Reena, 16 -> Mark, 15 -> Russel)
    // stud1.++(stud2)) : HashMap(14 -> Peter, 13 -> Micheal, 17 -> Steve, 12 -> Reena, 16 -> Mark, 15 -> Russel)

    val stud3 = Map(15 -> "Russel", 16 -> "Mark" , 17 -> "Steve")
    stud3.keys.foreach { i =>
      print( "Key = " + i )
      println(" Value = " + stud3(i) )
    }
    // Key = 15 Value = Russel
    // Key = 16 Value = Mark
    // Key = 17 Value = Steve

    val stud4 = Map(15 -> "Russel", 16 -> "Mark" , 17 -> "Steve")

    if( stud4.contains( 15 )) {
      println("Student Id 15 exists with value :"  + stud4(15))
    } else {
      println("Student Id with 15 does not exist")
    }

    if( stud4.contains(16)) {
      println("Student Id 16  exists with value :"  + stud4(16))
    } else{
      println("Student Id 16 does not exist")
    }

    if(stud4.contains(17)) {
      println("Student Id 17  exists with value :"  + stud4(17))
    } else {
      println("Student Id 17 does not exist")
    }

    if(stud4.contains( 18 )){
      println("Student Id 18  exists with value :"  + stud4(18))
    } else {
      println("Student Id 18 does not exist")
    }
    // Student Id 15 exists with value :Russel
    // Student Id 16 exists with value :Mark
    // Student Id 17 exists with value :Steve
    // Student Id 18 does not exist

    var m = Map("Ayushi"->0,"Megha"->1)
    var m1 = Map("Megha"->3,"Ruchi"->2,"Becky"->4)
    println(m.++(m1))
    // Map(Ayushi -> 0, Megha -> 3, Ruchi -> 2, Becky -> 4)
    println(m1.++(m))
    // Map(Megha -> 1, Ruchi -> 2, Becky -> 4, Ayushi -> 0)
    println(m.-("Ayushi","Ruchi"))
    // Map(Megha -> 1)
    println(m.count(x=>true))
    // 2
    println(m.max)
    // (Megha,1)
    println(m.min)
    // (Ayushi,0)

    println("%-------------------------------%")

    val m1_tuple = (20, 12, 16, 4)
    val mul = m1_tuple._1 * m1_tuple._2 * m1_tuple._4
    println("Result is : " + mul)
    // Result is : 960 (20 * 12 * 4)

    println("%-------------------------------%")

    val names_tuple = ("John","Smith","Anderson","Steve","Rob")
    names_tuple.productIterator.foreach{ i => println("Value = " + i )}
    // Value = John
    // Value = Smith
    // Value = Anderson
    // Value = Steve
    // Value = Rob

    val id_tuple = new Tuple2(12,34)
    println("Swapped Tuple Id is:" + id_tuple.swap)
    // Swapped Tuple Id is:(34,12)

    val student_tuple = new Tuple3(12, "Rob", "IT")
    println("Concatenated String: " + student_tuple.toString())
    // Concatenated String: (12,Rob,IT)

    println("%-------------------------------%")

    val car = Iterator("Santro", "Punto", "WagonR", "Polo", "Audi")
    while (car.hasNext) {
      println(car.next())
    }
    // Santro
    // Punto
    // WagonR
    // Polo
    // Audi

    val c1 = Iterator("Santro", "Punto", "WagonR", "Polo", "Audi")
    val c2 = Iterator("Volkswagen", "Alto", "Xylo", "Innova")
    println("Iterator c1 : " + c1.size)
    println("Length of c2 : " + c2.length)
    // Iterator c1 : 5
    // Length of c2 : 4

    val m1_iterator = Iterator(12,45,67,89)
    val m2_iterator = Iterator(44,66,77,88)
    println("Smallest element " + m1_iterator.min )
    println("Largest element " + m2_iterator.max )
    // Smallest element 12
    // Largest element 88

    val it=Iterator(3,2,4,9,7)
    println(it.addString(new StringBuilder()))
    // 32497

    val it_buffered = Iterator(3,2,4,9,7)
    println(it_buffered.buffered)
    // <iterator>

    val it_indexOf=Iterator(3,2,4,9,7)
    println(it_indexOf.indexOf(2))
    // 1

    val it_mul = Iterator(3,2,4,9,7)
    println(it_mul.product)
    // 1512

    println("%-------------------------------%")

    val absentGreeting: Option[String] = Option(null) // absentGreeting will be None
    val presentGreeting: Option[String] = Option("Hello!") // presentGreeting will be Some("Hello!")

    case class User(
                     id: Int,
                     firstName: String,
                     lastName: String,
                     age: Int,
                     gender: Option[String]
                   )

    object UserRepository {
      private val users = Map(
        1 -> User(1, "John", "Doe", 32, Some("male")),
        2 -> User(2, "Johanna", "Doe", 30, None)
      )

      def findById(id: Int): Option[User] = users.get(id)
      def findAll = users.values
    }

    var user1 = UserRepository.findById(1)
    if (user1.isDefined) {
      println(user1.get.firstName)
    }
    // John
    var user2 = UserRepository.findById(2)
    if (user2.isDefined) {
      println("Gender: " + user2.get.gender.getOrElse("not specified"))
    }
    // Gender: not specified

    val user3 = User(2, "Johanna", "Doe", 30, None)
    val gender = user3.gender match {
      case Some(gender) => gender
      case None => "not specified"
    }
    println("Gender: " + gender)
    // Gender: not specified

    UserRepository.findById(2).foreach(user => println(user.firstName))
    // Johanna

    val age_user_1 = UserRepository.findById(1).map(_.age)
    println(age_user_1)
    // Some(32)

    val gender_user_1 = UserRepository.findById(1).map(_.gender)
    println(gender_user_1)
    // Some(Some(male))

    println(UserRepository.findById(1).flatMap(_.gender))
    println(UserRepository.findById(2).flatMap(_.gender))
    println(UserRepository.findById(3).flatMap(_.gender))
    // Some(male)
    // None
    // None

    val names_list: List[List[String]] = List (
      List("John", "Johanna", "Daniel"),
      List(),
      List("Doe", "Westheide")
    )
    println(names_list.map(_.map(_.toUpperCase)))
    // List(List(JOHN, JOHANNA, DANIEL), List(), List(DOE, WESTHEIDE))
    println(names_list.flatMap(_.map(_.toUpperCase)))
    // List(JOHN, JOHANNA, DANIEL, DOE, WESTHEIDE)

    val names_list_2: List[Option[String]] = List(
      Some("Johanna"),
      None,
      Some("Daniel")
    )
    println(names_list_2.map(_.map(_.toUpperCase)))
    // List(Some(JOHANNA), None, Some(DANIEL))
    println(names_list_2.flatMap(xs => xs.map(_.toUpperCase)))
    // List(JOHANNA, DANIEL)
    println(UserRepository.findById(1).filter(_.age > 30))
    // Some(User(1,John,Doe,32,Some(male))), because age is > 30
    println(UserRepository.findById(2).filter(_.age > 30))
    // None, because age is <= 30

    var for_comprehension_user_1 = for {
      user <- UserRepository.findById(1)
      gender <- user.gender
    } yield gender

    println(for_comprehension_user_1)
    // Some(male)

    var for_comprehension_all_users = for {
      user <- UserRepository.findAll
      gender <- user.gender
    } yield gender

    println(for_comprehension_all_users)
    // List("male")

    println("%-------------------------------%")

    println((1 to 10).toList)
    // List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    println((1 to 10).toStream)
    // Stream(1, <not computed>)
    val stream = 1 #:: 2 #:: 3 #:: Stream.empty
    println(stream)

    def isPrime(number: Int) =
      number > 1 && !(2 to number - 1).exists(e => e % number == 0)

    def generatePrimes(starting: Int): Stream[Int] = {
      if(isPrime(starting))
        starting #:: generatePrimes(starting + 1)
      else
        generatePrimes(starting + 1)
    }

    println(generatePrimes(100).take(10))
    // Stream(100, <not computed>)

    println(generatePrimes(100).take(10).force)
    // Stream(100, 101, 102, 103, 104, 105, 106, 107, 108, 109)
    println((1 to 1000000000).view.filter(_ % 2 != 0).take(20).toList)
    // List(1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39)

    println("%-------------------------------%")

    class Animal[+T](val animal:T)
    class Dog
    class Puppy extends Dog
    class AnimalCarer(val dog:Animal[Dog])

    val puppy = new Puppy
    val dog = new Dog

    val puppyAnimal:Animal[Puppy] = new Animal[Puppy](puppy)
    val dogAnimal:Animal[Dog] = new Animal[Dog](dog)

    val dogCarer = new AnimalCarer(dogAnimal)
    val puppyCarer = new AnimalCarer(puppyAnimal)

    println(dogCarer)
    println(puppyCarer)
    // Covariance in Scalar

    abstract class Type [-T]{
      def typeName : Unit
    }

    class SuperType extends Type[AnyVal]{
      override def typeName: Unit = {
        println("SuperType")
      }
    }

    class SubType extends Type[Int]{
      override def typeName: Unit = {
        println("SubType")
      }
    }

    class TypeCarer{
      def display(t: Type[Int]){
        t.typeName
      }
    }

    val superType = new SuperType
    val subType = new SubType

    val typeCarer = new TypeCarer

    typeCarer.display(subType)
    // SubType
    typeCarer.display(superType)
    // SuperType
    // Contra-variance

    println("%-------------------------------%")

    class Animal_UpperBound
    class Dog_UpperBound extends Animal_UpperBound
    class Puppy_UpperBound extends Dog_UpperBound

    class AnimalCarer_UpperBound {
      def display [T <: Dog_UpperBound](t: T){
        println(t)
      }
    }

    val animal_upperbound = new Animal_UpperBound
    val dog_upperbound = new Dog_UpperBound
    val puppy_upperbound = new Puppy_UpperBound
    val animalCarer = new AnimalCarer_UpperBound

    // animalCarer.display(animal_upperbound) => Error
    animalCarer.display(dog_upperbound)
    animalCarer.display(puppy_upperbound)

    // basic.Main$Dog_UpperBound$1@39c0f4a
    // basic.Main$Puppy_UpperBound$1@1794d431

    class Animal_LowerBound
    class Dog_LowerBound extends Animal_LowerBound
    class Puppy_LowerBound extends Animal_LowerBound

    class AnimalCarer_LowerBound {
      def display [T >: Puppy](t: T){
        println(t)
      }
    }

    val animal_lowerbound = new Animal_LowerBound
    val dog_lowerbound = new Dog_LowerBound
    val puppy_lowerbound = new Puppy_LowerBound
    val animalCarer_lowerbound = new AnimalCarer_LowerBound

    animalCarer_lowerbound.display(animal_lowerbound)
    animalCarer_lowerbound.display(dog_lowerbound)
    animalCarer_lowerbound.display(puppy_lowerbound)

    // basic.Main$Animal_LowerBound$1@42e26948
    // basic.Main$Dog_LowerBound$1@57baeedf
    // basic.Main$Puppy_LowerBound$1@343f4d3d

    class Person[T <% Ordered[T]](val firstName: T, val lastName: T) {
      def greater = if (firstName > lastName) firstName else lastName
    }

    val p1 = new Person("Rams","Posa")
    val p2 = new Person("Chintu","Charan")

    println(p1.greater)
    println(p2.greater)
    // Rams
    // Chintu

    println("%-------------------------------%")

    class MyClass {
      var myField = 0;

      def getMyField() : Int = {
        return this.myField;
      }

      def addToMyField(value : Int) {
        this.myField += value;
      }
    }

    var my_class = new MyClass()
    my_class.addToMyField(1)
    println(my_class.getMyField())
    // 1

    println("%-------------------------------%")

    abstract class Bike {
      def run()
    }

    class Hero extends Bike {
      def run(){
        println("running fine...")
      }
    }

    var h = new Hero()
    h.run()
    // running fine...

    println("%-------------------------------%")

    abstract class AbstractBike(a:Int){             // Tạo constructor
      var b:Int = 20                      // Tạo các biến
      var c:Int = 25
      def run()                           // phương thức
      def performance(){                  // Phương thức non-abstract
        println("Performance awesome")
      }
    }

    class SportBike (a:Int) extends AbstractBike (a){
      c = 30
      def run(){
        println("Running fine...")
        println("a = "+a)
        println("b = "+b)
        println("c = "+c)
      }
    }

    var sportBike = new SportBike(10)
    sportBike.run()
    sportBike.performance()
    // Running fine...
    // a = 10
    // b = 20
    // c = 30
    // Performance awesome

    println("%-------------------------------%")

    import scala.io.Source

    trait Cardetails {
      def details(d:String) : String
    }

    class Cardet extends Cardetails {
      override def details(source:String) = {
        Source.fromString(source).mkString
      }
    }

    val cardetInstance = new Cardet
    println(cardetInstance.details("Car details are being displayed"))
    println(cardetInstance.isInstanceOf[Cardetails])
    // Car details are being displayed
    // true

    trait detcar{
      def readdetails(d: String):String =
        Source.fromString(d).mkString
    }

    class Car(var cname: String, var cno: Int){
      def details = cname+" "+cno
    }

    class Alto( cname:String, cno:Int, var color:String)
      extends Car(cname, cno)
      with detcar {
        override def details = {
          val det = readdetails(color)
          cname+"\n"+cno+"\n"+"Color:"+color
        }
    }

    val a1 = new Alto("Alto",34,"Black")
    println(a1.details)
    // Alto
    // 34
    // Color:Black

    println("%-------------------------------%")

    object SingletonObject {
      def hello(){
        println("Hello, This is Singleton Object")
      }
    }

    SingletonObject.hello()
    // Hello, This is Singleton Object

    class ComapanionClass{
      def hello(){
        println("Hello, this is Companion Class.")
      }
    }

    object CompanionObject{
      def main(args:Array[String]){
        new ComapanionClass().hello()
        // Hello, this is Companion Class.
        println("And this is Companion Object.")
      }
    }

    CompanionObject.main(null)
    // println("And this is Companion Object.")

    println("%-------------------------------%")

    object firstNameObject {
      def main(args: Array[String]) {
        println("Apply method : " + apply("Steve", "Smith"));
        println("Unapply method : " + unapply("Steve Smith"));
        println("Unapply method : " + unapply("Rob"));
      }

      def apply(fname: String, lname: String): String = {
        fname + " " + lname
      }

      def unapply(s: String): Option[(String, String)] = {
        val pts = s split " "
        if (pts.length == 2) {
          Some(pts(0), pts(1))
        } else {
          None
        }
      }
    }

    firstNameObject.main(null)
    // Apply method : Steve Smith
    // Unapply method : Some((Steve,Smith))
    // Unapply method : None

    case class PatternAddress(city: String, state: String)
    case class PatternStudent(name: String, address: Seq[PatternAddress])

    object City {
      def unapply(s: PatternStudent): Option[Seq[String]] =
        Some(
          for (c <- s.address)
            yield c.state
        )
    }

    class StringSeqContains(value: String) {
      def unapply(in: Seq[String]): Boolean =
        in contains value
    }

    object PatternMatch {
      def main(args: Array[String]) {

        val stud = List(
          PatternStudent("Harris", List(PatternAddress("LosAngeles", "California"))),
          PatternStudent("Reena", List(PatternAddress("Houston", "Texas"))),
          PatternStudent("Rob", List(PatternAddress("Dallas", "Texas"))),
          PatternStudent("Chris", List(PatternAddress("Jacksonville", "Florida")))
        )

        val Texas = new StringSeqContains("Texas")

        val students = stud collect {
          case student @ City(Texas()) => student.name
        }

        println(students)
      }
    }

    PatternMatch.main(null)
    // List(Reena, Rob)
    println("%-------------------------------%")

    val boo: Boolean = 5 < 10
    println(boo match {
      case true => 1
      case false => 0
    })
    // 1

    def matchFunction(v: Int) = v match {
      case 1 => "one"
      case 2 => "two"
      case _ => "unknown number"
    }
    println(matchFunction(2))
    println(matchFunction(5))
    // two
    // unknown number

    trait Payment {
      def pay(amount: Double): Unit
    }

    class Cash extends Payment {
      def pay(amount: Double): Unit = println(s"Pay with cash $amount")
    }

    class CreditCard extends Payment {
      def pay(amount: Double): Unit = println(s"Pay with credit card $amount")
      def verify(): Unit = println("Verification...")
    }

    def processPayment(amount: Double, method: Payment) = method match {
      case c: Cash => c.pay(amount)
      case cc: CreditCard => cc.verify(); cc.pay(amount)
      case _ => println("Unknown payment method")
    }

    processPayment(10, new Cash)
    // Pay with cash 10.0
    processPayment(50, new CreditCard)
    // Verification...
    // Pay with credit card 50.0

    val commonList = List(1, 2, 3, 4, 5)
    val emptyList = Nil
    val oneElement = 'a' :: Nil

    def checkList[T](list: List[T]): String = list match {
      case Nil => "Empty list"
      case list :: Nil => "One element"
      case _ => "More than one element"
    }

    println(checkList(emptyList))
    // Empty list
    println(checkList(oneElement))
    // One element
    println(checkList(commonList))
    // More than one element

    val recursionCommonList = List(1, 2, 3, 4, 5)

    def sum(list: List[Int]): Int = {
      def recursion(sum: Int, list: List[Int]): Int = list match {
        case Nil => sum
        case el :: tail => (recursion(sum + el, tail))
      }
      recursion(0, list)
    }

    println(sum(recursionCommonList))
    //15

    println("%-------------------------------%")

    val sumAnonymous = (a:Int,b:Float) => a+b
    println(sumAnonymous(2, 3))

    var g=7
    val sum1=(a:Int,b:Int)=>(a+b)*g
    println(sum1(2,3))
    // 35
    g=8
    println(sum1(2,3))
    // 40
    var ag=22
    val sayhello = (name:String) => println(s"I am $name and I am $ag")
    sayhello("Ayushi")
    // I am Ayushi and I am 22
    def func(f:String => Unit, s:String ){
      f(s)
    }
    func(sayhello, "Ayushi")
    // I am Ayushi and I am 22

    val nums = Array(1,2,4)
    val saynum = (a:Int) => println(s"The number is $a")

    for (i <- 0 to 2) {
      saynum(nums(i))
    }
    // The number is 1
    // The number is 2
    // The number is 4

    import scala.collection.mutable.ArrayBuffer
    val arrayBufferColors = ArrayBuffer("Red","Green","Blue")
    val arrayBufferFunc = (f:String => Unit, s:String) => f(s)

    for(i <- 0 to 2){
      arrayBufferFunc(sayhello,
        arrayBufferColors(i))
    }
    // I am Red and I am 22
    // I am Green and I am 22
    // I am Blue and I am 22

    println("%-------------------------------%")

    import java.io._
    val fileObject = new File("ScalaFile.txt" )
    val printWriter = new PrintWriter(fileObject)
    printWriter.write("Hello, This is scala file")
    printWriter.close()

    val filename = "ScalaFile.txt"
    val fileSource = Source.fromFile(filename)
    while(fileSource.hasNext){
      println(fileSource.next)
    }
    fileSource.close()

    // H
    // e
    // l
    // l
    // o
    // ,
    //
    // T
    // h
    // i
    // s
    //
    // i
    // s
    //
    // s
    // c
    // a
    // l
    // a
    //
    // f
    // i
    // l
    // e

    import scala.io.Source
    val fileSource2 = Source.fromFile(filename)
    for(line <- fileSource2.getLines){
      println(line)
    }
    fileSource2.close()
    // Hello, This is scala file

    println("%-------------------------------%")

//    case class CustomList[A](elements: A*) {
//      val elems = new ArrayBuffer[A]
//      elems ++= elements
//
//      def foreach(c: A => Unit): Unit = {
//        elems.foreach(c)
//      }
//
//      def map[B](f: A => B): CustomList[B] = {
//        val temp = elems.map(f)
//        CustomList(temp: _*)
//      }
//
//      def withFilter(p: A => Boolean): CustomList[A] = {
//        val temp = elems.filter(p)
//        CustomList(temp: _*)
//      }
//
//      def flatMap[B](f: A => CustomList[B]): CustomList[B] = {
//        val temp = elems.map(f)
//        flatternLike(temp: _*)
//      }
//
//      def flatternLike[B](seq: Seq[CustomList[B]]): CustomList[B] = {
//        val temp = new ArrayBuffer[B]
//        for(i <- seq) {
//          for(j <- i) {
//            temp += j
//          }
//        }
//        CustomList(temp: _*)
//      }
//    }

    println("%-------------------------------%")

    case class Customer(age: Int)
    class Cigarettes

    case class UnderAgeException(message: String)
      extends Exception(message)

    def buyCigarettes(customer: Customer): Cigarettes =
      if (customer.age < 16)
        throw UnderAgeException(s"Customer must be older than 16 but was ${customer.age}")
      else new Cigarettes

    val youngCustomer = Customer(15)

    try {
      buyCigarettes(youngCustomer)
      println("Yo, here are your cancer sticks! Happy smokin'!")
    } catch {
      case UnderAgeException(msg) => println(msg)
    }
    // Customer must be older than 16 but was 15
    // Yo, here are your cancer sticks! Happy smokin' not shown

    import scala.util.Try
    import java.net.URL
    def parseURL(url: String): Try[URL] = Try(new URL(url))
    println(parseURL("https://itexpertvn.com").map(_.getProtocol))
    // Success("https")
    println(parseURL("garbage").map(_.getProtocol))
    // Failure(java.net.MalformedURLException: no protocol: garbage)
    println("%-------------------------------%")

    import scala.concurrent._
    import scala.concurrent.ExecutionContext.Implicits.global

    val oneFuture: Future[Int] = Future {
      Thread.sleep(1000)
      1
    }

    def checkState(): Unit = {
      println("Before the job finishes")
      Thread.sleep(500)
      println(s"Completed : ${oneFuture.isCompleted}, Value : ${oneFuture.value}")

      println("After the job finishes")
      Thread.sleep(1100)
      println(s"Completed : ${oneFuture.isCompleted}, Value : ${oneFuture.value}")
    }

    checkState()
    // Before the job finishes
    // Completed : false, Value : None
    // After the job finishes
    // Completed : true, Value : Some(Success(1))

    import scala.util.{Success, Failure}

    class PromisingFutures {
      val oneFuture: Future[Int] = Future {
        Thread.sleep(1000)
        1
      }

      val oneDangerousFuture = Future {
        Thread.sleep(2000)
        throw new SomeComputationException("Welcome to the Dark side !")
      }

      case class SomeComputationException(msg: String) extends Exception(msg)

      def printFuture[T](future: Future[T]): Unit = future.onComplete {
        case Success(result) => println(s"Success $result")
        case Failure(throwable) => println(s"Failure $throwable")
      }
    }

    val promisingFutures = new PromisingFutures()
    promisingFutures.printFuture(promisingFutures.oneFuture)
    promisingFutures.printFuture(promisingFutures.oneDangerousFuture)
    // Success 1
    // Failure basic.Main$PromisingFutures$1$SomeComputationException: Welcome to the Dark side!

    synchronized(wait(3000))

    class PromiseInternals {
      def aCompletedPromiseUsingSuccess(num: Int): Future[Int] = {
        val promise = Promise[Int]()
        promise.success(num)
        promise.future
      }

      def aCompletedPromiseUsingComplete(num:Int): Future[Int] = {
        val promise=Promise[Int]()
        promise.complete(Success(num))
        promise.future
      }

      def aCompletedPromiseUsingFailure(num:Int): Future[Int] = {
        val promise=Promise[Int]()
        promise.failure(new RuntimeException("Evil Exception"))
        promise.future
      }

      val somePool = Executors.newFixedThreadPool(2)

      def someExternalDelayedCalculation(f:()=>Int): Future[Int] = {
        val promise=Promise[Int]()
        val thisIsWhereWeCallSomeExternalComputation = new Runnable {
          override def run(): Unit ={
            promise.complete{
              try(Success(f()))
              catch {
                case NonFatal (msg)=> Failure(msg)
              }
            }
          }
        }

        somePool.execute(thisIsWhereWeCallSomeExternalComputation)
        promise.future
      }

      def alreadyCompletedPromise(): Future[Int] = {
        val promise = Promise[Int]()
        promise.success(100) //completed
        promise.failure(new RuntimeException("Will never be set because an IllegalStateException will be thrown beforehand"))
        promise.future
      }
    }

    // Give out the correct value when a Promise is completed
    val promiseInternals = new PromiseInternals
    val aCompletedPromise = promiseInternals.aCompletedPromiseUsingSuccess(100)
    println(aCompletedPromise)
    // Future(Success(100))

    val promiseInternals2 = new PromiseInternals
    val longCalculationFuture = promiseInternals2.someExternalDelayedCalculation{()=>
      Thread.sleep(2000)
      100
    }
    println (s"We have submitted a block to be executed asynchronously ${longCalculationFuture.isCompleted}")
    // We have submitted a block to be executed asynchronously false

    val promiseInternals3 = new PromiseInternals
    // println(promiseInternals3.alreadyCompletedPromise())
    // Exception in thread "main" java.lang.IllegalStateException: Promise already completed.

    class FutureCombinators {
      def sumOfThreeNumbersSequentialMap(): Future[Int] = {
        Future {
          Thread.sleep(1000)
          1
        }.flatMap { oneValue =>
          Future {
            Thread.sleep(2000)
            2
          }.flatMap { twoValue =>
            Future {
              Thread.sleep(3000)
              3
            }.map { thirdValue =>
              oneValue + twoValue + thirdValue
            }
          }
        }
      }

      val oneFutureParallel: Future[Int] = Future {
        Thread.sleep(1000)
        1
      }

      val twoFutureParallel: Future[Int] = Future {
        Thread.sleep(2000)
        2
      }

      val threeFutureParallel: Future[Int] = Future {
        Thread.sleep(3000)
        3
      }

      def sumOfThreeNumbersParallelMapForComprehension(): Future[Int] = for {
        oneValue <- oneFutureParallel
        twoValue <- twoFutureParallel
        threeValue <- threeFutureParallel
      } yield oneValue + twoValue + threeValue

      def sumOfThreeNumbersParallelWithGuard(): Future[Int] = for {
        oneValue <- oneFutureParallel
        twoValue <- twoFutureParallel if twoValue > 1
        threeValue <- threeFutureParallel
      } yield oneValue + twoValue + threeValue

      def throwsNoSuchElementIfGuardFails(): Future[Int] = for {
        oneValue <- oneFuture
        twoValue <- twoFutureParallel if twoValue > 2
        threeValue <- threeFutureParallel
      } yield oneValue + twoValue + threeValue

      val futureCallingLegacyCode: Future[Int] = Future {
        Thread.sleep(1000)
        throw new LegacyException("Danger! Danger!")
      }

      case class LegacyException(msg: String) extends Exception(msg)

      def throwsExceptionFromComputation(): Future[Int] = for {
        oneValue <- oneFuture
        futureThrowingException <- futureCallingLegacyCode
      } yield oneValue + futureThrowingException

      val futureCallingLegacyCodeWithRecover: Future[Int] = futureCallingLegacyCode.recover {
        case LegacyException(msg) => 200
      }

      def recoversFromExceptionUsingRecover(): Future[Int] = for {
        oneValue <- oneFuture
        futureThrowingException <- futureCallingLegacyCodeWithRecover
      } yield oneValue + futureThrowingException

      val futureCallingLegacyCodeWithRecoverWith: Future[Int] = futureCallingLegacyCode.recoverWith {
        case LegacyException(msg) =>
          println("Exception occurred. Recovering with a Future that wraps 1000")
          Thread.sleep(2000)
          Future(1000)
      }

      def recoversFromExceptionUsingRecoverWith(): Future[Int] = for {
        oneValue <- oneFuture
        futureThrowingException <- futureCallingLegacyCodeWithRecoverWith
      } yield oneValue + futureThrowingException

      val anotherErrorThrowingFuture: Future[Int] = Future {
        Thread.sleep(1000)
        throw new LegacyException("Dieded!!")
      }

      val futureRecoveringWithAnotherErrorThrowingFuture: Future[Int] = futureCallingLegacyCode.recoverWith {
        case LegacyException(msg) =>
          anotherErrorThrowingFuture
      }

      def recoversFromExceptionUsingRecoverWithThatFails(): Future[Int] = for {
        oneValue <- oneFuture
        futureThrowingException <- futureRecoveringWithAnotherErrorThrowingFuture
      } yield oneValue + futureThrowingException

      val futureFallingBackToAnotherErrorThrowingFuture: Future[Int] = futureCallingLegacyCode.fallbackTo(anotherErrorThrowingFuture)

      def recoversFromExceptionUsingFallbackTo(): Future[Int] = for {
        oneValue <- oneFuture
        futureThrowingException <- futureFallingBackToAnotherErrorThrowingFuture
      } yield oneValue + futureThrowingException
    }

    object ConcurrentUtils {
      def timed[T](block: => T): T = {
        val start = System.currentTimeMillis()
        val result = block
        val duration = System.currentTimeMillis() - start
        println(s"Time taken : $duration")
        result
      }
    }

    import scala.concurrent.Await
    import scala.concurrent.duration._
    import scala.language.postfixOps

    val futureCombinators = new FutureCombinators
    val result = ConcurrentUtils.timed(Await.result(futureCombinators.sumOfThreeNumbersSequentialMap(), 7 seconds))
    // Time taken: 6017

    val futureCombinators2 = new FutureCombinators
    val result2 = ConcurrentUtils.timed(Await.result(futureCombinators2.sumOfThreeNumbersParallelMapForComprehension(), 4 seconds))
    // Time taken : 3003

    val futureCombinators3 = new FutureCombinators
    val result3 = ConcurrentUtils.timed(Await.result(futureCombinators3.sumOfThreeNumbersParallelWithGuard(), 4 seconds))
    // Time taken : 3001

    val futureCombinators4 = new FutureCombinators
    val result4 = ConcurrentUtils.timed(Await.result(futureCombinators4.sumOfThreeNumbersParallelWithGuard(), 4 seconds))
    // Time taken : 3001

    // Could blow up on the caller code when guard fails
    val futureCombinators5 = new FutureCombinators
    // val result5 = ConcurrentUtils.timed(Await.result(futureCombinators5.throwsNoSuchElementIfGuardFails(), 4 seconds))
    // Future.filter predicate is not satisfied

    val futureCombinators6 = new FutureCombinators
    // val result6 = ConcurrentUtils.timed(Await.result(futureCombinators6.throwsExceptionFromComputation(), 4 seconds))
    // Danger! Danger!

    val futureCombinators7 = new FutureCombinators
    val result7 = ConcurrentUtils.timed(Await.result(futureCombinators7.recoversFromExceptionUsingRecover(), 4 seconds))
    // Time taken : 3006

    val futureCombinators8 = new FutureCombinators
    val result8 = ConcurrentUtils.timed(Await.result(futureCombinators8.recoversFromExceptionUsingRecoverWith(), 4 seconds))
    // Time taken : 3002

    val futureCombinators9 = new FutureCombinators
    // val result9 = ConcurrentUtils.timed(Await.result(futureCombinators9.recoversFromExceptionUsingRecoverWithThatFails(), 4 seconds))
    // Dieded!!

    val futureCombinators10 = new FutureCombinators
    // val result10 = ConcurrentUtils.timed(Await.result(futureCombinators10.recoversFromExceptionUsingFallbackTo(), 8 seconds))
    // Danger! Danger!

    println("%-------------------------------%")

    import scala.collection.mutable
    case class MapImm(m1: Map[Int, Int], m2: Map[Int, Int], m3: Map[Int, Int])
    case class MapMut(m1: mutable.Map[Int, Int], m2: mutable.Map[Int, Int], m3: mutable.Map[Int, Int])
    val range = (1 to 1000)

    println("%-------------------------------%")

    trait Computer {
      def getRAM():String
      def getHDD():String
      def getCPU():String

      override def toString = "RAM= " + getRAM +", HDD=" + getHDD + ", CPU=" + getCPU
    }

    class PC(val ram:String, val hdd:String, val cpu:String)
      extends Computer {
      def getRAM():String =  ram
      def getHDD():String = hdd
      def getCPU():String = cpu
    }

    class Server(val ram:String, val hdd:String, val cpu:String)
      extends Computer {
      def getRAM():String =  ram
      def getHDD():String = hdd
      def getCPU():String = cpu
    }

    object ComputerFactory {
      def apply(compType:String, ram:String, hdd:String, cpu:String) = compType.toUpperCase match {
        case "PC" => new PC(ram,hdd,cpu)
        case "SERVER" => new Server(ram,hdd,cpu)
      }
    }

    val pc = ComputerFactory("pc", "2 GB", "500 GB", "2.4 GHz")
    val server = ComputerFactory("server", "16 GB", "1 TB", "2.9 GHz")
    println("Factory PC Config::" + pc)
    println("Factory Server Config::" + server)
    // Factory PC Config::RAM= 2 GB, HDD=500 GB, CPU=2.4 GHz
    // Factory Server Config::RAM= 16 GB, HDD=1 TB, CPU=2.9 GHz

    println("%-------------------------------%")

    trait Target {
      def f
    }

    class Adaptee {
      def g = println("g")
    }

    trait Adapter {
      self: Target with Adaptee =>
      def f = g
    }

    val adapter = new Adaptee with Adapter with Target
    println(adapter.f)
    // g
    // ()
    println(adapter.g)
    // g
    // ()

    println("%-------------------------------%")

    trait Topping {
      def getName() : String
      def getPrice() : Double
      def addTopping() : Topping
    }

    class BasePizza extends Topping {
      def getName() : String = "Pizza"
      def getPrice() : Double = 77.0
      def addTopping() : Topping = this
    }

    class ToppingDecorator(val topping : Topping)
      extends Topping {
      var nextTopping : Topping = topping

      def getName() : String = nextTopping.getName()
      def getPrice() : Double = nextTopping.getPrice()
      def addTopping() : Topping = this
    }

    class CheeseTopping(override val topping : Topping)
      extends ToppingDecorator(topping) {
      override def getPrice() : Double = {
        super.getPrice() + 59.0
      }

      override def getName() : String = {
        val previous = super.getName()
        "Ocean Cheese " + previous
      }
    }

    class OnionTopping(override val topping : Topping)
      extends ToppingDecorator(topping) {
      override def getPrice() : Double = {
        super.getPrice() + 39.0
      }

      override def getName() : String = {
        val previous = super.getName()
        "Sprinkled Onion " + previous
      }
    }

    class Pizza {
      var topping : Topping = new BasePizza

      def getPrice() : Double = {
        topping.getPrice()
      }

      def getName() : String = {
        topping.getName()
      }

      def addNewTopping(toppingName : String) : Boolean = {
        toppingName match {
          case "Onion" => {
            this.topping = new OnionTopping(topping)
            true
          }
          case "Cheese" => {
            this.topping = new CheeseTopping(topping)
            true
          }
          case _ =>
            println("Topping unavailable! Better luck next time! :(")
            false
        }
      }
    }

    val pizza = new Pizza
    pizza.addNewTopping("Cheese")
    pizza.addNewTopping("Onion")
    println(s"You have ordered ${pizza.getName}")
    println(s"You have to pay Rupees ${pizza.getPrice}")
    // You have ordered Sprinkled Onion Ocean Cheese Pizza
    // You have to pay Rupees 175.0

    println("%-------------------------------%")

    case class DIUser(id: String, name: String, email: String)

    trait UserDALComponent {
      def getUser(id: String): DIUser
      def create(user: DIUser)
      def delete(user: DIUser)
    }

    class UserDAL extends UserDALComponent {
      def getUser(id: String): DIUser = {
        val user = DIUser("12334", "testUser", "test@knoldus.com")
        println("UserDAL: Getting user " + user)
        user
      }

      def create(user: DIUser) = {
        println("UserDAL: creating user: " + user)
      }

      def delete(user: DIUser) = {
        println("UserDAL: deleting user: " + user)
      }
    }

    class UserService(userDAL: UserDALComponent) {
      def getUserInfo(id: String): DIUser = {
        val user = userDAL.getUser(id)
        println("UserService: Getting user " + user)
        user
      }

      def createUser(user: DIUser) = {
        userDAL.create(user)
        println("UserService: creating user: " + user)
      }

      def deleteUser(user: DIUser) = {
        userDAL.delete(user)
        println("UserService: deleting user: " + user)
      }
    }

    val userService = new UserService(new UserDAL())
    userService.getUserInfo("1234")
    // UserDAL: Getting user DIUser(12334,testUser,test@knoldus.com)
    // UserService: Getting user DIUser(12334,testUser,test@knoldus.com)


    case class CakeUser(id: String, name: String, email: String)

    trait CakeUserDALComponent {
      val userDAL: UserDAL

      class UserDAL {
        // a dummy data access layer
        def getUser(id: String): CakeUser = {
          val user = CakeUser("12334", "testUser", "test@knoldus.com")
          println("UserDAL: Getting user " + user)
          user
        }

        def create(user: CakeUser) = {
          println("UserDAL: creating user: " + user)
        }

        def delete(user: CakeUser) = {
          println("UserDAL: deleting user: " + user)
        }
      }
    }

    trait CakeUserServiceComponent { this: CakeUserDALComponent =>
      val userService: UserService

      class UserService {
        def getUserInfo(id: String): CakeUser = {
          val user = userDAL.getUser(id)
          println("UserService: Getting user " + user)
          user
        }

        def createUser(user: CakeUser) = {
          userDAL.create(user)
          println("UserService: creating user: " + user)
        }

        def deleteUser(user: CakeUser) = {
          userDAL.delete(user)
          println("UserService: deleting user: " + user)
        }

      }
    }

    object CakeUserObj extends CakeUserServiceComponent with CakeUserDALComponent {
      val userService = new UserService
      val userDAL = new UserDAL // mock UserDAL object for unit testing
    }

    CakeUserObj.userService.getUserInfo("1234")
    // UserDAL: Getting user CakeUser(12334,testUser,test@knoldus.com)
    // UserService: Getting user CakeUser(12334,testUser,test@knoldus.com)

    println("%-------------------------------%")

    class TourAndTravelFacade {
      val ticketBooking = new TicketBooking
      val hotelBooking = new HotelBooking
      val cabBooking = new ShuttleBooking
      val guideBooking = new GuideBooking

      def bookHolidayPackage(tourPackage : String, destination : String, departure : String) : Boolean = {
        ticketBooking.bookFlight(tourPackage, destination, departure)
        hotelBooking.bookHotel(tourPackage, destination)
        cabBooking.bookShuttle(tourPackage)
        guideBooking.bookGuide(destination)
        true
      }
    }

    class TicketBooking {
      def bookFlight(tourPackage : String, destination : String, departure : String) : Unit = {
        println("Round Trip Flight tickets for " + destination + " & " + departure + " is booked under package " + tourPackage)
      }
    }

    class HotelBooking {
      def bookHotel(tourPackage : String, destination : String) : Unit = {
        println("Hotel booked at " + destination+ " under package " + tourPackage)
      }
    }

    class ShuttleBooking {
      def bookShuttle(tourPackage : String) : Unit = {
        println("Shuttle is booked under package " + tourPackage)
      }
    }

    class GuideBooking {
      def bookGuide(destination : String) : Unit = {
        println("A guide is booked for sightseeing at " + destination)
      }
    }

    val facade = new TourAndTravelFacade
    val tourPackage = "Platinum"
    val destination = "Udaipur"
    val departure = "Delhi"
    facade.bookHolidayPackage(tourPackage, destination, departure)
    // Round Trip Flight tickets for Udaipur & Delhi is booked under package Platinum
    // Hotel booked at Udaipur under package Platinum
    // Shuttle is booked under package Platinum
    // A guide is booked for sightseeing at Udaipur

    println("%-------------------------------%")

    def pfSum(firstNumber: Int, secondNumber: Int) = {
      firstNumber + secondNumber
    }

    println("Pure Function from 3 and 5 is: " + pfSum(3, 5))
    // Pure Function from 3 and 5 is: 8

    println("%-------------------------------%")

    val x = (0 to 4).toList
    // x: List[Int] = List(0, 1, 2, 3, 4)
    val x2 = x map { x => x * 3 }
    // x2: List[Int] = List(0, 3, 6, 9, 12)
    val x3 = x map { _ * 3 }
    // x3: List[Int] = List(0, 3, 6, 9, 12)
    val x4 = x map { _ * 0.1 }
    // x4: List[Double] = List(0.0, 0.1, 0.2, 0.30000000000000004, 0.4)

    val xv = x.toVector
    // xv: Vector[Int] = Vector(0, 1, 2, 3, 4)
    val xv2 = xv map { _ * 0.2 }
    // xv2: scala.collection.immutable.Vector[Double] = Vector(0.0, 0.2, 0.4, 0.6000000000000001, 0.8)
    val xv3 = for (xi <- xv) yield (xi * 0.2)
    // xv3: scala.collection.immutable.Vector[Double] = Vector(0.0, 0.2, 0.4, 0.6000000000000001, 0.8)

    /* trait F[T] {
      def map(f: T => S): F[S]
    } */

    val x5 = x map { x => List(x - 0.1, x + 0.1) }
    // x5: List[List[Double]] = List(
    //  List(-0.1, 0.1),
    //  List(0.9, 1.1),
    //  List(1.9, 2.1),
    //  List(2.9, 3.1),
    //  List(3.9, 4.1)
    //  )

    val x6 = x flatMap { x => List(x - 0.1, x + 0.1) }
    // x6: List[Double] = List(-0.1, 0.1, 0.9, 1.1, 1.9, 2.1, 2.9, 3.1, 3.9, 4.1)

    val y = (0 to 12 by 2).toList
    // y: List[Int] = List(0, 2, 4, 6, 8, 10, 12)

    // x: List[Int] = List(0, 1, 2, 3, 4)
    // y: List[Int] = List(0, 2, 4, 6, 8, 10, 12)
    val xy = x flatMap { xi => y map { yi => xi * yi } }
    // xy: List[Int] = List(
    //  0, 0, 0, 0, 0, 0, 0, 0,
    //  2, 4, 6, 8, 10, 12,
    //  0, 4, 8, 12, 16, 20, 24,
    //  0, 6, 12, 18, 24, 30, 36,
    //  0, 8, 16, 24, 32, 40, 48
    //  )
    val xy2 = for {
      xi <- x
      yi <- y
    } yield (xi * yi)
    // xy2: List[Int] = List(
    //  0, 0, 0, 0, 0, 0, 0, 0,
    //  2, 4, 6, 8, 10, 12,
    //  0, 4, 8, 12, 16, 20, 24,
    //  0, 6, 12, 18, 24, 30, 36,
    //  0, 8, 16, 24, 32, 40, 48
    //  )

    /* Monad
    trait M[T] {
      def map(f: T => S): M[S]
      def flatMap(f: T => M[S]): M[S]
    }
    */

    val three = Option(3)
    // three: Option[Int] = Some(3)
    val twelve = three map (_ * 4)
    // twelve: Option[Int] = Some(12)
    val four = Option(4)
    // four: Option[Int] = Some(4)
    val twelveB = three map (i => four map (i * _))
    // twelveB: Option[Option[Int]] = Some(Some(12))
    val twelveC = three flatMap (i => four map (i * _))
    // twelveC: Option[Int] = Some(12)
    val twelveD = for {
      i <- three
      j <- four
    } yield (i * j)
    // twelveD: Option[Int] = Some(12)

    val oops: Option[Int] = None
    val oopsB = for {
      i <- three
      j <- oops
    } yield (i * j)
    // oopsB: Option[Int] = None

    val oopsC = for {
      i <- oops
      j <- four
    } yield (i * j)
    // oopsC: Option[Int] = None

    import scala.concurrent.duration._
    import scala.concurrent.{Future,ExecutionContext,Await}
    import ExecutionContext.Implicits.global

    val func1 = Future{
      Thread.sleep(10000)
      1
    }

    val func2 = Future{
      Thread.sleep(10000)
      2
    }

    val func3 = for {
      v1 <- func1
      v2 <- func2
    } yield (v1+v2)

    println(Await.result(func3, 30.second))
    // 3 - it is rendered after 10 seconds because func1 + func2 run in parallel

    println("%-------------------------------%")
  }
}
